//POVRay-File created by 3d41.ulp v20110101
//C:/Users/jgurzoni/Documents/Futebol Robos/F180/Eletronica/kick revB_jrtest/kick.brd
//6/2/2011 3:09:56 AM

#version 3.5;

//Set to on if the file should be used as .inc
#local use_file_as_inc = off;
#if(use_file_as_inc=off)


//changes the apperance of resistors (1 Blob / 0 real)
#declare global_res_shape = 1;
//randomize color of resistors 1=random 0=same color
#declare global_res_colselect = 0;
//Number of the color for the resistors
//0=Green, 1="normal color" 2=Blue 3=Brown
#declare global_res_col = 1;
//Set to on if you want to render the PCB upside-down
#declare pcb_upsidedown = on;
//Set to x or z to rotate around the corresponding axis (referring to pcb_upsidedown)
#declare pcb_rotdir = x;
//Set the length off short pins over the PCB
#declare pin_length = 2.5;
#declare global_diode_bend_radius = 1;
#declare global_res_bend_radius = 1;
#declare global_solder = on;

#declare global_show_screws = on;
#declare global_show_washers = on;
#declare global_show_nuts = on;

#declare global_use_radiosity = on;

#declare global_ambient_mul = 1;
#declare global_ambient_mul_emit = 0;

//Animation
#declare global_anim = off;
#local global_anim_showcampath = no;

#declare global_fast_mode = off;

#declare col_preset = 0;
#declare pin_short = on;

#declare e3d_environment = off;

#local cam_x = 0;
#local cam_y = 256;
#local cam_z = -69;
#local cam_a = 20;
#local cam_look_x = 0;
#local cam_look_y = -3;
#local cam_look_z = 0;

#local pcb_rotate_x = 0;
#local pcb_rotate_y = 0;
#local pcb_rotate_z = 0;

#local pcb_board = on;
#local pcb_parts = on;
#local pcb_wire_bridges = off;
#if(global_fast_mode=off)
	#local pcb_polygons = on;
	#local pcb_silkscreen = on;
	#local pcb_wires = on;
	#local pcb_pads_smds = on;
#else
	#local pcb_polygons = off;
	#local pcb_silkscreen = off;
	#local pcb_wires = off;
	#local pcb_pads_smds = off;
#end

#local lgt1_pos_x = 30;
#local lgt1_pos_y = 45;
#local lgt1_pos_z = 15;
#local lgt1_intense = 0.734286;
#local lgt2_pos_x = -30;
#local lgt2_pos_y = 45;
#local lgt2_pos_z = 15;
#local lgt2_intense = 0.734286;
#local lgt3_pos_x = 30;
#local lgt3_pos_y = 45;
#local lgt3_pos_z = -10;
#local lgt3_intense = 0.734286;
#local lgt4_pos_x = -30;
#local lgt4_pos_y = 45;
#local lgt4_pos_z = -10;
#local lgt4_intense = 0.734286;

//Do not change these values
#declare pcb_height = 1.500000;
#declare pcb_cuheight = 0.035000;
#declare pcb_x_size = 80.000000;
#declare pcb_y_size = 30.000000;
#declare pcb_layer1_used = 1;
#declare pcb_layer16_used = 1;
#declare inc_testmode = off;
#declare global_seed=seed(389);
#declare global_pcb_layer_dis = array[16]
{
	0.000000,
	0.000000,
	0.000000,
	0.000000,
	0.000000,
	0.000000,
	0.000000,
	0.000000,
	0.000000,
	0.000000,
	0.000000,
	0.000000,
	0.000000,
	0.000000,
	0.000000,
	1.535000,
}
#declare global_pcb_real_hole = 2.000000;

#include "e3d_tools.inc"
#include "e3d_user.inc"

global_settings{charset utf8}

#declare col_brd = texture{pigment{rgb<0.084314,0.184314,0.309804>}}
#declare col_wrs = texture{pigment{rgb<0.368627,0.352941,0.054902>}}
#declare col_pds = texture{pigment{Gray70}}
#declare col_hls = texture{pigment{Black}}
#declare col_bgr = Gray50;
#declare col_slk = texture{pigment{White}}
#declare col_thl = texture{pigment{Gray70}}
#declare col_pol = texture{pigment{rgb<0.368627,0.352941,0.054902>}}

#if(e3d_environment=on)
sky_sphere {pigment {Navy}
pigment {bozo turbulence 0.65 octaves 7 omega 0.7 lambda 2
color_map {
[0.0 0.1 color rgb <0.85, 0.85, 0.85> color rgb <0.75, 0.75, 0.75>]
[0.1 0.5 color rgb <0.75, 0.75, 0.75> color rgbt <1, 1, 1, 1>]
[0.5 1.0 color rgbt <1, 1, 1, 1> color rgbt <1, 1, 1, 1>]}
scale <0.1, 0.5, 0.1>} rotate -90*x}
plane{y, -10.0-max(pcb_x_size,pcb_y_size)*abs(max(sin((pcb_rotate_x/180)*pi),sin((pcb_rotate_z/180)*pi)))
texture{T_Chrome_2D
normal{waves 0.1 frequency 3000.0 scale 3000.0}} translate<0,0,0>}
#end

//Animation data
#if(global_anim=on)
#declare global_anim_showcampath = no;
#end

#if((global_anim=on)|(global_anim_showcampath=yes))
#declare global_anim_npoints_cam_flight=0;
#warning "No/not enough Animation Data available (min. 3 points) (Flight path)"
#end

#if((global_anim=on)|(global_anim_showcampath=yes))
#declare global_anim_npoints_cam_view=0;
#warning "No/not enough Animation Data available (min. 3 points) (View path)"
#end

#if((global_anim=on)|(global_anim_showcampath=yes))
#end

#if((global_anim_showcampath=yes)&(global_anim=off))
#end
#if(global_anim=on)
camera
{
	location global_anim_spline_cam_flight(clock)
	#if(global_anim_npoints_cam_view>2)
		look_at global_anim_spline_cam_view(clock)
	#else
		look_at global_anim_spline_cam_flight(clock+0.01)-<0,-0.01,0>
	#end
	angle 45
}
light_source
{
	global_anim_spline_cam_flight(clock)
	color rgb <1,1,1>
	spotlight point_at 
	#if(global_anim_npoints_cam_view>2)
		global_anim_spline_cam_view(clock)
	#else
		global_anim_spline_cam_flight(clock+0.01)-<0,-0.01,0>
	#end
	radius 35 falloff  40
}
#else
camera
{
	location <cam_x,cam_y,cam_z>
	look_at <cam_look_x,cam_look_y,cam_look_z>
	angle cam_a
	//translates the camera that <0,0,0> is over the Eagle <0,0>
	//translate<-40.000000,0,-15.000000>
}
#end

background{col_bgr}
light_source{<lgt1_pos_x,lgt1_pos_y,lgt1_pos_z> White*lgt1_intense}
light_source{<lgt2_pos_x,lgt2_pos_y,lgt2_pos_z> White*lgt2_intense}
light_source{<lgt3_pos_x,lgt3_pos_y,lgt3_pos_z> White*lgt3_intense}
light_source{<lgt4_pos_x,lgt4_pos_y,lgt4_pos_z> White*lgt4_intense}
#end


#macro KICK(mac_x_ver,mac_y_ver,mac_z_ver,mac_x_rot,mac_y_rot,mac_z_rot)
union{
#if(pcb_board = on)
difference{
union{
//Board
prism{-1.500000,0.000000,8
<-18.000000,0.000000><62.000000,0.000000>
<62.000000,0.000000><62.000000,30.000000>
<62.000000,30.000000><-18.000000,30.000000>
<-18.000000,30.000000><-18.000000,0.000000>
texture{col_brd}}
}//End union(PCB)
//Holes(real)/Parts
//Holes(real)/Board
cylinder{<2.000000,1,2.000000><2.000000,-5,2.000000>1.600000 texture{col_hls}}
cylinder{<60.000000,1,2.005000><60.000000,-5,2.005000>1.600000 texture{col_hls}}
cylinder{<59.962000,1,28.054000><59.962000,-5,28.054000>1.600000 texture{col_hls}}
cylinder{<2.000000,1,28.000000><2.000000,-5,28.000000>1.600000 texture{col_hls}}
//Holes(real)/Vias
}//End difference(reale Bohrungen/Durchbrüche)
#end
#if(pcb_parts=on)//Parts
union{
#ifndef(pack_C1) #declare global_pack_C1=yes; object {CAP_SMD_CHIP_1206()translate<0,0,0> rotate<0,0.000000,0>rotate<0,-180.000000,0> rotate<0,0,0> translate<45.800000,0.000000,15.400000>translate<0,0.035000,0> }#end		//SMD Capacitor 1206 C1 100Kpf/250V C1206
#ifndef(pack_C2) #declare global_pack_C2=yes; object {CAP_SMD_CHIP_2220()translate<0,0,0> rotate<0,0.000000,0>rotate<0,0.000000,0> rotate<0,0,0> translate<26.600000,0.000000,14.500000>translate<0,0.035000,0> }#end		//SMD Capacitor 2220 C2 10uF/250V C2225K
#ifndef(pack_C3) #declare global_pack_C3=yes; object {CAP_SMD_CHIP_0603()translate<0,0,0> rotate<0,0.000000,0>rotate<0,0.000000,0> rotate<0,0,0> translate<-2.600000,0.000000,18.600000>translate<0,0.035000,0> }#end		//SMD Capacitor 0603 C3 1nF/50V C0603
#ifndef(pack_C4) #declare global_pack_C4=yes; object {CAP_SMD_CHIP_1206()translate<0,0,0> rotate<0,0.000000,0>rotate<0,0.000000,0> rotate<0,0,0> translate<1.600000,0.000000,9.600000>translate<0,0.035000,0> }#end		//SMD Capacitor 1206 C4 10nF/250V C1206
#ifndef(pack_D1) #declare global_pack_D1=yes; object {DIODE_SMD_SMC()translate<0,0,0> rotate<0,0.000000,0>rotate<0,-180.000000,0> rotate<0,0,0> translate<1.400000,0.000000,22.600000>translate<0,0.035000,0> }#end		//DO214 BA D1 MBRS3201T3G SMC
#ifndef(pack_D2) #declare global_pack_D2=yes; object {DIODE_SMD_SMB()translate<0,-0,0> rotate<0,0.000000,0>rotate<0,0.000000,0> rotate<0,0,180> translate<-2.200000,-1.500000,22.600000>translate<0,-0.035000,0> }#end		//DO214 AA D2 MBRS130LT3 SMB
#ifndef(pack_D3) #declare global_pack_D3=yes; object {DIODE_SMD_SMC()translate<0,-0,0> rotate<0,0.000000,0>rotate<0,-270.000000,0> rotate<0,0,180> translate<39.200000,-1.500000,13.800000>translate<0,-0.035000,0> }#end		//DO214 BA D3 MBRS3201T3G SMC
#ifndef(pack_D4) #declare global_pack_D4=yes; object {DIODE_SMD_SMC()translate<0,-0,0> rotate<0,0.000000,0>rotate<0,-90.000000,0> rotate<0,0,180> translate<46.200000,-1.500000,17.600000>translate<0,-0.035000,0> }#end		//DO214 BA D4 MBRS3201T3G SMC
#ifndef(pack_F1) #declare global_pack_F1=yes; object {SPC_FUSE2("2A",)translate<0,0,0> rotate<0,0.000000,0>rotate<0,-180.000000,0> rotate<0,0,0> translate<29.600000,0.000000,4.400000>}#end		//Fuse Holder (special.lib) F1 2A FUSE
#ifndef(pack_IC1) #declare global_pack_IC1=yes; object {IC_SMD_MB6S()translate<0,-0,0> rotate<0,180.000000,0>rotate<0,-90.000000,0> rotate<0,0,180> translate<27.000000,-1.500000,6.400000>}#end		//SMD Bridge IC1 PC817 SFH
#ifndef(pack_IC2) #declare global_pack_IC2=yes; object {IC_SMD_SO8("TC4427","",)translate<0,-0,0> rotate<0,0.000000,0>rotate<0,-270.000000,0> rotate<0,0,180> translate<6.800000,-1.500000,23.000000>translate<0,-0.035000,0> }#end		//SMD IC SO8 Package IC2 TC4427 SO08
#ifndef(pack_IC3) #declare global_pack_IC3=yes; object {IC_SMD_MB6S()translate<0,-0,0> rotate<0,180.000000,0>rotate<0,-90.000000,0> rotate<0,0,180> translate<27.000000,-1.500000,17.700000>}#end		//SMD Bridge IC3 PC817 SFH
#ifndef(pack_IC4) #declare global_pack_IC4=yes; object {IC_SMD_MB6S()translate<0,-0,0> rotate<0,180.000000,0>rotate<0,-90.000000,0> rotate<0,0,180> translate<27.100000,-1.500000,12.100000>}#end		//SMD Bridge IC4 PC817 SFH
#ifndef(pack_J1) #declare global_pack_J1=yes; object {CON_MOLEX_PSL2W()translate<0,0,0> rotate<0,0.000000,0>rotate<0,-90.000000,0> rotate<0,0,0> translate<48.400000,0.000000,6.000000>}#end		//PC mount header J1 22-05-7028-02 7395-02
#ifndef(pack_L1) #declare global_pack_L1=yes; object {SPC_L_WE_PD_L("47uH/2A",)translate<0,0,0> rotate<0,0.000000,0>rotate<0,0.000000,0> rotate<0,0,0> translate<-9.000000,0.000000,7.000000>}#end		//Inductor WE-PD Wuerth Elektronik L1 47uH/2A DR125
#ifndef(pack_LED1) #declare global_pack_LED1=yes; object {DIODE_DIS_LED_3MM(Red,0.500000,0.000000,)translate<0,0,0> rotate<0,0.000000,0>rotate<0,-180.000000,0> rotate<0,0,0> translate<55.600000,0.000000,27.600000>}#end		//Diskrete 3MM LED LED1  LED3MM
#ifndef(pack_Q1) #declare global_pack_Q1=yes; object {TR_TO220_3_V("IRF740",)translate<0,0,0> rotate<0,0.000000,0>rotate<0,0.000000,0> rotate<0,0,0> translate<-9.600000,0.000000,29.600000>}#end		//TO220 vertical straight leads Q1 IRF740 TO220BV
#ifndef(pack_Q2) #declare global_pack_Q2=yes; object {TR_TO220_3_V("IRF3704L-V",)translate<0,0,0> rotate<0,0.000000,0>rotate<0,0.000000,0> rotate<0,0,0> translate<26.400000,0.000000,26.400000>}#end		//TO220 vertical straight leads Q2 IRF3704L-V TO262-V
#ifndef(pack_Q3) #declare global_pack_Q3=yes; object {TR_TO220_3_V("IRF3704L-V",)translate<0,0,0> rotate<0,0.000000,0>rotate<0,0.000000,0> rotate<0,0,0> translate<38.000000,0.000000,26.400000>}#end		//TO220 vertical straight leads Q3 IRF3704L-V TO262-V
#ifndef(pack_R1) #declare global_pack_R1=yes; object {RES_SMD_CHIP_2010("1R0",)translate<0,0,0> rotate<0,0.000000,0>rotate<0,-90.000000,0> rotate<0,0,0> translate<-15.600000,0.000000,17.000000>translate<0,0.035000,0> }#end		//SMD Resistor 2010 R1 0.1R 2W M2012
#ifndef(pack_R2) #declare global_pack_R2=yes; object {RES_SMD_CHIP_0805("470",)translate<0,0,0> rotate<0,0.000000,0>rotate<0,0.000000,0> rotate<0,0,0> translate<-14.400000,0.000000,22.100000>translate<0,0.035000,0> }#end		//SMD Resistor 0805 R2 47R M0805
#ifndef(pack_R3) #declare global_pack_R3=yes; object {RES_SMD_CHIP_0805("681",)translate<0,0,0> rotate<0,0.000000,0>rotate<0,-180.000000,0> rotate<0,0,0> translate<1.800000,0.000000,6.400000>translate<0,0.035000,0> }#end		//SMD Resistor 0805 R3 680R M0805
#ifndef(pack_R4) #declare global_pack_R4=yes; object {RES_SMD_CHIP_2512("332",)translate<0,0,0> rotate<0,0.000000,0>rotate<0,-180.000000,0> rotate<0,0,0> translate<47.000000,0.000000,19.000000>translate<0,0.035000,0> }#end		//SMD Resistor 2512 R4 3K3 2W R2512
#ifndef(pack_R5) #declare global_pack_R5=yes; object {RES_SMD_CHIP_0805("102",)translate<0,-0,0> rotate<0,0.000000,0>rotate<0,-180.000000,0> rotate<0,0,180> translate<0.800000,-1.500000,12.600000>translate<0,-0.035000,0> }#end		//SMD Resistor 0805 R5 1K M0805
#ifndef(pack_R6) #declare global_pack_R6=yes; object {RES_SMD_CHIP_0805("102",)translate<0,0,0> rotate<0,0.000000,0>rotate<0,0.000000,0> rotate<0,0,0> translate<-8.000000,0.000000,22.800000>translate<0,0.035000,0> }#end		//SMD Resistor 0805 R6 1K M0805
#ifndef(pack_R7) #declare global_pack_R7=yes; object {RES_SMD_CHIP_0805("154",)translate<0,0,0> rotate<0,0.000000,0>rotate<0,-90.000000,0> rotate<0,0,0> translate<6.800000,0.000000,3.600000>translate<0,0.035000,0> }#end		//SMD Resistor 0805 R7 150K M0805
#ifndef(pack_R8) #declare global_pack_R8=yes; object {RES_SMD_CHIP_0805("102",)translate<0,0,0> rotate<0,0.000000,0>rotate<0,-90.000000,0> rotate<0,0,0> translate<6.600000,0.000000,8.800000>translate<0,0.035000,0> }#end		//SMD Resistor 0805 R8 1K M0805
#ifndef(pack_R9) #declare global_pack_R9=yes; object {RES_SMD_CHIP_0805("391",)translate<0,-0,0> rotate<0,0.000000,0>rotate<0,0.000000,0> rotate<0,0,180> translate<16.500000,-1.500000,10.300000>translate<0,-0.035000,0> }#end		//SMD Resistor 0805 R9 390R M0805
#ifndef(pack_R10) #declare global_pack_R10=yes; object {RES_SMD_CHIP_2512("223",)translate<0,-0,0> rotate<0,0.000000,0>rotate<0,-180.000000,0> rotate<0,0,180> translate<39.200000,-1.500000,22.200000>translate<0,-0.035000,0> }#end		//SMD Resistor 2512 R10 22K 2W R2512
#ifndef(pack_R11) #declare global_pack_R11=yes; object {RES_SMD_CHIP_0805("122",)translate<0,-0,0> rotate<0,0.000000,0>rotate<0,-180.000000,0> rotate<0,0,180> translate<17.100000,-1.500000,17.000000>translate<0,-0.035000,0> }#end		//SMD Resistor 0805 R11 1K2 M0805
#ifndef(pack_R12) #declare global_pack_R12=yes; object {RES_SMD_CHIP_0805("122",)translate<0,-0,0> rotate<0,0.000000,0>rotate<0,-90.000000,0> rotate<0,0,180> translate<11.200000,-1.500000,4.400000>translate<0,-0.035000,0> }#end		//SMD Resistor 0805 R12 1K2 M0805
#ifndef(pack_R13) #declare global_pack_R13=yes; object {RES_SMD_CHIP_0805("102",)translate<0,-0,0> rotate<0,0.000000,0>rotate<0,-90.000000,0> rotate<0,0,180> translate<19.800000,-1.500000,26.800000>translate<0,-0.035000,0> }#end		//SMD Resistor 0805 R13 1K M0805
#ifndef(pack_R14) #declare global_pack_R14=yes; object {RES_SMD_CHIP_0805("102",)translate<0,-0,0> rotate<0,0.000000,0>rotate<0,-90.000000,0> rotate<0,0,180> translate<32.000000,-1.500000,26.800000>translate<0,-0.035000,0> }#end		//SMD Resistor 0805 R14 1K M0805
#ifndef(pack_R15) #declare global_pack_R15=yes; object {RES_SMD_CHIP_0805("391",)translate<0,-0,0> rotate<0,0.000000,0>rotate<0,-180.000000,0> rotate<0,0,180> translate<16.800000,-1.500000,13.600000>translate<0,-0.035000,0> }#end		//SMD Resistor 0805 R15 390R M0805
#ifndef(pack_R16) #declare global_pack_R16=yes; object {RES_SMD_CHIP_0805("122",)translate<0,0,0> rotate<0,0.000000,0>rotate<0,-180.000000,0> rotate<0,0,0> translate<5.400000,0.000000,12.800000>translate<0,0.035000,0> }#end		//SMD Resistor 0805 R16 1K2 M0805
#ifndef(pack_S1) #declare global_pack_S1=yes; object {SWITCH_B3F_10XX1()translate<0,0,0> rotate<0,0.000000,0>rotate<0,0.000000,0> rotate<0,0,0> translate<48.200000,0.000000,26.400000>}#end		//Tactile Switch-Omron S1  B3F-10XX
#ifndef(pack_SV1) #declare global_pack_SV1=yes; object {CON_PH_2X4()translate<0,-0,0> rotate<0,0.000000,0>rotate<0,-90.000000,0> rotate<0,0,180> translate<11.000000,-1.500000,15.000000>}#end		//Header 2,54mm Grid 4Pin 2Row (jumper.lib) SV1  MA04-2
#ifndef(pack_T1) #declare global_pack_T1=yes; object {IC_SMD_SOT23("MMBT3906","",)translate<0,-0,0> rotate<0,0.000000,0>rotate<0,-180.000000,0> rotate<0,0,180> translate<-10.800000,-1.500000,20.900000>translate<0,-0.035000,0> }#end		//SOT23 T1 MMBT3906 SOT23-BEC
#ifndef(pack_U1) #declare global_pack_U1=yes; object {IC_SMD_SO8("MC34063AD","",)translate<0,0,0> rotate<0,0.000000,0>rotate<0,-270.000000,0> rotate<0,0,0> translate<-9.600000,0.000000,17.400000>translate<0,0.035000,0> }#end		//SMD IC SO8 Package U1 MC34063AD SOIC8
#ifndef(pack_X1) #declare global_pack_X1=yes; object {CON_MOLEX_PSL2G()translate<0,0,0> rotate<0,0.000000,0>rotate<0,-270.000000,0> rotate<0,0,0> translate<14.600000,0.000000,25.800000>}#end		//PC mount header X1  KK-156-2
#ifndef(pack_X2) #declare global_pack_X2=yes; object {CON_MOLEX_PSL4G()translate<0,0,0> rotate<0,0.000000,0>rotate<0,-90.000000,0> rotate<0,0,0> translate<57.000000,0.000000,17.000000>}#end		//PC mount header X2  KK-156-4
}//End union
#end
#if(pcb_pads_smds=on)
//Pads&SMD/Parts
object{TOOLS_PCB_SMD(1.600000,1.800000,0.037000,0) rotate<0,-180.000000,0> texture{col_pds} translate<47.200000,0.000000,15.400000>}
object{TOOLS_PCB_SMD(1.600000,1.800000,0.037000,0) rotate<0,-180.000000,0> texture{col_pds} translate<44.400000,0.000000,15.400000>}
object{TOOLS_PCB_SMD(1.850000,6.800000,0.037000,0) rotate<0,-0.000000,0> texture{col_pds} translate<24.050000,0.000000,14.500000>}
object{TOOLS_PCB_SMD(1.850000,6.800000,0.037000,0) rotate<0,-0.000000,0> texture{col_pds} translate<29.150000,0.000000,14.500000>}
object{TOOLS_PCB_SMD(1.100000,1.000000,0.037000,0) rotate<0,-0.000000,0> texture{col_pds} translate<-3.450000,0.000000,18.600000>}
object{TOOLS_PCB_SMD(1.100000,1.000000,0.037000,0) rotate<0,-0.000000,0> texture{col_pds} translate<-1.750000,0.000000,18.600000>}
object{TOOLS_PCB_SMD(1.600000,1.800000,0.037000,0) rotate<0,-0.000000,0> texture{col_pds} translate<0.200000,0.000000,9.600000>}
object{TOOLS_PCB_SMD(1.600000,1.800000,0.037000,0) rotate<0,-0.000000,0> texture{col_pds} translate<3.000000,0.000000,9.600000>}
object{TOOLS_PCB_SMD(2.800000,3.800000,0.037000,0) rotate<0,-180.000000,0> texture{col_pds} translate<-2.300000,0.000000,22.600000>}
object{TOOLS_PCB_SMD(2.800000,3.800000,0.037000,0) rotate<0,-180.000000,0> texture{col_pds} translate<5.100000,0.000000,22.600000>}
object{TOOLS_PCB_SMD(2.400000,2.400000,0.037000,0) rotate<0,-180.000000,0> texture{col_pds} translate<-4.400000,-1.537000,22.600000>}
object{TOOLS_PCB_SMD(2.400000,2.400000,0.037000,0) rotate<0,-180.000000,0> texture{col_pds} translate<0.000000,-1.537000,22.600000>}
object{TOOLS_PCB_SMD(2.800000,3.800000,0.037000,0) rotate<0,-270.000000,0> texture{col_pds} translate<39.200000,-1.537000,10.100000>}
object{TOOLS_PCB_SMD(2.800000,3.800000,0.037000,0) rotate<0,-270.000000,0> texture{col_pds} translate<39.200000,-1.537000,17.500000>}
object{TOOLS_PCB_SMD(2.800000,3.800000,0.037000,0) rotate<0,-90.000000,0> texture{col_pds} translate<46.200000,-1.537000,21.300000>}
object{TOOLS_PCB_SMD(2.800000,3.800000,0.037000,0) rotate<0,-90.000000,0> texture{col_pds} translate<46.200000,-1.537000,13.900000>}
#ifndef(global_pack_F1) #local global_tmp=0; #else #local global_tmp=100; #end object{TOOLS_PCB_VIA(1.981200,1.320800,1,16,3+global_tmp,100) rotate<0,-180.000000,0>translate<41.030000,0,4.400000> texture{col_thl}}
#ifndef(global_pack_F1) #local global_tmp=0; #else #local global_tmp=100; #end object{TOOLS_PCB_VIA(1.981200,1.320800,1,16,3+global_tmp,100) rotate<0,-180.000000,0>translate<18.170000,0,4.400000> texture{col_thl}}
object{TOOLS_PCB_SMD(1.400000,1.800000,0.037000,0) rotate<0,-90.000000,0> texture{col_pds} translate<32.080000,-1.537000,7.670000>}
object{TOOLS_PCB_SMD(1.400000,1.800000,0.037000,0) rotate<0,-90.000000,0> texture{col_pds} translate<32.080000,-1.537000,5.130000>}
object{TOOLS_PCB_SMD(1.400000,1.800000,0.037000,0) rotate<0,-90.000000,0> texture{col_pds} translate<21.920000,-1.537000,5.130000>}
object{TOOLS_PCB_SMD(1.400000,1.800000,0.037000,0) rotate<0,-90.000000,0> texture{col_pds} translate<21.920000,-1.537000,7.670000>}
object{TOOLS_PCB_SMD(0.720000,1.780000,0.037000,0) rotate<0,-270.000000,0> texture{col_pds} translate<9.150000,-1.537000,24.905000>}
object{TOOLS_PCB_SMD(0.720000,1.780000,0.037000,0) rotate<0,-270.000000,0> texture{col_pds} translate<9.150000,-1.537000,23.635000>}
object{TOOLS_PCB_SMD(0.720000,1.780000,0.037000,0) rotate<0,-270.000000,0> texture{col_pds} translate<9.150000,-1.537000,22.365000>}
object{TOOLS_PCB_SMD(0.720000,1.780000,0.037000,0) rotate<0,-270.000000,0> texture{col_pds} translate<9.150000,-1.537000,21.095000>}
object{TOOLS_PCB_SMD(0.720000,1.780000,0.037000,0) rotate<0,-270.000000,0> texture{col_pds} translate<4.450000,-1.537000,21.095000>}
object{TOOLS_PCB_SMD(0.720000,1.780000,0.037000,0) rotate<0,-270.000000,0> texture{col_pds} translate<4.450000,-1.537000,22.365000>}
object{TOOLS_PCB_SMD(0.720000,1.780000,0.037000,0) rotate<0,-270.000000,0> texture{col_pds} translate<4.450000,-1.537000,23.635000>}
object{TOOLS_PCB_SMD(0.720000,1.780000,0.037000,0) rotate<0,-270.000000,0> texture{col_pds} translate<4.450000,-1.537000,24.905000>}
object{TOOLS_PCB_SMD(1.400000,1.800000,0.037000,0) rotate<0,-90.000000,0> texture{col_pds} translate<32.080000,-1.537000,18.970000>}
object{TOOLS_PCB_SMD(1.400000,1.800000,0.037000,0) rotate<0,-90.000000,0> texture{col_pds} translate<32.080000,-1.537000,16.430000>}
object{TOOLS_PCB_SMD(1.400000,1.800000,0.037000,0) rotate<0,-90.000000,0> texture{col_pds} translate<21.920000,-1.537000,16.430000>}
object{TOOLS_PCB_SMD(1.400000,1.800000,0.037000,0) rotate<0,-90.000000,0> texture{col_pds} translate<21.920000,-1.537000,18.970000>}
object{TOOLS_PCB_SMD(1.400000,1.800000,0.037000,0) rotate<0,-90.000000,0> texture{col_pds} translate<32.180000,-1.537000,13.370000>}
object{TOOLS_PCB_SMD(1.400000,1.800000,0.037000,0) rotate<0,-90.000000,0> texture{col_pds} translate<32.180000,-1.537000,10.830000>}
object{TOOLS_PCB_SMD(1.400000,1.800000,0.037000,0) rotate<0,-90.000000,0> texture{col_pds} translate<22.020000,-1.537000,10.830000>}
object{TOOLS_PCB_SMD(1.400000,1.800000,0.037000,0) rotate<0,-90.000000,0> texture{col_pds} translate<22.020000,-1.537000,13.370000>}
#ifndef(global_pack_J1) #local global_tmp=0; #else #local global_tmp=100; #end object{TOOLS_PCB_VIA(1.524000,1.016000,1,16,3+global_tmp,100) rotate<0,-180.000000,0>translate<48.400000,0,7.270000> texture{col_thl}}
#ifndef(global_pack_J1) #local global_tmp=0; #else #local global_tmp=100; #end object{TOOLS_PCB_VIA(1.524000,1.016000,1,16,3+global_tmp,100) rotate<0,-180.000000,0>translate<48.400000,0,4.730000> texture{col_thl}}
object{TOOLS_PCB_SMD(3.850000,5.500000,0.037000,0) rotate<0,-0.000000,0> texture{col_pds} translate<-13.975000,0.000000,7.000000>}
object{TOOLS_PCB_SMD(3.850000,5.500000,0.037000,0) rotate<0,-0.000000,0> texture{col_pds} translate<-4.025000,0.000000,7.000000>}
#ifndef(global_pack_LED1) #local global_tmp=0; #else #local global_tmp=100; #end object{TOOLS_PCB_VIA(1.320800,0.812800,1,16,2+global_tmp,0) rotate<0,-180.000000,0>translate<56.870000,0,27.600000> texture{col_thl}}
#ifndef(global_pack_LED1) #local global_tmp=0; #else #local global_tmp=100; #end object{TOOLS_PCB_VIA(1.320800,0.812800,1,16,2+global_tmp,0) rotate<0,-180.000000,0>translate<54.330000,0,27.600000> texture{col_thl}}
#ifndef(global_pack_Q1) #local global_tmp=0; #else #local global_tmp=100; #end object{TOOLS_PCB_VIA(1.524000,1.016000,1,16,3+global_tmp,100) rotate<0,-90.000000,0>translate<-9.600000,0,27.060000> texture{col_thl}}
#ifndef(global_pack_Q1) #local global_tmp=0; #else #local global_tmp=100; #end object{TOOLS_PCB_VIA(1.524000,1.016000,1,16,3+global_tmp,100) rotate<0,-90.000000,0>translate<-12.140000,0,27.060000> texture{col_thl}}
#ifndef(global_pack_Q1) #local global_tmp=0; #else #local global_tmp=100; #end object{TOOLS_PCB_VIA(1.524000,1.016000,1,16,3+global_tmp,100) rotate<0,-90.000000,0>translate<-7.060000,0,27.060000> texture{col_thl}}
#ifndef(global_pack_Q2) #local global_tmp=0; #else #local global_tmp=100; #end object{TOOLS_PCB_VIA(1.524000,1.016000,1,16,3+global_tmp,100) rotate<0,-90.000000,0>translate<23.860000,0,26.400000> texture{col_thl}}
#ifndef(global_pack_Q2) #local global_tmp=0; #else #local global_tmp=100; #end object{TOOLS_PCB_VIA(1.524000,1.016000,1,16,3+global_tmp,100) rotate<0,-90.000000,0>translate<26.400000,0,26.400000> texture{col_thl}}
#ifndef(global_pack_Q2) #local global_tmp=0; #else #local global_tmp=100; #end object{TOOLS_PCB_VIA(1.524000,1.016000,1,16,3+global_tmp,100) rotate<0,-90.000000,0>translate<28.940000,0,26.400000> texture{col_thl}}
#ifndef(global_pack_Q3) #local global_tmp=0; #else #local global_tmp=100; #end object{TOOLS_PCB_VIA(1.524000,1.016000,1,16,3+global_tmp,100) rotate<0,-90.000000,0>translate<35.460000,0,26.400000> texture{col_thl}}
#ifndef(global_pack_Q3) #local global_tmp=0; #else #local global_tmp=100; #end object{TOOLS_PCB_VIA(1.524000,1.016000,1,16,3+global_tmp,100) rotate<0,-90.000000,0>translate<38.000000,0,26.400000> texture{col_thl}}
#ifndef(global_pack_Q3) #local global_tmp=0; #else #local global_tmp=100; #end object{TOOLS_PCB_VIA(1.524000,1.016000,1,16,3+global_tmp,100) rotate<0,-90.000000,0>translate<40.540000,0,26.400000> texture{col_thl}}
object{TOOLS_PCB_SMD(1.300000,1.600000,0.037000,0) rotate<0,-90.000000,0> texture{col_pds} translate<-15.600000,0.000000,16.050000>}
object{TOOLS_PCB_SMD(1.300000,1.600000,0.037000,0) rotate<0,-90.000000,0> texture{col_pds} translate<-15.600000,0.000000,17.950000>}
object{TOOLS_PCB_SMD(1.300000,1.600000,0.037000,0) rotate<0,-0.000000,0> texture{col_pds} translate<-15.350000,0.000000,22.100000>}
object{TOOLS_PCB_SMD(1.300000,1.600000,0.037000,0) rotate<0,-0.000000,0> texture{col_pds} translate<-13.450000,0.000000,22.100000>}
object{TOOLS_PCB_SMD(1.300000,1.600000,0.037000,0) rotate<0,-180.000000,0> texture{col_pds} translate<2.750000,0.000000,6.400000>}
object{TOOLS_PCB_SMD(1.300000,1.600000,0.037000,0) rotate<0,-180.000000,0> texture{col_pds} translate<0.850000,0.000000,6.400000>}
object{TOOLS_PCB_SMD(1.800000,3.200000,0.037000,0) rotate<0,-180.000000,0> texture{col_pds} translate<49.800000,0.000000,19.000000>}
object{TOOLS_PCB_SMD(1.800000,3.200000,0.037000,0) rotate<0,-180.000000,0> texture{col_pds} translate<44.200000,0.000000,19.000000>}
object{TOOLS_PCB_SMD(1.300000,1.600000,0.037000,0) rotate<0,-0.000000,0> texture{col_pds} translate<-0.150000,-1.537000,12.600000>}
object{TOOLS_PCB_SMD(1.300000,1.600000,0.037000,0) rotate<0,-0.000000,0> texture{col_pds} translate<1.750000,-1.537000,12.600000>}
object{TOOLS_PCB_SMD(1.300000,1.600000,0.037000,0) rotate<0,-0.000000,0> texture{col_pds} translate<-8.950000,0.000000,22.800000>}
object{TOOLS_PCB_SMD(1.300000,1.600000,0.037000,0) rotate<0,-0.000000,0> texture{col_pds} translate<-7.050000,0.000000,22.800000>}
object{TOOLS_PCB_SMD(1.300000,1.600000,0.037000,0) rotate<0,-90.000000,0> texture{col_pds} translate<6.800000,0.000000,2.650000>}
object{TOOLS_PCB_SMD(1.300000,1.600000,0.037000,0) rotate<0,-90.000000,0> texture{col_pds} translate<6.800000,0.000000,4.550000>}
object{TOOLS_PCB_SMD(1.300000,1.600000,0.037000,0) rotate<0,-90.000000,0> texture{col_pds} translate<6.600000,0.000000,7.850000>}
object{TOOLS_PCB_SMD(1.300000,1.600000,0.037000,0) rotate<0,-90.000000,0> texture{col_pds} translate<6.600000,0.000000,9.750000>}
object{TOOLS_PCB_SMD(1.300000,1.600000,0.037000,0) rotate<0,-180.000000,0> texture{col_pds} translate<17.450000,-1.537000,10.300000>}
object{TOOLS_PCB_SMD(1.300000,1.600000,0.037000,0) rotate<0,-180.000000,0> texture{col_pds} translate<15.550000,-1.537000,10.300000>}
object{TOOLS_PCB_SMD(1.800000,3.200000,0.037000,0) rotate<0,-0.000000,0> texture{col_pds} translate<36.400000,-1.537000,22.200000>}
object{TOOLS_PCB_SMD(1.800000,3.200000,0.037000,0) rotate<0,-0.000000,0> texture{col_pds} translate<42.000000,-1.537000,22.200000>}
object{TOOLS_PCB_SMD(1.300000,1.600000,0.037000,0) rotate<0,-0.000000,0> texture{col_pds} translate<16.150000,-1.537000,17.000000>}
object{TOOLS_PCB_SMD(1.300000,1.600000,0.037000,0) rotate<0,-0.000000,0> texture{col_pds} translate<18.050000,-1.537000,17.000000>}
object{TOOLS_PCB_SMD(1.300000,1.600000,0.037000,0) rotate<0,-90.000000,0> texture{col_pds} translate<11.200000,-1.537000,3.450000>}
object{TOOLS_PCB_SMD(1.300000,1.600000,0.037000,0) rotate<0,-90.000000,0> texture{col_pds} translate<11.200000,-1.537000,5.350000>}
object{TOOLS_PCB_SMD(1.300000,1.600000,0.037000,0) rotate<0,-90.000000,0> texture{col_pds} translate<19.800000,-1.537000,25.850000>}
object{TOOLS_PCB_SMD(1.300000,1.600000,0.037000,0) rotate<0,-90.000000,0> texture{col_pds} translate<19.800000,-1.537000,27.750000>}
object{TOOLS_PCB_SMD(1.300000,1.600000,0.037000,0) rotate<0,-90.000000,0> texture{col_pds} translate<32.000000,-1.537000,25.850000>}
object{TOOLS_PCB_SMD(1.300000,1.600000,0.037000,0) rotate<0,-90.000000,0> texture{col_pds} translate<32.000000,-1.537000,27.750000>}
object{TOOLS_PCB_SMD(1.300000,1.600000,0.037000,0) rotate<0,-0.000000,0> texture{col_pds} translate<15.850000,-1.537000,13.600000>}
object{TOOLS_PCB_SMD(1.300000,1.600000,0.037000,0) rotate<0,-0.000000,0> texture{col_pds} translate<17.750000,-1.537000,13.600000>}
object{TOOLS_PCB_SMD(1.300000,1.600000,0.037000,0) rotate<0,-180.000000,0> texture{col_pds} translate<6.350000,0.000000,12.800000>}
object{TOOLS_PCB_SMD(1.300000,1.600000,0.037000,0) rotate<0,-180.000000,0> texture{col_pds} translate<4.450000,0.000000,12.800000>}
#ifndef(global_pack_S1) #local global_tmp=0; #else #local global_tmp=100; #end object{TOOLS_PCB_VIA(1.524000,1.016000,1,16,3+global_tmp,100) rotate<0,-0.000000,0>translate<44.948800,0,28.660600> texture{col_thl}}
#ifndef(global_pack_S1) #local global_tmp=0; #else #local global_tmp=100; #end object{TOOLS_PCB_VIA(1.524000,1.016000,1,16,3+global_tmp,100) rotate<0,-0.000000,0>translate<51.451200,0,28.660600> texture{col_thl}}
#ifndef(global_pack_S1) #local global_tmp=0; #else #local global_tmp=100; #end object{TOOLS_PCB_VIA(1.524000,1.016000,1,16,3+global_tmp,100) rotate<0,-0.000000,0>translate<44.948800,0,24.139400> texture{col_thl}}
#ifndef(global_pack_S1) #local global_tmp=0; #else #local global_tmp=100; #end object{TOOLS_PCB_VIA(1.524000,1.016000,1,16,3+global_tmp,100) rotate<0,-0.000000,0>translate<51.451200,0,24.139400> texture{col_thl}}
#ifndef(global_pack_SV1) #local global_tmp=0; #else #local global_tmp=100; #end object{TOOLS_PCB_VIA(1.524000,1.016000,1,16,2+global_tmp,0) rotate<0,-90.000000,0>translate<9.730000,0,11.190000> texture{col_thl}}
#ifndef(global_pack_SV1) #local global_tmp=0; #else #local global_tmp=100; #end object{TOOLS_PCB_VIA(1.524000,1.016000,1,16,2+global_tmp,0) rotate<0,-90.000000,0>translate<12.270000,0,11.190000> texture{col_thl}}
#ifndef(global_pack_SV1) #local global_tmp=0; #else #local global_tmp=100; #end object{TOOLS_PCB_VIA(1.524000,1.016000,1,16,2+global_tmp,0) rotate<0,-90.000000,0>translate<9.730000,0,13.730000> texture{col_thl}}
#ifndef(global_pack_SV1) #local global_tmp=0; #else #local global_tmp=100; #end object{TOOLS_PCB_VIA(1.524000,1.016000,1,16,2+global_tmp,0) rotate<0,-90.000000,0>translate<12.270000,0,13.730000> texture{col_thl}}
#ifndef(global_pack_SV1) #local global_tmp=0; #else #local global_tmp=100; #end object{TOOLS_PCB_VIA(1.524000,1.016000,1,16,2+global_tmp,0) rotate<0,-90.000000,0>translate<9.730000,0,16.270000> texture{col_thl}}
#ifndef(global_pack_SV1) #local global_tmp=0; #else #local global_tmp=100; #end object{TOOLS_PCB_VIA(1.524000,1.016000,1,16,2+global_tmp,0) rotate<0,-90.000000,0>translate<12.270000,0,16.270000> texture{col_thl}}
#ifndef(global_pack_SV1) #local global_tmp=0; #else #local global_tmp=100; #end object{TOOLS_PCB_VIA(1.524000,1.016000,1,16,2+global_tmp,0) rotate<0,-90.000000,0>translate<9.730000,0,18.810000> texture{col_thl}}
#ifndef(global_pack_SV1) #local global_tmp=0; #else #local global_tmp=100; #end object{TOOLS_PCB_VIA(1.524000,1.016000,1,16,2+global_tmp,0) rotate<0,-90.000000,0>translate<12.270000,0,18.810000> texture{col_thl}}
object{TOOLS_PCB_SMD(1.000000,1.400000,0.037000,0) rotate<0,-0.000000,0> texture{col_pds} translate<-11.750000,-1.537000,22.000000>}
object{TOOLS_PCB_SMD(1.000000,1.400000,0.037000,0) rotate<0,-0.000000,0> texture{col_pds} translate<-10.800000,-1.537000,19.800000>}
object{TOOLS_PCB_SMD(1.000000,1.400000,0.037000,0) rotate<0,-0.000000,0> texture{col_pds} translate<-9.850000,-1.537000,22.000000>}
object{TOOLS_PCB_SMD(0.600000,2.200000,0.037000,0) rotate<0,-270.000000,0> texture{col_pds} translate<-12.200000,0.000000,19.305000>}
object{TOOLS_PCB_SMD(0.600000,2.200000,0.037000,0) rotate<0,-270.000000,0> texture{col_pds} translate<-12.200000,0.000000,18.035000>}
object{TOOLS_PCB_SMD(0.600000,2.200000,0.037000,0) rotate<0,-270.000000,0> texture{col_pds} translate<-12.200000,0.000000,16.765000>}
object{TOOLS_PCB_SMD(0.600000,2.200000,0.037000,0) rotate<0,-270.000000,0> texture{col_pds} translate<-12.200000,0.000000,15.495000>}
object{TOOLS_PCB_SMD(0.600000,2.200000,0.037000,0) rotate<0,-270.000000,0> texture{col_pds} translate<-7.000000,0.000000,15.495000>}
object{TOOLS_PCB_SMD(0.600000,2.200000,0.037000,0) rotate<0,-270.000000,0> texture{col_pds} translate<-7.000000,0.000000,16.765000>}
object{TOOLS_PCB_SMD(0.600000,2.200000,0.037000,0) rotate<0,-270.000000,0> texture{col_pds} translate<-7.000000,0.000000,18.035000>}
object{TOOLS_PCB_SMD(0.600000,2.200000,0.037000,0) rotate<0,-270.000000,0> texture{col_pds} translate<-7.000000,0.000000,19.305000>}
#ifndef(global_pack_X1) #local global_tmp=0; #else #local global_tmp=100; #end object{TOOLS_PCB_VIA(2.550000,1.700000,1,16,3+global_tmp,100) rotate<0,-0.000000,0>translate<14.600000,0,27.780000> texture{col_thl}}
#ifndef(global_pack_X1) #local global_tmp=0; #else #local global_tmp=100; #end object{TOOLS_PCB_VIA(2.550000,1.700000,1,16,3+global_tmp,100) rotate<0,-0.000000,0>translate<14.600000,0,23.820000> texture{col_thl}}
#ifndef(global_pack_X2) #local global_tmp=0; #else #local global_tmp=100; #end object{TOOLS_PCB_VIA(2.550000,1.700000,1,16,3+global_tmp,100) rotate<0,-180.000000,0>translate<57.000000,0,11.060000> texture{col_thl}}
#ifndef(global_pack_X2) #local global_tmp=0; #else #local global_tmp=100; #end object{TOOLS_PCB_VIA(2.550000,1.700000,1,16,3+global_tmp,100) rotate<0,-180.000000,0>translate<57.000000,0,15.020000> texture{col_thl}}
#ifndef(global_pack_X2) #local global_tmp=0; #else #local global_tmp=100; #end object{TOOLS_PCB_VIA(2.550000,1.700000,1,16,3+global_tmp,100) rotate<0,-180.000000,0>translate<57.000000,0,18.980000> texture{col_thl}}
#ifndef(global_pack_X2) #local global_tmp=0; #else #local global_tmp=100; #end object{TOOLS_PCB_VIA(2.550000,1.700000,1,16,3+global_tmp,100) rotate<0,-180.000000,0>translate<57.000000,0,22.940000> texture{col_thl}}
//Pads/Vias
object{TOOLS_PCB_VIA(0.762000,0.254000,1,16,1,0) translate<-0.500000,0,18.600000> texture{col_thl}}
object{TOOLS_PCB_VIA(0.762000,0.254000,1,16,1,0) translate<-8.900000,0,24.300000> texture{col_thl}}
object{TOOLS_PCB_VIA(0.762000,0.254000,1,16,1,0) translate<8.500000,0,9.000000> texture{col_thl}}
object{TOOLS_PCB_VIA(0.762000,0.254000,1,16,1,0) translate<-10.400000,0,15.500000> texture{col_thl}}
object{TOOLS_PCB_VIA(0.762000,0.254000,1,16,1,0) translate<27.100000,0,16.100000> texture{col_thl}}
object{TOOLS_PCB_VIA(0.762000,0.254000,1,16,1,0) translate<14.500000,0,21.600000> texture{col_thl}}
object{TOOLS_PCB_VIA(0.762000,0.254000,1,16,1,0) translate<14.500000,0,18.000000> texture{col_thl}}
object{TOOLS_PCB_VIA(0.762000,0.254000,1,16,1,0) translate<42.900000,0,16.600000> texture{col_thl}}
object{TOOLS_PCB_VIA(1.016000,0.508000,1,16,1,0) translate<51.200000,0,7.400000> texture{col_thl}}
object{TOOLS_PCB_VIA(1.016000,0.508000,1,16,1,0) translate<19.100000,0,22.800000> texture{col_thl}}
object{TOOLS_PCB_VIA(1.016000,0.508000,1,16,1,0) translate<28.900000,0,22.900000> texture{col_thl}}
object{TOOLS_PCB_VIA(1.016000,0.508000,1,16,1,0) translate<33.400000,0,22.900000> texture{col_thl}}
object{TOOLS_PCB_VIA(0.762000,0.254000,1,16,1,0) translate<17.800000,0,21.700000> texture{col_thl}}
object{TOOLS_PCB_VIA(0.762000,0.254000,1,16,1,0) translate<17.800000,0,20.000000> texture{col_thl}}
object{TOOLS_PCB_VIA(0.762000,0.254000,1,16,1,0) translate<10.800000,0,6.700000> texture{col_thl}}
object{TOOLS_PCB_VIA(0.762000,0.254000,1,16,1,0) translate<10.700000,0,23.600000> texture{col_thl}}
object{TOOLS_PCB_VIA(0.762000,0.254000,1,16,1,0) translate<3.500000,0,19.700000> texture{col_thl}}
object{TOOLS_PCB_VIA(0.762000,0.254000,1,16,1,0) translate<19.800000,0,13.400000> texture{col_thl}}
object{TOOLS_PCB_VIA(0.762000,0.254000,1,16,1,0) translate<10.700000,0,21.100000> texture{col_thl}}
object{TOOLS_PCB_VIA(0.762000,0.254000,1,16,1,0) translate<-3.100000,0,15.400000> texture{col_thl}}
object{TOOLS_PCB_VIA(0.762000,0.254000,1,16,1,0) translate<-9.300000,0,15.400000> texture{col_thl}}
object{TOOLS_PCB_VIA(0.762000,0.254000,1,16,1,0) translate<-11.700000,0,23.400000> texture{col_thl}}
object{TOOLS_PCB_VIA(0.762000,0.254000,1,16,1,0) translate<-14.000000,0,18.100000> texture{col_thl}}
object{TOOLS_PCB_VIA(0.762000,0.254000,1,16,1,0) translate<-7.000000,0,21.300000> texture{col_thl}}
object{TOOLS_PCB_VIA(0.762000,0.254000,1,16,1,0) translate<3.100000,0,13.000000> texture{col_thl}}
object{TOOLS_PCB_VIA(0.762000,0.254000,1,16,1,0) translate<1.900000,0,22.600000> texture{col_thl}}
object{TOOLS_PCB_VIA(0.762000,0.254000,1,16,1,0) translate<-12.300000,0,20.300000> texture{col_thl}}
object{TOOLS_PCB_VIA(0.762000,0.254000,1,16,1,0) translate<-8.000000,0,20.500000> texture{col_thl}}
object{TOOLS_PCB_VIA(0.762000,0.254000,1,16,1,0) translate<-3.900000,0,16.400000> texture{col_thl}}
object{TOOLS_PCB_VIA(0.762000,0.254000,1,16,1,0) translate<-5.200000,0,18.600000> texture{col_thl}}
object{TOOLS_PCB_VIA(0.762000,0.254000,1,16,1,0) translate<4.100000,0,6.500000> texture{col_thl}}
object{TOOLS_PCB_VIA(0.762000,0.254000,1,16,1,0) translate<5.900000,0,6.500000> texture{col_thl}}
object{TOOLS_PCB_VIA(0.762000,0.254000,1,16,1,0) translate<32.100000,0,20.400000> texture{col_thl}}
object{TOOLS_PCB_VIA(0.762000,0.254000,1,16,1,0) translate<32.200000,0,29.100000> texture{col_thl}}
object{TOOLS_PCB_VIA(0.762000,0.254000,1,16,1,0) translate<25.100000,0,5.100000> texture{col_thl}}
object{TOOLS_PCB_VIA(0.762000,0.254000,1,16,1,0) translate<19.400000,0,9.800000> texture{col_thl}}
object{TOOLS_PCB_VIA(1.016000,0.508000,1,16,1,0) translate<35.000000,0,13.000000> texture{col_thl}}
object{TOOLS_PCB_VIA(1.016000,0.508000,1,16,1,0) translate<42.900000,0,7.100000> texture{col_thl}}
object{TOOLS_PCB_VIA(1.016000,0.508000,1,16,1,0) translate<39.000000,0,21.100000> texture{col_thl}}
object{TOOLS_PCB_VIA(1.016000,0.508000,1,16,1,0) translate<46.600000,0,10.400000> texture{col_thl}}
object{TOOLS_PCB_VIA(1.016000,0.508000,1,16,1,0) translate<52.200000,0,10.400000> texture{col_thl}}
object{TOOLS_PCB_VIA(1.016000,0.508000,1,16,1,0) translate<51.700000,0,21.300000> texture{col_thl}}
object{TOOLS_PCB_VIA(0.762000,0.254000,1,16,1,0) translate<-14.000000,0,16.800000> texture{col_thl}}
object{TOOLS_PCB_VIA(0.762000,0.254000,1,16,1,0) translate<-9.300000,0,16.800000> texture{col_thl}}
object{TOOLS_PCB_VIA(0.762000,0.254000,1,16,1,0) translate<7.400000,0,22.200000> texture{col_thl}}
object{TOOLS_PCB_VIA(0.762000,0.254000,1,16,1,0) translate<5.000000,0,3.500000> texture{col_thl}}
object{TOOLS_PCB_VIA(1.016000,0.508000,1,16,1,0) translate<44.600000,0,9.200000> texture{col_thl}}
object{TOOLS_PCB_VIA(1.016000,0.508000,1,16,1,0) translate<50.300000,0,15.000000> texture{col_thl}}
#end
#if(pcb_wires=on)
union{
//Signals
cylinder{<0,0,0><0,0.035000,0>0.254000 translate<-17.200000,0.000000,18.000000>}
cylinder{<0,0,0><0,0.035000,0>0.254000 translate<-17.200000,0.000000,9.700000>}
box{<0,0,-0.254000><8.300000,0.035000,0.254000> rotate<0,-90.000000,0> translate<-17.200000,0.000000,9.700000> }
cylinder{<0,0,0><0,0.035000,0>0.254000 translate<-17.200000,0.000000,18.000000>}
cylinder{<0,0,0><0,0.035000,0>0.254000 translate<-16.300000,0.000000,18.000000>}
box{<0,0,-0.254000><0.900000,0.035000,0.254000> rotate<0,0.000000,0> translate<-17.200000,0.000000,18.000000> }
cylinder{<0,0,0><0,0.035000,0>0.254000 translate<-17.200000,0.000000,9.700000>}
cylinder{<0,0,0><0,0.035000,0>0.254000 translate<-15.800000,0.000000,9.700000>}
box{<0,0,-0.254000><1.400000,0.035000,0.254000> rotate<0,0.000000,0> translate<-17.200000,0.000000,9.700000> }
cylinder{<0,0,0><0,0.035000,0>0.254000 translate<-16.300000,0.000000,18.000000>}
cylinder{<0,0,0><0,0.035000,0>0.254000 translate<-15.600000,0.000000,17.950000>}
box{<0,0,-0.254000><0.701783,0.035000,0.254000> rotate<0,4.085347,0> translate<-16.300000,0.000000,18.000000> }
cylinder{<0,0,0><0,0.035000,0>0.254000 translate<-15.600000,0.000000,17.950000>}
cylinder{<0,0,0><0,0.035000,0>0.254000 translate<-15.600000,0.000000,21.550000>}
box{<0,0,-0.254000><3.600000,0.035000,0.254000> rotate<0,90.000000,0> translate<-15.600000,0.000000,21.550000> }
cylinder{<0,0,0><0,0.035000,0>0.254000 translate<-15.600000,0.000000,21.550000>}
cylinder{<0,0,0><0,0.035000,0>0.254000 translate<-15.350000,0.000000,22.100000>}
box{<0,0,-0.254000><0.604152,0.035000,0.254000> rotate<0,-65.551719,0> translate<-15.600000,0.000000,21.550000> }
cylinder{<0,0,0><0,0.035000,0>0.254000 translate<-15.600000,0.000000,16.050000>}
cylinder{<0,0,0><0,0.035000,0>0.254000 translate<-14.900000,0.000000,16.100000>}
box{<0,0,-0.254000><0.701783,0.035000,0.254000> rotate<0,-4.085347,0> translate<-15.600000,0.000000,16.050000> }
cylinder{<0,0,0><0,0.035000,0>0.254000 translate<-15.600000,0.000000,17.950000>}
cylinder{<0,0,0><0,0.035000,0>0.254000 translate<-14.900000,0.000000,18.500000>}
box{<0,0,-0.254000><0.890225,0.035000,0.254000> rotate<0,-38.154708,0> translate<-15.600000,0.000000,17.950000> }
cylinder{<0,0,0><0,0.035000,0>0.254000 translate<-14.900000,0.000000,18.500000>}
cylinder{<0,0,0><0,0.035000,0>0.254000 translate<-14.800000,0.000000,18.600000>}
box{<0,0,-0.254000><0.141421,0.035000,0.254000> rotate<0,-44.997030,0> translate<-14.900000,0.000000,18.500000> }
cylinder{<0,0,0><0,0.035000,0>0.254000 translate<-14.800000,0.000000,18.600000>}
cylinder{<0,0,0><0,0.035000,0>0.254000 translate<-14.100000,0.000000,19.300000>}
box{<0,0,-0.254000><0.989949,0.035000,0.254000> rotate<0,-44.997030,0> translate<-14.800000,0.000000,18.600000> }
cylinder{<0,0,0><0,0.035000,0>0.254000 translate<-14.900000,0.000000,16.100000>}
cylinder{<0,0,0><0,0.035000,0>0.254000 translate<-14.000000,0.000000,16.100000>}
box{<0,0,-0.254000><0.900000,0.035000,0.254000> rotate<0,0.000000,0> translate<-14.900000,0.000000,16.100000> }
cylinder{<0,0,0><0,0.035000,0>0.254000 translate<-14.000000,0.000000,16.100000>}
cylinder{<0,0,0><0,0.035000,0>0.254000 translate<-14.000000,0.000000,16.800000>}
box{<0,0,-0.254000><0.700000,0.035000,0.254000> rotate<0,90.000000,0> translate<-14.000000,0.000000,16.800000> }
cylinder{<0,0,0><0,0.035000,0>0.127000 translate<-14.000000,0.000000,18.100000>}
cylinder{<0,0,0><0,0.035000,0>0.127000 translate<-14.000000,0.000000,18.000000>}
box{<0,0,-0.127000><0.100000,0.035000,0.127000> rotate<0,-90.000000,0> translate<-14.000000,0.000000,18.000000> }
cylinder{<0,0,0><0,0.035000,0>0.254000 translate<-15.800000,0.000000,9.700000>}
cylinder{<0,0,0><0,0.035000,0>0.254000 translate<-13.975000,0.000000,7.000000>}
box{<0,0,-0.254000><3.258930,0.035000,0.254000> rotate<0,55.940528,0> translate<-15.800000,0.000000,9.700000> }
cylinder{<0,0,0><0,0.035000,0>0.127000 translate<-14.000000,-1.535000,18.100000>}
cylinder{<0,0,0><0,0.035000,0>0.127000 translate<-13.900000,-1.535000,18.200000>}
box{<0,0,-0.127000><0.141421,0.035000,0.127000> rotate<0,-44.997030,0> translate<-14.000000,-1.535000,18.100000> }
cylinder{<0,0,0><0,0.035000,0>0.127000 translate<-13.900000,-1.535000,18.200000>}
cylinder{<0,0,0><0,0.035000,0>0.127000 translate<-13.700000,-1.535000,18.200000>}
box{<0,0,-0.127000><0.200000,0.035000,0.127000> rotate<0,0.000000,0> translate<-13.900000,-1.535000,18.200000> }
cylinder{<0,0,0><0,0.035000,0>0.127000 translate<-13.700000,-1.535000,18.200000>}
cylinder{<0,0,0><0,0.035000,0>0.127000 translate<-13.600000,-1.535000,18.300000>}
box{<0,0,-0.127000><0.141421,0.035000,0.127000> rotate<0,-44.997030,0> translate<-13.700000,-1.535000,18.200000> }
cylinder{<0,0,0><0,0.035000,0>0.127000 translate<-13.600000,-1.535000,18.300000>}
cylinder{<0,0,0><0,0.035000,0>0.127000 translate<-13.600000,-1.535000,22.400000>}
box{<0,0,-0.127000><4.100000,0.035000,0.127000> rotate<0,90.000000,0> translate<-13.600000,-1.535000,22.400000> }
cylinder{<0,0,0><0,0.035000,0>0.127000 translate<-13.600000,-1.535000,22.400000>}
cylinder{<0,0,0><0,0.035000,0>0.127000 translate<-13.500000,-1.535000,22.500000>}
box{<0,0,-0.127000><0.141421,0.035000,0.127000> rotate<0,-44.997030,0> translate<-13.600000,-1.535000,22.400000> }
cylinder{<0,0,0><0,0.035000,0>0.127000 translate<-14.000000,0.000000,18.000000>}
cylinder{<0,0,0><0,0.035000,0>0.127000 translate<-13.400000,0.000000,18.000000>}
box{<0,0,-0.127000><0.600000,0.035000,0.127000> rotate<0,0.000000,0> translate<-14.000000,0.000000,18.000000> }
cylinder{<0,0,0><0,0.035000,0>0.127000 translate<-13.500000,-1.535000,22.500000>}
cylinder{<0,0,0><0,0.035000,0>0.127000 translate<-13.400000,-1.535000,22.500000>}
box{<0,0,-0.127000><0.100000,0.035000,0.127000> rotate<0,0.000000,0> translate<-13.500000,-1.535000,22.500000> }
cylinder{<0,0,0><0,0.035000,0>0.127000 translate<-13.400000,0.000000,18.000000>}
cylinder{<0,0,0><0,0.035000,0>0.127000 translate<-13.300000,0.000000,18.100000>}
box{<0,0,-0.127000><0.141421,0.035000,0.127000> rotate<0,-44.997030,0> translate<-13.400000,0.000000,18.000000> }
cylinder{<0,0,0><0,0.035000,0>0.127000 translate<-13.400000,-1.535000,22.500000>}
cylinder{<0,0,0><0,0.035000,0>0.127000 translate<-13.300000,-1.535000,22.600000>}
box{<0,0,-0.127000><0.141421,0.035000,0.127000> rotate<0,-44.997030,0> translate<-13.400000,-1.535000,22.500000> }
cylinder{<0,0,0><0,0.035000,0>0.127000 translate<-13.300000,0.000000,18.100000>}
cylinder{<0,0,0><0,0.035000,0>0.127000 translate<-13.200000,0.000000,18.100000>}
box{<0,0,-0.127000><0.100000,0.035000,0.127000> rotate<0,0.000000,0> translate<-13.300000,0.000000,18.100000> }
cylinder{<0,0,0><0,0.035000,0>0.254000 translate<-14.100000,0.000000,19.300000>}
cylinder{<0,0,0><0,0.035000,0>0.254000 translate<-13.200000,0.000000,19.300000>}
box{<0,0,-0.254000><0.900000,0.035000,0.254000> rotate<0,0.000000,0> translate<-14.100000,0.000000,19.300000> }
cylinder{<0,0,0><0,0.035000,0>0.127000 translate<-13.100000,0.000000,22.050000>}
cylinder{<0,0,0><0,0.035000,0>0.127000 translate<-13.100000,0.000000,21.200000>}
box{<0,0,-0.127000><0.850000,0.035000,0.127000> rotate<0,-90.000000,0> translate<-13.100000,0.000000,21.200000> }
cylinder{<0,0,0><0,0.035000,0>0.127000 translate<-13.450000,0.000000,22.100000>}
cylinder{<0,0,0><0,0.035000,0>0.127000 translate<-13.100000,0.000000,22.050000>}
box{<0,0,-0.127000><0.353553,0.035000,0.127000> rotate<0,8.129566,0> translate<-13.450000,0.000000,22.100000> }
cylinder{<0,0,0><0,0.035000,0>0.127000 translate<-12.400000,-1.535000,20.200000>}
cylinder{<0,0,0><0,0.035000,0>0.127000 translate<-12.400000,-1.535000,19.700000>}
box{<0,0,-0.127000><0.500000,0.035000,0.127000> rotate<0,-90.000000,0> translate<-12.400000,-1.535000,19.700000> }
cylinder{<0,0,0><0,0.035000,0>0.127000 translate<-12.400000,-1.535000,20.200000>}
cylinder{<0,0,0><0,0.035000,0>0.127000 translate<-12.300000,-1.535000,20.300000>}
box{<0,0,-0.127000><0.141421,0.035000,0.127000> rotate<0,-44.997030,0> translate<-12.400000,-1.535000,20.200000> }
cylinder{<0,0,0><0,0.035000,0>0.127000 translate<-13.100000,0.000000,21.200000>}
cylinder{<0,0,0><0,0.035000,0>0.127000 translate<-12.300000,0.000000,20.400000>}
box{<0,0,-0.127000><1.131371,0.035000,0.127000> rotate<0,44.997030,0> translate<-13.100000,0.000000,21.200000> }
cylinder{<0,0,0><0,0.035000,0>0.127000 translate<-13.200000,0.000000,18.100000>}
cylinder{<0,0,0><0,0.035000,0>0.127000 translate<-12.200000,0.000000,18.035000>}
box{<0,0,-0.127000><1.002110,0.035000,0.127000> rotate<0,3.718749,0> translate<-13.200000,0.000000,18.100000> }
cylinder{<0,0,0><0,0.035000,0>0.254000 translate<-13.200000,0.000000,19.300000>}
cylinder{<0,0,0><0,0.035000,0>0.254000 translate<-12.200000,0.000000,19.305000>}
box{<0,0,-0.254000><1.000012,0.035000,0.254000> rotate<0,-0.286458,0> translate<-13.200000,0.000000,19.300000> }
cylinder{<0,0,0><0,0.035000,0>0.127000 translate<-12.300000,0.000000,20.300000>}
cylinder{<0,0,0><0,0.035000,0>0.127000 translate<-12.200000,0.000000,20.400000>}
box{<0,0,-0.127000><0.141421,0.035000,0.127000> rotate<0,-44.997030,0> translate<-12.300000,0.000000,20.300000> }
cylinder{<0,0,0><0,0.035000,0>0.127000 translate<-12.300000,0.000000,20.400000>}
cylinder{<0,0,0><0,0.035000,0>0.127000 translate<-12.200000,0.000000,20.400000>}
box{<0,0,-0.127000><0.100000,0.035000,0.127000> rotate<0,0.000000,0> translate<-12.300000,0.000000,20.400000> }
cylinder{<0,0,0><0,0.035000,0>0.127000 translate<-13.300000,-1.535000,22.600000>}
cylinder{<0,0,0><0,0.035000,0>0.127000 translate<-12.200000,-1.535000,22.600000>}
box{<0,0,-0.127000><1.100000,0.035000,0.127000> rotate<0,0.000000,0> translate<-13.300000,-1.535000,22.600000> }
cylinder{<0,0,0><0,0.035000,0>0.127000 translate<-12.200000,-1.535000,26.300000>}
cylinder{<0,0,0><0,0.035000,0>0.127000 translate<-12.200000,-1.535000,27.000000>}
box{<0,0,-0.127000><0.700000,0.035000,0.127000> rotate<0,90.000000,0> translate<-12.200000,-1.535000,27.000000> }
cylinder{<0,0,0><0,0.035000,0>0.127000 translate<-12.200000,-1.535000,27.000000>}
cylinder{<0,0,0><0,0.035000,0>0.127000 translate<-12.140000,-1.535000,27.060000>}
box{<0,0,-0.127000><0.084853,0.035000,0.127000> rotate<0,-44.997030,0> translate<-12.200000,-1.535000,27.000000> }
cylinder{<0,0,0><0,0.035000,0>0.127000 translate<-12.200000,-1.535000,22.600000>}
cylinder{<0,0,0><0,0.035000,0>0.127000 translate<-11.750000,-1.535000,22.000000>}
box{<0,0,-0.127000><0.750000,0.035000,0.127000> rotate<0,53.126596,0> translate<-12.200000,-1.535000,22.600000> }
cylinder{<0,0,0><0,0.035000,0>0.127000 translate<-11.700000,0.000000,23.400000>}
cylinder{<0,0,0><0,0.035000,0>0.127000 translate<-11.700000,0.000000,23.000000>}
box{<0,0,-0.127000><0.400000,0.035000,0.127000> rotate<0,-90.000000,0> translate<-11.700000,0.000000,23.000000> }
cylinder{<0,0,0><0,0.035000,0>0.127000 translate<-11.750000,-1.535000,22.000000>}
cylinder{<0,0,0><0,0.035000,0>0.127000 translate<-11.700000,-1.535000,23.200000>}
box{<0,0,-0.127000><1.201041,0.035000,0.127000> rotate<0,-87.608274,0> translate<-11.750000,-1.535000,22.000000> }
cylinder{<0,0,0><0,0.035000,0>0.127000 translate<-11.700000,-1.535000,23.200000>}
cylinder{<0,0,0><0,0.035000,0>0.127000 translate<-11.700000,-1.535000,23.400000>}
box{<0,0,-0.127000><0.200000,0.035000,0.127000> rotate<0,90.000000,0> translate<-11.700000,-1.535000,23.400000> }
cylinder{<0,0,0><0,0.035000,0>0.127000 translate<-12.400000,-1.535000,19.700000>}
cylinder{<0,0,0><0,0.035000,0>0.127000 translate<-11.400000,-1.535000,18.700000>}
box{<0,0,-0.127000><1.414214,0.035000,0.127000> rotate<0,44.997030,0> translate<-12.400000,-1.535000,19.700000> }
cylinder{<0,0,0><0,0.035000,0>0.127000 translate<-12.200000,0.000000,15.495000>}
cylinder{<0,0,0><0,0.035000,0>0.127000 translate<-11.200000,0.000000,15.500000>}
box{<0,0,-0.127000><1.000012,0.035000,0.127000> rotate<0,-0.286458,0> translate<-12.200000,0.000000,15.495000> }
cylinder{<0,0,0><0,0.035000,0>0.127000 translate<-12.200000,0.000000,16.765000>}
cylinder{<0,0,0><0,0.035000,0>0.127000 translate<-11.200000,0.000000,16.800000>}
box{<0,0,-0.127000><1.000612,0.035000,0.127000> rotate<0,-2.004402,0> translate<-12.200000,0.000000,16.765000> }
cylinder{<0,0,0><0,0.035000,0>0.254000 translate<-12.200000,0.000000,19.305000>}
cylinder{<0,0,0><0,0.035000,0>0.254000 translate<-11.200000,0.000000,19.300000>}
box{<0,0,-0.254000><1.000012,0.035000,0.254000> rotate<0,0.286458,0> translate<-12.200000,0.000000,19.305000> }
cylinder{<0,0,0><0,0.035000,0>0.127000 translate<-11.200000,0.000000,15.500000>}
cylinder{<0,0,0><0,0.035000,0>0.127000 translate<-10.400000,0.000000,15.500000>}
box{<0,0,-0.127000><0.800000,0.035000,0.127000> rotate<0,0.000000,0> translate<-11.200000,0.000000,15.500000> }
cylinder{<0,0,0><0,0.035000,0>0.127000 translate<-11.200000,0.000000,16.800000>}
cylinder{<0,0,0><0,0.035000,0>0.127000 translate<-10.200000,0.000000,16.800000>}
box{<0,0,-0.127000><1.000000,0.035000,0.127000> rotate<0,0.000000,0> translate<-11.200000,0.000000,16.800000> }
cylinder{<0,0,0><0,0.035000,0>0.127000 translate<-11.700000,0.000000,23.000000>}
cylinder{<0,0,0><0,0.035000,0>0.127000 translate<-10.200000,0.000000,21.500000>}
box{<0,0,-0.127000><2.121320,0.035000,0.127000> rotate<0,44.997030,0> translate<-11.700000,0.000000,23.000000> }
cylinder{<0,0,0><0,0.035000,0>0.127000 translate<-10.100000,0.000000,16.700000>}
cylinder{<0,0,0><0,0.035000,0>0.127000 translate<-10.100000,0.000000,16.400000>}
box{<0,0,-0.127000><0.300000,0.035000,0.127000> rotate<0,-90.000000,0> translate<-10.100000,0.000000,16.400000> }
cylinder{<0,0,0><0,0.035000,0>0.127000 translate<-10.200000,0.000000,16.800000>}
cylinder{<0,0,0><0,0.035000,0>0.127000 translate<-10.100000,0.000000,16.700000>}
box{<0,0,-0.127000><0.141421,0.035000,0.127000> rotate<0,44.997030,0> translate<-10.200000,0.000000,16.800000> }
cylinder{<0,0,0><0,0.035000,0>0.127000 translate<-10.200000,0.000000,21.500000>}
cylinder{<0,0,0><0,0.035000,0>0.127000 translate<-10.100000,0.000000,21.400000>}
box{<0,0,-0.127000><0.141421,0.035000,0.127000> rotate<0,44.997030,0> translate<-10.200000,0.000000,21.500000> }
cylinder{<0,0,0><0,0.035000,0>0.127000 translate<-10.000000,0.000000,16.300000>}
cylinder{<0,0,0><0,0.035000,0>0.127000 translate<-10.000000,0.000000,16.200000>}
box{<0,0,-0.127000><0.100000,0.035000,0.127000> rotate<0,-90.000000,0> translate<-10.000000,0.000000,16.200000> }
cylinder{<0,0,0><0,0.035000,0>0.127000 translate<-10.100000,0.000000,16.400000>}
cylinder{<0,0,0><0,0.035000,0>0.127000 translate<-10.000000,0.000000,16.300000>}
box{<0,0,-0.127000><0.141421,0.035000,0.127000> rotate<0,44.997030,0> translate<-10.100000,0.000000,16.400000> }
cylinder{<0,0,0><0,0.035000,0>0.127000 translate<-10.000000,0.000000,16.200000>}
cylinder{<0,0,0><0,0.035000,0>0.127000 translate<-9.900000,0.000000,16.200000>}
box{<0,0,-0.127000><0.100000,0.035000,0.127000> rotate<0,0.000000,0> translate<-10.000000,0.000000,16.200000> }
cylinder{<0,0,0><0,0.035000,0>0.127000 translate<-9.800000,0.000000,16.100000>}
cylinder{<0,0,0><0,0.035000,0>0.127000 translate<-9.800000,0.000000,16.000000>}
box{<0,0,-0.127000><0.100000,0.035000,0.127000> rotate<0,-90.000000,0> translate<-9.800000,0.000000,16.000000> }
cylinder{<0,0,0><0,0.035000,0>0.127000 translate<-9.900000,0.000000,16.200000>}
cylinder{<0,0,0><0,0.035000,0>0.127000 translate<-9.800000,0.000000,16.100000>}
box{<0,0,-0.127000><0.141421,0.035000,0.127000> rotate<0,44.997030,0> translate<-9.900000,0.000000,16.200000> }
cylinder{<0,0,0><0,0.035000,0>0.127000 translate<-11.400000,-1.535000,18.700000>}
cylinder{<0,0,0><0,0.035000,0>0.127000 translate<-9.800000,-1.535000,18.700000>}
box{<0,0,-0.127000><1.600000,0.035000,0.127000> rotate<0,0.000000,0> translate<-11.400000,-1.535000,18.700000> }
cylinder{<0,0,0><0,0.035000,0>0.127000 translate<-9.850000,-1.535000,22.000000>}
cylinder{<0,0,0><0,0.035000,0>0.127000 translate<-9.800000,-1.535000,23.500000>}
box{<0,0,-0.127000><1.500833,0.035000,0.127000> rotate<0,-88.085034,0> translate<-9.850000,-1.535000,22.000000> }
cylinder{<0,0,0><0,0.035000,0>0.127000 translate<-12.200000,-1.535000,26.300000>}
cylinder{<0,0,0><0,0.035000,0>0.127000 translate<-9.800000,-1.535000,23.900000>}
box{<0,0,-0.127000><3.394113,0.035000,0.127000> rotate<0,44.997030,0> translate<-12.200000,-1.535000,26.300000> }
cylinder{<0,0,0><0,0.035000,0>0.127000 translate<-9.800000,-1.535000,23.500000>}
cylinder{<0,0,0><0,0.035000,0>0.127000 translate<-9.800000,-1.535000,23.900000>}
box{<0,0,-0.127000><0.400000,0.035000,0.127000> rotate<0,90.000000,0> translate<-9.800000,-1.535000,23.900000> }
cylinder{<0,0,0><0,0.035000,0>0.127000 translate<-9.800000,0.000000,16.000000>}
cylinder{<0,0,0><0,0.035000,0>0.127000 translate<-9.700000,0.000000,16.000000>}
box{<0,0,-0.127000><0.100000,0.035000,0.127000> rotate<0,0.000000,0> translate<-9.800000,0.000000,16.000000> }
cylinder{<0,0,0><0,0.035000,0>0.254000 translate<-11.200000,0.000000,19.300000>}
cylinder{<0,0,0><0,0.035000,0>0.254000 translate<-9.700000,0.000000,19.300000>}
box{<0,0,-0.254000><1.500000,0.035000,0.254000> rotate<0,0.000000,0> translate<-11.200000,0.000000,19.300000> }
cylinder{<0,0,0><0,0.035000,0>0.127000 translate<-9.600000,0.000000,15.900000>}
cylinder{<0,0,0><0,0.035000,0>0.127000 translate<-9.600000,0.000000,15.400000>}
box{<0,0,-0.127000><0.500000,0.035000,0.127000> rotate<0,-90.000000,0> translate<-9.600000,0.000000,15.400000> }
cylinder{<0,0,0><0,0.035000,0>0.127000 translate<-9.700000,0.000000,16.000000>}
cylinder{<0,0,0><0,0.035000,0>0.127000 translate<-9.600000,0.000000,15.900000>}
box{<0,0,-0.127000><0.141421,0.035000,0.127000> rotate<0,44.997030,0> translate<-9.700000,0.000000,16.000000> }
cylinder{<0,0,0><0,0.035000,0>0.254000 translate<-9.600000,0.000000,19.200000>}
cylinder{<0,0,0><0,0.035000,0>0.254000 translate<-9.600000,0.000000,18.000000>}
box{<0,0,-0.254000><1.200000,0.035000,0.254000> rotate<0,-90.000000,0> translate<-9.600000,0.000000,18.000000> }
cylinder{<0,0,0><0,0.035000,0>0.254000 translate<-9.700000,0.000000,19.300000>}
cylinder{<0,0,0><0,0.035000,0>0.254000 translate<-9.600000,0.000000,19.200000>}
box{<0,0,-0.254000><0.141421,0.035000,0.254000> rotate<0,44.997030,0> translate<-9.700000,0.000000,19.300000> }
cylinder{<0,0,0><0,0.035000,0>0.254000 translate<-9.600000,0.000000,26.500000>}
cylinder{<0,0,0><0,0.035000,0>0.254000 translate<-9.600000,0.000000,27.060000>}
box{<0,0,-0.254000><0.560000,0.035000,0.254000> rotate<0,90.000000,0> translate<-9.600000,0.000000,27.060000> }
cylinder{<0,0,0><0,0.035000,0>0.127000 translate<-9.850000,-1.535000,22.000000>}
cylinder{<0,0,0><0,0.035000,0>0.127000 translate<-9.400000,-1.535000,23.200000>}
box{<0,0,-0.127000><1.281601,0.035000,0.127000> rotate<0,-69.439372,0> translate<-9.850000,-1.535000,22.000000> }
cylinder{<0,0,0><0,0.035000,0>0.127000 translate<-9.600000,0.000000,15.400000>}
cylinder{<0,0,0><0,0.035000,0>0.127000 translate<-9.300000,0.000000,15.400000>}
box{<0,0,-0.127000><0.300000,0.035000,0.127000> rotate<0,0.000000,0> translate<-9.600000,0.000000,15.400000> }
cylinder{<0,0,0><0,0.035000,0>0.254000 translate<-14.000000,-1.535000,16.800000>}
cylinder{<0,0,0><0,0.035000,0>0.254000 translate<-9.300000,-1.535000,16.800000>}
box{<0,0,-0.254000><4.700000,0.035000,0.254000> rotate<0,0.000000,0> translate<-14.000000,-1.535000,16.800000> }
cylinder{<0,0,0><0,0.035000,0>0.254000 translate<-9.300000,-1.535000,16.800000>}
cylinder{<0,0,0><0,0.035000,0>0.254000 translate<-9.100000,-1.535000,17.100000>}
box{<0,0,-0.254000><0.360555,0.035000,0.254000> rotate<0,-56.306216,0> translate<-9.300000,-1.535000,16.800000> }
cylinder{<0,0,0><0,0.035000,0>0.254000 translate<-9.100000,-1.535000,17.100000>}
cylinder{<0,0,0><0,0.035000,0>0.254000 translate<-9.000000,-1.535000,17.200000>}
box{<0,0,-0.254000><0.141421,0.035000,0.254000> rotate<0,-44.997030,0> translate<-9.100000,-1.535000,17.100000> }
cylinder{<0,0,0><0,0.035000,0>0.127000 translate<-8.950000,0.000000,22.800000>}
cylinder{<0,0,0><0,0.035000,0>0.127000 translate<-8.900000,0.000000,23.500000>}
box{<0,0,-0.127000><0.701783,0.035000,0.127000> rotate<0,-85.908713,0> translate<-8.950000,0.000000,22.800000> }
cylinder{<0,0,0><0,0.035000,0>0.127000 translate<-8.900000,0.000000,23.500000>}
cylinder{<0,0,0><0,0.035000,0>0.127000 translate<-8.900000,0.000000,24.300000>}
box{<0,0,-0.127000><0.800000,0.035000,0.127000> rotate<0,90.000000,0> translate<-8.900000,0.000000,24.300000> }
cylinder{<0,0,0><0,0.035000,0>0.254000 translate<-9.000000,-1.535000,17.200000>}
cylinder{<0,0,0><0,0.035000,0>0.254000 translate<-8.800000,-1.535000,17.200000>}
box{<0,0,-0.254000><0.200000,0.035000,0.254000> rotate<0,0.000000,0> translate<-9.000000,-1.535000,17.200000> }
cylinder{<0,0,0><0,0.035000,0>0.254000 translate<-8.800000,-1.535000,17.200000>}
cylinder{<0,0,0><0,0.035000,0>0.254000 translate<-8.700000,-1.535000,17.300000>}
box{<0,0,-0.254000><0.141421,0.035000,0.254000> rotate<0,-44.997030,0> translate<-8.800000,-1.535000,17.200000> }
cylinder{<0,0,0><0,0.035000,0>0.254000 translate<-8.700000,-1.535000,17.300000>}
cylinder{<0,0,0><0,0.035000,0>0.254000 translate<-8.700000,-1.535000,18.800000>}
box{<0,0,-0.254000><1.500000,0.035000,0.254000> rotate<0,90.000000,0> translate<-8.700000,-1.535000,18.800000> }
cylinder{<0,0,0><0,0.035000,0>0.254000 translate<-8.700000,-1.535000,18.800000>}
cylinder{<0,0,0><0,0.035000,0>0.254000 translate<-8.600000,-1.535000,18.900000>}
box{<0,0,-0.254000><0.141421,0.035000,0.254000> rotate<0,-44.997030,0> translate<-8.700000,-1.535000,18.800000> }
cylinder{<0,0,0><0,0.035000,0>0.254000 translate<-8.600000,-1.535000,18.900000>}
cylinder{<0,0,0><0,0.035000,0>0.254000 translate<-8.500000,-1.535000,18.900000>}
box{<0,0,-0.254000><0.100000,0.035000,0.254000> rotate<0,0.000000,0> translate<-8.600000,-1.535000,18.900000> }
cylinder{<0,0,0><0,0.035000,0>0.254000 translate<-8.500000,-1.535000,18.900000>}
cylinder{<0,0,0><0,0.035000,0>0.254000 translate<-8.400000,-1.535000,19.000000>}
box{<0,0,-0.254000><0.141421,0.035000,0.254000> rotate<0,-44.997030,0> translate<-8.500000,-1.535000,18.900000> }
cylinder{<0,0,0><0,0.035000,0>0.127000 translate<-9.400000,-1.535000,23.200000>}
cylinder{<0,0,0><0,0.035000,0>0.127000 translate<-8.400000,-1.535000,23.200000>}
box{<0,0,-0.127000><1.000000,0.035000,0.127000> rotate<0,0.000000,0> translate<-9.400000,-1.535000,23.200000> }
cylinder{<0,0,0><0,0.035000,0>0.254000 translate<-8.400000,-1.535000,19.000000>}
cylinder{<0,0,0><0,0.035000,0>0.254000 translate<-8.300000,-1.535000,19.000000>}
box{<0,0,-0.254000><0.100000,0.035000,0.254000> rotate<0,0.000000,0> translate<-8.400000,-1.535000,19.000000> }
cylinder{<0,0,0><0,0.035000,0>0.127000 translate<-8.400000,-1.535000,23.200000>}
cylinder{<0,0,0><0,0.035000,0>0.127000 translate<-8.300000,-1.535000,23.300000>}
box{<0,0,-0.127000><0.141421,0.035000,0.127000> rotate<0,-44.997030,0> translate<-8.400000,-1.535000,23.200000> }
cylinder{<0,0,0><0,0.035000,0>0.127000 translate<-8.300000,-1.535000,23.700000>}
cylinder{<0,0,0><0,0.035000,0>0.127000 translate<-8.300000,-1.535000,23.300000>}
box{<0,0,-0.127000><0.400000,0.035000,0.127000> rotate<0,-90.000000,0> translate<-8.300000,-1.535000,23.300000> }
cylinder{<0,0,0><0,0.035000,0>0.254000 translate<-9.600000,0.000000,18.000000>}
cylinder{<0,0,0><0,0.035000,0>0.254000 translate<-8.200000,0.000000,18.000000>}
box{<0,0,-0.254000><1.400000,0.035000,0.254000> rotate<0,0.000000,0> translate<-9.600000,0.000000,18.000000> }
cylinder{<0,0,0><0,0.035000,0>0.254000 translate<-8.300000,-1.535000,19.000000>}
cylinder{<0,0,0><0,0.035000,0>0.254000 translate<-8.200000,-1.535000,19.100000>}
box{<0,0,-0.254000><0.141421,0.035000,0.254000> rotate<0,-44.997030,0> translate<-8.300000,-1.535000,19.000000> }
cylinder{<0,0,0><0,0.035000,0>0.254000 translate<-8.200000,0.000000,18.000000>}
cylinder{<0,0,0><0,0.035000,0>0.254000 translate<-8.100000,0.000000,18.100000>}
box{<0,0,-0.254000><0.141421,0.035000,0.254000> rotate<0,-44.997030,0> translate<-8.200000,0.000000,18.000000> }
cylinder{<0,0,0><0,0.035000,0>0.254000 translate<-8.200000,-1.535000,19.100000>}
cylinder{<0,0,0><0,0.035000,0>0.254000 translate<-8.100000,-1.535000,19.100000>}
box{<0,0,-0.254000><0.100000,0.035000,0.254000> rotate<0,0.000000,0> translate<-8.200000,-1.535000,19.100000> }
cylinder{<0,0,0><0,0.035000,0>0.254000 translate<-9.300000,0.000000,16.800000>}
cylinder{<0,0,0><0,0.035000,0>0.254000 translate<-8.000000,0.000000,16.800000>}
box{<0,0,-0.254000><1.300000,0.035000,0.254000> rotate<0,0.000000,0> translate<-9.300000,0.000000,16.800000> }
cylinder{<0,0,0><0,0.035000,0>0.254000 translate<-8.100000,0.000000,18.100000>}
cylinder{<0,0,0><0,0.035000,0>0.254000 translate<-8.000000,0.000000,18.100000>}
box{<0,0,-0.254000><0.100000,0.035000,0.254000> rotate<0,0.000000,0> translate<-8.100000,0.000000,18.100000> }
cylinder{<0,0,0><0,0.035000,0>0.254000 translate<-8.100000,-1.535000,19.100000>}
cylinder{<0,0,0><0,0.035000,0>0.254000 translate<-8.000000,-1.535000,19.200000>}
box{<0,0,-0.254000><0.141421,0.035000,0.254000> rotate<0,-44.997030,0> translate<-8.100000,-1.535000,19.100000> }
cylinder{<0,0,0><0,0.035000,0>0.127000 translate<-8.000000,0.000000,20.500000>}
cylinder{<0,0,0><0,0.035000,0>0.127000 translate<-8.000000,0.000000,19.600000>}
box{<0,0,-0.127000><0.900000,0.035000,0.127000> rotate<0,-90.000000,0> translate<-8.000000,0.000000,19.600000> }
cylinder{<0,0,0><0,0.035000,0>0.127000 translate<-9.800000,-1.535000,18.700000>}
cylinder{<0,0,0><0,0.035000,0>0.127000 translate<-8.000000,-1.535000,20.500000>}
box{<0,0,-0.127000><2.545584,0.035000,0.127000> rotate<0,-44.997030,0> translate<-9.800000,-1.535000,18.700000> }
cylinder{<0,0,0><0,0.035000,0>0.254000 translate<-8.000000,-1.535000,19.200000>}
cylinder{<0,0,0><0,0.035000,0>0.254000 translate<-7.900000,-1.535000,19.200000>}
box{<0,0,-0.254000><0.100000,0.035000,0.254000> rotate<0,0.000000,0> translate<-8.000000,-1.535000,19.200000> }
cylinder{<0,0,0><0,0.035000,0>0.254000 translate<-7.900000,-1.535000,19.200000>}
cylinder{<0,0,0><0,0.035000,0>0.254000 translate<-7.800000,-1.535000,19.300000>}
box{<0,0,-0.254000><0.141421,0.035000,0.254000> rotate<0,-44.997030,0> translate<-7.900000,-1.535000,19.200000> }
cylinder{<0,0,0><0,0.035000,0>0.127000 translate<-8.300000,-1.535000,23.700000>}
cylinder{<0,0,0><0,0.035000,0>0.127000 translate<-7.800000,-1.535000,24.200000>}
box{<0,0,-0.127000><0.707107,0.035000,0.127000> rotate<0,-44.997030,0> translate<-8.300000,-1.535000,23.700000> }
cylinder{<0,0,0><0,0.035000,0>0.254000 translate<-9.600000,0.000000,26.500000>}
cylinder{<0,0,0><0,0.035000,0>0.254000 translate<-7.750000,0.000000,24.650000>}
box{<0,0,-0.254000><2.616295,0.035000,0.254000> rotate<0,44.997030,0> translate<-9.600000,0.000000,26.500000> }
cylinder{<0,0,0><0,0.035000,0>0.254000 translate<-7.800000,-1.535000,19.300000>}
cylinder{<0,0,0><0,0.035000,0>0.254000 translate<-7.700000,-1.535000,19.300000>}
box{<0,0,-0.254000><0.100000,0.035000,0.254000> rotate<0,0.000000,0> translate<-7.800000,-1.535000,19.300000> }
cylinder{<0,0,0><0,0.035000,0>0.254000 translate<-7.750000,0.000000,24.650000>}
cylinder{<0,0,0><0,0.035000,0>0.254000 translate<-7.700000,0.000000,24.700000>}
box{<0,0,-0.254000><0.070711,0.035000,0.254000> rotate<0,-44.997030,0> translate<-7.750000,0.000000,24.650000> }
cylinder{<0,0,0><0,0.035000,0>0.254000 translate<-7.750000,0.000000,24.650000>}
cylinder{<0,0,0><0,0.035000,0>0.254000 translate<-7.650000,0.000000,24.550000>}
box{<0,0,-0.254000><0.141421,0.035000,0.254000> rotate<0,44.997030,0> translate<-7.750000,0.000000,24.650000> }
cylinder{<0,0,0><0,0.035000,0>0.254000 translate<-7.700000,-1.535000,19.300000>}
cylinder{<0,0,0><0,0.035000,0>0.254000 translate<-7.600000,-1.535000,19.400000>}
box{<0,0,-0.254000><0.141421,0.035000,0.254000> rotate<0,-44.997030,0> translate<-7.700000,-1.535000,19.300000> }
cylinder{<0,0,0><0,0.035000,0>0.127000 translate<-10.100000,0.000000,21.400000>}
cylinder{<0,0,0><0,0.035000,0>0.127000 translate<-7.600000,0.000000,21.400000>}
box{<0,0,-0.127000><2.500000,0.035000,0.127000> rotate<0,0.000000,0> translate<-10.100000,0.000000,21.400000> }
cylinder{<0,0,0><0,0.035000,0>0.127000 translate<-7.600000,0.000000,21.400000>}
cylinder{<0,0,0><0,0.035000,0>0.127000 translate<-7.600000,0.000000,22.100000>}
box{<0,0,-0.127000><0.700000,0.035000,0.127000> rotate<0,90.000000,0> translate<-7.600000,0.000000,22.100000> }
cylinder{<0,0,0><0,0.035000,0>0.254000 translate<-7.700000,0.000000,24.700000>}
cylinder{<0,0,0><0,0.035000,0>0.254000 translate<-7.600000,0.000000,24.500000>}
box{<0,0,-0.254000><0.223607,0.035000,0.254000> rotate<0,63.430762,0> translate<-7.700000,0.000000,24.700000> }
cylinder{<0,0,0><0,0.035000,0>0.254000 translate<-7.600000,-1.535000,19.400000>}
cylinder{<0,0,0><0,0.035000,0>0.254000 translate<-7.500000,-1.535000,19.400000>}
box{<0,0,-0.254000><0.100000,0.035000,0.254000> rotate<0,0.000000,0> translate<-7.600000,-1.535000,19.400000> }
cylinder{<0,0,0><0,0.035000,0>0.254000 translate<-7.600000,0.000000,24.500000>}
cylinder{<0,0,0><0,0.035000,0>0.254000 translate<-7.500000,0.000000,24.400000>}
box{<0,0,-0.254000><0.141421,0.035000,0.254000> rotate<0,44.997030,0> translate<-7.600000,0.000000,24.500000> }
cylinder{<0,0,0><0,0.035000,0>0.254000 translate<-7.500000,-1.535000,19.400000>}
cylinder{<0,0,0><0,0.035000,0>0.254000 translate<-7.400000,-1.535000,19.500000>}
box{<0,0,-0.254000><0.141421,0.035000,0.254000> rotate<0,-44.997030,0> translate<-7.500000,-1.535000,19.400000> }
cylinder{<0,0,0><0,0.035000,0>0.254000 translate<-7.400000,-1.535000,19.500000>}
cylinder{<0,0,0><0,0.035000,0>0.254000 translate<-7.300000,-1.535000,19.500000>}
box{<0,0,-0.254000><0.100000,0.035000,0.254000> rotate<0,0.000000,0> translate<-7.400000,-1.535000,19.500000> }
cylinder{<0,0,0><0,0.035000,0>0.254000 translate<-7.300000,-1.535000,19.500000>}
cylinder{<0,0,0><0,0.035000,0>0.254000 translate<-7.200000,-1.535000,19.600000>}
box{<0,0,-0.254000><0.141421,0.035000,0.254000> rotate<0,-44.997030,0> translate<-7.300000,-1.535000,19.500000> }
cylinder{<0,0,0><0,0.035000,0>0.254000 translate<-7.200000,-1.535000,19.600000>}
cylinder{<0,0,0><0,0.035000,0>0.254000 translate<-7.100000,-1.535000,19.600000>}
box{<0,0,-0.254000><0.100000,0.035000,0.254000> rotate<0,0.000000,0> translate<-7.200000,-1.535000,19.600000> }
cylinder{<0,0,0><0,0.035000,0>0.127000 translate<-7.600000,0.000000,22.100000>}
cylinder{<0,0,0><0,0.035000,0>0.127000 translate<-7.050000,0.000000,22.800000>}
box{<0,0,-0.127000><0.890225,0.035000,0.127000> rotate<0,-51.839352,0> translate<-7.600000,0.000000,22.100000> }
cylinder{<0,0,0><0,0.035000,0>0.254000 translate<-8.000000,0.000000,16.800000>}
cylinder{<0,0,0><0,0.035000,0>0.254000 translate<-7.000000,0.000000,16.765000>}
box{<0,0,-0.254000><1.000612,0.035000,0.254000> rotate<0,2.004402,0> translate<-8.000000,0.000000,16.800000> }
cylinder{<0,0,0><0,0.035000,0>0.254000 translate<-8.000000,0.000000,18.100000>}
cylinder{<0,0,0><0,0.035000,0>0.254000 translate<-7.000000,0.000000,18.035000>}
box{<0,0,-0.254000><1.002110,0.035000,0.254000> rotate<0,3.718749,0> translate<-8.000000,0.000000,18.100000> }
cylinder{<0,0,0><0,0.035000,0>0.127000 translate<-8.000000,0.000000,19.600000>}
cylinder{<0,0,0><0,0.035000,0>0.127000 translate<-7.000000,0.000000,19.305000>}
box{<0,0,-0.127000><1.042605,0.035000,0.127000> rotate<0,16.434975,0> translate<-8.000000,0.000000,19.600000> }
cylinder{<0,0,0><0,0.035000,0>0.254000 translate<-7.100000,-1.535000,19.600000>}
cylinder{<0,0,0><0,0.035000,0>0.254000 translate<-7.000000,-1.535000,19.700000>}
box{<0,0,-0.254000><0.141421,0.035000,0.254000> rotate<0,-44.997030,0> translate<-7.100000,-1.535000,19.600000> }
cylinder{<0,0,0><0,0.035000,0>0.127000 translate<-7.000000,0.000000,22.100000>}
cylinder{<0,0,0><0,0.035000,0>0.127000 translate<-7.000000,0.000000,21.300000>}
box{<0,0,-0.127000><0.800000,0.035000,0.127000> rotate<0,-90.000000,0> translate<-7.000000,0.000000,21.300000> }
cylinder{<0,0,0><0,0.035000,0>0.127000 translate<-7.000000,-1.535000,21.300000>}
cylinder{<0,0,0><0,0.035000,0>0.127000 translate<-7.000000,-1.535000,21.400000>}
box{<0,0,-0.127000><0.100000,0.035000,0.127000> rotate<0,90.000000,0> translate<-7.000000,-1.535000,21.400000> }
cylinder{<0,0,0><0,0.035000,0>0.127000 translate<-7.050000,0.000000,22.800000>}
cylinder{<0,0,0><0,0.035000,0>0.127000 translate<-7.000000,0.000000,22.100000>}
box{<0,0,-0.127000><0.701783,0.035000,0.127000> rotate<0,85.908713,0> translate<-7.050000,0.000000,22.800000> }
cylinder{<0,0,0><0,0.035000,0>0.254000 translate<-7.000000,-1.535000,19.700000>}
cylinder{<0,0,0><0,0.035000,0>0.254000 translate<-6.900000,-1.535000,19.700000>}
box{<0,0,-0.254000><0.100000,0.035000,0.254000> rotate<0,0.000000,0> translate<-7.000000,-1.535000,19.700000> }
cylinder{<0,0,0><0,0.035000,0>0.127000 translate<-7.000000,-1.535000,21.400000>}
cylinder{<0,0,0><0,0.035000,0>0.127000 translate<-6.900000,-1.535000,21.500000>}
box{<0,0,-0.127000><0.141421,0.035000,0.127000> rotate<0,-44.997030,0> translate<-7.000000,-1.535000,21.400000> }
cylinder{<0,0,0><0,0.035000,0>0.254000 translate<-6.900000,-1.535000,19.700000>}
cylinder{<0,0,0><0,0.035000,0>0.254000 translate<-6.800000,-1.535000,19.800000>}
box{<0,0,-0.254000><0.141421,0.035000,0.254000> rotate<0,-44.997030,0> translate<-6.900000,-1.535000,19.700000> }
cylinder{<0,0,0><0,0.035000,0>0.254000 translate<-6.800000,-1.535000,19.800000>}
cylinder{<0,0,0><0,0.035000,0>0.254000 translate<-6.700000,-1.535000,19.800000>}
box{<0,0,-0.254000><0.100000,0.035000,0.254000> rotate<0,0.000000,0> translate<-6.800000,-1.535000,19.800000> }
cylinder{<0,0,0><0,0.035000,0>0.254000 translate<-6.700000,-1.535000,19.800000>}
cylinder{<0,0,0><0,0.035000,0>0.254000 translate<-6.600000,-1.535000,19.900000>}
box{<0,0,-0.254000><0.141421,0.035000,0.254000> rotate<0,-44.997030,0> translate<-6.700000,-1.535000,19.800000> }
cylinder{<0,0,0><0,0.035000,0>0.127000 translate<-6.900000,-1.535000,21.500000>}
cylinder{<0,0,0><0,0.035000,0>0.127000 translate<-6.600000,-1.535000,21.500000>}
box{<0,0,-0.127000><0.300000,0.035000,0.127000> rotate<0,0.000000,0> translate<-6.900000,-1.535000,21.500000> }
cylinder{<0,0,0><0,0.035000,0>0.127000 translate<-7.000000,0.000000,15.495000>}
cylinder{<0,0,0><0,0.035000,0>0.127000 translate<-6.500000,0.000000,15.200000>}
box{<0,0,-0.127000><0.580539,0.035000,0.127000> rotate<0,30.538589,0> translate<-7.000000,0.000000,15.495000> }
cylinder{<0,0,0><0,0.035000,0>0.127000 translate<-6.500000,0.000000,3.800000>}
cylinder{<0,0,0><0,0.035000,0>0.127000 translate<-6.500000,0.000000,15.200000>}
box{<0,0,-0.127000><11.400000,0.035000,0.127000> rotate<0,90.000000,0> translate<-6.500000,0.000000,15.200000> }
cylinder{<0,0,0><0,0.035000,0>0.254000 translate<-6.600000,-1.535000,19.900000>}
cylinder{<0,0,0><0,0.035000,0>0.254000 translate<-6.500000,-1.535000,19.900000>}
box{<0,0,-0.254000><0.100000,0.035000,0.254000> rotate<0,0.000000,0> translate<-6.600000,-1.535000,19.900000> }
cylinder{<0,0,0><0,0.035000,0>0.127000 translate<-6.600000,-1.535000,21.500000>}
cylinder{<0,0,0><0,0.035000,0>0.127000 translate<-6.500000,-1.535000,21.600000>}
box{<0,0,-0.127000><0.141421,0.035000,0.127000> rotate<0,-44.997030,0> translate<-6.600000,-1.535000,21.500000> }
cylinder{<0,0,0><0,0.035000,0>0.127000 translate<-7.800000,-1.535000,24.200000>}
cylinder{<0,0,0><0,0.035000,0>0.127000 translate<-6.500000,-1.535000,24.200000>}
box{<0,0,-0.127000><1.300000,0.035000,0.127000> rotate<0,0.000000,0> translate<-7.800000,-1.535000,24.200000> }
cylinder{<0,0,0><0,0.035000,0>0.254000 translate<-6.500000,-1.535000,19.900000>}
cylinder{<0,0,0><0,0.035000,0>0.254000 translate<-6.400000,-1.535000,20.000000>}
box{<0,0,-0.254000><0.141421,0.035000,0.254000> rotate<0,-44.997030,0> translate<-6.500000,-1.535000,19.900000> }
cylinder{<0,0,0><0,0.035000,0>0.254000 translate<-6.400000,-1.535000,20.000000>}
cylinder{<0,0,0><0,0.035000,0>0.254000 translate<-6.300000,-1.535000,20.000000>}
box{<0,0,-0.254000><0.100000,0.035000,0.254000> rotate<0,0.000000,0> translate<-6.400000,-1.535000,20.000000> }
cylinder{<0,0,0><0,0.035000,0>0.254000 translate<-6.300000,-1.535000,20.000000>}
cylinder{<0,0,0><0,0.035000,0>0.254000 translate<-6.200000,-1.535000,20.100000>}
box{<0,0,-0.254000><0.141421,0.035000,0.254000> rotate<0,-44.997030,0> translate<-6.300000,-1.535000,20.000000> }
cylinder{<0,0,0><0,0.035000,0>0.127000 translate<-6.500000,0.000000,3.800000>}
cylinder{<0,0,0><0,0.035000,0>0.127000 translate<-6.100000,0.000000,3.400000>}
box{<0,0,-0.127000><0.565685,0.035000,0.127000> rotate<0,44.997030,0> translate<-6.500000,0.000000,3.800000> }
cylinder{<0,0,0><0,0.035000,0>0.254000 translate<-6.200000,-1.535000,20.100000>}
cylinder{<0,0,0><0,0.035000,0>0.254000 translate<-6.100000,-1.535000,20.100000>}
box{<0,0,-0.254000><0.100000,0.035000,0.254000> rotate<0,0.000000,0> translate<-6.200000,-1.535000,20.100000> }
cylinder{<0,0,0><0,0.035000,0>0.254000 translate<-6.100000,-1.535000,20.100000>}
cylinder{<0,0,0><0,0.035000,0>0.254000 translate<-6.000000,-1.535000,20.200000>}
box{<0,0,-0.254000><0.141421,0.035000,0.254000> rotate<0,-44.997030,0> translate<-6.100000,-1.535000,20.100000> }
cylinder{<0,0,0><0,0.035000,0>0.127000 translate<-6.500000,-1.535000,24.200000>}
cylinder{<0,0,0><0,0.035000,0>0.127000 translate<-5.900000,-1.535000,24.800000>}
box{<0,0,-0.127000><0.848528,0.035000,0.127000> rotate<0,-44.997030,0> translate<-6.500000,-1.535000,24.200000> }
cylinder{<0,0,0><0,0.035000,0>0.127000 translate<-6.500000,-1.535000,21.600000>}
cylinder{<0,0,0><0,0.035000,0>0.127000 translate<-5.500000,-1.535000,21.600000>}
box{<0,0,-0.127000><1.000000,0.035000,0.127000> rotate<0,0.000000,0> translate<-6.500000,-1.535000,21.600000> }
cylinder{<0,0,0><0,0.035000,0>0.254000 translate<-5.300000,-1.535000,18.600000>}
cylinder{<0,0,0><0,0.035000,0>0.254000 translate<-5.200000,-1.535000,18.600000>}
box{<0,0,-0.254000><0.100000,0.035000,0.254000> rotate<0,0.000000,0> translate<-5.300000,-1.535000,18.600000> }
cylinder{<0,0,0><0,0.035000,0>0.254000 translate<-5.200000,-1.535000,17.700000>}
cylinder{<0,0,0><0,0.035000,0>0.254000 translate<-5.200000,-1.535000,18.600000>}
box{<0,0,-0.254000><0.900000,0.035000,0.254000> rotate<0,90.000000,0> translate<-5.200000,-1.535000,18.600000> }
cylinder{<0,0,0><0,0.035000,0>0.254000 translate<-5.200000,0.000000,18.600000>}
cylinder{<0,0,0><0,0.035000,0>0.254000 translate<-5.200000,0.000000,24.200000>}
box{<0,0,-0.254000><5.600000,0.035000,0.254000> rotate<0,90.000000,0> translate<-5.200000,0.000000,24.200000> }
cylinder{<0,0,0><0,0.035000,0>0.254000 translate<-7.500000,0.000000,24.400000>}
cylinder{<0,0,0><0,0.035000,0>0.254000 translate<-5.200000,0.000000,24.400000>}
box{<0,0,-0.254000><2.300000,0.035000,0.254000> rotate<0,0.000000,0> translate<-7.500000,0.000000,24.400000> }
cylinder{<0,0,0><0,0.035000,0>0.254000 translate<-5.200000,0.000000,24.200000>}
cylinder{<0,0,0><0,0.035000,0>0.254000 translate<-5.200000,0.000000,24.400000>}
box{<0,0,-0.254000><0.200000,0.035000,0.254000> rotate<0,90.000000,0> translate<-5.200000,0.000000,24.400000> }
cylinder{<0,0,0><0,0.035000,0>0.127000 translate<-5.500000,-1.535000,21.600000>}
cylinder{<0,0,0><0,0.035000,0>0.127000 translate<-4.400000,-1.535000,22.600000>}
box{<0,0,-0.127000><1.486607,0.035000,0.127000> rotate<0,-42.270899,0> translate<-5.500000,-1.535000,21.600000> }
cylinder{<0,0,0><0,0.035000,0>0.254000 translate<-4.025000,0.000000,7.000000>}
cylinder{<0,0,0><0,0.035000,0>0.254000 translate<-4.000000,0.000000,9.700000>}
box{<0,0,-0.254000><2.700116,0.035000,0.254000> rotate<0,-89.463594,0> translate<-4.025000,0.000000,7.000000> }
cylinder{<0,0,0><0,0.035000,0>0.254000 translate<-4.000000,0.000000,9.700000>}
cylinder{<0,0,0><0,0.035000,0>0.254000 translate<-4.000000,0.000000,16.300000>}
box{<0,0,-0.254000><6.600000,0.035000,0.254000> rotate<0,90.000000,0> translate<-4.000000,0.000000,16.300000> }
cylinder{<0,0,0><0,0.035000,0>0.254000 translate<-5.200000,-1.535000,17.700000>}
cylinder{<0,0,0><0,0.035000,0>0.254000 translate<-4.000000,-1.535000,16.500000>}
box{<0,0,-0.254000><1.697056,0.035000,0.254000> rotate<0,44.997030,0> translate<-5.200000,-1.535000,17.700000> }
cylinder{<0,0,0><0,0.035000,0>0.254000 translate<-4.000000,0.000000,16.300000>}
cylinder{<0,0,0><0,0.035000,0>0.254000 translate<-3.900000,0.000000,16.400000>}
box{<0,0,-0.254000><0.141421,0.035000,0.254000> rotate<0,-44.997030,0> translate<-4.000000,0.000000,16.300000> }
cylinder{<0,0,0><0,0.035000,0>0.254000 translate<-4.000000,-1.535000,16.500000>}
cylinder{<0,0,0><0,0.035000,0>0.254000 translate<-3.900000,-1.535000,16.400000>}
box{<0,0,-0.254000><0.141421,0.035000,0.254000> rotate<0,44.997030,0> translate<-4.000000,-1.535000,16.500000> }
cylinder{<0,0,0><0,0.035000,0>0.254000 translate<-5.200000,0.000000,24.400000>}
cylinder{<0,0,0><0,0.035000,0>0.254000 translate<-3.600000,0.000000,24.400000>}
box{<0,0,-0.254000><1.600000,0.035000,0.254000> rotate<0,0.000000,0> translate<-5.200000,0.000000,24.400000> }
cylinder{<0,0,0><0,0.035000,0>0.127000 translate<-9.300000,-1.535000,15.400000>}
cylinder{<0,0,0><0,0.035000,0>0.127000 translate<-3.100000,-1.535000,15.400000>}
box{<0,0,-0.127000><6.200000,0.035000,0.127000> rotate<0,0.000000,0> translate<-9.300000,-1.535000,15.400000> }
cylinder{<0,0,0><0,0.035000,0>0.127000 translate<-3.450000,0.000000,18.600000>}
cylinder{<0,0,0><0,0.035000,0>0.127000 translate<-3.100000,0.000000,18.200000>}
box{<0,0,-0.127000><0.531507,0.035000,0.127000> rotate<0,48.810853,0> translate<-3.450000,0.000000,18.600000> }
cylinder{<0,0,0><0,0.035000,0>0.127000 translate<-3.100000,0.000000,15.400000>}
cylinder{<0,0,0><0,0.035000,0>0.127000 translate<-3.100000,0.000000,18.200000>}
box{<0,0,-0.127000><2.800000,0.035000,0.127000> rotate<0,90.000000,0> translate<-3.100000,0.000000,18.200000> }
cylinder{<0,0,0><0,0.035000,0>0.254000 translate<-3.600000,0.000000,24.400000>}
cylinder{<0,0,0><0,0.035000,0>0.254000 translate<-2.300000,0.000000,22.600000>}
box{<0,0,-0.254000><2.220360,0.035000,0.254000> rotate<0,54.158773,0> translate<-3.600000,0.000000,24.400000> }
cylinder{<0,0,0><0,0.035000,0>0.254000 translate<-4.025000,0.000000,7.000000>}
cylinder{<0,0,0><0,0.035000,0>0.254000 translate<-2.200000,0.000000,6.400000>}
box{<0,0,-0.254000><1.921100,0.035000,0.254000> rotate<0,18.197964,0> translate<-4.025000,0.000000,7.000000> }
cylinder{<0,0,0><0,0.035000,0>0.127000 translate<-5.900000,-1.535000,24.800000>}
cylinder{<0,0,0><0,0.035000,0>0.127000 translate<-0.900000,-1.535000,24.800000>}
box{<0,0,-0.127000><5.000000,0.035000,0.127000> rotate<0,0.000000,0> translate<-5.900000,-1.535000,24.800000> }
cylinder{<0,0,0><0,0.035000,0>0.127000 translate<-0.900000,-1.535000,24.800000>}
cylinder{<0,0,0><0,0.035000,0>0.127000 translate<-0.800000,-1.535000,24.700000>}
box{<0,0,-0.127000><0.141421,0.035000,0.127000> rotate<0,44.997030,0> translate<-0.900000,-1.535000,24.800000> }
cylinder{<0,0,0><0,0.035000,0>0.127000 translate<-0.800000,-1.535000,23.700000>}
cylinder{<0,0,0><0,0.035000,0>0.127000 translate<-0.800000,-1.535000,24.700000>}
box{<0,0,-0.127000><1.000000,0.035000,0.127000> rotate<0,90.000000,0> translate<-0.800000,-1.535000,24.700000> }
cylinder{<0,0,0><0,0.035000,0>0.254000 translate<-1.750000,0.000000,18.600000>}
cylinder{<0,0,0><0,0.035000,0>0.254000 translate<-0.500000,0.000000,18.600000>}
box{<0,0,-0.254000><1.250000,0.035000,0.254000> rotate<0,0.000000,0> translate<-1.750000,0.000000,18.600000> }
cylinder{<0,0,0><0,0.035000,0>0.127000 translate<-6.100000,0.000000,3.400000>}
cylinder{<0,0,0><0,0.035000,0>0.127000 translate<-0.400000,0.000000,3.400000>}
box{<0,0,-0.127000><5.700000,0.035000,0.127000> rotate<0,0.000000,0> translate<-6.100000,0.000000,3.400000> }
cylinder{<0,0,0><0,0.035000,0>0.127000 translate<-0.500000,0.000000,18.600000>}
cylinder{<0,0,0><0,0.035000,0>0.127000 translate<-0.200000,0.000000,18.500000>}
box{<0,0,-0.127000><0.316228,0.035000,0.127000> rotate<0,18.433732,0> translate<-0.500000,0.000000,18.600000> }
cylinder{<0,0,0><0,0.035000,0>0.127000 translate<-0.200000,0.000000,18.500000>}
cylinder{<0,0,0><0,0.035000,0>0.127000 translate<0.000000,0.000000,18.600000>}
box{<0,0,-0.127000><0.223607,0.035000,0.127000> rotate<0,-26.563298,0> translate<-0.200000,0.000000,18.500000> }
cylinder{<0,0,0><0,0.035000,0>0.254000 translate<-0.200000,0.000000,18.600000>}
cylinder{<0,0,0><0,0.035000,0>0.254000 translate<0.000000,0.000000,18.600000>}
box{<0,0,-0.254000><0.200000,0.035000,0.254000> rotate<0,0.000000,0> translate<-0.200000,0.000000,18.600000> }
cylinder{<0,0,0><0,0.035000,0>0.127000 translate<-0.800000,-1.535000,23.700000>}
cylinder{<0,0,0><0,0.035000,0>0.127000 translate<0.000000,-1.535000,22.600000>}
box{<0,0,-0.127000><1.360147,0.035000,0.127000> rotate<0,53.969065,0> translate<-0.800000,-1.535000,23.700000> }
cylinder{<0,0,0><0,0.035000,0>0.254000 translate<-2.200000,0.000000,6.400000>}
cylinder{<0,0,0><0,0.035000,0>0.254000 translate<0.300000,0.000000,6.400000>}
box{<0,0,-0.254000><2.500000,0.035000,0.254000> rotate<0,0.000000,0> translate<-2.200000,0.000000,6.400000> }
cylinder{<0,0,0><0,0.035000,0>0.127000 translate<-0.400000,0.000000,3.400000>}
cylinder{<0,0,0><0,0.035000,0>0.127000 translate<0.700000,0.000000,4.500000>}
box{<0,0,-0.127000><1.555635,0.035000,0.127000> rotate<0,-44.997030,0> translate<-0.400000,0.000000,3.400000> }
cylinder{<0,0,0><0,0.035000,0>0.254000 translate<0.300000,0.000000,6.400000>}
cylinder{<0,0,0><0,0.035000,0>0.254000 translate<0.850000,0.000000,6.400000>}
box{<0,0,-0.254000><0.550000,0.035000,0.254000> rotate<0,0.000000,0> translate<0.300000,0.000000,6.400000> }
cylinder{<0,0,0><0,0.035000,0>0.127000 translate<0.200000,0.000000,9.600000>}
cylinder{<0,0,0><0,0.035000,0>0.127000 translate<0.900000,0.000000,8.800000>}
box{<0,0,-0.127000><1.063015,0.035000,0.127000> rotate<0,48.810853,0> translate<0.200000,0.000000,9.600000> }
cylinder{<0,0,0><0,0.035000,0>0.127000 translate<0.900000,0.000000,8.100000>}
cylinder{<0,0,0><0,0.035000,0>0.127000 translate<0.900000,0.000000,8.800000>}
box{<0,0,-0.127000><0.700000,0.035000,0.127000> rotate<0,90.000000,0> translate<0.900000,0.000000,8.800000> }
cylinder{<0,0,0><0,0.035000,0>0.127000 translate<0.000000,-1.535000,22.600000>}
cylinder{<0,0,0><0,0.035000,0>0.127000 translate<1.100000,-1.535000,22.600000>}
box{<0,0,-0.127000><1.100000,0.035000,0.127000> rotate<0,0.000000,0> translate<0.000000,-1.535000,22.600000> }
cylinder{<0,0,0><0,0.035000,0>0.254000 translate<0.000000,0.000000,18.600000>}
cylinder{<0,0,0><0,0.035000,0>0.254000 translate<1.400000,0.000000,17.200000>}
box{<0,0,-0.254000><1.979899,0.035000,0.254000> rotate<0,44.997030,0> translate<0.000000,0.000000,18.600000> }
cylinder{<0,0,0><0,0.035000,0>0.254000 translate<1.400000,0.000000,11.253100>}
cylinder{<0,0,0><0,0.035000,0>0.254000 translate<1.400000,0.000000,17.200000>}
box{<0,0,-0.254000><5.946900,0.035000,0.254000> rotate<0,90.000000,0> translate<1.400000,0.000000,17.200000> }
cylinder{<0,0,0><0,0.035000,0>0.127000 translate<0.700000,0.000000,4.500000>}
cylinder{<0,0,0><0,0.035000,0>0.127000 translate<1.500000,0.000000,4.500000>}
box{<0,0,-0.127000><0.800000,0.035000,0.127000> rotate<0,0.000000,0> translate<0.700000,0.000000,4.500000> }
cylinder{<0,0,0><0,0.035000,0>0.254000 translate<-6.000000,-1.535000,20.200000>}
cylinder{<0,0,0><0,0.035000,0>0.254000 translate<1.600000,-1.535000,20.200000>}
box{<0,0,-0.254000><7.600000,0.035000,0.254000> rotate<0,0.000000,0> translate<-6.000000,-1.535000,20.200000> }
cylinder{<0,0,0><0,0.035000,0>0.254000 translate<1.600000,-1.535000,20.200000>}
cylinder{<0,0,0><0,0.035000,0>0.254000 translate<1.600000,-1.535000,20.900000>}
box{<0,0,-0.254000><0.700000,0.035000,0.254000> rotate<0,90.000000,0> translate<1.600000,-1.535000,20.900000> }
cylinder{<0,0,0><0,0.035000,0>0.127000 translate<1.100000,-1.535000,22.600000>}
cylinder{<0,0,0><0,0.035000,0>0.127000 translate<1.900000,-1.535000,22.600000>}
box{<0,0,-0.127000><0.800000,0.035000,0.127000> rotate<0,0.000000,0> translate<1.100000,-1.535000,22.600000> }
cylinder{<0,0,0><0,0.035000,0>0.127000 translate<1.900000,0.000000,19.100000>}
cylinder{<0,0,0><0,0.035000,0>0.127000 translate<1.900000,0.000000,22.600000>}
box{<0,0,-0.127000><3.500000,0.035000,0.127000> rotate<0,90.000000,0> translate<1.900000,0.000000,22.600000> }
cylinder{<0,0,0><0,0.035000,0>0.127000 translate<0.900000,0.000000,8.100000>}
cylinder{<0,0,0><0,0.035000,0>0.127000 translate<2.000000,0.000000,8.100000>}
box{<0,0,-0.127000><1.100000,0.035000,0.127000> rotate<0,0.000000,0> translate<0.900000,0.000000,8.100000> }
cylinder{<0,0,0><0,0.035000,0>0.127000 translate<2.000000,0.000000,8.100000>}
cylinder{<0,0,0><0,0.035000,0>0.127000 translate<2.100000,0.000000,8.000000>}
box{<0,0,-0.127000><0.141421,0.035000,0.127000> rotate<0,44.997030,0> translate<2.000000,0.000000,8.100000> }
cylinder{<0,0,0><0,0.035000,0>0.127000 translate<2.100000,0.000000,7.200000>}
cylinder{<0,0,0><0,0.035000,0>0.127000 translate<2.100000,0.000000,8.000000>}
box{<0,0,-0.127000><0.800000,0.035000,0.127000> rotate<0,90.000000,0> translate<2.100000,0.000000,8.000000> }
cylinder{<0,0,0><0,0.035000,0>0.127000 translate<2.100000,0.000000,7.200000>}
cylinder{<0,0,0><0,0.035000,0>0.127000 translate<2.200000,0.000000,7.100000>}
box{<0,0,-0.127000><0.141421,0.035000,0.127000> rotate<0,44.997030,0> translate<2.100000,0.000000,7.200000> }
cylinder{<0,0,0><0,0.035000,0>0.127000 translate<1.750000,-1.535000,12.600000>}
cylinder{<0,0,0><0,0.035000,0>0.127000 translate<2.300000,-1.535000,13.000000>}
box{<0,0,-0.127000><0.680074,0.035000,0.127000> rotate<0,-36.024996,0> translate<1.750000,-1.535000,12.600000> }
cylinder{<0,0,0><0,0.035000,0>0.127000 translate<2.500000,-1.535000,19.500000>}
cylinder{<0,0,0><0,0.035000,0>0.127000 translate<2.500000,-1.535000,20.300000>}
box{<0,0,-0.127000><0.800000,0.035000,0.127000> rotate<0,90.000000,0> translate<2.500000,-1.535000,20.300000> }
cylinder{<0,0,0><0,0.035000,0>0.254000 translate<1.400000,0.000000,11.253100>}
cylinder{<0,0,0><0,0.035000,0>0.254000 translate<2.628300,0.000000,10.024800>}
box{<0,0,-0.254000><1.737079,0.035000,0.254000> rotate<0,44.997030,0> translate<1.400000,0.000000,11.253100> }
cylinder{<0,0,0><0,0.035000,0>0.127000 translate<2.300000,0.000000,10.400000>}
cylinder{<0,0,0><0,0.035000,0>0.127000 translate<2.628300,0.000000,10.024800>}
box{<0,0,-0.127000><0.498554,0.035000,0.127000> rotate<0,48.810853,0> translate<2.300000,0.000000,10.400000> }
cylinder{<0,0,0><0,0.035000,0>0.127000 translate<1.900000,0.000000,19.100000>}
cylinder{<0,0,0><0,0.035000,0>0.127000 translate<2.700000,0.000000,19.100000>}
box{<0,0,-0.127000><0.800000,0.035000,0.127000> rotate<0,0.000000,0> translate<1.900000,0.000000,19.100000> }
cylinder{<0,0,0><0,0.035000,0>0.127000 translate<2.200000,0.000000,7.100000>}
cylinder{<0,0,0><0,0.035000,0>0.127000 translate<2.750000,0.000000,6.400000>}
box{<0,0,-0.127000><0.890225,0.035000,0.127000> rotate<0,51.839352,0> translate<2.200000,0.000000,7.100000> }
cylinder{<0,0,0><0,0.035000,0>0.127000 translate<2.700000,0.000000,19.100000>}
cylinder{<0,0,0><0,0.035000,0>0.127000 translate<2.800000,0.000000,19.000000>}
box{<0,0,-0.127000><0.141421,0.035000,0.127000> rotate<0,44.997030,0> translate<2.700000,0.000000,19.100000> }
cylinder{<0,0,0><0,0.035000,0>0.127000 translate<2.800000,0.000000,13.000000>}
cylinder{<0,0,0><0,0.035000,0>0.127000 translate<2.800000,0.000000,19.000000>}
box{<0,0,-0.127000><6.000000,0.035000,0.127000> rotate<0,90.000000,0> translate<2.800000,0.000000,19.000000> }
cylinder{<0,0,0><0,0.035000,0>0.127000 translate<2.628300,0.000000,10.024800>}
cylinder{<0,0,0><0,0.035000,0>0.127000 translate<3.000000,0.000000,9.600000>}
box{<0,0,-0.127000><0.564461,0.035000,0.127000> rotate<0,48.810853,0> translate<2.628300,0.000000,10.024800> }
cylinder{<0,0,0><0,0.035000,0>0.254000 translate<1.600000,-1.535000,20.900000>}
cylinder{<0,0,0><0,0.035000,0>0.254000 translate<3.000000,-1.535000,22.300000>}
box{<0,0,-0.254000><1.979899,0.035000,0.254000> rotate<0,-44.997030,0> translate<1.600000,-1.535000,20.900000> }
cylinder{<0,0,0><0,0.035000,0>0.127000 translate<2.300000,-1.535000,13.000000>}
cylinder{<0,0,0><0,0.035000,0>0.127000 translate<3.100000,-1.535000,13.000000>}
box{<0,0,-0.127000><0.800000,0.035000,0.127000> rotate<0,0.000000,0> translate<2.300000,-1.535000,13.000000> }
cylinder{<0,0,0><0,0.035000,0>0.127000 translate<2.800000,0.000000,13.000000>}
cylinder{<0,0,0><0,0.035000,0>0.127000 translate<3.100000,0.000000,13.000000>}
box{<0,0,-0.127000><0.300000,0.035000,0.127000> rotate<0,0.000000,0> translate<2.800000,0.000000,13.000000> }
cylinder{<0,0,0><0,0.035000,0>0.127000 translate<2.500000,-1.535000,19.500000>}
cylinder{<0,0,0><0,0.035000,0>0.127000 translate<3.100000,-1.535000,18.900000>}
box{<0,0,-0.127000><0.848528,0.035000,0.127000> rotate<0,44.997030,0> translate<2.500000,-1.535000,19.500000> }
cylinder{<0,0,0><0,0.035000,0>0.127000 translate<2.500000,-1.535000,20.300000>}
cylinder{<0,0,0><0,0.035000,0>0.127000 translate<3.300000,-1.535000,21.100000>}
box{<0,0,-0.127000><1.131371,0.035000,0.127000> rotate<0,-44.997030,0> translate<2.500000,-1.535000,20.300000> }
cylinder{<0,0,0><0,0.035000,0>0.127000 translate<3.300000,-1.535000,21.100000>}
cylinder{<0,0,0><0,0.035000,0>0.127000 translate<3.600000,-1.535000,21.100000>}
box{<0,0,-0.127000><0.300000,0.035000,0.127000> rotate<0,0.000000,0> translate<3.300000,-1.535000,21.100000> }
cylinder{<0,0,0><0,0.035000,0>0.127000 translate<3.500000,0.000000,19.700000>}
cylinder{<0,0,0><0,0.035000,0>0.127000 translate<3.700000,0.000000,19.700000>}
box{<0,0,-0.127000><0.200000,0.035000,0.127000> rotate<0,0.000000,0> translate<3.500000,0.000000,19.700000> }
cylinder{<0,0,0><0,0.035000,0>0.127000 translate<3.700000,0.000000,19.700000>}
cylinder{<0,0,0><0,0.035000,0>0.127000 translate<3.800000,0.000000,19.600000>}
box{<0,0,-0.127000><0.141421,0.035000,0.127000> rotate<0,44.997030,0> translate<3.700000,0.000000,19.700000> }
cylinder{<0,0,0><0,0.035000,0>0.127000 translate<3.800000,0.000000,13.600000>}
cylinder{<0,0,0><0,0.035000,0>0.127000 translate<3.800000,0.000000,19.600000>}
box{<0,0,-0.127000><6.000000,0.035000,0.127000> rotate<0,90.000000,0> translate<3.800000,0.000000,19.600000> }
cylinder{<0,0,0><0,0.035000,0>0.127000 translate<3.800000,0.000000,13.600000>}
cylinder{<0,0,0><0,0.035000,0>0.127000 translate<3.900000,0.000000,13.500000>}
box{<0,0,-0.127000><0.141421,0.035000,0.127000> rotate<0,44.997030,0> translate<3.800000,0.000000,13.600000> }
cylinder{<0,0,0><0,0.035000,0>0.127000 translate<4.200000,0.000000,3.800000>}
cylinder{<0,0,0><0,0.035000,0>0.127000 translate<4.200000,0.000000,2.700000>}
box{<0,0,-0.127000><1.100000,0.035000,0.127000> rotate<0,-90.000000,0> translate<4.200000,0.000000,2.700000> }
cylinder{<0,0,0><0,0.035000,0>0.127000 translate<4.100000,0.000000,3.700000>}
cylinder{<0,0,0><0,0.035000,0>0.127000 translate<4.200000,0.000000,3.800000>}
box{<0,0,-0.127000><0.141421,0.035000,0.127000> rotate<0,-44.997030,0> translate<4.100000,0.000000,3.700000> }
cylinder{<0,0,0><0,0.035000,0>0.127000 translate<4.200000,0.000000,4.300000>}
cylinder{<0,0,0><0,0.035000,0>0.127000 translate<4.200000,0.000000,3.800000>}
box{<0,0,-0.127000><0.500000,0.035000,0.127000> rotate<0,-90.000000,0> translate<4.200000,0.000000,3.800000> }
cylinder{<0,0,0><0,0.035000,0>0.127000 translate<4.200000,0.000000,4.500000>}
cylinder{<0,0,0><0,0.035000,0>0.127000 translate<4.200000,0.000000,4.300000>}
box{<0,0,-0.127000><0.200000,0.035000,0.127000> rotate<0,-90.000000,0> translate<4.200000,0.000000,4.300000> }
cylinder{<0,0,0><0,0.035000,0>0.127000 translate<1.500000,0.000000,4.500000>}
cylinder{<0,0,0><0,0.035000,0>0.127000 translate<4.200000,0.000000,4.500000>}
box{<0,0,-0.127000><2.700000,0.035000,0.127000> rotate<0,0.000000,0> translate<1.500000,0.000000,4.500000> }
cylinder{<0,0,0><0,0.035000,0>0.127000 translate<4.100000,0.000000,6.500000>}
cylinder{<0,0,0><0,0.035000,0>0.127000 translate<4.200000,0.000000,6.400000>}
box{<0,0,-0.127000><0.141421,0.035000,0.127000> rotate<0,44.997030,0> translate<4.100000,0.000000,6.500000> }
cylinder{<0,0,0><0,0.035000,0>0.127000 translate<4.200000,0.000000,4.500000>}
cylinder{<0,0,0><0,0.035000,0>0.127000 translate<4.200000,0.000000,6.400000>}
box{<0,0,-0.127000><1.900000,0.035000,0.127000> rotate<0,90.000000,0> translate<4.200000,0.000000,6.400000> }
cylinder{<0,0,0><0,0.035000,0>0.254000 translate<3.000000,-1.535000,22.300000>}
cylinder{<0,0,0><0,0.035000,0>0.254000 translate<4.385000,-1.535000,22.300000>}
box{<0,0,-0.254000><1.385000,0.035000,0.254000> rotate<0,0.000000,0> translate<3.000000,-1.535000,22.300000> }
cylinder{<0,0,0><0,0.035000,0>0.127000 translate<3.900000,0.000000,13.500000>}
cylinder{<0,0,0><0,0.035000,0>0.127000 translate<4.450000,0.000000,12.800000>}
box{<0,0,-0.127000><0.890225,0.035000,0.127000> rotate<0,51.839352,0> translate<3.900000,0.000000,13.500000> }
cylinder{<0,0,0><0,0.035000,0>0.127000 translate<3.600000,-1.535000,21.100000>}
cylinder{<0,0,0><0,0.035000,0>0.127000 translate<4.450000,-1.535000,21.095000>}
box{<0,0,-0.127000><0.850015,0.035000,0.127000> rotate<0,0.337008,0> translate<3.600000,-1.535000,21.100000> }
cylinder{<0,0,0><0,0.035000,0>0.254000 translate<4.385000,-1.535000,22.300000>}
cylinder{<0,0,0><0,0.035000,0>0.254000 translate<4.450000,-1.535000,22.365000>}
box{<0,0,-0.254000><0.091924,0.035000,0.254000> rotate<0,-44.997030,0> translate<4.385000,-1.535000,22.300000> }
cylinder{<0,0,0><0,0.035000,0>0.254000 translate<5.000000,0.000000,11.200000>}
cylinder{<0,0,0><0,0.035000,0>0.254000 translate<5.000000,0.000000,3.500000>}
box{<0,0,-0.254000><7.700000,0.035000,0.254000> rotate<0,-90.000000,0> translate<5.000000,0.000000,3.500000> }
cylinder{<0,0,0><0,0.035000,0>0.635000 translate<5.000000,0.000000,23.200000>}
cylinder{<0,0,0><0,0.035000,0>0.635000 translate<5.000000,0.000000,22.700000>}
box{<0,0,-0.635000><0.500000,0.035000,0.635000> rotate<0,-90.000000,0> translate<5.000000,0.000000,22.700000> }
cylinder{<0,0,0><0,0.035000,0>0.254000 translate<5.000000,0.000000,11.200000>}
cylinder{<0,0,0><0,0.035000,0>0.254000 translate<5.100000,0.000000,11.300000>}
box{<0,0,-0.254000><0.141421,0.035000,0.254000> rotate<0,-44.997030,0> translate<5.000000,0.000000,11.200000> }
cylinder{<0,0,0><0,0.035000,0>0.635000 translate<5.000000,0.000000,22.700000>}
cylinder{<0,0,0><0,0.035000,0>0.635000 translate<5.100000,0.000000,22.600000>}
box{<0,0,-0.635000><0.141421,0.035000,0.635000> rotate<0,44.997030,0> translate<5.000000,0.000000,22.700000> }
cylinder{<0,0,0><0,0.035000,0>0.127000 translate<4.450000,-1.535000,23.635000>}
cylinder{<0,0,0><0,0.035000,0>0.127000 translate<5.300000,-1.535000,23.600000>}
box{<0,0,-0.127000><0.850720,0.035000,0.127000> rotate<0,2.357750,0> translate<4.450000,-1.535000,23.635000> }
cylinder{<0,0,0><0,0.035000,0>0.254000 translate<5.100000,0.000000,11.300000>}
cylinder{<0,0,0><0,0.035000,0>0.254000 translate<5.800000,0.000000,11.300000>}
box{<0,0,-0.254000><0.700000,0.035000,0.254000> rotate<0,0.000000,0> translate<5.100000,0.000000,11.300000> }
cylinder{<0,0,0><0,0.035000,0>0.127000 translate<4.100000,-1.535000,6.500000>}
cylinder{<0,0,0><0,0.035000,0>0.127000 translate<5.900000,-1.535000,6.500000>}
box{<0,0,-0.127000><1.800000,0.035000,0.127000> rotate<0,0.000000,0> translate<4.100000,-1.535000,6.500000> }
cylinder{<0,0,0><0,0.035000,0>0.127000 translate<5.900000,0.000000,6.500000>}
cylinder{<0,0,0><0,0.035000,0>0.127000 translate<5.900000,0.000000,7.300000>}
box{<0,0,-0.127000><0.800000,0.035000,0.127000> rotate<0,90.000000,0> translate<5.900000,0.000000,7.300000> }
cylinder{<0,0,0><0,0.035000,0>0.127000 translate<3.500000,-1.535000,19.700000>}
cylinder{<0,0,0><0,0.035000,0>0.127000 translate<6.000000,-1.535000,19.700000>}
box{<0,0,-0.127000><2.500000,0.035000,0.127000> rotate<0,0.000000,0> translate<3.500000,-1.535000,19.700000> }
cylinder{<0,0,0><0,0.035000,0>0.127000 translate<4.200000,0.000000,2.700000>}
cylinder{<0,0,0><0,0.035000,0>0.127000 translate<6.100000,0.000000,2.700000>}
box{<0,0,-0.127000><1.900000,0.035000,0.127000> rotate<0,0.000000,0> translate<4.200000,0.000000,2.700000> }
cylinder{<0,0,0><0,0.035000,0>0.127000 translate<6.000000,-1.535000,19.700000>}
cylinder{<0,0,0><0,0.035000,0>0.127000 translate<6.100000,-1.535000,19.800000>}
box{<0,0,-0.127000><0.141421,0.035000,0.127000> rotate<0,-44.997030,0> translate<6.000000,-1.535000,19.700000> }
cylinder{<0,0,0><0,0.035000,0>0.127000 translate<6.100000,-1.535000,19.800000>}
cylinder{<0,0,0><0,0.035000,0>0.127000 translate<6.100000,-1.535000,19.900000>}
box{<0,0,-0.127000><0.100000,0.035000,0.127000> rotate<0,90.000000,0> translate<6.100000,-1.535000,19.900000> }
cylinder{<0,0,0><0,0.035000,0>0.127000 translate<6.100000,-1.535000,19.900000>}
cylinder{<0,0,0><0,0.035000,0>0.127000 translate<6.200000,-1.535000,20.000000>}
box{<0,0,-0.127000><0.141421,0.035000,0.127000> rotate<0,-44.997030,0> translate<6.100000,-1.535000,19.900000> }
cylinder{<0,0,0><0,0.035000,0>0.127000 translate<6.200000,-1.535000,20.000000>}
cylinder{<0,0,0><0,0.035000,0>0.127000 translate<6.200000,-1.535000,20.100000>}
box{<0,0,-0.127000><0.100000,0.035000,0.127000> rotate<0,90.000000,0> translate<6.200000,-1.535000,20.100000> }
cylinder{<0,0,0><0,0.035000,0>0.127000 translate<6.200000,-1.535000,20.100000>}
cylinder{<0,0,0><0,0.035000,0>0.127000 translate<6.300000,-1.535000,20.200000>}
box{<0,0,-0.127000><0.141421,0.035000,0.127000> rotate<0,-44.997030,0> translate<6.200000,-1.535000,20.100000> }
cylinder{<0,0,0><0,0.035000,0>0.127000 translate<6.300000,-1.535000,20.200000>}
cylinder{<0,0,0><0,0.035000,0>0.127000 translate<6.300000,-1.535000,20.700000>}
box{<0,0,-0.127000><0.500000,0.035000,0.127000> rotate<0,90.000000,0> translate<6.300000,-1.535000,20.700000> }
cylinder{<0,0,0><0,0.035000,0>0.254000 translate<5.800000,0.000000,11.300000>}
cylinder{<0,0,0><0,0.035000,0>0.254000 translate<6.350000,0.000000,11.850000>}
box{<0,0,-0.254000><0.777817,0.035000,0.254000> rotate<0,-44.997030,0> translate<5.800000,0.000000,11.300000> }
cylinder{<0,0,0><0,0.035000,0>0.254000 translate<6.350000,0.000000,12.800000>}
cylinder{<0,0,0><0,0.035000,0>0.254000 translate<6.350000,0.000000,11.850000>}
box{<0,0,-0.254000><0.950000,0.035000,0.254000> rotate<0,-90.000000,0> translate<6.350000,0.000000,11.850000> }
cylinder{<0,0,0><0,0.035000,0>0.127000 translate<6.300000,-1.535000,20.700000>}
cylinder{<0,0,0><0,0.035000,0>0.127000 translate<6.400000,-1.535000,20.800000>}
box{<0,0,-0.127000><0.141421,0.035000,0.127000> rotate<0,-44.997030,0> translate<6.300000,-1.535000,20.700000> }
cylinder{<0,0,0><0,0.035000,0>0.127000 translate<5.300000,-1.535000,23.600000>}
cylinder{<0,0,0><0,0.035000,0>0.127000 translate<6.400000,-1.535000,23.600000>}
box{<0,0,-0.127000><1.100000,0.035000,0.127000> rotate<0,0.000000,0> translate<5.300000,-1.535000,23.600000> }
cylinder{<0,0,0><0,0.035000,0>0.127000 translate<6.400000,-1.535000,20.800000>}
cylinder{<0,0,0><0,0.035000,0>0.127000 translate<6.500000,-1.535000,20.800000>}
box{<0,0,-0.127000><0.100000,0.035000,0.127000> rotate<0,0.000000,0> translate<6.400000,-1.535000,20.800000> }
cylinder{<0,0,0><0,0.035000,0>0.127000 translate<6.400000,-1.535000,23.600000>}
cylinder{<0,0,0><0,0.035000,0>0.127000 translate<6.500000,-1.535000,23.700000>}
box{<0,0,-0.127000><0.141421,0.035000,0.127000> rotate<0,-44.997030,0> translate<6.400000,-1.535000,23.600000> }
cylinder{<0,0,0><0,0.035000,0>0.127000 translate<5.900000,0.000000,7.300000>}
cylinder{<0,0,0><0,0.035000,0>0.127000 translate<6.600000,0.000000,7.850000>}
box{<0,0,-0.127000><0.890225,0.035000,0.127000> rotate<0,-38.154708,0> translate<5.900000,0.000000,7.300000> }
cylinder{<0,0,0><0,0.035000,0>0.127000 translate<3.100000,-1.535000,18.900000>}
cylinder{<0,0,0><0,0.035000,0>0.127000 translate<6.600000,-1.535000,18.900000>}
box{<0,0,-0.127000><3.500000,0.035000,0.127000> rotate<0,0.000000,0> translate<3.100000,-1.535000,18.900000> }
cylinder{<0,0,0><0,0.035000,0>0.127000 translate<6.500000,-1.535000,20.800000>}
cylinder{<0,0,0><0,0.035000,0>0.127000 translate<6.600000,-1.535000,20.900000>}
box{<0,0,-0.127000><0.141421,0.035000,0.127000> rotate<0,-44.997030,0> translate<6.500000,-1.535000,20.800000> }
cylinder{<0,0,0><0,0.035000,0>0.127000 translate<6.500000,-1.535000,23.700000>}
cylinder{<0,0,0><0,0.035000,0>0.127000 translate<6.600000,-1.535000,23.700000>}
box{<0,0,-0.127000><0.100000,0.035000,0.127000> rotate<0,0.000000,0> translate<6.500000,-1.535000,23.700000> }
cylinder{<0,0,0><0,0.035000,0>0.127000 translate<6.600000,-1.535000,20.900000>}
cylinder{<0,0,0><0,0.035000,0>0.127000 translate<6.700000,-1.535000,20.900000>}
box{<0,0,-0.127000><0.100000,0.035000,0.127000> rotate<0,0.000000,0> translate<6.600000,-1.535000,20.900000> }
cylinder{<0,0,0><0,0.035000,0>0.127000 translate<6.600000,-1.535000,23.700000>}
cylinder{<0,0,0><0,0.035000,0>0.127000 translate<6.700000,-1.535000,23.800000>}
box{<0,0,-0.127000><0.141421,0.035000,0.127000> rotate<0,-44.997030,0> translate<6.600000,-1.535000,23.700000> }
cylinder{<0,0,0><0,0.035000,0>0.127000 translate<6.100000,0.000000,2.700000>}
cylinder{<0,0,0><0,0.035000,0>0.127000 translate<6.800000,0.000000,2.650000>}
box{<0,0,-0.127000><0.701783,0.035000,0.127000> rotate<0,4.085347,0> translate<6.100000,0.000000,2.700000> }
cylinder{<0,0,0><0,0.035000,0>0.127000 translate<6.700000,-1.535000,20.900000>}
cylinder{<0,0,0><0,0.035000,0>0.127000 translate<6.800000,-1.535000,21.000000>}
box{<0,0,-0.127000><0.141421,0.035000,0.127000> rotate<0,-44.997030,0> translate<6.700000,-1.535000,20.900000> }
cylinder{<0,0,0><0,0.035000,0>0.127000 translate<6.700000,-1.535000,23.800000>}
cylinder{<0,0,0><0,0.035000,0>0.127000 translate<6.800000,-1.535000,23.800000>}
box{<0,0,-0.127000><0.100000,0.035000,0.127000> rotate<0,0.000000,0> translate<6.700000,-1.535000,23.800000> }
cylinder{<0,0,0><0,0.035000,0>0.127000 translate<6.800000,-1.535000,21.000000>}
cylinder{<0,0,0><0,0.035000,0>0.127000 translate<6.900000,-1.535000,21.000000>}
box{<0,0,-0.127000><0.100000,0.035000,0.127000> rotate<0,0.000000,0> translate<6.800000,-1.535000,21.000000> }
cylinder{<0,0,0><0,0.035000,0>0.127000 translate<6.800000,-1.535000,23.800000>}
cylinder{<0,0,0><0,0.035000,0>0.127000 translate<6.900000,-1.535000,23.900000>}
box{<0,0,-0.127000><0.141421,0.035000,0.127000> rotate<0,-44.997030,0> translate<6.800000,-1.535000,23.800000> }
cylinder{<0,0,0><0,0.035000,0>0.127000 translate<6.900000,-1.535000,21.000000>}
cylinder{<0,0,0><0,0.035000,0>0.127000 translate<7.000000,-1.535000,21.100000>}
box{<0,0,-0.127000><0.141421,0.035000,0.127000> rotate<0,-44.997030,0> translate<6.900000,-1.535000,21.000000> }
cylinder{<0,0,0><0,0.035000,0>0.127000 translate<6.900000,-1.535000,23.900000>}
cylinder{<0,0,0><0,0.035000,0>0.127000 translate<7.000000,-1.535000,23.900000>}
box{<0,0,-0.127000><0.100000,0.035000,0.127000> rotate<0,0.000000,0> translate<6.900000,-1.535000,23.900000> }
cylinder{<0,0,0><0,0.035000,0>0.127000 translate<7.000000,-1.535000,23.900000>}
cylinder{<0,0,0><0,0.035000,0>0.127000 translate<7.100000,-1.535000,24.000000>}
box{<0,0,-0.127000><0.141421,0.035000,0.127000> rotate<0,-44.997030,0> translate<7.000000,-1.535000,23.900000> }
cylinder{<0,0,0><0,0.035000,0>0.127000 translate<7.100000,-1.535000,24.000000>}
cylinder{<0,0,0><0,0.035000,0>0.127000 translate<7.200000,-1.535000,24.000000>}
box{<0,0,-0.127000><0.100000,0.035000,0.127000> rotate<0,0.000000,0> translate<7.100000,-1.535000,24.000000> }
cylinder{<0,0,0><0,0.035000,0>0.254000 translate<4.450000,-1.535000,22.365000>}
cylinder{<0,0,0><0,0.035000,0>0.254000 translate<7.235000,-1.535000,22.365000>}
box{<0,0,-0.254000><2.785000,0.035000,0.254000> rotate<0,0.000000,0> translate<4.450000,-1.535000,22.365000> }
cylinder{<0,0,0><0,0.035000,0>0.127000 translate<6.600000,0.000000,9.750000>}
cylinder{<0,0,0><0,0.035000,0>0.127000 translate<7.300000,0.000000,9.800000>}
box{<0,0,-0.127000><0.701783,0.035000,0.127000> rotate<0,-4.085347,0> translate<6.600000,0.000000,9.750000> }
cylinder{<0,0,0><0,0.035000,0>0.127000 translate<7.200000,-1.535000,24.000000>}
cylinder{<0,0,0><0,0.035000,0>0.127000 translate<7.300000,-1.535000,24.100000>}
box{<0,0,-0.127000><0.141421,0.035000,0.127000> rotate<0,-44.997030,0> translate<7.200000,-1.535000,24.000000> }
cylinder{<0,0,0><0,0.035000,0>0.127000 translate<7.300000,-1.535000,24.100000>}
cylinder{<0,0,0><0,0.035000,0>0.127000 translate<7.300000,-1.535000,25.800000>}
box{<0,0,-0.127000><1.700000,0.035000,0.127000> rotate<0,90.000000,0> translate<7.300000,-1.535000,25.800000> }
cylinder{<0,0,0><0,0.035000,0>0.254000 translate<7.235000,-1.535000,22.365000>}
cylinder{<0,0,0><0,0.035000,0>0.254000 translate<7.400000,-1.535000,22.200000>}
box{<0,0,-0.254000><0.233345,0.035000,0.254000> rotate<0,44.997030,0> translate<7.235000,-1.535000,22.365000> }
cylinder{<0,0,0><0,0.035000,0>0.127000 translate<7.300000,-1.535000,25.800000>}
cylinder{<0,0,0><0,0.035000,0>0.127000 translate<7.400000,-1.535000,25.900000>}
box{<0,0,-0.127000><0.141421,0.035000,0.127000> rotate<0,-44.997030,0> translate<7.300000,-1.535000,25.800000> }
cylinder{<0,0,0><0,0.035000,0>0.127000 translate<7.400000,-1.535000,25.900000>}
cylinder{<0,0,0><0,0.035000,0>0.127000 translate<7.400000,-1.535000,26.000000>}
box{<0,0,-0.127000><0.100000,0.035000,0.127000> rotate<0,90.000000,0> translate<7.400000,-1.535000,26.000000> }
cylinder{<0,0,0><0,0.035000,0>0.635000 translate<6.800000,0.000000,4.550000>}
cylinder{<0,0,0><0,0.035000,0>0.635000 translate<7.500000,0.000000,4.700000>}
box{<0,0,-0.635000><0.715891,0.035000,0.635000> rotate<0,-12.093959,0> translate<6.800000,0.000000,4.550000> }
cylinder{<0,0,0><0,0.035000,0>0.254000 translate<6.350000,0.000000,12.800000>}
cylinder{<0,0,0><0,0.035000,0>0.254000 translate<7.500000,0.000000,13.950000>}
box{<0,0,-0.254000><1.626346,0.035000,0.254000> rotate<0,-44.997030,0> translate<6.350000,0.000000,12.800000> }
cylinder{<0,0,0><0,0.035000,0>0.254000 translate<7.500000,0.000000,22.100000>}
cylinder{<0,0,0><0,0.035000,0>0.254000 translate<7.500000,0.000000,13.950000>}
box{<0,0,-0.254000><8.150000,0.035000,0.254000> rotate<0,-90.000000,0> translate<7.500000,0.000000,13.950000> }
cylinder{<0,0,0><0,0.035000,0>0.254000 translate<7.400000,0.000000,22.200000>}
cylinder{<0,0,0><0,0.035000,0>0.254000 translate<7.500000,0.000000,22.100000>}
box{<0,0,-0.254000><0.141421,0.035000,0.254000> rotate<0,44.997030,0> translate<7.400000,0.000000,22.200000> }
cylinder{<0,0,0><0,0.035000,0>0.127000 translate<7.400000,-1.535000,26.000000>}
cylinder{<0,0,0><0,0.035000,0>0.127000 translate<7.500000,-1.535000,26.100000>}
box{<0,0,-0.127000><0.141421,0.035000,0.127000> rotate<0,-44.997030,0> translate<7.400000,-1.535000,26.000000> }
cylinder{<0,0,0><0,0.035000,0>0.127000 translate<7.500000,-1.535000,26.100000>}
cylinder{<0,0,0><0,0.035000,0>0.127000 translate<7.500000,-1.535000,26.200000>}
box{<0,0,-0.127000><0.100000,0.035000,0.127000> rotate<0,90.000000,0> translate<7.500000,-1.535000,26.200000> }
cylinder{<0,0,0><0,0.035000,0>0.635000 translate<7.500000,0.000000,4.700000>}
cylinder{<0,0,0><0,0.035000,0>0.635000 translate<7.600000,0.000000,4.700000>}
box{<0,0,-0.635000><0.100000,0.035000,0.635000> rotate<0,0.000000,0> translate<7.500000,0.000000,4.700000> }
cylinder{<0,0,0><0,0.035000,0>0.127000 translate<7.500000,-1.535000,26.200000>}
cylinder{<0,0,0><0,0.035000,0>0.127000 translate<7.600000,-1.535000,26.300000>}
box{<0,0,-0.127000><0.141421,0.035000,0.127000> rotate<0,-44.997030,0> translate<7.500000,-1.535000,26.200000> }
cylinder{<0,0,0><0,0.035000,0>0.635000 translate<7.600000,0.000000,4.700000>}
cylinder{<0,0,0><0,0.035000,0>0.635000 translate<7.700000,0.000000,4.600000>}
box{<0,0,-0.635000><0.141421,0.035000,0.635000> rotate<0,44.997030,0> translate<7.600000,0.000000,4.700000> }
cylinder{<0,0,0><0,0.035000,0>0.127000 translate<6.600000,-1.535000,18.900000>}
cylinder{<0,0,0><0,0.035000,0>0.127000 translate<7.700000,-1.535000,20.000000>}
box{<0,0,-0.127000><1.555635,0.035000,0.127000> rotate<0,-44.997030,0> translate<6.600000,-1.535000,18.900000> }
cylinder{<0,0,0><0,0.035000,0>0.127000 translate<8.200000,0.000000,10.600000>}
cylinder{<0,0,0><0,0.035000,0>0.127000 translate<8.200000,0.000000,21.100000>}
box{<0,0,-0.127000><10.500000,0.035000,0.127000> rotate<0,90.000000,0> translate<8.200000,0.000000,21.100000> }
cylinder{<0,0,0><0,0.035000,0>0.127000 translate<7.000000,-1.535000,21.100000>}
cylinder{<0,0,0><0,0.035000,0>0.127000 translate<8.300000,-1.535000,21.100000>}
box{<0,0,-0.127000><1.300000,0.035000,0.127000> rotate<0,0.000000,0> translate<7.000000,-1.535000,21.100000> }
cylinder{<0,0,0><0,0.035000,0>0.127000 translate<7.300000,0.000000,9.800000>}
cylinder{<0,0,0><0,0.035000,0>0.127000 translate<8.500000,0.000000,9.000000>}
box{<0,0,-0.127000><1.442221,0.035000,0.127000> rotate<0,33.687844,0> translate<7.300000,0.000000,9.800000> }
cylinder{<0,0,0><0,0.035000,0>0.127000 translate<8.300000,-1.535000,21.100000>}
cylinder{<0,0,0><0,0.035000,0>0.127000 translate<9.150000,-1.535000,21.095000>}
box{<0,0,-0.127000><0.850015,0.035000,0.127000> rotate<0,0.337008,0> translate<8.300000,-1.535000,21.100000> }
cylinder{<0,0,0><0,0.035000,0>0.127000 translate<7.700000,-1.535000,20.000000>}
cylinder{<0,0,0><0,0.035000,0>0.127000 translate<9.200000,-1.535000,20.000000>}
box{<0,0,-0.127000><1.500000,0.035000,0.127000> rotate<0,0.000000,0> translate<7.700000,-1.535000,20.000000> }
cylinder{<0,0,0><0,0.035000,0>0.635000 translate<5.000000,0.000000,23.200000>}
cylinder{<0,0,0><0,0.035000,0>0.635000 translate<9.580000,0.000000,27.780000>}
box{<0,0,-0.635000><6.477098,0.035000,0.635000> rotate<0,-44.997030,0> translate<5.000000,0.000000,23.200000> }
cylinder{<0,0,0><0,0.035000,0>0.127000 translate<9.150000,-1.535000,21.095000>}
cylinder{<0,0,0><0,0.035000,0>0.127000 translate<10.000000,-1.535000,21.100000>}
box{<0,0,-0.127000><0.850015,0.035000,0.127000> rotate<0,-0.337008,0> translate<9.150000,-1.535000,21.095000> }
cylinder{<0,0,0><0,0.035000,0>0.127000 translate<9.150000,-1.535000,23.635000>}
cylinder{<0,0,0><0,0.035000,0>0.127000 translate<10.000000,-1.535000,23.600000>}
box{<0,0,-0.127000><0.850720,0.035000,0.127000> rotate<0,2.357750,0> translate<9.150000,-1.535000,23.635000> }
cylinder{<0,0,0><0,0.035000,0>0.635000 translate<7.700000,0.000000,4.600000>}
cylinder{<0,0,0><0,0.035000,0>0.635000 translate<10.400000,0.000000,4.600000>}
box{<0,0,-0.635000><2.700000,0.035000,0.635000> rotate<0,0.000000,0> translate<7.700000,0.000000,4.600000> }
cylinder{<0,0,0><0,0.035000,0>0.127000 translate<7.600000,-1.535000,26.300000>}
cylinder{<0,0,0><0,0.035000,0>0.127000 translate<10.400000,-1.535000,26.300000>}
box{<0,0,-0.127000><2.800000,0.035000,0.127000> rotate<0,0.000000,0> translate<7.600000,-1.535000,26.300000> }
cylinder{<0,0,0><0,0.035000,0>0.254000 translate<5.000000,-1.535000,3.500000>}
cylinder{<0,0,0><0,0.035000,0>0.254000 translate<10.500000,-1.535000,3.500000>}
box{<0,0,-0.254000><5.500000,0.035000,0.254000> rotate<0,0.000000,0> translate<5.000000,-1.535000,3.500000> }
cylinder{<0,0,0><0,0.035000,0>0.127000 translate<10.400000,-1.535000,26.300000>}
cylinder{<0,0,0><0,0.035000,0>0.127000 translate<10.500000,-1.535000,26.200000>}
box{<0,0,-0.127000><0.141421,0.035000,0.127000> rotate<0,44.997030,0> translate<10.400000,-1.535000,26.300000> }
cylinder{<0,0,0><0,0.035000,0>0.127000 translate<10.600000,-1.535000,26.200000>}
cylinder{<0,0,0><0,0.035000,0>0.127000 translate<10.600000,-1.535000,26.100000>}
box{<0,0,-0.127000><0.100000,0.035000,0.127000> rotate<0,-90.000000,0> translate<10.600000,-1.535000,26.100000> }
cylinder{<0,0,0><0,0.035000,0>0.127000 translate<10.500000,-1.535000,26.200000>}
cylinder{<0,0,0><0,0.035000,0>0.127000 translate<10.600000,-1.535000,26.200000>}
box{<0,0,-0.127000><0.100000,0.035000,0.127000> rotate<0,0.000000,0> translate<10.500000,-1.535000,26.200000> }
cylinder{<0,0,0><0,0.035000,0>0.127000 translate<10.000000,-1.535000,21.100000>}
cylinder{<0,0,0><0,0.035000,0>0.127000 translate<10.700000,-1.535000,21.100000>}
box{<0,0,-0.127000><0.700000,0.035000,0.127000> rotate<0,0.000000,0> translate<10.000000,-1.535000,21.100000> }
cylinder{<0,0,0><0,0.035000,0>0.127000 translate<8.200000,0.000000,21.100000>}
cylinder{<0,0,0><0,0.035000,0>0.127000 translate<10.700000,0.000000,23.600000>}
box{<0,0,-0.127000><3.535534,0.035000,0.127000> rotate<0,-44.997030,0> translate<8.200000,0.000000,21.100000> }
cylinder{<0,0,0><0,0.035000,0>0.127000 translate<10.000000,-1.535000,23.600000>}
cylinder{<0,0,0><0,0.035000,0>0.127000 translate<10.700000,-1.535000,23.600000>}
box{<0,0,-0.127000><0.700000,0.035000,0.127000> rotate<0,0.000000,0> translate<10.000000,-1.535000,23.600000> }
cylinder{<0,0,0><0,0.035000,0>0.127000 translate<10.600000,-1.535000,26.100000>}
cylinder{<0,0,0><0,0.035000,0>0.127000 translate<10.700000,-1.535000,26.000000>}
box{<0,0,-0.127000><0.141421,0.035000,0.127000> rotate<0,44.997030,0> translate<10.600000,-1.535000,26.100000> }
cylinder{<0,0,0><0,0.035000,0>0.127000 translate<10.800000,-1.535000,5.900000>}
cylinder{<0,0,0><0,0.035000,0>0.127000 translate<10.800000,-1.535000,6.700000>}
box{<0,0,-0.127000><0.800000,0.035000,0.127000> rotate<0,90.000000,0> translate<10.800000,-1.535000,6.700000> }
cylinder{<0,0,0><0,0.035000,0>0.127000 translate<8.200000,0.000000,10.600000>}
cylinder{<0,0,0><0,0.035000,0>0.127000 translate<10.800000,0.000000,8.000000>}
box{<0,0,-0.127000><3.676955,0.035000,0.127000> rotate<0,44.997030,0> translate<8.200000,0.000000,10.600000> }
cylinder{<0,0,0><0,0.035000,0>0.127000 translate<10.800000,0.000000,6.700000>}
cylinder{<0,0,0><0,0.035000,0>0.127000 translate<10.800000,0.000000,8.000000>}
box{<0,0,-0.127000><1.300000,0.035000,0.127000> rotate<0,90.000000,0> translate<10.800000,0.000000,8.000000> }
cylinder{<0,0,0><0,0.035000,0>0.127000 translate<10.700000,0.000000,23.600000>}
cylinder{<0,0,0><0,0.035000,0>0.127000 translate<10.800000,0.000000,23.500000>}
box{<0,0,-0.127000><0.141421,0.035000,0.127000> rotate<0,44.997030,0> translate<10.700000,0.000000,23.600000> }
cylinder{<0,0,0><0,0.035000,0>0.127000 translate<10.800000,-1.535000,26.000000>}
cylinder{<0,0,0><0,0.035000,0>0.127000 translate<10.800000,-1.535000,25.900000>}
box{<0,0,-0.127000><0.100000,0.035000,0.127000> rotate<0,-90.000000,0> translate<10.800000,-1.535000,25.900000> }
cylinder{<0,0,0><0,0.035000,0>0.127000 translate<10.700000,-1.535000,26.000000>}
cylinder{<0,0,0><0,0.035000,0>0.127000 translate<10.800000,-1.535000,26.000000>}
box{<0,0,-0.127000><0.100000,0.035000,0.127000> rotate<0,0.000000,0> translate<10.700000,-1.535000,26.000000> }
cylinder{<0,0,0><0,0.035000,0>0.127000 translate<9.200000,-1.535000,20.000000>}
cylinder{<0,0,0><0,0.035000,0>0.127000 translate<10.900000,-1.535000,20.000000>}
box{<0,0,-0.127000><1.700000,0.035000,0.127000> rotate<0,0.000000,0> translate<9.200000,-1.535000,20.000000> }
cylinder{<0,0,0><0,0.035000,0>0.127000 translate<10.900000,-1.535000,20.000000>}
cylinder{<0,0,0><0,0.035000,0>0.127000 translate<10.900000,-1.535000,20.200000>}
box{<0,0,-0.127000><0.200000,0.035000,0.127000> rotate<0,90.000000,0> translate<10.900000,-1.535000,20.200000> }
cylinder{<0,0,0><0,0.035000,0>0.127000 translate<10.800000,-1.535000,25.900000>}
cylinder{<0,0,0><0,0.035000,0>0.127000 translate<10.900000,-1.535000,25.800000>}
box{<0,0,-0.127000><0.141421,0.035000,0.127000> rotate<0,44.997030,0> translate<10.800000,-1.535000,25.900000> }
cylinder{<0,0,0><0,0.035000,0>0.127000 translate<10.900000,-1.535000,20.200000>}
cylinder{<0,0,0><0,0.035000,0>0.127000 translate<11.000000,-1.535000,20.300000>}
box{<0,0,-0.127000><0.141421,0.035000,0.127000> rotate<0,-44.997030,0> translate<10.900000,-1.535000,20.200000> }
cylinder{<0,0,0><0,0.035000,0>0.254000 translate<10.500000,-1.535000,3.500000>}
cylinder{<0,0,0><0,0.035000,0>0.254000 translate<11.200000,-1.535000,3.450000>}
box{<0,0,-0.254000><0.701783,0.035000,0.254000> rotate<0,4.085347,0> translate<10.500000,-1.535000,3.500000> }
cylinder{<0,0,0><0,0.035000,0>0.127000 translate<10.800000,-1.535000,5.900000>}
cylinder{<0,0,0><0,0.035000,0>0.127000 translate<11.200000,-1.535000,5.350000>}
box{<0,0,-0.127000><0.680074,0.035000,0.127000> rotate<0,53.969065,0> translate<10.800000,-1.535000,5.900000> }
cylinder{<0,0,0><0,0.035000,0>0.254000 translate<11.200000,-1.535000,3.450000>}
cylinder{<0,0,0><0,0.035000,0>0.254000 translate<11.350000,-1.535000,3.600000>}
box{<0,0,-0.254000><0.212132,0.035000,0.254000> rotate<0,-44.997030,0> translate<11.200000,-1.535000,3.450000> }
cylinder{<0,0,0><0,0.035000,0>0.635000 translate<10.400000,0.000000,4.600000>}
cylinder{<0,0,0><0,0.035000,0>0.635000 translate<11.700000,0.000000,4.600000>}
box{<0,0,-0.635000><1.300000,0.035000,0.635000> rotate<0,0.000000,0> translate<10.400000,0.000000,4.600000> }
cylinder{<0,0,0><0,0.035000,0>0.254000 translate<11.200000,-1.535000,3.450000>}
cylinder{<0,0,0><0,0.035000,0>0.254000 translate<11.900000,-1.535000,3.500000>}
box{<0,0,-0.254000><0.701783,0.035000,0.254000> rotate<0,-4.085347,0> translate<11.200000,-1.535000,3.450000> }
cylinder{<0,0,0><0,0.035000,0>0.127000 translate<11.200000,-1.535000,5.350000>}
cylinder{<0,0,0><0,0.035000,0>0.127000 translate<11.900000,-1.535000,5.400000>}
box{<0,0,-0.127000><0.701783,0.035000,0.127000> rotate<0,-4.085347,0> translate<11.200000,-1.535000,5.350000> }
cylinder{<0,0,0><0,0.035000,0>0.127000 translate<12.270000,0.000000,13.730000>}
cylinder{<0,0,0><0,0.035000,0>0.127000 translate<13.000000,0.000000,13.400000>}
box{<0,0,-0.127000><0.801124,0.035000,0.127000> rotate<0,24.323970,0> translate<12.270000,0.000000,13.730000> }
cylinder{<0,0,0><0,0.035000,0>0.127000 translate<13.000000,0.000000,12.900000>}
cylinder{<0,0,0><0,0.035000,0>0.127000 translate<13.000000,0.000000,13.400000>}
box{<0,0,-0.127000><0.500000,0.035000,0.127000> rotate<0,90.000000,0> translate<13.000000,0.000000,13.400000> }
cylinder{<0,0,0><0,0.035000,0>0.127000 translate<12.270000,0.000000,16.270000>}
cylinder{<0,0,0><0,0.035000,0>0.127000 translate<13.000000,0.000000,16.000000>}
box{<0,0,-0.127000><0.778332,0.035000,0.127000> rotate<0,20.296230,0> translate<12.270000,0.000000,16.270000> }
cylinder{<0,0,0><0,0.035000,0>0.127000 translate<11.900000,-1.535000,5.400000>}
cylinder{<0,0,0><0,0.035000,0>0.127000 translate<13.200000,-1.535000,5.400000>}
box{<0,0,-0.127000><1.300000,0.035000,0.127000> rotate<0,0.000000,0> translate<11.900000,-1.535000,5.400000> }
cylinder{<0,0,0><0,0.035000,0>0.127000 translate<13.200000,-1.535000,5.400000>}
cylinder{<0,0,0><0,0.035000,0>0.127000 translate<13.200000,-1.535000,5.500000>}
box{<0,0,-0.127000><0.100000,0.035000,0.127000> rotate<0,90.000000,0> translate<13.200000,-1.535000,5.500000> }
cylinder{<0,0,0><0,0.035000,0>0.127000 translate<11.000000,-1.535000,20.300000>}
cylinder{<0,0,0><0,0.035000,0>0.127000 translate<13.200000,-1.535000,20.300000>}
box{<0,0,-0.127000><2.200000,0.035000,0.127000> rotate<0,0.000000,0> translate<11.000000,-1.535000,20.300000> }
cylinder{<0,0,0><0,0.035000,0>0.127000 translate<13.200000,-1.535000,5.500000>}
cylinder{<0,0,0><0,0.035000,0>0.127000 translate<13.300000,-1.535000,5.600000>}
box{<0,0,-0.127000><0.141421,0.035000,0.127000> rotate<0,-44.997030,0> translate<13.200000,-1.535000,5.500000> }
cylinder{<0,0,0><0,0.035000,0>0.127000 translate<13.300000,-1.535000,5.600000>}
cylinder{<0,0,0><0,0.035000,0>0.127000 translate<13.300000,-1.535000,5.700000>}
box{<0,0,-0.127000><0.100000,0.035000,0.127000> rotate<0,90.000000,0> translate<13.300000,-1.535000,5.700000> }
cylinder{<0,0,0><0,0.035000,0>0.127000 translate<13.200000,-1.535000,20.300000>}
cylinder{<0,0,0><0,0.035000,0>0.127000 translate<13.300000,-1.535000,20.200000>}
box{<0,0,-0.127000><0.141421,0.035000,0.127000> rotate<0,44.997030,0> translate<13.200000,-1.535000,20.300000> }
cylinder{<0,0,0><0,0.035000,0>0.127000 translate<13.300000,-1.535000,5.700000>}
cylinder{<0,0,0><0,0.035000,0>0.127000 translate<13.400000,-1.535000,5.800000>}
box{<0,0,-0.127000><0.141421,0.035000,0.127000> rotate<0,-44.997030,0> translate<13.300000,-1.535000,5.700000> }
cylinder{<0,0,0><0,0.035000,0>0.127000 translate<13.000000,0.000000,12.900000>}
cylinder{<0,0,0><0,0.035000,0>0.127000 translate<13.500000,0.000000,12.900000>}
box{<0,0,-0.127000><0.500000,0.035000,0.127000> rotate<0,0.000000,0> translate<13.000000,0.000000,12.900000> }
cylinder{<0,0,0><0,0.035000,0>0.127000 translate<13.300000,-1.535000,20.200000>}
cylinder{<0,0,0><0,0.035000,0>0.127000 translate<13.500000,-1.535000,20.200000>}
box{<0,0,-0.127000><0.200000,0.035000,0.127000> rotate<0,0.000000,0> translate<13.300000,-1.535000,20.200000> }
cylinder{<0,0,0><0,0.035000,0>0.127000 translate<13.500000,0.000000,12.900000>}
cylinder{<0,0,0><0,0.035000,0>0.127000 translate<13.600000,0.000000,12.800000>}
box{<0,0,-0.127000><0.141421,0.035000,0.127000> rotate<0,44.997030,0> translate<13.500000,0.000000,12.900000> }
cylinder{<0,0,0><0,0.035000,0>0.127000 translate<13.600000,0.000000,8.200000>}
cylinder{<0,0,0><0,0.035000,0>0.127000 translate<13.600000,0.000000,12.800000>}
box{<0,0,-0.127000><4.600000,0.035000,0.127000> rotate<0,90.000000,0> translate<13.600000,0.000000,12.800000> }
cylinder{<0,0,0><0,0.035000,0>0.127000 translate<13.000000,0.000000,16.000000>}
cylinder{<0,0,0><0,0.035000,0>0.127000 translate<13.600000,0.000000,16.000000>}
box{<0,0,-0.127000><0.600000,0.035000,0.127000> rotate<0,0.000000,0> translate<13.000000,0.000000,16.000000> }
cylinder{<0,0,0><0,0.035000,0>0.127000 translate<13.500000,-1.535000,20.200000>}
cylinder{<0,0,0><0,0.035000,0>0.127000 translate<13.600000,-1.535000,20.100000>}
box{<0,0,-0.127000><0.141421,0.035000,0.127000> rotate<0,44.997030,0> translate<13.500000,-1.535000,20.200000> }
cylinder{<0,0,0><0,0.035000,0>0.127000 translate<10.700000,0.000000,21.100000>}
cylinder{<0,0,0><0,0.035000,0>0.127000 translate<13.600000,0.000000,21.100000>}
box{<0,0,-0.127000><2.900000,0.035000,0.127000> rotate<0,0.000000,0> translate<10.700000,0.000000,21.100000> }
cylinder{<0,0,0><0,0.035000,0>0.254000 translate<11.350000,-1.535000,3.600000>}
cylinder{<0,0,0><0,0.035000,0>0.254000 translate<13.700000,-1.535000,3.600000>}
box{<0,0,-0.254000><2.350000,0.035000,0.254000> rotate<0,0.000000,0> translate<11.350000,-1.535000,3.600000> }
cylinder{<0,0,0><0,0.035000,0>0.127000 translate<13.600000,0.000000,16.000000>}
cylinder{<0,0,0><0,0.035000,0>0.127000 translate<13.700000,0.000000,15.900000>}
box{<0,0,-0.127000><0.141421,0.035000,0.127000> rotate<0,44.997030,0> translate<13.600000,0.000000,16.000000> }
cylinder{<0,0,0><0,0.035000,0>0.127000 translate<13.700000,0.000000,13.400000>}
cylinder{<0,0,0><0,0.035000,0>0.127000 translate<13.700000,0.000000,15.900000>}
box{<0,0,-0.127000><2.500000,0.035000,0.127000> rotate<0,90.000000,0> translate<13.700000,0.000000,15.900000> }
cylinder{<0,0,0><0,0.035000,0>0.127000 translate<13.700000,0.000000,21.000000>}
cylinder{<0,0,0><0,0.035000,0>0.127000 translate<13.700000,0.000000,17.600000>}
box{<0,0,-0.127000><3.400000,0.035000,0.127000> rotate<0,-90.000000,0> translate<13.700000,0.000000,17.600000> }
cylinder{<0,0,0><0,0.035000,0>0.127000 translate<13.600000,0.000000,21.100000>}
cylinder{<0,0,0><0,0.035000,0>0.127000 translate<13.700000,0.000000,21.000000>}
box{<0,0,-0.127000><0.141421,0.035000,0.127000> rotate<0,44.997030,0> translate<13.600000,0.000000,21.100000> }
cylinder{<0,0,0><0,0.035000,0>0.127000 translate<13.700000,0.000000,13.400000>}
cylinder{<0,0,0><0,0.035000,0>0.127000 translate<13.800000,0.000000,13.300000>}
box{<0,0,-0.127000><0.141421,0.035000,0.127000> rotate<0,44.997030,0> translate<13.700000,0.000000,13.400000> }
cylinder{<0,0,0><0,0.035000,0>0.127000 translate<13.700000,0.000000,17.600000>}
cylinder{<0,0,0><0,0.035000,0>0.127000 translate<13.800000,0.000000,17.600000>}
box{<0,0,-0.127000><0.100000,0.035000,0.127000> rotate<0,0.000000,0> translate<13.700000,0.000000,17.600000> }
cylinder{<0,0,0><0,0.035000,0>0.127000 translate<13.800000,0.000000,13.300000>}
cylinder{<0,0,0><0,0.035000,0>0.127000 translate<13.900000,0.000000,13.300000>}
box{<0,0,-0.127000><0.100000,0.035000,0.127000> rotate<0,0.000000,0> translate<13.800000,0.000000,13.300000> }
cylinder{<0,0,0><0,0.035000,0>0.127000 translate<13.900000,0.000000,17.500000>}
cylinder{<0,0,0><0,0.035000,0>0.127000 translate<13.900000,0.000000,16.400000>}
box{<0,0,-0.127000><1.100000,0.035000,0.127000> rotate<0,-90.000000,0> translate<13.900000,0.000000,16.400000> }
cylinder{<0,0,0><0,0.035000,0>0.127000 translate<13.800000,0.000000,17.600000>}
cylinder{<0,0,0><0,0.035000,0>0.127000 translate<13.900000,0.000000,17.500000>}
box{<0,0,-0.127000><0.141421,0.035000,0.127000> rotate<0,44.997030,0> translate<13.800000,0.000000,17.600000> }
cylinder{<0,0,0><0,0.035000,0>0.127000 translate<13.900000,0.000000,13.300000>}
cylinder{<0,0,0><0,0.035000,0>0.127000 translate<14.000000,0.000000,13.200000>}
box{<0,0,-0.127000><0.141421,0.035000,0.127000> rotate<0,44.997030,0> translate<13.900000,0.000000,13.300000> }
cylinder{<0,0,0><0,0.035000,0>0.127000 translate<14.000000,0.000000,13.100000>}
cylinder{<0,0,0><0,0.035000,0>0.127000 translate<14.000000,0.000000,13.200000>}
box{<0,0,-0.127000><0.100000,0.035000,0.127000> rotate<0,90.000000,0> translate<14.000000,0.000000,13.200000> }
cylinder{<0,0,0><0,0.035000,0>0.127000 translate<13.900000,0.000000,16.400000>}
cylinder{<0,0,0><0,0.035000,0>0.127000 translate<14.000000,0.000000,16.400000>}
box{<0,0,-0.127000><0.100000,0.035000,0.127000> rotate<0,0.000000,0> translate<13.900000,0.000000,16.400000> }
cylinder{<0,0,0><0,0.035000,0>0.127000 translate<14.000000,0.000000,13.100000>}
cylinder{<0,0,0><0,0.035000,0>0.127000 translate<14.100000,0.000000,13.000000>}
box{<0,0,-0.127000><0.141421,0.035000,0.127000> rotate<0,44.997030,0> translate<14.000000,0.000000,13.100000> }
cylinder{<0,0,0><0,0.035000,0>0.127000 translate<14.100000,0.000000,9.800000>}
cylinder{<0,0,0><0,0.035000,0>0.127000 translate<14.100000,0.000000,13.000000>}
box{<0,0,-0.127000><3.200000,0.035000,0.127000> rotate<0,90.000000,0> translate<14.100000,0.000000,13.000000> }
cylinder{<0,0,0><0,0.035000,0>0.127000 translate<14.100000,0.000000,16.300000>}
cylinder{<0,0,0><0,0.035000,0>0.127000 translate<14.100000,0.000000,16.200000>}
box{<0,0,-0.127000><0.100000,0.035000,0.127000> rotate<0,-90.000000,0> translate<14.100000,0.000000,16.200000> }
cylinder{<0,0,0><0,0.035000,0>0.127000 translate<14.000000,0.000000,16.400000>}
cylinder{<0,0,0><0,0.035000,0>0.127000 translate<14.100000,0.000000,16.300000>}
box{<0,0,-0.127000><0.141421,0.035000,0.127000> rotate<0,44.997030,0> translate<14.000000,0.000000,16.400000> }
cylinder{<0,0,0><0,0.035000,0>0.127000 translate<14.200000,0.000000,16.100000>}
cylinder{<0,0,0><0,0.035000,0>0.127000 translate<14.200000,0.000000,13.700000>}
box{<0,0,-0.127000><2.400000,0.035000,0.127000> rotate<0,-90.000000,0> translate<14.200000,0.000000,13.700000> }
cylinder{<0,0,0><0,0.035000,0>0.127000 translate<14.100000,0.000000,16.200000>}
cylinder{<0,0,0><0,0.035000,0>0.127000 translate<14.200000,0.000000,16.100000>}
box{<0,0,-0.127000><0.141421,0.035000,0.127000> rotate<0,44.997030,0> translate<14.100000,0.000000,16.200000> }
cylinder{<0,0,0><0,0.035000,0>0.635000 translate<11.700000,0.000000,4.600000>}
cylinder{<0,0,0><0,0.035000,0>0.635000 translate<14.300000,0.000000,2.000000>}
box{<0,0,-0.635000><3.676955,0.035000,0.635000> rotate<0,44.997030,0> translate<11.700000,0.000000,4.600000> }
cylinder{<0,0,0><0,0.035000,0>0.127000 translate<14.200000,0.000000,13.700000>}
cylinder{<0,0,0><0,0.035000,0>0.127000 translate<14.300000,0.000000,13.700000>}
box{<0,0,-0.127000><0.100000,0.035000,0.127000> rotate<0,0.000000,0> translate<14.200000,0.000000,13.700000> }
cylinder{<0,0,0><0,0.035000,0>0.127000 translate<14.400000,0.000000,13.600000>}
cylinder{<0,0,0><0,0.035000,0>0.127000 translate<14.400000,0.000000,13.500000>}
box{<0,0,-0.127000><0.100000,0.035000,0.127000> rotate<0,-90.000000,0> translate<14.400000,0.000000,13.500000> }
cylinder{<0,0,0><0,0.035000,0>0.127000 translate<14.300000,0.000000,13.700000>}
cylinder{<0,0,0><0,0.035000,0>0.127000 translate<14.400000,0.000000,13.600000>}
box{<0,0,-0.127000><0.141421,0.035000,0.127000> rotate<0,44.997030,0> translate<14.300000,0.000000,13.700000> }
cylinder{<0,0,0><0,0.035000,0>0.254000 translate<13.700000,-1.535000,3.600000>}
cylinder{<0,0,0><0,0.035000,0>0.254000 translate<14.500000,-1.535000,4.400000>}
box{<0,0,-0.254000><1.131371,0.035000,0.254000> rotate<0,-44.997030,0> translate<13.700000,-1.535000,3.600000> }
cylinder{<0,0,0><0,0.035000,0>0.127000 translate<14.400000,0.000000,13.500000>}
cylinder{<0,0,0><0,0.035000,0>0.127000 translate<14.500000,0.000000,13.400000>}
box{<0,0,-0.127000><0.141421,0.035000,0.127000> rotate<0,44.997030,0> translate<14.400000,0.000000,13.500000> }
cylinder{<0,0,0><0,0.035000,0>0.406400 translate<14.500000,0.000000,18.000000>}
cylinder{<0,0,0><0,0.035000,0>0.406400 translate<14.500000,0.000000,21.600000>}
box{<0,0,-0.406400><3.600000,0.035000,0.406400> rotate<0,90.000000,0> translate<14.500000,0.000000,21.600000> }
cylinder{<0,0,0><0,0.035000,0>0.635000 translate<9.580000,0.000000,27.780000>}
cylinder{<0,0,0><0,0.035000,0>0.635000 translate<14.600000,0.000000,27.780000>}
box{<0,0,-0.635000><5.020000,0.035000,0.635000> rotate<0,0.000000,0> translate<9.580000,0.000000,27.780000> }
cylinder{<0,0,0><0,0.035000,0>0.127000 translate<12.270000,-1.535000,11.190000>}
cylinder{<0,0,0><0,0.035000,0>0.127000 translate<14.680000,-1.535000,13.600000>}
box{<0,0,-0.127000><3.408255,0.035000,0.127000> rotate<0,-44.997030,0> translate<12.270000,-1.535000,11.190000> }
cylinder{<0,0,0><0,0.035000,0>0.127000 translate<13.400000,-1.535000,5.800000>}
cylinder{<0,0,0><0,0.035000,0>0.127000 translate<15.800000,-1.535000,5.800000>}
box{<0,0,-0.127000><2.400000,0.035000,0.127000> rotate<0,0.000000,0> translate<13.400000,-1.535000,5.800000> }
cylinder{<0,0,0><0,0.035000,0>0.127000 translate<15.850000,-1.535000,13.600000>}
cylinder{<0,0,0><0,0.035000,0>0.127000 translate<15.850000,-1.535000,12.450000>}
box{<0,0,-0.127000><1.150000,0.035000,0.127000> rotate<0,-90.000000,0> translate<15.850000,-1.535000,12.450000> }
cylinder{<0,0,0><0,0.035000,0>0.127000 translate<14.680000,-1.535000,13.600000>}
cylinder{<0,0,0><0,0.035000,0>0.127000 translate<15.850000,-1.535000,13.600000>}
box{<0,0,-0.127000><1.170000,0.035000,0.127000> rotate<0,0.000000,0> translate<14.680000,-1.535000,13.600000> }
cylinder{<0,0,0><0,0.035000,0>0.127000 translate<15.800000,-1.535000,5.800000>}
cylinder{<0,0,0><0,0.035000,0>0.127000 translate<15.900000,-1.535000,5.900000>}
box{<0,0,-0.127000><0.141421,0.035000,0.127000> rotate<0,-44.997030,0> translate<15.800000,-1.535000,5.800000> }
cylinder{<0,0,0><0,0.035000,0>0.127000 translate<15.900000,-1.535000,5.900000>}
cylinder{<0,0,0><0,0.035000,0>0.127000 translate<15.900000,-1.535000,6.100000>}
box{<0,0,-0.127000><0.200000,0.035000,0.127000> rotate<0,90.000000,0> translate<15.900000,-1.535000,6.100000> }
cylinder{<0,0,0><0,0.035000,0>0.127000 translate<15.900000,-1.535000,10.100000>}
cylinder{<0,0,0><0,0.035000,0>0.127000 translate<15.900000,-1.535000,9.000000>}
box{<0,0,-0.127000><1.100000,0.035000,0.127000> rotate<0,-90.000000,0> translate<15.900000,-1.535000,9.000000> }
cylinder{<0,0,0><0,0.035000,0>0.127000 translate<15.550000,-1.535000,10.300000>}
cylinder{<0,0,0><0,0.035000,0>0.127000 translate<15.900000,-1.535000,10.100000>}
box{<0,0,-0.127000><0.403113,0.035000,0.127000> rotate<0,29.742918,0> translate<15.550000,-1.535000,10.300000> }
cylinder{<0,0,0><0,0.035000,0>0.127000 translate<15.850000,-1.535000,13.600000>}
cylinder{<0,0,0><0,0.035000,0>0.127000 translate<15.900000,-1.535000,14.300000>}
box{<0,0,-0.127000><0.701783,0.035000,0.127000> rotate<0,-85.908713,0> translate<15.850000,-1.535000,13.600000> }
cylinder{<0,0,0><0,0.035000,0>0.127000 translate<15.900000,-1.535000,14.300000>}
cylinder{<0,0,0><0,0.035000,0>0.127000 translate<15.900000,-1.535000,15.700000>}
box{<0,0,-0.127000><1.400000,0.035000,0.127000> rotate<0,90.000000,0> translate<15.900000,-1.535000,15.700000> }
cylinder{<0,0,0><0,0.035000,0>0.127000 translate<15.900000,-1.535000,6.100000>}
cylinder{<0,0,0><0,0.035000,0>0.127000 translate<16.000000,-1.535000,6.200000>}
box{<0,0,-0.127000><0.141421,0.035000,0.127000> rotate<0,-44.997030,0> translate<15.900000,-1.535000,6.100000> }
cylinder{<0,0,0><0,0.035000,0>0.127000 translate<16.000000,-1.535000,6.200000>}
cylinder{<0,0,0><0,0.035000,0>0.127000 translate<16.000000,-1.535000,6.300000>}
box{<0,0,-0.127000><0.100000,0.035000,0.127000> rotate<0,90.000000,0> translate<16.000000,-1.535000,6.300000> }
cylinder{<0,0,0><0,0.035000,0>0.127000 translate<16.000000,-1.535000,6.300000>}
cylinder{<0,0,0><0,0.035000,0>0.127000 translate<16.100000,-1.535000,6.400000>}
box{<0,0,-0.127000><0.141421,0.035000,0.127000> rotate<0,-44.997030,0> translate<16.000000,-1.535000,6.300000> }
cylinder{<0,0,0><0,0.035000,0>0.127000 translate<15.850000,-1.535000,12.450000>}
cylinder{<0,0,0><0,0.035000,0>0.127000 translate<16.100000,-1.535000,12.200000>}
box{<0,0,-0.127000><0.353553,0.035000,0.127000> rotate<0,44.997030,0> translate<15.850000,-1.535000,12.450000> }
cylinder{<0,0,0><0,0.035000,0>0.127000 translate<15.900000,-1.535000,15.700000>}
cylinder{<0,0,0><0,0.035000,0>0.127000 translate<16.150000,-1.535000,17.000000>}
box{<0,0,-0.127000><1.323820,0.035000,0.127000> rotate<0,-79.109252,0> translate<15.900000,-1.535000,15.700000> }
cylinder{<0,0,0><0,0.035000,0>0.127000 translate<12.270000,-1.535000,18.810000>}
cylinder{<0,0,0><0,0.035000,0>0.127000 translate<16.390000,-1.535000,18.810000>}
box{<0,0,-0.127000><4.120000,0.035000,0.127000> rotate<0,0.000000,0> translate<12.270000,-1.535000,18.810000> }
cylinder{<0,0,0><0,0.035000,0>0.127000 translate<16.390000,-1.535000,18.810000>}
cylinder{<0,0,0><0,0.035000,0>0.127000 translate<16.650000,-1.535000,18.550000>}
box{<0,0,-0.127000><0.367696,0.035000,0.127000> rotate<0,44.997030,0> translate<16.390000,-1.535000,18.810000> }
cylinder{<0,0,0><0,0.035000,0>0.127000 translate<16.650000,-1.535000,18.550000>}
cylinder{<0,0,0><0,0.035000,0>0.127000 translate<16.700000,-1.535000,18.600000>}
box{<0,0,-0.127000><0.070711,0.035000,0.127000> rotate<0,-44.997030,0> translate<16.650000,-1.535000,18.550000> }
cylinder{<0,0,0><0,0.035000,0>0.635000 translate<14.600000,0.000000,23.820000>}
cylinder{<0,0,0><0,0.035000,0>0.635000 translate<17.100000,0.000000,23.300000>}
box{<0,0,-0.635000><2.553507,0.035000,0.635000> rotate<0,11.749208,0> translate<14.600000,0.000000,23.820000> }
cylinder{<0,0,0><0,0.035000,0>0.127000 translate<10.900000,-1.535000,25.800000>}
cylinder{<0,0,0><0,0.035000,0>0.127000 translate<17.500000,-1.535000,25.800000>}
box{<0,0,-0.127000><6.600000,0.035000,0.127000> rotate<0,0.000000,0> translate<10.900000,-1.535000,25.800000> }
cylinder{<0,0,0><0,0.035000,0>0.127000 translate<16.100000,-1.535000,12.200000>}
cylinder{<0,0,0><0,0.035000,0>0.127000 translate<17.600000,-1.535000,12.200000>}
box{<0,0,-0.127000><1.500000,0.035000,0.127000> rotate<0,0.000000,0> translate<16.100000,-1.535000,12.200000> }
cylinder{<0,0,0><0,0.035000,0>0.127000 translate<17.500000,-1.535000,25.800000>}
cylinder{<0,0,0><0,0.035000,0>0.127000 translate<17.600000,-1.535000,25.900000>}
box{<0,0,-0.127000><0.141421,0.035000,0.127000> rotate<0,-44.997030,0> translate<17.500000,-1.535000,25.800000> }
cylinder{<0,0,0><0,0.035000,0>0.127000 translate<13.600000,-1.535000,20.100000>}
cylinder{<0,0,0><0,0.035000,0>0.127000 translate<17.700000,-1.535000,20.100000>}
box{<0,0,-0.127000><4.100000,0.035000,0.127000> rotate<0,0.000000,0> translate<13.600000,-1.535000,20.100000> }
cylinder{<0,0,0><0,0.035000,0>0.127000 translate<17.450000,-1.535000,10.300000>}
cylinder{<0,0,0><0,0.035000,0>0.127000 translate<17.800000,-1.535000,10.850000>}
box{<0,0,-0.127000><0.651920,0.035000,0.127000> rotate<0,-57.525011,0> translate<17.450000,-1.535000,10.300000> }
cylinder{<0,0,0><0,0.035000,0>0.127000 translate<17.800000,-1.535000,12.000000>}
cylinder{<0,0,0><0,0.035000,0>0.127000 translate<17.800000,-1.535000,10.850000>}
box{<0,0,-0.127000><1.150000,0.035000,0.127000> rotate<0,-90.000000,0> translate<17.800000,-1.535000,10.850000> }
cylinder{<0,0,0><0,0.035000,0>0.127000 translate<17.600000,-1.535000,12.200000>}
cylinder{<0,0,0><0,0.035000,0>0.127000 translate<17.800000,-1.535000,12.000000>}
box{<0,0,-0.127000><0.282843,0.035000,0.127000> rotate<0,44.997030,0> translate<17.600000,-1.535000,12.200000> }
cylinder{<0,0,0><0,0.035000,0>0.127000 translate<17.700000,-1.535000,20.100000>}
cylinder{<0,0,0><0,0.035000,0>0.127000 translate<17.800000,-1.535000,20.000000>}
box{<0,0,-0.127000><0.141421,0.035000,0.127000> rotate<0,44.997030,0> translate<17.700000,-1.535000,20.100000> }
cylinder{<0,0,0><0,0.035000,0>0.127000 translate<17.800000,0.000000,20.000000>}
cylinder{<0,0,0><0,0.035000,0>0.127000 translate<17.800000,0.000000,21.700000>}
box{<0,0,-0.127000><1.700000,0.035000,0.127000> rotate<0,90.000000,0> translate<17.800000,0.000000,21.700000> }
cylinder{<0,0,0><0,0.035000,0>0.127000 translate<17.800000,-1.535000,21.700000>}
cylinder{<0,0,0><0,0.035000,0>0.127000 translate<17.800000,-1.535000,23.600000>}
box{<0,0,-0.127000><1.900000,0.035000,0.127000> rotate<0,90.000000,0> translate<17.800000,-1.535000,23.600000> }
cylinder{<0,0,0><0,0.035000,0>0.127000 translate<17.900000,-1.535000,17.300000>}
cylinder{<0,0,0><0,0.035000,0>0.127000 translate<17.900000,-1.535000,16.550000>}
box{<0,0,-0.127000><0.750000,0.035000,0.127000> rotate<0,-90.000000,0> translate<17.900000,-1.535000,16.550000> }
cylinder{<0,0,0><0,0.035000,0>0.127000 translate<16.650000,-1.535000,18.550000>}
cylinder{<0,0,0><0,0.035000,0>0.127000 translate<17.900000,-1.535000,17.300000>}
box{<0,0,-0.127000><1.767767,0.035000,0.127000> rotate<0,44.997030,0> translate<16.650000,-1.535000,18.550000> }
cylinder{<0,0,0><0,0.035000,0>0.127000 translate<17.900000,-1.535000,16.550000>}
cylinder{<0,0,0><0,0.035000,0>0.127000 translate<18.050000,-1.535000,17.000000>}
box{<0,0,-0.127000><0.474342,0.035000,0.127000> rotate<0,-71.560328,0> translate<17.900000,-1.535000,16.550000> }
cylinder{<0,0,0><0,0.035000,0>0.635000 translate<14.600000,0.000000,27.780000>}
cylinder{<0,0,0><0,0.035000,0>0.635000 translate<18.120000,0.000000,27.780000>}
box{<0,0,-0.635000><3.520000,0.035000,0.635000> rotate<0,0.000000,0> translate<14.600000,0.000000,27.780000> }
cylinder{<0,0,0><0,0.035000,0>0.254000 translate<14.500000,-1.535000,4.400000>}
cylinder{<0,0,0><0,0.035000,0>0.254000 translate<18.170000,-1.535000,4.400000>}
box{<0,0,-0.254000><3.670000,0.035000,0.254000> rotate<0,0.000000,0> translate<14.500000,-1.535000,4.400000> }
cylinder{<0,0,0><0,0.035000,0>0.127000 translate<16.700000,-1.535000,18.600000>}
cylinder{<0,0,0><0,0.035000,0>0.127000 translate<18.200000,-1.535000,18.600000>}
box{<0,0,-0.127000><1.500000,0.035000,0.127000> rotate<0,0.000000,0> translate<16.700000,-1.535000,18.600000> }
cylinder{<0,0,0><0,0.035000,0>0.127000 translate<17.750000,-1.535000,13.600000>}
cylinder{<0,0,0><0,0.035000,0>0.127000 translate<18.300000,-1.535000,14.200000>}
box{<0,0,-0.127000><0.813941,0.035000,0.127000> rotate<0,-47.486419,0> translate<17.750000,-1.535000,13.600000> }
cylinder{<0,0,0><0,0.035000,0>0.127000 translate<18.200000,-1.535000,18.600000>}
cylinder{<0,0,0><0,0.035000,0>0.127000 translate<18.300000,-1.535000,18.700000>}
box{<0,0,-0.127000><0.141421,0.035000,0.127000> rotate<0,-44.997030,0> translate<18.200000,-1.535000,18.600000> }
cylinder{<0,0,0><0,0.035000,0>0.127000 translate<17.800000,-1.535000,23.600000>}
cylinder{<0,0,0><0,0.035000,0>0.127000 translate<18.400000,-1.535000,24.200000>}
box{<0,0,-0.127000><0.848528,0.035000,0.127000> rotate<0,-44.997030,0> translate<17.800000,-1.535000,23.600000> }
cylinder{<0,0,0><0,0.035000,0>0.127000 translate<18.300000,-1.535000,18.700000>}
cylinder{<0,0,0><0,0.035000,0>0.127000 translate<18.500000,-1.535000,18.700000>}
box{<0,0,-0.127000><0.200000,0.035000,0.127000> rotate<0,0.000000,0> translate<18.300000,-1.535000,18.700000> }
cylinder{<0,0,0><0,0.035000,0>0.127000 translate<18.500000,-1.535000,18.700000>}
cylinder{<0,0,0><0,0.035000,0>0.127000 translate<18.600000,-1.535000,18.800000>}
box{<0,0,-0.127000><0.141421,0.035000,0.127000> rotate<0,-44.997030,0> translate<18.500000,-1.535000,18.700000> }
cylinder{<0,0,0><0,0.035000,0>0.635000 translate<17.100000,0.000000,23.300000>}
cylinder{<0,0,0><0,0.035000,0>0.635000 translate<18.600000,0.000000,23.300000>}
box{<0,0,-0.635000><1.500000,0.035000,0.635000> rotate<0,0.000000,0> translate<17.100000,0.000000,23.300000> }
cylinder{<0,0,0><0,0.035000,0>0.127000 translate<18.600000,-1.535000,18.800000>}
cylinder{<0,0,0><0,0.035000,0>0.127000 translate<18.700000,-1.535000,18.800000>}
box{<0,0,-0.127000><0.100000,0.035000,0.127000> rotate<0,0.000000,0> translate<18.600000,-1.535000,18.800000> }
cylinder{<0,0,0><0,0.035000,0>0.127000 translate<18.700000,-1.535000,18.800000>}
cylinder{<0,0,0><0,0.035000,0>0.127000 translate<18.700000,-1.535000,18.900000>}
box{<0,0,-0.127000><0.100000,0.035000,0.127000> rotate<0,90.000000,0> translate<18.700000,-1.535000,18.900000> }
cylinder{<0,0,0><0,0.035000,0>0.127000 translate<18.700000,-1.535000,18.900000>}
cylinder{<0,0,0><0,0.035000,0>0.127000 translate<18.800000,-1.535000,19.000000>}
box{<0,0,-0.127000><0.141421,0.035000,0.127000> rotate<0,-44.997030,0> translate<18.700000,-1.535000,18.900000> }
cylinder{<0,0,0><0,0.035000,0>0.635000 translate<18.600000,0.000000,23.300000>}
cylinder{<0,0,0><0,0.035000,0>0.635000 translate<19.100000,0.000000,22.800000>}
box{<0,0,-0.635000><0.707107,0.035000,0.635000> rotate<0,44.997030,0> translate<18.600000,0.000000,23.300000> }
cylinder{<0,0,0><0,0.035000,0>0.127000 translate<17.600000,-1.535000,25.900000>}
cylinder{<0,0,0><0,0.035000,0>0.127000 translate<19.100000,-1.535000,25.900000>}
box{<0,0,-0.127000><1.500000,0.035000,0.127000> rotate<0,0.000000,0> translate<17.600000,-1.535000,25.900000> }
cylinder{<0,0,0><0,0.035000,0>0.635000 translate<19.100000,-1.535000,22.800000>}
cylinder{<0,0,0><0,0.035000,0>0.635000 translate<19.200000,-1.535000,22.900000>}
box{<0,0,-0.635000><0.141421,0.035000,0.635000> rotate<0,-44.997030,0> translate<19.100000,-1.535000,22.800000> }
cylinder{<0,0,0><0,0.035000,0>0.127000 translate<18.300000,-1.535000,14.200000>}
cylinder{<0,0,0><0,0.035000,0>0.127000 translate<19.300000,-1.535000,14.200000>}
box{<0,0,-0.127000><1.000000,0.035000,0.127000> rotate<0,0.000000,0> translate<18.300000,-1.535000,14.200000> }
cylinder{<0,0,0><0,0.035000,0>0.127000 translate<16.100000,-1.535000,6.400000>}
cylinder{<0,0,0><0,0.035000,0>0.127000 translate<19.400000,-1.535000,6.400000>}
box{<0,0,-0.127000><3.300000,0.035000,0.127000> rotate<0,0.000000,0> translate<16.100000,-1.535000,6.400000> }
cylinder{<0,0,0><0,0.035000,0>0.127000 translate<14.100000,0.000000,9.800000>}
cylinder{<0,0,0><0,0.035000,0>0.127000 translate<19.400000,0.000000,9.800000>}
box{<0,0,-0.127000><5.300000,0.035000,0.127000> rotate<0,0.000000,0> translate<14.100000,0.000000,9.800000> }
cylinder{<0,0,0><0,0.035000,0>0.127000 translate<19.300000,-1.535000,14.200000>}
cylinder{<0,0,0><0,0.035000,0>0.127000 translate<19.400000,-1.535000,14.300000>}
box{<0,0,-0.127000><0.141421,0.035000,0.127000> rotate<0,-44.997030,0> translate<19.300000,-1.535000,14.200000> }
cylinder{<0,0,0><0,0.035000,0>0.127000 translate<19.400000,-1.535000,6.400000>}
cylinder{<0,0,0><0,0.035000,0>0.127000 translate<19.500000,-1.535000,6.500000>}
box{<0,0,-0.127000><0.141421,0.035000,0.127000> rotate<0,-44.997030,0> translate<19.400000,-1.535000,6.400000> }
cylinder{<0,0,0><0,0.035000,0>0.127000 translate<19.500000,-1.535000,6.500000>}
cylinder{<0,0,0><0,0.035000,0>0.127000 translate<19.700000,-1.535000,6.500000>}
box{<0,0,-0.127000><0.200000,0.035000,0.127000> rotate<0,0.000000,0> translate<19.500000,-1.535000,6.500000> }
cylinder{<0,0,0><0,0.035000,0>0.127000 translate<19.700000,-1.535000,6.500000>}
cylinder{<0,0,0><0,0.035000,0>0.127000 translate<19.800000,-1.535000,6.600000>}
box{<0,0,-0.127000><0.141421,0.035000,0.127000> rotate<0,-44.997030,0> translate<19.700000,-1.535000,6.500000> }
cylinder{<0,0,0><0,0.035000,0>0.127000 translate<14.500000,0.000000,13.400000>}
cylinder{<0,0,0><0,0.035000,0>0.127000 translate<19.800000,0.000000,13.400000>}
box{<0,0,-0.127000><5.300000,0.035000,0.127000> rotate<0,0.000000,0> translate<14.500000,0.000000,13.400000> }
cylinder{<0,0,0><0,0.035000,0>0.127000 translate<19.100000,-1.535000,25.900000>}
cylinder{<0,0,0><0,0.035000,0>0.127000 translate<19.800000,-1.535000,25.850000>}
box{<0,0,-0.127000><0.701783,0.035000,0.127000> rotate<0,4.085347,0> translate<19.100000,-1.535000,25.900000> }
cylinder{<0,0,0><0,0.035000,0>0.127000 translate<19.800000,-1.535000,6.600000>}
cylinder{<0,0,0><0,0.035000,0>0.127000 translate<19.900000,-1.535000,6.600000>}
box{<0,0,-0.127000><0.100000,0.035000,0.127000> rotate<0,0.000000,0> translate<19.800000,-1.535000,6.600000> }
cylinder{<0,0,0><0,0.035000,0>0.127000 translate<19.900000,-1.535000,6.600000>}
cylinder{<0,0,0><0,0.035000,0>0.127000 translate<20.000000,-1.535000,6.700000>}
box{<0,0,-0.127000><0.141421,0.035000,0.127000> rotate<0,-44.997030,0> translate<19.900000,-1.535000,6.600000> }
cylinder{<0,0,0><0,0.035000,0>0.127000 translate<20.000000,-1.535000,6.700000>}
cylinder{<0,0,0><0,0.035000,0>0.127000 translate<20.000000,-1.535000,7.000000>}
box{<0,0,-0.127000><0.300000,0.035000,0.127000> rotate<0,90.000000,0> translate<20.000000,-1.535000,7.000000> }
cylinder{<0,0,0><0,0.035000,0>0.127000 translate<19.400000,-1.535000,14.300000>}
cylinder{<0,0,0><0,0.035000,0>0.127000 translate<20.000000,-1.535000,14.300000>}
box{<0,0,-0.127000><0.600000,0.035000,0.127000> rotate<0,0.000000,0> translate<19.400000,-1.535000,14.300000> }
cylinder{<0,0,0><0,0.035000,0>0.127000 translate<20.000000,-1.535000,7.000000>}
cylinder{<0,0,0><0,0.035000,0>0.127000 translate<20.100000,-1.535000,7.100000>}
box{<0,0,-0.127000><0.141421,0.035000,0.127000> rotate<0,-44.997030,0> translate<20.000000,-1.535000,7.000000> }
cylinder{<0,0,0><0,0.035000,0>0.127000 translate<20.000000,-1.535000,14.300000>}
cylinder{<0,0,0><0,0.035000,0>0.127000 translate<20.100000,-1.535000,14.400000>}
box{<0,0,-0.127000><0.141421,0.035000,0.127000> rotate<0,-44.997030,0> translate<20.000000,-1.535000,14.300000> }
cylinder{<0,0,0><0,0.035000,0>0.127000 translate<20.100000,-1.535000,14.400000>}
cylinder{<0,0,0><0,0.035000,0>0.127000 translate<20.100000,-1.535000,14.500000>}
box{<0,0,-0.127000><0.100000,0.035000,0.127000> rotate<0,90.000000,0> translate<20.100000,-1.535000,14.500000> }
cylinder{<0,0,0><0,0.035000,0>0.127000 translate<20.100000,-1.535000,14.500000>}
cylinder{<0,0,0><0,0.035000,0>0.127000 translate<20.200000,-1.535000,14.500000>}
box{<0,0,-0.127000><0.100000,0.035000,0.127000> rotate<0,0.000000,0> translate<20.100000,-1.535000,14.500000> }
cylinder{<0,0,0><0,0.035000,0>0.127000 translate<13.600000,0.000000,8.200000>}
cylinder{<0,0,0><0,0.035000,0>0.127000 translate<20.300000,0.000000,8.200000>}
box{<0,0,-0.127000><6.700000,0.035000,0.127000> rotate<0,0.000000,0> translate<13.600000,0.000000,8.200000> }
cylinder{<0,0,0><0,0.035000,0>0.127000 translate<20.200000,-1.535000,14.500000>}
cylinder{<0,0,0><0,0.035000,0>0.127000 translate<20.300000,-1.535000,14.600000>}
box{<0,0,-0.127000><0.141421,0.035000,0.127000> rotate<0,-44.997030,0> translate<20.200000,-1.535000,14.500000> }
cylinder{<0,0,0><0,0.035000,0>0.127000 translate<20.400000,0.000000,8.100000>}
cylinder{<0,0,0><0,0.035000,0>0.127000 translate<20.400000,0.000000,5.100000>}
box{<0,0,-0.127000><3.000000,0.035000,0.127000> rotate<0,-90.000000,0> translate<20.400000,0.000000,5.100000> }
cylinder{<0,0,0><0,0.035000,0>0.127000 translate<20.300000,0.000000,8.200000>}
cylinder{<0,0,0><0,0.035000,0>0.127000 translate<20.400000,0.000000,8.100000>}
box{<0,0,-0.127000><0.141421,0.035000,0.127000> rotate<0,44.997030,0> translate<20.300000,0.000000,8.200000> }
cylinder{<0,0,0><0,0.035000,0>0.127000 translate<19.800000,-1.535000,27.750000>}
cylinder{<0,0,0><0,0.035000,0>0.127000 translate<20.500000,-1.535000,27.800000>}
box{<0,0,-0.127000><0.701783,0.035000,0.127000> rotate<0,-4.085347,0> translate<19.800000,-1.535000,27.750000> }
cylinder{<0,0,0><0,0.035000,0>0.127000 translate<20.900000,-1.535000,9.800000>}
cylinder{<0,0,0><0,0.035000,0>0.127000 translate<20.900000,-1.535000,9.700000>}
box{<0,0,-0.127000><0.100000,0.035000,0.127000> rotate<0,-90.000000,0> translate<20.900000,-1.535000,9.700000> }
cylinder{<0,0,0><0,0.035000,0>0.127000 translate<19.400000,-1.535000,9.800000>}
cylinder{<0,0,0><0,0.035000,0>0.127000 translate<20.900000,-1.535000,9.800000>}
box{<0,0,-0.127000><1.500000,0.035000,0.127000> rotate<0,0.000000,0> translate<19.400000,-1.535000,9.800000> }
cylinder{<0,0,0><0,0.035000,0>0.127000 translate<20.100000,-1.535000,7.100000>}
cylinder{<0,0,0><0,0.035000,0>0.127000 translate<21.100000,-1.535000,7.100000>}
box{<0,0,-0.127000><1.000000,0.035000,0.127000> rotate<0,0.000000,0> translate<20.100000,-1.535000,7.100000> }
cylinder{<0,0,0><0,0.035000,0>0.127000 translate<18.800000,-1.535000,19.000000>}
cylinder{<0,0,0><0,0.035000,0>0.127000 translate<21.100000,-1.535000,19.000000>}
box{<0,0,-0.127000><2.300000,0.035000,0.127000> rotate<0,0.000000,0> translate<18.800000,-1.535000,19.000000> }
cylinder{<0,0,0><0,0.035000,0>0.127000 translate<19.800000,-1.535000,13.400000>}
cylinder{<0,0,0><0,0.035000,0>0.127000 translate<21.200000,-1.535000,13.400000>}
box{<0,0,-0.127000><1.400000,0.035000,0.127000> rotate<0,0.000000,0> translate<19.800000,-1.535000,13.400000> }
cylinder{<0,0,0><0,0.035000,0>0.635000 translate<21.700000,0.000000,11.200000>}
cylinder{<0,0,0><0,0.035000,0>0.635000 translate<21.700000,0.000000,11.100000>}
box{<0,0,-0.635000><0.100000,0.035000,0.635000> rotate<0,-90.000000,0> translate<21.700000,0.000000,11.100000> }
cylinder{<0,0,0><0,0.035000,0>0.635000 translate<21.700000,0.000000,10.500000>}
cylinder{<0,0,0><0,0.035000,0>0.635000 translate<21.700000,0.000000,11.200000>}
box{<0,0,-0.635000><0.700000,0.035000,0.635000> rotate<0,90.000000,0> translate<21.700000,0.000000,11.200000> }
cylinder{<0,0,0><0,0.035000,0>0.635000 translate<21.700000,0.000000,11.200000>}
cylinder{<0,0,0><0,0.035000,0>0.635000 translate<21.700000,0.000000,13.300000>}
box{<0,0,-0.635000><2.100000,0.035000,0.635000> rotate<0,90.000000,0> translate<21.700000,0.000000,13.300000> }
cylinder{<0,0,0><0,0.035000,0>0.635000 translate<21.700000,0.000000,13.300000>}
cylinder{<0,0,0><0,0.035000,0>0.635000 translate<21.700000,0.000000,14.300000>}
box{<0,0,-0.635000><1.000000,0.035000,0.635000> rotate<0,90.000000,0> translate<21.700000,0.000000,14.300000> }
cylinder{<0,0,0><0,0.035000,0>0.635000 translate<18.120000,0.000000,27.780000>}
cylinder{<0,0,0><0,0.035000,0>0.635000 translate<21.700000,0.000000,24.200000>}
box{<0,0,-0.635000><5.062885,0.035000,0.635000> rotate<0,44.997030,0> translate<18.120000,0.000000,27.780000> }
cylinder{<0,0,0><0,0.035000,0>0.635000 translate<21.700000,0.000000,14.300000>}
cylinder{<0,0,0><0,0.035000,0>0.635000 translate<21.700000,0.000000,24.200000>}
box{<0,0,-0.635000><9.900000,0.035000,0.635000> rotate<0,90.000000,0> translate<21.700000,0.000000,24.200000> }
cylinder{<0,0,0><0,0.035000,0>0.127000 translate<20.500000,-1.535000,27.800000>}
cylinder{<0,0,0><0,0.035000,0>0.127000 translate<21.800000,-1.535000,27.800000>}
box{<0,0,-0.127000><1.300000,0.035000,0.127000> rotate<0,0.000000,0> translate<20.500000,-1.535000,27.800000> }
cylinder{<0,0,0><0,0.035000,0>0.127000 translate<21.900000,-1.535000,27.700000>}
cylinder{<0,0,0><0,0.035000,0>0.127000 translate<21.900000,-1.535000,27.600000>}
box{<0,0,-0.127000><0.100000,0.035000,0.127000> rotate<0,-90.000000,0> translate<21.900000,-1.535000,27.600000> }
cylinder{<0,0,0><0,0.035000,0>0.127000 translate<21.800000,-1.535000,27.800000>}
cylinder{<0,0,0><0,0.035000,0>0.127000 translate<21.900000,-1.535000,27.700000>}
box{<0,0,-0.127000><0.141421,0.035000,0.127000> rotate<0,44.997030,0> translate<21.800000,-1.535000,27.800000> }
cylinder{<0,0,0><0,0.035000,0>0.127000 translate<21.100000,-1.535000,7.100000>}
cylinder{<0,0,0><0,0.035000,0>0.127000 translate<21.920000,-1.535000,7.670000>}
box{<0,0,-0.127000><0.998649,0.035000,0.127000> rotate<0,-34.801715,0> translate<21.100000,-1.535000,7.100000> }
cylinder{<0,0,0><0,0.035000,0>0.127000 translate<21.100000,-1.535000,19.000000>}
cylinder{<0,0,0><0,0.035000,0>0.127000 translate<21.920000,-1.535000,18.970000>}
box{<0,0,-0.127000><0.820549,0.035000,0.127000> rotate<0,2.095114,0> translate<21.100000,-1.535000,19.000000> }
cylinder{<0,0,0><0,0.035000,0>0.127000 translate<22.000000,-1.535000,27.500000>}
cylinder{<0,0,0><0,0.035000,0>0.127000 translate<22.000000,-1.535000,27.400000>}
box{<0,0,-0.127000><0.100000,0.035000,0.127000> rotate<0,-90.000000,0> translate<22.000000,-1.535000,27.400000> }
cylinder{<0,0,0><0,0.035000,0>0.127000 translate<21.900000,-1.535000,27.600000>}
cylinder{<0,0,0><0,0.035000,0>0.127000 translate<22.000000,-1.535000,27.500000>}
box{<0,0,-0.127000><0.141421,0.035000,0.127000> rotate<0,44.997030,0> translate<21.900000,-1.535000,27.600000> }
cylinder{<0,0,0><0,0.035000,0>0.127000 translate<21.200000,-1.535000,13.400000>}
cylinder{<0,0,0><0,0.035000,0>0.127000 translate<22.020000,-1.535000,13.370000>}
box{<0,0,-0.127000><0.820549,0.035000,0.127000> rotate<0,2.095114,0> translate<21.200000,-1.535000,13.400000> }
cylinder{<0,0,0><0,0.035000,0>0.127000 translate<22.100000,-1.535000,27.300000>}
cylinder{<0,0,0><0,0.035000,0>0.127000 translate<22.100000,-1.535000,27.200000>}
box{<0,0,-0.127000><0.100000,0.035000,0.127000> rotate<0,-90.000000,0> translate<22.100000,-1.535000,27.200000> }
cylinder{<0,0,0><0,0.035000,0>0.127000 translate<22.000000,-1.535000,27.400000>}
cylinder{<0,0,0><0,0.035000,0>0.127000 translate<22.100000,-1.535000,27.300000>}
box{<0,0,-0.127000><0.141421,0.035000,0.127000> rotate<0,44.997030,0> translate<22.000000,-1.535000,27.400000> }
cylinder{<0,0,0><0,0.035000,0>0.635000 translate<21.700000,0.000000,10.500000>}
cylinder{<0,0,0><0,0.035000,0>0.635000 translate<23.000000,0.000000,9.200000>}
box{<0,0,-0.635000><1.838478,0.035000,0.635000> rotate<0,44.997030,0> translate<21.700000,0.000000,10.500000> }
cylinder{<0,0,0><0,0.035000,0>0.127000 translate<22.100000,-1.535000,27.200000>}
cylinder{<0,0,0><0,0.035000,0>0.127000 translate<23.100000,-1.535000,27.200000>}
box{<0,0,-0.127000><1.000000,0.035000,0.127000> rotate<0,0.000000,0> translate<22.100000,-1.535000,27.200000> }
cylinder{<0,0,0><0,0.035000,0>0.127000 translate<15.900000,-1.535000,9.000000>}
cylinder{<0,0,0><0,0.035000,0>0.127000 translate<23.400000,-1.535000,9.000000>}
box{<0,0,-0.127000><7.500000,0.035000,0.127000> rotate<0,0.000000,0> translate<15.900000,-1.535000,9.000000> }
cylinder{<0,0,0><0,0.035000,0>0.127000 translate<23.400000,-1.535000,9.000000>}
cylinder{<0,0,0><0,0.035000,0>0.127000 translate<23.400000,-1.535000,9.100000>}
box{<0,0,-0.127000><0.100000,0.035000,0.127000> rotate<0,90.000000,0> translate<23.400000,-1.535000,9.100000> }
cylinder{<0,0,0><0,0.035000,0>0.127000 translate<23.400000,-1.535000,9.100000>}
cylinder{<0,0,0><0,0.035000,0>0.127000 translate<23.600000,-1.535000,9.100000>}
box{<0,0,-0.127000><0.200000,0.035000,0.127000> rotate<0,0.000000,0> translate<23.400000,-1.535000,9.100000> }
cylinder{<0,0,0><0,0.035000,0>0.127000 translate<23.700000,-1.535000,9.000000>}
cylinder{<0,0,0><0,0.035000,0>0.127000 translate<23.700000,-1.535000,8.900000>}
box{<0,0,-0.127000><0.100000,0.035000,0.127000> rotate<0,-90.000000,0> translate<23.700000,-1.535000,8.900000> }
cylinder{<0,0,0><0,0.035000,0>0.127000 translate<23.600000,-1.535000,9.100000>}
cylinder{<0,0,0><0,0.035000,0>0.127000 translate<23.700000,-1.535000,9.000000>}
box{<0,0,-0.127000><0.141421,0.035000,0.127000> rotate<0,44.997030,0> translate<23.600000,-1.535000,9.100000> }
cylinder{<0,0,0><0,0.035000,0>0.127000 translate<23.800000,-1.535000,8.800000>}
cylinder{<0,0,0><0,0.035000,0>0.127000 translate<23.800000,-1.535000,8.200000>}
box{<0,0,-0.127000><0.600000,0.035000,0.127000> rotate<0,-90.000000,0> translate<23.800000,-1.535000,8.200000> }
cylinder{<0,0,0><0,0.035000,0>0.127000 translate<23.700000,-1.535000,8.900000>}
cylinder{<0,0,0><0,0.035000,0>0.127000 translate<23.800000,-1.535000,8.800000>}
box{<0,0,-0.127000><0.141421,0.035000,0.127000> rotate<0,44.997030,0> translate<23.700000,-1.535000,8.900000> }
cylinder{<0,0,0><0,0.035000,0>0.127000 translate<20.300000,-1.535000,14.600000>}
cylinder{<0,0,0><0,0.035000,0>0.127000 translate<23.800000,-1.535000,14.600000>}
box{<0,0,-0.127000><3.500000,0.035000,0.127000> rotate<0,0.000000,0> translate<20.300000,-1.535000,14.600000> }
cylinder{<0,0,0><0,0.035000,0>0.635000 translate<21.700000,0.000000,14.300000>}
cylinder{<0,0,0><0,0.035000,0>0.635000 translate<23.850000,0.000000,14.300000>}
box{<0,0,-0.635000><2.150000,0.035000,0.635000> rotate<0,0.000000,0> translate<21.700000,0.000000,14.300000> }
cylinder{<0,0,0><0,0.035000,0>0.127000 translate<23.100000,-1.535000,27.200000>}
cylinder{<0,0,0><0,0.035000,0>0.127000 translate<23.860000,-1.535000,26.400000>}
box{<0,0,-0.127000><1.103449,0.035000,0.127000> rotate<0,46.465734,0> translate<23.100000,-1.535000,27.200000> }
cylinder{<0,0,0><0,0.035000,0>0.127000 translate<23.800000,-1.535000,8.200000>}
cylinder{<0,0,0><0,0.035000,0>0.127000 translate<23.900000,-1.535000,8.100000>}
box{<0,0,-0.127000><0.141421,0.035000,0.127000> rotate<0,44.997030,0> translate<23.800000,-1.535000,8.200000> }
cylinder{<0,0,0><0,0.035000,0>0.127000 translate<23.900000,-1.535000,14.500000>}
cylinder{<0,0,0><0,0.035000,0>0.127000 translate<23.900000,-1.535000,13.900000>}
box{<0,0,-0.127000><0.600000,0.035000,0.127000> rotate<0,-90.000000,0> translate<23.900000,-1.535000,13.900000> }
cylinder{<0,0,0><0,0.035000,0>0.127000 translate<23.800000,-1.535000,14.600000>}
cylinder{<0,0,0><0,0.035000,0>0.127000 translate<23.900000,-1.535000,14.500000>}
box{<0,0,-0.127000><0.141421,0.035000,0.127000> rotate<0,44.997030,0> translate<23.800000,-1.535000,14.600000> }
cylinder{<0,0,0><0,0.035000,0>0.127000 translate<23.900000,-1.535000,8.100000>}
cylinder{<0,0,0><0,0.035000,0>0.127000 translate<24.000000,-1.535000,8.100000>}
box{<0,0,-0.127000><0.100000,0.035000,0.127000> rotate<0,0.000000,0> translate<23.900000,-1.535000,8.100000> }
cylinder{<0,0,0><0,0.035000,0>0.127000 translate<20.900000,-1.535000,9.700000>}
cylinder{<0,0,0><0,0.035000,0>0.127000 translate<24.000000,-1.535000,9.700000>}
box{<0,0,-0.127000><3.100000,0.035000,0.127000> rotate<0,0.000000,0> translate<20.900000,-1.535000,9.700000> }
cylinder{<0,0,0><0,0.035000,0>0.127000 translate<23.900000,-1.535000,13.900000>}
cylinder{<0,0,0><0,0.035000,0>0.127000 translate<24.000000,-1.535000,13.800000>}
box{<0,0,-0.127000><0.141421,0.035000,0.127000> rotate<0,44.997030,0> translate<23.900000,-1.535000,13.900000> }
cylinder{<0,0,0><0,0.035000,0>0.635000 translate<23.850000,0.000000,14.300000>}
cylinder{<0,0,0><0,0.035000,0>0.635000 translate<24.050000,0.000000,14.500000>}
box{<0,0,-0.635000><0.282843,0.035000,0.635000> rotate<0,-44.997030,0> translate<23.850000,0.000000,14.300000> }
cylinder{<0,0,0><0,0.035000,0>0.127000 translate<24.000000,-1.535000,8.100000>}
cylinder{<0,0,0><0,0.035000,0>0.127000 translate<24.100000,-1.535000,8.000000>}
box{<0,0,-0.127000><0.141421,0.035000,0.127000> rotate<0,44.997030,0> translate<24.000000,-1.535000,8.100000> }
cylinder{<0,0,0><0,0.035000,0>0.127000 translate<24.000000,-1.535000,9.700000>}
cylinder{<0,0,0><0,0.035000,0>0.127000 translate<24.100000,-1.535000,9.800000>}
box{<0,0,-0.127000><0.141421,0.035000,0.127000> rotate<0,-44.997030,0> translate<24.000000,-1.535000,9.700000> }
cylinder{<0,0,0><0,0.035000,0>0.127000 translate<24.000000,-1.535000,13.800000>}
cylinder{<0,0,0><0,0.035000,0>0.127000 translate<24.100000,-1.535000,13.800000>}
box{<0,0,-0.127000><0.100000,0.035000,0.127000> rotate<0,0.000000,0> translate<24.000000,-1.535000,13.800000> }
cylinder{<0,0,0><0,0.035000,0>0.127000 translate<24.100000,-1.535000,8.000000>}
cylinder{<0,0,0><0,0.035000,0>0.127000 translate<24.200000,-1.535000,8.000000>}
box{<0,0,-0.127000><0.100000,0.035000,0.127000> rotate<0,0.000000,0> translate<24.100000,-1.535000,8.000000> }
cylinder{<0,0,0><0,0.035000,0>0.127000 translate<24.100000,-1.535000,13.800000>}
cylinder{<0,0,0><0,0.035000,0>0.127000 translate<24.200000,-1.535000,13.700000>}
box{<0,0,-0.127000><0.141421,0.035000,0.127000> rotate<0,44.997030,0> translate<24.100000,-1.535000,13.800000> }
cylinder{<0,0,0><0,0.035000,0>0.127000 translate<24.200000,-1.535000,8.000000>}
cylinder{<0,0,0><0,0.035000,0>0.127000 translate<24.300000,-1.535000,7.900000>}
box{<0,0,-0.127000><0.141421,0.035000,0.127000> rotate<0,44.997030,0> translate<24.200000,-1.535000,8.000000> }
cylinder{<0,0,0><0,0.035000,0>0.127000 translate<24.200000,-1.535000,13.700000>}
cylinder{<0,0,0><0,0.035000,0>0.127000 translate<24.300000,-1.535000,13.700000>}
box{<0,0,-0.127000><0.100000,0.035000,0.127000> rotate<0,0.000000,0> translate<24.200000,-1.535000,13.700000> }
cylinder{<0,0,0><0,0.035000,0>0.127000 translate<24.300000,-1.535000,7.900000>}
cylinder{<0,0,0><0,0.035000,0>0.127000 translate<24.400000,-1.535000,7.900000>}
box{<0,0,-0.127000><0.100000,0.035000,0.127000> rotate<0,0.000000,0> translate<24.300000,-1.535000,7.900000> }
cylinder{<0,0,0><0,0.035000,0>0.127000 translate<24.300000,-1.535000,13.700000>}
cylinder{<0,0,0><0,0.035000,0>0.127000 translate<24.400000,-1.535000,13.600000>}
box{<0,0,-0.127000><0.141421,0.035000,0.127000> rotate<0,44.997030,0> translate<24.300000,-1.535000,13.700000> }
cylinder{<0,0,0><0,0.035000,0>0.127000 translate<24.400000,-1.535000,7.900000>}
cylinder{<0,0,0><0,0.035000,0>0.127000 translate<24.500000,-1.535000,7.800000>}
box{<0,0,-0.127000><0.141421,0.035000,0.127000> rotate<0,44.997030,0> translate<24.400000,-1.535000,7.900000> }
cylinder{<0,0,0><0,0.035000,0>0.127000 translate<24.400000,-1.535000,13.600000>}
cylinder{<0,0,0><0,0.035000,0>0.127000 translate<24.500000,-1.535000,13.600000>}
box{<0,0,-0.127000><0.100000,0.035000,0.127000> rotate<0,0.000000,0> translate<24.400000,-1.535000,13.600000> }
cylinder{<0,0,0><0,0.035000,0>0.127000 translate<24.500000,-1.535000,7.800000>}
cylinder{<0,0,0><0,0.035000,0>0.127000 translate<24.600000,-1.535000,7.800000>}
box{<0,0,-0.127000><0.100000,0.035000,0.127000> rotate<0,0.000000,0> translate<24.500000,-1.535000,7.800000> }
cylinder{<0,0,0><0,0.035000,0>0.127000 translate<24.500000,-1.535000,13.600000>}
cylinder{<0,0,0><0,0.035000,0>0.127000 translate<24.600000,-1.535000,13.500000>}
box{<0,0,-0.127000><0.141421,0.035000,0.127000> rotate<0,44.997030,0> translate<24.500000,-1.535000,13.600000> }
cylinder{<0,0,0><0,0.035000,0>0.127000 translate<24.600000,-1.535000,7.800000>}
cylinder{<0,0,0><0,0.035000,0>0.127000 translate<24.700000,-1.535000,7.700000>}
box{<0,0,-0.127000><0.141421,0.035000,0.127000> rotate<0,44.997030,0> translate<24.600000,-1.535000,7.800000> }
cylinder{<0,0,0><0,0.035000,0>0.127000 translate<24.600000,-1.535000,13.500000>}
cylinder{<0,0,0><0,0.035000,0>0.127000 translate<24.700000,-1.535000,13.500000>}
box{<0,0,-0.127000><0.100000,0.035000,0.127000> rotate<0,0.000000,0> translate<24.600000,-1.535000,13.500000> }
cylinder{<0,0,0><0,0.035000,0>0.127000 translate<24.700000,-1.535000,13.500000>}
cylinder{<0,0,0><0,0.035000,0>0.127000 translate<24.800000,-1.535000,13.400000>}
box{<0,0,-0.127000><0.141421,0.035000,0.127000> rotate<0,44.997030,0> translate<24.700000,-1.535000,13.500000> }
cylinder{<0,0,0><0,0.035000,0>0.127000 translate<20.400000,0.000000,5.100000>}
cylinder{<0,0,0><0,0.035000,0>0.127000 translate<25.100000,0.000000,5.100000>}
box{<0,0,-0.127000><4.700000,0.035000,0.127000> rotate<0,0.000000,0> translate<20.400000,0.000000,5.100000> }
cylinder{<0,0,0><0,0.035000,0>0.635000 translate<14.300000,0.000000,2.000000>}
cylinder{<0,0,0><0,0.035000,0>0.635000 translate<25.700000,0.000000,2.000000>}
box{<0,0,-0.635000><11.400000,0.035000,0.635000> rotate<0,0.000000,0> translate<14.300000,0.000000,2.000000> }
cylinder{<0,0,0><0,0.035000,0>0.635000 translate<26.400000,0.000000,22.100000>}
cylinder{<0,0,0><0,0.035000,0>0.635000 translate<26.400000,0.000000,25.100000>}
box{<0,0,-0.635000><3.000000,0.035000,0.635000> rotate<0,90.000000,0> translate<26.400000,0.000000,25.100000> }
cylinder{<0,0,0><0,0.035000,0>0.635000 translate<26.400000,0.000000,25.100000>}
cylinder{<0,0,0><0,0.035000,0>0.635000 translate<26.400000,0.000000,26.400000>}
box{<0,0,-0.635000><1.300000,0.035000,0.635000> rotate<0,90.000000,0> translate<26.400000,0.000000,26.400000> }
cylinder{<0,0,0><0,0.035000,0>0.635000 translate<23.000000,0.000000,9.200000>}
cylinder{<0,0,0><0,0.035000,0>0.635000 translate<26.900000,0.000000,9.200000>}
box{<0,0,-0.635000><3.900000,0.035000,0.635000> rotate<0,0.000000,0> translate<23.000000,0.000000,9.200000> }
cylinder{<0,0,0><0,0.035000,0>0.635000 translate<26.900000,0.000000,3.400000>}
cylinder{<0,0,0><0,0.035000,0>0.635000 translate<26.900000,0.000000,9.200000>}
box{<0,0,-0.635000><5.800000,0.035000,0.635000> rotate<0,90.000000,0> translate<26.900000,0.000000,9.200000> }
cylinder{<0,0,0><0,0.035000,0>0.635000 translate<25.700000,0.000000,2.000000>}
cylinder{<0,0,0><0,0.035000,0>0.635000 translate<27.000000,0.000000,3.300000>}
box{<0,0,-0.635000><1.838478,0.035000,0.635000> rotate<0,-44.997030,0> translate<25.700000,0.000000,2.000000> }
cylinder{<0,0,0><0,0.035000,0>0.635000 translate<26.900000,0.000000,3.400000>}
cylinder{<0,0,0><0,0.035000,0>0.635000 translate<27.000000,0.000000,3.300000>}
box{<0,0,-0.635000><0.141421,0.035000,0.635000> rotate<0,44.997030,0> translate<26.900000,0.000000,3.400000> }
cylinder{<0,0,0><0,0.035000,0>0.635000 translate<19.200000,-1.535000,22.900000>}
cylinder{<0,0,0><0,0.035000,0>0.635000 translate<27.500000,-1.535000,22.900000>}
box{<0,0,-0.635000><8.300000,0.035000,0.635000> rotate<0,0.000000,0> translate<19.200000,-1.535000,22.900000> }
cylinder{<0,0,0><0,0.035000,0>0.635000 translate<26.400000,0.000000,22.100000>}
cylinder{<0,0,0><0,0.035000,0>0.635000 translate<27.700000,0.000000,20.800000>}
box{<0,0,-0.635000><1.838478,0.035000,0.635000> rotate<0,44.997030,0> translate<26.400000,0.000000,22.100000> }
cylinder{<0,0,0><0,0.035000,0>0.254000 translate<27.100000,0.000000,16.100000>}
cylinder{<0,0,0><0,0.035000,0>0.254000 translate<28.700000,0.000000,14.500000>}
box{<0,0,-0.254000><2.262742,0.035000,0.254000> rotate<0,44.997030,0> translate<27.100000,0.000000,16.100000> }
cylinder{<0,0,0><0,0.035000,0>0.635000 translate<27.500000,-1.535000,22.900000>}
cylinder{<0,0,0><0,0.035000,0>0.635000 translate<28.900000,-1.535000,22.900000>}
box{<0,0,-0.635000><1.400000,0.035000,0.635000> rotate<0,0.000000,0> translate<27.500000,-1.535000,22.900000> }
cylinder{<0,0,0><0,0.035000,0>0.635000 translate<28.900000,0.000000,22.900000>}
cylinder{<0,0,0><0,0.035000,0>0.635000 translate<28.900000,0.000000,26.360000>}
box{<0,0,-0.635000><3.460000,0.035000,0.635000> rotate<0,90.000000,0> translate<28.900000,0.000000,26.360000> }
cylinder{<0,0,0><0,0.035000,0>0.635000 translate<28.900000,0.000000,26.360000>}
cylinder{<0,0,0><0,0.035000,0>0.635000 translate<28.940000,0.000000,26.400000>}
box{<0,0,-0.635000><0.056569,0.035000,0.635000> rotate<0,-44.997030,0> translate<28.900000,0.000000,26.360000> }
cylinder{<0,0,0><0,0.035000,0>0.635000 translate<27.700000,0.000000,20.800000>}
cylinder{<0,0,0><0,0.035000,0>0.635000 translate<29.000000,0.000000,20.800000>}
box{<0,0,-0.635000><1.300000,0.035000,0.635000> rotate<0,0.000000,0> translate<27.700000,0.000000,20.800000> }
cylinder{<0,0,0><0,0.035000,0>0.254000 translate<28.700000,0.000000,14.500000>}
cylinder{<0,0,0><0,0.035000,0>0.254000 translate<29.150000,0.000000,14.500000>}
box{<0,0,-0.254000><0.450000,0.035000,0.254000> rotate<0,0.000000,0> translate<28.700000,0.000000,14.500000> }
cylinder{<0,0,0><0,0.035000,0>0.127000 translate<24.100000,-1.535000,9.800000>}
cylinder{<0,0,0><0,0.035000,0>0.127000 translate<29.500000,-1.535000,9.800000>}
box{<0,0,-0.127000><5.400000,0.035000,0.127000> rotate<0,0.000000,0> translate<24.100000,-1.535000,9.800000> }
cylinder{<0,0,0><0,0.035000,0>0.127000 translate<29.500000,-1.535000,9.800000>}
cylinder{<0,0,0><0,0.035000,0>0.127000 translate<29.600000,-1.535000,9.900000>}
box{<0,0,-0.127000><0.141421,0.035000,0.127000> rotate<0,-44.997030,0> translate<29.500000,-1.535000,9.800000> }
cylinder{<0,0,0><0,0.035000,0>0.127000 translate<29.600000,-1.535000,9.900000>}
cylinder{<0,0,0><0,0.035000,0>0.127000 translate<29.700000,-1.535000,9.900000>}
box{<0,0,-0.127000><0.100000,0.035000,0.127000> rotate<0,0.000000,0> translate<29.600000,-1.535000,9.900000> }
cylinder{<0,0,0><0,0.035000,0>0.127000 translate<29.700000,-1.535000,9.900000>}
cylinder{<0,0,0><0,0.035000,0>0.127000 translate<29.800000,-1.535000,10.000000>}
box{<0,0,-0.127000><0.141421,0.035000,0.127000> rotate<0,-44.997030,0> translate<29.700000,-1.535000,9.900000> }
cylinder{<0,0,0><0,0.035000,0>0.127000 translate<29.800000,-1.535000,10.000000>}
cylinder{<0,0,0><0,0.035000,0>0.127000 translate<29.900000,-1.535000,10.000000>}
box{<0,0,-0.127000><0.100000,0.035000,0.127000> rotate<0,0.000000,0> translate<29.800000,-1.535000,10.000000> }
cylinder{<0,0,0><0,0.035000,0>0.127000 translate<29.900000,-1.535000,10.000000>}
cylinder{<0,0,0><0,0.035000,0>0.127000 translate<30.000000,-1.535000,10.100000>}
box{<0,0,-0.127000><0.141421,0.035000,0.127000> rotate<0,-44.997030,0> translate<29.900000,-1.535000,10.000000> }
cylinder{<0,0,0><0,0.035000,0>0.127000 translate<30.000000,-1.535000,10.100000>}
cylinder{<0,0,0><0,0.035000,0>0.127000 translate<30.100000,-1.535000,10.100000>}
box{<0,0,-0.127000><0.100000,0.035000,0.127000> rotate<0,0.000000,0> translate<30.000000,-1.535000,10.100000> }
cylinder{<0,0,0><0,0.035000,0>0.127000 translate<30.100000,-1.535000,10.100000>}
cylinder{<0,0,0><0,0.035000,0>0.127000 translate<30.200000,-1.535000,10.200000>}
box{<0,0,-0.127000><0.141421,0.035000,0.127000> rotate<0,-44.997030,0> translate<30.100000,-1.535000,10.100000> }
cylinder{<0,0,0><0,0.035000,0>0.127000 translate<30.200000,-1.535000,10.200000>}
cylinder{<0,0,0><0,0.035000,0>0.127000 translate<30.200000,-1.535000,10.700000>}
box{<0,0,-0.127000><0.500000,0.035000,0.127000> rotate<0,90.000000,0> translate<30.200000,-1.535000,10.700000> }
cylinder{<0,0,0><0,0.035000,0>0.127000 translate<30.200000,-1.535000,10.700000>}
cylinder{<0,0,0><0,0.035000,0>0.127000 translate<30.300000,-1.535000,10.800000>}
box{<0,0,-0.127000><0.141421,0.035000,0.127000> rotate<0,-44.997030,0> translate<30.200000,-1.535000,10.700000> }
cylinder{<0,0,0><0,0.035000,0>0.127000 translate<18.400000,-1.535000,24.200000>}
cylinder{<0,0,0><0,0.035000,0>0.127000 translate<31.000000,-1.535000,24.200000>}
box{<0,0,-0.127000><12.600000,0.035000,0.127000> rotate<0,0.000000,0> translate<18.400000,-1.535000,24.200000> }
cylinder{<0,0,0><0,0.035000,0>0.127000 translate<25.100000,-1.535000,5.100000>}
cylinder{<0,0,0><0,0.035000,0>0.127000 translate<31.200000,-1.535000,5.100000>}
box{<0,0,-0.127000><6.100000,0.035000,0.127000> rotate<0,0.000000,0> translate<25.100000,-1.535000,5.100000> }
cylinder{<0,0,0><0,0.035000,0>0.127000 translate<24.700000,-1.535000,7.700000>}
cylinder{<0,0,0><0,0.035000,0>0.127000 translate<31.200000,-1.535000,7.700000>}
box{<0,0,-0.127000><6.500000,0.035000,0.127000> rotate<0,0.000000,0> translate<24.700000,-1.535000,7.700000> }
cylinder{<0,0,0><0,0.035000,0>0.127000 translate<30.300000,-1.535000,10.800000>}
cylinder{<0,0,0><0,0.035000,0>0.127000 translate<31.300000,-1.535000,10.800000>}
box{<0,0,-0.127000><1.000000,0.035000,0.127000> rotate<0,0.000000,0> translate<30.300000,-1.535000,10.800000> }
cylinder{<0,0,0><0,0.035000,0>0.127000 translate<24.800000,-1.535000,13.400000>}
cylinder{<0,0,0><0,0.035000,0>0.127000 translate<31.300000,-1.535000,13.400000>}
box{<0,0,-0.127000><6.500000,0.035000,0.127000> rotate<0,0.000000,0> translate<24.800000,-1.535000,13.400000> }
cylinder{<0,0,0><0,0.035000,0>0.127000 translate<31.000000,-1.535000,24.200000>}
cylinder{<0,0,0><0,0.035000,0>0.127000 translate<31.700000,-1.535000,24.900000>}
box{<0,0,-0.127000><0.989949,0.035000,0.127000> rotate<0,-44.997030,0> translate<31.000000,-1.535000,24.200000> }
cylinder{<0,0,0><0,0.035000,0>0.127000 translate<31.700000,-1.535000,24.900000>}
cylinder{<0,0,0><0,0.035000,0>0.127000 translate<31.700000,-1.535000,25.300000>}
box{<0,0,-0.127000><0.400000,0.035000,0.127000> rotate<0,90.000000,0> translate<31.700000,-1.535000,25.300000> }
cylinder{<0,0,0><0,0.035000,0>0.127000 translate<31.700000,-1.535000,25.300000>}
cylinder{<0,0,0><0,0.035000,0>0.127000 translate<32.000000,-1.535000,25.850000>}
box{<0,0,-0.127000><0.626498,0.035000,0.127000> rotate<0,-61.385489,0> translate<31.700000,-1.535000,25.300000> }
cylinder{<0,0,0><0,0.035000,0>0.127000 translate<31.200000,-1.535000,5.100000>}
cylinder{<0,0,0><0,0.035000,0>0.127000 translate<32.080000,-1.535000,5.130000>}
box{<0,0,-0.127000><0.880511,0.035000,0.127000> rotate<0,-1.952380,0> translate<31.200000,-1.535000,5.100000> }
cylinder{<0,0,0><0,0.035000,0>0.127000 translate<31.200000,-1.535000,7.700000>}
cylinder{<0,0,0><0,0.035000,0>0.127000 translate<32.080000,-1.535000,7.670000>}
box{<0,0,-0.127000><0.880511,0.035000,0.127000> rotate<0,1.952380,0> translate<31.200000,-1.535000,7.700000> }
cylinder{<0,0,0><0,0.035000,0>0.127000 translate<32.080000,-1.535000,18.970000>}
cylinder{<0,0,0><0,0.035000,0>0.127000 translate<32.100000,-1.535000,19.600000>}
box{<0,0,-0.127000><0.630317,0.035000,0.127000> rotate<0,-88.175877,0> translate<32.080000,-1.535000,18.970000> }
cylinder{<0,0,0><0,0.035000,0>0.127000 translate<32.100000,-1.535000,19.600000>}
cylinder{<0,0,0><0,0.035000,0>0.127000 translate<32.100000,-1.535000,20.400000>}
box{<0,0,-0.127000><0.800000,0.035000,0.127000> rotate<0,90.000000,0> translate<32.100000,-1.535000,20.400000> }
cylinder{<0,0,0><0,0.035000,0>0.127000 translate<32.100000,0.000000,20.400000>}
cylinder{<0,0,0><0,0.035000,0>0.127000 translate<32.100000,0.000000,24.400000>}
box{<0,0,-0.127000><4.000000,0.035000,0.127000> rotate<0,90.000000,0> translate<32.100000,0.000000,24.400000> }
cylinder{<0,0,0><0,0.035000,0>0.127000 translate<31.300000,-1.535000,10.800000>}
cylinder{<0,0,0><0,0.035000,0>0.127000 translate<32.180000,-1.535000,10.830000>}
box{<0,0,-0.127000><0.880511,0.035000,0.127000> rotate<0,-1.952380,0> translate<31.300000,-1.535000,10.800000> }
cylinder{<0,0,0><0,0.035000,0>0.127000 translate<31.300000,-1.535000,13.400000>}
cylinder{<0,0,0><0,0.035000,0>0.127000 translate<32.180000,-1.535000,13.370000>}
box{<0,0,-0.127000><0.880511,0.035000,0.127000> rotate<0,1.952380,0> translate<31.300000,-1.535000,13.400000> }
cylinder{<0,0,0><0,0.035000,0>0.127000 translate<32.100000,0.000000,24.400000>}
cylinder{<0,0,0><0,0.035000,0>0.127000 translate<32.200000,0.000000,24.500000>}
box{<0,0,-0.127000><0.141421,0.035000,0.127000> rotate<0,-44.997030,0> translate<32.100000,0.000000,24.400000> }
cylinder{<0,0,0><0,0.035000,0>0.127000 translate<32.100000,-1.535000,29.300000>}
cylinder{<0,0,0><0,0.035000,0>0.127000 translate<32.200000,-1.535000,29.100000>}
box{<0,0,-0.127000><0.223607,0.035000,0.127000> rotate<0,63.430762,0> translate<32.100000,-1.535000,29.300000> }
cylinder{<0,0,0><0,0.035000,0>0.127000 translate<32.200000,0.000000,24.500000>}
cylinder{<0,0,0><0,0.035000,0>0.127000 translate<32.200000,0.000000,29.100000>}
box{<0,0,-0.127000><4.600000,0.035000,0.127000> rotate<0,90.000000,0> translate<32.200000,0.000000,29.100000> }
cylinder{<0,0,0><0,0.035000,0>0.127000 translate<32.000000,-1.535000,27.750000>}
cylinder{<0,0,0><0,0.035000,0>0.127000 translate<32.700000,-1.535000,27.400000>}
box{<0,0,-0.127000><0.782624,0.035000,0.127000> rotate<0,26.563298,0> translate<32.000000,-1.535000,27.750000> }
cylinder{<0,0,0><0,0.035000,0>0.635000 translate<28.900000,-1.535000,22.900000>}
cylinder{<0,0,0><0,0.035000,0>0.635000 translate<33.400000,-1.535000,22.900000>}
box{<0,0,-0.635000><4.500000,0.035000,0.635000> rotate<0,0.000000,0> translate<28.900000,-1.535000,22.900000> }
cylinder{<0,0,0><0,0.035000,0>0.127000 translate<32.700000,-1.535000,27.400000>}
cylinder{<0,0,0><0,0.035000,0>0.127000 translate<34.700000,-1.535000,27.400000>}
box{<0,0,-0.127000><2.000000,0.035000,0.127000> rotate<0,0.000000,0> translate<32.700000,-1.535000,27.400000> }
cylinder{<0,0,0><0,0.035000,0>0.635000 translate<29.000000,0.000000,20.800000>}
cylinder{<0,0,0><0,0.035000,0>0.635000 translate<35.000000,0.000000,14.800000>}
box{<0,0,-0.635000><8.485281,0.035000,0.635000> rotate<0,44.997030,0> translate<29.000000,0.000000,20.800000> }
cylinder{<0,0,0><0,0.035000,0>0.635000 translate<35.000000,0.000000,13.000000>}
cylinder{<0,0,0><0,0.035000,0>0.635000 translate<35.000000,0.000000,14.800000>}
box{<0,0,-0.635000><1.800000,0.035000,0.635000> rotate<0,90.000000,0> translate<35.000000,0.000000,14.800000> }
cylinder{<0,0,0><0,0.035000,0>0.127000 translate<34.700000,-1.535000,27.400000>}
cylinder{<0,0,0><0,0.035000,0>0.127000 translate<35.460000,-1.535000,26.400000>}
box{<0,0,-0.127000><1.256025,0.035000,0.127000> rotate<0,52.761684,0> translate<34.700000,-1.535000,27.400000> }
cylinder{<0,0,0><0,0.035000,0>0.635000 translate<36.400000,-1.535000,20.300000>}
cylinder{<0,0,0><0,0.035000,0>0.635000 translate<36.400000,-1.535000,22.200000>}
box{<0,0,-0.635000><1.900000,0.035000,0.635000> rotate<0,90.000000,0> translate<36.400000,-1.535000,22.200000> }
cylinder{<0,0,0><0,0.035000,0>0.635000 translate<35.000000,-1.535000,13.000000>}
cylinder{<0,0,0><0,0.035000,0>0.635000 translate<38.000000,-1.535000,9.900000>}
box{<0,0,-0.635000><4.313931,0.035000,0.635000> rotate<0,45.936159,0> translate<35.000000,-1.535000,13.000000> }
cylinder{<0,0,0><0,0.035000,0>0.635000 translate<38.000000,-1.535000,26.400000>}
cylinder{<0,0,0><0,0.035000,0>0.635000 translate<38.100000,-1.535000,26.300000>}
box{<0,0,-0.635000><0.141421,0.035000,0.635000> rotate<0,44.997030,0> translate<38.000000,-1.535000,26.400000> }
cylinder{<0,0,0><0,0.035000,0>0.635000 translate<38.100000,-1.535000,25.100000>}
cylinder{<0,0,0><0,0.035000,0>0.635000 translate<38.100000,-1.535000,26.300000>}
box{<0,0,-0.635000><1.200000,0.035000,0.635000> rotate<0,90.000000,0> translate<38.100000,-1.535000,26.300000> }
cylinder{<0,0,0><0,0.035000,0>0.635000 translate<38.000000,-1.535000,9.900000>}
cylinder{<0,0,0><0,0.035000,0>0.635000 translate<38.600000,-1.535000,9.900000>}
box{<0,0,-0.635000><0.600000,0.035000,0.635000> rotate<0,0.000000,0> translate<38.000000,-1.535000,9.900000> }
cylinder{<0,0,0><0,0.035000,0>0.635000 translate<26.900000,0.000000,9.200000>}
cylinder{<0,0,0><0,0.035000,0>0.635000 translate<38.800000,0.000000,9.200000>}
box{<0,0,-0.635000><11.900000,0.035000,0.635000> rotate<0,0.000000,0> translate<26.900000,0.000000,9.200000> }
cylinder{<0,0,0><0,0.035000,0>0.635000 translate<38.500000,0.000000,9.200000>}
cylinder{<0,0,0><0,0.035000,0>0.635000 translate<38.800000,0.000000,9.200000>}
box{<0,0,-0.635000><0.300000,0.035000,0.635000> rotate<0,0.000000,0> translate<38.500000,0.000000,9.200000> }
cylinder{<0,0,0><0,0.035000,0>0.635000 translate<39.000000,0.000000,13.800000>}
cylinder{<0,0,0><0,0.035000,0>0.635000 translate<39.000000,0.000000,21.100000>}
box{<0,0,-0.635000><7.300000,0.035000,0.635000> rotate<0,90.000000,0> translate<39.000000,0.000000,21.100000> }
cylinder{<0,0,0><0,0.035000,0>0.635000 translate<38.100000,-1.535000,25.100000>}
cylinder{<0,0,0><0,0.035000,0>0.635000 translate<39.000000,-1.535000,24.200000>}
box{<0,0,-0.635000><1.272792,0.035000,0.635000> rotate<0,44.997030,0> translate<38.100000,-1.535000,25.100000> }
cylinder{<0,0,0><0,0.035000,0>0.635000 translate<39.000000,-1.535000,21.100000>}
cylinder{<0,0,0><0,0.035000,0>0.635000 translate<39.000000,-1.535000,24.200000>}
box{<0,0,-0.635000><3.100000,0.035000,0.635000> rotate<0,90.000000,0> translate<39.000000,-1.535000,24.200000> }
cylinder{<0,0,0><0,0.035000,0>0.635000 translate<36.400000,-1.535000,20.300000>}
cylinder{<0,0,0><0,0.035000,0>0.635000 translate<39.200000,-1.535000,17.500000>}
box{<0,0,-0.635000><3.959798,0.035000,0.635000> rotate<0,44.997030,0> translate<36.400000,-1.535000,20.300000> }
cylinder{<0,0,0><0,0.035000,0>0.635000 translate<39.200000,-1.535000,10.100000>}
cylinder{<0,0,0><0,0.035000,0>0.635000 translate<39.700000,-1.535000,10.100000>}
box{<0,0,-0.635000><0.500000,0.035000,0.635000> rotate<0,0.000000,0> translate<39.200000,-1.535000,10.100000> }
cylinder{<0,0,0><0,0.035000,0>0.635000 translate<39.700000,-1.535000,10.100000>}
cylinder{<0,0,0><0,0.035000,0>0.635000 translate<39.800000,-1.535000,10.000000>}
box{<0,0,-0.635000><0.141421,0.035000,0.635000> rotate<0,44.997030,0> translate<39.700000,-1.535000,10.100000> }
cylinder{<0,0,0><0,0.035000,0>0.635000 translate<39.200000,-1.535000,17.500000>}
cylinder{<0,0,0><0,0.035000,0>0.635000 translate<39.800000,-1.535000,16.200000>}
box{<0,0,-0.635000><1.431782,0.035000,0.635000> rotate<0,65.220555,0> translate<39.200000,-1.535000,17.500000> }
cylinder{<0,0,0><0,0.035000,0>0.635000 translate<39.800000,-1.535000,14.000000>}
cylinder{<0,0,0><0,0.035000,0>0.635000 translate<39.800000,-1.535000,16.200000>}
box{<0,0,-0.635000><2.200000,0.035000,0.635000> rotate<0,90.000000,0> translate<39.800000,-1.535000,16.200000> }
cylinder{<0,0,0><0,0.035000,0>0.635000 translate<38.600000,-1.535000,9.900000>}
cylinder{<0,0,0><0,0.035000,0>0.635000 translate<39.900000,-1.535000,9.900000>}
box{<0,0,-0.635000><1.300000,0.035000,0.635000> rotate<0,0.000000,0> translate<38.600000,-1.535000,9.900000> }
cylinder{<0,0,0><0,0.035000,0>0.635000 translate<39.800000,-1.535000,10.000000>}
cylinder{<0,0,0><0,0.035000,0>0.635000 translate<39.900000,-1.535000,9.900000>}
box{<0,0,-0.635000><0.141421,0.035000,0.635000> rotate<0,44.997030,0> translate<39.800000,-1.535000,10.000000> }
cylinder{<0,0,0><0,0.035000,0>0.635000 translate<39.800000,-1.535000,14.000000>}
cylinder{<0,0,0><0,0.035000,0>0.635000 translate<39.900000,-1.535000,13.900000>}
box{<0,0,-0.635000><0.141421,0.035000,0.635000> rotate<0,44.997030,0> translate<39.800000,-1.535000,14.000000> }
cylinder{<0,0,0><0,0.035000,0>0.635000 translate<33.400000,0.000000,22.900000>}
cylinder{<0,0,0><0,0.035000,0>0.635000 translate<40.194800,0.000000,22.900000>}
box{<0,0,-0.635000><6.794800,0.035000,0.635000> rotate<0,0.000000,0> translate<33.400000,0.000000,22.900000> }
cylinder{<0,0,0><0,0.035000,0>0.635000 translate<40.374400,0.000000,22.974400>}
cylinder{<0,0,0><0,0.035000,0>0.635000 translate<40.600000,0.000000,23.200000>}
box{<0,0,-0.635000><0.319047,0.035000,0.635000> rotate<0,-44.997030,0> translate<40.374400,0.000000,22.974400> }
cylinder{<0,0,0><0,0.035000,0>0.635000 translate<40.600000,0.000000,23.200000>}
cylinder{<0,0,0><0,0.035000,0>0.635000 translate<40.600000,0.000000,26.255100>}
box{<0,0,-0.635000><3.055100,0.035000,0.635000> rotate<0,90.000000,0> translate<40.600000,0.000000,26.255100> }
cylinder{<0,0,0><0,0.035000,0>0.635000 translate<40.600000,0.000000,23.200000>}
cylinder{<0,0,0><0,0.035000,0>0.635000 translate<41.025600,0.000000,22.774400>}
box{<0,0,-0.635000><0.601889,0.035000,0.635000> rotate<0,44.997030,0> translate<40.600000,0.000000,23.200000> }
cylinder{<0,0,0><0,0.035000,0>0.635000 translate<41.100000,0.000000,19.100000>}
cylinder{<0,0,0><0,0.035000,0>0.635000 translate<41.100000,0.000000,14.800000>}
box{<0,0,-0.635000><4.300000,0.035000,0.635000> rotate<0,-90.000000,0> translate<41.100000,0.000000,14.800000> }
cylinder{<0,0,0><0,0.035000,0>0.635000 translate<41.100000,0.000000,22.594800>}
cylinder{<0,0,0><0,0.035000,0>0.635000 translate<41.100000,0.000000,19.100000>}
box{<0,0,-0.635000><3.494800,0.035000,0.635000> rotate<0,-90.000000,0> translate<41.100000,0.000000,19.100000> }
cylinder{<0,0,0><0,0.035000,0>0.635000 translate<41.100000,0.000000,19.100000>}
cylinder{<0,0,0><0,0.035000,0>0.635000 translate<41.200000,0.000000,19.000000>}
box{<0,0,-0.635000><0.141421,0.035000,0.635000> rotate<0,44.997030,0> translate<41.100000,0.000000,19.100000> }
cylinder{<0,0,0><0,0.035000,0>0.635000 translate<38.600000,-1.535000,9.900000>}
cylinder{<0,0,0><0,0.035000,0>0.635000 translate<41.400000,-1.535000,7.100000>}
box{<0,0,-0.635000><3.959798,0.035000,0.635000> rotate<0,44.997030,0> translate<38.600000,-1.535000,9.900000> }
cylinder{<0,0,0><0,0.035000,0>0.635000 translate<39.000000,0.000000,13.800000>}
cylinder{<0,0,0><0,0.035000,0>0.635000 translate<41.700000,0.000000,11.100000>}
box{<0,0,-0.635000><3.818377,0.035000,0.635000> rotate<0,44.997030,0> translate<39.000000,0.000000,13.800000> }
cylinder{<0,0,0><0,0.035000,0>0.127000 translate<42.000000,-1.535000,22.200000>}
cylinder{<0,0,0><0,0.035000,0>0.127000 translate<42.255600,-1.535000,23.158500>}
box{<0,0,-0.127000><0.991995,0.035000,0.127000> rotate<0,-75.063629,0> translate<42.000000,-1.535000,22.200000> }
cylinder{<0,0,0><0,0.035000,0>0.127000 translate<42.255600,-1.535000,23.158500>}
cylinder{<0,0,0><0,0.035000,0>0.127000 translate<42.255600,-1.535000,24.555600>}
box{<0,0,-0.127000><1.397100,0.035000,0.127000> rotate<0,90.000000,0> translate<42.255600,-1.535000,24.555600> }
cylinder{<0,0,0><0,0.035000,0>0.127000 translate<42.255600,-1.535000,23.158500>}
cylinder{<0,0,0><0,0.035000,0>0.127000 translate<42.400000,-1.535000,23.700000>}
box{<0,0,-0.127000><0.560423,0.035000,0.127000> rotate<0,-75.063629,0> translate<42.255600,-1.535000,23.158500> }
cylinder{<0,0,0><0,0.035000,0>0.127000 translate<42.255600,-1.535000,24.555600>}
cylinder{<0,0,0><0,0.035000,0>0.127000 translate<42.500000,-1.535000,24.800000>}
box{<0,0,-0.127000><0.345634,0.035000,0.127000> rotate<0,-44.997030,0> translate<42.255600,-1.535000,24.555600> }
cylinder{<0,0,0><0,0.035000,0>0.127000 translate<32.100000,-1.535000,29.300000>}
cylinder{<0,0,0><0,0.035000,0>0.127000 translate<42.600000,-1.535000,29.300000>}
box{<0,0,-0.127000><10.500000,0.035000,0.127000> rotate<0,0.000000,0> translate<32.100000,-1.535000,29.300000> }
cylinder{<0,0,0><0,0.035000,0>0.127000 translate<42.700000,-1.535000,29.200000>}
cylinder{<0,0,0><0,0.035000,0>0.127000 translate<42.700000,-1.535000,27.000000>}
box{<0,0,-0.127000><2.200000,0.035000,0.127000> rotate<0,-90.000000,0> translate<42.700000,-1.535000,27.000000> }
cylinder{<0,0,0><0,0.035000,0>0.127000 translate<42.600000,-1.535000,29.300000>}
cylinder{<0,0,0><0,0.035000,0>0.127000 translate<42.700000,-1.535000,29.200000>}
box{<0,0,-0.127000><0.141421,0.035000,0.127000> rotate<0,44.997030,0> translate<42.600000,-1.535000,29.300000> }
cylinder{<0,0,0><0,0.035000,0>0.635000 translate<41.100000,0.000000,14.800000>}
cylinder{<0,0,0><0,0.035000,0>0.635000 translate<42.725600,0.000000,13.174400>}
box{<0,0,-0.635000><2.298946,0.035000,0.635000> rotate<0,44.997030,0> translate<41.100000,0.000000,14.800000> }
cylinder{<0,0,0><0,0.035000,0>0.635000 translate<42.900000,0.000000,7.100000>}
cylinder{<0,0,0><0,0.035000,0>0.635000 translate<42.900000,0.000000,7.000000>}
box{<0,0,-0.635000><0.100000,0.035000,0.635000> rotate<0,-90.000000,0> translate<42.900000,0.000000,7.000000> }
cylinder{<0,0,0><0,0.035000,0>0.635000 translate<41.400000,-1.535000,7.100000>}
cylinder{<0,0,0><0,0.035000,0>0.635000 translate<42.900000,-1.535000,7.100000>}
box{<0,0,-0.635000><1.500000,0.035000,0.635000> rotate<0,0.000000,0> translate<41.400000,-1.535000,7.100000> }
cylinder{<0,0,0><0,0.035000,0>0.127000 translate<42.900000,0.000000,16.600000>}
cylinder{<0,0,0><0,0.035000,0>0.127000 translate<42.900000,0.000000,16.200000>}
box{<0,0,-0.127000><0.400000,0.035000,0.127000> rotate<0,-90.000000,0> translate<42.900000,0.000000,16.200000> }
cylinder{<0,0,0><0,0.035000,0>0.254000 translate<41.030000,-1.535000,4.400000>}
cylinder{<0,0,0><0,0.035000,0>0.254000 translate<43.000000,-1.535000,4.400000>}
box{<0,0,-0.254000><1.970000,0.035000,0.254000> rotate<0,0.000000,0> translate<41.030000,-1.535000,4.400000> }
cylinder{<0,0,0><0,0.035000,0>0.127000 translate<42.500000,-1.535000,24.800000>}
cylinder{<0,0,0><0,0.035000,0>0.127000 translate<43.100000,-1.535000,25.400000>}
box{<0,0,-0.127000><0.848528,0.035000,0.127000> rotate<0,-44.997030,0> translate<42.500000,-1.535000,24.800000> }
cylinder{<0,0,0><0,0.035000,0>0.127000 translate<42.900000,0.000000,16.200000>}
cylinder{<0,0,0><0,0.035000,0>0.127000 translate<43.700000,0.000000,16.200000>}
box{<0,0,-0.127000><0.800000,0.035000,0.127000> rotate<0,0.000000,0> translate<42.900000,0.000000,16.200000> }
cylinder{<0,0,0><0,0.035000,0>0.635000 translate<41.200000,0.000000,19.000000>}
cylinder{<0,0,0><0,0.035000,0>0.635000 translate<44.200000,0.000000,19.000000>}
box{<0,0,-0.635000><3.000000,0.035000,0.635000> rotate<0,0.000000,0> translate<41.200000,0.000000,19.000000> }
cylinder{<0,0,0><0,0.035000,0>0.635000 translate<39.900000,-1.535000,13.900000>}
cylinder{<0,0,0><0,0.035000,0>0.635000 translate<44.400000,-1.535000,13.900000>}
box{<0,0,-0.635000><4.500000,0.035000,0.635000> rotate<0,0.000000,0> translate<39.900000,-1.535000,13.900000> }
cylinder{<0,0,0><0,0.035000,0>0.127000 translate<43.700000,0.000000,16.200000>}
cylinder{<0,0,0><0,0.035000,0>0.127000 translate<44.400000,0.000000,15.400000>}
box{<0,0,-0.127000><1.063015,0.035000,0.127000> rotate<0,48.810853,0> translate<43.700000,0.000000,16.200000> }
cylinder{<0,0,0><0,0.035000,0>0.127000 translate<44.200000,0.000000,16.200000>}
cylinder{<0,0,0><0,0.035000,0>0.127000 translate<44.400000,0.000000,15.400000>}
box{<0,0,-0.127000><0.824621,0.035000,0.127000> rotate<0,75.958743,0> translate<44.200000,0.000000,16.200000> }
cylinder{<0,0,0><0,0.035000,0>0.635000 translate<42.900000,0.000000,7.000000>}
cylinder{<0,0,0><0,0.035000,0>0.635000 translate<44.600000,0.000000,5.300000>}
box{<0,0,-0.635000><2.404163,0.035000,0.635000> rotate<0,44.997030,0> translate<42.900000,0.000000,7.000000> }
cylinder{<0,0,0><0,0.035000,0>0.635000 translate<38.800000,0.000000,9.200000>}
cylinder{<0,0,0><0,0.035000,0>0.635000 translate<44.600000,0.000000,9.200000>}
box{<0,0,-0.635000><5.800000,0.035000,0.635000> rotate<0,0.000000,0> translate<38.800000,0.000000,9.200000> }
cylinder{<0,0,0><0,0.035000,0>0.635000 translate<44.600000,-1.535000,9.200000>}
cylinder{<0,0,0><0,0.035000,0>0.635000 translate<44.600000,-1.535000,10.900000>}
box{<0,0,-0.635000><1.700000,0.035000,0.635000> rotate<0,90.000000,0> translate<44.600000,-1.535000,10.900000> }
cylinder{<0,0,0><0,0.035000,0>0.635000 translate<44.700000,0.000000,5.200000>}
cylinder{<0,0,0><0,0.035000,0>0.635000 translate<44.700000,0.000000,3.300000>}
box{<0,0,-0.635000><1.900000,0.035000,0.635000> rotate<0,-90.000000,0> translate<44.700000,0.000000,3.300000> }
cylinder{<0,0,0><0,0.035000,0>0.635000 translate<44.600000,0.000000,5.300000>}
cylinder{<0,0,0><0,0.035000,0>0.635000 translate<44.700000,0.000000,5.200000>}
box{<0,0,-0.635000><0.141421,0.035000,0.635000> rotate<0,44.997030,0> translate<44.600000,0.000000,5.300000> }
cylinder{<0,0,0><0,0.035000,0>0.635000 translate<44.600000,-1.535000,10.900000>}
cylinder{<0,0,0><0,0.035000,0>0.635000 translate<44.900000,-1.535000,11.200000>}
box{<0,0,-0.635000><0.424264,0.035000,0.635000> rotate<0,-44.997030,0> translate<44.600000,-1.535000,10.900000> }
cylinder{<0,0,0><0,0.035000,0>0.635000 translate<44.400000,-1.535000,13.900000>}
cylinder{<0,0,0><0,0.035000,0>0.635000 translate<44.900000,-1.535000,13.900000>}
box{<0,0,-0.635000><0.500000,0.035000,0.635000> rotate<0,0.000000,0> translate<44.400000,-1.535000,13.900000> }
cylinder{<0,0,0><0,0.035000,0>0.635000 translate<44.900000,-1.535000,11.200000>}
cylinder{<0,0,0><0,0.035000,0>0.635000 translate<44.900000,-1.535000,13.900000>}
box{<0,0,-0.635000><2.700000,0.035000,0.635000> rotate<0,90.000000,0> translate<44.900000,-1.535000,13.900000> }
cylinder{<0,0,0><0,0.035000,0>0.635000 translate<44.700000,0.000000,3.300000>}
cylinder{<0,0,0><0,0.035000,0>0.635000 translate<45.600000,0.000000,2.400000>}
box{<0,0,-0.635000><1.272792,0.035000,0.635000> rotate<0,44.997030,0> translate<44.700000,0.000000,3.300000> }
cylinder{<0,0,0><0,0.035000,0>0.635000 translate<41.700000,0.000000,11.100000>}
cylinder{<0,0,0><0,0.035000,0>0.635000 translate<45.900000,0.000000,11.100000>}
box{<0,0,-0.635000><4.200000,0.035000,0.635000> rotate<0,0.000000,0> translate<41.700000,0.000000,11.100000> }
cylinder{<0,0,0><0,0.035000,0>0.635000 translate<46.000000,0.000000,11.000000>}
cylinder{<0,0,0><0,0.035000,0>0.635000 translate<46.000000,0.000000,10.900000>}
box{<0,0,-0.635000><0.100000,0.035000,0.635000> rotate<0,-90.000000,0> translate<46.000000,0.000000,10.900000> }
cylinder{<0,0,0><0,0.035000,0>0.635000 translate<45.900000,0.000000,11.100000>}
cylinder{<0,0,0><0,0.035000,0>0.635000 translate<46.000000,0.000000,11.000000>}
box{<0,0,-0.635000><0.141421,0.035000,0.635000> rotate<0,44.997030,0> translate<45.900000,0.000000,11.100000> }
cylinder{<0,0,0><0,0.035000,0>0.635000 translate<46.000000,0.000000,10.900000>}
cylinder{<0,0,0><0,0.035000,0>0.635000 translate<46.100000,0.000000,10.800000>}
box{<0,0,-0.635000><0.141421,0.035000,0.635000> rotate<0,44.997030,0> translate<46.000000,0.000000,10.900000> }
cylinder{<0,0,0><0,0.035000,0>0.635000 translate<46.100000,0.000000,10.800000>}
cylinder{<0,0,0><0,0.035000,0>0.635000 translate<46.200000,0.000000,10.800000>}
box{<0,0,-0.635000><0.100000,0.035000,0.635000> rotate<0,0.000000,0> translate<46.100000,0.000000,10.800000> }
cylinder{<0,0,0><0,0.035000,0>0.635000 translate<44.900000,-1.535000,13.900000>}
cylinder{<0,0,0><0,0.035000,0>0.635000 translate<46.200000,-1.535000,13.900000>}
box{<0,0,-0.635000><1.300000,0.035000,0.635000> rotate<0,0.000000,0> translate<44.900000,-1.535000,13.900000> }
cylinder{<0,0,0><0,0.035000,0>0.635000 translate<46.300000,0.000000,10.700000>}
cylinder{<0,0,0><0,0.035000,0>0.635000 translate<46.300000,0.000000,10.600000>}
box{<0,0,-0.635000><0.100000,0.035000,0.635000> rotate<0,-90.000000,0> translate<46.300000,0.000000,10.600000> }
cylinder{<0,0,0><0,0.035000,0>0.635000 translate<46.200000,0.000000,10.800000>}
cylinder{<0,0,0><0,0.035000,0>0.635000 translate<46.300000,0.000000,10.700000>}
box{<0,0,-0.635000><0.141421,0.035000,0.635000> rotate<0,44.997030,0> translate<46.200000,0.000000,10.800000> }
cylinder{<0,0,0><0,0.035000,0>0.635000 translate<42.905200,0.000000,13.100000>}
cylinder{<0,0,0><0,0.035000,0>0.635000 translate<46.394800,0.000000,13.100000>}
box{<0,0,-0.635000><3.489600,0.035000,0.635000> rotate<0,0.000000,0> translate<42.905200,0.000000,13.100000> }
cylinder{<0,0,0><0,0.035000,0>0.635000 translate<46.300000,0.000000,10.600000>}
cylinder{<0,0,0><0,0.035000,0>0.635000 translate<46.400000,0.000000,10.500000>}
box{<0,0,-0.635000><0.141421,0.035000,0.635000> rotate<0,44.997030,0> translate<46.300000,0.000000,10.600000> }
cylinder{<0,0,0><0,0.035000,0>0.127000 translate<44.948800,-1.535000,24.139400>}
cylinder{<0,0,0><0,0.035000,0>0.127000 translate<46.400000,-1.535000,24.300000>}
box{<0,0,-0.127000><1.460060,0.035000,0.127000> rotate<0,-6.314640,0> translate<44.948800,-1.535000,24.139400> }
cylinder{<0,0,0><0,0.035000,0>0.635000 translate<46.400000,0.000000,10.500000>}
cylinder{<0,0,0><0,0.035000,0>0.635000 translate<46.500000,0.000000,10.500000>}
box{<0,0,-0.635000><0.100000,0.035000,0.635000> rotate<0,0.000000,0> translate<46.400000,0.000000,10.500000> }
cylinder{<0,0,0><0,0.035000,0>0.635000 translate<46.500000,0.000000,10.500000>}
cylinder{<0,0,0><0,0.035000,0>0.635000 translate<46.600000,0.000000,10.400000>}
box{<0,0,-0.635000><0.141421,0.035000,0.635000> rotate<0,44.997030,0> translate<46.500000,0.000000,10.500000> }
cylinder{<0,0,0><0,0.035000,0>0.635000 translate<46.200000,-1.535000,13.900000>}
cylinder{<0,0,0><0,0.035000,0>0.635000 translate<46.800000,-1.535000,15.200000>}
box{<0,0,-0.635000><1.431782,0.035000,0.635000> rotate<0,-65.220555,0> translate<46.200000,-1.535000,13.900000> }
cylinder{<0,0,0><0,0.035000,0>0.254000 translate<43.000000,-1.535000,4.400000>}
cylinder{<0,0,0><0,0.035000,0>0.254000 translate<46.900000,-1.535000,4.400000>}
box{<0,0,-0.254000><3.900000,0.035000,0.254000> rotate<0,0.000000,0> translate<43.000000,-1.535000,4.400000> }
cylinder{<0,0,0><0,0.035000,0>0.635000 translate<47.200000,0.000000,15.400000>}
cylinder{<0,0,0><0,0.035000,0>0.635000 translate<47.200000,0.000000,16.200000>}
box{<0,0,-0.635000><0.800000,0.035000,0.635000> rotate<0,90.000000,0> translate<47.200000,0.000000,16.200000> }
cylinder{<0,0,0><0,0.035000,0>0.635000 translate<47.200000,0.000000,16.200000>}
cylinder{<0,0,0><0,0.035000,0>0.635000 translate<47.200000,0.000000,22.700000>}
box{<0,0,-0.635000><6.500000,0.035000,0.635000> rotate<0,90.000000,0> translate<47.200000,0.000000,22.700000> }
cylinder{<0,0,0><0,0.035000,0>0.635000 translate<47.200000,0.000000,22.700000>}
cylinder{<0,0,0><0,0.035000,0>0.635000 translate<47.300000,0.000000,22.800000>}
box{<0,0,-0.635000><0.141421,0.035000,0.635000> rotate<0,-44.997030,0> translate<47.200000,0.000000,22.700000> }
cylinder{<0,0,0><0,0.035000,0>0.635000 translate<47.300000,0.000000,22.800000>}
cylinder{<0,0,0><0,0.035000,0>0.635000 translate<47.700000,0.000000,22.800000>}
box{<0,0,-0.635000><0.400000,0.035000,0.635000> rotate<0,0.000000,0> translate<47.300000,0.000000,22.800000> }
cylinder{<0,0,0><0,0.035000,0>0.635000 translate<47.700000,0.000000,22.800000>}
cylinder{<0,0,0><0,0.035000,0>0.635000 translate<47.700000,0.000000,28.600000>}
box{<0,0,-0.635000><5.800000,0.035000,0.635000> rotate<0,90.000000,0> translate<47.700000,0.000000,28.600000> }
cylinder{<0,0,0><0,0.035000,0>0.635000 translate<44.948800,0.000000,28.660600>}
cylinder{<0,0,0><0,0.035000,0>0.635000 translate<47.700000,0.000000,28.700000>}
box{<0,0,-0.635000><2.751482,0.035000,0.635000> rotate<0,-0.820424,0> translate<44.948800,0.000000,28.660600> }
cylinder{<0,0,0><0,0.035000,0>0.635000 translate<47.700000,0.000000,28.600000>}
cylinder{<0,0,0><0,0.035000,0>0.635000 translate<47.700000,0.000000,28.700000>}
box{<0,0,-0.635000><0.100000,0.035000,0.635000> rotate<0,90.000000,0> translate<47.700000,0.000000,28.700000> }
cylinder{<0,0,0><0,0.035000,0>0.635000 translate<46.200000,-1.535000,21.300000>}
cylinder{<0,0,0><0,0.035000,0>0.635000 translate<48.000000,-1.535000,21.300000>}
box{<0,0,-0.635000><1.800000,0.035000,0.635000> rotate<0,0.000000,0> translate<46.200000,-1.535000,21.300000> }
cylinder{<0,0,0><0,0.035000,0>0.254000 translate<46.900000,-1.535000,4.400000>}
cylinder{<0,0,0><0,0.035000,0>0.254000 translate<48.400000,-1.535000,4.730000>}
box{<0,0,-0.254000><1.535871,0.035000,0.254000> rotate<0,-12.406600,0> translate<46.900000,-1.535000,4.400000> }
cylinder{<0,0,0><0,0.035000,0>0.635000 translate<46.200000,-1.535000,13.900000>}
cylinder{<0,0,0><0,0.035000,0>0.635000 translate<49.200000,-1.535000,13.900000>}
box{<0,0,-0.635000><3.000000,0.035000,0.635000> rotate<0,0.000000,0> translate<46.200000,-1.535000,13.900000> }
cylinder{<0,0,0><0,0.035000,0>0.635000 translate<47.200000,0.000000,15.400000>}
cylinder{<0,0,0><0,0.035000,0>0.635000 translate<49.600000,0.000000,15.400000>}
box{<0,0,-0.635000><2.400000,0.035000,0.635000> rotate<0,0.000000,0> translate<47.200000,0.000000,15.400000> }
cylinder{<0,0,0><0,0.035000,0>0.127000 translate<49.800000,0.000000,19.000000>}
cylinder{<0,0,0><0,0.035000,0>0.127000 translate<49.800000,0.000000,20.500000>}
box{<0,0,-0.127000><1.500000,0.035000,0.127000> rotate<0,90.000000,0> translate<49.800000,0.000000,20.500000> }
cylinder{<0,0,0><0,0.035000,0>0.635000 translate<49.800000,0.000000,19.000000>}
cylinder{<0,0,0><0,0.035000,0>0.635000 translate<49.800000,0.000000,22.488200>}
box{<0,0,-0.635000><3.488200,0.035000,0.635000> rotate<0,90.000000,0> translate<49.800000,0.000000,22.488200> }
cylinder{<0,0,0><0,0.035000,0>0.254000 translate<46.400000,-1.535000,24.300000>}
cylinder{<0,0,0><0,0.035000,0>0.254000 translate<50.000000,-1.535000,24.300000>}
box{<0,0,-0.254000><3.600000,0.035000,0.254000> rotate<0,0.000000,0> translate<46.400000,-1.535000,24.300000> }
cylinder{<0,0,0><0,0.035000,0>0.635000 translate<49.200000,-1.535000,13.900000>}
cylinder{<0,0,0><0,0.035000,0>0.635000 translate<50.300000,-1.535000,15.000000>}
box{<0,0,-0.635000><1.555635,0.035000,0.635000> rotate<0,-44.997030,0> translate<49.200000,-1.535000,13.900000> }
cylinder{<0,0,0><0,0.035000,0>0.635000 translate<49.600000,0.000000,15.400000>}
cylinder{<0,0,0><0,0.035000,0>0.635000 translate<50.300000,0.000000,15.000000>}
box{<0,0,-0.635000><0.806226,0.035000,0.635000> rotate<0,29.742918,0> translate<49.600000,0.000000,15.400000> }
cylinder{<0,0,0><0,0.035000,0>0.635000 translate<46.574400,0.000000,13.025600>}
cylinder{<0,0,0><0,0.035000,0>0.635000 translate<50.800000,0.000000,8.700000>}
box{<0,0,-0.635000><6.047025,0.035000,0.635000> rotate<0,45.666987,0> translate<46.574400,0.000000,13.025600> }
cylinder{<0,0,0><0,0.035000,0>0.635000 translate<48.300000,0.000000,7.300000>}
cylinder{<0,0,0><0,0.035000,0>0.635000 translate<51.200000,0.000000,7.300000>}
box{<0,0,-0.635000><2.900000,0.035000,0.635000> rotate<0,0.000000,0> translate<48.300000,0.000000,7.300000> }
cylinder{<0,0,0><0,0.035000,0>0.635000 translate<51.200000,0.000000,8.300000>}
cylinder{<0,0,0><0,0.035000,0>0.635000 translate<51.200000,0.000000,7.400000>}
box{<0,0,-0.635000><0.900000,0.035000,0.635000> rotate<0,-90.000000,0> translate<51.200000,0.000000,7.400000> }
cylinder{<0,0,0><0,0.035000,0>0.635000 translate<50.800000,0.000000,8.700000>}
cylinder{<0,0,0><0,0.035000,0>0.635000 translate<51.200000,0.000000,8.300000>}
box{<0,0,-0.635000><0.565685,0.035000,0.635000> rotate<0,44.997030,0> translate<50.800000,0.000000,8.700000> }
cylinder{<0,0,0><0,0.035000,0>0.635000 translate<49.800000,0.000000,22.488200>}
cylinder{<0,0,0><0,0.035000,0>0.635000 translate<51.451200,0.000000,24.139400>}
box{<0,0,-0.635000><2.335149,0.035000,0.635000> rotate<0,-44.997030,0> translate<49.800000,0.000000,22.488200> }
cylinder{<0,0,0><0,0.035000,0>0.127000 translate<50.000000,-1.535000,24.300000>}
cylinder{<0,0,0><0,0.035000,0>0.127000 translate<51.451200,-1.535000,24.139400>}
box{<0,0,-0.127000><1.460060,0.035000,0.127000> rotate<0,6.314640,0> translate<50.000000,-1.535000,24.300000> }
cylinder{<0,0,0><0,0.035000,0>0.635000 translate<47.700000,0.000000,28.700000>}
cylinder{<0,0,0><0,0.035000,0>0.635000 translate<51.451200,0.000000,28.660600>}
box{<0,0,-0.635000><3.751407,0.035000,0.635000> rotate<0,0.601733,0> translate<47.700000,0.000000,28.700000> }
cylinder{<0,0,0><0,0.035000,0>0.635000 translate<48.000000,-1.535000,21.300000>}
cylinder{<0,0,0><0,0.035000,0>0.635000 translate<51.700000,-1.535000,21.300000>}
box{<0,0,-0.635000><3.700000,0.035000,0.635000> rotate<0,0.000000,0> translate<48.000000,-1.535000,21.300000> }
cylinder{<0,0,0><0,0.035000,0>0.635000 translate<50.300000,-1.535000,15.000000>}
cylinder{<0,0,0><0,0.035000,0>0.635000 translate<51.800000,-1.535000,15.000000>}
box{<0,0,-0.635000><1.500000,0.035000,0.635000> rotate<0,0.000000,0> translate<50.300000,-1.535000,15.000000> }
cylinder{<0,0,0><0,0.035000,0>0.635000 translate<51.800000,-1.535000,15.000000>}
cylinder{<0,0,0><0,0.035000,0>0.635000 translate<51.820000,-1.535000,15.020000>}
box{<0,0,-0.635000><0.028284,0.035000,0.635000> rotate<0,-44.997030,0> translate<51.800000,-1.535000,15.000000> }
cylinder{<0,0,0><0,0.035000,0>0.635000 translate<46.600000,-1.535000,10.400000>}
cylinder{<0,0,0><0,0.035000,0>0.635000 translate<52.200000,-1.535000,10.400000>}
box{<0,0,-0.635000><5.600000,0.035000,0.635000> rotate<0,0.000000,0> translate<46.600000,-1.535000,10.400000> }
cylinder{<0,0,0><0,0.035000,0>0.635000 translate<51.700000,0.000000,21.300000>}
cylinder{<0,0,0><0,0.035000,0>0.635000 translate<52.200000,0.000000,21.300000>}
box{<0,0,-0.635000><0.500000,0.035000,0.635000> rotate<0,0.000000,0> translate<51.700000,0.000000,21.300000> }
cylinder{<0,0,0><0,0.035000,0>0.635000 translate<52.200000,0.000000,10.400000>}
cylinder{<0,0,0><0,0.035000,0>0.635000 translate<52.200000,0.000000,21.300000>}
box{<0,0,-0.635000><10.900000,0.035000,0.635000> rotate<0,90.000000,0> translate<52.200000,0.000000,21.300000> }
cylinder{<0,0,0><0,0.035000,0>0.635000 translate<52.200000,0.000000,21.300000>}
cylinder{<0,0,0><0,0.035000,0>0.635000 translate<52.494800,0.000000,21.300000>}
box{<0,0,-0.635000><0.294800,0.035000,0.635000> rotate<0,0.000000,0> translate<52.200000,0.000000,21.300000> }
cylinder{<0,0,0><0,0.035000,0>0.635000 translate<45.600000,0.000000,2.400000>}
cylinder{<0,0,0><0,0.035000,0>0.635000 translate<53.500000,0.000000,2.400000>}
box{<0,0,-0.635000><7.900000,0.035000,0.635000> rotate<0,0.000000,0> translate<45.600000,0.000000,2.400000> }
cylinder{<0,0,0><0,0.035000,0>0.127000 translate<42.700000,-1.535000,27.000000>}
cylinder{<0,0,0><0,0.035000,0>0.127000 translate<54.000000,-1.535000,27.000000>}
box{<0,0,-0.127000><11.300000,0.035000,0.127000> rotate<0,0.000000,0> translate<42.700000,-1.535000,27.000000> }
cylinder{<0,0,0><0,0.035000,0>0.127000 translate<54.000000,-1.535000,27.000000>}
cylinder{<0,0,0><0,0.035000,0>0.127000 translate<54.330000,-1.535000,27.600000>}
box{<0,0,-0.127000><0.684763,0.035000,0.127000> rotate<0,-61.185168,0> translate<54.000000,-1.535000,27.000000> }
cylinder{<0,0,0><0,0.035000,0>0.635000 translate<52.674400,0.000000,21.374400>}
cylinder{<0,0,0><0,0.035000,0>0.635000 translate<54.600000,0.000000,23.300000>}
box{<0,0,-0.635000><2.723210,0.035000,0.635000> rotate<0,-44.997030,0> translate<52.674400,0.000000,21.374400> }
cylinder{<0,0,0><0,0.035000,0>0.127000 translate<43.100000,-1.535000,25.400000>}
cylinder{<0,0,0><0,0.035000,0>0.127000 translate<54.900000,-1.535000,25.400000>}
box{<0,0,-0.127000><11.800000,0.035000,0.127000> rotate<0,0.000000,0> translate<43.100000,-1.535000,25.400000> }
cylinder{<0,0,0><0,0.035000,0>0.127000 translate<54.900000,-1.535000,25.400000>}
cylinder{<0,0,0><0,0.035000,0>0.127000 translate<56.870000,-1.535000,27.370000>}
box{<0,0,-0.127000><2.786001,0.035000,0.127000> rotate<0,-44.997030,0> translate<54.900000,-1.535000,25.400000> }
cylinder{<0,0,0><0,0.035000,0>0.127000 translate<56.870000,-1.535000,27.370000>}
cylinder{<0,0,0><0,0.035000,0>0.127000 translate<56.870000,-1.535000,27.600000>}
box{<0,0,-0.127000><0.230000,0.035000,0.127000> rotate<0,90.000000,0> translate<56.870000,-1.535000,27.600000> }
cylinder{<0,0,0><0,0.035000,0>0.635000 translate<54.600000,0.000000,23.300000>}
cylinder{<0,0,0><0,0.035000,0>0.635000 translate<56.903400,0.000000,23.300000>}
box{<0,0,-0.635000><2.303400,0.035000,0.635000> rotate<0,0.000000,0> translate<54.600000,0.000000,23.300000> }
cylinder{<0,0,0><0,0.035000,0>0.635000 translate<53.500000,0.000000,2.400000>}
cylinder{<0,0,0><0,0.035000,0>0.635000 translate<57.000000,0.000000,5.900000>}
box{<0,0,-0.635000><4.949747,0.035000,0.635000> rotate<0,-44.997030,0> translate<53.500000,0.000000,2.400000> }
cylinder{<0,0,0><0,0.035000,0>0.635000 translate<57.000000,0.000000,5.900000>}
cylinder{<0,0,0><0,0.035000,0>0.635000 translate<57.000000,0.000000,11.060000>}
box{<0,0,-0.635000><5.160000,0.035000,0.635000> rotate<0,90.000000,0> translate<57.000000,0.000000,11.060000> }
cylinder{<0,0,0><0,0.035000,0>0.635000 translate<51.820000,-1.535000,15.020000>}
cylinder{<0,0,0><0,0.035000,0>0.635000 translate<57.000000,-1.535000,15.020000>}
box{<0,0,-0.635000><5.180000,0.035000,0.635000> rotate<0,0.000000,0> translate<51.820000,-1.535000,15.020000> }
cylinder{<0,0,0><0,0.035000,0>0.635000 translate<57.000000,-1.535000,15.020000>}
cylinder{<0,0,0><0,0.035000,0>0.635000 translate<57.000000,-1.535000,17.700000>}
box{<0,0,-0.635000><2.680000,0.035000,0.635000> rotate<0,90.000000,0> translate<57.000000,-1.535000,17.700000> }
cylinder{<0,0,0><0,0.035000,0>0.635000 translate<57.000000,-1.535000,17.700000>}
cylinder{<0,0,0><0,0.035000,0>0.635000 translate<57.000000,-1.535000,18.980000>}
box{<0,0,-0.635000><1.280000,0.035000,0.635000> rotate<0,90.000000,0> translate<57.000000,-1.535000,18.980000> }
object{ARC(0.254000,1.270000,45.000000,90.000000,0.036000) translate<46.394800,0.000000,12.846000>}
object{ARC(0.254000,1.270000,225.000000,270.000000,0.036000) translate<42.905200,0.000000,13.354000>}
object{ARC(0.254000,1.270000,0.000000,45.000000,0.036000) translate<40.846000,0.000000,22.594800>}
object{ARC(0.204900,1.270000,0.000000,45.000000,0.036000) translate<40.395100,0.000000,26.255100>}
object{ARC(0.254000,1.270000,270.000000,315.000000,0.036000) translate<40.194800,0.000000,23.154000>}
object{ARC(0.254000,1.270000,270.000000,315.000000,0.036000) translate<52.494800,0.000000,21.554000>}
object{ARC(0.486900,1.270000,172.518878,217.524113,0.036000) translate<57.386200,0.000000,23.236600>}
//Text
//Rect
union{
texture{col_pds}
}
texture{col_wrs}
}
#end
#if(pcb_polygons=on)
union{
//Polygons
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<-17.288800,-1.535000,0.711100>}
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<-17.288800,-1.535000,29.288800>}
box{<0,0,-0.203200><28.577700,0.035000,0.203200> rotate<0,90.000000,0> translate<-17.288800,-1.535000,29.288800> }
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<-17.288800,-1.535000,0.711100>}
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<0.032300,-1.535000,0.711100>}
box{<0,0,-0.203200><17.321100,0.035000,0.203200> rotate<0,0.000000,0> translate<-17.288800,-1.535000,0.711100> }
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<-17.288800,-1.535000,0.812800>}
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<-0.009900,-1.535000,0.812800>}
box{<0,0,-0.203200><17.278900,0.035000,0.203200> rotate<0,0.000000,0> translate<-17.288800,-1.535000,0.812800> }
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<-17.288800,-1.535000,1.219200>}
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<-0.178200,-1.535000,1.219200>}
box{<0,0,-0.203200><17.110600,0.035000,0.203200> rotate<0,0.000000,0> translate<-17.288800,-1.535000,1.219200> }
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<-17.288800,-1.535000,1.625600>}
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<-0.311100,-1.535000,1.625600>}
box{<0,0,-0.203200><16.977700,0.035000,0.203200> rotate<0,0.000000,0> translate<-17.288800,-1.535000,1.625600> }
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<-17.288800,-1.535000,2.032000>}
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<-0.311100,-1.535000,2.032000>}
box{<0,0,-0.203200><16.977700,0.035000,0.203200> rotate<0,0.000000,0> translate<-17.288800,-1.535000,2.032000> }
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<-17.288800,-1.535000,2.438400>}
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<-0.311100,-1.535000,2.438400>}
box{<0,0,-0.203200><16.977700,0.035000,0.203200> rotate<0,0.000000,0> translate<-17.288800,-1.535000,2.438400> }
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<-17.288800,-1.535000,2.844800>}
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<-0.151600,-1.535000,2.844800>}
box{<0,0,-0.203200><17.137200,0.035000,0.203200> rotate<0,0.000000,0> translate<-17.288800,-1.535000,2.844800> }
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<-17.288800,-1.535000,3.251200>}
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<0.016700,-1.535000,3.251200>}
box{<0,0,-0.203200><17.305500,0.035000,0.203200> rotate<0,0.000000,0> translate<-17.288800,-1.535000,3.251200> }
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<-17.288800,-1.535000,3.657600>}
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<0.389100,-1.535000,3.657600>}
box{<0,0,-0.203200><17.677900,0.035000,0.203200> rotate<0,0.000000,0> translate<-17.288800,-1.535000,3.657600> }
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<-17.288800,-1.535000,4.064000>}
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<0.943600,-1.535000,4.064000>}
box{<0,0,-0.203200><18.232400,0.035000,0.203200> rotate<0,0.000000,0> translate<-17.288800,-1.535000,4.064000> }
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<-17.288800,-1.535000,4.470400>}
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<10.054800,-1.535000,4.470400>}
box{<0,0,-0.203200><27.343600,0.035000,0.203200> rotate<0,0.000000,0> translate<-17.288800,-1.535000,4.470400> }
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<-17.288800,-1.535000,4.876800>}
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<9.993600,-1.535000,4.876800>}
box{<0,0,-0.203200><27.282400,0.035000,0.203200> rotate<0,0.000000,0> translate<-17.288800,-1.535000,4.876800> }
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<-17.288800,-1.535000,5.283200>}
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<9.993600,-1.535000,5.283200>}
box{<0,0,-0.203200><27.282400,0.035000,0.203200> rotate<0,0.000000,0> translate<-17.288800,-1.535000,5.283200> }
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<-17.288800,-1.535000,5.689600>}
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<3.866000,-1.535000,5.689600>}
box{<0,0,-0.203200><21.154800,0.035000,0.203200> rotate<0,0.000000,0> translate<-17.288800,-1.535000,5.689600> }
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<-17.288800,-1.535000,6.096000>}
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<3.360000,-1.535000,6.096000>}
box{<0,0,-0.203200><20.648800,0.035000,0.203200> rotate<0,0.000000,0> translate<-17.288800,-1.535000,6.096000> }
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<-17.288800,-1.535000,6.502400>}
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<3.261800,-1.535000,6.502400>}
box{<0,0,-0.203200><20.550600,0.035000,0.203200> rotate<0,0.000000,0> translate<-17.288800,-1.535000,6.502400> }
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<-17.288800,-1.535000,6.908800>}
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<3.362100,-1.535000,6.908800>}
box{<0,0,-0.203200><20.650900,0.035000,0.203200> rotate<0,0.000000,0> translate<-17.288800,-1.535000,6.908800> }
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<-17.288800,-1.535000,7.315200>}
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<3.878000,-1.535000,7.315200>}
box{<0,0,-0.203200><21.166800,0.035000,0.203200> rotate<0,0.000000,0> translate<-17.288800,-1.535000,7.315200> }
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<-17.288800,-1.535000,7.721600>}
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<20.613600,-1.535000,7.721600>}
box{<0,0,-0.203200><37.902400,0.035000,0.203200> rotate<0,0.000000,0> translate<-17.288800,-1.535000,7.721600> }
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<-17.288800,-1.535000,8.128000>}
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<20.613600,-1.535000,8.128000>}
box{<0,0,-0.203200><37.902400,0.035000,0.203200> rotate<0,0.000000,0> translate<-17.288800,-1.535000,8.128000> }
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<-17.288800,-1.535000,8.534400>}
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<15.611200,-1.535000,8.534400>}
box{<0,0,-0.203200><32.900000,0.035000,0.203200> rotate<0,0.000000,0> translate<-17.288800,-1.535000,8.534400> }
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<-17.288800,-1.535000,8.940800>}
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<15.366600,-1.535000,8.940800>}
box{<0,0,-0.203200><32.655400,0.035000,0.203200> rotate<0,0.000000,0> translate<-17.288800,-1.535000,8.940800> }
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<-17.288800,-1.535000,9.347200>}
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<14.493600,-1.535000,9.347200>}
box{<0,0,-0.203200><31.782400,0.035000,0.203200> rotate<0,0.000000,0> translate<-17.288800,-1.535000,9.347200> }
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<-17.288800,-1.535000,9.753600>}
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<14.493600,-1.535000,9.753600>}
box{<0,0,-0.203200><31.782400,0.035000,0.203200> rotate<0,0.000000,0> translate<-17.288800,-1.535000,9.753600> }
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<-17.288800,-1.535000,10.160000>}
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<9.107600,-1.535000,10.160000>}
box{<0,0,-0.203200><26.396400,0.035000,0.203200> rotate<0,0.000000,0> translate<-17.288800,-1.535000,10.160000> }
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<-17.288800,-1.535000,10.566400>}
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<8.701200,-1.535000,10.566400>}
box{<0,0,-0.203200><25.990000,0.035000,0.203200> rotate<0,0.000000,0> translate<-17.288800,-1.535000,10.566400> }
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<-17.288800,-1.535000,10.972800>}
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<8.561600,-1.535000,10.972800>}
box{<0,0,-0.203200><25.850400,0.035000,0.203200> rotate<0,0.000000,0> translate<-17.288800,-1.535000,10.972800> }
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<-17.288800,-1.535000,11.379200>}
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<-1.097600,-1.535000,11.379200>}
box{<0,0,-0.203200><16.191200,0.035000,0.203200> rotate<0,0.000000,0> translate<-17.288800,-1.535000,11.379200> }
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<-17.288800,-1.535000,11.785600>}
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<-1.307900,-1.535000,11.785600>}
box{<0,0,-0.203200><15.980900,0.035000,0.203200> rotate<0,0.000000,0> translate<-17.288800,-1.535000,11.785600> }
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<-17.288800,-1.535000,12.192000>}
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<-1.307900,-1.535000,12.192000>}
box{<0,0,-0.203200><15.980900,0.035000,0.203200> rotate<0,0.000000,0> translate<-17.288800,-1.535000,12.192000> }
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<-17.288800,-1.535000,12.598400>}
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<-0.272400,-1.535000,12.598400>}
box{<0,0,-0.203200><17.016400,0.035000,0.203200> rotate<0,0.000000,0> translate<-17.288800,-1.535000,12.598400> }
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<-17.288800,-1.535000,13.004800>}
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<-1.307900,-1.535000,13.004800>}
box{<0,0,-0.203200><15.980900,0.035000,0.203200> rotate<0,0.000000,0> translate<-17.288800,-1.535000,13.004800> }
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<-17.288800,-1.535000,13.411200>}
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<-1.307900,-1.535000,13.411200>}
box{<0,0,-0.203200><15.980900,0.035000,0.203200> rotate<0,0.000000,0> translate<-17.288800,-1.535000,13.411200> }
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<-17.288800,-1.535000,13.817600>}
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<-1.100700,-1.535000,13.817600>}
box{<0,0,-0.203200><16.188100,0.035000,0.203200> rotate<0,0.000000,0> translate<-17.288800,-1.535000,13.817600> }
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<-17.288800,-1.535000,14.224000>}
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<8.460000,-1.535000,14.224000>}
box{<0,0,-0.203200><25.748800,0.035000,0.203200> rotate<0,0.000000,0> translate<-17.288800,-1.535000,14.224000> }
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<-17.288800,-1.535000,14.630400>}
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<-9.632300,-1.535000,14.630400>}
box{<0,0,-0.203200><7.656500,0.035000,0.203200> rotate<0,0.000000,0> translate<-17.288800,-1.535000,14.630400> }
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<-17.288800,-1.535000,15.036800>}
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<-10.056800,-1.535000,15.036800>}
box{<0,0,-0.203200><7.232000,0.035000,0.203200> rotate<0,0.000000,0> translate<-17.288800,-1.535000,15.036800> }
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<-17.288800,-1.535000,15.443200>}
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<-10.138100,-1.535000,15.443200>}
box{<0,0,-0.203200><7.150700,0.035000,0.203200> rotate<0,0.000000,0> translate<-17.288800,-1.535000,15.443200> }
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<-17.288800,-1.535000,15.849600>}
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<-10.020800,-1.535000,15.849600>}
box{<0,0,-0.203200><7.268000,0.035000,0.203200> rotate<0,0.000000,0> translate<-17.288800,-1.535000,15.849600> }
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<-17.288800,-1.535000,16.256000>}
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<-14.641300,-1.535000,16.256000>}
box{<0,0,-0.203200><2.647500,0.035000,0.203200> rotate<0,0.000000,0> translate<-17.288800,-1.535000,16.256000> }
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<-17.288800,-1.535000,16.662400>}
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<-14.838100,-1.535000,16.662400>}
box{<0,0,-0.203200><2.450700,0.035000,0.203200> rotate<0,0.000000,0> translate<-17.288800,-1.535000,16.662400> }
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<-17.288800,-1.535000,17.068800>}
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<-14.795800,-1.535000,17.068800>}
box{<0,0,-0.203200><2.493000,0.035000,0.203200> rotate<0,0.000000,0> translate<-17.288800,-1.535000,17.068800> }
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<-17.288800,-1.535000,17.475200>}
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<-14.560500,-1.535000,17.475200>}
box{<0,0,-0.203200><2.728300,0.035000,0.203200> rotate<0,0.000000,0> translate<-17.288800,-1.535000,17.475200> }
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<-17.288800,-1.535000,17.881600>}
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<-14.816800,-1.535000,17.881600>}
box{<0,0,-0.203200><2.472000,0.035000,0.203200> rotate<0,0.000000,0> translate<-17.288800,-1.535000,17.881600> }
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<-17.288800,-1.535000,18.288000>}
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<-14.829200,-1.535000,18.288000>}
box{<0,0,-0.203200><2.459600,0.035000,0.203200> rotate<0,0.000000,0> translate<-17.288800,-1.535000,18.288000> }
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<-17.288800,-1.535000,18.694400>}
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<-14.590800,-1.535000,18.694400>}
box{<0,0,-0.203200><2.698000,0.035000,0.203200> rotate<0,0.000000,0> translate<-17.288800,-1.535000,18.694400> }
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<-17.288800,-1.535000,19.100800>}
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<-14.133300,-1.535000,19.100800>}
box{<0,0,-0.203200><3.155500,0.035000,0.203200> rotate<0,0.000000,0> translate<-17.288800,-1.535000,19.100800> }
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<-17.288800,-1.535000,19.507200>}
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<-14.133300,-1.535000,19.507200>}
box{<0,0,-0.203200><3.155500,0.035000,0.203200> rotate<0,0.000000,0> translate<-17.288800,-1.535000,19.507200> }
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<-17.288800,-1.535000,19.913600>}
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<-14.133300,-1.535000,19.913600>}
box{<0,0,-0.203200><3.155500,0.035000,0.203200> rotate<0,0.000000,0> translate<-17.288800,-1.535000,19.913600> }
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<-17.288800,-1.535000,20.320000>}
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<-14.133300,-1.535000,20.320000>}
box{<0,0,-0.203200><3.155500,0.035000,0.203200> rotate<0,0.000000,0> translate<-17.288800,-1.535000,20.320000> }
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<-17.288800,-1.535000,20.726400>}
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<-14.133300,-1.535000,20.726400>}
box{<0,0,-0.203200><3.155500,0.035000,0.203200> rotate<0,0.000000,0> translate<-17.288800,-1.535000,20.726400> }
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<-17.288800,-1.535000,21.132800>}
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<-14.133300,-1.535000,21.132800>}
box{<0,0,-0.203200><3.155500,0.035000,0.203200> rotate<0,0.000000,0> translate<-17.288800,-1.535000,21.132800> }
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<-17.288800,-1.535000,21.539200>}
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<-14.133300,-1.535000,21.539200>}
box{<0,0,-0.203200><3.155500,0.035000,0.203200> rotate<0,0.000000,0> translate<-17.288800,-1.535000,21.539200> }
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<-17.288800,-1.535000,21.945600>}
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<-14.133300,-1.535000,21.945600>}
box{<0,0,-0.203200><3.155500,0.035000,0.203200> rotate<0,0.000000,0> translate<-17.288800,-1.535000,21.945600> }
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<-17.288800,-1.535000,22.352000>}
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<-14.133300,-1.535000,22.352000>}
box{<0,0,-0.203200><3.155500,0.035000,0.203200> rotate<0,0.000000,0> translate<-17.288800,-1.535000,22.352000> }
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<-17.288800,-1.535000,22.758400>}
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<-13.995800,-1.535000,22.758400>}
box{<0,0,-0.203200><3.293000,0.035000,0.203200> rotate<0,0.000000,0> translate<-17.288800,-1.535000,22.758400> }
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<-17.288800,-1.535000,23.164800>}
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<-12.509800,-1.535000,23.164800>}
box{<0,0,-0.203200><4.779000,0.035000,0.203200> rotate<0,0.000000,0> translate<-17.288800,-1.535000,23.164800> }
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<-17.288800,-1.535000,23.571200>}
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<-12.536200,-1.535000,23.571200>}
box{<0,0,-0.203200><4.752600,0.035000,0.203200> rotate<0,0.000000,0> translate<-17.288800,-1.535000,23.571200> }
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<-17.288800,-1.535000,23.977600>}
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<-12.307600,-1.535000,23.977600>}
box{<0,0,-0.203200><4.981200,0.035000,0.203200> rotate<0,0.000000,0> translate<-17.288800,-1.535000,23.977600> }
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<-17.288800,-1.535000,24.384000>}
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<-11.038300,-1.535000,24.384000>}
box{<0,0,-0.203200><6.250500,0.035000,0.203200> rotate<0,0.000000,0> translate<-17.288800,-1.535000,24.384000> }
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<-17.288800,-1.535000,24.790400>}
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<-11.444700,-1.535000,24.790400>}
box{<0,0,-0.203200><5.844100,0.035000,0.203200> rotate<0,0.000000,0> translate<-17.288800,-1.535000,24.790400> }
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<-17.288800,-1.535000,25.196800>}
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<-12.534700,-1.535000,25.196800>}
box{<0,0,-0.203200><4.754100,0.035000,0.203200> rotate<0,0.000000,0> translate<-17.288800,-1.535000,25.196800> }
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<-17.288800,-1.535000,25.603200>}
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<-13.097500,-1.535000,25.603200>}
box{<0,0,-0.203200><4.191300,0.035000,0.203200> rotate<0,0.000000,0> translate<-17.288800,-1.535000,25.603200> }
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<-17.288800,-1.535000,26.009600>}
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<-13.285200,-1.535000,26.009600>}
box{<0,0,-0.203200><4.003600,0.035000,0.203200> rotate<0,0.000000,0> translate<-17.288800,-1.535000,26.009600> }
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<-17.288800,-1.535000,26.416000>}
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<-13.308300,-1.535000,26.416000>}
box{<0,0,-0.203200><3.980500,0.035000,0.203200> rotate<0,0.000000,0> translate<-17.288800,-1.535000,26.416000> }
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<-17.288800,-1.535000,26.822400>}
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<-13.308300,-1.535000,26.822400>}
box{<0,0,-0.203200><3.980500,0.035000,0.203200> rotate<0,0.000000,0> translate<-17.288800,-1.535000,26.822400> }
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<-17.288800,-1.535000,27.228800>}
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<-13.308300,-1.535000,27.228800>}
box{<0,0,-0.203200><3.980500,0.035000,0.203200> rotate<0,0.000000,0> translate<-17.288800,-1.535000,27.228800> }
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<-17.288800,-1.535000,27.635200>}
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<-13.308300,-1.535000,27.635200>}
box{<0,0,-0.203200><3.980500,0.035000,0.203200> rotate<0,0.000000,0> translate<-17.288800,-1.535000,27.635200> }
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<-17.288800,-1.535000,28.041600>}
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<-13.308300,-1.535000,28.041600>}
box{<0,0,-0.203200><3.980500,0.035000,0.203200> rotate<0,0.000000,0> translate<-17.288800,-1.535000,28.041600> }
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<-17.288800,-1.535000,28.448000>}
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<-13.145200,-1.535000,28.448000>}
box{<0,0,-0.203200><4.143600,0.035000,0.203200> rotate<0,0.000000,0> translate<-17.288800,-1.535000,28.448000> }
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<-17.288800,-1.535000,28.854400>}
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<-12.700400,-1.535000,28.854400>}
box{<0,0,-0.203200><4.588400,0.035000,0.203200> rotate<0,0.000000,0> translate<-17.288800,-1.535000,28.854400> }
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<-17.288800,-1.535000,29.260800>}
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<0.020800,-1.535000,29.260800>}
box{<0,0,-0.203200><17.309600,0.035000,0.203200> rotate<0,0.000000,0> translate<-17.288800,-1.535000,29.260800> }
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<-17.288800,-1.535000,29.288800>}
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<0.032300,-1.535000,29.288800>}
box{<0,0,-0.203200><17.321100,0.035000,0.203200> rotate<0,0.000000,0> translate<-17.288800,-1.535000,29.288800> }
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<-14.838100,-1.535000,16.633200>}
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<-14.710500,-1.535000,16.325200>}
box{<0,0,-0.203200><0.333385,0.035000,0.203200> rotate<0,67.492017,0> translate<-14.838100,-1.535000,16.633200> }
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<-14.838100,-1.535000,16.966700>}
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<-14.838100,-1.535000,16.633200>}
box{<0,0,-0.203200><0.333500,0.035000,0.203200> rotate<0,-90.000000,0> translate<-14.838100,-1.535000,16.633200> }
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<-14.838100,-1.535000,16.966700>}
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<-14.710500,-1.535000,17.274700>}
box{<0,0,-0.203200><0.333385,0.035000,0.203200> rotate<0,-67.492017,0> translate<-14.838100,-1.535000,16.966700> }
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<-14.838100,-1.535000,17.933200>}
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<-14.710500,-1.535000,17.625200>}
box{<0,0,-0.203200><0.333385,0.035000,0.203200> rotate<0,67.492017,0> translate<-14.838100,-1.535000,17.933200> }
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<-14.838100,-1.535000,18.266700>}
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<-14.838100,-1.535000,17.933200>}
box{<0,0,-0.203200><0.333500,0.035000,0.203200> rotate<0,-90.000000,0> translate<-14.838100,-1.535000,17.933200> }
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<-14.838100,-1.535000,18.266700>}
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<-14.710500,-1.535000,18.574700>}
box{<0,0,-0.203200><0.333385,0.035000,0.203200> rotate<0,-67.492017,0> translate<-14.838100,-1.535000,18.266700> }
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<-14.710500,-1.535000,16.325200>}
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<-14.474700,-1.535000,16.089400>}
box{<0,0,-0.203200><0.333472,0.035000,0.203200> rotate<0,44.997030,0> translate<-14.710500,-1.535000,16.325200> }
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<-14.710500,-1.535000,17.274700>}
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<-14.535200,-1.535000,17.449900>}
box{<0,0,-0.203200><0.247841,0.035000,0.203200> rotate<0,-44.980684,0> translate<-14.710500,-1.535000,17.274700> }
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<-14.710500,-1.535000,17.625200>}
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<-14.535200,-1.535000,17.449900>}
box{<0,0,-0.203200><0.247912,0.035000,0.203200> rotate<0,44.997030,0> translate<-14.710500,-1.535000,17.625200> }
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<-14.710500,-1.535000,18.574700>}
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<-14.474700,-1.535000,18.810500>}
box{<0,0,-0.203200><0.333472,0.035000,0.203200> rotate<0,-44.997030,0> translate<-14.710500,-1.535000,18.574700> }
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<-14.474700,-1.535000,16.089400>}
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<-14.166700,-1.535000,15.961800>}
box{<0,0,-0.203200><0.333385,0.035000,0.203200> rotate<0,22.502043,0> translate<-14.474700,-1.535000,16.089400> }
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<-14.474700,-1.535000,18.810500>}
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<-14.166700,-1.535000,18.938100>}
box{<0,0,-0.203200><0.333385,0.035000,0.203200> rotate<0,-22.502043,0> translate<-14.474700,-1.535000,18.810500> }
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<-14.166700,-1.535000,15.961800>}
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<-13.833200,-1.535000,15.961800>}
box{<0,0,-0.203200><0.333500,0.035000,0.203200> rotate<0,0.000000,0> translate<-14.166700,-1.535000,15.961800> }
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<-14.166700,-1.535000,18.938100>}
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<-14.133300,-1.535000,18.938100>}
box{<0,0,-0.203200><0.033400,0.035000,0.203200> rotate<0,0.000000,0> translate<-14.166700,-1.535000,18.938100> }
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<-14.133300,-1.535000,22.179000>}
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<-14.133300,-1.535000,18.938100>}
box{<0,0,-0.203200><3.240900,0.035000,0.203200> rotate<0,-90.000000,0> translate<-14.133300,-1.535000,18.938100> }
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<-14.133300,-1.535000,22.620900>}
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<-14.133300,-1.535000,22.179000>}
box{<0,0,-0.203200><0.441900,0.035000,0.203200> rotate<0,-90.000000,0> translate<-14.133300,-1.535000,22.179000> }
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<-14.133300,-1.535000,22.620900>}
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<-14.033300,-1.535000,22.720900>}
box{<0,0,-0.203200><0.141421,0.035000,0.203200> rotate<0,-44.997030,0> translate<-14.133300,-1.535000,22.620900> }
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<-14.033300,-1.535000,22.720900>}
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<-13.720900,-1.535000,23.033300>}
box{<0,0,-0.203200><0.441800,0.035000,0.203200> rotate<0,-44.997030,0> translate<-14.033300,-1.535000,22.720900> }
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<-13.833200,-1.535000,15.961800>}
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<-13.525200,-1.535000,16.089400>}
box{<0,0,-0.203200><0.333385,0.035000,0.203200> rotate<0,-22.502043,0> translate<-13.833200,-1.535000,15.961800> }
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<-13.720900,-1.535000,23.033300>}
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<-13.620900,-1.535000,23.033300>}
box{<0,0,-0.203200><0.100000,0.035000,0.203200> rotate<0,0.000000,0> translate<-13.720900,-1.535000,23.033300> }
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<-13.620900,-1.535000,23.033300>}
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<-13.520900,-1.535000,23.133300>}
box{<0,0,-0.203200><0.141421,0.035000,0.203200> rotate<0,-44.997030,0> translate<-13.620900,-1.535000,23.033300> }
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<-13.525200,-1.535000,16.089400>}
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<-13.475000,-1.535000,16.139600>}
box{<0,0,-0.203200><0.070994,0.035000,0.203200> rotate<0,-44.997030,0> translate<-13.525200,-1.535000,16.089400> }
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<-13.520900,-1.535000,23.133300>}
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<-12.496700,-1.535000,23.133300>}
box{<0,0,-0.203200><1.024200,0.035000,0.203200> rotate<0,0.000000,0> translate<-13.520900,-1.535000,23.133300> }
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<-13.475000,-1.535000,16.139600>}
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<-9.824900,-1.535000,16.139600>}
box{<0,0,-0.203200><3.650100,0.035000,0.203200> rotate<0,0.000000,0> translate<-13.475000,-1.535000,16.139600> }
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<-13.308300,-1.535000,26.065500>}
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<-13.130400,-1.535000,25.636100>}
box{<0,0,-0.203200><0.464793,0.035000,0.203200> rotate<0,67.491366,0> translate<-13.308300,-1.535000,26.065500> }
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<-13.308300,-1.535000,28.054400>}
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<-13.308300,-1.535000,26.065500>}
box{<0,0,-0.203200><1.988900,0.035000,0.203200> rotate<0,-90.000000,0> translate<-13.308300,-1.535000,26.065500> }
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<-13.308300,-1.535000,28.054400>}
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<-13.130400,-1.535000,28.483800>}
box{<0,0,-0.203200><0.464793,0.035000,0.203200> rotate<0,-67.491366,0> translate<-13.308300,-1.535000,28.054400> }
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<-13.130400,-1.535000,25.636100>}
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<-12.801800,-1.535000,25.307500>}
box{<0,0,-0.203200><0.464711,0.035000,0.203200> rotate<0,44.997030,0> translate<-13.130400,-1.535000,25.636100> }
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<-13.130400,-1.535000,28.483800>}
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<-12.801800,-1.535000,28.812400>}
box{<0,0,-0.203200><0.464711,0.035000,0.203200> rotate<0,-44.997030,0> translate<-13.130400,-1.535000,28.483800> }
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<-12.801800,-1.535000,25.307500>}
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<-12.372400,-1.535000,25.129600>}
box{<0,0,-0.203200><0.464793,0.035000,0.203200> rotate<0,22.502694,0> translate<-12.801800,-1.535000,25.307500> }
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<-12.801800,-1.535000,28.812400>}
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<-12.372400,-1.535000,28.990300>}
box{<0,0,-0.203200><0.464793,0.035000,0.203200> rotate<0,-22.502694,0> translate<-12.801800,-1.535000,28.812400> }
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<-12.538100,-1.535000,23.233200>}
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<-12.496700,-1.535000,23.133300>}
box{<0,0,-0.203200><0.108139,0.035000,0.203200> rotate<0,67.485724,0> translate<-12.538100,-1.535000,23.233200> }
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<-12.538100,-1.535000,23.566700>}
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<-12.538100,-1.535000,23.233200>}
box{<0,0,-0.203200><0.333500,0.035000,0.203200> rotate<0,-90.000000,0> translate<-12.538100,-1.535000,23.233200> }
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<-12.538100,-1.535000,23.566700>}
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<-12.410500,-1.535000,23.874700>}
box{<0,0,-0.203200><0.333385,0.035000,0.203200> rotate<0,-67.492017,0> translate<-12.538100,-1.535000,23.566700> }
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<-12.410500,-1.535000,23.874700>}
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<-12.174700,-1.535000,24.110500>}
box{<0,0,-0.203200><0.333472,0.035000,0.203200> rotate<0,-44.997030,0> translate<-12.410500,-1.535000,23.874700> }
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<-12.372400,-1.535000,25.129600>}
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<-11.907500,-1.535000,25.129600>}
box{<0,0,-0.203200><0.464900,0.035000,0.203200> rotate<0,0.000000,0> translate<-12.372400,-1.535000,25.129600> }
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<-12.372400,-1.535000,28.990300>}
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<-11.907500,-1.535000,28.990300>}
box{<0,0,-0.203200><0.464900,0.035000,0.203200> rotate<0,0.000000,0> translate<-12.372400,-1.535000,28.990300> }
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<-12.174700,-1.535000,24.110500>}
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<-11.866700,-1.535000,24.238100>}
box{<0,0,-0.203200><0.333385,0.035000,0.203200> rotate<0,-22.502043,0> translate<-12.174700,-1.535000,24.110500> }
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<-11.907500,-1.535000,25.129600>}
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<-11.820100,-1.535000,25.165900>}
box{<0,0,-0.203200><0.094639,0.035000,0.203200> rotate<0,-22.553178,0> translate<-11.907500,-1.535000,25.129600> }
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<-11.907500,-1.535000,28.990300>}
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<-11.478100,-1.535000,28.812400>}
box{<0,0,-0.203200><0.464793,0.035000,0.203200> rotate<0,22.502694,0> translate<-11.907500,-1.535000,28.990300> }
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<-11.866700,-1.535000,24.238100>}
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<-11.533200,-1.535000,24.238100>}
box{<0,0,-0.203200><0.333500,0.035000,0.203200> rotate<0,0.000000,0> translate<-11.866700,-1.535000,24.238100> }
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<-11.820100,-1.535000,25.165900>}
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<-10.333300,-1.535000,23.679000>}
box{<0,0,-0.203200><2.102723,0.035000,0.203200> rotate<0,44.998957,0> translate<-11.820100,-1.535000,25.165900> }
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<-11.579400,-1.535000,28.854400>}
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<-10.160400,-1.535000,28.854400>}
box{<0,0,-0.203200><1.419000,0.035000,0.203200> rotate<0,0.000000,0> translate<-11.579400,-1.535000,28.854400> }
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<-11.533200,-1.535000,24.238100>}
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<-11.225200,-1.535000,24.110500>}
box{<0,0,-0.203200><0.333385,0.035000,0.203200> rotate<0,22.502043,0> translate<-11.533200,-1.535000,24.238100> }
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<-11.478100,-1.535000,28.812400>}
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<-11.149500,-1.535000,28.483800>}
box{<0,0,-0.203200><0.464711,0.035000,0.203200> rotate<0,44.997030,0> translate<-11.478100,-1.535000,28.812400> }
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<-11.225200,-1.535000,24.110500>}
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<-10.989400,-1.535000,23.874700>}
box{<0,0,-0.203200><0.333472,0.035000,0.203200> rotate<0,44.997030,0> translate<-11.225200,-1.535000,24.110500> }
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<-11.149500,-1.535000,28.483800>}
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<-10.971600,-1.535000,28.054400>}
box{<0,0,-0.203200><0.464793,0.035000,0.203200> rotate<0,67.491366,0> translate<-11.149500,-1.535000,28.483800> }
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<-11.134600,-1.535000,28.448000>}
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<-10.605200,-1.535000,28.448000>}
box{<0,0,-0.203200><0.529400,0.035000,0.203200> rotate<0,0.000000,0> translate<-11.134600,-1.535000,28.448000> }
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<-11.092300,-1.535000,23.977600>}
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<-10.631900,-1.535000,23.977600>}
box{<0,0,-0.203200><0.460400,0.035000,0.203200> rotate<0,0.000000,0> translate<-11.092300,-1.535000,23.977600> }
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<-11.041800,-1.535000,25.896100>}
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<-10.971600,-1.535000,26.065500>}
box{<0,0,-0.203200><0.183370,0.035000,0.203200> rotate<0,-67.486244,0> translate<-11.041800,-1.535000,25.896100> }
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<-11.041800,-1.535000,25.896100>}
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<-9.266600,-1.535000,24.120900>}
box{<0,0,-0.203200><2.510512,0.035000,0.203200> rotate<0,44.997030,0> translate<-11.041800,-1.535000,25.896100> }
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<-10.994800,-1.535000,26.009600>}
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<-10.745200,-1.535000,26.009600>}
box{<0,0,-0.203200><0.249600,0.035000,0.203200> rotate<0,0.000000,0> translate<-10.994800,-1.535000,26.009600> }
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<-10.989400,-1.535000,23.874700>}
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<-10.861800,-1.535000,23.566700>}
box{<0,0,-0.203200><0.333385,0.035000,0.203200> rotate<0,67.492017,0> translate<-10.989400,-1.535000,23.874700> }
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<-10.971600,-1.535000,26.065500>}
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<-10.971600,-1.535000,28.054400>}
box{<0,0,-0.203200><1.988900,0.035000,0.203200> rotate<0,90.000000,0> translate<-10.971600,-1.535000,28.054400> }
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<-10.971600,-1.535000,26.416000>}
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<-10.768300,-1.535000,26.416000>}
box{<0,0,-0.203200><0.203300,0.035000,0.203200> rotate<0,0.000000,0> translate<-10.971600,-1.535000,26.416000> }
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<-10.971600,-1.535000,26.822400>}
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<-10.768300,-1.535000,26.822400>}
box{<0,0,-0.203200><0.203300,0.035000,0.203200> rotate<0,0.000000,0> translate<-10.971600,-1.535000,26.822400> }
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<-10.971600,-1.535000,27.228800>}
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<-10.768300,-1.535000,27.228800>}
box{<0,0,-0.203200><0.203300,0.035000,0.203200> rotate<0,0.000000,0> translate<-10.971600,-1.535000,27.228800> }
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<-10.971600,-1.535000,27.635200>}
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<-10.768300,-1.535000,27.635200>}
box{<0,0,-0.203200><0.203300,0.035000,0.203200> rotate<0,0.000000,0> translate<-10.971600,-1.535000,27.635200> }
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<-10.971600,-1.535000,28.041600>}
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<-10.768300,-1.535000,28.041600>}
box{<0,0,-0.203200><0.203300,0.035000,0.203200> rotate<0,0.000000,0> translate<-10.971600,-1.535000,28.041600> }
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<-10.967400,-1.535000,21.007900>}
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<-10.846800,-1.535000,21.007900>}
box{<0,0,-0.203200><0.120600,0.035000,0.203200> rotate<0,0.000000,0> translate<-10.967400,-1.535000,21.007900> }
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<-10.967400,-1.535000,21.007900>}
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<-10.843600,-1.535000,21.131600>}
box{<0,0,-0.203200><0.175009,0.035000,0.203200> rotate<0,-44.973882,0> translate<-10.967400,-1.535000,21.007900> }
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<-10.963400,-1.535000,22.988000>}
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<-10.861800,-1.535000,23.233200>}
box{<0,0,-0.203200><0.265416,0.035000,0.203200> rotate<0,-67.488598,0> translate<-10.963400,-1.535000,22.988000> }
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<-10.963400,-1.535000,22.988000>}
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<-10.843600,-1.535000,22.868300>}
box{<0,0,-0.203200><0.169352,0.035000,0.203200> rotate<0,44.973109,0> translate<-10.963400,-1.535000,22.988000> }
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<-10.890200,-1.535000,23.164800>}
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<-10.344700,-1.535000,23.164800>}
box{<0,0,-0.203200><0.545500,0.035000,0.203200> rotate<0,0.000000,0> translate<-10.890200,-1.535000,23.164800> }
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<-10.863600,-1.535000,23.571200>}
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<-10.333300,-1.535000,23.571200>}
box{<0,0,-0.203200><0.530300,0.035000,0.203200> rotate<0,0.000000,0> translate<-10.863600,-1.535000,23.571200> }
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<-10.861800,-1.535000,23.233200>}
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<-10.861800,-1.535000,23.566700>}
box{<0,0,-0.203200><0.333500,0.035000,0.203200> rotate<0,90.000000,0> translate<-10.861800,-1.535000,23.566700> }
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<-10.846800,-1.535000,19.847400>}
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<-10.846800,-1.535000,21.007900>}
box{<0,0,-0.203200><1.160500,0.035000,0.203200> rotate<0,90.000000,0> translate<-10.846800,-1.535000,21.007900> }
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<-10.846800,-1.535000,19.847400>}
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<-10.753000,-1.535000,19.847400>}
box{<0,0,-0.203200><0.093800,0.035000,0.203200> rotate<0,0.000000,0> translate<-10.846800,-1.535000,19.847400> }
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<-10.846800,-1.535000,19.913600>}
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<-10.753000,-1.535000,19.913600>}
box{<0,0,-0.203200><0.093800,0.035000,0.203200> rotate<0,0.000000,0> translate<-10.846800,-1.535000,19.913600> }
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<-10.846800,-1.535000,20.320000>}
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<-10.753000,-1.535000,20.320000>}
box{<0,0,-0.203200><0.093800,0.035000,0.203200> rotate<0,0.000000,0> translate<-10.846800,-1.535000,20.320000> }
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<-10.846800,-1.535000,20.726400>}
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<-10.753000,-1.535000,20.726400>}
box{<0,0,-0.203200><0.093800,0.035000,0.203200> rotate<0,0.000000,0> translate<-10.846800,-1.535000,20.726400> }
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<-10.843600,-1.535000,21.131600>}
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<-10.843600,-1.535000,22.868300>}
box{<0,0,-0.203200><1.736700,0.035000,0.203200> rotate<0,90.000000,0> translate<-10.843600,-1.535000,22.868300> }
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<-10.843600,-1.535000,21.132800>}
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<-10.756300,-1.535000,21.132800>}
box{<0,0,-0.203200><0.087300,0.035000,0.203200> rotate<0,0.000000,0> translate<-10.843600,-1.535000,21.132800> }
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<-10.843600,-1.535000,21.539200>}
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<-10.756300,-1.535000,21.539200>}
box{<0,0,-0.203200><0.087300,0.035000,0.203200> rotate<0,0.000000,0> translate<-10.843600,-1.535000,21.539200> }
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<-10.843600,-1.535000,21.945600>}
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<-10.756300,-1.535000,21.945600>}
box{<0,0,-0.203200><0.087300,0.035000,0.203200> rotate<0,0.000000,0> translate<-10.843600,-1.535000,21.945600> }
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<-10.843600,-1.535000,22.352000>}
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<-10.756300,-1.535000,22.352000>}
box{<0,0,-0.203200><0.087300,0.035000,0.203200> rotate<0,0.000000,0> translate<-10.843600,-1.535000,22.352000> }
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<-10.843600,-1.535000,22.758400>}
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<-10.756300,-1.535000,22.758400>}
box{<0,0,-0.203200><0.087300,0.035000,0.203200> rotate<0,0.000000,0> translate<-10.843600,-1.535000,22.758400> }
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<-10.768300,-1.535000,26.065500>}
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<-10.590400,-1.535000,25.636100>}
box{<0,0,-0.203200><0.464793,0.035000,0.203200> rotate<0,67.491366,0> translate<-10.768300,-1.535000,26.065500> }
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<-10.768300,-1.535000,28.054400>}
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<-10.768300,-1.535000,26.065500>}
box{<0,0,-0.203200><1.988900,0.035000,0.203200> rotate<0,-90.000000,0> translate<-10.768300,-1.535000,26.065500> }
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<-10.768300,-1.535000,28.054400>}
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<-10.590400,-1.535000,28.483800>}
box{<0,0,-0.203200><0.464793,0.035000,0.203200> rotate<0,-67.491366,0> translate<-10.768300,-1.535000,28.054400> }
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<-10.756300,-1.535000,21.131600>}
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<-10.632500,-1.535000,21.007900>}
box{<0,0,-0.203200><0.175009,0.035000,0.203200> rotate<0,44.973882,0> translate<-10.756300,-1.535000,21.131600> }
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<-10.756300,-1.535000,22.868300>}
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<-10.756300,-1.535000,21.131600>}
box{<0,0,-0.203200><1.736700,0.035000,0.203200> rotate<0,-90.000000,0> translate<-10.756300,-1.535000,21.131600> }
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<-10.756300,-1.535000,22.868300>}
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<-10.518300,-1.535000,23.106300>}
box{<0,0,-0.203200><0.336583,0.035000,0.203200> rotate<0,-44.997030,0> translate<-10.756300,-1.535000,22.868300> }
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<-10.753000,-1.535000,21.007900>}
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<-10.753000,-1.535000,19.847400>}
box{<0,0,-0.203200><1.160500,0.035000,0.203200> rotate<0,-90.000000,0> translate<-10.753000,-1.535000,19.847400> }
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<-10.753000,-1.535000,21.007900>}
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<-10.632500,-1.535000,21.007900>}
box{<0,0,-0.203200><0.120500,0.035000,0.203200> rotate<0,0.000000,0> translate<-10.753000,-1.535000,21.007900> }
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<-10.752500,-1.535000,19.753000>}
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<-10.752500,-1.535000,19.846800>}
box{<0,0,-0.203200><0.093800,0.035000,0.203200> rotate<0,90.000000,0> translate<-10.752500,-1.535000,19.846800> }
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<-10.752500,-1.535000,19.753000>}
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<-9.792000,-1.535000,19.753000>}
box{<0,0,-0.203200><0.960500,0.035000,0.203200> rotate<0,0.000000,0> translate<-10.752500,-1.535000,19.753000> }
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<-10.752500,-1.535000,19.846800>}
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<-9.792000,-1.535000,19.846800>}
box{<0,0,-0.203200><0.960500,0.035000,0.203200> rotate<0,0.000000,0> translate<-10.752500,-1.535000,19.846800> }
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<-10.748900,-1.535000,25.603200>}
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<-10.557500,-1.535000,25.603200>}
box{<0,0,-0.203200><0.191400,0.035000,0.203200> rotate<0,0.000000,0> translate<-10.748900,-1.535000,25.603200> }
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<-10.590400,-1.535000,25.636100>}
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<-10.261800,-1.535000,25.307500>}
box{<0,0,-0.203200><0.464711,0.035000,0.203200> rotate<0,44.997030,0> translate<-10.590400,-1.535000,25.636100> }
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<-10.590400,-1.535000,28.483800>}
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<-10.261800,-1.535000,28.812400>}
box{<0,0,-0.203200><0.464711,0.035000,0.203200> rotate<0,-44.997030,0> translate<-10.590400,-1.535000,28.483800> }
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<-10.518300,-1.535000,23.106300>}
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<-10.346700,-1.535000,23.106300>}
box{<0,0,-0.203200><0.171600,0.035000,0.203200> rotate<0,0.000000,0> translate<-10.518300,-1.535000,23.106300> }
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<-10.346700,-1.535000,23.106300>}
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<-10.333300,-1.535000,23.508400>}
box{<0,0,-0.203200><0.402323,0.035000,0.203200> rotate<0,-88.085508,0> translate<-10.346700,-1.535000,23.106300> }
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<-10.342500,-1.535000,25.196800>}
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<-9.994700,-1.535000,25.196800>}
box{<0,0,-0.203200><0.347800,0.035000,0.203200> rotate<0,0.000000,0> translate<-10.342500,-1.535000,25.196800> }
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<-10.333300,-1.535000,23.679000>}
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<-10.333300,-1.535000,23.508400>}
box{<0,0,-0.203200><0.170600,0.035000,0.203200> rotate<0,-90.000000,0> translate<-10.333300,-1.535000,23.508400> }
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<-10.261800,-1.535000,25.307500>}
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<-9.832400,-1.535000,25.129600>}
box{<0,0,-0.203200><0.464793,0.035000,0.203200> rotate<0,22.502694,0> translate<-10.261800,-1.535000,25.307500> }
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<-10.261800,-1.535000,28.812400>}
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<-9.832400,-1.535000,28.990300>}
box{<0,0,-0.203200><0.464793,0.035000,0.203200> rotate<0,-22.502694,0> translate<-10.261800,-1.535000,28.812400> }
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<-10.138100,-1.535000,15.233200>}
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<-10.010500,-1.535000,14.925200>}
box{<0,0,-0.203200><0.333385,0.035000,0.203200> rotate<0,67.492017,0> translate<-10.138100,-1.535000,15.233200> }
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<-10.138100,-1.535000,15.566700>}
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<-10.138100,-1.535000,15.233200>}
box{<0,0,-0.203200><0.333500,0.035000,0.203200> rotate<0,-90.000000,0> translate<-10.138100,-1.535000,15.233200> }
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<-10.138100,-1.535000,15.566700>}
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<-10.010500,-1.535000,15.874700>}
box{<0,0,-0.203200><0.333385,0.035000,0.203200> rotate<0,-67.492017,0> translate<-10.138100,-1.535000,15.566700> }
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<-10.010500,-1.535000,14.925200>}
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<-9.774700,-1.535000,14.689400>}
box{<0,0,-0.203200><0.333472,0.035000,0.203200> rotate<0,44.997030,0> translate<-10.010500,-1.535000,14.925200> }
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<-10.010500,-1.535000,15.874700>}
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<-9.785200,-1.535000,16.099900>}
box{<0,0,-0.203200><0.318552,0.035000,0.203200> rotate<0,-44.984313,0> translate<-10.010500,-1.535000,15.874700> }
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<-9.975300,-1.535000,20.893600>}
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<-9.792000,-1.535000,20.710400>}
box{<0,0,-0.203200><0.259155,0.035000,0.203200> rotate<0,44.981398,0> translate<-9.975300,-1.535000,20.893600> }
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<-9.975300,-1.535000,20.893600>}
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<-9.181600,-1.535000,20.893600>}
box{<0,0,-0.203200><0.793700,0.035000,0.203200> rotate<0,0.000000,0> translate<-9.975300,-1.535000,20.893600> }
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<-9.936100,-1.535000,24.790400>}
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<-6.663700,-1.535000,24.790400>}
box{<0,0,-0.203200><3.272400,0.035000,0.203200> rotate<0,0.000000,0> translate<-9.936100,-1.535000,24.790400> }
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<-9.832400,-1.535000,25.129600>}
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<-9.367500,-1.535000,25.129600>}
box{<0,0,-0.203200><0.464900,0.035000,0.203200> rotate<0,0.000000,0> translate<-9.832400,-1.535000,25.129600> }
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<-9.832400,-1.535000,28.990300>}
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<-9.367500,-1.535000,28.990300>}
box{<0,0,-0.203200><0.464900,0.035000,0.203200> rotate<0,0.000000,0> translate<-9.832400,-1.535000,28.990300> }
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<-9.824900,-1.535000,16.139600>}
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<-9.785200,-1.535000,16.099900>}
box{<0,0,-0.203200><0.056144,0.035000,0.203200> rotate<0,44.997030,0> translate<-9.824900,-1.535000,16.139600> }
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<-9.808000,-1.535000,20.726400>}
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<-8.813300,-1.535000,20.726400>}
box{<0,0,-0.203200><0.994700,0.035000,0.203200> rotate<0,0.000000,0> translate<-9.808000,-1.535000,20.726400> }
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<-9.792000,-1.535000,19.462100>}
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<-9.792000,-1.535000,19.753000>}
box{<0,0,-0.203200><0.290900,0.035000,0.203200> rotate<0,90.000000,0> translate<-9.792000,-1.535000,19.753000> }
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<-9.792000,-1.535000,19.462100>}
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<-8.838100,-1.535000,20.416000>}
box{<0,0,-0.203200><1.349018,0.035000,0.203200> rotate<0,-44.997030,0> translate<-9.792000,-1.535000,19.462100> }
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<-9.792000,-1.535000,19.507200>}
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<-9.746900,-1.535000,19.507200>}
box{<0,0,-0.203200><0.045100,0.035000,0.203200> rotate<0,0.000000,0> translate<-9.792000,-1.535000,19.507200> }
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<-9.792000,-1.535000,19.846800>}
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<-9.792000,-1.535000,20.710400>}
box{<0,0,-0.203200><0.863600,0.035000,0.203200> rotate<0,90.000000,0> translate<-9.792000,-1.535000,20.710400> }
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<-9.792000,-1.535000,19.913600>}
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<-9.340500,-1.535000,19.913600>}
box{<0,0,-0.203200><0.451500,0.035000,0.203200> rotate<0,0.000000,0> translate<-9.792000,-1.535000,19.913600> }
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<-9.792000,-1.535000,20.320000>}
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<-8.934100,-1.535000,20.320000>}
box{<0,0,-0.203200><0.857900,0.035000,0.203200> rotate<0,0.000000,0> translate<-9.792000,-1.535000,20.320000> }
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<-9.774700,-1.535000,14.689400>}
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<-9.466700,-1.535000,14.561800>}
box{<0,0,-0.203200><0.333385,0.035000,0.203200> rotate<0,22.502043,0> translate<-9.774700,-1.535000,14.689400> }
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<-9.529700,-1.535000,24.384000>}
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<-8.370200,-1.535000,24.384000>}
box{<0,0,-0.203200><1.159500,0.035000,0.203200> rotate<0,0.000000,0> translate<-9.529700,-1.535000,24.384000> }
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<-9.466700,-1.535000,14.561800>}
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<-9.133200,-1.535000,14.561800>}
box{<0,0,-0.203200><0.333500,0.035000,0.203200> rotate<0,0.000000,0> translate<-9.466700,-1.535000,14.561800> }
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<-9.367500,-1.535000,25.129600>}
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<-8.938100,-1.535000,25.307500>}
box{<0,0,-0.203200><0.464793,0.035000,0.203200> rotate<0,-22.502694,0> translate<-9.367500,-1.535000,25.129600> }
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<-9.367500,-1.535000,28.990300>}
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<-8.938100,-1.535000,28.812400>}
box{<0,0,-0.203200><0.464793,0.035000,0.203200> rotate<0,22.502694,0> translate<-9.367500,-1.535000,28.990300> }
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<-9.266600,-1.535000,23.733300>}
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<-9.266600,-1.535000,24.120900>}
box{<0,0,-0.203200><0.387600,0.035000,0.203200> rotate<0,90.000000,0> translate<-9.266600,-1.535000,24.120900> }
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<-9.266600,-1.535000,23.733300>}
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<-8.833300,-1.535000,23.733300>}
box{<0,0,-0.203200><0.433300,0.035000,0.203200> rotate<0,0.000000,0> translate<-9.266600,-1.535000,23.733300> }
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<-9.266600,-1.535000,23.977600>}
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<-8.776600,-1.535000,23.977600>}
box{<0,0,-0.203200><0.490000,0.035000,0.203200> rotate<0,0.000000,0> translate<-9.266600,-1.535000,23.977600> }
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<-9.205300,-1.535000,25.196800>}
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<-7.720200,-1.535000,25.196800>}
box{<0,0,-0.203200><1.485100,0.035000,0.203200> rotate<0,0.000000,0> translate<-9.205300,-1.535000,25.196800> }
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<-9.181600,-1.535000,20.893600>}
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<-8.943600,-1.535000,21.131600>}
box{<0,0,-0.203200><0.336583,0.035000,0.203200> rotate<0,-44.997030,0> translate<-9.181600,-1.535000,20.893600> }
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<-9.133200,-1.535000,14.561800>}
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<-8.825200,-1.535000,14.689400>}
box{<0,0,-0.203200><0.333385,0.035000,0.203200> rotate<0,-22.502043,0> translate<-9.133200,-1.535000,14.561800> }
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<-9.039400,-1.535000,28.854400>}
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<-7.823500,-1.535000,28.854400>}
box{<0,0,-0.203200><1.215900,0.035000,0.203200> rotate<0,0.000000,0> translate<-9.039400,-1.535000,28.854400> }
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<-8.967700,-1.535000,14.630400>}
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<-3.432300,-1.535000,14.630400>}
box{<0,0,-0.203200><5.535400,0.035000,0.203200> rotate<0,0.000000,0> translate<-8.967700,-1.535000,14.630400> }
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<-8.943600,-1.535000,21.131600>}
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<-8.943600,-1.535000,22.666600>}
box{<0,0,-0.203200><1.535000,0.035000,0.203200> rotate<0,90.000000,0> translate<-8.943600,-1.535000,22.666600> }
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<-8.943600,-1.535000,21.132800>}
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<-8.552400,-1.535000,21.132800>}
box{<0,0,-0.203200><0.391200,0.035000,0.203200> rotate<0,0.000000,0> translate<-8.943600,-1.535000,21.132800> }
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<-8.943600,-1.535000,21.539200>}
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<-7.808000,-1.535000,21.539200>}
box{<0,0,-0.203200><1.135600,0.035000,0.203200> rotate<0,0.000000,0> translate<-8.943600,-1.535000,21.539200> }
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<-8.943600,-1.535000,21.945600>}
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<-7.539600,-1.535000,21.945600>}
box{<0,0,-0.203200><1.404000,0.035000,0.203200> rotate<0,0.000000,0> translate<-8.943600,-1.535000,21.945600> }
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<-8.943600,-1.535000,22.352000>}
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<-6.006300,-1.535000,22.352000>}
box{<0,0,-0.203200><2.937300,0.035000,0.203200> rotate<0,0.000000,0> translate<-8.943600,-1.535000,22.352000> }
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<-8.943600,-1.535000,22.666600>}
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<-8.620900,-1.535000,22.666600>}
box{<0,0,-0.203200><0.322700,0.035000,0.203200> rotate<0,0.000000,0> translate<-8.943600,-1.535000,22.666600> }
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<-8.938100,-1.535000,25.307500>}
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<-8.609500,-1.535000,25.636100>}
box{<0,0,-0.203200><0.464711,0.035000,0.203200> rotate<0,-44.997030,0> translate<-8.938100,-1.535000,25.307500> }
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<-8.938100,-1.535000,28.812400>}
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<-8.609500,-1.535000,28.483800>}
box{<0,0,-0.203200><0.464711,0.035000,0.203200> rotate<0,44.997030,0> translate<-8.938100,-1.535000,28.812400> }
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<-8.838100,-1.535000,20.666700>}
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<-8.838100,-1.535000,20.416000>}
box{<0,0,-0.203200><0.250700,0.035000,0.203200> rotate<0,-90.000000,0> translate<-8.838100,-1.535000,20.416000> }
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<-8.838100,-1.535000,20.666700>}
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<-8.710500,-1.535000,20.974700>}
box{<0,0,-0.203200><0.333385,0.035000,0.203200> rotate<0,-67.492017,0> translate<-8.838100,-1.535000,20.666700> }
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<-8.833300,-1.535000,23.920900>}
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<-8.833300,-1.535000,23.733300>}
box{<0,0,-0.203200><0.187600,0.035000,0.203200> rotate<0,-90.000000,0> translate<-8.833300,-1.535000,23.733300> }
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<-8.833300,-1.535000,23.920900>}
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<-8.520900,-1.535000,24.233300>}
box{<0,0,-0.203200><0.441800,0.035000,0.203200> rotate<0,-44.997030,0> translate<-8.833300,-1.535000,23.920900> }
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<-8.825200,-1.535000,14.689400>}
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<-8.648000,-1.535000,14.866600>}
box{<0,0,-0.203200><0.250599,0.035000,0.203200> rotate<0,-44.997030,0> translate<-8.825200,-1.535000,14.689400> }
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<-8.814700,-1.535000,16.099900>}
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<-8.648000,-1.535000,15.933300>}
box{<0,0,-0.203200><0.235679,0.035000,0.203200> rotate<0,44.979841,0> translate<-8.814700,-1.535000,16.099900> }
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<-8.814700,-1.535000,16.099900>}
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<-8.589400,-1.535000,16.325200>}
box{<0,0,-0.203200><0.318622,0.035000,0.203200> rotate<0,-44.997030,0> translate<-8.814700,-1.535000,16.099900> }
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<-8.710500,-1.535000,20.974700>}
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<-8.474700,-1.535000,21.210500>}
box{<0,0,-0.203200><0.333472,0.035000,0.203200> rotate<0,-44.997030,0> translate<-8.710500,-1.535000,20.974700> }
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<-8.658600,-1.535000,16.256000>}
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<-4.738100,-1.535000,16.256000>}
box{<0,0,-0.203200><3.920500,0.035000,0.203200> rotate<0,0.000000,0> translate<-8.658600,-1.535000,16.256000> }
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<-8.648000,-1.535000,14.866600>}
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<-3.751900,-1.535000,14.866600>}
box{<0,0,-0.203200><4.896100,0.035000,0.203200> rotate<0,0.000000,0> translate<-8.648000,-1.535000,14.866600> }
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<-8.648000,-1.535000,15.933300>}
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<-4.613800,-1.535000,15.933300>}
box{<0,0,-0.203200><4.034200,0.035000,0.203200> rotate<0,0.000000,0> translate<-8.648000,-1.535000,15.933300> }
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<-8.642400,-1.535000,25.603200>}
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<-8.146800,-1.535000,25.603200>}
box{<0,0,-0.203200><0.495600,0.035000,0.203200> rotate<0,0.000000,0> translate<-8.642400,-1.535000,25.603200> }
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<-8.620900,-1.535000,22.666600>}
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<-8.179000,-1.535000,22.666600>}
box{<0,0,-0.203200><0.441900,0.035000,0.203200> rotate<0,0.000000,0> translate<-8.620900,-1.535000,22.666600> }
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<-8.609500,-1.535000,25.636100>}
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<-8.431600,-1.535000,26.065500>}
box{<0,0,-0.203200><0.464793,0.035000,0.203200> rotate<0,-67.491366,0> translate<-8.609500,-1.535000,25.636100> }
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<-8.609500,-1.535000,28.483800>}
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<-8.431600,-1.535000,28.054400>}
box{<0,0,-0.203200><0.464793,0.035000,0.203200> rotate<0,67.491366,0> translate<-8.609500,-1.535000,28.483800> }
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<-8.594600,-1.535000,28.448000>}
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<-8.175200,-1.535000,28.448000>}
box{<0,0,-0.203200><0.419400,0.035000,0.203200> rotate<0,0.000000,0> translate<-8.594600,-1.535000,28.448000> }
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<-8.589400,-1.535000,16.325200>}
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<-8.465800,-1.535000,16.623600>}
box{<0,0,-0.203200><0.322985,0.035000,0.203200> rotate<0,-67.495763,0> translate<-8.589400,-1.535000,16.325200> }
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<-8.520900,-1.535000,24.233300>}
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<-8.020900,-1.535000,24.733300>}
box{<0,0,-0.203200><0.707107,0.035000,0.203200> rotate<0,-44.997030,0> translate<-8.520900,-1.535000,24.233300> }
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<-8.474700,-1.535000,21.210500>}
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<-8.166700,-1.535000,21.338100>}
box{<0,0,-0.203200><0.333385,0.035000,0.203200> rotate<0,-22.502043,0> translate<-8.474700,-1.535000,21.210500> }
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<-8.465800,-1.535000,16.623600>}
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<-8.425900,-1.535000,16.640100>}
box{<0,0,-0.203200><0.043177,0.035000,0.203200> rotate<0,-22.465267,0> translate<-8.465800,-1.535000,16.623600> }
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<-8.454800,-1.535000,26.009600>}
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<-8.315200,-1.535000,26.009600>}
box{<0,0,-0.203200><0.139600,0.035000,0.203200> rotate<0,0.000000,0> translate<-8.454800,-1.535000,26.009600> }
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<-8.431600,-1.535000,26.065500>}
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<-8.431600,-1.535000,28.054400>}
box{<0,0,-0.203200><1.988900,0.035000,0.203200> rotate<0,90.000000,0> translate<-8.431600,-1.535000,28.054400> }
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<-8.431600,-1.535000,26.416000>}
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<-8.329900,-1.535000,26.416000>}
box{<0,0,-0.203200><0.101700,0.035000,0.203200> rotate<0,0.000000,0> translate<-8.431600,-1.535000,26.416000> }
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<-8.431600,-1.535000,26.822400>}
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<-8.329900,-1.535000,26.822400>}
box{<0,0,-0.203200><0.101700,0.035000,0.203200> rotate<0,0.000000,0> translate<-8.431600,-1.535000,26.822400> }
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<-8.431600,-1.535000,27.228800>}
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<-8.329900,-1.535000,27.228800>}
box{<0,0,-0.203200><0.101700,0.035000,0.203200> rotate<0,0.000000,0> translate<-8.431600,-1.535000,27.228800> }
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<-8.431600,-1.535000,27.635200>}
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<-8.329900,-1.535000,27.635200>}
box{<0,0,-0.203200><0.101700,0.035000,0.203200> rotate<0,0.000000,0> translate<-8.431600,-1.535000,27.635200> }
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<-8.431600,-1.535000,28.041600>}
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<-8.329900,-1.535000,28.041600>}
box{<0,0,-0.203200><0.101700,0.035000,0.203200> rotate<0,0.000000,0> translate<-8.431600,-1.535000,28.041600> }
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<-8.425900,-1.535000,16.640100>}
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<-8.325900,-1.535000,16.740100>}
box{<0,0,-0.203200><0.141421,0.035000,0.203200> rotate<0,-44.997030,0> translate<-8.425900,-1.535000,16.640100> }
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<-8.403600,-1.535000,16.662400>}
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<-5.096300,-1.535000,16.662400>}
box{<0,0,-0.203200><3.307300,0.035000,0.203200> rotate<0,0.000000,0> translate<-8.403600,-1.535000,16.662400> }
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<-8.329900,-1.535000,26.045300>}
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<-8.136600,-1.535000,25.578600>}
box{<0,0,-0.203200><0.505147,0.035000,0.203200> rotate<0,67.496957,0> translate<-8.329900,-1.535000,26.045300> }
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<-8.329900,-1.535000,27.009000>}
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<-8.329900,-1.535000,26.045300>}
box{<0,0,-0.203200><0.963700,0.035000,0.203200> rotate<0,-90.000000,0> translate<-8.329900,-1.535000,26.045300> }
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<-8.329900,-1.535000,27.009000>}
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<-7.111400,-1.535000,27.009000>}
box{<0,0,-0.203200><1.218500,0.035000,0.203200> rotate<0,0.000000,0> translate<-8.329900,-1.535000,27.009000> }
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<-8.329900,-1.535000,27.110800>}
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<-7.111400,-1.535000,27.110800>}
box{<0,0,-0.203200><1.218500,0.035000,0.203200> rotate<0,0.000000,0> translate<-8.329900,-1.535000,27.110800> }
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<-8.329900,-1.535000,28.074600>}
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<-8.329900,-1.535000,27.110800>}
box{<0,0,-0.203200><0.963800,0.035000,0.203200> rotate<0,-90.000000,0> translate<-8.329900,-1.535000,27.110800> }
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<-8.329900,-1.535000,28.074600>}
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<-8.136600,-1.535000,28.541300>}
box{<0,0,-0.203200><0.505147,0.035000,0.203200> rotate<0,-67.496957,0> translate<-8.329900,-1.535000,28.074600> }
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<-8.325900,-1.535000,16.740100>}
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<-8.140100,-1.535000,16.925900>}
box{<0,0,-0.203200><0.262761,0.035000,0.203200> rotate<0,-44.997030,0> translate<-8.325900,-1.535000,16.740100> }
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<-8.179000,-1.535000,22.666600>}
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<-7.766600,-1.535000,23.079000>}
box{<0,0,-0.203200><0.583222,0.035000,0.203200> rotate<0,-44.997030,0> translate<-8.179000,-1.535000,22.666600> }
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<-8.166700,-1.535000,21.338100>}
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<-7.838100,-1.535000,21.338100>}
box{<0,0,-0.203200><0.328600,0.035000,0.203200> rotate<0,0.000000,0> translate<-8.166700,-1.535000,21.338100> }
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<-8.140100,-1.535000,16.925900>}
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<-8.039600,-1.535000,17.168600>}
box{<0,0,-0.203200><0.262685,0.035000,0.203200> rotate<0,-67.501516,0> translate<-8.140100,-1.535000,16.925900> }
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<-8.136600,-1.535000,25.578600>}
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<-7.779300,-1.535000,25.221300>}
box{<0,0,-0.203200><0.505299,0.035000,0.203200> rotate<0,44.997030,0> translate<-8.136600,-1.535000,25.578600> }
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<-8.136600,-1.535000,28.541300>}
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<-7.779300,-1.535000,28.898600>}
box{<0,0,-0.203200><0.505299,0.035000,0.203200> rotate<0,-44.997030,0> translate<-8.136600,-1.535000,28.541300> }
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<-8.087200,-1.535000,22.758400>}
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<-6.006300,-1.535000,22.758400>}
box{<0,0,-0.203200><2.080900,0.035000,0.203200> rotate<0,0.000000,0> translate<-8.087200,-1.535000,22.758400> }
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<-8.081000,-1.535000,17.068800>}
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<-5.502700,-1.535000,17.068800>}
box{<0,0,-0.203200><2.578300,0.035000,0.203200> rotate<0,0.000000,0> translate<-8.081000,-1.535000,17.068800> }
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<-8.039600,-1.535000,17.168600>}
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<-8.039600,-1.535000,18.393100>}
box{<0,0,-0.203200><1.224500,0.035000,0.203200> rotate<0,90.000000,0> translate<-8.039600,-1.535000,18.393100> }
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<-8.039600,-1.535000,17.475200>}
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<-5.821700,-1.535000,17.475200>}
box{<0,0,-0.203200><2.217900,0.035000,0.203200> rotate<0,0.000000,0> translate<-8.039600,-1.535000,17.475200> }
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<-8.039600,-1.535000,17.881600>}
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<-5.860300,-1.535000,17.881600>}
box{<0,0,-0.203200><2.179300,0.035000,0.203200> rotate<0,0.000000,0> translate<-8.039600,-1.535000,17.881600> }
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<-8.039600,-1.535000,18.288000>}
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<-5.978000,-1.535000,18.288000>}
box{<0,0,-0.203200><2.061600,0.035000,0.203200> rotate<0,0.000000,0> translate<-8.039600,-1.535000,18.288000> }
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<-8.039600,-1.535000,18.393100>}
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<-7.925900,-1.535000,18.440100>}
box{<0,0,-0.203200><0.123031,0.035000,0.203200> rotate<0,-22.457178,0> translate<-8.039600,-1.535000,18.393100> }
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<-8.020900,-1.535000,24.733300>}
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<-7.579000,-1.535000,24.733300>}
box{<0,0,-0.203200><0.441900,0.035000,0.203200> rotate<0,0.000000,0> translate<-8.020900,-1.535000,24.733300> }
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<-7.925900,-1.535000,18.440100>}
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<-7.896600,-1.535000,18.469500>}
box{<0,0,-0.203200><0.041507,0.035000,0.203200> rotate<0,-45.094631,0> translate<-7.925900,-1.535000,18.440100> }
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<-7.896600,-1.535000,18.469500>}
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<-7.725900,-1.535000,18.540100>}
box{<0,0,-0.203200><0.184724,0.035000,0.203200> rotate<0,-22.468069,0> translate<-7.896600,-1.535000,18.469500> }
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<-7.838100,-1.535000,21.466700>}
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<-7.838100,-1.535000,21.338100>}
box{<0,0,-0.203200><0.128600,0.035000,0.203200> rotate<0,-90.000000,0> translate<-7.838100,-1.535000,21.338100> }
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<-7.838100,-1.535000,21.466700>}
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<-7.710500,-1.535000,21.774700>}
box{<0,0,-0.203200><0.333385,0.035000,0.203200> rotate<0,-67.492017,0> translate<-7.838100,-1.535000,21.466700> }
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<-7.779300,-1.535000,25.221300>}
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<-7.312600,-1.535000,25.028000>}
box{<0,0,-0.203200><0.505147,0.035000,0.203200> rotate<0,22.497104,0> translate<-7.779300,-1.535000,25.221300> }
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<-7.779300,-1.535000,28.898600>}
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<-7.312600,-1.535000,29.091900>}
box{<0,0,-0.203200><0.505147,0.035000,0.203200> rotate<0,-22.497104,0> translate<-7.779300,-1.535000,28.898600> }
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<-7.766600,-1.535000,23.079000>}
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<-7.766600,-1.535000,23.479100>}
box{<0,0,-0.203200><0.400100,0.035000,0.203200> rotate<0,90.000000,0> translate<-7.766600,-1.535000,23.479100> }
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<-7.766600,-1.535000,23.164800>}
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<-6.006300,-1.535000,23.164800>}
box{<0,0,-0.203200><1.760300,0.035000,0.203200> rotate<0,0.000000,0> translate<-7.766600,-1.535000,23.164800> }
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<-7.766600,-1.535000,23.479100>}
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<-7.579000,-1.535000,23.666600>}
box{<0,0,-0.203200><0.265236,0.035000,0.203200> rotate<0,-44.981756,0> translate<-7.766600,-1.535000,23.479100> }
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<-7.725900,-1.535000,18.540100>}
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<-7.696600,-1.535000,18.569400>}
box{<0,0,-0.203200><0.041436,0.035000,0.203200> rotate<0,-44.997030,0> translate<-7.725900,-1.535000,18.540100> }
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<-7.710500,-1.535000,21.774700>}
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<-7.474700,-1.535000,22.010500>}
box{<0,0,-0.203200><0.333472,0.035000,0.203200> rotate<0,-44.997030,0> translate<-7.710500,-1.535000,21.774700> }
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<-7.696600,-1.535000,18.569400>}
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<-7.525900,-1.535000,18.640100>}
box{<0,0,-0.203200><0.184762,0.035000,0.203200> rotate<0,-22.496723,0> translate<-7.696600,-1.535000,18.569400> }
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<-7.674500,-1.535000,23.571200>}
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<-6.006300,-1.535000,23.571200>}
box{<0,0,-0.203200><1.668200,0.035000,0.203200> rotate<0,0.000000,0> translate<-7.674500,-1.535000,23.571200> }
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<-7.579000,-1.535000,23.666600>}
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<-6.279000,-1.535000,23.666600>}
box{<0,0,-0.203200><1.300000,0.035000,0.203200> rotate<0,0.000000,0> translate<-7.579000,-1.535000,23.666600> }
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<-7.579000,-1.535000,24.733300>}
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<-6.720900,-1.535000,24.733300>}
box{<0,0,-0.203200><0.858100,0.035000,0.203200> rotate<0,0.000000,0> translate<-7.579000,-1.535000,24.733300> }
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<-7.525900,-1.535000,18.640100>}
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<-7.496600,-1.535000,18.669400>}
box{<0,0,-0.203200><0.041436,0.035000,0.203200> rotate<0,-44.997030,0> translate<-7.525900,-1.535000,18.640100> }
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<-7.496600,-1.535000,18.669400>}
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<-7.325900,-1.535000,18.740100>}
box{<0,0,-0.203200><0.184762,0.035000,0.203200> rotate<0,-22.496723,0> translate<-7.496600,-1.535000,18.669400> }
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<-7.474700,-1.535000,22.010500>}
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<-7.166700,-1.535000,22.138100>}
box{<0,0,-0.203200><0.333385,0.035000,0.203200> rotate<0,-22.502043,0> translate<-7.474700,-1.535000,22.010500> }
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<-7.436300,-1.535000,18.694400>}
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<-6.038100,-1.535000,18.694400>}
box{<0,0,-0.203200><1.398200,0.035000,0.203200> rotate<0,0.000000,0> translate<-7.436300,-1.535000,18.694400> }
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<-7.325900,-1.535000,18.740100>}
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<-7.296600,-1.535000,18.769400>}
box{<0,0,-0.203200><0.041436,0.035000,0.203200> rotate<0,-44.997030,0> translate<-7.325900,-1.535000,18.740100> }
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<-7.312600,-1.535000,25.028000>}
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<-7.110800,-1.535000,25.028000>}
box{<0,0,-0.203200><0.201800,0.035000,0.203200> rotate<0,0.000000,0> translate<-7.312600,-1.535000,25.028000> }
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<-7.312600,-1.535000,29.091900>}
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<-7.110800,-1.535000,29.091900>}
box{<0,0,-0.203200><0.201800,0.035000,0.203200> rotate<0,0.000000,0> translate<-7.312600,-1.535000,29.091900> }
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<-7.296600,-1.535000,18.769400>}
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<-7.125900,-1.535000,18.840100>}
box{<0,0,-0.203200><0.184762,0.035000,0.203200> rotate<0,-22.496723,0> translate<-7.296600,-1.535000,18.769400> }
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<-7.166700,-1.535000,22.138100>}
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<-6.833200,-1.535000,22.138100>}
box{<0,0,-0.203200><0.333500,0.035000,0.203200> rotate<0,0.000000,0> translate<-7.166700,-1.535000,22.138100> }
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<-7.125900,-1.535000,18.840100>}
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<-7.096600,-1.535000,18.869400>}
box{<0,0,-0.203200><0.041436,0.035000,0.203200> rotate<0,-44.997030,0> translate<-7.125900,-1.535000,18.840100> }
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<-7.111400,-1.535000,27.110800>}
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<-7.111400,-1.535000,27.009000>}
box{<0,0,-0.203200><0.101800,0.035000,0.203200> rotate<0,-90.000000,0> translate<-7.111400,-1.535000,27.009000> }
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<-7.110800,-1.535000,25.028000>}
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<-7.110800,-1.535000,27.008500>}
box{<0,0,-0.203200><1.980500,0.035000,0.203200> rotate<0,90.000000,0> translate<-7.110800,-1.535000,27.008500> }
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<-7.110800,-1.535000,25.196800>}
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<-7.009000,-1.535000,25.196800>}
box{<0,0,-0.203200><0.101800,0.035000,0.203200> rotate<0,0.000000,0> translate<-7.110800,-1.535000,25.196800> }
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<-7.110800,-1.535000,25.603200>}
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<-7.009000,-1.535000,25.603200>}
box{<0,0,-0.203200><0.101800,0.035000,0.203200> rotate<0,0.000000,0> translate<-7.110800,-1.535000,25.603200> }
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<-7.110800,-1.535000,26.009600>}
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<-7.009000,-1.535000,26.009600>}
box{<0,0,-0.203200><0.101800,0.035000,0.203200> rotate<0,0.000000,0> translate<-7.110800,-1.535000,26.009600> }
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<-7.110800,-1.535000,26.416000>}
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<-7.009000,-1.535000,26.416000>}
box{<0,0,-0.203200><0.101800,0.035000,0.203200> rotate<0,0.000000,0> translate<-7.110800,-1.535000,26.416000> }
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<-7.110800,-1.535000,26.822400>}
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<-7.009000,-1.535000,26.822400>}
box{<0,0,-0.203200><0.101800,0.035000,0.203200> rotate<0,0.000000,0> translate<-7.110800,-1.535000,26.822400> }
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<-7.110800,-1.535000,27.008500>}
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<-7.009000,-1.535000,27.008500>}
box{<0,0,-0.203200><0.101800,0.035000,0.203200> rotate<0,0.000000,0> translate<-7.110800,-1.535000,27.008500> }
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<-7.110800,-1.535000,27.111400>}
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<-7.110800,-1.535000,29.091900>}
box{<0,0,-0.203200><1.980500,0.035000,0.203200> rotate<0,90.000000,0> translate<-7.110800,-1.535000,29.091900> }
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<-7.110800,-1.535000,27.111400>}
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<-7.009000,-1.535000,27.111400>}
box{<0,0,-0.203200><0.101800,0.035000,0.203200> rotate<0,0.000000,0> translate<-7.110800,-1.535000,27.111400> }
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<-7.110800,-1.535000,27.228800>}
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<-7.009000,-1.535000,27.228800>}
box{<0,0,-0.203200><0.101800,0.035000,0.203200> rotate<0,0.000000,0> translate<-7.110800,-1.535000,27.228800> }
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<-7.110800,-1.535000,27.635200>}
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<-7.009000,-1.535000,27.635200>}
box{<0,0,-0.203200><0.101800,0.035000,0.203200> rotate<0,0.000000,0> translate<-7.110800,-1.535000,27.635200> }
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<-7.110800,-1.535000,28.041600>}
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<-7.009000,-1.535000,28.041600>}
box{<0,0,-0.203200><0.101800,0.035000,0.203200> rotate<0,0.000000,0> translate<-7.110800,-1.535000,28.041600> }
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<-7.110800,-1.535000,28.448000>}
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<-7.009000,-1.535000,28.448000>}
box{<0,0,-0.203200><0.101800,0.035000,0.203200> rotate<0,0.000000,0> translate<-7.110800,-1.535000,28.448000> }
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<-7.110800,-1.535000,28.854400>}
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<-7.009000,-1.535000,28.854400>}
box{<0,0,-0.203200><0.101800,0.035000,0.203200> rotate<0,0.000000,0> translate<-7.110800,-1.535000,28.854400> }
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<-7.096600,-1.535000,18.869400>}
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<-6.925900,-1.535000,18.940100>}
box{<0,0,-0.203200><0.184762,0.035000,0.203200> rotate<0,-22.496723,0> translate<-7.096600,-1.535000,18.869400> }
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<-7.009000,-1.535000,25.028000>}
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<-6.807300,-1.535000,25.028000>}
box{<0,0,-0.203200><0.201700,0.035000,0.203200> rotate<0,0.000000,0> translate<-7.009000,-1.535000,25.028000> }
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<-7.009000,-1.535000,27.008500>}
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<-7.009000,-1.535000,25.028000>}
box{<0,0,-0.203200><1.980500,0.035000,0.203200> rotate<0,-90.000000,0> translate<-7.009000,-1.535000,25.028000> }
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<-7.009000,-1.535000,29.091900>}
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<-7.009000,-1.535000,27.111400>}
box{<0,0,-0.203200><1.980500,0.035000,0.203200> rotate<0,-90.000000,0> translate<-7.009000,-1.535000,27.111400> }
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<-7.009000,-1.535000,29.091900>}
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<-6.807300,-1.535000,29.091900>}
box{<0,0,-0.203200><0.201700,0.035000,0.203200> rotate<0,0.000000,0> translate<-7.009000,-1.535000,29.091900> }
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<-7.008500,-1.535000,27.009000>}
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<-7.008500,-1.535000,27.110800>}
box{<0,0,-0.203200><0.101800,0.035000,0.203200> rotate<0,90.000000,0> translate<-7.008500,-1.535000,27.110800> }
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<-7.008500,-1.535000,27.009000>}
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<-5.790000,-1.535000,27.009000>}
box{<0,0,-0.203200><1.218500,0.035000,0.203200> rotate<0,0.000000,0> translate<-7.008500,-1.535000,27.009000> }
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<-7.008500,-1.535000,27.110800>}
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<-5.790000,-1.535000,27.110800>}
box{<0,0,-0.203200><1.218500,0.035000,0.203200> rotate<0,0.000000,0> translate<-7.008500,-1.535000,27.110800> }
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<-6.925900,-1.535000,18.940100>}
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<-6.896600,-1.535000,18.969400>}
box{<0,0,-0.203200><0.041436,0.035000,0.203200> rotate<0,-44.997030,0> translate<-6.925900,-1.535000,18.940100> }
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<-6.896600,-1.535000,18.969400>}
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<-6.725900,-1.535000,19.040100>}
box{<0,0,-0.203200><0.184762,0.035000,0.203200> rotate<0,-22.496723,0> translate<-6.896600,-1.535000,18.969400> }
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<-6.833200,-1.535000,22.138100>}
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<-6.750400,-1.535000,22.103800>}
box{<0,0,-0.203200><0.089623,0.035000,0.203200> rotate<0,22.500356,0> translate<-6.833200,-1.535000,22.138100> }
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<-6.807300,-1.535000,25.028000>}
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<-6.340600,-1.535000,25.221300>}
box{<0,0,-0.203200><0.505147,0.035000,0.203200> rotate<0,-22.497104,0> translate<-6.807300,-1.535000,25.028000> }
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<-6.807300,-1.535000,29.091900>}
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<-6.340600,-1.535000,28.898600>}
box{<0,0,-0.203200><0.505147,0.035000,0.203200> rotate<0,22.497104,0> translate<-6.807300,-1.535000,29.091900> }
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<-6.750400,-1.535000,22.103800>}
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<-6.720900,-1.535000,22.133300>}
box{<0,0,-0.203200><0.041719,0.035000,0.203200> rotate<0,-44.997030,0> translate<-6.750400,-1.535000,22.103800> }
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<-6.725900,-1.535000,19.040100>}
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<-6.696600,-1.535000,19.069400>}
box{<0,0,-0.203200><0.041436,0.035000,0.203200> rotate<0,-44.997030,0> translate<-6.725900,-1.535000,19.040100> }
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<-6.720900,-1.535000,22.133300>}
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<-6.279000,-1.535000,22.133300>}
box{<0,0,-0.203200><0.441900,0.035000,0.203200> rotate<0,0.000000,0> translate<-6.720900,-1.535000,22.133300> }
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<-6.720900,-1.535000,24.733300>}
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<-6.433300,-1.535000,25.020800>}
box{<0,0,-0.203200><0.406657,0.035000,0.203200> rotate<0,-44.987068,0> translate<-6.720900,-1.535000,24.733300> }
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<-6.696600,-1.535000,19.069400>}
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<-6.525900,-1.535000,19.140100>}
box{<0,0,-0.203200><0.184762,0.035000,0.203200> rotate<0,-22.496723,0> translate<-6.696600,-1.535000,19.069400> }
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<-6.620800,-1.535000,19.100800>}
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<-5.884400,-1.535000,19.100800>}
box{<0,0,-0.203200><0.736400,0.035000,0.203200> rotate<0,0.000000,0> translate<-6.620800,-1.535000,19.100800> }
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<-6.525900,-1.535000,19.140100>}
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<-6.496600,-1.535000,19.169400>}
box{<0,0,-0.203200><0.041436,0.035000,0.203200> rotate<0,-44.997030,0> translate<-6.525900,-1.535000,19.140100> }
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<-6.496600,-1.535000,19.169400>}
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<-6.325900,-1.535000,19.240100>}
box{<0,0,-0.203200><0.184762,0.035000,0.203200> rotate<0,-22.496723,0> translate<-6.496600,-1.535000,19.169400> }
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<-6.433300,-1.535000,25.020900>}
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<-6.433300,-1.535000,25.020800>}
box{<0,0,-0.203200><0.000100,0.035000,0.203200> rotate<0,-90.000000,0> translate<-6.433300,-1.535000,25.020800> }
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<-6.433300,-1.535000,25.020900>}
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<-6.120900,-1.535000,25.333300>}
box{<0,0,-0.203200><0.441800,0.035000,0.203200> rotate<0,-44.997030,0> translate<-6.433300,-1.535000,25.020900> }
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<-6.399800,-1.535000,25.196800>}
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<-6.257400,-1.535000,25.196800>}
box{<0,0,-0.203200><0.142400,0.035000,0.203200> rotate<0,0.000000,0> translate<-6.399800,-1.535000,25.196800> }
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<-6.340600,-1.535000,25.221300>}
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<-5.983300,-1.535000,25.578600>}
box{<0,0,-0.203200><0.505299,0.035000,0.203200> rotate<0,-44.997030,0> translate<-6.340600,-1.535000,25.221300> }
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<-6.340600,-1.535000,28.898600>}
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<-5.983300,-1.535000,28.541300>}
box{<0,0,-0.203200><0.505299,0.035000,0.203200> rotate<0,44.997030,0> translate<-6.340600,-1.535000,28.898600> }
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<-6.325900,-1.535000,19.240100>}
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<-6.296500,-1.535000,19.269500>}
box{<0,0,-0.203200><0.041578,0.035000,0.203200> rotate<0,-44.997030,0> translate<-6.325900,-1.535000,19.240100> }
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<-6.296500,-1.535000,19.269500>}
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<-6.125900,-1.535000,19.340100>}
box{<0,0,-0.203200><0.184631,0.035000,0.203200> rotate<0,-22.479928,0> translate<-6.296500,-1.535000,19.269500> }
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<-6.296400,-1.535000,28.854400>}
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<-0.147600,-1.535000,28.854400>}
box{<0,0,-0.203200><6.148800,0.035000,0.203200> rotate<0,0.000000,0> translate<-6.296400,-1.535000,28.854400> }
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<-6.279000,-1.535000,22.133300>}
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<-6.006300,-1.535000,22.133300>}
box{<0,0,-0.203200><0.272700,0.035000,0.203200> rotate<0,0.000000,0> translate<-6.279000,-1.535000,22.133300> }
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<-6.279000,-1.535000,23.666600>}
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<-6.006300,-1.535000,23.939400>}
box{<0,0,-0.203200><0.385727,0.035000,0.203200> rotate<0,-45.007533,0> translate<-6.279000,-1.535000,23.666600> }
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<-6.125900,-1.535000,19.340100>}
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<-6.096600,-1.535000,19.369500>}
box{<0,0,-0.203200><0.041507,0.035000,0.203200> rotate<0,-45.094631,0> translate<-6.125900,-1.535000,19.340100> }
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<-6.120900,-1.535000,25.333300>}
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<-1.120900,-1.535000,25.333300>}
box{<0,0,-0.203200><5.000000,0.035000,0.203200> rotate<0,0.000000,0> translate<-6.120900,-1.535000,25.333300> }
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<-6.096600,-1.535000,19.369500>}
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<-5.925900,-1.535000,19.440100>}
box{<0,0,-0.203200><0.184724,0.035000,0.203200> rotate<0,-22.468069,0> translate<-6.096600,-1.535000,19.369500> }
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<-6.038100,-1.535000,18.433200>}
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<-5.910500,-1.535000,18.125200>}
box{<0,0,-0.203200><0.333385,0.035000,0.203200> rotate<0,67.492017,0> translate<-6.038100,-1.535000,18.433200> }
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<-6.038100,-1.535000,18.766700>}
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<-6.038100,-1.535000,18.433200>}
box{<0,0,-0.203200><0.333500,0.035000,0.203200> rotate<0,-90.000000,0> translate<-6.038100,-1.535000,18.433200> }
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<-6.038100,-1.535000,18.766700>}
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<-5.910500,-1.535000,19.074700>}
box{<0,0,-0.203200><0.333385,0.035000,0.203200> rotate<0,-67.492017,0> translate<-6.038100,-1.535000,18.766700> }
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<-6.006300,-1.535000,23.939400>}
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<-6.006300,-1.535000,22.133300>}
box{<0,0,-0.203200><1.806100,0.035000,0.203200> rotate<0,-90.000000,0> translate<-6.006300,-1.535000,22.133300> }
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<-5.983300,-1.535000,25.578600>}
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<-5.790000,-1.535000,26.045300>}
box{<0,0,-0.203200><0.505147,0.035000,0.203200> rotate<0,-67.496957,0> translate<-5.983300,-1.535000,25.578600> }
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<-5.983300,-1.535000,28.541300>}
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<-5.790000,-1.535000,28.074600>}
box{<0,0,-0.203200><0.505147,0.035000,0.203200> rotate<0,67.496957,0> translate<-5.983300,-1.535000,28.541300> }
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<-5.973200,-1.535000,25.603200>}
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<3.323500,-1.535000,25.603200>}
box{<0,0,-0.203200><9.296700,0.035000,0.203200> rotate<0,0.000000,0> translate<-5.973200,-1.535000,25.603200> }
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<-5.944600,-1.535000,28.448000>}
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<-0.311100,-1.535000,28.448000>}
box{<0,0,-0.203200><5.633500,0.035000,0.203200> rotate<0,0.000000,0> translate<-5.944600,-1.535000,28.448000> }
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<-5.925900,-1.535000,19.440100>}
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<-5.896600,-1.535000,19.469500>}
box{<0,0,-0.203200><0.041507,0.035000,0.203200> rotate<0,-45.094631,0> translate<-5.925900,-1.535000,19.440100> }
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<-5.910500,-1.535000,18.125200>}
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<-5.860300,-1.535000,18.075000>}
box{<0,0,-0.203200><0.070994,0.035000,0.203200> rotate<0,44.997030,0> translate<-5.910500,-1.535000,18.125200> }
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<-5.910500,-1.535000,19.074700>}
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<-5.674700,-1.535000,19.310500>}
box{<0,0,-0.203200><0.333472,0.035000,0.203200> rotate<0,-44.997030,0> translate<-5.910500,-1.535000,19.074700> }
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<-5.896600,-1.535000,19.469500>}
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<-5.727200,-1.535000,19.539600>}
box{<0,0,-0.203200><0.183331,0.035000,0.203200> rotate<0,-22.478947,0> translate<-5.896600,-1.535000,19.469500> }
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<-5.860300,-1.535000,17.568600>}
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<-5.759800,-1.535000,17.325900>}
box{<0,0,-0.203200><0.262685,0.035000,0.203200> rotate<0,67.501516,0> translate<-5.860300,-1.535000,17.568600> }
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<-5.860300,-1.535000,18.075000>}
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<-5.860300,-1.535000,17.568600>}
box{<0,0,-0.203200><0.506400,0.035000,0.203200> rotate<0,-90.000000,0> translate<-5.860300,-1.535000,17.568600> }
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<-5.805500,-1.535000,19.507200>}
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<1.966600,-1.535000,19.507200>}
box{<0,0,-0.203200><7.772100,0.035000,0.203200> rotate<0,0.000000,0> translate<-5.805500,-1.535000,19.507200> }
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<-5.804800,-1.535000,26.009600>}
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<0.765600,-1.535000,26.009600>}
box{<0,0,-0.203200><6.570400,0.035000,0.203200> rotate<0,0.000000,0> translate<-5.804800,-1.535000,26.009600> }
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<-5.790000,-1.535000,26.045300>}
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<-5.790000,-1.535000,27.009000>}
box{<0,0,-0.203200><0.963700,0.035000,0.203200> rotate<0,90.000000,0> translate<-5.790000,-1.535000,27.009000> }
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<-5.790000,-1.535000,26.416000>}
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<0.315400,-1.535000,26.416000>}
box{<0,0,-0.203200><6.105400,0.035000,0.203200> rotate<0,0.000000,0> translate<-5.790000,-1.535000,26.416000> }
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<-5.790000,-1.535000,26.822400>}
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<-0.013900,-1.535000,26.822400>}
box{<0,0,-0.203200><5.776100,0.035000,0.203200> rotate<0,0.000000,0> translate<-5.790000,-1.535000,26.822400> }
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<-5.790000,-1.535000,27.110800>}
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<-5.790000,-1.535000,28.074600>}
box{<0,0,-0.203200><0.963800,0.035000,0.203200> rotate<0,90.000000,0> translate<-5.790000,-1.535000,28.074600> }
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<-5.790000,-1.535000,27.228800>}
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<-0.182200,-1.535000,27.228800>}
box{<0,0,-0.203200><5.607800,0.035000,0.203200> rotate<0,0.000000,0> translate<-5.790000,-1.535000,27.228800> }
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<-5.790000,-1.535000,27.635200>}
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<-0.311100,-1.535000,27.635200>}
box{<0,0,-0.203200><5.478900,0.035000,0.203200> rotate<0,0.000000,0> translate<-5.790000,-1.535000,27.635200> }
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<-5.790000,-1.535000,28.041600>}
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<-0.311100,-1.535000,28.041600>}
box{<0,0,-0.203200><5.478900,0.035000,0.203200> rotate<0,0.000000,0> translate<-5.790000,-1.535000,28.041600> }
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<-5.759800,-1.535000,17.325900>}
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<-5.574000,-1.535000,17.140100>}
box{<0,0,-0.203200><0.262761,0.035000,0.203200> rotate<0,44.997030,0> translate<-5.759800,-1.535000,17.325900> }
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<-5.727200,-1.535000,19.539600>}
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<1.468600,-1.535000,19.539600>}
box{<0,0,-0.203200><7.195800,0.035000,0.203200> rotate<0,0.000000,0> translate<-5.727200,-1.535000,19.539600> }
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<-5.674700,-1.535000,19.310500>}
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<-5.366700,-1.535000,19.438100>}
box{<0,0,-0.203200><0.333385,0.035000,0.203200> rotate<0,-22.502043,0> translate<-5.674700,-1.535000,19.310500> }
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<-5.574000,-1.535000,17.140100>}
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<-4.738100,-1.535000,16.304200>}
box{<0,0,-0.203200><1.182141,0.035000,0.203200> rotate<0,44.997030,0> translate<-5.574000,-1.535000,17.140100> }
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<-5.366700,-1.535000,19.438100>}
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<-5.033200,-1.535000,19.438100>}
box{<0,0,-0.203200><0.333500,0.035000,0.203200> rotate<0,0.000000,0> translate<-5.366700,-1.535000,19.438100> }
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<-5.033200,-1.535000,19.438100>}
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<-4.725200,-1.535000,19.310500>}
box{<0,0,-0.203200><0.333385,0.035000,0.203200> rotate<0,22.502043,0> translate<-5.033200,-1.535000,19.438100> }
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<-4.738100,-1.535000,16.233200>}
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<-4.613800,-1.535000,15.933300>}
box{<0,0,-0.203200><0.324639,0.035000,0.203200> rotate<0,67.482933,0> translate<-4.738100,-1.535000,16.233200> }
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<-4.738100,-1.535000,16.304200>}
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<-4.738100,-1.535000,16.233200>}
box{<0,0,-0.203200><0.071000,0.035000,0.203200> rotate<0,-90.000000,0> translate<-4.738100,-1.535000,16.233200> }
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<-4.725200,-1.535000,19.310500>}
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<-4.489400,-1.535000,19.074700>}
box{<0,0,-0.203200><0.333472,0.035000,0.203200> rotate<0,44.997030,0> translate<-4.725200,-1.535000,19.310500> }
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<-4.539600,-1.535000,17.973500>}
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<-4.539600,-1.535000,18.075000>}
box{<0,0,-0.203200><0.101500,0.035000,0.203200> rotate<0,90.000000,0> translate<-4.539600,-1.535000,18.075000> }
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<-4.539600,-1.535000,17.973500>}
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<-3.804200,-1.535000,17.238100>}
box{<0,0,-0.203200><1.040013,0.035000,0.203200> rotate<0,44.997030,0> translate<-4.539600,-1.535000,17.973500> }
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<-4.539600,-1.535000,18.075000>}
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<-4.489400,-1.535000,18.125200>}
box{<0,0,-0.203200><0.070994,0.035000,0.203200> rotate<0,-44.997030,0> translate<-4.539600,-1.535000,18.075000> }
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<-4.515500,-1.535000,19.100800>}
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<2.144800,-1.535000,19.100800>}
box{<0,0,-0.203200><6.660300,0.035000,0.203200> rotate<0,0.000000,0> translate<-4.515500,-1.535000,19.100800> }
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<-4.489400,-1.535000,18.125200>}
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<-4.361800,-1.535000,18.433200>}
box{<0,0,-0.203200><0.333385,0.035000,0.203200> rotate<0,-67.492017,0> translate<-4.489400,-1.535000,18.125200> }
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<-4.489400,-1.535000,19.074700>}
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<-4.361800,-1.535000,18.766700>}
box{<0,0,-0.203200><0.333385,0.035000,0.203200> rotate<0,67.492017,0> translate<-4.489400,-1.535000,19.074700> }
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<-4.447700,-1.535000,17.881600>}
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<8.862300,-1.535000,17.881600>}
box{<0,0,-0.203200><13.310000,0.035000,0.203200> rotate<0,0.000000,0> translate<-4.447700,-1.535000,17.881600> }
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<-4.422000,-1.535000,18.288000>}
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<8.460000,-1.535000,18.288000>}
box{<0,0,-0.203200><12.882000,0.035000,0.203200> rotate<0,0.000000,0> translate<-4.422000,-1.535000,18.288000> }
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<-4.361800,-1.535000,18.433200>}
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<-4.361800,-1.535000,18.766700>}
box{<0,0,-0.203200><0.333500,0.035000,0.203200> rotate<0,90.000000,0> translate<-4.361800,-1.535000,18.766700> }
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<-4.361800,-1.535000,18.694400>}
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<2.551200,-1.535000,18.694400>}
box{<0,0,-0.203200><6.913000,0.035000,0.203200> rotate<0,0.000000,0> translate<-4.361800,-1.535000,18.694400> }
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<-4.041300,-1.535000,17.475200>}
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<9.139200,-1.535000,17.475200>}
box{<0,0,-0.203200><13.180500,0.035000,0.203200> rotate<0,0.000000,0> translate<-4.041300,-1.535000,17.475200> }
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<-3.804200,-1.535000,17.238100>}
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<-3.733200,-1.535000,17.238100>}
box{<0,0,-0.203200><0.071000,0.035000,0.203200> rotate<0,0.000000,0> translate<-3.804200,-1.535000,17.238100> }
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<-3.751900,-1.535000,14.866600>}
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<-3.574700,-1.535000,14.689400>}
box{<0,0,-0.203200><0.250599,0.035000,0.203200> rotate<0,44.997030,0> translate<-3.751900,-1.535000,14.866600> }
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<-3.733200,-1.535000,17.238100>}
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<-3.425200,-1.535000,17.110500>}
box{<0,0,-0.203200><0.333385,0.035000,0.203200> rotate<0,22.502043,0> translate<-3.733200,-1.535000,17.238100> }
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<-3.574700,-1.535000,14.689400>}
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<-3.266700,-1.535000,14.561800>}
box{<0,0,-0.203200><0.333385,0.035000,0.203200> rotate<0,22.502043,0> translate<-3.574700,-1.535000,14.689400> }
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<-3.425200,-1.535000,17.110500>}
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<-3.189400,-1.535000,16.874700>}
box{<0,0,-0.203200><0.333472,0.035000,0.203200> rotate<0,44.997030,0> translate<-3.425200,-1.535000,17.110500> }
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<-3.383500,-1.535000,17.068800>}
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<8.732800,-1.535000,17.068800>}
box{<0,0,-0.203200><12.116300,0.035000,0.203200> rotate<0,0.000000,0> translate<-3.383500,-1.535000,17.068800> }
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<-3.266700,-1.535000,14.561800>}
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<-2.933200,-1.535000,14.561800>}
box{<0,0,-0.203200><0.333500,0.035000,0.203200> rotate<0,0.000000,0> translate<-3.266700,-1.535000,14.561800> }
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<-3.189400,-1.535000,16.874700>}
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<-3.061800,-1.535000,16.566700>}
box{<0,0,-0.203200><0.333385,0.035000,0.203200> rotate<0,67.492017,0> translate<-3.189400,-1.535000,16.874700> }
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<-3.101400,-1.535000,16.662400>}
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<8.460000,-1.535000,16.662400>}
box{<0,0,-0.203200><11.561400,0.035000,0.203200> rotate<0,0.000000,0> translate<-3.101400,-1.535000,16.662400> }
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<-3.061800,-1.535000,16.238100>}
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<-3.061800,-1.535000,16.566700>}
box{<0,0,-0.203200><0.328600,0.035000,0.203200> rotate<0,90.000000,0> translate<-3.061800,-1.535000,16.566700> }
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<-3.061800,-1.535000,16.238100>}
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<-2.933200,-1.535000,16.238100>}
box{<0,0,-0.203200><0.128600,0.035000,0.203200> rotate<0,0.000000,0> translate<-3.061800,-1.535000,16.238100> }
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<-3.061800,-1.535000,16.256000>}
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<9.678500,-1.535000,16.256000>}
box{<0,0,-0.203200><12.740300,0.035000,0.203200> rotate<0,0.000000,0> translate<-3.061800,-1.535000,16.256000> }
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<-2.933200,-1.535000,14.561800>}
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<-2.625200,-1.535000,14.689400>}
box{<0,0,-0.203200><0.333385,0.035000,0.203200> rotate<0,-22.502043,0> translate<-2.933200,-1.535000,14.561800> }
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<-2.933200,-1.535000,16.238100>}
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<-2.625200,-1.535000,16.110500>}
box{<0,0,-0.203200><0.333385,0.035000,0.203200> rotate<0,22.502043,0> translate<-2.933200,-1.535000,16.238100> }
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<-2.767700,-1.535000,14.630400>}
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<8.834400,-1.535000,14.630400>}
box{<0,0,-0.203200><11.602100,0.035000,0.203200> rotate<0,0.000000,0> translate<-2.767700,-1.535000,14.630400> }
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<-2.625200,-1.535000,14.689400>}
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<-2.389400,-1.535000,14.925200>}
box{<0,0,-0.203200><0.333472,0.035000,0.203200> rotate<0,-44.997030,0> translate<-2.625200,-1.535000,14.689400> }
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<-2.625200,-1.535000,16.110500>}
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<-2.389400,-1.535000,15.874700>}
box{<0,0,-0.203200><0.333472,0.035000,0.203200> rotate<0,44.997030,0> translate<-2.625200,-1.535000,16.110500> }
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<-2.389400,-1.535000,14.925200>}
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<-2.261800,-1.535000,15.233200>}
box{<0,0,-0.203200><0.333385,0.035000,0.203200> rotate<0,-67.492017,0> translate<-2.389400,-1.535000,14.925200> }
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<-2.389400,-1.535000,15.874700>}
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<-2.261800,-1.535000,15.566700>}
box{<0,0,-0.203200><0.333385,0.035000,0.203200> rotate<0,67.492017,0> translate<-2.389400,-1.535000,15.874700> }
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<-2.379000,-1.535000,15.849600>}
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<8.460000,-1.535000,15.849600>}
box{<0,0,-0.203200><10.839000,0.035000,0.203200> rotate<0,0.000000,0> translate<-2.379000,-1.535000,15.849600> }
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<-2.343200,-1.535000,15.036800>}
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<9.167100,-1.535000,15.036800>}
box{<0,0,-0.203200><11.510300,0.035000,0.203200> rotate<0,0.000000,0> translate<-2.343200,-1.535000,15.036800> }
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<-2.261800,-1.535000,15.233200>}
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<-2.261800,-1.535000,15.566700>}
box{<0,0,-0.203200><0.333500,0.035000,0.203200> rotate<0,90.000000,0> translate<-2.261800,-1.535000,15.566700> }
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<-2.261800,-1.535000,15.443200>}
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<8.760700,-1.535000,15.443200>}
box{<0,0,-0.203200><11.022500,0.035000,0.203200> rotate<0,0.000000,0> translate<-2.261800,-1.535000,15.443200> }
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<-1.307900,-1.535000,11.589500>}
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<-1.010400,-1.535000,11.292000>}
box{<0,0,-0.203200><0.420729,0.035000,0.203200> rotate<0,44.997030,0> translate<-1.307900,-1.535000,11.589500> }
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<-1.307900,-1.535000,12.478000>}
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<-1.307900,-1.535000,11.589500>}
box{<0,0,-0.203200><0.888500,0.035000,0.203200> rotate<0,-90.000000,0> translate<-1.307900,-1.535000,11.589500> }
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<-1.307900,-1.535000,12.478000>}
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<-0.272400,-1.535000,12.478000>}
box{<0,0,-0.203200><1.035500,0.035000,0.203200> rotate<0,0.000000,0> translate<-1.307900,-1.535000,12.478000> }
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<-1.307900,-1.535000,12.721800>}
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<-0.272400,-1.535000,12.721800>}
box{<0,0,-0.203200><1.035500,0.035000,0.203200> rotate<0,0.000000,0> translate<-1.307900,-1.535000,12.721800> }
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<-1.307900,-1.535000,13.610400>}
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<-1.307900,-1.535000,12.721800>}
box{<0,0,-0.203200><0.888600,0.035000,0.203200> rotate<0,-90.000000,0> translate<-1.307900,-1.535000,12.721800> }
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<-1.307900,-1.535000,13.610400>}
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<-1.010400,-1.535000,13.907900>}
box{<0,0,-0.203200><0.420729,0.035000,0.203200> rotate<0,-44.997030,0> translate<-1.307900,-1.535000,13.610400> }
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<-1.120900,-1.535000,25.333300>}
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<-0.679000,-1.535000,25.333300>}
box{<0,0,-0.203200><0.441900,0.035000,0.203200> rotate<0,0.000000,0> translate<-1.120900,-1.535000,25.333300> }
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<-1.010400,-1.535000,11.292000>}
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<-0.271800,-1.535000,11.292000>}
box{<0,0,-0.203200><0.738600,0.035000,0.203200> rotate<0,0.000000,0> translate<-1.010400,-1.535000,11.292000> }
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<-1.010400,-1.535000,13.907900>}
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<-0.271800,-1.535000,13.907900>}
box{<0,0,-0.203200><0.738600,0.035000,0.203200> rotate<0,0.000000,0> translate<-1.010400,-1.535000,13.907900> }
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<-0.679000,-1.535000,25.333300>}
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<-0.579000,-1.535000,25.233300>}
box{<0,0,-0.203200><0.141421,0.035000,0.203200> rotate<0,44.997030,0> translate<-0.679000,-1.535000,25.333300> }
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<-0.579000,-1.535000,25.233300>}
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<-0.266600,-1.535000,24.920900>}
box{<0,0,-0.203200><0.441800,0.035000,0.203200> rotate<0,44.997030,0> translate<-0.579000,-1.535000,25.233300> }
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<-0.542500,-1.535000,25.196800>}
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<3.153600,-1.535000,25.196800>}
box{<0,0,-0.203200><3.696100,0.035000,0.203200> rotate<0,0.000000,0> translate<-0.542500,-1.535000,25.196800> }
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<-0.311100,-1.535000,1.540200>}
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<0.032300,-1.535000,0.711100>}
box{<0,0,-0.203200><0.897402,0.035000,0.203200> rotate<0,67.496988,0> translate<-0.311100,-1.535000,1.540200> }
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<-0.311100,-1.535000,2.459700>}
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<-0.311100,-1.535000,1.540200>}
box{<0,0,-0.203200><0.919500,0.035000,0.203200> rotate<0,-90.000000,0> translate<-0.311100,-1.535000,1.540200> }
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<-0.311100,-1.535000,2.459700>}
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<0.040600,-1.535000,3.309100>}
box{<0,0,-0.203200><0.919333,0.035000,0.203200> rotate<0,-67.503203,0> translate<-0.311100,-1.535000,2.459700> }
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<-0.311100,-1.535000,27.540200>}
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<0.040600,-1.535000,26.690800>}
box{<0,0,-0.203200><0.919333,0.035000,0.203200> rotate<0,67.503203,0> translate<-0.311100,-1.535000,27.540200> }
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<-0.311100,-1.535000,28.459700>}
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<-0.311100,-1.535000,27.540200>}
box{<0,0,-0.203200><0.919500,0.035000,0.203200> rotate<0,-90.000000,0> translate<-0.311100,-1.535000,27.540200> }
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<-0.311100,-1.535000,28.459700>}
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<0.032300,-1.535000,29.288800>}
box{<0,0,-0.203200><0.897402,0.035000,0.203200> rotate<0,-67.496988,0> translate<-0.311100,-1.535000,28.459700> }
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<-0.272400,-1.535000,12.721800>}
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<-0.272400,-1.535000,12.478000>}
box{<0,0,-0.203200><0.243800,0.035000,0.203200> rotate<0,-90.000000,0> translate<-0.272400,-1.535000,12.478000> }
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<-0.271800,-1.535000,11.292000>}
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<-0.271800,-1.535000,12.477500>}
box{<0,0,-0.203200><1.185500,0.035000,0.203200> rotate<0,90.000000,0> translate<-0.271800,-1.535000,12.477500> }
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<-0.271800,-1.535000,11.379200>}
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<-0.028000,-1.535000,11.379200>}
box{<0,0,-0.203200><0.243800,0.035000,0.203200> rotate<0,0.000000,0> translate<-0.271800,-1.535000,11.379200> }
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<-0.271800,-1.535000,11.785600>}
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<-0.028000,-1.535000,11.785600>}
box{<0,0,-0.203200><0.243800,0.035000,0.203200> rotate<0,0.000000,0> translate<-0.271800,-1.535000,11.785600> }
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<-0.271800,-1.535000,12.192000>}
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<-0.028000,-1.535000,12.192000>}
box{<0,0,-0.203200><0.243800,0.035000,0.203200> rotate<0,0.000000,0> translate<-0.271800,-1.535000,12.192000> }
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<-0.271800,-1.535000,12.477500>}
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<-0.028000,-1.535000,12.477500>}
box{<0,0,-0.203200><0.243800,0.035000,0.203200> rotate<0,0.000000,0> translate<-0.271800,-1.535000,12.477500> }
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<-0.271800,-1.535000,12.722400>}
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<-0.271800,-1.535000,13.907900>}
box{<0,0,-0.203200><1.185500,0.035000,0.203200> rotate<0,90.000000,0> translate<-0.271800,-1.535000,13.907900> }
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<-0.271800,-1.535000,12.722400>}
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<-0.028000,-1.535000,12.722400>}
box{<0,0,-0.203200><0.243800,0.035000,0.203200> rotate<0,0.000000,0> translate<-0.271800,-1.535000,12.722400> }
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<-0.271800,-1.535000,13.004800>}
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<-0.028000,-1.535000,13.004800>}
box{<0,0,-0.203200><0.243800,0.035000,0.203200> rotate<0,0.000000,0> translate<-0.271800,-1.535000,13.004800> }
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<-0.271800,-1.535000,13.411200>}
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<-0.028000,-1.535000,13.411200>}
box{<0,0,-0.203200><0.243800,0.035000,0.203200> rotate<0,0.000000,0> translate<-0.271800,-1.535000,13.411200> }
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<-0.271800,-1.535000,13.817600>}
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<-0.028000,-1.535000,13.817600>}
box{<0,0,-0.203200><0.243800,0.035000,0.203200> rotate<0,0.000000,0> translate<-0.271800,-1.535000,13.817600> }
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<-0.266600,-1.535000,24.206300>}
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<-0.266600,-1.535000,24.920900>}
box{<0,0,-0.203200><0.714600,0.035000,0.203200> rotate<0,90.000000,0> translate<-0.266600,-1.535000,24.920900> }
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<-0.266600,-1.535000,24.206300>}
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<1.368300,-1.535000,24.206300>}
box{<0,0,-0.203200><1.634900,0.035000,0.203200> rotate<0,0.000000,0> translate<-0.266600,-1.535000,24.206300> }
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<-0.266600,-1.535000,24.384000>}
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<3.153600,-1.535000,24.384000>}
box{<0,0,-0.203200><3.420200,0.035000,0.203200> rotate<0,0.000000,0> translate<-0.266600,-1.535000,24.384000> }
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<-0.266600,-1.535000,24.790400>}
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<3.153600,-1.535000,24.790400>}
box{<0,0,-0.203200><3.420200,0.035000,0.203200> rotate<0,0.000000,0> translate<-0.266600,-1.535000,24.790400> }
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<-0.028000,-1.535000,11.292000>}
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<0.710400,-1.535000,11.292000>}
box{<0,0,-0.203200><0.738400,0.035000,0.203200> rotate<0,0.000000,0> translate<-0.028000,-1.535000,11.292000> }
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<-0.028000,-1.535000,12.477500>}
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<-0.028000,-1.535000,11.292000>}
box{<0,0,-0.203200><1.185500,0.035000,0.203200> rotate<0,-90.000000,0> translate<-0.028000,-1.535000,11.292000> }
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<-0.028000,-1.535000,13.907900>}
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<-0.028000,-1.535000,12.722400>}
box{<0,0,-0.203200><1.185500,0.035000,0.203200> rotate<0,-90.000000,0> translate<-0.028000,-1.535000,12.722400> }
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<-0.028000,-1.535000,13.907900>}
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<0.710400,-1.535000,13.907900>}
box{<0,0,-0.203200><0.738400,0.035000,0.203200> rotate<0,0.000000,0> translate<-0.028000,-1.535000,13.907900> }
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<0.040600,-1.535000,3.309100>}
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<0.690800,-1.535000,3.959300>}
box{<0,0,-0.203200><0.919522,0.035000,0.203200> rotate<0,-44.997030,0> translate<0.040600,-1.535000,3.309100> }
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<0.040600,-1.535000,26.690800>}
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<0.690800,-1.535000,26.040600>}
box{<0,0,-0.203200><0.919522,0.035000,0.203200> rotate<0,44.997030,0> translate<0.040600,-1.535000,26.690800> }
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<0.690800,-1.535000,3.959300>}
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<1.540200,-1.535000,4.311100>}
box{<0,0,-0.203200><0.919371,0.035000,0.203200> rotate<0,-22.496615,0> translate<0.690800,-1.535000,3.959300> }
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<0.690800,-1.535000,26.040600>}
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<1.540200,-1.535000,25.688800>}
box{<0,0,-0.203200><0.919371,0.035000,0.203200> rotate<0,22.496615,0> translate<0.690800,-1.535000,26.040600> }
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<0.710400,-1.535000,11.292000>}
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<0.871800,-1.535000,11.453500>}
box{<0,0,-0.203200><0.228325,0.035000,0.203200> rotate<0,-45.014773,0> translate<0.710400,-1.535000,11.292000> }
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<0.710400,-1.535000,13.907900>}
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<0.871800,-1.535000,13.746400>}
box{<0,0,-0.203200><0.228325,0.035000,0.203200> rotate<0,45.014773,0> translate<0.710400,-1.535000,13.907900> }
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<0.797500,-1.535000,11.379200>}
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<8.561600,-1.535000,11.379200>}
box{<0,0,-0.203200><7.764100,0.035000,0.203200> rotate<0,0.000000,0> translate<0.797500,-1.535000,11.379200> }
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<0.800700,-1.535000,13.817600>}
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<2.883800,-1.535000,13.817600>}
box{<0,0,-0.203200><2.083100,0.035000,0.203200> rotate<0,0.000000,0> translate<0.800700,-1.535000,13.817600> }
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<0.871800,-1.535000,11.453500>}
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<0.931600,-1.535000,11.393600>}
box{<0,0,-0.203200><0.084641,0.035000,0.203200> rotate<0,45.044893,0> translate<0.871800,-1.535000,11.453500> }
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<0.871800,-1.535000,13.746400>}
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<0.931600,-1.535000,13.806300>}
box{<0,0,-0.203200><0.084641,0.035000,0.203200> rotate<0,-45.044893,0> translate<0.871800,-1.535000,13.746400> }
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<0.931600,-1.535000,11.393600>}
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<2.568300,-1.535000,11.393600>}
box{<0,0,-0.203200><1.636700,0.035000,0.203200> rotate<0,0.000000,0> translate<0.931600,-1.535000,11.393600> }
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<0.931600,-1.535000,13.806300>}
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<2.568300,-1.535000,13.806300>}
box{<0,0,-0.203200><1.636700,0.035000,0.203200> rotate<0,0.000000,0> translate<0.931600,-1.535000,13.806300> }
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<1.368300,-1.535000,24.206300>}
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<1.606300,-1.535000,23.968300>}
box{<0,0,-0.203200><0.336583,0.035000,0.203200> rotate<0,44.997030,0> translate<1.368300,-1.535000,24.206300> }
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<1.468600,-1.535000,19.539600>}
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<1.731300,-1.535000,19.539600>}
box{<0,0,-0.203200><0.262700,0.035000,0.203200> rotate<0,0.000000,0> translate<1.468600,-1.535000,19.539600> }
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<1.540200,-1.535000,4.311100>}
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<2.459700,-1.535000,4.311100>}
box{<0,0,-0.203200><0.919500,0.035000,0.203200> rotate<0,0.000000,0> translate<1.540200,-1.535000,4.311100> }
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<1.540200,-1.535000,25.688800>}
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<2.459700,-1.535000,25.688800>}
box{<0,0,-0.203200><0.919500,0.035000,0.203200> rotate<0,0.000000,0> translate<1.540200,-1.535000,25.688800> }
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<1.597000,-1.535000,23.977600>}
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<3.153600,-1.535000,23.977600>}
box{<0,0,-0.203200><1.556600,0.035000,0.203200> rotate<0,0.000000,0> translate<1.597000,-1.535000,23.977600> }
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<1.606300,-1.535000,23.385500>}
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<1.606300,-1.535000,23.968300>}
box{<0,0,-0.203200><0.582800,0.035000,0.203200> rotate<0,90.000000,0> translate<1.606300,-1.535000,23.968300> }
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<1.606300,-1.535000,23.385500>}
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<1.733200,-1.535000,23.438100>}
box{<0,0,-0.203200><0.137369,0.035000,0.203200> rotate<0,-22.512502,0> translate<1.606300,-1.535000,23.385500> }
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<1.606300,-1.535000,23.571200>}
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<3.153600,-1.535000,23.571200>}
box{<0,0,-0.203200><1.547300,0.035000,0.203200> rotate<0,0.000000,0> translate<1.606300,-1.535000,23.571200> }
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<1.731300,-1.535000,19.539600>}
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<1.966600,-1.535000,19.637100>}
box{<0,0,-0.203200><0.254700,0.035000,0.203200> rotate<0,-22.505903,0> translate<1.731300,-1.535000,19.539600> }
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<1.733200,-1.535000,23.438100>}
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<2.066700,-1.535000,23.438100>}
box{<0,0,-0.203200><0.333500,0.035000,0.203200> rotate<0,0.000000,0> translate<1.733200,-1.535000,23.438100> }
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<1.966600,-1.535000,19.279000>}
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<2.566600,-1.535000,18.679000>}
box{<0,0,-0.203200><0.848528,0.035000,0.203200> rotate<0,44.997030,0> translate<1.966600,-1.535000,19.279000> }
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<1.966600,-1.535000,19.637100>}
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<1.966600,-1.535000,19.279000>}
box{<0,0,-0.203200><0.358100,0.035000,0.203200> rotate<0,-90.000000,0> translate<1.966600,-1.535000,19.279000> }
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<2.066700,-1.535000,23.438100>}
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<2.374700,-1.535000,23.310500>}
box{<0,0,-0.203200><0.333385,0.035000,0.203200> rotate<0,22.502043,0> translate<2.066700,-1.535000,23.438100> }
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<2.374700,-1.535000,23.310500>}
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<2.610500,-1.535000,23.074700>}
box{<0,0,-0.203200><0.333472,0.035000,0.203200> rotate<0,44.997030,0> translate<2.374700,-1.535000,23.310500> }
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<2.459700,-1.535000,4.311100>}
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<3.309100,-1.535000,3.959300>}
box{<0,0,-0.203200><0.919371,0.035000,0.203200> rotate<0,22.496615,0> translate<2.459700,-1.535000,4.311100> }
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<2.459700,-1.535000,25.688800>}
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<3.309100,-1.535000,26.040600>}
box{<0,0,-0.203200><0.919371,0.035000,0.203200> rotate<0,-22.496615,0> translate<2.459700,-1.535000,25.688800> }
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<2.520400,-1.535000,23.164800>}
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<3.153600,-1.535000,23.164800>}
box{<0,0,-0.203200><0.633200,0.035000,0.203200> rotate<0,0.000000,0> translate<2.520400,-1.535000,23.164800> }
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<2.566600,-1.535000,18.679000>}
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<2.879000,-1.535000,18.366600>}
box{<0,0,-0.203200><0.441800,0.035000,0.203200> rotate<0,44.997030,0> translate<2.566600,-1.535000,18.679000> }
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<2.568300,-1.535000,11.393600>}
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<2.806300,-1.535000,11.631600>}
box{<0,0,-0.203200><0.336583,0.035000,0.203200> rotate<0,-44.997030,0> translate<2.568300,-1.535000,11.393600> }
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<2.568300,-1.535000,13.806300>}
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<2.652700,-1.535000,13.721900>}
box{<0,0,-0.203200><0.119360,0.035000,0.203200> rotate<0,44.997030,0> translate<2.568300,-1.535000,13.806300> }
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<2.610500,-1.535000,23.074700>}
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<2.688700,-1.535000,22.885800>}
box{<0,0,-0.203200><0.204447,0.035000,0.203200> rotate<0,67.507181,0> translate<2.610500,-1.535000,23.074700> }
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<2.652700,-1.535000,13.721900>}
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<2.933200,-1.535000,13.838100>}
box{<0,0,-0.203200><0.303616,0.035000,0.203200> rotate<0,-22.500798,0> translate<2.652700,-1.535000,13.721900> }
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<2.688700,-1.535000,22.885800>}
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<2.868600,-1.535000,22.960300>}
box{<0,0,-0.203200><0.194716,0.035000,0.203200> rotate<0,-22.493888,0> translate<2.688700,-1.535000,22.885800> }
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<2.806300,-1.535000,11.631600>}
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<2.806300,-1.535000,12.214400>}
box{<0,0,-0.203200><0.582800,0.035000,0.203200> rotate<0,90.000000,0> translate<2.806300,-1.535000,12.214400> }
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<2.806300,-1.535000,11.785600>}
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<8.673300,-1.535000,11.785600>}
box{<0,0,-0.203200><5.867000,0.035000,0.203200> rotate<0,0.000000,0> translate<2.806300,-1.535000,11.785600> }
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<2.806300,-1.535000,12.192000>}
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<2.860300,-1.535000,12.192000>}
box{<0,0,-0.203200><0.054000,0.035000,0.203200> rotate<0,0.000000,0> translate<2.806300,-1.535000,12.192000> }
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<2.806300,-1.535000,12.214400>}
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<2.933200,-1.535000,12.161800>}
box{<0,0,-0.203200><0.137369,0.035000,0.203200> rotate<0,22.512502,0> translate<2.806300,-1.535000,12.214400> }
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<2.868600,-1.535000,22.960300>}
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<3.220700,-1.535000,22.960300>}
box{<0,0,-0.203200><0.352100,0.035000,0.203200> rotate<0,0.000000,0> translate<2.868600,-1.535000,22.960300> }
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<2.879000,-1.535000,18.366600>}
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<6.379000,-1.535000,18.366600>}
box{<0,0,-0.203200><3.500000,0.035000,0.203200> rotate<0,0.000000,0> translate<2.879000,-1.535000,18.366600> }
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<2.933200,-1.535000,12.161800>}
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<3.266700,-1.535000,12.161800>}
box{<0,0,-0.203200><0.333500,0.035000,0.203200> rotate<0,0.000000,0> translate<2.933200,-1.535000,12.161800> }
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<2.933200,-1.535000,13.838100>}
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<3.266700,-1.535000,13.838100>}
box{<0,0,-0.203200><0.333500,0.035000,0.203200> rotate<0,0.000000,0> translate<2.933200,-1.535000,13.838100> }
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<3.056400,-1.535000,4.064000>}
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<4.378700,-1.535000,4.064000>}
box{<0,0,-0.203200><1.322300,0.035000,0.203200> rotate<0,0.000000,0> translate<3.056400,-1.535000,4.064000> }
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<3.153600,-1.535000,23.106600>}
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<3.260300,-1.535000,23.000000>}
box{<0,0,-0.203200><0.150826,0.035000,0.203200> rotate<0,44.970170,0> translate<3.153600,-1.535000,23.106600> }
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<3.153600,-1.535000,24.163300>}
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<3.153600,-1.535000,23.106600>}
box{<0,0,-0.203200><1.056700,0.035000,0.203200> rotate<0,-90.000000,0> translate<3.153600,-1.535000,23.106600> }
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<3.153600,-1.535000,24.163300>}
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<3.260300,-1.535000,24.269900>}
box{<0,0,-0.203200><0.150826,0.035000,0.203200> rotate<0,-44.970170,0> translate<3.153600,-1.535000,24.163300> }
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<3.153600,-1.535000,24.376600>}
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<3.260300,-1.535000,24.269900>}
box{<0,0,-0.203200><0.150897,0.035000,0.203200> rotate<0,44.997030,0> translate<3.153600,-1.535000,24.376600> }
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<3.153600,-1.535000,25.433300>}
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<3.153600,-1.535000,24.376600>}
box{<0,0,-0.203200><1.056700,0.035000,0.203200> rotate<0,-90.000000,0> translate<3.153600,-1.535000,24.376600> }
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<3.153600,-1.535000,25.433300>}
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<3.391600,-1.535000,25.671300>}
box{<0,0,-0.203200><0.336583,0.035000,0.203200> rotate<0,-44.997030,0> translate<3.153600,-1.535000,25.433300> }
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<3.220700,-1.535000,22.960300>}
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<3.260300,-1.535000,23.000000>}
box{<0,0,-0.203200><0.056074,0.035000,0.203200> rotate<0,-45.069277,0> translate<3.220700,-1.535000,22.960300> }
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<3.234200,-1.535000,26.009600>}
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<6.766600,-1.535000,26.009600>}
box{<0,0,-0.203200><3.532400,0.035000,0.203200> rotate<0,0.000000,0> translate<3.234200,-1.535000,26.009600> }
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<3.261800,-1.535000,6.333200>}
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<3.389400,-1.535000,6.025200>}
box{<0,0,-0.203200><0.333385,0.035000,0.203200> rotate<0,67.492017,0> translate<3.261800,-1.535000,6.333200> }
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<3.261800,-1.535000,6.666700>}
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<3.261800,-1.535000,6.333200>}
box{<0,0,-0.203200><0.333500,0.035000,0.203200> rotate<0,-90.000000,0> translate<3.261800,-1.535000,6.333200> }
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<3.261800,-1.535000,6.666700>}
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<3.389400,-1.535000,6.974700>}
box{<0,0,-0.203200><0.333385,0.035000,0.203200> rotate<0,-67.492017,0> translate<3.261800,-1.535000,6.666700> }
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<3.266700,-1.535000,12.161800>}
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<3.574700,-1.535000,12.289400>}
box{<0,0,-0.203200><0.333385,0.035000,0.203200> rotate<0,-22.502043,0> translate<3.266700,-1.535000,12.161800> }
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<3.266700,-1.535000,13.838100>}
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<3.574700,-1.535000,13.710500>}
box{<0,0,-0.203200><0.333385,0.035000,0.203200> rotate<0,22.502043,0> translate<3.266700,-1.535000,13.838100> }
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<3.309100,-1.535000,3.959300>}
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<3.959300,-1.535000,3.309100>}
box{<0,0,-0.203200><0.919522,0.035000,0.203200> rotate<0,44.997030,0> translate<3.309100,-1.535000,3.959300> }
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<3.309100,-1.535000,26.040600>}
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<3.959300,-1.535000,26.690800>}
box{<0,0,-0.203200><0.919522,0.035000,0.203200> rotate<0,-44.997030,0> translate<3.309100,-1.535000,26.040600> }
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<3.316200,-1.535000,13.817600>}
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<8.460000,-1.535000,13.817600>}
box{<0,0,-0.203200><5.143800,0.035000,0.203200> rotate<0,0.000000,0> translate<3.316200,-1.535000,13.817600> }
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<3.339500,-1.535000,12.192000>}
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<9.079700,-1.535000,12.192000>}
box{<0,0,-0.203200><5.740200,0.035000,0.203200> rotate<0,0.000000,0> translate<3.339500,-1.535000,12.192000> }
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<3.389400,-1.535000,6.025200>}
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<3.625200,-1.535000,5.789400>}
box{<0,0,-0.203200><0.333472,0.035000,0.203200> rotate<0,44.997030,0> translate<3.389400,-1.535000,6.025200> }
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<3.389400,-1.535000,6.974700>}
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<3.625200,-1.535000,7.210500>}
box{<0,0,-0.203200><0.333472,0.035000,0.203200> rotate<0,-44.997030,0> translate<3.389400,-1.535000,6.974700> }
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<3.391600,-1.535000,25.671300>}
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<5.508300,-1.535000,25.671300>}
box{<0,0,-0.203200><2.116700,0.035000,0.203200> rotate<0,0.000000,0> translate<3.391600,-1.535000,25.671300> }
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<3.574700,-1.535000,12.289400>}
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<3.810500,-1.535000,12.525200>}
box{<0,0,-0.203200><0.333472,0.035000,0.203200> rotate<0,-44.997030,0> translate<3.574700,-1.535000,12.289400> }
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<3.574700,-1.535000,13.710500>}
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<3.810500,-1.535000,13.474700>}
box{<0,0,-0.203200><0.333472,0.035000,0.203200> rotate<0,44.997030,0> translate<3.574700,-1.535000,13.710500> }
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<3.610800,-1.535000,3.657600>}
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<4.161800,-1.535000,3.657600>}
box{<0,0,-0.203200><0.551000,0.035000,0.203200> rotate<0,0.000000,0> translate<3.610800,-1.535000,3.657600> }
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<3.625200,-1.535000,5.789400>}
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<3.933200,-1.535000,5.661800>}
box{<0,0,-0.203200><0.333385,0.035000,0.203200> rotate<0,22.502043,0> translate<3.625200,-1.535000,5.789400> }
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<3.625200,-1.535000,7.210500>}
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<3.933200,-1.535000,7.338100>}
box{<0,0,-0.203200><0.333385,0.035000,0.203200> rotate<0,-22.502043,0> translate<3.625200,-1.535000,7.210500> }
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<3.684500,-1.535000,26.416000>}
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<6.966600,-1.535000,26.416000>}
box{<0,0,-0.203200><3.282100,0.035000,0.203200> rotate<0,0.000000,0> translate<3.684500,-1.535000,26.416000> }
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<3.810500,-1.535000,12.525200>}
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<3.938100,-1.535000,12.833200>}
box{<0,0,-0.203200><0.333385,0.035000,0.203200> rotate<0,-67.492017,0> translate<3.810500,-1.535000,12.525200> }
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<3.810500,-1.535000,13.474700>}
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<3.938100,-1.535000,13.166700>}
box{<0,0,-0.203200><0.333385,0.035000,0.203200> rotate<0,67.492017,0> translate<3.810500,-1.535000,13.474700> }
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<3.836900,-1.535000,13.411200>}
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<8.460000,-1.535000,13.411200>}
box{<0,0,-0.203200><4.623100,0.035000,0.203200> rotate<0,0.000000,0> translate<3.836900,-1.535000,13.411200> }
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<3.840800,-1.535000,12.598400>}
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<9.065500,-1.535000,12.598400>}
box{<0,0,-0.203200><5.224700,0.035000,0.203200> rotate<0,0.000000,0> translate<3.840800,-1.535000,12.598400> }
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<3.933200,-1.535000,5.661800>}
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<4.266700,-1.535000,5.661800>}
box{<0,0,-0.203200><0.333500,0.035000,0.203200> rotate<0,0.000000,0> translate<3.933200,-1.535000,5.661800> }
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<3.933200,-1.535000,7.338100>}
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<4.266700,-1.535000,7.338100>}
box{<0,0,-0.203200><0.333500,0.035000,0.203200> rotate<0,0.000000,0> translate<3.933200,-1.535000,7.338100> }
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<3.938100,-1.535000,12.833200>}
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<3.938100,-1.535000,13.166700>}
box{<0,0,-0.203200><0.333500,0.035000,0.203200> rotate<0,90.000000,0> translate<3.938100,-1.535000,13.166700> }
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<3.938100,-1.535000,13.004800>}
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<8.659100,-1.535000,13.004800>}
box{<0,0,-0.203200><4.721000,0.035000,0.203200> rotate<0,0.000000,0> translate<3.938100,-1.535000,13.004800> }
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<3.959300,-1.535000,3.309100>}
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<4.311100,-1.535000,2.459700>}
box{<0,0,-0.203200><0.919371,0.035000,0.203200> rotate<0,67.497445,0> translate<3.959300,-1.535000,3.309100> }
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<3.959300,-1.535000,26.690800>}
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<4.311100,-1.535000,27.540200>}
box{<0,0,-0.203200><0.919371,0.035000,0.203200> rotate<0,-67.497445,0> translate<3.959300,-1.535000,26.690800> }
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<3.967600,-1.535000,0.711100>}
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<4.311100,-1.535000,1.540200>}
box{<0,0,-0.203200><0.897440,0.035000,0.203200> rotate<0,-67.491090,0> translate<3.967600,-1.535000,0.711100> }
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<3.967600,-1.535000,0.711100>}
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<58.034400,-1.535000,0.711100>}
box{<0,0,-0.203200><54.066800,0.035000,0.203200> rotate<0,0.000000,0> translate<3.967600,-1.535000,0.711100> }
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<3.967600,-1.535000,29.288800>}
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<4.311100,-1.535000,28.459700>}
box{<0,0,-0.203200><0.897440,0.035000,0.203200> rotate<0,67.491090,0> translate<3.967600,-1.535000,29.288800> }
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<3.967600,-1.535000,29.288800>}
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<12.025100,-1.535000,29.288800>}
box{<0,0,-0.203200><8.057500,0.035000,0.203200> rotate<0,0.000000,0> translate<3.967600,-1.535000,29.288800> }
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<3.979300,-1.535000,29.260800>}
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<11.997100,-1.535000,29.260800>}
box{<0,0,-0.203200><8.017800,0.035000,0.203200> rotate<0,0.000000,0> translate<3.979300,-1.535000,29.260800> }
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<3.983300,-1.535000,3.251200>}
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<4.195700,-1.535000,3.251200>}
box{<0,0,-0.203200><0.212400,0.035000,0.203200> rotate<0,0.000000,0> translate<3.983300,-1.535000,3.251200> }
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<4.009700,-1.535000,0.812800>}
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<57.992200,-1.535000,0.812800>}
box{<0,0,-0.203200><53.982500,0.035000,0.203200> rotate<0,0.000000,0> translate<4.009700,-1.535000,0.812800> }
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<4.013800,-1.535000,26.822400>}
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<7.368100,-1.535000,26.822400>}
box{<0,0,-0.203200><3.354300,0.035000,0.203200> rotate<0,0.000000,0> translate<4.013800,-1.535000,26.822400> }
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<4.147600,-1.535000,28.854400>}
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<11.620300,-1.535000,28.854400>}
box{<0,0,-0.203200><7.472700,0.035000,0.203200> rotate<0,0.000000,0> translate<4.147600,-1.535000,28.854400> }
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<4.151700,-1.535000,2.844800>}
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<4.469800,-1.535000,2.844800>}
box{<0,0,-0.203200><0.318100,0.035000,0.203200> rotate<0,0.000000,0> translate<4.151700,-1.535000,2.844800> }
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<4.161800,-1.535000,3.333200>}
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<4.289400,-1.535000,3.025200>}
box{<0,0,-0.203200><0.333385,0.035000,0.203200> rotate<0,67.492017,0> translate<4.161800,-1.535000,3.333200> }
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<4.161800,-1.535000,3.666700>}
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<4.161800,-1.535000,3.333200>}
box{<0,0,-0.203200><0.333500,0.035000,0.203200> rotate<0,-90.000000,0> translate<4.161800,-1.535000,3.333200> }
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<4.161800,-1.535000,3.666700>}
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<4.289400,-1.535000,3.974700>}
box{<0,0,-0.203200><0.333385,0.035000,0.203200> rotate<0,-67.492017,0> translate<4.161800,-1.535000,3.666700> }
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<4.178100,-1.535000,1.219200>}
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<57.823800,-1.535000,1.219200>}
box{<0,0,-0.203200><53.645700,0.035000,0.203200> rotate<0,0.000000,0> translate<4.178100,-1.535000,1.219200> }
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<4.182100,-1.535000,27.228800>}
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<11.403400,-1.535000,27.228800>}
box{<0,0,-0.203200><7.221300,0.035000,0.203200> rotate<0,0.000000,0> translate<4.182100,-1.535000,27.228800> }
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<4.266700,-1.535000,5.661800>}
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<4.574700,-1.535000,5.789400>}
box{<0,0,-0.203200><0.333385,0.035000,0.203200> rotate<0,-22.502043,0> translate<4.266700,-1.535000,5.661800> }
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<4.266700,-1.535000,7.338100>}
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<4.574700,-1.535000,7.210500>}
box{<0,0,-0.203200><0.333385,0.035000,0.203200> rotate<0,22.502043,0> translate<4.266700,-1.535000,7.338100> }
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<4.289400,-1.535000,3.025200>}
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<4.525200,-1.535000,2.789400>}
box{<0,0,-0.203200><0.333472,0.035000,0.203200> rotate<0,44.997030,0> translate<4.289400,-1.535000,3.025200> }
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<4.289400,-1.535000,3.974700>}
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<4.525200,-1.535000,4.210500>}
box{<0,0,-0.203200><0.333472,0.035000,0.203200> rotate<0,-44.997030,0> translate<4.289400,-1.535000,3.974700> }
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<4.311100,-1.535000,1.540200>}
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<4.311100,-1.535000,2.459700>}
box{<0,0,-0.203200><0.919500,0.035000,0.203200> rotate<0,90.000000,0> translate<4.311100,-1.535000,2.459700> }
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<4.311100,-1.535000,1.625600>}
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<57.688800,-1.535000,1.625600>}
box{<0,0,-0.203200><53.377700,0.035000,0.203200> rotate<0,0.000000,0> translate<4.311100,-1.535000,1.625600> }
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<4.311100,-1.535000,2.032000>}
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<57.688800,-1.535000,2.032000>}
box{<0,0,-0.203200><53.377700,0.035000,0.203200> rotate<0,0.000000,0> translate<4.311100,-1.535000,2.032000> }
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<4.311100,-1.535000,2.438400>}
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<10.186800,-1.535000,2.438400>}
box{<0,0,-0.203200><5.875700,0.035000,0.203200> rotate<0,0.000000,0> translate<4.311100,-1.535000,2.438400> }
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<4.311100,-1.535000,27.540200>}
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<4.311100,-1.535000,28.459700>}
box{<0,0,-0.203200><0.919500,0.035000,0.203200> rotate<0,90.000000,0> translate<4.311100,-1.535000,28.459700> }
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<4.311100,-1.535000,27.635200>}
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<11.338800,-1.535000,27.635200>}
box{<0,0,-0.203200><7.027700,0.035000,0.203200> rotate<0,0.000000,0> translate<4.311100,-1.535000,27.635200> }
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<4.311100,-1.535000,28.041600>}
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<11.338800,-1.535000,28.041600>}
box{<0,0,-0.203200><7.027700,0.035000,0.203200> rotate<0,0.000000,0> translate<4.311100,-1.535000,28.041600> }
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<4.311100,-1.535000,28.448000>}
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<11.451900,-1.535000,28.448000>}
box{<0,0,-0.203200><7.140800,0.035000,0.203200> rotate<0,0.000000,0> translate<4.311100,-1.535000,28.448000> }
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<4.322000,-1.535000,7.315200>}
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<5.678000,-1.535000,7.315200>}
box{<0,0,-0.203200><1.356000,0.035000,0.203200> rotate<0,0.000000,0> translate<4.322000,-1.535000,7.315200> }
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<4.333800,-1.535000,5.689600>}
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<5.666000,-1.535000,5.689600>}
box{<0,0,-0.203200><1.332200,0.035000,0.203200> rotate<0,0.000000,0> translate<4.333800,-1.535000,5.689600> }
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<4.525200,-1.535000,2.789400>}
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<4.833200,-1.535000,2.661800>}
box{<0,0,-0.203200><0.333385,0.035000,0.203200> rotate<0,22.502043,0> translate<4.525200,-1.535000,2.789400> }
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<4.525200,-1.535000,4.210500>}
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<4.833200,-1.535000,4.338100>}
box{<0,0,-0.203200><0.333385,0.035000,0.203200> rotate<0,-22.502043,0> translate<4.525200,-1.535000,4.210500> }
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<4.574700,-1.535000,5.789400>}
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<4.751900,-1.535000,5.966600>}
box{<0,0,-0.203200><0.250599,0.035000,0.203200> rotate<0,-44.997030,0> translate<4.574700,-1.535000,5.789400> }
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<4.574700,-1.535000,7.210500>}
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<4.751900,-1.535000,7.033300>}
box{<0,0,-0.203200><0.250599,0.035000,0.203200> rotate<0,44.997030,0> translate<4.574700,-1.535000,7.210500> }
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<4.751900,-1.535000,5.966600>}
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<5.248000,-1.535000,5.966600>}
box{<0,0,-0.203200><0.496100,0.035000,0.203200> rotate<0,0.000000,0> translate<4.751900,-1.535000,5.966600> }
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<4.751900,-1.535000,7.033300>}
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<5.248000,-1.535000,7.033300>}
box{<0,0,-0.203200><0.496100,0.035000,0.203200> rotate<0,0.000000,0> translate<4.751900,-1.535000,7.033300> }
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<4.833200,-1.535000,2.661800>}
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<5.166700,-1.535000,2.661800>}
box{<0,0,-0.203200><0.333500,0.035000,0.203200> rotate<0,0.000000,0> translate<4.833200,-1.535000,2.661800> }
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<4.833200,-1.535000,4.338100>}
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<5.166700,-1.535000,4.338100>}
box{<0,0,-0.203200><0.333500,0.035000,0.203200> rotate<0,0.000000,0> translate<4.833200,-1.535000,4.338100> }
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<5.166700,-1.535000,2.661800>}
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<5.474700,-1.535000,2.789400>}
box{<0,0,-0.203200><0.333385,0.035000,0.203200> rotate<0,-22.502043,0> translate<5.166700,-1.535000,2.661800> }
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<5.166700,-1.535000,4.338100>}
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<5.474700,-1.535000,4.210500>}
box{<0,0,-0.203200><0.333385,0.035000,0.203200> rotate<0,22.502043,0> translate<5.166700,-1.535000,4.338100> }
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<5.248000,-1.535000,5.966600>}
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<5.425200,-1.535000,5.789400>}
box{<0,0,-0.203200><0.250599,0.035000,0.203200> rotate<0,44.997030,0> translate<5.248000,-1.535000,5.966600> }
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<5.248000,-1.535000,7.033300>}
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<5.425200,-1.535000,7.210500>}
box{<0,0,-0.203200><0.250599,0.035000,0.203200> rotate<0,-44.997030,0> translate<5.248000,-1.535000,7.033300> }
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<5.425200,-1.535000,5.789400>}
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<5.733200,-1.535000,5.661800>}
box{<0,0,-0.203200><0.333385,0.035000,0.203200> rotate<0,22.502043,0> translate<5.425200,-1.535000,5.789400> }
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<5.425200,-1.535000,7.210500>}
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<5.733200,-1.535000,7.338100>}
box{<0,0,-0.203200><0.333385,0.035000,0.203200> rotate<0,-22.502043,0> translate<5.425200,-1.535000,7.210500> }
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<5.474700,-1.535000,2.789400>}
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<5.524900,-1.535000,2.839600>}
box{<0,0,-0.203200><0.070994,0.035000,0.203200> rotate<0,-44.997030,0> translate<5.474700,-1.535000,2.789400> }
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<5.474700,-1.535000,4.210500>}
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<5.524900,-1.535000,4.160300>}
box{<0,0,-0.203200><0.070994,0.035000,0.203200> rotate<0,44.997030,0> translate<5.474700,-1.535000,4.210500> }
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<5.508300,-1.535000,25.671300>}
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<5.746300,-1.535000,25.433300>}
box{<0,0,-0.203200><0.336583,0.035000,0.203200> rotate<0,44.997030,0> translate<5.508300,-1.535000,25.671300> }
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<5.524900,-1.535000,2.839600>}
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<9.993600,-1.535000,2.839600>}
box{<0,0,-0.203200><4.468700,0.035000,0.203200> rotate<0,0.000000,0> translate<5.524900,-1.535000,2.839600> }
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<5.524900,-1.535000,4.160300>}
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<9.993600,-1.535000,4.160300>}
box{<0,0,-0.203200><4.468700,0.035000,0.203200> rotate<0,0.000000,0> translate<5.524900,-1.535000,4.160300> }
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<5.576400,-1.535000,25.603200>}
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<6.766600,-1.535000,25.603200>}
box{<0,0,-0.203200><1.190200,0.035000,0.203200> rotate<0,0.000000,0> translate<5.576400,-1.535000,25.603200> }
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<5.639600,-1.535000,24.269900>}
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<5.746300,-1.535000,24.163300>}
box{<0,0,-0.203200><0.150826,0.035000,0.203200> rotate<0,44.970170,0> translate<5.639600,-1.535000,24.269900> }
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<5.639600,-1.535000,24.269900>}
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<5.746300,-1.535000,24.376600>}
box{<0,0,-0.203200><0.150897,0.035000,0.203200> rotate<0,-44.997030,0> translate<5.639600,-1.535000,24.269900> }
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<5.733200,-1.535000,5.661800>}
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<6.066700,-1.535000,5.661800>}
box{<0,0,-0.203200><0.333500,0.035000,0.203200> rotate<0,0.000000,0> translate<5.733200,-1.535000,5.661800> }
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<5.733200,-1.535000,7.338100>}
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<6.066700,-1.535000,7.338100>}
box{<0,0,-0.203200><0.333500,0.035000,0.203200> rotate<0,0.000000,0> translate<5.733200,-1.535000,7.338100> }
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<5.746300,-1.535000,24.133300>}
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<5.746300,-1.535000,24.163300>}
box{<0,0,-0.203200><0.030000,0.035000,0.203200> rotate<0,90.000000,0> translate<5.746300,-1.535000,24.163300> }
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<5.746300,-1.535000,24.133300>}
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<6.179000,-1.535000,24.133300>}
box{<0,0,-0.203200><0.432700,0.035000,0.203200> rotate<0,0.000000,0> translate<5.746300,-1.535000,24.133300> }
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<5.746300,-1.535000,24.376600>}
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<5.746300,-1.535000,25.433300>}
box{<0,0,-0.203200><1.056700,0.035000,0.203200> rotate<0,90.000000,0> translate<5.746300,-1.535000,25.433300> }
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<5.746300,-1.535000,24.384000>}
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<6.629700,-1.535000,24.384000>}
box{<0,0,-0.203200><0.883400,0.035000,0.203200> rotate<0,0.000000,0> translate<5.746300,-1.535000,24.384000> }
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<5.746300,-1.535000,24.790400>}
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<6.766600,-1.535000,24.790400>}
box{<0,0,-0.203200><1.020300,0.035000,0.203200> rotate<0,0.000000,0> translate<5.746300,-1.535000,24.790400> }
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<5.746300,-1.535000,25.196800>}
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<6.766600,-1.535000,25.196800>}
box{<0,0,-0.203200><1.020300,0.035000,0.203200> rotate<0,0.000000,0> translate<5.746300,-1.535000,25.196800> }
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<6.066700,-1.535000,5.661800>}
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<6.374700,-1.535000,5.789400>}
box{<0,0,-0.203200><0.333385,0.035000,0.203200> rotate<0,-22.502043,0> translate<6.066700,-1.535000,5.661800> }
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<6.066700,-1.535000,7.338100>}
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<6.374700,-1.535000,7.210500>}
box{<0,0,-0.203200><0.333385,0.035000,0.203200> rotate<0,22.502043,0> translate<6.066700,-1.535000,7.338100> }
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<6.122000,-1.535000,7.315200>}
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<10.229900,-1.535000,7.315200>}
box{<0,0,-0.203200><4.107900,0.035000,0.203200> rotate<0,0.000000,0> translate<6.122000,-1.535000,7.315200> }
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<6.133800,-1.535000,5.689600>}
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<9.993600,-1.535000,5.689600>}
box{<0,0,-0.203200><3.859800,0.035000,0.203200> rotate<0,0.000000,0> translate<6.133800,-1.535000,5.689600> }
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<6.179000,-1.535000,24.133300>}
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<6.279000,-1.535000,24.233300>}
box{<0,0,-0.203200><0.141421,0.035000,0.203200> rotate<0,-44.997030,0> translate<6.179000,-1.535000,24.133300> }
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<6.279000,-1.535000,24.233300>}
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<6.379000,-1.535000,24.233300>}
box{<0,0,-0.203200><0.100000,0.035000,0.203200> rotate<0,0.000000,0> translate<6.279000,-1.535000,24.233300> }
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<6.374700,-1.535000,5.789400>}
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<6.610500,-1.535000,6.025200>}
box{<0,0,-0.203200><0.333472,0.035000,0.203200> rotate<0,-44.997030,0> translate<6.374700,-1.535000,5.789400> }
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<6.374700,-1.535000,7.210500>}
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<6.610500,-1.535000,6.974700>}
box{<0,0,-0.203200><0.333472,0.035000,0.203200> rotate<0,44.997030,0> translate<6.374700,-1.535000,7.210500> }
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<6.379000,-1.535000,18.366600>}
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<6.820900,-1.535000,18.366600>}
box{<0,0,-0.203200><0.441900,0.035000,0.203200> rotate<0,0.000000,0> translate<6.379000,-1.535000,18.366600> }
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<6.379000,-1.535000,24.233300>}
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<6.479000,-1.535000,24.333300>}
box{<0,0,-0.203200><0.141421,0.035000,0.203200> rotate<0,-44.997030,0> translate<6.379000,-1.535000,24.233300> }
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<6.479000,-1.535000,24.333300>}
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<6.579000,-1.535000,24.333300>}
box{<0,0,-0.203200><0.100000,0.035000,0.203200> rotate<0,0.000000,0> translate<6.479000,-1.535000,24.333300> }
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<6.579000,-1.535000,24.333300>}
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<6.679000,-1.535000,24.433300>}
box{<0,0,-0.203200><0.141421,0.035000,0.203200> rotate<0,-44.997030,0> translate<6.579000,-1.535000,24.333300> }
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<6.610500,-1.535000,6.025200>}
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<6.738100,-1.535000,6.333200>}
box{<0,0,-0.203200><0.333385,0.035000,0.203200> rotate<0,-67.492017,0> translate<6.610500,-1.535000,6.025200> }
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<6.610500,-1.535000,6.974700>}
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<6.738100,-1.535000,6.666700>}
box{<0,0,-0.203200><0.333385,0.035000,0.203200> rotate<0,67.492017,0> translate<6.610500,-1.535000,6.974700> }
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<6.637900,-1.535000,6.908800>}
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<9.979300,-1.535000,6.908800>}
box{<0,0,-0.203200><3.341400,0.035000,0.203200> rotate<0,0.000000,0> translate<6.637900,-1.535000,6.908800> }
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<6.639800,-1.535000,6.096000>}
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<9.993600,-1.535000,6.096000>}
box{<0,0,-0.203200><3.353800,0.035000,0.203200> rotate<0,0.000000,0> translate<6.639800,-1.535000,6.096000> }
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<6.679000,-1.535000,24.433300>}
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<6.766600,-1.535000,24.433300>}
box{<0,0,-0.203200><0.087600,0.035000,0.203200> rotate<0,0.000000,0> translate<6.679000,-1.535000,24.433300> }
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<6.738100,-1.535000,6.333200>}
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<6.738100,-1.535000,6.666700>}
box{<0,0,-0.203200><0.333500,0.035000,0.203200> rotate<0,90.000000,0> translate<6.738100,-1.535000,6.666700> }
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<6.738100,-1.535000,6.502400>}
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<9.974500,-1.535000,6.502400>}
box{<0,0,-0.203200><3.236400,0.035000,0.203200> rotate<0,0.000000,0> translate<6.738100,-1.535000,6.502400> }
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<6.766600,-1.535000,26.020900>}
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<6.766600,-1.535000,24.433300>}
box{<0,0,-0.203200><1.587600,0.035000,0.203200> rotate<0,-90.000000,0> translate<6.766600,-1.535000,24.433300> }
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<6.766600,-1.535000,26.020900>}
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<6.866600,-1.535000,26.120900>}
box{<0,0,-0.203200><0.141421,0.035000,0.203200> rotate<0,-44.997030,0> translate<6.766600,-1.535000,26.020900> }
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<6.820900,-1.535000,18.366600>}
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<7.920900,-1.535000,19.466600>}
box{<0,0,-0.203200><1.555635,0.035000,0.203200> rotate<0,-44.997030,0> translate<6.820900,-1.535000,18.366600> }
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<6.866600,-1.535000,26.220900>}
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<6.866600,-1.535000,26.120900>}
box{<0,0,-0.203200><0.100000,0.035000,0.203200> rotate<0,-90.000000,0> translate<6.866600,-1.535000,26.120900> }
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<6.866600,-1.535000,26.220900>}
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<6.966600,-1.535000,26.320900>}
box{<0,0,-0.203200><0.141421,0.035000,0.203200> rotate<0,-44.997030,0> translate<6.866600,-1.535000,26.220900> }
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<6.966600,-1.535000,26.420900>}
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<6.966600,-1.535000,26.320900>}
box{<0,0,-0.203200><0.100000,0.035000,0.203200> rotate<0,-90.000000,0> translate<6.966600,-1.535000,26.320900> }
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<6.966600,-1.535000,26.420900>}
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<7.066600,-1.535000,26.520800>}
box{<0,0,-0.203200><0.141351,0.035000,0.203200> rotate<0,-44.968370,0> translate<6.966600,-1.535000,26.420900> }
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<7.066600,-1.535000,26.520900>}
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<7.066600,-1.535000,26.520800>}
box{<0,0,-0.203200><0.000100,0.035000,0.203200> rotate<0,-90.000000,0> translate<7.066600,-1.535000,26.520800> }
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<7.066600,-1.535000,26.520900>}
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<7.379000,-1.535000,26.833300>}
box{<0,0,-0.203200><0.441800,0.035000,0.203200> rotate<0,-44.997030,0> translate<7.066600,-1.535000,26.520900> }
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<7.148700,-1.535000,18.694400>}
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<8.460000,-1.535000,18.694400>}
box{<0,0,-0.203200><1.311300,0.035000,0.203200> rotate<0,0.000000,0> translate<7.148700,-1.535000,18.694400> }
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<7.379000,-1.535000,26.833300>}
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<10.620900,-1.535000,26.833300>}
box{<0,0,-0.203200><3.241900,0.035000,0.203200> rotate<0,0.000000,0> translate<7.379000,-1.535000,26.833300> }
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<7.555100,-1.535000,19.100800>}
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<8.460000,-1.535000,19.100800>}
box{<0,0,-0.203200><0.904900,0.035000,0.203200> rotate<0,0.000000,0> translate<7.555100,-1.535000,19.100800> }
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<7.920900,-1.535000,19.466600>}
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<8.590700,-1.535000,19.466600>}
box{<0,0,-0.203200><0.669800,0.035000,0.203200> rotate<0,0.000000,0> translate<7.920900,-1.535000,19.466600> }
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<8.460000,-1.535000,13.203900>}
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<9.203900,-1.535000,12.460000>}
box{<0,0,-0.203200><1.052033,0.035000,0.203200> rotate<0,44.997030,0> translate<8.460000,-1.535000,13.203900> }
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<8.460000,-1.535000,13.679000>}
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<8.460000,-1.535000,13.203900>}
box{<0,0,-0.203200><0.475100,0.035000,0.203200> rotate<0,-90.000000,0> translate<8.460000,-1.535000,13.203900> }
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<8.460000,-1.535000,13.679000>}
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<9.678500,-1.535000,13.679000>}
box{<0,0,-0.203200><1.218500,0.035000,0.203200> rotate<0,0.000000,0> translate<8.460000,-1.535000,13.679000> }
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<8.460000,-1.535000,13.780800>}
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<9.678500,-1.535000,13.780800>}
box{<0,0,-0.203200><1.218500,0.035000,0.203200> rotate<0,0.000000,0> translate<8.460000,-1.535000,13.780800> }
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<8.460000,-1.535000,14.256000>}
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<8.460000,-1.535000,13.780800>}
box{<0,0,-0.203200><0.475200,0.035000,0.203200> rotate<0,-90.000000,0> translate<8.460000,-1.535000,13.780800> }
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<8.460000,-1.535000,14.256000>}
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<9.203900,-1.535000,14.999900>}
box{<0,0,-0.203200><1.052033,0.035000,0.203200> rotate<0,-44.997030,0> translate<8.460000,-1.535000,14.256000> }
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<8.460000,-1.535000,15.743900>}
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<9.203900,-1.535000,15.000000>}
box{<0,0,-0.203200><1.052033,0.035000,0.203200> rotate<0,44.997030,0> translate<8.460000,-1.535000,15.743900> }
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<8.460000,-1.535000,16.219000>}
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<8.460000,-1.535000,15.743900>}
box{<0,0,-0.203200><0.475100,0.035000,0.203200> rotate<0,-90.000000,0> translate<8.460000,-1.535000,15.743900> }
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<8.460000,-1.535000,16.219000>}
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<9.678500,-1.535000,16.219000>}
box{<0,0,-0.203200><1.218500,0.035000,0.203200> rotate<0,0.000000,0> translate<8.460000,-1.535000,16.219000> }
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<8.460000,-1.535000,16.320800>}
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<9.678500,-1.535000,16.320800>}
box{<0,0,-0.203200><1.218500,0.035000,0.203200> rotate<0,0.000000,0> translate<8.460000,-1.535000,16.320800> }
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<8.460000,-1.535000,16.796000>}
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<8.460000,-1.535000,16.320800>}
box{<0,0,-0.203200><0.475200,0.035000,0.203200> rotate<0,-90.000000,0> translate<8.460000,-1.535000,16.320800> }
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<8.460000,-1.535000,16.796000>}
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<9.203900,-1.535000,17.539900>}
box{<0,0,-0.203200><1.052033,0.035000,0.203200> rotate<0,-44.997030,0> translate<8.460000,-1.535000,16.796000> }
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<8.460000,-1.535000,18.283900>}
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<9.203900,-1.535000,17.540000>}
box{<0,0,-0.203200><1.052033,0.035000,0.203200> rotate<0,44.997030,0> translate<8.460000,-1.535000,18.283900> }
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<8.460000,-1.535000,18.759000>}
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<8.460000,-1.535000,18.283900>}
box{<0,0,-0.203200><0.475100,0.035000,0.203200> rotate<0,-90.000000,0> translate<8.460000,-1.535000,18.283900> }
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<8.460000,-1.535000,18.759000>}
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<9.678500,-1.535000,18.759000>}
box{<0,0,-0.203200><1.218500,0.035000,0.203200> rotate<0,0.000000,0> translate<8.460000,-1.535000,18.759000> }
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<8.460000,-1.535000,18.860800>}
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<9.678500,-1.535000,18.860800>}
box{<0,0,-0.203200><1.218500,0.035000,0.203200> rotate<0,0.000000,0> translate<8.460000,-1.535000,18.860800> }
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<8.460000,-1.535000,19.336000>}
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<8.460000,-1.535000,18.860800>}
box{<0,0,-0.203200><0.475200,0.035000,0.203200> rotate<0,-90.000000,0> translate<8.460000,-1.535000,18.860800> }
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<8.460000,-1.535000,19.336000>}
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<8.590700,-1.535000,19.466600>}
box{<0,0,-0.203200><0.184767,0.035000,0.203200> rotate<0,-44.975104,0> translate<8.460000,-1.535000,19.336000> }
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<8.561600,-1.535000,10.706000>}
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<9.246000,-1.535000,10.021600>}
box{<0,0,-0.203200><0.967888,0.035000,0.203200> rotate<0,44.997030,0> translate<8.561600,-1.535000,10.706000> }
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<8.561600,-1.535000,11.673900>}
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<8.561600,-1.535000,10.706000>}
box{<0,0,-0.203200><0.967900,0.035000,0.203200> rotate<0,-90.000000,0> translate<8.561600,-1.535000,10.706000> }
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<8.561600,-1.535000,11.673900>}
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<9.246000,-1.535000,12.358300>}
box{<0,0,-0.203200><0.967888,0.035000,0.203200> rotate<0,-44.997030,0> translate<8.561600,-1.535000,11.673900> }
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<9.151000,-1.535000,22.364400>}
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<9.151000,-1.535000,22.365400>}
box{<0,0,-0.203200><0.001000,0.035000,0.203200> rotate<0,90.000000,0> translate<9.151000,-1.535000,22.365400> }
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<9.151000,-1.535000,22.364400>}
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<10.547900,-1.535000,22.364400>}
box{<0,0,-0.203200><1.396900,0.035000,0.203200> rotate<0,0.000000,0> translate<9.151000,-1.535000,22.364400> }
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<9.151000,-1.535000,22.365400>}
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<10.547900,-1.535000,22.365400>}
box{<0,0,-0.203200><1.396900,0.035000,0.203200> rotate<0,0.000000,0> translate<9.151000,-1.535000,22.365400> }
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<9.203900,-1.535000,12.460000>}
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<9.679000,-1.535000,12.460000>}
box{<0,0,-0.203200><0.475100,0.035000,0.203200> rotate<0,0.000000,0> translate<9.203900,-1.535000,12.460000> }
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<9.203900,-1.535000,14.999900>}
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<9.679000,-1.535000,14.999900>}
box{<0,0,-0.203200><0.475100,0.035000,0.203200> rotate<0,0.000000,0> translate<9.203900,-1.535000,14.999900> }
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<9.203900,-1.535000,15.000000>}
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<9.679000,-1.535000,15.000000>}
box{<0,0,-0.203200><0.475100,0.035000,0.203200> rotate<0,0.000000,0> translate<9.203900,-1.535000,15.000000> }
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<9.203900,-1.535000,17.539900>}
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<9.679000,-1.535000,17.539900>}
box{<0,0,-0.203200><0.475100,0.035000,0.203200> rotate<0,0.000000,0> translate<9.203900,-1.535000,17.539900> }
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<9.203900,-1.535000,17.540000>}
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<9.679000,-1.535000,17.540000>}
box{<0,0,-0.203200><0.475100,0.035000,0.203200> rotate<0,0.000000,0> translate<9.203900,-1.535000,17.540000> }
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<9.246000,-1.535000,10.021600>}
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<10.213900,-1.535000,10.021600>}
box{<0,0,-0.203200><0.967900,0.035000,0.203200> rotate<0,0.000000,0> translate<9.246000,-1.535000,10.021600> }
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<9.246000,-1.535000,12.358300>}
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<10.213900,-1.535000,12.358300>}
box{<0,0,-0.203200><0.967900,0.035000,0.203200> rotate<0,0.000000,0> translate<9.246000,-1.535000,12.358300> }
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<9.678500,-1.535000,13.780800>}
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<9.678500,-1.535000,13.679000>}
box{<0,0,-0.203200><0.101800,0.035000,0.203200> rotate<0,-90.000000,0> translate<9.678500,-1.535000,13.679000> }
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<9.678500,-1.535000,16.320800>}
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<9.678500,-1.535000,16.219000>}
box{<0,0,-0.203200><0.101800,0.035000,0.203200> rotate<0,-90.000000,0> translate<9.678500,-1.535000,16.219000> }
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<9.678500,-1.535000,18.860800>}
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<9.678500,-1.535000,18.759000>}
box{<0,0,-0.203200><0.101800,0.035000,0.203200> rotate<0,-90.000000,0> translate<9.678500,-1.535000,18.759000> }
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<9.679000,-1.535000,12.460000>}
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<9.679000,-1.535000,13.678500>}
box{<0,0,-0.203200><1.218500,0.035000,0.203200> rotate<0,90.000000,0> translate<9.679000,-1.535000,13.678500> }
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<9.679000,-1.535000,12.598400>}
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<9.780800,-1.535000,12.598400>}
box{<0,0,-0.203200><0.101800,0.035000,0.203200> rotate<0,0.000000,0> translate<9.679000,-1.535000,12.598400> }
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<9.679000,-1.535000,13.004800>}
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<9.780800,-1.535000,13.004800>}
box{<0,0,-0.203200><0.101800,0.035000,0.203200> rotate<0,0.000000,0> translate<9.679000,-1.535000,13.004800> }
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<9.679000,-1.535000,13.411200>}
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<9.780800,-1.535000,13.411200>}
box{<0,0,-0.203200><0.101800,0.035000,0.203200> rotate<0,0.000000,0> translate<9.679000,-1.535000,13.411200> }
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<9.679000,-1.535000,13.678500>}
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<9.780800,-1.535000,13.678500>}
box{<0,0,-0.203200><0.101800,0.035000,0.203200> rotate<0,0.000000,0> translate<9.679000,-1.535000,13.678500> }
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<9.679000,-1.535000,13.781400>}
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<9.679000,-1.535000,14.999900>}
box{<0,0,-0.203200><1.218500,0.035000,0.203200> rotate<0,90.000000,0> translate<9.679000,-1.535000,14.999900> }
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<9.679000,-1.535000,13.781400>}
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<9.780800,-1.535000,13.781400>}
box{<0,0,-0.203200><0.101800,0.035000,0.203200> rotate<0,0.000000,0> translate<9.679000,-1.535000,13.781400> }
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<9.679000,-1.535000,13.817600>}
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<9.780800,-1.535000,13.817600>}
box{<0,0,-0.203200><0.101800,0.035000,0.203200> rotate<0,0.000000,0> translate<9.679000,-1.535000,13.817600> }
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<9.679000,-1.535000,14.224000>}
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<9.780800,-1.535000,14.224000>}
box{<0,0,-0.203200><0.101800,0.035000,0.203200> rotate<0,0.000000,0> translate<9.679000,-1.535000,14.224000> }
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<9.679000,-1.535000,14.630400>}
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<9.780800,-1.535000,14.630400>}
box{<0,0,-0.203200><0.101800,0.035000,0.203200> rotate<0,0.000000,0> translate<9.679000,-1.535000,14.630400> }
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<9.679000,-1.535000,15.000000>}
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<9.679000,-1.535000,16.218500>}
box{<0,0,-0.203200><1.218500,0.035000,0.203200> rotate<0,90.000000,0> translate<9.679000,-1.535000,16.218500> }
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<9.679000,-1.535000,15.036800>}
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<9.780800,-1.535000,15.036800>}
box{<0,0,-0.203200><0.101800,0.035000,0.203200> rotate<0,0.000000,0> translate<9.679000,-1.535000,15.036800> }
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<9.679000,-1.535000,15.443200>}
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<9.780800,-1.535000,15.443200>}
box{<0,0,-0.203200><0.101800,0.035000,0.203200> rotate<0,0.000000,0> translate<9.679000,-1.535000,15.443200> }
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<9.679000,-1.535000,15.849600>}
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<9.780800,-1.535000,15.849600>}
box{<0,0,-0.203200><0.101800,0.035000,0.203200> rotate<0,0.000000,0> translate<9.679000,-1.535000,15.849600> }
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<9.679000,-1.535000,16.218500>}
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<9.780800,-1.535000,16.218500>}
box{<0,0,-0.203200><0.101800,0.035000,0.203200> rotate<0,0.000000,0> translate<9.679000,-1.535000,16.218500> }
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<9.679000,-1.535000,16.321400>}
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<9.679000,-1.535000,17.539900>}
box{<0,0,-0.203200><1.218500,0.035000,0.203200> rotate<0,90.000000,0> translate<9.679000,-1.535000,17.539900> }
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<9.679000,-1.535000,16.321400>}
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<9.780800,-1.535000,16.321400>}
box{<0,0,-0.203200><0.101800,0.035000,0.203200> rotate<0,0.000000,0> translate<9.679000,-1.535000,16.321400> }
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<9.679000,-1.535000,16.662400>}
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<9.780800,-1.535000,16.662400>}
box{<0,0,-0.203200><0.101800,0.035000,0.203200> rotate<0,0.000000,0> translate<9.679000,-1.535000,16.662400> }
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<9.679000,-1.535000,17.068800>}
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<9.780800,-1.535000,17.068800>}
box{<0,0,-0.203200><0.101800,0.035000,0.203200> rotate<0,0.000000,0> translate<9.679000,-1.535000,17.068800> }
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<9.679000,-1.535000,17.475200>}
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<9.780800,-1.535000,17.475200>}
box{<0,0,-0.203200><0.101800,0.035000,0.203200> rotate<0,0.000000,0> translate<9.679000,-1.535000,17.475200> }
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<9.679000,-1.535000,17.540000>}
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<9.679000,-1.535000,18.758500>}
box{<0,0,-0.203200><1.218500,0.035000,0.203200> rotate<0,90.000000,0> translate<9.679000,-1.535000,18.758500> }
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<9.679000,-1.535000,17.881600>}
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<9.780800,-1.535000,17.881600>}
box{<0,0,-0.203200><0.101800,0.035000,0.203200> rotate<0,0.000000,0> translate<9.679000,-1.535000,17.881600> }
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<9.679000,-1.535000,18.288000>}
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<9.780800,-1.535000,18.288000>}
box{<0,0,-0.203200><0.101800,0.035000,0.203200> rotate<0,0.000000,0> translate<9.679000,-1.535000,18.288000> }
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<9.679000,-1.535000,18.694400>}
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<9.780800,-1.535000,18.694400>}
box{<0,0,-0.203200><0.101800,0.035000,0.203200> rotate<0,0.000000,0> translate<9.679000,-1.535000,18.694400> }
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<9.679000,-1.535000,18.758500>}
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<9.780800,-1.535000,18.758500>}
box{<0,0,-0.203200><0.101800,0.035000,0.203200> rotate<0,0.000000,0> translate<9.679000,-1.535000,18.758500> }
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<9.780800,-1.535000,12.460000>}
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<10.256000,-1.535000,12.460000>}
box{<0,0,-0.203200><0.475200,0.035000,0.203200> rotate<0,0.000000,0> translate<9.780800,-1.535000,12.460000> }
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<9.780800,-1.535000,13.678500>}
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<9.780800,-1.535000,12.460000>}
box{<0,0,-0.203200><1.218500,0.035000,0.203200> rotate<0,-90.000000,0> translate<9.780800,-1.535000,12.460000> }
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<9.780800,-1.535000,14.999900>}
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<9.780800,-1.535000,13.781400>}
box{<0,0,-0.203200><1.218500,0.035000,0.203200> rotate<0,-90.000000,0> translate<9.780800,-1.535000,13.781400> }
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<9.780800,-1.535000,14.999900>}
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<10.256000,-1.535000,14.999900>}
box{<0,0,-0.203200><0.475200,0.035000,0.203200> rotate<0,0.000000,0> translate<9.780800,-1.535000,14.999900> }
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<9.780800,-1.535000,15.000000>}
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<10.256000,-1.535000,15.000000>}
box{<0,0,-0.203200><0.475200,0.035000,0.203200> rotate<0,0.000000,0> translate<9.780800,-1.535000,15.000000> }
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<9.780800,-1.535000,16.218500>}
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<9.780800,-1.535000,15.000000>}
box{<0,0,-0.203200><1.218500,0.035000,0.203200> rotate<0,-90.000000,0> translate<9.780800,-1.535000,15.000000> }
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<9.780800,-1.535000,17.539900>}
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<9.780800,-1.535000,16.321400>}
box{<0,0,-0.203200><1.218500,0.035000,0.203200> rotate<0,-90.000000,0> translate<9.780800,-1.535000,16.321400> }
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<9.780800,-1.535000,17.539900>}
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<10.256000,-1.535000,17.539900>}
box{<0,0,-0.203200><0.475200,0.035000,0.203200> rotate<0,0.000000,0> translate<9.780800,-1.535000,17.539900> }
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<9.780800,-1.535000,17.540000>}
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<10.256000,-1.535000,17.540000>}
box{<0,0,-0.203200><0.475200,0.035000,0.203200> rotate<0,0.000000,0> translate<9.780800,-1.535000,17.540000> }
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<9.780800,-1.535000,18.758500>}
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<9.780800,-1.535000,17.540000>}
box{<0,0,-0.203200><1.218500,0.035000,0.203200> rotate<0,-90.000000,0> translate<9.780800,-1.535000,17.540000> }
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<9.781400,-1.535000,13.679000>}
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<9.781400,-1.535000,13.780800>}
box{<0,0,-0.203200><0.101800,0.035000,0.203200> rotate<0,90.000000,0> translate<9.781400,-1.535000,13.780800> }
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<9.781400,-1.535000,13.679000>}
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<10.999900,-1.535000,13.679000>}
box{<0,0,-0.203200><1.218500,0.035000,0.203200> rotate<0,0.000000,0> translate<9.781400,-1.535000,13.679000> }
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<9.781400,-1.535000,13.780800>}
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<10.999900,-1.535000,13.780800>}
box{<0,0,-0.203200><1.218500,0.035000,0.203200> rotate<0,0.000000,0> translate<9.781400,-1.535000,13.780800> }
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<9.781400,-1.535000,16.219000>}
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<9.781400,-1.535000,16.320800>}
box{<0,0,-0.203200><0.101800,0.035000,0.203200> rotate<0,90.000000,0> translate<9.781400,-1.535000,16.320800> }
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<9.781400,-1.535000,16.219000>}
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<10.999900,-1.535000,16.219000>}
box{<0,0,-0.203200><1.218500,0.035000,0.203200> rotate<0,0.000000,0> translate<9.781400,-1.535000,16.219000> }
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<9.781400,-1.535000,16.256000>}
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<11.101600,-1.535000,16.256000>}
box{<0,0,-0.203200><1.320200,0.035000,0.203200> rotate<0,0.000000,0> translate<9.781400,-1.535000,16.256000> }
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<9.781400,-1.535000,16.320800>}
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<10.999900,-1.535000,16.320800>}
box{<0,0,-0.203200><1.218500,0.035000,0.203200> rotate<0,0.000000,0> translate<9.781400,-1.535000,16.320800> }
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<9.781400,-1.535000,18.759000>}
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<9.781400,-1.535000,18.860800>}
box{<0,0,-0.203200><0.101800,0.035000,0.203200> rotate<0,90.000000,0> translate<9.781400,-1.535000,18.860800> }
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<9.781400,-1.535000,18.759000>}
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<10.999900,-1.535000,18.759000>}
box{<0,0,-0.203200><1.218500,0.035000,0.203200> rotate<0,0.000000,0> translate<9.781400,-1.535000,18.759000> }
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<9.781400,-1.535000,18.860800>}
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<10.999900,-1.535000,18.860800>}
box{<0,0,-0.203200><1.218500,0.035000,0.203200> rotate<0,0.000000,0> translate<9.781400,-1.535000,18.860800> }
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<9.961800,-1.535000,6.533200>}
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<10.078100,-1.535000,6.252700>}
box{<0,0,-0.203200><0.303654,0.035000,0.203200> rotate<0,67.475831,0> translate<9.961800,-1.535000,6.533200> }
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<9.961800,-1.535000,6.866700>}
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<9.961800,-1.535000,6.533200>}
box{<0,0,-0.203200><0.333500,0.035000,0.203200> rotate<0,-90.000000,0> translate<9.961800,-1.535000,6.533200> }
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<9.961800,-1.535000,6.866700>}
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<10.089400,-1.535000,7.174700>}
box{<0,0,-0.203200><0.333385,0.035000,0.203200> rotate<0,-67.492017,0> translate<9.961800,-1.535000,6.866700> }
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<9.993600,-1.535000,2.631600>}
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<10.231600,-1.535000,2.393600>}
box{<0,0,-0.203200><0.336583,0.035000,0.203200> rotate<0,44.997030,0> translate<9.993600,-1.535000,2.631600> }
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<9.993600,-1.535000,2.839600>}
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<9.993600,-1.535000,2.631600>}
box{<0,0,-0.203200><0.208000,0.035000,0.203200> rotate<0,-90.000000,0> translate<9.993600,-1.535000,2.631600> }
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<9.993600,-1.535000,4.268300>}
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<9.993600,-1.535000,4.160300>}
box{<0,0,-0.203200><0.108000,0.035000,0.203200> rotate<0,-90.000000,0> translate<9.993600,-1.535000,4.160300> }
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<9.993600,-1.535000,4.268300>}
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<10.125300,-1.535000,4.399900>}
box{<0,0,-0.203200><0.186181,0.035000,0.203200> rotate<0,-44.975271,0> translate<9.993600,-1.535000,4.268300> }
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<9.993600,-1.535000,4.531600>}
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<10.125300,-1.535000,4.399900>}
box{<0,0,-0.203200><0.186252,0.035000,0.203200> rotate<0,44.997030,0> translate<9.993600,-1.535000,4.531600> }
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<9.993600,-1.535000,6.168300>}
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<9.993600,-1.535000,4.531600>}
box{<0,0,-0.203200><1.636700,0.035000,0.203200> rotate<0,-90.000000,0> translate<9.993600,-1.535000,4.531600> }
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<9.993600,-1.535000,6.168300>}
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<10.078100,-1.535000,6.252700>}
box{<0,0,-0.203200><0.119430,0.035000,0.203200> rotate<0,-44.963110,0> translate<9.993600,-1.535000,6.168300> }
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<10.089400,-1.535000,7.174700>}
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<10.325200,-1.535000,7.410500>}
box{<0,0,-0.203200><0.333472,0.035000,0.203200> rotate<0,-44.997030,0> translate<10.089400,-1.535000,7.174700> }
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<10.213900,-1.535000,10.021600>}
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<10.898300,-1.535000,10.706000>}
box{<0,0,-0.203200><0.967888,0.035000,0.203200> rotate<0,-44.997030,0> translate<10.213900,-1.535000,10.021600> }
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<10.213900,-1.535000,12.358300>}
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<10.898300,-1.535000,11.673900>}
box{<0,0,-0.203200><0.967888,0.035000,0.203200> rotate<0,44.997030,0> translate<10.213900,-1.535000,12.358300> }
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<10.231600,-1.535000,2.393600>}
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<12.168300,-1.535000,2.393600>}
box{<0,0,-0.203200><1.936700,0.035000,0.203200> rotate<0,0.000000,0> translate<10.231600,-1.535000,2.393600> }
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<10.256000,-1.535000,12.460000>}
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<10.999900,-1.535000,13.203900>}
box{<0,0,-0.203200><1.052033,0.035000,0.203200> rotate<0,-44.997030,0> translate<10.256000,-1.535000,12.460000> }
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<10.256000,-1.535000,14.999900>}
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<10.999900,-1.535000,14.256000>}
box{<0,0,-0.203200><1.052033,0.035000,0.203200> rotate<0,44.997030,0> translate<10.256000,-1.535000,14.999900> }
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<10.256000,-1.535000,15.000000>}
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<10.999900,-1.535000,15.743900>}
box{<0,0,-0.203200><1.052033,0.035000,0.203200> rotate<0,-44.997030,0> translate<10.256000,-1.535000,15.000000> }
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<10.256000,-1.535000,17.539900>}
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<10.999900,-1.535000,16.796000>}
box{<0,0,-0.203200><1.052033,0.035000,0.203200> rotate<0,44.997030,0> translate<10.256000,-1.535000,17.539900> }
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<10.256000,-1.535000,17.540000>}
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<10.999900,-1.535000,18.283900>}
box{<0,0,-0.203200><1.052033,0.035000,0.203200> rotate<0,-44.997030,0> translate<10.256000,-1.535000,17.540000> }
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<10.292800,-1.535000,15.036800>}
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<15.366600,-1.535000,15.036800>}
box{<0,0,-0.203200><5.073800,0.035000,0.203200> rotate<0,0.000000,0> translate<10.292800,-1.535000,15.036800> }
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<10.320700,-1.535000,17.475200>}
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<15.093600,-1.535000,17.475200>}
box{<0,0,-0.203200><4.772900,0.035000,0.203200> rotate<0,0.000000,0> translate<10.320700,-1.535000,17.475200> }
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<10.325200,-1.535000,7.410500>}
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<10.633200,-1.535000,7.538100>}
box{<0,0,-0.203200><0.333385,0.035000,0.203200> rotate<0,-22.502043,0> translate<10.325200,-1.535000,7.410500> }
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<10.352300,-1.535000,10.160000>}
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<11.647600,-1.535000,10.160000>}
box{<0,0,-0.203200><1.295300,0.035000,0.203200> rotate<0,0.000000,0> translate<10.352300,-1.535000,10.160000> }
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<10.380200,-1.535000,12.192000>}
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<11.619700,-1.535000,12.192000>}
box{<0,0,-0.203200><1.239500,0.035000,0.203200> rotate<0,0.000000,0> translate<10.380200,-1.535000,12.192000> }
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<10.394400,-1.535000,12.598400>}
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<11.749200,-1.535000,12.598400>}
box{<0,0,-0.203200><1.354800,0.035000,0.203200> rotate<0,0.000000,0> translate<10.394400,-1.535000,12.598400> }
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<10.547900,-1.535000,21.938100>}
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<10.547900,-1.535000,22.364400>}
box{<0,0,-0.203200><0.426300,0.035000,0.203200> rotate<0,90.000000,0> translate<10.547900,-1.535000,22.364400> }
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<10.547900,-1.535000,21.938100>}
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<10.866700,-1.535000,21.938100>}
box{<0,0,-0.203200><0.318800,0.035000,0.203200> rotate<0,0.000000,0> translate<10.547900,-1.535000,21.938100> }
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<10.547900,-1.535000,21.945600>}
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<12.659900,-1.535000,21.945600>}
box{<0,0,-0.203200><2.112000,0.035000,0.203200> rotate<0,0.000000,0> translate<10.547900,-1.535000,21.945600> }
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<10.547900,-1.535000,22.352000>}
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<11.984100,-1.535000,22.352000>}
box{<0,0,-0.203200><1.436200,0.035000,0.203200> rotate<0,0.000000,0> translate<10.547900,-1.535000,22.352000> }
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<10.547900,-1.535000,22.365400>}
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<10.547900,-1.535000,22.761800>}
box{<0,0,-0.203200><0.396400,0.035000,0.203200> rotate<0,90.000000,0> translate<10.547900,-1.535000,22.761800> }
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<10.547900,-1.535000,22.758400>}
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<11.614800,-1.535000,22.758400>}
box{<0,0,-0.203200><1.066900,0.035000,0.203200> rotate<0,0.000000,0> translate<10.547900,-1.535000,22.758400> }
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<10.547900,-1.535000,22.761800>}
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<10.866700,-1.535000,22.761800>}
box{<0,0,-0.203200><0.318800,0.035000,0.203200> rotate<0,0.000000,0> translate<10.547900,-1.535000,22.761800> }
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<10.597600,-1.535000,17.881600>}
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<11.546000,-1.535000,17.881600>}
box{<0,0,-0.203200><0.948400,0.035000,0.203200> rotate<0,0.000000,0> translate<10.597600,-1.535000,17.881600> }
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<10.620900,-1.535000,26.833300>}
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<10.720900,-1.535000,26.733300>}
box{<0,0,-0.203200><0.141421,0.035000,0.203200> rotate<0,44.997030,0> translate<10.620900,-1.535000,26.833300> }
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<10.625500,-1.535000,14.630400>}
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<11.518100,-1.535000,14.630400>}
box{<0,0,-0.203200><0.892600,0.035000,0.203200> rotate<0,0.000000,0> translate<10.625500,-1.535000,14.630400> }
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<10.631800,-1.535000,26.822400>}
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<11.571800,-1.535000,26.822400>}
box{<0,0,-0.203200><0.940000,0.035000,0.203200> rotate<0,0.000000,0> translate<10.631800,-1.535000,26.822400> }
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<10.633200,-1.535000,7.538100>}
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<10.966700,-1.535000,7.538100>}
box{<0,0,-0.203200><0.333500,0.035000,0.203200> rotate<0,0.000000,0> translate<10.633200,-1.535000,7.538100> }
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<10.699200,-1.535000,15.443200>}
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<11.444400,-1.535000,15.443200>}
box{<0,0,-0.203200><0.745200,0.035000,0.203200> rotate<0,0.000000,0> translate<10.699200,-1.535000,15.443200> }
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<10.720900,-1.535000,26.733300>}
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<10.820900,-1.535000,26.733300>}
box{<0,0,-0.203200><0.100000,0.035000,0.203200> rotate<0,0.000000,0> translate<10.720900,-1.535000,26.733300> }
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<10.727100,-1.535000,17.068800>}
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<11.416500,-1.535000,17.068800>}
box{<0,0,-0.203200><0.689400,0.035000,0.203200> rotate<0,0.000000,0> translate<10.727100,-1.535000,17.068800> }
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<10.758700,-1.535000,10.566400>}
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<11.241200,-1.535000,10.566400>}
box{<0,0,-0.203200><0.482500,0.035000,0.203200> rotate<0,0.000000,0> translate<10.758700,-1.535000,10.566400> }
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<10.786600,-1.535000,11.785600>}
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<11.213300,-1.535000,11.785600>}
box{<0,0,-0.203200><0.426700,0.035000,0.203200> rotate<0,0.000000,0> translate<10.786600,-1.535000,11.785600> }
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<10.800800,-1.535000,13.004800>}
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<11.342800,-1.535000,13.004800>}
box{<0,0,-0.203200><0.542000,0.035000,0.203200> rotate<0,0.000000,0> translate<10.800800,-1.535000,13.004800> }
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<10.820900,-1.535000,26.733300>}
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<11.133300,-1.535000,26.420900>}
box{<0,0,-0.203200><0.441800,0.035000,0.203200> rotate<0,44.997030,0> translate<10.820900,-1.535000,26.733300> }
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<10.866700,-1.535000,21.938100>}
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<11.174700,-1.535000,21.810500>}
box{<0,0,-0.203200><0.333385,0.035000,0.203200> rotate<0,22.502043,0> translate<10.866700,-1.535000,21.938100> }
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<10.866700,-1.535000,22.761800>}
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<11.174700,-1.535000,22.889400>}
box{<0,0,-0.203200><0.333385,0.035000,0.203200> rotate<0,-22.502043,0> translate<10.866700,-1.535000,22.761800> }
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<10.869200,-1.535000,19.466600>}
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<10.999900,-1.535000,19.336000>}
box{<0,0,-0.203200><0.184767,0.035000,0.203200> rotate<0,44.975104,0> translate<10.869200,-1.535000,19.466600> }
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<10.869200,-1.535000,19.466600>}
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<11.120900,-1.535000,19.466600>}
box{<0,0,-0.203200><0.251700,0.035000,0.203200> rotate<0,0.000000,0> translate<10.869200,-1.535000,19.466600> }
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<10.898300,-1.535000,10.706000>}
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<10.898300,-1.535000,11.673900>}
box{<0,0,-0.203200><0.967900,0.035000,0.203200> rotate<0,90.000000,0> translate<10.898300,-1.535000,11.673900> }
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<10.898300,-1.535000,10.972800>}
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<11.101600,-1.535000,10.972800>}
box{<0,0,-0.203200><0.203300,0.035000,0.203200> rotate<0,0.000000,0> translate<10.898300,-1.535000,10.972800> }
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<10.898300,-1.535000,11.379200>}
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<11.101600,-1.535000,11.379200>}
box{<0,0,-0.203200><0.203300,0.035000,0.203200> rotate<0,0.000000,0> translate<10.898300,-1.535000,11.379200> }
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<10.966700,-1.535000,7.538100>}
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<11.274700,-1.535000,7.410500>}
box{<0,0,-0.203200><0.333385,0.035000,0.203200> rotate<0,22.502043,0> translate<10.966700,-1.535000,7.538100> }
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<10.999900,-1.535000,13.203900>}
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<10.999900,-1.535000,13.679000>}
box{<0,0,-0.203200><0.475100,0.035000,0.203200> rotate<0,90.000000,0> translate<10.999900,-1.535000,13.679000> }
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<10.999900,-1.535000,13.411200>}
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<11.101600,-1.535000,13.411200>}
box{<0,0,-0.203200><0.101700,0.035000,0.203200> rotate<0,0.000000,0> translate<10.999900,-1.535000,13.411200> }
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<10.999900,-1.535000,13.780800>}
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<10.999900,-1.535000,14.256000>}
box{<0,0,-0.203200><0.475200,0.035000,0.203200> rotate<0,90.000000,0> translate<10.999900,-1.535000,14.256000> }
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<10.999900,-1.535000,13.817600>}
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<11.101600,-1.535000,13.817600>}
box{<0,0,-0.203200><0.101700,0.035000,0.203200> rotate<0,0.000000,0> translate<10.999900,-1.535000,13.817600> }
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<10.999900,-1.535000,14.224000>}
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<11.111700,-1.535000,14.224000>}
box{<0,0,-0.203200><0.111800,0.035000,0.203200> rotate<0,0.000000,0> translate<10.999900,-1.535000,14.224000> }
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<10.999900,-1.535000,15.743900>}
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<10.999900,-1.535000,16.219000>}
box{<0,0,-0.203200><0.475100,0.035000,0.203200> rotate<0,90.000000,0> translate<10.999900,-1.535000,16.219000> }
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<10.999900,-1.535000,15.849600>}
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<11.101600,-1.535000,15.849600>}
box{<0,0,-0.203200><0.101700,0.035000,0.203200> rotate<0,0.000000,0> translate<10.999900,-1.535000,15.849600> }
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<10.999900,-1.535000,16.320800>}
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<10.999900,-1.535000,16.796000>}
box{<0,0,-0.203200><0.475200,0.035000,0.203200> rotate<0,90.000000,0> translate<10.999900,-1.535000,16.796000> }
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<10.999900,-1.535000,16.662400>}
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<11.101600,-1.535000,16.662400>}
box{<0,0,-0.203200><0.101700,0.035000,0.203200> rotate<0,0.000000,0> translate<10.999900,-1.535000,16.662400> }
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<10.999900,-1.535000,18.283900>}
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<10.999900,-1.535000,18.759000>}
box{<0,0,-0.203200><0.475100,0.035000,0.203200> rotate<0,90.000000,0> translate<10.999900,-1.535000,18.759000> }
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<10.999900,-1.535000,18.288000>}
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<11.139600,-1.535000,18.288000>}
box{<0,0,-0.203200><0.139700,0.035000,0.203200> rotate<0,0.000000,0> translate<10.999900,-1.535000,18.288000> }
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<10.999900,-1.535000,18.694400>}
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<11.101600,-1.535000,18.694400>}
box{<0,0,-0.203200><0.101700,0.035000,0.203200> rotate<0,0.000000,0> translate<10.999900,-1.535000,18.694400> }
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<10.999900,-1.535000,18.860800>}
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<10.999900,-1.535000,19.336000>}
box{<0,0,-0.203200><0.475200,0.035000,0.203200> rotate<0,90.000000,0> translate<10.999900,-1.535000,19.336000> }
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<10.999900,-1.535000,19.100800>}
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<11.101600,-1.535000,19.100800>}
box{<0,0,-0.203200><0.101700,0.035000,0.203200> rotate<0,0.000000,0> translate<10.999900,-1.535000,19.100800> }
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<11.101600,-1.535000,10.706000>}
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<11.786000,-1.535000,10.021600>}
box{<0,0,-0.203200><0.967888,0.035000,0.203200> rotate<0,44.997030,0> translate<11.101600,-1.535000,10.706000> }
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<11.101600,-1.535000,11.673900>}
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<11.101600,-1.535000,10.706000>}
box{<0,0,-0.203200><0.967900,0.035000,0.203200> rotate<0,-90.000000,0> translate<11.101600,-1.535000,10.706000> }
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<11.101600,-1.535000,11.673900>}
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<11.786000,-1.535000,12.358300>}
box{<0,0,-0.203200><0.967888,0.035000,0.203200> rotate<0,-44.997030,0> translate<11.101600,-1.535000,11.673900> }
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<11.101600,-1.535000,13.246000>}
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<11.786000,-1.535000,12.561600>}
box{<0,0,-0.203200><0.967888,0.035000,0.203200> rotate<0,44.997030,0> translate<11.101600,-1.535000,13.246000> }
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<11.101600,-1.535000,14.213900>}
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<11.101600,-1.535000,13.246000>}
box{<0,0,-0.203200><0.967900,0.035000,0.203200> rotate<0,-90.000000,0> translate<11.101600,-1.535000,13.246000> }
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<11.101600,-1.535000,14.213900>}
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<11.786000,-1.535000,14.898300>}
box{<0,0,-0.203200><0.967888,0.035000,0.203200> rotate<0,-44.997030,0> translate<11.101600,-1.535000,14.213900> }
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<11.101600,-1.535000,15.786000>}
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<11.786000,-1.535000,15.101600>}
box{<0,0,-0.203200><0.967888,0.035000,0.203200> rotate<0,44.997030,0> translate<11.101600,-1.535000,15.786000> }
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<11.101600,-1.535000,16.753900>}
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<11.101600,-1.535000,15.786000>}
box{<0,0,-0.203200><0.967900,0.035000,0.203200> rotate<0,-90.000000,0> translate<11.101600,-1.535000,15.786000> }
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<11.101600,-1.535000,16.753900>}
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<11.786000,-1.535000,17.438300>}
box{<0,0,-0.203200><0.967888,0.035000,0.203200> rotate<0,-44.997030,0> translate<11.101600,-1.535000,16.753900> }
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<11.101600,-1.535000,18.326000>}
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<11.786000,-1.535000,17.641600>}
box{<0,0,-0.203200><0.967888,0.035000,0.203200> rotate<0,44.997030,0> translate<11.101600,-1.535000,18.326000> }
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<11.101600,-1.535000,19.293900>}
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<11.101600,-1.535000,18.326000>}
box{<0,0,-0.203200><0.967900,0.035000,0.203200> rotate<0,-90.000000,0> translate<11.101600,-1.535000,18.326000> }
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<11.101600,-1.535000,19.293900>}
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<11.574400,-1.535000,19.766600>}
box{<0,0,-0.203200><0.668569,0.035000,0.203200> rotate<0,-44.990971,0> translate<11.101600,-1.535000,19.293900> }
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<11.120900,-1.535000,19.466600>}
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<11.420900,-1.535000,19.766600>}
box{<0,0,-0.203200><0.424264,0.035000,0.203200> rotate<0,-44.997030,0> translate<11.120900,-1.535000,19.466600> }
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<11.133300,-1.535000,26.420900>}
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<11.220900,-1.535000,26.333300>}
box{<0,0,-0.203200><0.123885,0.035000,0.203200> rotate<0,44.997030,0> translate<11.133300,-1.535000,26.420900> }
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<11.138200,-1.535000,26.416000>}
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<11.880100,-1.535000,26.416000>}
box{<0,0,-0.203200><0.741900,0.035000,0.203200> rotate<0,0.000000,0> translate<11.138200,-1.535000,26.416000> }
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<11.161500,-1.535000,19.507200>}
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<11.315000,-1.535000,19.507200>}
box{<0,0,-0.203200><0.153500,0.035000,0.203200> rotate<0,0.000000,0> translate<11.161500,-1.535000,19.507200> }
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<11.174700,-1.535000,21.810500>}
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<11.410500,-1.535000,21.574700>}
box{<0,0,-0.203200><0.333472,0.035000,0.203200> rotate<0,44.997030,0> translate<11.174700,-1.535000,21.810500> }
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<11.174700,-1.535000,22.889400>}
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<11.410500,-1.535000,23.125200>}
box{<0,0,-0.203200><0.333472,0.035000,0.203200> rotate<0,-44.997030,0> translate<11.174700,-1.535000,22.889400> }
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<11.220900,-1.535000,26.333300>}
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<11.962900,-1.535000,26.333300>}
box{<0,0,-0.203200><0.742000,0.035000,0.203200> rotate<0,0.000000,0> translate<11.220900,-1.535000,26.333300> }
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<11.274700,-1.535000,7.410500>}
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<11.510500,-1.535000,7.174700>}
box{<0,0,-0.203200><0.333472,0.035000,0.203200> rotate<0,44.997030,0> translate<11.274700,-1.535000,7.410500> }
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<11.338800,-1.535000,27.384800>}
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<11.641200,-1.535000,26.654900>}
box{<0,0,-0.203200><0.790063,0.035000,0.203200> rotate<0,67.491156,0> translate<11.338800,-1.535000,27.384800> }
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<11.338800,-1.535000,28.175100>}
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<11.338800,-1.535000,27.384800>}
box{<0,0,-0.203200><0.790300,0.035000,0.203200> rotate<0,-90.000000,0> translate<11.338800,-1.535000,27.384800> }
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<11.338800,-1.535000,28.175100>}
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<11.641200,-1.535000,28.905000>}
box{<0,0,-0.203200><0.790063,0.035000,0.203200> rotate<0,-67.491156,0> translate<11.338800,-1.535000,28.175100> }
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<11.370000,-1.535000,7.315200>}
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<19.560900,-1.535000,7.315200>}
box{<0,0,-0.203200><8.190900,0.035000,0.203200> rotate<0,0.000000,0> translate<11.370000,-1.535000,7.315200> }
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<11.410500,-1.535000,21.574700>}
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<11.538100,-1.535000,21.266700>}
box{<0,0,-0.203200><0.333385,0.035000,0.203200> rotate<0,67.492017,0> translate<11.410500,-1.535000,21.574700> }
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<11.410500,-1.535000,23.125200>}
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<11.436700,-1.535000,23.188600>}
box{<0,0,-0.203200><0.068600,0.035000,0.203200> rotate<0,-67.542720,0> translate<11.410500,-1.535000,23.125200> }
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<11.420900,-1.535000,19.766600>}
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<11.574400,-1.535000,19.766600>}
box{<0,0,-0.203200><0.153500,0.035000,0.203200> rotate<0,0.000000,0> translate<11.420900,-1.535000,19.766600> }
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<11.425300,-1.535000,21.539200>}
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<16.961800,-1.535000,21.539200>}
box{<0,0,-0.203200><5.536500,0.035000,0.203200> rotate<0,0.000000,0> translate<11.425300,-1.535000,21.539200> }
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<11.426800,-1.535000,23.164800>}
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<11.446500,-1.535000,23.164800>}
box{<0,0,-0.203200><0.019700,0.035000,0.203200> rotate<0,0.000000,0> translate<11.426800,-1.535000,23.164800> }
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<11.436700,-1.535000,23.188600>}
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<11.641200,-1.535000,22.694900>}
box{<0,0,-0.203200><0.534378,0.035000,0.203200> rotate<0,67.495271,0> translate<11.436700,-1.535000,23.188600> }
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<11.496700,-1.535000,20.833300>}
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<11.538100,-1.535000,20.933200>}
box{<0,0,-0.203200><0.108139,0.035000,0.203200> rotate<0,-67.485724,0> translate<11.496700,-1.535000,20.833300> }
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<11.496700,-1.535000,20.833300>}
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<13.420900,-1.535000,20.833300>}
box{<0,0,-0.203200><1.924200,0.035000,0.203200> rotate<0,0.000000,0> translate<11.496700,-1.535000,20.833300> }
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<11.510500,-1.535000,7.174700>}
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<11.638100,-1.535000,6.866700>}
box{<0,0,-0.203200><0.333385,0.035000,0.203200> rotate<0,67.492017,0> translate<11.510500,-1.535000,7.174700> }
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<11.538100,-1.535000,20.933200>}
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<11.538100,-1.535000,21.266700>}
box{<0,0,-0.203200><0.333500,0.035000,0.203200> rotate<0,90.000000,0> translate<11.538100,-1.535000,21.266700> }
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<11.538100,-1.535000,21.132800>}
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<17.181800,-1.535000,21.132800>}
box{<0,0,-0.203200><5.643700,0.035000,0.203200> rotate<0,0.000000,0> translate<11.538100,-1.535000,21.132800> }
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<11.585500,-1.535000,6.406300>}
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<11.638100,-1.535000,6.533200>}
box{<0,0,-0.203200><0.137369,0.035000,0.203200> rotate<0,-67.481559,0> translate<11.585500,-1.535000,6.406300> }
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<11.585500,-1.535000,6.406300>}
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<12.168300,-1.535000,6.406300>}
box{<0,0,-0.203200><0.582800,0.035000,0.203200> rotate<0,0.000000,0> translate<11.585500,-1.535000,6.406300> }
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<11.620700,-1.535000,6.908800>}
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<15.854500,-1.535000,6.908800>}
box{<0,0,-0.203200><4.233800,0.035000,0.203200> rotate<0,0.000000,0> translate<11.620700,-1.535000,6.908800> }
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<11.625300,-1.535000,6.502400>}
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<15.466600,-1.535000,6.502400>}
box{<0,0,-0.203200><3.841300,0.035000,0.203200> rotate<0,0.000000,0> translate<11.625300,-1.535000,6.502400> }
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<11.638100,-1.535000,6.533200>}
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<11.638100,-1.535000,6.866700>}
box{<0,0,-0.203200><0.333500,0.035000,0.203200> rotate<0,90.000000,0> translate<11.638100,-1.535000,6.866700> }
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<11.641200,-1.535000,22.694900>}
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<12.199900,-1.535000,22.136200>}
box{<0,0,-0.203200><0.790121,0.035000,0.203200> rotate<0,44.997030,0> translate<11.641200,-1.535000,22.694900> }
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<11.641200,-1.535000,26.654900>}
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<11.962900,-1.535000,26.333300>}
box{<0,0,-0.203200><0.454882,0.035000,0.203200> rotate<0,44.988124,0> translate<11.641200,-1.535000,26.654900> }
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<11.641200,-1.535000,28.905000>}
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<12.025100,-1.535000,29.288800>}
box{<0,0,-0.203200><0.542846,0.035000,0.203200> rotate<0,-44.989567,0> translate<11.641200,-1.535000,28.905000> }
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<11.786000,-1.535000,10.021600>}
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<12.753900,-1.535000,10.021600>}
box{<0,0,-0.203200><0.967900,0.035000,0.203200> rotate<0,0.000000,0> translate<11.786000,-1.535000,10.021600> }
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<11.786000,-1.535000,12.358300>}
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<12.684100,-1.535000,12.358300>}
box{<0,0,-0.203200><0.898100,0.035000,0.203200> rotate<0,0.000000,0> translate<11.786000,-1.535000,12.358300> }
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<11.786000,-1.535000,12.561600>}
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<12.753900,-1.535000,12.561600>}
box{<0,0,-0.203200><0.967900,0.035000,0.203200> rotate<0,0.000000,0> translate<11.786000,-1.535000,12.561600> }
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<11.786000,-1.535000,14.898300>}
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<12.753900,-1.535000,14.898300>}
box{<0,0,-0.203200><0.967900,0.035000,0.203200> rotate<0,0.000000,0> translate<11.786000,-1.535000,14.898300> }
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<11.786000,-1.535000,15.101600>}
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<12.753900,-1.535000,15.101600>}
box{<0,0,-0.203200><0.967900,0.035000,0.203200> rotate<0,0.000000,0> translate<11.786000,-1.535000,15.101600> }
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<11.786000,-1.535000,17.438300>}
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<12.753900,-1.535000,17.438300>}
box{<0,0,-0.203200><0.967900,0.035000,0.203200> rotate<0,0.000000,0> translate<11.786000,-1.535000,17.438300> }
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<11.786000,-1.535000,17.641600>}
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<12.753900,-1.535000,17.641600>}
box{<0,0,-0.203200><0.967900,0.035000,0.203200> rotate<0,0.000000,0> translate<11.786000,-1.535000,17.641600> }
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<12.168300,-1.535000,2.393600>}
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<12.406300,-1.535000,2.631600>}
box{<0,0,-0.203200><0.336583,0.035000,0.203200> rotate<0,-44.997030,0> translate<12.168300,-1.535000,2.393600> }
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<12.168300,-1.535000,6.406300>}
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<12.406300,-1.535000,6.168300>}
box{<0,0,-0.203200><0.336583,0.035000,0.203200> rotate<0,44.997030,0> translate<12.168300,-1.535000,6.406300> }
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<12.199900,-1.535000,22.136200>}
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<12.929800,-1.535000,21.833800>}
box{<0,0,-0.203200><0.790063,0.035000,0.203200> rotate<0,22.502905,0> translate<12.199900,-1.535000,22.136200> }
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<12.213100,-1.535000,2.438400>}
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<57.688800,-1.535000,2.438400>}
box{<0,0,-0.203200><45.475700,0.035000,0.203200> rotate<0,0.000000,0> translate<12.213100,-1.535000,2.438400> }
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<12.274600,-1.535000,4.399900>}
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<12.406300,-1.535000,4.268300>}
box{<0,0,-0.203200><0.186181,0.035000,0.203200> rotate<0,44.975271,0> translate<12.274600,-1.535000,4.399900> }
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<12.274600,-1.535000,4.399900>}
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<12.406300,-1.535000,4.531600>}
box{<0,0,-0.203200><0.186252,0.035000,0.203200> rotate<0,-44.997030,0> translate<12.274600,-1.535000,4.399900> }
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<12.345100,-1.535000,4.470400>}
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<13.636500,-1.535000,4.470400>}
box{<0,0,-0.203200><1.291400,0.035000,0.203200> rotate<0,0.000000,0> translate<12.345100,-1.535000,4.470400> }
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<12.406300,-1.535000,2.631600>}
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<12.406300,-1.535000,2.939600>}
box{<0,0,-0.203200><0.308000,0.035000,0.203200> rotate<0,90.000000,0> translate<12.406300,-1.535000,2.939600> }
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<12.406300,-1.535000,2.844800>}
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<57.846300,-1.535000,2.844800>}
box{<0,0,-0.203200><45.440000,0.035000,0.203200> rotate<0,0.000000,0> translate<12.406300,-1.535000,2.844800> }
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<12.406300,-1.535000,2.939600>}
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<13.831300,-1.535000,2.939600>}
box{<0,0,-0.203200><1.425000,0.035000,0.203200> rotate<0,0.000000,0> translate<12.406300,-1.535000,2.939600> }
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<12.406300,-1.535000,4.260300>}
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<12.406300,-1.535000,4.268300>}
box{<0,0,-0.203200><0.008000,0.035000,0.203200> rotate<0,90.000000,0> translate<12.406300,-1.535000,4.268300> }
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<12.406300,-1.535000,4.260300>}
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<13.426400,-1.535000,4.260300>}
box{<0,0,-0.203200><1.020100,0.035000,0.203200> rotate<0,0.000000,0> translate<12.406300,-1.535000,4.260300> }
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<12.406300,-1.535000,4.531600>}
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<12.406300,-1.535000,4.866600>}
box{<0,0,-0.203200><0.335000,0.035000,0.203200> rotate<0,90.000000,0> translate<12.406300,-1.535000,4.866600> }
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<12.406300,-1.535000,4.866600>}
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<13.420900,-1.535000,4.866600>}
box{<0,0,-0.203200><1.014600,0.035000,0.203200> rotate<0,0.000000,0> translate<12.406300,-1.535000,4.866600> }
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<12.406300,-1.535000,5.933300>}
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<12.406300,-1.535000,6.168300>}
box{<0,0,-0.203200><0.235000,0.035000,0.203200> rotate<0,90.000000,0> translate<12.406300,-1.535000,6.168300> }
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<12.406300,-1.535000,5.933300>}
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<12.779000,-1.535000,5.933300>}
box{<0,0,-0.203200><0.372700,0.035000,0.203200> rotate<0,0.000000,0> translate<12.406300,-1.535000,5.933300> }
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<12.406300,-1.535000,6.096000>}
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<12.941700,-1.535000,6.096000>}
box{<0,0,-0.203200><0.535400,0.035000,0.203200> rotate<0,0.000000,0> translate<12.406300,-1.535000,6.096000> }
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<12.684100,-1.535000,12.358300>}
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<14.459000,-1.535000,14.133300>}
box{<0,0,-0.203200><2.510158,0.035000,0.203200> rotate<0,-44.998644,0> translate<12.684100,-1.535000,12.358300> }
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<12.753900,-1.535000,10.021600>}
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<13.438300,-1.535000,10.706000>}
box{<0,0,-0.203200><0.967888,0.035000,0.203200> rotate<0,-44.997030,0> translate<12.753900,-1.535000,10.021600> }
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<12.753900,-1.535000,12.561600>}
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<13.438300,-1.535000,13.246000>}
box{<0,0,-0.203200><0.967888,0.035000,0.203200> rotate<0,-44.997030,0> translate<12.753900,-1.535000,12.561600> }
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<12.753900,-1.535000,14.898300>}
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<13.438300,-1.535000,14.213900>}
box{<0,0,-0.203200><0.967888,0.035000,0.203200> rotate<0,44.997030,0> translate<12.753900,-1.535000,14.898300> }
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<12.753900,-1.535000,15.101600>}
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<13.438300,-1.535000,15.786000>}
box{<0,0,-0.203200><0.967888,0.035000,0.203200> rotate<0,-44.997030,0> translate<12.753900,-1.535000,15.101600> }
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<12.753900,-1.535000,17.438300>}
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<13.438300,-1.535000,16.753900>}
box{<0,0,-0.203200><0.967888,0.035000,0.203200> rotate<0,44.997030,0> translate<12.753900,-1.535000,17.438300> }
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<12.753900,-1.535000,17.641600>}
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<13.388900,-1.535000,18.276600>}
box{<0,0,-0.203200><0.898026,0.035000,0.203200> rotate<0,-44.997030,0> translate<12.753900,-1.535000,17.641600> }
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<12.779000,-1.535000,5.933300>}
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<13.079000,-1.535000,6.233300>}
box{<0,0,-0.203200><0.424264,0.035000,0.203200> rotate<0,-44.997030,0> translate<12.779000,-1.535000,5.933300> }
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<12.790700,-1.535000,12.598400>}
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<12.924200,-1.535000,12.598400>}
box{<0,0,-0.203200><0.133500,0.035000,0.203200> rotate<0,0.000000,0> translate<12.790700,-1.535000,12.598400> }
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<12.892300,-1.535000,10.160000>}
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<14.493600,-1.535000,10.160000>}
box{<0,0,-0.203200><1.601300,0.035000,0.203200> rotate<0,0.000000,0> translate<12.892300,-1.535000,10.160000> }
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<12.929800,-1.535000,21.833800>}
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<16.270100,-1.535000,21.833800>}
box{<0,0,-0.203200><3.340300,0.035000,0.203200> rotate<0,0.000000,0> translate<12.929800,-1.535000,21.833800> }
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<12.993900,-1.535000,17.881600>}
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<15.093600,-1.535000,17.881600>}
box{<0,0,-0.203200><2.099700,0.035000,0.203200> rotate<0,0.000000,0> translate<12.993900,-1.535000,17.881600> }
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<13.021800,-1.535000,14.630400>}
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<14.855700,-1.535000,14.630400>}
box{<0,0,-0.203200><1.833900,0.035000,0.203200> rotate<0,0.000000,0> translate<13.021800,-1.535000,14.630400> }
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<13.079000,-1.535000,6.233300>}
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<13.179000,-1.535000,6.333300>}
box{<0,0,-0.203200><0.141421,0.035000,0.203200> rotate<0,-44.997030,0> translate<13.079000,-1.535000,6.233300> }
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<13.095500,-1.535000,15.443200>}
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<15.366600,-1.535000,15.443200>}
box{<0,0,-0.203200><2.271100,0.035000,0.203200> rotate<0,0.000000,0> translate<13.095500,-1.535000,15.443200> }
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<13.123400,-1.535000,17.068800>}
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<15.093600,-1.535000,17.068800>}
box{<0,0,-0.203200><1.970200,0.035000,0.203200> rotate<0,0.000000,0> translate<13.123400,-1.535000,17.068800> }
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<13.179000,-1.535000,6.333300>}
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<13.620900,-1.535000,6.333300>}
box{<0,0,-0.203200><0.441900,0.035000,0.203200> rotate<0,0.000000,0> translate<13.179000,-1.535000,6.333300> }
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<13.197100,-1.535000,13.004800>}
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<13.330600,-1.535000,13.004800>}
box{<0,0,-0.203200><0.133500,0.035000,0.203200> rotate<0,0.000000,0> translate<13.197100,-1.535000,13.004800> }
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<13.298700,-1.535000,10.566400>}
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<14.493600,-1.535000,10.566400>}
box{<0,0,-0.203200><1.194900,0.035000,0.203200> rotate<0,0.000000,0> translate<13.298700,-1.535000,10.566400> }
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<13.388900,-1.535000,18.276600>}
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<16.169000,-1.535000,18.276600>}
box{<0,0,-0.203200><2.780100,0.035000,0.203200> rotate<0,0.000000,0> translate<13.388900,-1.535000,18.276600> }
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<13.420900,-1.535000,4.866600>}
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<13.733300,-1.535000,5.179000>}
box{<0,0,-0.203200><0.441800,0.035000,0.203200> rotate<0,-44.997030,0> translate<13.420900,-1.535000,4.866600> }
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<13.420900,-1.535000,20.833300>}
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<13.520900,-1.535000,20.733300>}
box{<0,0,-0.203200><0.141421,0.035000,0.203200> rotate<0,44.997030,0> translate<13.420900,-1.535000,20.833300> }
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<13.426400,-1.535000,4.260300>}
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<14.125900,-1.535000,4.959800>}
box{<0,0,-0.203200><0.989242,0.035000,0.203200> rotate<0,-44.997030,0> translate<13.426400,-1.535000,4.260300> }
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<13.428200,-1.535000,14.224000>}
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<14.793600,-1.535000,14.224000>}
box{<0,0,-0.203200><1.365400,0.035000,0.203200> rotate<0,0.000000,0> translate<13.428200,-1.535000,14.224000> }
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<13.431100,-1.535000,4.876800>}
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<14.042900,-1.535000,4.876800>}
box{<0,0,-0.203200><0.611800,0.035000,0.203200> rotate<0,0.000000,0> translate<13.431100,-1.535000,4.876800> }
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<13.438300,-1.535000,10.706000>}
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<13.438300,-1.535000,11.604100>}
box{<0,0,-0.203200><0.898100,0.035000,0.203200> rotate<0,90.000000,0> translate<13.438300,-1.535000,11.604100> }
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<13.438300,-1.535000,10.972800>}
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<14.493600,-1.535000,10.972800>}
box{<0,0,-0.203200><1.055300,0.035000,0.203200> rotate<0,0.000000,0> translate<13.438300,-1.535000,10.972800> }
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<13.438300,-1.535000,11.379200>}
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<14.604500,-1.535000,11.379200>}
box{<0,0,-0.203200><1.166200,0.035000,0.203200> rotate<0,0.000000,0> translate<13.438300,-1.535000,11.379200> }
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<13.438300,-1.535000,11.604100>}
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<14.793600,-1.535000,12.959400>}
box{<0,0,-0.203200><1.916684,0.035000,0.203200> rotate<0,-44.997030,0> translate<13.438300,-1.535000,11.604100> }
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<13.438300,-1.535000,13.246000>}
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<13.438300,-1.535000,14.213900>}
box{<0,0,-0.203200><0.967900,0.035000,0.203200> rotate<0,90.000000,0> translate<13.438300,-1.535000,14.213900> }
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<13.438300,-1.535000,13.411200>}
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<13.737000,-1.535000,13.411200>}
box{<0,0,-0.203200><0.298700,0.035000,0.203200> rotate<0,0.000000,0> translate<13.438300,-1.535000,13.411200> }
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<13.438300,-1.535000,13.817600>}
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<14.143400,-1.535000,13.817600>}
box{<0,0,-0.203200><0.705100,0.035000,0.203200> rotate<0,0.000000,0> translate<13.438300,-1.535000,13.817600> }
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<13.438300,-1.535000,15.786000>}
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<13.438300,-1.535000,16.753900>}
box{<0,0,-0.203200><0.967900,0.035000,0.203200> rotate<0,90.000000,0> translate<13.438300,-1.535000,16.753900> }
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<13.438300,-1.535000,15.849600>}
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<15.275600,-1.535000,15.849600>}
box{<0,0,-0.203200><1.837300,0.035000,0.203200> rotate<0,0.000000,0> translate<13.438300,-1.535000,15.849600> }
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<13.438300,-1.535000,16.256000>}
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<15.093600,-1.535000,16.256000>}
box{<0,0,-0.203200><1.655300,0.035000,0.203200> rotate<0,0.000000,0> translate<13.438300,-1.535000,16.256000> }
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<13.438300,-1.535000,16.662400>}
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<15.093600,-1.535000,16.662400>}
box{<0,0,-0.203200><1.655300,0.035000,0.203200> rotate<0,0.000000,0> translate<13.438300,-1.535000,16.662400> }
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<13.520900,-1.535000,20.733300>}
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<13.720900,-1.535000,20.733300>}
box{<0,0,-0.203200><0.200000,0.035000,0.203200> rotate<0,0.000000,0> translate<13.520900,-1.535000,20.733300> }
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<13.619800,-1.535000,11.785600>}
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<15.760000,-1.535000,11.785600>}
box{<0,0,-0.203200><2.140200,0.035000,0.203200> rotate<0,0.000000,0> translate<13.619800,-1.535000,11.785600> }
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<13.620900,-1.535000,6.333300>}
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<15.379000,-1.535000,6.333300>}
box{<0,0,-0.203200><1.758100,0.035000,0.203200> rotate<0,0.000000,0> translate<13.620900,-1.535000,6.333300> }
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<13.720900,-1.535000,20.733300>}
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<13.820900,-1.535000,20.633300>}
box{<0,0,-0.203200><0.141421,0.035000,0.203200> rotate<0,44.997030,0> translate<13.720900,-1.535000,20.733300> }
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<13.727800,-1.535000,20.726400>}
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<17.363600,-1.535000,20.726400>}
box{<0,0,-0.203200><3.635800,0.035000,0.203200> rotate<0,0.000000,0> translate<13.727800,-1.535000,20.726400> }
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<13.733300,-1.535000,5.179000>}
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<13.733300,-1.535000,5.266600>}
box{<0,0,-0.203200><0.087600,0.035000,0.203200> rotate<0,90.000000,0> translate<13.733300,-1.535000,5.266600> }
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<13.733300,-1.535000,5.266600>}
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<16.020900,-1.535000,5.266600>}
box{<0,0,-0.203200><2.287600,0.035000,0.203200> rotate<0,0.000000,0> translate<13.733300,-1.535000,5.266600> }
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<13.820900,-1.535000,20.633300>}
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<17.248000,-1.535000,20.633300>}
box{<0,0,-0.203200><3.427100,0.035000,0.203200> rotate<0,0.000000,0> translate<13.820900,-1.535000,20.633300> }
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<13.831300,-1.535000,2.939600>}
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<14.074000,-1.535000,3.040100>}
box{<0,0,-0.203200><0.262685,0.035000,0.203200> rotate<0,-22.492544,0> translate<13.831300,-1.535000,2.939600> }
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<14.026200,-1.535000,12.192000>}
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<15.353600,-1.535000,12.192000>}
box{<0,0,-0.203200><1.327400,0.035000,0.203200> rotate<0,0.000000,0> translate<14.026200,-1.535000,12.192000> }
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<14.074000,-1.535000,3.040100>}
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<14.259800,-1.535000,3.225900>}
box{<0,0,-0.203200><0.262761,0.035000,0.203200> rotate<0,-44.997030,0> translate<14.074000,-1.535000,3.040100> }
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<14.125900,-1.535000,4.959800>}
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<14.368600,-1.535000,5.060300>}
box{<0,0,-0.203200><0.262685,0.035000,0.203200> rotate<0,-22.492544,0> translate<14.125900,-1.535000,4.959800> }
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<14.259800,-1.535000,3.225900>}
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<14.773500,-1.535000,3.739600>}
box{<0,0,-0.203200><0.726482,0.035000,0.203200> rotate<0,-44.997030,0> translate<14.259800,-1.535000,3.225900> }
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<14.285100,-1.535000,3.251200>}
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<16.352500,-1.535000,3.251200>}
box{<0,0,-0.203200><2.067400,0.035000,0.203200> rotate<0,0.000000,0> translate<14.285100,-1.535000,3.251200> }
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<14.368600,-1.535000,5.060300>}
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<14.631300,-1.535000,5.060300>}
box{<0,0,-0.203200><0.262700,0.035000,0.203200> rotate<0,0.000000,0> translate<14.368600,-1.535000,5.060300> }
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<14.432600,-1.535000,12.598400>}
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<14.826800,-1.535000,12.598400>}
box{<0,0,-0.203200><0.394200,0.035000,0.203200> rotate<0,0.000000,0> translate<14.432600,-1.535000,12.598400> }
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<14.459000,-1.535000,14.133300>}
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<14.793600,-1.535000,14.133300>}
box{<0,0,-0.203200><0.334600,0.035000,0.203200> rotate<0,0.000000,0> translate<14.459000,-1.535000,14.133300> }
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<14.493600,-1.535000,9.331600>}
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<14.731600,-1.535000,9.093600>}
box{<0,0,-0.203200><0.336583,0.035000,0.203200> rotate<0,44.997030,0> translate<14.493600,-1.535000,9.331600> }
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<14.493600,-1.535000,11.268300>}
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<14.493600,-1.535000,9.331600>}
box{<0,0,-0.203200><1.936700,0.035000,0.203200> rotate<0,-90.000000,0> translate<14.493600,-1.535000,9.331600> }
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<14.493600,-1.535000,11.268300>}
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<14.731600,-1.535000,11.506300>}
box{<0,0,-0.203200><0.336583,0.035000,0.203200> rotate<0,-44.997030,0> translate<14.493600,-1.535000,11.268300> }
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<14.631300,-1.535000,5.060300>}
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<15.940800,-1.535000,5.060300>}
box{<0,0,-0.203200><1.309500,0.035000,0.203200> rotate<0,0.000000,0> translate<14.631300,-1.535000,5.060300> }
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<14.691500,-1.535000,3.657600>}
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<15.974700,-1.535000,3.657600>}
box{<0,0,-0.203200><1.283200,0.035000,0.203200> rotate<0,0.000000,0> translate<14.691500,-1.535000,3.657600> }
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<14.731600,-1.535000,9.093600>}
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<15.366600,-1.535000,9.093600>}
box{<0,0,-0.203200><0.635000,0.035000,0.203200> rotate<0,0.000000,0> translate<14.731600,-1.535000,9.093600> }
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<14.731600,-1.535000,11.506300>}
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<16.368300,-1.535000,11.506300>}
box{<0,0,-0.203200><1.636700,0.035000,0.203200> rotate<0,0.000000,0> translate<14.731600,-1.535000,11.506300> }
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<14.773500,-1.535000,3.739600>}
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<15.940800,-1.535000,3.739600>}
box{<0,0,-0.203200><1.167300,0.035000,0.203200> rotate<0,0.000000,0> translate<14.773500,-1.535000,3.739600> }
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<14.793600,-1.535000,12.631600>}
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<15.031600,-1.535000,12.393600>}
box{<0,0,-0.203200><0.336583,0.035000,0.203200> rotate<0,44.997030,0> translate<14.793600,-1.535000,12.631600> }
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<14.793600,-1.535000,12.959400>}
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<14.793600,-1.535000,12.631600>}
box{<0,0,-0.203200><0.327800,0.035000,0.203200> rotate<0,-90.000000,0> translate<14.793600,-1.535000,12.631600> }
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<14.793600,-1.535000,14.568300>}
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<14.793600,-1.535000,14.133300>}
box{<0,0,-0.203200><0.435000,0.035000,0.203200> rotate<0,-90.000000,0> translate<14.793600,-1.535000,14.133300> }
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<14.793600,-1.535000,14.568300>}
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<15.031600,-1.535000,14.806300>}
box{<0,0,-0.203200><0.336583,0.035000,0.203200> rotate<0,-44.997030,0> translate<14.793600,-1.535000,14.568300> }
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<15.031600,-1.535000,12.393600>}
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<15.316600,-1.535000,12.393600>}
box{<0,0,-0.203200><0.285000,0.035000,0.203200> rotate<0,0.000000,0> translate<15.031600,-1.535000,12.393600> }
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<15.031600,-1.535000,14.806300>}
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<15.366600,-1.535000,14.806300>}
box{<0,0,-0.203200><0.335000,0.035000,0.203200> rotate<0,0.000000,0> translate<15.031600,-1.535000,14.806300> }
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<15.093600,-1.535000,16.031600>}
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<15.331600,-1.535000,15.793600>}
box{<0,0,-0.203200><0.336583,0.035000,0.203200> rotate<0,44.997030,0> translate<15.093600,-1.535000,16.031600> }
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<15.093600,-1.535000,17.968300>}
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<15.093600,-1.535000,16.031600>}
box{<0,0,-0.203200><1.936700,0.035000,0.203200> rotate<0,-90.000000,0> translate<15.093600,-1.535000,16.031600> }
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<15.093600,-1.535000,17.968300>}
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<15.331600,-1.535000,18.206300>}
box{<0,0,-0.203200><0.336583,0.035000,0.203200> rotate<0,-44.997030,0> translate<15.093600,-1.535000,17.968300> }
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<15.316600,-1.535000,12.229000>}
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<15.566600,-1.535000,11.979000>}
box{<0,0,-0.203200><0.353553,0.035000,0.203200> rotate<0,44.997030,0> translate<15.316600,-1.535000,12.229000> }
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<15.316600,-1.535000,12.393600>}
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<15.316600,-1.535000,12.229000>}
box{<0,0,-0.203200><0.164600,0.035000,0.203200> rotate<0,-90.000000,0> translate<15.316600,-1.535000,12.229000> }
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<15.331600,-1.535000,15.793600>}
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<15.366600,-1.535000,15.793600>}
box{<0,0,-0.203200><0.035000,0.035000,0.203200> rotate<0,0.000000,0> translate<15.331600,-1.535000,15.793600> }
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<15.331600,-1.535000,18.206300>}
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<16.239400,-1.535000,18.206300>}
box{<0,0,-0.203200><0.907800,0.035000,0.203200> rotate<0,0.000000,0> translate<15.331600,-1.535000,18.206300> }
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<15.334500,-1.535000,15.583700>}
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<15.366600,-1.535000,15.536400>}
box{<0,0,-0.203200><0.057164,0.035000,0.203200> rotate<0,55.833658,0> translate<15.334500,-1.535000,15.583700> }
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<15.334500,-1.535000,15.583700>}
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<15.366600,-1.535000,15.750700>}
box{<0,0,-0.203200><0.170057,0.035000,0.203200> rotate<0,-79.114341,0> translate<15.334500,-1.535000,15.583700> }
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<15.366600,-1.535000,8.779000>}
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<15.679000,-1.535000,8.466600>}
box{<0,0,-0.203200><0.441800,0.035000,0.203200> rotate<0,44.997030,0> translate<15.366600,-1.535000,8.779000> }
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<15.366600,-1.535000,9.093600>}
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<15.366600,-1.535000,8.779000>}
box{<0,0,-0.203200><0.314600,0.035000,0.203200> rotate<0,-90.000000,0> translate<15.366600,-1.535000,8.779000> }
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<15.366600,-1.535000,15.536400>}
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<15.366600,-1.535000,14.806300>}
box{<0,0,-0.203200><0.730100,0.035000,0.203200> rotate<0,-90.000000,0> translate<15.366600,-1.535000,14.806300> }
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<15.366600,-1.535000,15.793600>}
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<15.366600,-1.535000,15.750700>}
box{<0,0,-0.203200><0.042900,0.035000,0.203200> rotate<0,-90.000000,0> translate<15.366600,-1.535000,15.750700> }
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<15.379000,-1.535000,6.333300>}
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<15.466600,-1.535000,6.420900>}
box{<0,0,-0.203200><0.123885,0.035000,0.203200> rotate<0,-44.997030,0> translate<15.379000,-1.535000,6.333300> }
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<15.466600,-1.535000,6.520900>}
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<15.466600,-1.535000,6.420900>}
box{<0,0,-0.203200><0.100000,0.035000,0.203200> rotate<0,-90.000000,0> translate<15.466600,-1.535000,6.420900> }
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<15.466600,-1.535000,6.520900>}
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<15.779000,-1.535000,6.833300>}
box{<0,0,-0.203200><0.441800,0.035000,0.203200> rotate<0,-44.997030,0> translate<15.466600,-1.535000,6.520900> }
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<15.566600,-1.535000,11.979000>}
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<15.879000,-1.535000,11.666600>}
box{<0,0,-0.203200><0.441800,0.035000,0.203200> rotate<0,44.997030,0> translate<15.566600,-1.535000,11.979000> }
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<15.679000,-1.535000,8.466600>}
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<20.613600,-1.535000,8.466600>}
box{<0,0,-0.203200><4.934600,0.035000,0.203200> rotate<0,0.000000,0> translate<15.679000,-1.535000,8.466600> }
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<15.779000,-1.535000,6.833300>}
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<15.879000,-1.535000,6.933300>}
box{<0,0,-0.203200><0.141421,0.035000,0.203200> rotate<0,-44.997030,0> translate<15.779000,-1.535000,6.833300> }
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<15.879000,-1.535000,6.933300>}
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<16.320900,-1.535000,6.933300>}
box{<0,0,-0.203200><0.441900,0.035000,0.203200> rotate<0,0.000000,0> translate<15.879000,-1.535000,6.933300> }
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<15.879000,-1.535000,11.666600>}
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<17.266600,-1.535000,11.666600>}
box{<0,0,-0.203200><1.387600,0.035000,0.203200> rotate<0,0.000000,0> translate<15.879000,-1.535000,11.666600> }
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<15.940800,-1.535000,3.739600>}
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<15.995100,-1.535000,3.608600>}
box{<0,0,-0.203200><0.141808,0.035000,0.203200> rotate<0,67.481353,0> translate<15.940800,-1.535000,3.739600> }
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<15.940800,-1.535000,5.060300>}
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<15.995100,-1.535000,5.191300>}
box{<0,0,-0.203200><0.141808,0.035000,0.203200> rotate<0,-67.481353,0> translate<15.940800,-1.535000,5.060300> }
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<15.995100,-1.535000,3.608600>}
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<16.388000,-1.535000,3.215700>}
box{<0,0,-0.203200><0.555645,0.035000,0.203200> rotate<0,44.997030,0> translate<15.995100,-1.535000,3.608600> }
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<15.995100,-1.535000,5.191300>}
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<16.388000,-1.535000,5.584200>}
box{<0,0,-0.203200><0.555645,0.035000,0.203200> rotate<0,-44.997030,0> translate<15.995100,-1.535000,5.191300> }
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<16.020900,-1.535000,5.266600>}
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<16.333300,-1.535000,5.579000>}
box{<0,0,-0.203200><0.441800,0.035000,0.203200> rotate<0,-44.997030,0> translate<16.020900,-1.535000,5.266600> }
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<16.037500,-1.535000,5.283200>}
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<16.087000,-1.535000,5.283200>}
box{<0,0,-0.203200><0.049500,0.035000,0.203200> rotate<0,0.000000,0> translate<16.037500,-1.535000,5.283200> }
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<16.169000,-1.535000,18.276600>}
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<16.239400,-1.535000,18.206300>}
box{<0,0,-0.203200><0.099490,0.035000,0.203200> rotate<0,44.956311,0> translate<16.169000,-1.535000,18.276600> }
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<16.270100,-1.535000,21.833800>}
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<17.000000,-1.535000,22.136200>}
box{<0,0,-0.203200><0.790063,0.035000,0.203200> rotate<0,-22.502905,0> translate<16.270100,-1.535000,21.833800> }
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<16.320900,-1.535000,6.933300>}
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<19.179000,-1.535000,6.933300>}
box{<0,0,-0.203200><2.858100,0.035000,0.203200> rotate<0,0.000000,0> translate<16.320900,-1.535000,6.933300> }
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<16.333300,-1.535000,5.579000>}
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<16.433300,-1.535000,5.679000>}
box{<0,0,-0.203200><0.141421,0.035000,0.203200> rotate<0,-44.997030,0> translate<16.333300,-1.535000,5.579000> }
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<16.368300,-1.535000,11.506300>}
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<16.500000,-1.535000,11.374600>}
box{<0,0,-0.203200><0.186252,0.035000,0.203200> rotate<0,44.997030,0> translate<16.368300,-1.535000,11.506300> }
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<16.388000,-1.535000,3.215700>}
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<16.901400,-1.535000,3.003000>}
box{<0,0,-0.203200><0.555717,0.035000,0.203200> rotate<0,22.502588,0> translate<16.388000,-1.535000,3.215700> }
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<16.388000,-1.535000,5.584200>}
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<16.901400,-1.535000,5.796900>}
box{<0,0,-0.203200><0.555717,0.035000,0.203200> rotate<0,-22.502588,0> translate<16.388000,-1.535000,5.584200> }
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<16.433300,-1.535000,5.679000>}
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<16.433300,-1.535000,5.866600>}
box{<0,0,-0.203200><0.187600,0.035000,0.203200> rotate<0,90.000000,0> translate<16.433300,-1.535000,5.866600> }
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<16.433300,-1.535000,5.689600>}
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<16.642500,-1.535000,5.689600>}
box{<0,0,-0.203200><0.209200,0.035000,0.203200> rotate<0,0.000000,0> translate<16.433300,-1.535000,5.689600> }
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<16.433300,-1.535000,5.866600>}
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<19.620900,-1.535000,5.866600>}
box{<0,0,-0.203200><3.187600,0.035000,0.203200> rotate<0,0.000000,0> translate<16.433300,-1.535000,5.866600> }
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<16.433300,-1.535000,14.806300>}
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<16.433300,-1.535000,15.649100>}
box{<0,0,-0.203200><0.842800,0.035000,0.203200> rotate<0,90.000000,0> translate<16.433300,-1.535000,15.649100> }
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<16.433300,-1.535000,14.806300>}
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<16.668300,-1.535000,14.806300>}
box{<0,0,-0.203200><0.235000,0.035000,0.203200> rotate<0,0.000000,0> translate<16.433300,-1.535000,14.806300> }
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<16.433300,-1.535000,15.036800>}
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<19.982500,-1.535000,15.036800>}
box{<0,0,-0.203200><3.549200,0.035000,0.203200> rotate<0,0.000000,0> translate<16.433300,-1.535000,15.036800> }
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<16.433300,-1.535000,15.443200>}
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<20.588300,-1.535000,15.443200>}
box{<0,0,-0.203200><4.155000,0.035000,0.203200> rotate<0,0.000000,0> translate<16.433300,-1.535000,15.443200> }
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<16.433300,-1.535000,15.649100>}
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<16.461000,-1.535000,15.793600>}
box{<0,0,-0.203200><0.147131,0.035000,0.203200> rotate<0,-79.143078,0> translate<16.433300,-1.535000,15.649100> }
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<16.461000,-1.535000,15.793600>}
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<16.968300,-1.535000,15.793600>}
box{<0,0,-0.203200><0.507300,0.035000,0.203200> rotate<0,0.000000,0> translate<16.461000,-1.535000,15.793600> }
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<16.495400,-1.535000,11.379200>}
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<16.504600,-1.535000,11.379200>}
box{<0,0,-0.203200><0.009200,0.035000,0.203200> rotate<0,0.000000,0> translate<16.495400,-1.535000,11.379200> }
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<16.500000,-1.535000,11.374600>}
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<16.631600,-1.535000,11.506300>}
box{<0,0,-0.203200><0.186181,0.035000,0.203200> rotate<0,-45.018789,0> translate<16.500000,-1.535000,11.374600> }
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<16.539900,-1.535000,21.945600>}
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<16.994500,-1.535000,21.945600>}
box{<0,0,-0.203200><0.454600,0.035000,0.203200> rotate<0,0.000000,0> translate<16.539900,-1.535000,21.945600> }
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<16.631600,-1.535000,11.506300>}
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<17.266600,-1.535000,11.506300>}
box{<0,0,-0.203200><0.635000,0.035000,0.203200> rotate<0,0.000000,0> translate<16.631600,-1.535000,11.506300> }
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<16.668300,-1.535000,14.806300>}
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<16.800000,-1.535000,14.674600>}
box{<0,0,-0.203200><0.186252,0.035000,0.203200> rotate<0,44.997030,0> translate<16.668300,-1.535000,14.806300> }
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<16.800000,-1.535000,14.674600>}
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<16.931600,-1.535000,14.806300>}
box{<0,0,-0.203200><0.186181,0.035000,0.203200> rotate<0,-45.018789,0> translate<16.800000,-1.535000,14.674600> }
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<16.901400,-1.535000,3.003000>}
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<19.438500,-1.535000,3.003000>}
box{<0,0,-0.203200><2.537100,0.035000,0.203200> rotate<0,0.000000,0> translate<16.901400,-1.535000,3.003000> }
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<16.901400,-1.535000,5.796900>}
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<19.438500,-1.535000,5.796900>}
box{<0,0,-0.203200><2.537100,0.035000,0.203200> rotate<0,0.000000,0> translate<16.901400,-1.535000,5.796900> }
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<16.931600,-1.535000,14.806300>}
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<18.568300,-1.535000,14.806300>}
box{<0,0,-0.203200><1.636700,0.035000,0.203200> rotate<0,0.000000,0> translate<16.931600,-1.535000,14.806300> }
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<16.961800,-1.535000,21.533200>}
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<17.089400,-1.535000,21.225200>}
box{<0,0,-0.203200><0.333385,0.035000,0.203200> rotate<0,67.492017,0> translate<16.961800,-1.535000,21.533200> }
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<16.961800,-1.535000,21.866700>}
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<16.961800,-1.535000,21.533200>}
box{<0,0,-0.203200><0.333500,0.035000,0.203200> rotate<0,-90.000000,0> translate<16.961800,-1.535000,21.533200> }
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<16.961800,-1.535000,21.866700>}
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<17.089400,-1.535000,22.174700>}
box{<0,0,-0.203200><0.333385,0.035000,0.203200> rotate<0,-67.492017,0> translate<16.961800,-1.535000,21.866700> }
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<16.968300,-1.535000,15.793600>}
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<17.099900,-1.535000,15.925300>}
box{<0,0,-0.203200><0.186181,0.035000,0.203200> rotate<0,-45.018789,0> translate<16.968300,-1.535000,15.793600> }
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<17.000000,-1.535000,22.136200>}
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<17.266600,-1.535000,22.402900>}
box{<0,0,-0.203200><0.377100,0.035000,0.203200> rotate<0,-45.007773,0> translate<17.000000,-1.535000,22.136200> }
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<17.024200,-1.535000,15.849600>}
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<17.175600,-1.535000,15.849600>}
box{<0,0,-0.203200><0.151400,0.035000,0.203200> rotate<0,0.000000,0> translate<17.024200,-1.535000,15.849600> }
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<17.089400,-1.535000,21.225200>}
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<17.325200,-1.535000,20.989400>}
box{<0,0,-0.203200><0.333472,0.035000,0.203200> rotate<0,44.997030,0> translate<17.089400,-1.535000,21.225200> }
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<17.089400,-1.535000,22.174700>}
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<17.266600,-1.535000,22.351900>}
box{<0,0,-0.203200><0.250599,0.035000,0.203200> rotate<0,-44.997030,0> translate<17.089400,-1.535000,22.174700> }
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<17.099900,-1.535000,15.925300>}
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<17.231600,-1.535000,15.793600>}
box{<0,0,-0.203200><0.186252,0.035000,0.203200> rotate<0,44.997030,0> translate<17.099900,-1.535000,15.925300> }
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<17.215700,-1.535000,22.352000>}
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<17.266600,-1.535000,22.352000>}
box{<0,0,-0.203200><0.050900,0.035000,0.203200> rotate<0,0.000000,0> translate<17.215700,-1.535000,22.352000> }
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<17.231600,-1.535000,15.793600>}
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<18.868300,-1.535000,15.793600>}
box{<0,0,-0.203200><1.636700,0.035000,0.203200> rotate<0,0.000000,0> translate<17.231600,-1.535000,15.793600> }
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<17.248000,-1.535000,20.633300>}
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<17.325200,-1.535000,20.710500>}
box{<0,0,-0.203200><0.109177,0.035000,0.203200> rotate<0,-44.997030,0> translate<17.248000,-1.535000,20.633300> }
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<17.266600,-1.535000,11.666600>}
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<17.266600,-1.535000,11.506300>}
box{<0,0,-0.203200><0.160300,0.035000,0.203200> rotate<0,-90.000000,0> translate<17.266600,-1.535000,11.506300> }
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<17.266600,-1.535000,22.402900>}
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<17.266600,-1.535000,22.351900>}
box{<0,0,-0.203200><0.051000,0.035000,0.203200> rotate<0,-90.000000,0> translate<17.266600,-1.535000,22.351900> }
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<17.325200,-1.535000,20.710500>}
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<17.633200,-1.535000,20.838100>}
box{<0,0,-0.203200><0.333385,0.035000,0.203200> rotate<0,-22.502043,0> translate<17.325200,-1.535000,20.710500> }
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<17.325200,-1.535000,20.989400>}
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<17.633200,-1.535000,20.861800>}
box{<0,0,-0.203200><0.333385,0.035000,0.203200> rotate<0,22.502043,0> translate<17.325200,-1.535000,20.989400> }
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<17.633200,-1.535000,20.838100>}
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<17.966700,-1.535000,20.838100>}
box{<0,0,-0.203200><0.333500,0.035000,0.203200> rotate<0,0.000000,0> translate<17.633200,-1.535000,20.838100> }
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<17.633200,-1.535000,20.861800>}
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<17.966700,-1.535000,20.861800>}
box{<0,0,-0.203200><0.333500,0.035000,0.203200> rotate<0,0.000000,0> translate<17.633200,-1.535000,20.861800> }
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<17.966700,-1.535000,20.838100>}
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<18.274700,-1.535000,20.710500>}
box{<0,0,-0.203200><0.333385,0.035000,0.203200> rotate<0,22.502043,0> translate<17.966700,-1.535000,20.838100> }
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<17.966700,-1.535000,20.861800>}
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<18.274700,-1.535000,20.989400>}
box{<0,0,-0.203200><0.333385,0.035000,0.203200> rotate<0,-22.502043,0> translate<17.966700,-1.535000,20.861800> }
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<18.139100,-1.535000,19.233300>}
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<18.274700,-1.535000,19.289400>}
box{<0,0,-0.203200><0.146747,0.035000,0.203200> rotate<0,-22.474219,0> translate<18.139100,-1.535000,19.233300> }
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<18.139100,-1.535000,19.233300>}
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<18.279000,-1.535000,19.233300>}
box{<0,0,-0.203200><0.139900,0.035000,0.203200> rotate<0,0.000000,0> translate<18.139100,-1.535000,19.233300> }
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<18.160500,-1.535000,12.393600>}
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<18.333300,-1.535000,12.220900>}
box{<0,0,-0.203200><0.244305,0.035000,0.203200> rotate<0,44.980448,0> translate<18.160500,-1.535000,12.393600> }
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<18.160500,-1.535000,12.393600>}
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<18.568300,-1.535000,12.393600>}
box{<0,0,-0.203200><0.407800,0.035000,0.203200> rotate<0,0.000000,0> translate<18.160500,-1.535000,12.393600> }
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<18.236400,-1.535000,20.726400>}
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<31.328000,-1.535000,20.726400>}
box{<0,0,-0.203200><13.091600,0.035000,0.203200> rotate<0,0.000000,0> translate<18.236400,-1.535000,20.726400> }
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<18.274700,-1.535000,19.289400>}
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<18.510500,-1.535000,19.525200>}
box{<0,0,-0.203200><0.333472,0.035000,0.203200> rotate<0,-44.997030,0> translate<18.274700,-1.535000,19.289400> }
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<18.274700,-1.535000,20.710500>}
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<18.510500,-1.535000,20.474700>}
box{<0,0,-0.203200><0.333472,0.035000,0.203200> rotate<0,44.997030,0> translate<18.274700,-1.535000,20.710500> }
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<18.274700,-1.535000,20.989400>}
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<18.510500,-1.535000,21.225200>}
box{<0,0,-0.203200><0.333472,0.035000,0.203200> rotate<0,-44.997030,0> translate<18.274700,-1.535000,20.989400> }
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<18.279000,-1.535000,19.233300>}
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<18.379000,-1.535000,19.333300>}
box{<0,0,-0.203200><0.141421,0.035000,0.203200> rotate<0,-44.997030,0> translate<18.279000,-1.535000,19.233300> }
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<18.333300,-1.535000,11.441300>}
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<18.333300,-1.535000,12.220900>}
box{<0,0,-0.203200><0.779600,0.035000,0.203200> rotate<0,90.000000,0> translate<18.333300,-1.535000,12.220900> }
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<18.333300,-1.535000,11.441300>}
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<18.506300,-1.535000,11.268300>}
box{<0,0,-0.203200><0.244659,0.035000,0.203200> rotate<0,44.997030,0> translate<18.333300,-1.535000,11.441300> }
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<18.333300,-1.535000,11.785600>}
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<20.657200,-1.535000,11.785600>}
box{<0,0,-0.203200><2.323900,0.035000,0.203200> rotate<0,0.000000,0> translate<18.333300,-1.535000,11.785600> }
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<18.333300,-1.535000,12.192000>}
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<33.908500,-1.535000,12.192000>}
box{<0,0,-0.203200><15.575200,0.035000,0.203200> rotate<0,0.000000,0> translate<18.333300,-1.535000,12.192000> }
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<18.379000,-1.535000,19.333300>}
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<18.479000,-1.535000,19.433300>}
box{<0,0,-0.203200><0.141421,0.035000,0.203200> rotate<0,-44.997030,0> translate<18.379000,-1.535000,19.333300> }
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<18.395400,-1.535000,11.379200>}
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<20.612000,-1.535000,11.379200>}
box{<0,0,-0.203200><2.216600,0.035000,0.203200> rotate<0,0.000000,0> translate<18.395400,-1.535000,11.379200> }
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<18.418100,-1.535000,21.132800>}
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<31.679100,-1.535000,21.132800>}
box{<0,0,-0.203200><13.261000,0.035000,0.203200> rotate<0,0.000000,0> translate<18.418100,-1.535000,21.132800> }
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<18.479000,-1.535000,19.433300>}
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<18.579000,-1.535000,19.533300>}
box{<0,0,-0.203200><0.141421,0.035000,0.203200> rotate<0,-44.997030,0> translate<18.479000,-1.535000,19.433300> }
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<18.492500,-1.535000,19.507200>}
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<18.552900,-1.535000,19.507200>}
box{<0,0,-0.203200><0.060400,0.035000,0.203200> rotate<0,0.000000,0> translate<18.492500,-1.535000,19.507200> }
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<18.506300,-1.535000,9.533300>}
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<18.506300,-1.535000,11.268300>}
box{<0,0,-0.203200><1.735000,0.035000,0.203200> rotate<0,90.000000,0> translate<18.506300,-1.535000,11.268300> }
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<18.506300,-1.535000,9.533300>}
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<18.603300,-1.535000,9.533300>}
box{<0,0,-0.203200><0.097000,0.035000,0.203200> rotate<0,0.000000,0> translate<18.506300,-1.535000,9.533300> }
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<18.506300,-1.535000,9.753600>}
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<18.561800,-1.535000,9.753600>}
box{<0,0,-0.203200><0.055500,0.035000,0.203200> rotate<0,0.000000,0> translate<18.506300,-1.535000,9.753600> }
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<18.506300,-1.535000,10.160000>}
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<18.641900,-1.535000,10.160000>}
box{<0,0,-0.203200><0.135600,0.035000,0.203200> rotate<0,0.000000,0> translate<18.506300,-1.535000,10.160000> }
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<18.506300,-1.535000,10.566400>}
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<19.060200,-1.535000,10.566400>}
box{<0,0,-0.203200><0.553900,0.035000,0.203200> rotate<0,0.000000,0> translate<18.506300,-1.535000,10.566400> }
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<18.506300,-1.535000,10.972800>}
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<21.872500,-1.535000,10.972800>}
box{<0,0,-0.203200><3.366200,0.035000,0.203200> rotate<0,0.000000,0> translate<18.506300,-1.535000,10.972800> }
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<18.510500,-1.535000,19.525200>}
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<18.638100,-1.535000,19.833200>}
box{<0,0,-0.203200><0.333385,0.035000,0.203200> rotate<0,-67.492017,0> translate<18.510500,-1.535000,19.525200> }
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<18.510500,-1.535000,20.474700>}
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<18.638100,-1.535000,20.166700>}
box{<0,0,-0.203200><0.333385,0.035000,0.203200> rotate<0,67.492017,0> translate<18.510500,-1.535000,20.474700> }
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<18.510500,-1.535000,21.225200>}
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<18.638100,-1.535000,21.533200>}
box{<0,0,-0.203200><0.333385,0.035000,0.203200> rotate<0,-67.492017,0> translate<18.510500,-1.535000,21.225200> }
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<18.561800,-1.535000,9.633200>}
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<18.603300,-1.535000,9.533300>}
box{<0,0,-0.203200><0.108177,0.035000,0.203200> rotate<0,67.436797,0> translate<18.561800,-1.535000,9.633200> }
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<18.561800,-1.535000,9.966700>}
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<18.561800,-1.535000,9.633200>}
box{<0,0,-0.203200><0.333500,0.035000,0.203200> rotate<0,-90.000000,0> translate<18.561800,-1.535000,9.633200> }
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<18.561800,-1.535000,9.966700>}
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<18.689400,-1.535000,10.274700>}
box{<0,0,-0.203200><0.333385,0.035000,0.203200> rotate<0,-67.492017,0> translate<18.561800,-1.535000,9.966700> }
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<18.568300,-1.535000,12.393600>}
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<18.806300,-1.535000,12.631600>}
box{<0,0,-0.203200><0.336583,0.035000,0.203200> rotate<0,-44.997030,0> translate<18.568300,-1.535000,12.393600> }
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<18.568300,-1.535000,14.806300>}
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<18.641200,-1.535000,14.733300>}
box{<0,0,-0.203200><0.103167,0.035000,0.203200> rotate<0,45.036298,0> translate<18.568300,-1.535000,14.806300> }
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<18.574600,-1.535000,20.320000>}
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<31.261800,-1.535000,20.320000>}
box{<0,0,-0.203200><12.687200,0.035000,0.203200> rotate<0,0.000000,0> translate<18.574600,-1.535000,20.320000> }
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<18.579000,-1.535000,19.533300>}
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<19.020900,-1.535000,19.533300>}
box{<0,0,-0.203200><0.441900,0.035000,0.203200> rotate<0,0.000000,0> translate<18.579000,-1.535000,19.533300> }
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<18.638100,-1.535000,19.833200>}
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<18.638100,-1.535000,20.166700>}
box{<0,0,-0.203200><0.333500,0.035000,0.203200> rotate<0,90.000000,0> translate<18.638100,-1.535000,20.166700> }
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<18.638100,-1.535000,19.913600>}
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<20.688900,-1.535000,19.913600>}
box{<0,0,-0.203200><2.050800,0.035000,0.203200> rotate<0,0.000000,0> translate<18.638100,-1.535000,19.913600> }
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<18.638100,-1.535000,21.533200>}
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<18.638100,-1.535000,21.534200>}
box{<0,0,-0.203200><0.001000,0.035000,0.203200> rotate<0,90.000000,0> translate<18.638100,-1.535000,21.534200> }
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<18.638100,-1.535000,21.534200>}
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<18.832100,-1.535000,21.453800>}
box{<0,0,-0.203200><0.210000,0.035000,0.203200> rotate<0,22.509245,0> translate<18.638100,-1.535000,21.534200> }
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<18.641200,-1.535000,14.733300>}
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<19.079000,-1.535000,14.733300>}
box{<0,0,-0.203200><0.437800,0.035000,0.203200> rotate<0,0.000000,0> translate<18.641200,-1.535000,14.733300> }
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<18.689400,-1.535000,10.274700>}
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<18.925200,-1.535000,10.510500>}
box{<0,0,-0.203200><0.333472,0.035000,0.203200> rotate<0,-44.997030,0> translate<18.689400,-1.535000,10.274700> }
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<18.760500,-1.535000,18.206300>}
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<18.820900,-1.535000,18.266600>}
box{<0,0,-0.203200><0.085348,0.035000,0.203200> rotate<0,-44.949564,0> translate<18.760500,-1.535000,18.206300> }
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<18.760500,-1.535000,18.206300>}
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<18.868300,-1.535000,18.206300>}
box{<0,0,-0.203200><0.107800,0.035000,0.203200> rotate<0,0.000000,0> translate<18.760500,-1.535000,18.206300> }
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<18.773100,-1.535000,12.598400>}
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<19.544800,-1.535000,12.598400>}
box{<0,0,-0.203200><0.771700,0.035000,0.203200> rotate<0,0.000000,0> translate<18.773100,-1.535000,12.598400> }
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<18.806300,-1.535000,12.631600>}
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<18.806300,-1.535000,13.666600>}
box{<0,0,-0.203200><1.035000,0.035000,0.203200> rotate<0,90.000000,0> translate<18.806300,-1.535000,13.666600> }
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<18.806300,-1.535000,13.004800>}
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<19.056400,-1.535000,13.004800>}
box{<0,0,-0.203200><0.250100,0.035000,0.203200> rotate<0,0.000000,0> translate<18.806300,-1.535000,13.004800> }
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<18.806300,-1.535000,13.411200>}
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<18.961800,-1.535000,13.411200>}
box{<0,0,-0.203200><0.155500,0.035000,0.203200> rotate<0,0.000000,0> translate<18.806300,-1.535000,13.411200> }
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<18.806300,-1.535000,13.666600>}
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<19.003200,-1.535000,13.666600>}
box{<0,0,-0.203200><0.196900,0.035000,0.203200> rotate<0,0.000000,0> translate<18.806300,-1.535000,13.666600> }
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<18.820900,-1.535000,18.266600>}
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<18.920900,-1.535000,18.266600>}
box{<0,0,-0.203200><0.100000,0.035000,0.203200> rotate<0,0.000000,0> translate<18.820900,-1.535000,18.266600> }
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<18.832100,-1.535000,21.453800>}
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<19.367800,-1.535000,21.453800>}
box{<0,0,-0.203200><0.535700,0.035000,0.203200> rotate<0,0.000000,0> translate<18.832100,-1.535000,21.453800> }
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<18.868300,-1.535000,15.793600>}
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<19.106300,-1.535000,16.031600>}
box{<0,0,-0.203200><0.336583,0.035000,0.203200> rotate<0,-44.997030,0> translate<18.868300,-1.535000,15.793600> }
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<18.868300,-1.535000,18.206300>}
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<19.106300,-1.535000,17.968300>}
box{<0,0,-0.203200><0.336583,0.035000,0.203200> rotate<0,44.997030,0> translate<18.868300,-1.535000,18.206300> }
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<18.920900,-1.535000,18.266600>}
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<19.120900,-1.535000,18.466600>}
box{<0,0,-0.203200><0.282843,0.035000,0.203200> rotate<0,-44.997030,0> translate<18.920900,-1.535000,18.266600> }
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<18.924300,-1.535000,15.849600>}
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<20.512000,-1.535000,15.849600>}
box{<0,0,-0.203200><1.587700,0.035000,0.203200> rotate<0,0.000000,0> translate<18.924300,-1.535000,15.849600> }
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<18.925200,-1.535000,10.510500>}
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<19.233200,-1.535000,10.638100>}
box{<0,0,-0.203200><0.333385,0.035000,0.203200> rotate<0,-22.502043,0> translate<18.925200,-1.535000,10.510500> }
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<18.942300,-1.535000,18.288000>}
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<20.613600,-1.535000,18.288000>}
box{<0,0,-0.203200><1.671300,0.035000,0.203200> rotate<0,0.000000,0> translate<18.942300,-1.535000,18.288000> }
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<18.961800,-1.535000,13.233200>}
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<19.089400,-1.535000,12.925200>}
box{<0,0,-0.203200><0.333385,0.035000,0.203200> rotate<0,67.492017,0> translate<18.961800,-1.535000,13.233200> }
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<18.961800,-1.535000,13.566700>}
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<18.961800,-1.535000,13.233200>}
box{<0,0,-0.203200><0.333500,0.035000,0.203200> rotate<0,-90.000000,0> translate<18.961800,-1.535000,13.233200> }
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<18.961800,-1.535000,13.566700>}
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<19.003200,-1.535000,13.666600>}
box{<0,0,-0.203200><0.108139,0.035000,0.203200> rotate<0,-67.485724,0> translate<18.961800,-1.535000,13.566700> }
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<19.020900,-1.535000,19.533300>}
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<20.613600,-1.535000,19.533300>}
box{<0,0,-0.203200><1.592700,0.035000,0.203200> rotate<0,0.000000,0> translate<19.020900,-1.535000,19.533300> }
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<19.079000,-1.535000,14.733300>}
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<19.179000,-1.535000,14.833300>}
box{<0,0,-0.203200><0.141421,0.035000,0.203200> rotate<0,-44.997030,0> translate<19.079000,-1.535000,14.733300> }
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<19.089400,-1.535000,12.925200>}
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<19.325200,-1.535000,12.689400>}
box{<0,0,-0.203200><0.333472,0.035000,0.203200> rotate<0,44.997030,0> translate<19.089400,-1.535000,12.925200> }
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<19.106300,-1.535000,16.031600>}
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<19.106300,-1.535000,17.968300>}
box{<0,0,-0.203200><1.936700,0.035000,0.203200> rotate<0,90.000000,0> translate<19.106300,-1.535000,17.968300> }
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<19.106300,-1.535000,16.256000>}
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<20.512000,-1.535000,16.256000>}
box{<0,0,-0.203200><1.405700,0.035000,0.203200> rotate<0,0.000000,0> translate<19.106300,-1.535000,16.256000> }
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<19.106300,-1.535000,16.662400>}
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<20.512000,-1.535000,16.662400>}
box{<0,0,-0.203200><1.405700,0.035000,0.203200> rotate<0,0.000000,0> translate<19.106300,-1.535000,16.662400> }
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<19.106300,-1.535000,17.068800>}
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<20.512000,-1.535000,17.068800>}
box{<0,0,-0.203200><1.405700,0.035000,0.203200> rotate<0,0.000000,0> translate<19.106300,-1.535000,17.068800> }
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<19.106300,-1.535000,17.475200>}
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<20.646800,-1.535000,17.475200>}
box{<0,0,-0.203200><1.540500,0.035000,0.203200> rotate<0,0.000000,0> translate<19.106300,-1.535000,17.475200> }
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<19.106300,-1.535000,17.881600>}
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<20.833600,-1.535000,17.881600>}
box{<0,0,-0.203200><1.727300,0.035000,0.203200> rotate<0,0.000000,0> translate<19.106300,-1.535000,17.881600> }
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<19.120900,-1.535000,18.466600>}
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<20.613600,-1.535000,18.466600>}
box{<0,0,-0.203200><1.492700,0.035000,0.203200> rotate<0,0.000000,0> translate<19.120900,-1.535000,18.466600> }
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<19.179000,-1.535000,6.933300>}
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<19.279000,-1.535000,7.033300>}
box{<0,0,-0.203200><0.141421,0.035000,0.203200> rotate<0,-44.997030,0> translate<19.179000,-1.535000,6.933300> }
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<19.179000,-1.535000,14.833300>}
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<19.679100,-1.535000,14.833300>}
box{<0,0,-0.203200><0.500100,0.035000,0.203200> rotate<0,0.000000,0> translate<19.179000,-1.535000,14.833300> }
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<19.233200,-1.535000,10.638100>}
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<19.566700,-1.535000,10.638100>}
box{<0,0,-0.203200><0.333500,0.035000,0.203200> rotate<0,0.000000,0> translate<19.233200,-1.535000,10.638100> }
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<19.279000,-1.535000,7.033300>}
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<19.466600,-1.535000,7.033300>}
box{<0,0,-0.203200><0.187600,0.035000,0.203200> rotate<0,0.000000,0> translate<19.279000,-1.535000,7.033300> }
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<19.325200,-1.535000,12.689400>}
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<19.633200,-1.535000,12.561800>}
box{<0,0,-0.203200><0.333385,0.035000,0.203200> rotate<0,22.502043,0> translate<19.325200,-1.535000,12.689400> }
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<19.367800,-1.535000,21.453800>}
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<19.609200,-1.535000,21.553800>}
box{<0,0,-0.203200><0.261293,0.035000,0.203200> rotate<0,-22.500307,0> translate<19.367800,-1.535000,21.453800> }
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<19.438500,-1.535000,3.003000>}
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<19.951900,-1.535000,3.215700>}
box{<0,0,-0.203200><0.555717,0.035000,0.203200> rotate<0,-22.502588,0> translate<19.438500,-1.535000,3.003000> }
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<19.438500,-1.535000,5.796900>}
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<19.951900,-1.535000,5.584200>}
box{<0,0,-0.203200><0.555717,0.035000,0.203200> rotate<0,22.502588,0> translate<19.438500,-1.535000,5.796900> }
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<19.466600,-1.535000,7.220900>}
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<19.466600,-1.535000,7.033300>}
box{<0,0,-0.203200><0.187600,0.035000,0.203200> rotate<0,-90.000000,0> translate<19.466600,-1.535000,7.033300> }
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<19.466600,-1.535000,7.220900>}
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<19.779000,-1.535000,7.533300>}
box{<0,0,-0.203200><0.441800,0.035000,0.203200> rotate<0,-44.997030,0> translate<19.466600,-1.535000,7.220900> }
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<19.566700,-1.535000,10.638100>}
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<19.874700,-1.535000,10.510500>}
box{<0,0,-0.203200><0.333385,0.035000,0.203200> rotate<0,22.502043,0> translate<19.566700,-1.535000,10.638100> }
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<19.573900,-1.535000,21.539200>}
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<34.788800,-1.535000,21.539200>}
box{<0,0,-0.203200><15.214900,0.035000,0.203200> rotate<0,0.000000,0> translate<19.573900,-1.535000,21.539200> }
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<19.609200,-1.535000,21.553800>}
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<27.767800,-1.535000,21.553800>}
box{<0,0,-0.203200><8.158600,0.035000,0.203200> rotate<0,0.000000,0> translate<19.609200,-1.535000,21.553800> }
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<19.620900,-1.535000,5.866600>}
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<19.720900,-1.535000,5.966600>}
box{<0,0,-0.203200><0.141421,0.035000,0.203200> rotate<0,-44.997030,0> translate<19.620900,-1.535000,5.866600> }
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<19.633200,-1.535000,12.561800>}
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<19.966700,-1.535000,12.561800>}
box{<0,0,-0.203200><0.333500,0.035000,0.203200> rotate<0,0.000000,0> translate<19.633200,-1.535000,12.561800> }
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<19.679100,-1.535000,14.833300>}
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<19.879000,-1.535000,15.033300>}
box{<0,0,-0.203200><0.282772,0.035000,0.203200> rotate<0,-45.011357,0> translate<19.679100,-1.535000,14.833300> }
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<19.697500,-1.535000,5.689600>}
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<20.512000,-1.535000,5.689600>}
box{<0,0,-0.203200><0.814500,0.035000,0.203200> rotate<0,0.000000,0> translate<19.697500,-1.535000,5.689600> }
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<19.720900,-1.535000,5.966600>}
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<19.920900,-1.535000,5.966600>}
box{<0,0,-0.203200><0.200000,0.035000,0.203200> rotate<0,0.000000,0> translate<19.720900,-1.535000,5.966600> }
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<19.739800,-1.535000,10.566400>}
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<20.612000,-1.535000,10.566400>}
box{<0,0,-0.203200><0.872200,0.035000,0.203200> rotate<0,0.000000,0> translate<19.739800,-1.535000,10.566400> }
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<19.779000,-1.535000,7.533300>}
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<19.879000,-1.535000,7.633300>}
box{<0,0,-0.203200><0.141421,0.035000,0.203200> rotate<0,-44.997030,0> translate<19.779000,-1.535000,7.533300> }
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<19.874700,-1.535000,10.510500>}
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<20.051800,-1.535000,10.333300>}
box{<0,0,-0.203200><0.250528,0.035000,0.203200> rotate<0,45.013201,0> translate<19.874700,-1.535000,10.510500> }
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<19.879000,-1.535000,7.633300>}
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<20.320900,-1.535000,7.633300>}
box{<0,0,-0.203200><0.441900,0.035000,0.203200> rotate<0,0.000000,0> translate<19.879000,-1.535000,7.633300> }
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<19.879000,-1.535000,15.033300>}
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<19.979000,-1.535000,15.033300>}
box{<0,0,-0.203200><0.100000,0.035000,0.203200> rotate<0,0.000000,0> translate<19.879000,-1.535000,15.033300> }
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<19.920900,-1.535000,5.966600>}
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<20.020900,-1.535000,6.066600>}
box{<0,0,-0.203200><0.141421,0.035000,0.203200> rotate<0,-44.997030,0> translate<19.920900,-1.535000,5.966600> }
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<19.951900,-1.535000,3.215700>}
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<20.344800,-1.535000,3.608600>}
box{<0,0,-0.203200><0.555645,0.035000,0.203200> rotate<0,-44.997030,0> translate<19.951900,-1.535000,3.215700> }
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<19.951900,-1.535000,5.584200>}
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<20.344800,-1.535000,5.191300>}
box{<0,0,-0.203200><0.555645,0.035000,0.203200> rotate<0,44.997030,0> translate<19.951900,-1.535000,5.584200> }
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<19.966700,-1.535000,12.561800>}
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<20.274700,-1.535000,12.689400>}
box{<0,0,-0.203200><0.333385,0.035000,0.203200> rotate<0,-22.502043,0> translate<19.966700,-1.535000,12.561800> }
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<19.979000,-1.535000,15.033300>}
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<20.079000,-1.535000,15.133300>}
box{<0,0,-0.203200><0.141421,0.035000,0.203200> rotate<0,-44.997030,0> translate<19.979000,-1.535000,15.033300> }
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<19.987400,-1.535000,3.251200>}
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<39.212500,-1.535000,3.251200>}
box{<0,0,-0.203200><19.225100,0.035000,0.203200> rotate<0,0.000000,0> translate<19.987400,-1.535000,3.251200> }
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<20.020900,-1.535000,6.066600>}
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<20.120900,-1.535000,6.066600>}
box{<0,0,-0.203200><0.100000,0.035000,0.203200> rotate<0,0.000000,0> translate<20.020900,-1.535000,6.066600> }
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<20.051800,-1.535000,10.333300>}
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<20.612000,-1.535000,10.333300>}
box{<0,0,-0.203200><0.560200,0.035000,0.203200> rotate<0,0.000000,0> translate<20.051800,-1.535000,10.333300> }
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<20.055000,-1.535000,12.598400>}
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<20.713600,-1.535000,12.598400>}
box{<0,0,-0.203200><0.658600,0.035000,0.203200> rotate<0,0.000000,0> translate<20.055000,-1.535000,12.598400> }
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<20.079000,-1.535000,15.133300>}
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<23.579000,-1.535000,15.133300>}
box{<0,0,-0.203200><3.500000,0.035000,0.203200> rotate<0,0.000000,0> translate<20.079000,-1.535000,15.133300> }
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<20.120900,-1.535000,6.066600>}
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<20.433300,-1.535000,6.379000>}
box{<0,0,-0.203200><0.441800,0.035000,0.203200> rotate<0,-44.997030,0> translate<20.120900,-1.535000,6.066600> }
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<20.150300,-1.535000,6.096000>}
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<20.567600,-1.535000,6.096000>}
box{<0,0,-0.203200><0.417300,0.035000,0.203200> rotate<0,0.000000,0> translate<20.150300,-1.535000,6.096000> }
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<20.252900,-1.535000,5.283200>}
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<20.512000,-1.535000,5.283200>}
box{<0,0,-0.203200><0.259100,0.035000,0.203200> rotate<0,0.000000,0> translate<20.252900,-1.535000,5.283200> }
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<20.274700,-1.535000,12.689400>}
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<20.451900,-1.535000,12.866600>}
box{<0,0,-0.203200><0.250599,0.035000,0.203200> rotate<0,-44.997030,0> translate<20.274700,-1.535000,12.689400> }
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<20.320900,-1.535000,7.633300>}
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<20.613600,-1.535000,7.633300>}
box{<0,0,-0.203200><0.292700,0.035000,0.203200> rotate<0,0.000000,0> translate<20.320900,-1.535000,7.633300> }
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<20.344800,-1.535000,3.608600>}
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<20.557500,-1.535000,4.122000>}
box{<0,0,-0.203200><0.555717,0.035000,0.203200> rotate<0,-67.491473,0> translate<20.344800,-1.535000,3.608600> }
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<20.344800,-1.535000,5.191300>}
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<20.512000,-1.535000,4.787600>}
box{<0,0,-0.203200><0.436955,0.035000,0.203200> rotate<0,67.497728,0> translate<20.344800,-1.535000,5.191300> }
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<20.365100,-1.535000,3.657600>}
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<38.834700,-1.535000,3.657600>}
box{<0,0,-0.203200><18.469600,0.035000,0.203200> rotate<0,0.000000,0> translate<20.365100,-1.535000,3.657600> }
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<20.433300,-1.535000,6.379000>}
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<20.533300,-1.535000,6.479000>}
box{<0,0,-0.203200><0.141421,0.035000,0.203200> rotate<0,-44.997030,0> translate<20.433300,-1.535000,6.379000> }
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<20.451900,-1.535000,12.866600>}
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<20.713600,-1.535000,12.866600>}
box{<0,0,-0.203200><0.261700,0.035000,0.203200> rotate<0,0.000000,0> translate<20.451900,-1.535000,12.866600> }
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<20.475100,-1.535000,4.876800>}
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<20.512000,-1.535000,4.876800>}
box{<0,0,-0.203200><0.036900,0.035000,0.203200> rotate<0,0.000000,0> translate<20.475100,-1.535000,4.876800> }
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<20.512000,-1.535000,4.983000>}
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<20.512000,-1.535000,4.787600>}
box{<0,0,-0.203200><0.195400,0.035000,0.203200> rotate<0,-90.000000,0> translate<20.512000,-1.535000,4.787600> }
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<20.512000,-1.535000,4.983000>}
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<21.772500,-1.535000,4.983000>}
box{<0,0,-0.203200><1.260500,0.035000,0.203200> rotate<0,0.000000,0> translate<20.512000,-1.535000,4.983000> }
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<20.512000,-1.535000,5.276800>}
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<21.772500,-1.535000,5.276800>}
box{<0,0,-0.203200><1.260500,0.035000,0.203200> rotate<0,0.000000,0> translate<20.512000,-1.535000,5.276800> }
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<20.512000,-1.535000,6.040400>}
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<20.512000,-1.535000,5.276800>}
box{<0,0,-0.203200><0.763600,0.035000,0.203200> rotate<0,-90.000000,0> translate<20.512000,-1.535000,5.276800> }
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<20.512000,-1.535000,6.040400>}
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<20.809500,-1.535000,6.337900>}
box{<0,0,-0.203200><0.420729,0.035000,0.203200> rotate<0,-44.997030,0> translate<20.512000,-1.535000,6.040400> }
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<20.512000,-1.535000,15.519500>}
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<20.809500,-1.535000,15.222000>}
box{<0,0,-0.203200><0.420729,0.035000,0.203200> rotate<0,44.997030,0> translate<20.512000,-1.535000,15.519500> }
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<20.512000,-1.535000,16.283000>}
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<20.512000,-1.535000,15.519500>}
box{<0,0,-0.203200><0.763500,0.035000,0.203200> rotate<0,-90.000000,0> translate<20.512000,-1.535000,15.519500> }
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<20.512000,-1.535000,16.283000>}
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<21.772500,-1.535000,16.283000>}
box{<0,0,-0.203200><1.260500,0.035000,0.203200> rotate<0,0.000000,0> translate<20.512000,-1.535000,16.283000> }
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<20.512000,-1.535000,16.576800>}
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<21.772500,-1.535000,16.576800>}
box{<0,0,-0.203200><1.260500,0.035000,0.203200> rotate<0,0.000000,0> translate<20.512000,-1.535000,16.576800> }
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<20.512000,-1.535000,17.340400>}
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<20.512000,-1.535000,16.576800>}
box{<0,0,-0.203200><0.763600,0.035000,0.203200> rotate<0,-90.000000,0> translate<20.512000,-1.535000,16.576800> }
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<20.512000,-1.535000,17.340400>}
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<20.809500,-1.535000,17.637900>}
box{<0,0,-0.203200><0.420729,0.035000,0.203200> rotate<0,-44.997030,0> translate<20.512000,-1.535000,17.340400> }
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<20.533300,-1.535000,6.479000>}
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<20.533300,-1.535000,6.566600>}
box{<0,0,-0.203200><0.087600,0.035000,0.203200> rotate<0,90.000000,0> translate<20.533300,-1.535000,6.566600> }
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<20.533300,-1.535000,6.502400>}
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<40.093700,-1.535000,6.502400>}
box{<0,0,-0.203200><19.560400,0.035000,0.203200> rotate<0,0.000000,0> translate<20.533300,-1.535000,6.502400> }
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<20.533300,-1.535000,6.566600>}
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<20.848700,-1.535000,6.566600>}
box{<0,0,-0.203200><0.315400,0.035000,0.203200> rotate<0,0.000000,0> translate<20.533300,-1.535000,6.566600> }
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<20.533400,-1.535000,4.064000>}
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<20.667500,-1.535000,4.064000>}
box{<0,0,-0.203200><0.134100,0.035000,0.203200> rotate<0,0.000000,0> translate<20.533400,-1.535000,4.064000> }
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<20.557500,-1.535000,4.122000>}
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<20.557500,-1.535000,4.174100>}
box{<0,0,-0.203200><0.052100,0.035000,0.203200> rotate<0,90.000000,0> translate<20.557500,-1.535000,4.174100> }
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<20.557500,-1.535000,4.174100>}
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<20.809500,-1.535000,3.922000>}
box{<0,0,-0.203200><0.356453,0.035000,0.203200> rotate<0,45.008395,0> translate<20.557500,-1.535000,4.174100> }
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<20.612000,-1.535000,10.683000>}
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<20.612000,-1.535000,10.333300>}
box{<0,0,-0.203200><0.349700,0.035000,0.203200> rotate<0,-90.000000,0> translate<20.612000,-1.535000,10.333300> }
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<20.612000,-1.535000,10.683000>}
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<21.872500,-1.535000,10.683000>}
box{<0,0,-0.203200><1.260500,0.035000,0.203200> rotate<0,0.000000,0> translate<20.612000,-1.535000,10.683000> }
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<20.612000,-1.535000,10.976800>}
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<21.872500,-1.535000,10.976800>}
box{<0,0,-0.203200><1.260500,0.035000,0.203200> rotate<0,0.000000,0> translate<20.612000,-1.535000,10.976800> }
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<20.612000,-1.535000,11.740400>}
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<20.612000,-1.535000,10.976800>}
box{<0,0,-0.203200><0.763600,0.035000,0.203200> rotate<0,-90.000000,0> translate<20.612000,-1.535000,10.976800> }
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<20.612000,-1.535000,11.740400>}
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<20.909500,-1.535000,12.037900>}
box{<0,0,-0.203200><0.420729,0.035000,0.203200> rotate<0,-44.997030,0> translate<20.612000,-1.535000,11.740400> }
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<20.613600,-1.535000,8.466600>}
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<20.613600,-1.535000,7.633300>}
box{<0,0,-0.203200><0.833300,0.035000,0.203200> rotate<0,-90.000000,0> translate<20.613600,-1.535000,7.633300> }
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<20.613600,-1.535000,18.101600>}
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<20.851600,-1.535000,17.863600>}
box{<0,0,-0.203200><0.336583,0.035000,0.203200> rotate<0,44.997030,0> translate<20.613600,-1.535000,18.101600> }
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<20.613600,-1.535000,18.466600>}
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<20.613600,-1.535000,18.101600>}
box{<0,0,-0.203200><0.365000,0.035000,0.203200> rotate<0,-90.000000,0> translate<20.613600,-1.535000,18.101600> }
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<20.613600,-1.535000,19.838300>}
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<20.613600,-1.535000,19.533300>}
box{<0,0,-0.203200><0.305000,0.035000,0.203200> rotate<0,-90.000000,0> translate<20.613600,-1.535000,19.533300> }
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<20.613600,-1.535000,19.838300>}
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<20.851600,-1.535000,20.076300>}
box{<0,0,-0.203200><0.336583,0.035000,0.203200> rotate<0,-44.997030,0> translate<20.613600,-1.535000,19.838300> }
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<20.713600,-1.535000,12.501600>}
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<20.951600,-1.535000,12.263600>}
box{<0,0,-0.203200><0.336583,0.035000,0.203200> rotate<0,44.997030,0> translate<20.713600,-1.535000,12.501600> }
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<20.713600,-1.535000,12.866600>}
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<20.713600,-1.535000,12.501600>}
box{<0,0,-0.203200><0.365000,0.035000,0.203200> rotate<0,-90.000000,0> translate<20.713600,-1.535000,12.501600> }
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<20.809500,-1.535000,3.922000>}
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<21.773000,-1.535000,3.922000>}
box{<0,0,-0.203200><0.963500,0.035000,0.203200> rotate<0,0.000000,0> translate<20.809500,-1.535000,3.922000> }
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<20.809500,-1.535000,6.337900>}
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<21.773000,-1.535000,6.337900>}
box{<0,0,-0.203200><0.963500,0.035000,0.203200> rotate<0,0.000000,0> translate<20.809500,-1.535000,6.337900> }
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<20.809500,-1.535000,15.222000>}
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<21.773000,-1.535000,15.222000>}
box{<0,0,-0.203200><0.963500,0.035000,0.203200> rotate<0,0.000000,0> translate<20.809500,-1.535000,15.222000> }
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<20.809500,-1.535000,17.637900>}
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<21.773000,-1.535000,17.637900>}
box{<0,0,-0.203200><0.963500,0.035000,0.203200> rotate<0,0.000000,0> translate<20.809500,-1.535000,17.637900> }
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<20.848700,-1.535000,6.566600>}
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<20.851600,-1.535000,6.563600>}
box{<0,0,-0.203200><0.004173,0.035000,0.203200> rotate<0,45.967988,0> translate<20.848700,-1.535000,6.566600> }
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<20.851600,-1.535000,6.563600>}
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<21.068700,-1.535000,6.563600>}
box{<0,0,-0.203200><0.217100,0.035000,0.203200> rotate<0,0.000000,0> translate<20.851600,-1.535000,6.563600> }
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<20.851600,-1.535000,17.863600>}
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<22.988300,-1.535000,17.863600>}
box{<0,0,-0.203200><2.136700,0.035000,0.203200> rotate<0,0.000000,0> translate<20.851600,-1.535000,17.863600> }
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<20.851600,-1.535000,20.076300>}
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<22.988300,-1.535000,20.076300>}
box{<0,0,-0.203200><2.136700,0.035000,0.203200> rotate<0,0.000000,0> translate<20.851600,-1.535000,20.076300> }
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<20.909500,-1.535000,12.037900>}
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<21.873000,-1.535000,12.037900>}
box{<0,0,-0.203200><0.963500,0.035000,0.203200> rotate<0,0.000000,0> translate<20.909500,-1.535000,12.037900> }
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<20.951600,-1.535000,12.263600>}
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<23.088300,-1.535000,12.263600>}
box{<0,0,-0.203200><2.136700,0.035000,0.203200> rotate<0,0.000000,0> translate<20.951600,-1.535000,12.263600> }
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<21.068700,-1.535000,6.563600>}
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<21.223000,-1.535000,6.535900>}
box{<0,0,-0.203200><0.156767,0.035000,0.203200> rotate<0,10.176684,0> translate<21.068700,-1.535000,6.563600> }
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<21.223000,-1.535000,6.535900>}
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<21.262900,-1.535000,6.563600>}
box{<0,0,-0.203200><0.048573,0.035000,0.203200> rotate<0,-34.767463,0> translate<21.223000,-1.535000,6.535900> }
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<21.262900,-1.535000,6.563600>}
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<22.988300,-1.535000,6.563600>}
box{<0,0,-0.203200><1.725400,0.035000,0.203200> rotate<0,0.000000,0> translate<21.262900,-1.535000,6.563600> }
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<21.772500,-1.535000,5.276800>}
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<21.772500,-1.535000,4.983000>}
box{<0,0,-0.203200><0.293800,0.035000,0.203200> rotate<0,-90.000000,0> translate<21.772500,-1.535000,4.983000> }
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<21.772500,-1.535000,16.576800>}
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<21.772500,-1.535000,16.283000>}
box{<0,0,-0.203200><0.293800,0.035000,0.203200> rotate<0,-90.000000,0> translate<21.772500,-1.535000,16.283000> }
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<21.773000,-1.535000,3.922000>}
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<21.773000,-1.535000,4.982500>}
box{<0,0,-0.203200><1.060500,0.035000,0.203200> rotate<0,90.000000,0> translate<21.773000,-1.535000,4.982500> }
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<21.773000,-1.535000,4.064000>}
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<22.066800,-1.535000,4.064000>}
box{<0,0,-0.203200><0.293800,0.035000,0.203200> rotate<0,0.000000,0> translate<21.773000,-1.535000,4.064000> }
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<21.773000,-1.535000,4.470400>}
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<22.066800,-1.535000,4.470400>}
box{<0,0,-0.203200><0.293800,0.035000,0.203200> rotate<0,0.000000,0> translate<21.773000,-1.535000,4.470400> }
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<21.773000,-1.535000,4.876800>}
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<22.066800,-1.535000,4.876800>}
box{<0,0,-0.203200><0.293800,0.035000,0.203200> rotate<0,0.000000,0> translate<21.773000,-1.535000,4.876800> }
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<21.773000,-1.535000,4.982500>}
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<22.066800,-1.535000,4.982500>}
box{<0,0,-0.203200><0.293800,0.035000,0.203200> rotate<0,0.000000,0> translate<21.773000,-1.535000,4.982500> }
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<21.773000,-1.535000,5.277400>}
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<21.773000,-1.535000,6.337900>}
box{<0,0,-0.203200><1.060500,0.035000,0.203200> rotate<0,90.000000,0> translate<21.773000,-1.535000,6.337900> }
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<21.773000,-1.535000,5.277400>}
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<22.066800,-1.535000,5.277400>}
box{<0,0,-0.203200><0.293800,0.035000,0.203200> rotate<0,0.000000,0> translate<21.773000,-1.535000,5.277400> }
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<21.773000,-1.535000,5.283200>}
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<22.066800,-1.535000,5.283200>}
box{<0,0,-0.203200><0.293800,0.035000,0.203200> rotate<0,0.000000,0> translate<21.773000,-1.535000,5.283200> }
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<21.773000,-1.535000,5.689600>}
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<22.066800,-1.535000,5.689600>}
box{<0,0,-0.203200><0.293800,0.035000,0.203200> rotate<0,0.000000,0> translate<21.773000,-1.535000,5.689600> }
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<21.773000,-1.535000,6.096000>}
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<22.066800,-1.535000,6.096000>}
box{<0,0,-0.203200><0.293800,0.035000,0.203200> rotate<0,0.000000,0> translate<21.773000,-1.535000,6.096000> }
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<21.773000,-1.535000,15.222000>}
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<21.773000,-1.535000,16.282500>}
box{<0,0,-0.203200><1.060500,0.035000,0.203200> rotate<0,90.000000,0> translate<21.773000,-1.535000,16.282500> }
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<21.773000,-1.535000,15.443200>}
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<22.066800,-1.535000,15.443200>}
box{<0,0,-0.203200><0.293800,0.035000,0.203200> rotate<0,0.000000,0> translate<21.773000,-1.535000,15.443200> }
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<21.773000,-1.535000,15.849600>}
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<22.066800,-1.535000,15.849600>}
box{<0,0,-0.203200><0.293800,0.035000,0.203200> rotate<0,0.000000,0> translate<21.773000,-1.535000,15.849600> }
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<21.773000,-1.535000,16.256000>}
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<22.066800,-1.535000,16.256000>}
box{<0,0,-0.203200><0.293800,0.035000,0.203200> rotate<0,0.000000,0> translate<21.773000,-1.535000,16.256000> }
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<21.773000,-1.535000,16.282500>}
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<22.066800,-1.535000,16.282500>}
box{<0,0,-0.203200><0.293800,0.035000,0.203200> rotate<0,0.000000,0> translate<21.773000,-1.535000,16.282500> }
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<21.773000,-1.535000,16.577400>}
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<21.773000,-1.535000,17.637900>}
box{<0,0,-0.203200><1.060500,0.035000,0.203200> rotate<0,90.000000,0> translate<21.773000,-1.535000,17.637900> }
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<21.773000,-1.535000,16.577400>}
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<22.066800,-1.535000,16.577400>}
box{<0,0,-0.203200><0.293800,0.035000,0.203200> rotate<0,0.000000,0> translate<21.773000,-1.535000,16.577400> }
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<21.773000,-1.535000,16.662400>}
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<22.066800,-1.535000,16.662400>}
box{<0,0,-0.203200><0.293800,0.035000,0.203200> rotate<0,0.000000,0> translate<21.773000,-1.535000,16.662400> }
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<21.773000,-1.535000,17.068800>}
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<22.066800,-1.535000,17.068800>}
box{<0,0,-0.203200><0.293800,0.035000,0.203200> rotate<0,0.000000,0> translate<21.773000,-1.535000,17.068800> }
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<21.773000,-1.535000,17.475200>}
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<22.066800,-1.535000,17.475200>}
box{<0,0,-0.203200><0.293800,0.035000,0.203200> rotate<0,0.000000,0> translate<21.773000,-1.535000,17.475200> }
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<21.872500,-1.535000,10.976800>}
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<21.872500,-1.535000,10.683000>}
box{<0,0,-0.203200><0.293800,0.035000,0.203200> rotate<0,-90.000000,0> translate<21.872500,-1.535000,10.683000> }
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<21.873000,-1.535000,10.977400>}
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<21.873000,-1.535000,12.037900>}
box{<0,0,-0.203200><1.060500,0.035000,0.203200> rotate<0,90.000000,0> translate<21.873000,-1.535000,12.037900> }
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<21.873000,-1.535000,10.977400>}
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<22.166800,-1.535000,10.977400>}
box{<0,0,-0.203200><0.293800,0.035000,0.203200> rotate<0,0.000000,0> translate<21.873000,-1.535000,10.977400> }
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<21.873000,-1.535000,11.379200>}
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<22.166800,-1.535000,11.379200>}
box{<0,0,-0.203200><0.293800,0.035000,0.203200> rotate<0,0.000000,0> translate<21.873000,-1.535000,11.379200> }
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<21.873000,-1.535000,11.785600>}
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<22.166800,-1.535000,11.785600>}
box{<0,0,-0.203200><0.293800,0.035000,0.203200> rotate<0,0.000000,0> translate<21.873000,-1.535000,11.785600> }
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<22.066800,-1.535000,3.922000>}
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<23.030400,-1.535000,3.922000>}
box{<0,0,-0.203200><0.963600,0.035000,0.203200> rotate<0,0.000000,0> translate<22.066800,-1.535000,3.922000> }
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<22.066800,-1.535000,4.982500>}
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<22.066800,-1.535000,3.922000>}
box{<0,0,-0.203200><1.060500,0.035000,0.203200> rotate<0,-90.000000,0> translate<22.066800,-1.535000,3.922000> }
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<22.066800,-1.535000,6.337900>}
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<22.066800,-1.535000,5.277400>}
box{<0,0,-0.203200><1.060500,0.035000,0.203200> rotate<0,-90.000000,0> translate<22.066800,-1.535000,5.277400> }
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<22.066800,-1.535000,6.337900>}
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<23.030400,-1.535000,6.337900>}
box{<0,0,-0.203200><0.963600,0.035000,0.203200> rotate<0,0.000000,0> translate<22.066800,-1.535000,6.337900> }
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<22.066800,-1.535000,15.222000>}
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<23.030400,-1.535000,15.222000>}
box{<0,0,-0.203200><0.963600,0.035000,0.203200> rotate<0,0.000000,0> translate<22.066800,-1.535000,15.222000> }
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<22.066800,-1.535000,16.282500>}
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<22.066800,-1.535000,15.222000>}
box{<0,0,-0.203200><1.060500,0.035000,0.203200> rotate<0,-90.000000,0> translate<22.066800,-1.535000,15.222000> }
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<22.066800,-1.535000,17.637900>}
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<22.066800,-1.535000,16.577400>}
box{<0,0,-0.203200><1.060500,0.035000,0.203200> rotate<0,-90.000000,0> translate<22.066800,-1.535000,16.577400> }
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<22.066800,-1.535000,17.637900>}
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<23.030400,-1.535000,17.637900>}
box{<0,0,-0.203200><0.963600,0.035000,0.203200> rotate<0,0.000000,0> translate<22.066800,-1.535000,17.637900> }
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<22.067400,-1.535000,4.983000>}
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<22.067400,-1.535000,5.276800>}
box{<0,0,-0.203200><0.293800,0.035000,0.203200> rotate<0,90.000000,0> translate<22.067400,-1.535000,5.276800> }
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<22.067400,-1.535000,4.983000>}
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<23.327900,-1.535000,4.983000>}
box{<0,0,-0.203200><1.260500,0.035000,0.203200> rotate<0,0.000000,0> translate<22.067400,-1.535000,4.983000> }
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<22.067400,-1.535000,5.276800>}
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<23.327900,-1.535000,5.276800>}
box{<0,0,-0.203200><1.260500,0.035000,0.203200> rotate<0,0.000000,0> translate<22.067400,-1.535000,5.276800> }
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<22.067400,-1.535000,16.283000>}
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<22.067400,-1.535000,16.576800>}
box{<0,0,-0.203200><0.293800,0.035000,0.203200> rotate<0,90.000000,0> translate<22.067400,-1.535000,16.576800> }
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<22.067400,-1.535000,16.283000>}
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<23.327900,-1.535000,16.283000>}
box{<0,0,-0.203200><1.260500,0.035000,0.203200> rotate<0,0.000000,0> translate<22.067400,-1.535000,16.283000> }
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<22.067400,-1.535000,16.576800>}
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<23.327900,-1.535000,16.576800>}
box{<0,0,-0.203200><1.260500,0.035000,0.203200> rotate<0,0.000000,0> translate<22.067400,-1.535000,16.576800> }
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<22.166800,-1.535000,12.037900>}
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<22.166800,-1.535000,10.977400>}
box{<0,0,-0.203200><1.060500,0.035000,0.203200> rotate<0,-90.000000,0> translate<22.166800,-1.535000,10.977400> }
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<22.166800,-1.535000,12.037900>}
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<23.130400,-1.535000,12.037900>}
box{<0,0,-0.203200><0.963600,0.035000,0.203200> rotate<0,0.000000,0> translate<22.166800,-1.535000,12.037900> }
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<22.167400,-1.535000,10.683000>}
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<22.167400,-1.535000,10.976800>}
box{<0,0,-0.203200><0.293800,0.035000,0.203200> rotate<0,90.000000,0> translate<22.167400,-1.535000,10.976800> }
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<22.167400,-1.535000,10.683000>}
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<23.427900,-1.535000,10.683000>}
box{<0,0,-0.203200><1.260500,0.035000,0.203200> rotate<0,0.000000,0> translate<22.167400,-1.535000,10.683000> }
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<22.167400,-1.535000,10.972800>}
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<29.718500,-1.535000,10.972800>}
box{<0,0,-0.203200><7.551100,0.035000,0.203200> rotate<0,0.000000,0> translate<22.167400,-1.535000,10.972800> }
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<22.167400,-1.535000,10.976800>}
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<23.427900,-1.535000,10.976800>}
box{<0,0,-0.203200><1.260500,0.035000,0.203200> rotate<0,0.000000,0> translate<22.167400,-1.535000,10.976800> }
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<22.988300,-1.535000,6.563600>}
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<23.226300,-1.535000,6.801600>}
box{<0,0,-0.203200><0.336583,0.035000,0.203200> rotate<0,-44.997030,0> translate<22.988300,-1.535000,6.563600> }
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<22.988300,-1.535000,17.863600>}
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<23.226300,-1.535000,18.101600>}
box{<0,0,-0.203200><0.336583,0.035000,0.203200> rotate<0,-44.997030,0> translate<22.988300,-1.535000,17.863600> }
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<22.988300,-1.535000,20.076300>}
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<23.226300,-1.535000,19.838300>}
box{<0,0,-0.203200><0.336583,0.035000,0.203200> rotate<0,44.997030,0> translate<22.988300,-1.535000,20.076300> }
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<23.006300,-1.535000,17.881600>}
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<30.993600,-1.535000,17.881600>}
box{<0,0,-0.203200><7.987300,0.035000,0.203200> rotate<0,0.000000,0> translate<23.006300,-1.535000,17.881600> }
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<23.030400,-1.535000,3.922000>}
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<23.327900,-1.535000,4.219500>}
box{<0,0,-0.203200><0.420729,0.035000,0.203200> rotate<0,-44.997030,0> translate<23.030400,-1.535000,3.922000> }
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<23.030400,-1.535000,6.337900>}
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<23.327900,-1.535000,6.040400>}
box{<0,0,-0.203200><0.420729,0.035000,0.203200> rotate<0,44.997030,0> translate<23.030400,-1.535000,6.337900> }
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<23.030400,-1.535000,15.222000>}
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<23.327900,-1.535000,15.519500>}
box{<0,0,-0.203200><0.420729,0.035000,0.203200> rotate<0,-44.997030,0> translate<23.030400,-1.535000,15.222000> }
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<23.030400,-1.535000,17.637900>}
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<23.327900,-1.535000,17.340400>}
box{<0,0,-0.203200><0.420729,0.035000,0.203200> rotate<0,44.997030,0> translate<23.030400,-1.535000,17.637900> }
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<23.088300,-1.535000,12.263600>}
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<23.326300,-1.535000,12.501600>}
box{<0,0,-0.203200><0.336583,0.035000,0.203200> rotate<0,-44.997030,0> translate<23.088300,-1.535000,12.263600> }
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<23.130400,-1.535000,12.037900>}
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<23.427900,-1.535000,11.740400>}
box{<0,0,-0.203200><0.420729,0.035000,0.203200> rotate<0,44.997030,0> translate<23.130400,-1.535000,12.037900> }
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<23.151000,-1.535000,19.913600>}
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<30.848900,-1.535000,19.913600>}
box{<0,0,-0.203200><7.697900,0.035000,0.203200> rotate<0,0.000000,0> translate<23.151000,-1.535000,19.913600> }
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<23.172400,-1.535000,4.064000>}
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<30.971200,-1.535000,4.064000>}
box{<0,0,-0.203200><7.798800,0.035000,0.203200> rotate<0,0.000000,0> translate<23.172400,-1.535000,4.064000> }
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<23.193100,-1.535000,17.475200>}
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<30.806800,-1.535000,17.475200>}
box{<0,0,-0.203200><7.613700,0.035000,0.203200> rotate<0,0.000000,0> translate<23.193100,-1.535000,17.475200> }
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<23.226300,-1.535000,6.801600>}
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<23.226300,-1.535000,8.466600>}
box{<0,0,-0.203200><1.665000,0.035000,0.203200> rotate<0,90.000000,0> translate<23.226300,-1.535000,8.466600> }
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<23.226300,-1.535000,6.908800>}
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<30.773600,-1.535000,6.908800>}
box{<0,0,-0.203200><7.547300,0.035000,0.203200> rotate<0,0.000000,0> translate<23.226300,-1.535000,6.908800> }
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<23.226300,-1.535000,7.315200>}
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<24.230400,-1.535000,7.315200>}
box{<0,0,-0.203200><1.004100,0.035000,0.203200> rotate<0,0.000000,0> translate<23.226300,-1.535000,7.315200> }
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<23.226300,-1.535000,7.721600>}
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<23.524000,-1.535000,7.721600>}
box{<0,0,-0.203200><0.297700,0.035000,0.203200> rotate<0,0.000000,0> translate<23.226300,-1.535000,7.721600> }
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<23.226300,-1.535000,8.128000>}
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<23.266600,-1.535000,8.128000>}
box{<0,0,-0.203200><0.040300,0.035000,0.203200> rotate<0,0.000000,0> translate<23.226300,-1.535000,8.128000> }
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<23.226300,-1.535000,8.466600>}
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<23.266600,-1.535000,8.466600>}
box{<0,0,-0.203200><0.040300,0.035000,0.203200> rotate<0,0.000000,0> translate<23.226300,-1.535000,8.466600> }
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<23.226300,-1.535000,18.101600>}
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<23.226300,-1.535000,19.838300>}
box{<0,0,-0.203200><1.736700,0.035000,0.203200> rotate<0,90.000000,0> translate<23.226300,-1.535000,19.838300> }
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<23.226300,-1.535000,18.288000>}
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<30.773600,-1.535000,18.288000>}
box{<0,0,-0.203200><7.547300,0.035000,0.203200> rotate<0,0.000000,0> translate<23.226300,-1.535000,18.288000> }
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<23.226300,-1.535000,18.694400>}
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<30.773600,-1.535000,18.694400>}
box{<0,0,-0.203200><7.547300,0.035000,0.203200> rotate<0,0.000000,0> translate<23.226300,-1.535000,18.694400> }
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<23.226300,-1.535000,19.100800>}
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<30.773600,-1.535000,19.100800>}
box{<0,0,-0.203200><7.547300,0.035000,0.203200> rotate<0,0.000000,0> translate<23.226300,-1.535000,19.100800> }
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<23.226300,-1.535000,19.507200>}
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<30.773600,-1.535000,19.507200>}
box{<0,0,-0.203200><7.547300,0.035000,0.203200> rotate<0,0.000000,0> translate<23.226300,-1.535000,19.507200> }
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<23.251600,-1.535000,15.443200>}
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<30.748300,-1.535000,15.443200>}
box{<0,0,-0.203200><7.496700,0.035000,0.203200> rotate<0,0.000000,0> translate<23.251600,-1.535000,15.443200> }
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<23.266600,-1.535000,7.979000>}
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<23.366600,-1.535000,7.879000>}
box{<0,0,-0.203200><0.141421,0.035000,0.203200> rotate<0,44.997030,0> translate<23.266600,-1.535000,7.979000> }
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<23.266600,-1.535000,8.420900>}
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<23.266600,-1.535000,7.979000>}
box{<0,0,-0.203200><0.441900,0.035000,0.203200> rotate<0,-90.000000,0> translate<23.266600,-1.535000,7.979000> }
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<23.266600,-1.535000,8.466600>}
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<23.266600,-1.535000,8.420900>}
box{<0,0,-0.203200><0.045700,0.035000,0.203200> rotate<0,-90.000000,0> translate<23.266600,-1.535000,8.420900> }
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<23.272300,-1.535000,6.096000>}
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<30.871300,-1.535000,6.096000>}
box{<0,0,-0.203200><7.599000,0.035000,0.203200> rotate<0,0.000000,0> translate<23.272300,-1.535000,6.096000> }
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<23.326300,-1.535000,12.501600>}
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<23.326300,-1.535000,14.066600>}
box{<0,0,-0.203200><1.565000,0.035000,0.203200> rotate<0,90.000000,0> translate<23.326300,-1.535000,14.066600> }
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<23.326300,-1.535000,12.598400>}
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<30.873600,-1.535000,12.598400>}
box{<0,0,-0.203200><7.547300,0.035000,0.203200> rotate<0,0.000000,0> translate<23.326300,-1.535000,12.598400> }
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<23.326300,-1.535000,13.004800>}
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<24.340800,-1.535000,13.004800>}
box{<0,0,-0.203200><1.014500,0.035000,0.203200> rotate<0,0.000000,0> translate<23.326300,-1.535000,13.004800> }
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<23.326300,-1.535000,13.411200>}
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<23.634400,-1.535000,13.411200>}
box{<0,0,-0.203200><0.308100,0.035000,0.203200> rotate<0,0.000000,0> translate<23.326300,-1.535000,13.411200> }
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<23.326300,-1.535000,13.817600>}
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<23.366600,-1.535000,13.817600>}
box{<0,0,-0.203200><0.040300,0.035000,0.203200> rotate<0,0.000000,0> translate<23.326300,-1.535000,13.817600> }
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<23.326300,-1.535000,14.066600>}
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<23.366600,-1.535000,14.066600>}
box{<0,0,-0.203200><0.040300,0.035000,0.203200> rotate<0,0.000000,0> translate<23.326300,-1.535000,14.066600> }
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<23.327900,-1.535000,4.219500>}
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<23.327900,-1.535000,4.983000>}
box{<0,0,-0.203200><0.763500,0.035000,0.203200> rotate<0,90.000000,0> translate<23.327900,-1.535000,4.983000> }
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<23.327900,-1.535000,4.470400>}
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<24.544200,-1.535000,4.470400>}
box{<0,0,-0.203200><1.216300,0.035000,0.203200> rotate<0,0.000000,0> translate<23.327900,-1.535000,4.470400> }
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<23.327900,-1.535000,4.876800>}
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<24.285100,-1.535000,4.876800>}
box{<0,0,-0.203200><0.957200,0.035000,0.203200> rotate<0,0.000000,0> translate<23.327900,-1.535000,4.876800> }
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<23.327900,-1.535000,5.276800>}
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<23.327900,-1.535000,6.040400>}
box{<0,0,-0.203200><0.763600,0.035000,0.203200> rotate<0,90.000000,0> translate<23.327900,-1.535000,6.040400> }
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<23.327900,-1.535000,5.283200>}
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<24.268700,-1.535000,5.283200>}
box{<0,0,-0.203200><0.940800,0.035000,0.203200> rotate<0,0.000000,0> translate<23.327900,-1.535000,5.283200> }
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<23.327900,-1.535000,5.689600>}
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<24.504300,-1.535000,5.689600>}
box{<0,0,-0.203200><1.176400,0.035000,0.203200> rotate<0,0.000000,0> translate<23.327900,-1.535000,5.689600> }
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<23.327900,-1.535000,15.519500>}
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<23.327900,-1.535000,16.283000>}
box{<0,0,-0.203200><0.763500,0.035000,0.203200> rotate<0,90.000000,0> translate<23.327900,-1.535000,16.283000> }
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<23.327900,-1.535000,15.849600>}
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<30.672000,-1.535000,15.849600>}
box{<0,0,-0.203200><7.344100,0.035000,0.203200> rotate<0,0.000000,0> translate<23.327900,-1.535000,15.849600> }
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<23.327900,-1.535000,16.256000>}
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<30.672000,-1.535000,16.256000>}
box{<0,0,-0.203200><7.344100,0.035000,0.203200> rotate<0,0.000000,0> translate<23.327900,-1.535000,16.256000> }
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<23.327900,-1.535000,16.576800>}
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<23.327900,-1.535000,17.340400>}
box{<0,0,-0.203200><0.763600,0.035000,0.203200> rotate<0,90.000000,0> translate<23.327900,-1.535000,17.340400> }
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<23.327900,-1.535000,16.662400>}
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<30.672000,-1.535000,16.662400>}
box{<0,0,-0.203200><7.344100,0.035000,0.203200> rotate<0,0.000000,0> translate<23.327900,-1.535000,16.662400> }
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<23.327900,-1.535000,17.068800>}
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<30.672000,-1.535000,17.068800>}
box{<0,0,-0.203200><7.344100,0.035000,0.203200> rotate<0,0.000000,0> translate<23.327900,-1.535000,17.068800> }
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<23.366600,-1.535000,7.879000>}
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<23.679000,-1.535000,7.566600>}
box{<0,0,-0.203200><0.441800,0.035000,0.203200> rotate<0,44.997030,0> translate<23.366600,-1.535000,7.879000> }
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<23.366600,-1.535000,13.679000>}
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<23.466600,-1.535000,13.579000>}
box{<0,0,-0.203200><0.141421,0.035000,0.203200> rotate<0,44.997030,0> translate<23.366600,-1.535000,13.679000> }
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<23.366600,-1.535000,14.066600>}
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<23.366600,-1.535000,13.679000>}
box{<0,0,-0.203200><0.387600,0.035000,0.203200> rotate<0,-90.000000,0> translate<23.366600,-1.535000,13.679000> }
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<23.382700,-1.535000,11.785600>}
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<30.960900,-1.535000,11.785600>}
box{<0,0,-0.203200><7.578200,0.035000,0.203200> rotate<0,0.000000,0> translate<23.382700,-1.535000,11.785600> }
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<23.427900,-1.535000,10.233300>}
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<23.427900,-1.535000,10.683000>}
box{<0,0,-0.203200><0.449700,0.035000,0.203200> rotate<0,90.000000,0> translate<23.427900,-1.535000,10.683000> }
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<23.427900,-1.535000,10.233300>}
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<23.779000,-1.535000,10.233300>}
box{<0,0,-0.203200><0.351100,0.035000,0.203200> rotate<0,0.000000,0> translate<23.427900,-1.535000,10.233300> }
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<23.427900,-1.535000,10.566400>}
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<29.666600,-1.535000,10.566400>}
box{<0,0,-0.203200><6.238700,0.035000,0.203200> rotate<0,0.000000,0> translate<23.427900,-1.535000,10.566400> }
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<23.427900,-1.535000,10.976800>}
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<23.427900,-1.535000,11.740400>}
box{<0,0,-0.203200><0.763600,0.035000,0.203200> rotate<0,90.000000,0> translate<23.427900,-1.535000,11.740400> }
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<23.427900,-1.535000,11.379200>}
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<30.873600,-1.535000,11.379200>}
box{<0,0,-0.203200><7.445700,0.035000,0.203200> rotate<0,0.000000,0> translate<23.427900,-1.535000,11.379200> }
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<23.466600,-1.535000,13.579000>}
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<23.779000,-1.535000,13.266600>}
box{<0,0,-0.203200><0.441800,0.035000,0.203200> rotate<0,44.997030,0> translate<23.466600,-1.535000,13.579000> }
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<23.579000,-1.535000,15.133300>}
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<24.020900,-1.535000,15.133300>}
box{<0,0,-0.203200><0.441900,0.035000,0.203200> rotate<0,0.000000,0> translate<23.579000,-1.535000,15.133300> }
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<23.679000,-1.535000,7.566600>}
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<23.779000,-1.535000,7.566600>}
box{<0,0,-0.203200><0.100000,0.035000,0.203200> rotate<0,0.000000,0> translate<23.679000,-1.535000,7.566600> }
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<23.779000,-1.535000,7.566600>}
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<23.879000,-1.535000,7.466600>}
box{<0,0,-0.203200><0.141421,0.035000,0.203200> rotate<0,44.997030,0> translate<23.779000,-1.535000,7.566600> }
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<23.779000,-1.535000,10.233300>}
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<23.879000,-1.535000,10.333300>}
box{<0,0,-0.203200><0.141421,0.035000,0.203200> rotate<0,-44.997030,0> translate<23.779000,-1.535000,10.233300> }
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<23.779000,-1.535000,13.266600>}
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<23.879000,-1.535000,13.266600>}
box{<0,0,-0.203200><0.100000,0.035000,0.203200> rotate<0,0.000000,0> translate<23.779000,-1.535000,13.266600> }
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<23.879000,-1.535000,7.466600>}
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<23.979000,-1.535000,7.466600>}
box{<0,0,-0.203200><0.100000,0.035000,0.203200> rotate<0,0.000000,0> translate<23.879000,-1.535000,7.466600> }
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<23.879000,-1.535000,10.333300>}
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<24.320900,-1.535000,10.333300>}
box{<0,0,-0.203200><0.441900,0.035000,0.203200> rotate<0,0.000000,0> translate<23.879000,-1.535000,10.333300> }
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<23.879000,-1.535000,13.266600>}
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<23.979000,-1.535000,13.166600>}
box{<0,0,-0.203200><0.141421,0.035000,0.203200> rotate<0,44.997030,0> translate<23.879000,-1.535000,13.266600> }
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<23.979000,-1.535000,7.466600>}
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<24.079000,-1.535000,7.366600>}
box{<0,0,-0.203200><0.141421,0.035000,0.203200> rotate<0,44.997030,0> translate<23.979000,-1.535000,7.466600> }
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<23.979000,-1.535000,13.166600>}
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<24.079000,-1.535000,13.166600>}
box{<0,0,-0.203200><0.100000,0.035000,0.203200> rotate<0,0.000000,0> translate<23.979000,-1.535000,13.166600> }
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<24.020900,-1.535000,15.133300>}
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<24.120900,-1.535000,15.033300>}
box{<0,0,-0.203200><0.141421,0.035000,0.203200> rotate<0,44.997030,0> translate<24.020900,-1.535000,15.133300> }
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<24.079000,-1.535000,7.366600>}
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<24.179000,-1.535000,7.366600>}
box{<0,0,-0.203200><0.100000,0.035000,0.203200> rotate<0,0.000000,0> translate<24.079000,-1.535000,7.366600> }
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<24.079000,-1.535000,13.166600>}
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<24.179000,-1.535000,13.066600>}
box{<0,0,-0.203200><0.141421,0.035000,0.203200> rotate<0,44.997030,0> translate<24.079000,-1.535000,13.166600> }
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<24.117400,-1.535000,15.036800>}
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<38.453800,-1.535000,15.036800>}
box{<0,0,-0.203200><14.336400,0.035000,0.203200> rotate<0,0.000000,0> translate<24.117400,-1.535000,15.036800> }
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<24.120900,-1.535000,15.033300>}
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<24.433300,-1.535000,14.720900>}
box{<0,0,-0.203200><0.441800,0.035000,0.203200> rotate<0,44.997030,0> translate<24.120900,-1.535000,15.033300> }
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<24.179000,-1.535000,7.366600>}
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<24.279000,-1.535000,7.266600>}
box{<0,0,-0.203200><0.141421,0.035000,0.203200> rotate<0,44.997030,0> translate<24.179000,-1.535000,7.366600> }
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<24.179000,-1.535000,13.066600>}
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<24.279000,-1.535000,13.066600>}
box{<0,0,-0.203200><0.100000,0.035000,0.203200> rotate<0,0.000000,0> translate<24.179000,-1.535000,13.066600> }
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<24.233300,-1.535000,9.120900>}
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<24.233300,-1.535000,9.179000>}
box{<0,0,-0.203200><0.058100,0.035000,0.203200> rotate<0,90.000000,0> translate<24.233300,-1.535000,9.179000> }
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<24.233300,-1.535000,9.120900>}
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<24.333300,-1.535000,9.020900>}
box{<0,0,-0.203200><0.141421,0.035000,0.203200> rotate<0,44.997030,0> translate<24.233300,-1.535000,9.120900> }
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<24.233300,-1.535000,9.179000>}
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<24.320900,-1.535000,9.266600>}
box{<0,0,-0.203200><0.123885,0.035000,0.203200> rotate<0,-44.997030,0> translate<24.233300,-1.535000,9.179000> }
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<24.261800,-1.535000,4.933200>}
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<24.389400,-1.535000,4.625200>}
box{<0,0,-0.203200><0.333385,0.035000,0.203200> rotate<0,67.492017,0> translate<24.261800,-1.535000,4.933200> }
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<24.261800,-1.535000,5.266700>}
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<24.261800,-1.535000,4.933200>}
box{<0,0,-0.203200><0.333500,0.035000,0.203200> rotate<0,-90.000000,0> translate<24.261800,-1.535000,4.933200> }
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<24.261800,-1.535000,5.266700>}
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<24.389400,-1.535000,5.574700>}
box{<0,0,-0.203200><0.333385,0.035000,0.203200> rotate<0,-67.492017,0> translate<24.261800,-1.535000,5.266700> }
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<24.279000,-1.535000,7.266600>}
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<24.379000,-1.535000,7.266600>}
box{<0,0,-0.203200><0.100000,0.035000,0.203200> rotate<0,0.000000,0> translate<24.279000,-1.535000,7.266600> }
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<24.279000,-1.535000,13.066600>}
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<24.379000,-1.535000,12.966600>}
box{<0,0,-0.203200><0.141421,0.035000,0.203200> rotate<0,44.997030,0> translate<24.279000,-1.535000,13.066600> }
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<24.320900,-1.535000,9.266600>}
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<29.720900,-1.535000,9.266600>}
box{<0,0,-0.203200><5.400000,0.035000,0.203200> rotate<0,0.000000,0> translate<24.320900,-1.535000,9.266600> }
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<24.320900,-1.535000,10.333300>}
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<29.279000,-1.535000,10.333300>}
box{<0,0,-0.203200><4.958100,0.035000,0.203200> rotate<0,0.000000,0> translate<24.320900,-1.535000,10.333300> }
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<24.333300,-1.535000,8.533300>}
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<24.333300,-1.535000,9.020900>}
box{<0,0,-0.203200><0.487600,0.035000,0.203200> rotate<0,90.000000,0> translate<24.333300,-1.535000,9.020900> }
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<24.333300,-1.535000,8.533300>}
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<24.420900,-1.535000,8.533300>}
box{<0,0,-0.203200><0.087600,0.035000,0.203200> rotate<0,0.000000,0> translate<24.333300,-1.535000,8.533300> }
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<24.333300,-1.535000,8.534400>}
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<30.773600,-1.535000,8.534400>}
box{<0,0,-0.203200><6.440300,0.035000,0.203200> rotate<0,0.000000,0> translate<24.333300,-1.535000,8.534400> }
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<24.333300,-1.535000,8.940800>}
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<36.588800,-1.535000,8.940800>}
box{<0,0,-0.203200><12.255500,0.035000,0.203200> rotate<0,0.000000,0> translate<24.333300,-1.535000,8.940800> }
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<24.379000,-1.535000,7.266600>}
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<24.479000,-1.535000,7.166600>}
box{<0,0,-0.203200><0.141421,0.035000,0.203200> rotate<0,44.997030,0> translate<24.379000,-1.535000,7.266600> }
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<24.379000,-1.535000,12.966600>}
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<24.479000,-1.535000,12.966600>}
box{<0,0,-0.203200><0.100000,0.035000,0.203200> rotate<0,0.000000,0> translate<24.379000,-1.535000,12.966600> }
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<24.389400,-1.535000,4.625200>}
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<24.625200,-1.535000,4.389400>}
box{<0,0,-0.203200><0.333472,0.035000,0.203200> rotate<0,44.997030,0> translate<24.389400,-1.535000,4.625200> }
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<24.389400,-1.535000,5.574700>}
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<24.625200,-1.535000,5.810500>}
box{<0,0,-0.203200><0.333472,0.035000,0.203200> rotate<0,-44.997030,0> translate<24.389400,-1.535000,5.574700> }
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<24.420900,-1.535000,8.533300>}
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<24.520900,-1.535000,8.433300>}
box{<0,0,-0.203200><0.141421,0.035000,0.203200> rotate<0,44.997030,0> translate<24.420900,-1.535000,8.533300> }
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<24.433300,-1.535000,14.233300>}
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<24.433300,-1.535000,14.720900>}
box{<0,0,-0.203200><0.487600,0.035000,0.203200> rotate<0,90.000000,0> translate<24.433300,-1.535000,14.720900> }
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<24.433300,-1.535000,14.233300>}
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<24.520900,-1.535000,14.233300>}
box{<0,0,-0.203200><0.087600,0.035000,0.203200> rotate<0,0.000000,0> translate<24.433300,-1.535000,14.233300> }
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<24.433300,-1.535000,14.630400>}
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<38.453800,-1.535000,14.630400>}
box{<0,0,-0.203200><14.020500,0.035000,0.203200> rotate<0,0.000000,0> translate<24.433300,-1.535000,14.630400> }
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<24.479000,-1.535000,7.166600>}
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<30.773600,-1.535000,7.166600>}
box{<0,0,-0.203200><6.294600,0.035000,0.203200> rotate<0,0.000000,0> translate<24.479000,-1.535000,7.166600> }
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<24.479000,-1.535000,12.966600>}
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<24.579000,-1.535000,12.866600>}
box{<0,0,-0.203200><0.141421,0.035000,0.203200> rotate<0,44.997030,0> translate<24.479000,-1.535000,12.966600> }
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<24.520900,-1.535000,8.433300>}
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<24.620900,-1.535000,8.433300>}
box{<0,0,-0.203200><0.100000,0.035000,0.203200> rotate<0,0.000000,0> translate<24.520900,-1.535000,8.433300> }
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<24.520900,-1.535000,14.233300>}
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<24.620900,-1.535000,14.133300>}
box{<0,0,-0.203200><0.141421,0.035000,0.203200> rotate<0,44.997030,0> translate<24.520900,-1.535000,14.233300> }
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<24.530200,-1.535000,14.224000>}
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<30.873600,-1.535000,14.224000>}
box{<0,0,-0.203200><6.343400,0.035000,0.203200> rotate<0,0.000000,0> translate<24.530200,-1.535000,14.224000> }
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<24.579000,-1.535000,12.866600>}
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<30.873600,-1.535000,12.866600>}
box{<0,0,-0.203200><6.294600,0.035000,0.203200> rotate<0,0.000000,0> translate<24.579000,-1.535000,12.866600> }
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<24.620900,-1.535000,8.433300>}
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<24.720900,-1.535000,8.333300>}
box{<0,0,-0.203200><0.141421,0.035000,0.203200> rotate<0,44.997030,0> translate<24.620900,-1.535000,8.433300> }
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<24.620900,-1.535000,14.133300>}
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<24.720900,-1.535000,14.133300>}
box{<0,0,-0.203200><0.100000,0.035000,0.203200> rotate<0,0.000000,0> translate<24.620900,-1.535000,14.133300> }
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<24.625200,-1.535000,4.389400>}
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<24.933200,-1.535000,4.261800>}
box{<0,0,-0.203200><0.333385,0.035000,0.203200> rotate<0,22.502043,0> translate<24.625200,-1.535000,4.389400> }
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<24.625200,-1.535000,5.810500>}
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<24.933200,-1.535000,5.938100>}
box{<0,0,-0.203200><0.333385,0.035000,0.203200> rotate<0,-22.502043,0> translate<24.625200,-1.535000,5.810500> }
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<24.720900,-1.535000,8.333300>}
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<24.820900,-1.535000,8.333300>}
box{<0,0,-0.203200><0.100000,0.035000,0.203200> rotate<0,0.000000,0> translate<24.720900,-1.535000,8.333300> }
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<24.720900,-1.535000,14.133300>}
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<24.820900,-1.535000,14.033300>}
box{<0,0,-0.203200><0.141421,0.035000,0.203200> rotate<0,44.997030,0> translate<24.720900,-1.535000,14.133300> }
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<24.820900,-1.535000,8.333300>}
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<24.920900,-1.535000,8.233300>}
box{<0,0,-0.203200><0.141421,0.035000,0.203200> rotate<0,44.997030,0> translate<24.820900,-1.535000,8.333300> }
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<24.820900,-1.535000,14.033300>}
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<24.920900,-1.535000,14.033300>}
box{<0,0,-0.203200><0.100000,0.035000,0.203200> rotate<0,0.000000,0> translate<24.820900,-1.535000,14.033300> }
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<24.920900,-1.535000,8.233300>}
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<30.773600,-1.535000,8.233300>}
box{<0,0,-0.203200><5.852700,0.035000,0.203200> rotate<0,0.000000,0> translate<24.920900,-1.535000,8.233300> }
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<24.920900,-1.535000,14.033300>}
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<25.020900,-1.535000,13.933300>}
box{<0,0,-0.203200><0.141421,0.035000,0.203200> rotate<0,44.997030,0> translate<24.920900,-1.535000,14.033300> }
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<24.933200,-1.535000,4.261800>}
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<25.266700,-1.535000,4.261800>}
box{<0,0,-0.203200><0.333500,0.035000,0.203200> rotate<0,0.000000,0> translate<24.933200,-1.535000,4.261800> }
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<24.933200,-1.535000,5.938100>}
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<25.266700,-1.535000,5.938100>}
box{<0,0,-0.203200><0.333500,0.035000,0.203200> rotate<0,0.000000,0> translate<24.933200,-1.535000,5.938100> }
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<25.020900,-1.535000,13.933300>}
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<30.873600,-1.535000,13.933300>}
box{<0,0,-0.203200><5.852700,0.035000,0.203200> rotate<0,0.000000,0> translate<25.020900,-1.535000,13.933300> }
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<25.266700,-1.535000,4.261800>}
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<25.574700,-1.535000,4.389400>}
box{<0,0,-0.203200><0.333385,0.035000,0.203200> rotate<0,-22.502043,0> translate<25.266700,-1.535000,4.261800> }
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<25.266700,-1.535000,5.938100>}
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<25.574700,-1.535000,5.810500>}
box{<0,0,-0.203200><0.333385,0.035000,0.203200> rotate<0,22.502043,0> translate<25.266700,-1.535000,5.938100> }
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<25.574700,-1.535000,4.389400>}
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<25.751900,-1.535000,4.566600>}
box{<0,0,-0.203200><0.250599,0.035000,0.203200> rotate<0,-44.997030,0> translate<25.574700,-1.535000,4.389400> }
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<25.574700,-1.535000,5.810500>}
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<25.751900,-1.535000,5.633300>}
box{<0,0,-0.203200><0.250599,0.035000,0.203200> rotate<0,44.997030,0> translate<25.574700,-1.535000,5.810500> }
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<25.655700,-1.535000,4.470400>}
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<30.773600,-1.535000,4.470400>}
box{<0,0,-0.203200><5.117900,0.035000,0.203200> rotate<0,0.000000,0> translate<25.655700,-1.535000,4.470400> }
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<25.695600,-1.535000,5.689600>}
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<30.773600,-1.535000,5.689600>}
box{<0,0,-0.203200><5.078000,0.035000,0.203200> rotate<0,0.000000,0> translate<25.695600,-1.535000,5.689600> }
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<25.751900,-1.535000,4.566600>}
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<30.773600,-1.535000,4.566600>}
box{<0,0,-0.203200><5.021700,0.035000,0.203200> rotate<0,0.000000,0> translate<25.751900,-1.535000,4.566600> }
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<25.751900,-1.535000,5.633300>}
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<30.773600,-1.535000,5.633300>}
box{<0,0,-0.203200><5.021700,0.035000,0.203200> rotate<0,0.000000,0> translate<25.751900,-1.535000,5.633300> }
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<27.767800,-1.535000,21.553800>}
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<29.167800,-1.535000,21.553800>}
box{<0,0,-0.203200><1.400000,0.035000,0.203200> rotate<0,0.000000,0> translate<27.767800,-1.535000,21.553800> }
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<29.167800,-1.535000,21.553800>}
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<33.667800,-1.535000,21.553800>}
box{<0,0,-0.203200><4.500000,0.035000,0.203200> rotate<0,0.000000,0> translate<29.167800,-1.535000,21.553800> }
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<29.279000,-1.535000,10.333300>}
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<29.379000,-1.535000,10.433300>}
box{<0,0,-0.203200><0.141421,0.035000,0.203200> rotate<0,-44.997030,0> translate<29.279000,-1.535000,10.333300> }
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<29.379000,-1.535000,10.433300>}
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<29.479000,-1.535000,10.433300>}
box{<0,0,-0.203200><0.100000,0.035000,0.203200> rotate<0,0.000000,0> translate<29.379000,-1.535000,10.433300> }
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<29.479000,-1.535000,10.433300>}
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<29.579000,-1.535000,10.533300>}
box{<0,0,-0.203200><0.141421,0.035000,0.203200> rotate<0,-44.997030,0> translate<29.479000,-1.535000,10.433300> }
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<29.579000,-1.535000,10.533300>}
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<29.666600,-1.535000,10.533300>}
box{<0,0,-0.203200><0.087600,0.035000,0.203200> rotate<0,0.000000,0> translate<29.579000,-1.535000,10.533300> }
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<29.666600,-1.535000,10.920900>}
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<29.666600,-1.535000,10.533300>}
box{<0,0,-0.203200><0.387600,0.035000,0.203200> rotate<0,-90.000000,0> translate<29.666600,-1.535000,10.533300> }
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<29.666600,-1.535000,10.920900>}
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<29.979000,-1.535000,11.233300>}
box{<0,0,-0.203200><0.441800,0.035000,0.203200> rotate<0,-44.997030,0> translate<29.666600,-1.535000,10.920900> }
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<29.720900,-1.535000,9.266600>}
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<29.820900,-1.535000,9.366600>}
box{<0,0,-0.203200><0.141421,0.035000,0.203200> rotate<0,-44.997030,0> translate<29.720900,-1.535000,9.266600> }
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<29.801500,-1.535000,9.347200>}
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<36.588800,-1.535000,9.347200>}
box{<0,0,-0.203200><6.787300,0.035000,0.203200> rotate<0,0.000000,0> translate<29.801500,-1.535000,9.347200> }
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<29.820900,-1.535000,9.366600>}
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<29.920900,-1.535000,9.366600>}
box{<0,0,-0.203200><0.100000,0.035000,0.203200> rotate<0,0.000000,0> translate<29.820900,-1.535000,9.366600> }
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<29.920900,-1.535000,9.366600>}
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<30.020900,-1.535000,9.466600>}
box{<0,0,-0.203200><0.141421,0.035000,0.203200> rotate<0,-44.997030,0> translate<29.920900,-1.535000,9.366600> }
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<29.979000,-1.535000,11.233300>}
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<30.079000,-1.535000,11.333300>}
box{<0,0,-0.203200><0.141421,0.035000,0.203200> rotate<0,-44.997030,0> translate<29.979000,-1.535000,11.233300> }
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<30.020900,-1.535000,9.466600>}
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<30.120900,-1.535000,9.466600>}
box{<0,0,-0.203200><0.100000,0.035000,0.203200> rotate<0,0.000000,0> translate<30.020900,-1.535000,9.466600> }
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<30.079000,-1.535000,11.333300>}
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<30.520900,-1.535000,11.333300>}
box{<0,0,-0.203200><0.441900,0.035000,0.203200> rotate<0,0.000000,0> translate<30.079000,-1.535000,11.333300> }
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<30.120900,-1.535000,9.466600>}
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<30.220900,-1.535000,9.566600>}
box{<0,0,-0.203200><0.141421,0.035000,0.203200> rotate<0,-44.997030,0> translate<30.120900,-1.535000,9.466600> }
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<30.220900,-1.535000,9.566600>}
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<30.320900,-1.535000,9.566600>}
box{<0,0,-0.203200><0.100000,0.035000,0.203200> rotate<0,0.000000,0> translate<30.220900,-1.535000,9.566600> }
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<30.320900,-1.535000,9.566600>}
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<30.633300,-1.535000,9.879000>}
box{<0,0,-0.203200><0.441800,0.035000,0.203200> rotate<0,-44.997030,0> translate<30.320900,-1.535000,9.566600> }
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<30.507900,-1.535000,9.753600>}
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<31.081600,-1.535000,9.753600>}
box{<0,0,-0.203200><0.573700,0.035000,0.203200> rotate<0,0.000000,0> translate<30.507900,-1.535000,9.753600> }
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<30.520900,-1.535000,11.333300>}
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<30.873600,-1.535000,11.333300>}
box{<0,0,-0.203200><0.352700,0.035000,0.203200> rotate<0,0.000000,0> translate<30.520900,-1.535000,11.333300> }
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<30.633300,-1.535000,9.879000>}
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<30.733300,-1.535000,9.979000>}
box{<0,0,-0.203200><0.141421,0.035000,0.203200> rotate<0,-44.997030,0> translate<30.633300,-1.535000,9.879000> }
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<30.672000,-1.535000,15.519500>}
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<30.969500,-1.535000,15.222000>}
box{<0,0,-0.203200><0.420729,0.035000,0.203200> rotate<0,44.997030,0> translate<30.672000,-1.535000,15.519500> }
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<30.672000,-1.535000,16.283000>}
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<30.672000,-1.535000,15.519500>}
box{<0,0,-0.203200><0.763500,0.035000,0.203200> rotate<0,-90.000000,0> translate<30.672000,-1.535000,15.519500> }
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<30.672000,-1.535000,16.283000>}
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<31.932500,-1.535000,16.283000>}
box{<0,0,-0.203200><1.260500,0.035000,0.203200> rotate<0,0.000000,0> translate<30.672000,-1.535000,16.283000> }
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<30.672000,-1.535000,16.576800>}
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<31.932500,-1.535000,16.576800>}
box{<0,0,-0.203200><1.260500,0.035000,0.203200> rotate<0,0.000000,0> translate<30.672000,-1.535000,16.576800> }
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<30.672000,-1.535000,17.340400>}
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<30.672000,-1.535000,16.576800>}
box{<0,0,-0.203200><0.763600,0.035000,0.203200> rotate<0,-90.000000,0> translate<30.672000,-1.535000,16.576800> }
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<30.672000,-1.535000,17.340400>}
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<30.969500,-1.535000,17.637900>}
box{<0,0,-0.203200><0.420729,0.035000,0.203200> rotate<0,-44.997030,0> translate<30.672000,-1.535000,17.340400> }
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<30.733300,-1.535000,9.979000>}
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<30.733300,-1.535000,10.266600>}
box{<0,0,-0.203200><0.287600,0.035000,0.203200> rotate<0,90.000000,0> translate<30.733300,-1.535000,10.266600> }
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<30.733300,-1.535000,10.160000>}
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<30.873600,-1.535000,10.160000>}
box{<0,0,-0.203200><0.140300,0.035000,0.203200> rotate<0,0.000000,0> translate<30.733300,-1.535000,10.160000> }
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<30.733300,-1.535000,10.266600>}
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<30.873600,-1.535000,10.266600>}
box{<0,0,-0.203200><0.140300,0.035000,0.203200> rotate<0,0.000000,0> translate<30.733300,-1.535000,10.266600> }
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<30.773600,-1.535000,4.261600>}
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<31.011600,-1.535000,4.023600>}
box{<0,0,-0.203200><0.336583,0.035000,0.203200> rotate<0,44.997030,0> translate<30.773600,-1.535000,4.261600> }
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<30.773600,-1.535000,4.566600>}
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<30.773600,-1.535000,4.261600>}
box{<0,0,-0.203200><0.305000,0.035000,0.203200> rotate<0,-90.000000,0> translate<30.773600,-1.535000,4.261600> }
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<30.773600,-1.535000,5.998300>}
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<30.773600,-1.535000,5.633300>}
box{<0,0,-0.203200><0.365000,0.035000,0.203200> rotate<0,-90.000000,0> translate<30.773600,-1.535000,5.633300> }
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<30.773600,-1.535000,5.998300>}
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<31.011600,-1.535000,6.236300>}
box{<0,0,-0.203200><0.336583,0.035000,0.203200> rotate<0,-44.997030,0> translate<30.773600,-1.535000,5.998300> }
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<30.773600,-1.535000,6.801600>}
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<31.011600,-1.535000,6.563600>}
box{<0,0,-0.203200><0.336583,0.035000,0.203200> rotate<0,44.997030,0> translate<30.773600,-1.535000,6.801600> }
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<30.773600,-1.535000,7.166600>}
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<30.773600,-1.535000,6.801600>}
box{<0,0,-0.203200><0.365000,0.035000,0.203200> rotate<0,-90.000000,0> translate<30.773600,-1.535000,6.801600> }
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<30.773600,-1.535000,8.538300>}
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<30.773600,-1.535000,8.233300>}
box{<0,0,-0.203200><0.305000,0.035000,0.203200> rotate<0,-90.000000,0> translate<30.773600,-1.535000,8.233300> }
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<30.773600,-1.535000,8.538300>}
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<31.011600,-1.535000,8.776300>}
box{<0,0,-0.203200><0.336583,0.035000,0.203200> rotate<0,-44.997030,0> translate<30.773600,-1.535000,8.538300> }
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<30.773600,-1.535000,18.101600>}
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<31.011600,-1.535000,17.863600>}
box{<0,0,-0.203200><0.336583,0.035000,0.203200> rotate<0,44.997030,0> translate<30.773600,-1.535000,18.101600> }
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<30.773600,-1.535000,19.838300>}
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<30.773600,-1.535000,18.101600>}
box{<0,0,-0.203200><1.736700,0.035000,0.203200> rotate<0,-90.000000,0> translate<30.773600,-1.535000,18.101600> }
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<30.773600,-1.535000,19.838300>}
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<31.011600,-1.535000,20.076300>}
box{<0,0,-0.203200><0.336583,0.035000,0.203200> rotate<0,-44.997030,0> translate<30.773600,-1.535000,19.838300> }
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<30.873600,-1.535000,9.961600>}
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<31.111600,-1.535000,9.723600>}
box{<0,0,-0.203200><0.336583,0.035000,0.203200> rotate<0,44.997030,0> translate<30.873600,-1.535000,9.961600> }
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<30.873600,-1.535000,10.266600>}
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<30.873600,-1.535000,9.961600>}
box{<0,0,-0.203200><0.305000,0.035000,0.203200> rotate<0,-90.000000,0> translate<30.873600,-1.535000,9.961600> }
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<30.873600,-1.535000,11.698300>}
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<30.873600,-1.535000,11.333300>}
box{<0,0,-0.203200><0.365000,0.035000,0.203200> rotate<0,-90.000000,0> translate<30.873600,-1.535000,11.333300> }
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<30.873600,-1.535000,11.698300>}
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<31.111600,-1.535000,11.936300>}
box{<0,0,-0.203200><0.336583,0.035000,0.203200> rotate<0,-44.997030,0> translate<30.873600,-1.535000,11.698300> }
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<30.873600,-1.535000,12.501600>}
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<31.111600,-1.535000,12.263600>}
box{<0,0,-0.203200><0.336583,0.035000,0.203200> rotate<0,44.997030,0> translate<30.873600,-1.535000,12.501600> }
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<30.873600,-1.535000,12.866600>}
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<30.873600,-1.535000,12.501600>}
box{<0,0,-0.203200><0.365000,0.035000,0.203200> rotate<0,-90.000000,0> translate<30.873600,-1.535000,12.501600> }
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<30.873600,-1.535000,14.238300>}
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<30.873600,-1.535000,13.933300>}
box{<0,0,-0.203200><0.305000,0.035000,0.203200> rotate<0,-90.000000,0> translate<30.873600,-1.535000,13.933300> }
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<30.873600,-1.535000,14.238300>}
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<31.111600,-1.535000,14.476300>}
box{<0,0,-0.203200><0.336583,0.035000,0.203200> rotate<0,-44.997030,0> translate<30.873600,-1.535000,14.238300> }
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<30.969500,-1.535000,15.222000>}
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<31.933000,-1.535000,15.222000>}
box{<0,0,-0.203200><0.963500,0.035000,0.203200> rotate<0,0.000000,0> translate<30.969500,-1.535000,15.222000> }
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<30.969500,-1.535000,17.637900>}
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<31.933000,-1.535000,17.637900>}
box{<0,0,-0.203200><0.963500,0.035000,0.203200> rotate<0,0.000000,0> translate<30.969500,-1.535000,17.637900> }
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<31.011600,-1.535000,4.023600>}
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<33.148300,-1.535000,4.023600>}
box{<0,0,-0.203200><2.136700,0.035000,0.203200> rotate<0,0.000000,0> translate<31.011600,-1.535000,4.023600> }
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<31.011600,-1.535000,6.236300>}
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<33.148300,-1.535000,6.236300>}
box{<0,0,-0.203200><2.136700,0.035000,0.203200> rotate<0,0.000000,0> translate<31.011600,-1.535000,6.236300> }
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<31.011600,-1.535000,6.563600>}
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<33.148300,-1.535000,6.563600>}
box{<0,0,-0.203200><2.136700,0.035000,0.203200> rotate<0,0.000000,0> translate<31.011600,-1.535000,6.563600> }
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<31.011600,-1.535000,8.776300>}
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<33.148300,-1.535000,8.776300>}
box{<0,0,-0.203200><2.136700,0.035000,0.203200> rotate<0,0.000000,0> translate<31.011600,-1.535000,8.776300> }
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<31.011600,-1.535000,17.863600>}
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<33.148300,-1.535000,17.863600>}
box{<0,0,-0.203200><2.136700,0.035000,0.203200> rotate<0,0.000000,0> translate<31.011600,-1.535000,17.863600> }
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<31.011600,-1.535000,20.076300>}
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<31.326900,-1.535000,20.076300>}
box{<0,0,-0.203200><0.315300,0.035000,0.203200> rotate<0,0.000000,0> translate<31.011600,-1.535000,20.076300> }
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<31.111600,-1.535000,9.723600>}
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<33.248300,-1.535000,9.723600>}
box{<0,0,-0.203200><2.136700,0.035000,0.203200> rotate<0,0.000000,0> translate<31.111600,-1.535000,9.723600> }
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<31.111600,-1.535000,11.936300>}
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<33.248300,-1.535000,11.936300>}
box{<0,0,-0.203200><2.136700,0.035000,0.203200> rotate<0,0.000000,0> translate<31.111600,-1.535000,11.936300> }
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<31.111600,-1.535000,12.263600>}
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<33.248300,-1.535000,12.263600>}
box{<0,0,-0.203200><2.136700,0.035000,0.203200> rotate<0,0.000000,0> translate<31.111600,-1.535000,12.263600> }
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<31.111600,-1.535000,14.476300>}
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<33.248300,-1.535000,14.476300>}
box{<0,0,-0.203200><2.136700,0.035000,0.203200> rotate<0,0.000000,0> translate<31.111600,-1.535000,14.476300> }
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<31.261800,-1.535000,20.233200>}
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<31.326900,-1.535000,20.076300>}
box{<0,0,-0.203200><0.169869,0.035000,0.203200> rotate<0,67.461303,0> translate<31.261800,-1.535000,20.233200> }
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<31.261800,-1.535000,20.566700>}
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<31.261800,-1.535000,20.233200>}
box{<0,0,-0.203200><0.333500,0.035000,0.203200> rotate<0,-90.000000,0> translate<31.261800,-1.535000,20.233200> }
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<31.261800,-1.535000,20.566700>}
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<31.389400,-1.535000,20.874700>}
box{<0,0,-0.203200><0.333385,0.035000,0.203200> rotate<0,-67.492017,0> translate<31.261800,-1.535000,20.566700> }
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<31.389400,-1.535000,20.874700>}
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<31.625200,-1.535000,21.110500>}
box{<0,0,-0.203200><0.333472,0.035000,0.203200> rotate<0,-44.997030,0> translate<31.389400,-1.535000,20.874700> }
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<31.625200,-1.535000,21.110500>}
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<31.933200,-1.535000,21.238100>}
box{<0,0,-0.203200><0.333385,0.035000,0.203200> rotate<0,-22.502043,0> translate<31.625200,-1.535000,21.110500> }
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<31.800300,-1.535000,24.246100>}
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<32.233300,-1.535000,24.679000>}
box{<0,0,-0.203200><0.612284,0.035000,0.203200> rotate<0,-44.990414,0> translate<31.800300,-1.535000,24.246100> }
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<31.800300,-1.535000,24.246100>}
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<33.667800,-1.535000,24.246100>}
box{<0,0,-0.203200><1.867500,0.035000,0.203200> rotate<0,0.000000,0> translate<31.800300,-1.535000,24.246100> }
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<31.932500,-1.535000,16.576800>}
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<31.932500,-1.535000,16.283000>}
box{<0,0,-0.203200><0.293800,0.035000,0.203200> rotate<0,-90.000000,0> translate<31.932500,-1.535000,16.283000> }
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<31.933000,-1.535000,15.222000>}
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<31.933000,-1.535000,16.282500>}
box{<0,0,-0.203200><1.060500,0.035000,0.203200> rotate<0,90.000000,0> translate<31.933000,-1.535000,16.282500> }
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<31.933000,-1.535000,15.443200>}
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<32.226800,-1.535000,15.443200>}
box{<0,0,-0.203200><0.293800,0.035000,0.203200> rotate<0,0.000000,0> translate<31.933000,-1.535000,15.443200> }
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<31.933000,-1.535000,15.849600>}
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<32.226800,-1.535000,15.849600>}
box{<0,0,-0.203200><0.293800,0.035000,0.203200> rotate<0,0.000000,0> translate<31.933000,-1.535000,15.849600> }
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<31.933000,-1.535000,16.256000>}
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<32.226800,-1.535000,16.256000>}
box{<0,0,-0.203200><0.293800,0.035000,0.203200> rotate<0,0.000000,0> translate<31.933000,-1.535000,16.256000> }
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<31.933000,-1.535000,16.282500>}
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<32.226800,-1.535000,16.282500>}
box{<0,0,-0.203200><0.293800,0.035000,0.203200> rotate<0,0.000000,0> translate<31.933000,-1.535000,16.282500> }
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<31.933000,-1.535000,16.577400>}
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<31.933000,-1.535000,17.637900>}
box{<0,0,-0.203200><1.060500,0.035000,0.203200> rotate<0,90.000000,0> translate<31.933000,-1.535000,17.637900> }
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<31.933000,-1.535000,16.577400>}
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<32.226800,-1.535000,16.577400>}
box{<0,0,-0.203200><0.293800,0.035000,0.203200> rotate<0,0.000000,0> translate<31.933000,-1.535000,16.577400> }
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<31.933000,-1.535000,16.662400>}
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<32.226800,-1.535000,16.662400>}
box{<0,0,-0.203200><0.293800,0.035000,0.203200> rotate<0,0.000000,0> translate<31.933000,-1.535000,16.662400> }
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<31.933000,-1.535000,17.068800>}
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<32.226800,-1.535000,17.068800>}
box{<0,0,-0.203200><0.293800,0.035000,0.203200> rotate<0,0.000000,0> translate<31.933000,-1.535000,17.068800> }
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<31.933000,-1.535000,17.475200>}
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<32.226800,-1.535000,17.475200>}
box{<0,0,-0.203200><0.293800,0.035000,0.203200> rotate<0,0.000000,0> translate<31.933000,-1.535000,17.475200> }
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<31.933200,-1.535000,21.238100>}
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<32.266700,-1.535000,21.238100>}
box{<0,0,-0.203200><0.333500,0.035000,0.203200> rotate<0,0.000000,0> translate<31.933200,-1.535000,21.238100> }
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<31.938200,-1.535000,24.384000>}
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<35.078300,-1.535000,24.384000>}
box{<0,0,-0.203200><3.140100,0.035000,0.203200> rotate<0,0.000000,0> translate<31.938200,-1.535000,24.384000> }
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<32.226800,-1.535000,15.222000>}
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<33.190400,-1.535000,15.222000>}
box{<0,0,-0.203200><0.963600,0.035000,0.203200> rotate<0,0.000000,0> translate<32.226800,-1.535000,15.222000> }
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<32.226800,-1.535000,16.282500>}
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<32.226800,-1.535000,15.222000>}
box{<0,0,-0.203200><1.060500,0.035000,0.203200> rotate<0,-90.000000,0> translate<32.226800,-1.535000,15.222000> }
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<32.226800,-1.535000,17.637900>}
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<32.226800,-1.535000,16.577400>}
box{<0,0,-0.203200><1.060500,0.035000,0.203200> rotate<0,-90.000000,0> translate<32.226800,-1.535000,16.577400> }
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<32.226800,-1.535000,17.637900>}
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<33.190400,-1.535000,17.637900>}
box{<0,0,-0.203200><0.963600,0.035000,0.203200> rotate<0,0.000000,0> translate<32.226800,-1.535000,17.637900> }
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<32.227400,-1.535000,16.283000>}
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<32.227400,-1.535000,16.576800>}
box{<0,0,-0.203200><0.293800,0.035000,0.203200> rotate<0,90.000000,0> translate<32.227400,-1.535000,16.576800> }
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<32.227400,-1.535000,16.283000>}
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<33.487900,-1.535000,16.283000>}
box{<0,0,-0.203200><1.260500,0.035000,0.203200> rotate<0,0.000000,0> translate<32.227400,-1.535000,16.283000> }
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<32.227400,-1.535000,16.576800>}
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<33.487900,-1.535000,16.576800>}
box{<0,0,-0.203200><1.260500,0.035000,0.203200> rotate<0,0.000000,0> translate<32.227400,-1.535000,16.576800> }
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<32.233300,-1.535000,24.679000>}
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<32.233300,-1.535000,24.793600>}
box{<0,0,-0.203200><0.114600,0.035000,0.203200> rotate<0,90.000000,0> translate<32.233300,-1.535000,24.793600> }
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<32.233300,-1.535000,24.790400>}
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<34.655200,-1.535000,24.790400>}
box{<0,0,-0.203200><2.421900,0.035000,0.203200> rotate<0,0.000000,0> translate<32.233300,-1.535000,24.790400> }
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<32.233300,-1.535000,24.793600>}
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<32.968300,-1.535000,24.793600>}
box{<0,0,-0.203200><0.735000,0.035000,0.203200> rotate<0,0.000000,0> translate<32.233300,-1.535000,24.793600> }
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<32.266700,-1.535000,21.238100>}
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<32.574700,-1.535000,21.110500>}
box{<0,0,-0.203200><0.333385,0.035000,0.203200> rotate<0,22.502043,0> translate<32.266700,-1.535000,21.238100> }
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<32.520900,-1.535000,21.132800>}
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<34.788800,-1.535000,21.132800>}
box{<0,0,-0.203200><2.267900,0.035000,0.203200> rotate<0,0.000000,0> translate<32.520900,-1.535000,21.132800> }
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<32.574700,-1.535000,21.110500>}
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<32.810500,-1.535000,20.874700>}
box{<0,0,-0.203200><0.333472,0.035000,0.203200> rotate<0,44.997030,0> translate<32.574700,-1.535000,21.110500> }
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<32.810500,-1.535000,20.874700>}
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<32.938100,-1.535000,20.566700>}
box{<0,0,-0.203200><0.333385,0.035000,0.203200> rotate<0,67.492017,0> translate<32.810500,-1.535000,20.874700> }
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<32.872000,-1.535000,20.726400>}
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<34.788800,-1.535000,20.726400>}
box{<0,0,-0.203200><1.916800,0.035000,0.203200> rotate<0,0.000000,0> translate<32.872000,-1.535000,20.726400> }
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<32.873000,-1.535000,20.076300>}
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<32.938100,-1.535000,20.233200>}
box{<0,0,-0.203200><0.169869,0.035000,0.203200> rotate<0,-67.461303,0> translate<32.873000,-1.535000,20.076300> }
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<32.873000,-1.535000,20.076300>}
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<33.148300,-1.535000,20.076300>}
box{<0,0,-0.203200><0.275300,0.035000,0.203200> rotate<0,0.000000,0> translate<32.873000,-1.535000,20.076300> }
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<32.938100,-1.535000,20.233200>}
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<32.938100,-1.535000,20.566700>}
box{<0,0,-0.203200><0.333500,0.035000,0.203200> rotate<0,90.000000,0> translate<32.938100,-1.535000,20.566700> }
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<32.938100,-1.535000,20.320000>}
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<34.846100,-1.535000,20.320000>}
box{<0,0,-0.203200><1.908000,0.035000,0.203200> rotate<0,0.000000,0> translate<32.938100,-1.535000,20.320000> }
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<32.968300,-1.535000,24.793600>}
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<33.206300,-1.535000,25.031600>}
box{<0,0,-0.203200><0.336583,0.035000,0.203200> rotate<0,-44.997030,0> translate<32.968300,-1.535000,24.793600> }
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<33.074600,-1.535000,26.799900>}
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<33.141200,-1.535000,26.866600>}
box{<0,0,-0.203200><0.094257,0.035000,0.203200> rotate<0,-45.040010,0> translate<33.074600,-1.535000,26.799900> }
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<33.074600,-1.535000,26.799900>}
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<33.206300,-1.535000,26.668300>}
box{<0,0,-0.203200><0.186181,0.035000,0.203200> rotate<0,44.975271,0> translate<33.074600,-1.535000,26.799900> }
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<33.097000,-1.535000,26.822400>}
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<34.291600,-1.535000,26.822400>}
box{<0,0,-0.203200><1.194600,0.035000,0.203200> rotate<0,0.000000,0> translate<33.097000,-1.535000,26.822400> }
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<33.141200,-1.535000,26.866600>}
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<34.291600,-1.535000,26.866600>}
box{<0,0,-0.203200><1.150400,0.035000,0.203200> rotate<0,0.000000,0> translate<33.141200,-1.535000,26.866600> }
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<33.148300,-1.535000,4.023600>}
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<33.386300,-1.535000,4.261600>}
box{<0,0,-0.203200><0.336583,0.035000,0.203200> rotate<0,-44.997030,0> translate<33.148300,-1.535000,4.023600> }
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<33.148300,-1.535000,6.236300>}
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<33.386300,-1.535000,5.998300>}
box{<0,0,-0.203200><0.336583,0.035000,0.203200> rotate<0,44.997030,0> translate<33.148300,-1.535000,6.236300> }
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<33.148300,-1.535000,6.563600>}
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<33.386300,-1.535000,6.801600>}
box{<0,0,-0.203200><0.336583,0.035000,0.203200> rotate<0,-44.997030,0> translate<33.148300,-1.535000,6.563600> }
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<33.148300,-1.535000,8.776300>}
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<33.386300,-1.535000,8.538300>}
box{<0,0,-0.203200><0.336583,0.035000,0.203200> rotate<0,44.997030,0> translate<33.148300,-1.535000,8.776300> }
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<33.148300,-1.535000,17.863600>}
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<33.386300,-1.535000,18.101600>}
box{<0,0,-0.203200><0.336583,0.035000,0.203200> rotate<0,-44.997030,0> translate<33.148300,-1.535000,17.863600> }
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<33.148300,-1.535000,20.076300>}
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<33.386300,-1.535000,19.838300>}
box{<0,0,-0.203200><0.336583,0.035000,0.203200> rotate<0,44.997030,0> translate<33.148300,-1.535000,20.076300> }
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<33.166300,-1.535000,17.881600>}
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<36.588800,-1.535000,17.881600>}
box{<0,0,-0.203200><3.422500,0.035000,0.203200> rotate<0,0.000000,0> translate<33.166300,-1.535000,17.881600> }
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<33.188700,-1.535000,4.064000>}
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<38.666400,-1.535000,4.064000>}
box{<0,0,-0.203200><5.477700,0.035000,0.203200> rotate<0,0.000000,0> translate<33.188700,-1.535000,4.064000> }
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<33.190400,-1.535000,15.222000>}
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<33.487900,-1.535000,15.519500>}
box{<0,0,-0.203200><0.420729,0.035000,0.203200> rotate<0,-44.997030,0> translate<33.190400,-1.535000,15.222000> }
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<33.190400,-1.535000,17.637900>}
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<33.487900,-1.535000,17.340400>}
box{<0,0,-0.203200><0.420729,0.035000,0.203200> rotate<0,44.997030,0> translate<33.190400,-1.535000,17.637900> }
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<33.206300,-1.535000,25.031600>}
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<33.206300,-1.535000,26.668300>}
box{<0,0,-0.203200><1.636700,0.035000,0.203200> rotate<0,90.000000,0> translate<33.206300,-1.535000,26.668300> }
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<33.206300,-1.535000,25.196800>}
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<34.378000,-1.535000,25.196800>}
box{<0,0,-0.203200><1.171700,0.035000,0.203200> rotate<0,0.000000,0> translate<33.206300,-1.535000,25.196800> }
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<33.206300,-1.535000,25.603200>}
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<34.291600,-1.535000,25.603200>}
box{<0,0,-0.203200><1.085300,0.035000,0.203200> rotate<0,0.000000,0> translate<33.206300,-1.535000,25.603200> }
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<33.206300,-1.535000,26.009600>}
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<34.291600,-1.535000,26.009600>}
box{<0,0,-0.203200><1.085300,0.035000,0.203200> rotate<0,0.000000,0> translate<33.206300,-1.535000,26.009600> }
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<33.206300,-1.535000,26.416000>}
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<34.291600,-1.535000,26.416000>}
box{<0,0,-0.203200><1.085300,0.035000,0.203200> rotate<0,0.000000,0> translate<33.206300,-1.535000,26.416000> }
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<33.248300,-1.535000,9.723600>}
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<33.486300,-1.535000,9.961600>}
box{<0,0,-0.203200><0.336583,0.035000,0.203200> rotate<0,-44.997030,0> translate<33.248300,-1.535000,9.723600> }
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<33.248300,-1.535000,11.936300>}
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<33.486300,-1.535000,11.698300>}
box{<0,0,-0.203200><0.336583,0.035000,0.203200> rotate<0,44.997030,0> translate<33.248300,-1.535000,11.936300> }
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<33.248300,-1.535000,12.263600>}
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<33.486300,-1.535000,12.501600>}
box{<0,0,-0.203200><0.336583,0.035000,0.203200> rotate<0,-44.997030,0> translate<33.248300,-1.535000,12.263600> }
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<33.248300,-1.535000,14.476300>}
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<33.486300,-1.535000,14.238300>}
box{<0,0,-0.203200><0.336583,0.035000,0.203200> rotate<0,44.997030,0> translate<33.248300,-1.535000,14.476300> }
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<33.278300,-1.535000,9.753600>}
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<36.268300,-1.535000,9.753600>}
box{<0,0,-0.203200><2.990000,0.035000,0.203200> rotate<0,0.000000,0> translate<33.278300,-1.535000,9.753600> }
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<33.288600,-1.535000,6.096000>}
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<40.500100,-1.535000,6.096000>}
box{<0,0,-0.203200><7.211500,0.035000,0.203200> rotate<0,0.000000,0> translate<33.288600,-1.535000,6.096000> }
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<33.311000,-1.535000,19.913600>}
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<35.102800,-1.535000,19.913600>}
box{<0,0,-0.203200><1.791800,0.035000,0.203200> rotate<0,0.000000,0> translate<33.311000,-1.535000,19.913600> }
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<33.353100,-1.535000,17.475200>}
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<36.588800,-1.535000,17.475200>}
box{<0,0,-0.203200><3.235700,0.035000,0.203200> rotate<0,0.000000,0> translate<33.353100,-1.535000,17.475200> }
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<33.386300,-1.535000,4.261600>}
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<33.386300,-1.535000,5.998300>}
box{<0,0,-0.203200><1.736700,0.035000,0.203200> rotate<0,90.000000,0> translate<33.386300,-1.535000,5.998300> }
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<33.386300,-1.535000,4.470400>}
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<38.642400,-1.535000,4.470400>}
box{<0,0,-0.203200><5.256100,0.035000,0.203200> rotate<0,0.000000,0> translate<33.386300,-1.535000,4.470400> }
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<33.386300,-1.535000,4.876800>}
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<38.724900,-1.535000,4.876800>}
box{<0,0,-0.203200><5.338600,0.035000,0.203200> rotate<0,0.000000,0> translate<33.386300,-1.535000,4.876800> }
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<33.386300,-1.535000,5.283200>}
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<38.947000,-1.535000,5.283200>}
box{<0,0,-0.203200><5.560700,0.035000,0.203200> rotate<0,0.000000,0> translate<33.386300,-1.535000,5.283200> }
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<33.386300,-1.535000,5.689600>}
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<39.502500,-1.535000,5.689600>}
box{<0,0,-0.203200><6.116200,0.035000,0.203200> rotate<0,0.000000,0> translate<33.386300,-1.535000,5.689600> }
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<33.386300,-1.535000,6.801600>}
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<33.386300,-1.535000,8.538300>}
box{<0,0,-0.203200><1.736700,0.035000,0.203200> rotate<0,90.000000,0> translate<33.386300,-1.535000,8.538300> }
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<33.386300,-1.535000,6.908800>}
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<39.687300,-1.535000,6.908800>}
box{<0,0,-0.203200><6.301000,0.035000,0.203200> rotate<0,0.000000,0> translate<33.386300,-1.535000,6.908800> }
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<33.386300,-1.535000,7.315200>}
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<39.280900,-1.535000,7.315200>}
box{<0,0,-0.203200><5.894600,0.035000,0.203200> rotate<0,0.000000,0> translate<33.386300,-1.535000,7.315200> }
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<33.386300,-1.535000,7.721600>}
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<38.874500,-1.535000,7.721600>}
box{<0,0,-0.203200><5.488200,0.035000,0.203200> rotate<0,0.000000,0> translate<33.386300,-1.535000,7.721600> }
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<33.386300,-1.535000,8.128000>}
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<36.866200,-1.535000,8.128000>}
box{<0,0,-0.203200><3.479900,0.035000,0.203200> rotate<0,0.000000,0> translate<33.386300,-1.535000,8.128000> }
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<33.386300,-1.535000,8.534400>}
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<36.598700,-1.535000,8.534400>}
box{<0,0,-0.203200><3.212400,0.035000,0.203200> rotate<0,0.000000,0> translate<33.386300,-1.535000,8.534400> }
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<33.386300,-1.535000,18.101600>}
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<33.386300,-1.535000,19.838300>}
box{<0,0,-0.203200><1.736700,0.035000,0.203200> rotate<0,90.000000,0> translate<33.386300,-1.535000,19.838300> }
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<33.386300,-1.535000,18.288000>}
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<36.508100,-1.535000,18.288000>}
box{<0,0,-0.203200><3.121800,0.035000,0.203200> rotate<0,0.000000,0> translate<33.386300,-1.535000,18.288000> }
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<33.386300,-1.535000,18.694400>}
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<36.101700,-1.535000,18.694400>}
box{<0,0,-0.203200><2.715400,0.035000,0.203200> rotate<0,0.000000,0> translate<33.386300,-1.535000,18.694400> }
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<33.386300,-1.535000,19.100800>}
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<35.695300,-1.535000,19.100800>}
box{<0,0,-0.203200><2.309000,0.035000,0.203200> rotate<0,0.000000,0> translate<33.386300,-1.535000,19.100800> }
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<33.386300,-1.535000,19.507200>}
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<35.288900,-1.535000,19.507200>}
box{<0,0,-0.203200><1.902600,0.035000,0.203200> rotate<0,0.000000,0> translate<33.386300,-1.535000,19.507200> }
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<33.399000,-1.535000,11.785600>}
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<34.301800,-1.535000,11.785600>}
box{<0,0,-0.203200><0.902800,0.035000,0.203200> rotate<0,0.000000,0> translate<33.399000,-1.535000,11.785600> }
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<33.411600,-1.535000,15.443200>}
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<37.027100,-1.535000,15.443200>}
box{<0,0,-0.203200><3.615500,0.035000,0.203200> rotate<0,0.000000,0> translate<33.411600,-1.535000,15.443200> }
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<33.486300,-1.535000,9.961600>}
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<33.486300,-1.535000,11.698300>}
box{<0,0,-0.203200><1.736700,0.035000,0.203200> rotate<0,90.000000,0> translate<33.486300,-1.535000,11.698300> }
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<33.486300,-1.535000,10.160000>}
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<35.875000,-1.535000,10.160000>}
box{<0,0,-0.203200><2.388700,0.035000,0.203200> rotate<0,0.000000,0> translate<33.486300,-1.535000,10.160000> }
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<33.486300,-1.535000,10.566400>}
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<35.481700,-1.535000,10.566400>}
box{<0,0,-0.203200><1.995400,0.035000,0.203200> rotate<0,0.000000,0> translate<33.486300,-1.535000,10.566400> }
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<33.486300,-1.535000,10.972800>}
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<35.088400,-1.535000,10.972800>}
box{<0,0,-0.203200><1.602100,0.035000,0.203200> rotate<0,0.000000,0> translate<33.486300,-1.535000,10.972800> }
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<33.486300,-1.535000,11.379200>}
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<34.695100,-1.535000,11.379200>}
box{<0,0,-0.203200><1.208800,0.035000,0.203200> rotate<0,0.000000,0> translate<33.486300,-1.535000,11.379200> }
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<33.486300,-1.535000,12.501600>}
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<33.486300,-1.535000,14.238300>}
box{<0,0,-0.203200><1.736700,0.035000,0.203200> rotate<0,90.000000,0> translate<33.486300,-1.535000,14.238300> }
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<33.486300,-1.535000,12.598400>}
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<33.711100,-1.535000,12.598400>}
box{<0,0,-0.203200><0.224800,0.035000,0.203200> rotate<0,0.000000,0> translate<33.486300,-1.535000,12.598400> }
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<33.486300,-1.535000,13.004800>}
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<33.653800,-1.535000,13.004800>}
box{<0,0,-0.203200><0.167500,0.035000,0.203200> rotate<0,0.000000,0> translate<33.486300,-1.535000,13.004800> }
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<33.486300,-1.535000,13.411200>}
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<33.711100,-1.535000,13.411200>}
box{<0,0,-0.203200><0.224800,0.035000,0.203200> rotate<0,0.000000,0> translate<33.486300,-1.535000,13.411200> }
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<33.486300,-1.535000,13.817600>}
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<33.909200,-1.535000,13.817600>}
box{<0,0,-0.203200><0.422900,0.035000,0.203200> rotate<0,0.000000,0> translate<33.486300,-1.535000,13.817600> }
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<33.486300,-1.535000,14.224000>}
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<34.434600,-1.535000,14.224000>}
box{<0,0,-0.203200><0.948300,0.035000,0.203200> rotate<0,0.000000,0> translate<33.486300,-1.535000,14.224000> }
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<33.487900,-1.535000,15.519500>}
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<33.487900,-1.535000,16.283000>}
box{<0,0,-0.203200><0.763500,0.035000,0.203200> rotate<0,90.000000,0> translate<33.487900,-1.535000,16.283000> }
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<33.487900,-1.535000,15.849600>}
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<36.633900,-1.535000,15.849600>}
box{<0,0,-0.203200><3.146000,0.035000,0.203200> rotate<0,0.000000,0> translate<33.487900,-1.535000,15.849600> }
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<33.487900,-1.535000,16.256000>}
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<36.588800,-1.535000,16.256000>}
box{<0,0,-0.203200><3.100900,0.035000,0.203200> rotate<0,0.000000,0> translate<33.487900,-1.535000,16.256000> }
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<33.487900,-1.535000,16.576800>}
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<33.487900,-1.535000,17.340400>}
box{<0,0,-0.203200><0.763600,0.035000,0.203200> rotate<0,90.000000,0> translate<33.487900,-1.535000,17.340400> }
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<33.487900,-1.535000,16.662400>}
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<36.588800,-1.535000,16.662400>}
box{<0,0,-0.203200><3.100900,0.035000,0.203200> rotate<0,0.000000,0> translate<33.487900,-1.535000,16.662400> }
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<33.487900,-1.535000,17.068800>}
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<36.588800,-1.535000,17.068800>}
box{<0,0,-0.203200><3.100900,0.035000,0.203200> rotate<0,0.000000,0> translate<33.487900,-1.535000,17.068800> }
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<33.649600,-1.535000,12.754200>}
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<33.658400,-1.535000,13.289800>}
box{<0,0,-0.203200><0.535672,0.035000,0.203200> rotate<0,-89.052828,0> translate<33.649600,-1.535000,12.754200> }
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<33.649600,-1.535000,12.754200>}
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<33.846400,-1.535000,12.256200>}
box{<0,0,-0.203200><0.535476,0.035000,0.203200> rotate<0,68.432507,0> translate<33.649600,-1.535000,12.754200> }
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<33.658400,-1.535000,13.289800>}
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<33.871400,-1.535000,13.781100>}
box{<0,0,-0.203200><0.535485,0.035000,0.203200> rotate<0,-66.556770,0> translate<33.658400,-1.535000,13.289800> }
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<33.667800,-1.535000,21.553800>}
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<34.162500,-1.535000,21.758700>}
box{<0,0,-0.203200><0.535455,0.035000,0.203200> rotate<0,-22.497383,0> translate<33.667800,-1.535000,21.553800> }
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<33.667800,-1.535000,24.246100>}
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<34.162500,-1.535000,24.041200>}
box{<0,0,-0.203200><0.535455,0.035000,0.203200> rotate<0,22.497383,0> translate<33.667800,-1.535000,24.246100> }
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<33.846400,-1.535000,12.256200>}
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<36.588800,-1.535000,9.422500>}
box{<0,0,-0.203200><3.943427,0.035000,0.203200> rotate<0,45.935014,0> translate<33.846400,-1.535000,12.256200> }
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<33.871400,-1.535000,13.781100>}
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<34.256200,-1.535000,14.153500>}
box{<0,0,-0.203200><0.535493,0.035000,0.203200> rotate<0,-44.058893,0> translate<33.871400,-1.535000,13.781100> }
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<34.162500,-1.535000,21.758700>}
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<34.541200,-1.535000,22.137400>}
box{<0,0,-0.203200><0.535563,0.035000,0.203200> rotate<0,-44.997030,0> translate<34.162500,-1.535000,21.758700> }
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<34.162500,-1.535000,24.041200>}
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<34.541200,-1.535000,23.662500>}
box{<0,0,-0.203200><0.535563,0.035000,0.203200> rotate<0,44.997030,0> translate<34.162500,-1.535000,24.041200> }
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<34.226100,-1.535000,23.977600>}
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<34.803800,-1.535000,23.977600>}
box{<0,0,-0.203200><0.577700,0.035000,0.203200> rotate<0,0.000000,0> translate<34.226100,-1.535000,23.977600> }
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<34.256200,-1.535000,14.153500>}
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<34.754200,-1.535000,14.350300>}
box{<0,0,-0.203200><0.535476,0.035000,0.203200> rotate<0,-21.561553,0> translate<34.256200,-1.535000,14.153500> }
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<34.291600,-1.535000,25.405500>}
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<34.469500,-1.535000,24.976100>}
box{<0,0,-0.203200><0.464793,0.035000,0.203200> rotate<0,67.491366,0> translate<34.291600,-1.535000,25.405500> }
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<34.291600,-1.535000,26.866600>}
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<34.291600,-1.535000,25.405500>}
box{<0,0,-0.203200><1.461100,0.035000,0.203200> rotate<0,-90.000000,0> translate<34.291600,-1.535000,25.405500> }
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<34.349400,-1.535000,21.945600>}
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<34.788800,-1.535000,21.945600>}
box{<0,0,-0.203200><0.439400,0.035000,0.203200> rotate<0,0.000000,0> translate<34.349400,-1.535000,21.945600> }
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<34.469500,-1.535000,24.976100>}
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<34.798100,-1.535000,24.647500>}
box{<0,0,-0.203200><0.464711,0.035000,0.203200> rotate<0,44.997030,0> translate<34.469500,-1.535000,24.976100> }
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<34.541200,-1.535000,22.137400>}
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<34.746100,-1.535000,22.632100>}
box{<0,0,-0.203200><0.535455,0.035000,0.203200> rotate<0,-67.496677,0> translate<34.541200,-1.535000,22.137400> }
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<34.541200,-1.535000,23.662500>}
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<34.746100,-1.535000,23.167800>}
box{<0,0,-0.203200><0.535455,0.035000,0.203200> rotate<0,67.496677,0> translate<34.541200,-1.535000,23.662500> }
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<34.579100,-1.535000,23.571200>}
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<34.788800,-1.535000,23.571200>}
box{<0,0,-0.203200><0.209700,0.035000,0.203200> rotate<0,0.000000,0> translate<34.579100,-1.535000,23.571200> }
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<34.630000,-1.535000,22.352000>}
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<34.788800,-1.535000,22.352000>}
box{<0,0,-0.203200><0.158800,0.035000,0.203200> rotate<0,0.000000,0> translate<34.630000,-1.535000,22.352000> }
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<34.746100,-1.535000,22.632100>}
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<34.746100,-1.535000,23.167800>}
box{<0,0,-0.203200><0.535700,0.035000,0.203200> rotate<0,90.000000,0> translate<34.746100,-1.535000,23.167800> }
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<34.746100,-1.535000,22.758400>}
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<34.788800,-1.535000,22.758400>}
box{<0,0,-0.203200><0.042700,0.035000,0.203200> rotate<0,0.000000,0> translate<34.746100,-1.535000,22.758400> }
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<34.746100,-1.535000,23.164800>}
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<34.788800,-1.535000,23.164800>}
box{<0,0,-0.203200><0.042700,0.035000,0.203200> rotate<0,0.000000,0> translate<34.746100,-1.535000,23.164800> }
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<34.754200,-1.535000,14.350300>}
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<35.289800,-1.535000,14.341500>}
box{<0,0,-0.203200><0.535672,0.035000,0.203200> rotate<0,0.941233,0> translate<34.754200,-1.535000,14.350300> }
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<34.788800,-1.535000,20.458500>}
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<34.897100,-1.535000,20.197100>}
box{<0,0,-0.203200><0.282947,0.035000,0.203200> rotate<0,67.490948,0> translate<34.788800,-1.535000,20.458500> }
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<34.788800,-1.535000,23.941400>}
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<34.788800,-1.535000,20.458500>}
box{<0,0,-0.203200><3.482900,0.035000,0.203200> rotate<0,-90.000000,0> translate<34.788800,-1.535000,20.458500> }
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<34.788800,-1.535000,23.941400>}
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<34.897100,-1.535000,24.202800>}
box{<0,0,-0.203200><0.282947,0.035000,0.203200> rotate<0,-67.490948,0> translate<34.788800,-1.535000,23.941400> }
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<34.798100,-1.535000,24.647500>}
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<35.227500,-1.535000,24.469600>}
box{<0,0,-0.203200><0.464793,0.035000,0.203200> rotate<0,22.502694,0> translate<34.798100,-1.535000,24.647500> }
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<34.897100,-1.535000,20.197100>}
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<35.053800,-1.535000,20.040500>}
box{<0,0,-0.203200><0.221537,0.035000,0.203200> rotate<0,44.978744,0> translate<34.897100,-1.535000,20.197100> }
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<34.897100,-1.535000,24.202800>}
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<35.097100,-1.535000,24.402800>}
box{<0,0,-0.203200><0.282843,0.035000,0.203200> rotate<0,-44.997030,0> translate<34.897100,-1.535000,24.202800> }
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<35.053800,-1.535000,20.032100>}
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<35.258700,-1.535000,19.537400>}
box{<0,0,-0.203200><0.535455,0.035000,0.203200> rotate<0,67.496677,0> translate<35.053800,-1.535000,20.032100> }
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<35.053800,-1.535000,20.040500>}
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<35.053800,-1.535000,20.032100>}
box{<0,0,-0.203200><0.008400,0.035000,0.203200> rotate<0,-90.000000,0> translate<35.053800,-1.535000,20.032100> }
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<35.097100,-1.535000,24.402800>}
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<35.258600,-1.535000,24.469600>}
box{<0,0,-0.203200><0.174770,0.035000,0.203200> rotate<0,-22.469595,0> translate<35.097100,-1.535000,24.402800> }
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<35.227500,-1.535000,24.469600>}
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<35.258600,-1.535000,24.469600>}
box{<0,0,-0.203200><0.031100,0.035000,0.203200> rotate<0,0.000000,0> translate<35.227500,-1.535000,24.469600> }
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<35.258700,-1.535000,19.537400>}
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<36.588800,-1.535000,18.207300>}
box{<0,0,-0.203200><1.881045,0.035000,0.203200> rotate<0,44.997030,0> translate<35.258700,-1.535000,19.537400> }
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<35.289800,-1.535000,14.341500>}
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<35.781100,-1.535000,14.128500>}
box{<0,0,-0.203200><0.535485,0.035000,0.203200> rotate<0,23.437291,0> translate<35.289800,-1.535000,14.341500> }
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<35.560900,-1.535000,14.224000>}
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<38.453800,-1.535000,14.224000>}
box{<0,0,-0.203200><2.892900,0.035000,0.203200> rotate<0,0.000000,0> translate<35.560900,-1.535000,14.224000> }
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<35.781100,-1.535000,14.128500>}
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<37.636600,-1.535000,12.211100>}
box{<0,0,-0.203200><2.668202,0.035000,0.203200> rotate<0,45.936906,0> translate<35.781100,-1.535000,14.128500> }
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<36.082000,-1.535000,13.817600>}
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<38.453800,-1.535000,13.817600>}
box{<0,0,-0.203200><2.371800,0.035000,0.203200> rotate<0,0.000000,0> translate<36.082000,-1.535000,13.817600> }
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<36.475300,-1.535000,13.411200>}
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<38.586700,-1.535000,13.411200>}
box{<0,0,-0.203200><2.111400,0.035000,0.203200> rotate<0,0.000000,0> translate<36.475300,-1.535000,13.411200> }
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<36.588800,-1.535000,8.558500>}
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<36.697100,-1.535000,8.297100>}
box{<0,0,-0.203200><0.282947,0.035000,0.203200> rotate<0,67.490948,0> translate<36.588800,-1.535000,8.558500> }
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<36.588800,-1.535000,9.422500>}
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<36.588800,-1.535000,8.558500>}
box{<0,0,-0.203200><0.864000,0.035000,0.203200> rotate<0,-90.000000,0> translate<36.588800,-1.535000,8.558500> }
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<36.588800,-1.535000,15.958500>}
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<36.697100,-1.535000,15.697100>}
box{<0,0,-0.203200><0.282947,0.035000,0.203200> rotate<0,67.490948,0> translate<36.588800,-1.535000,15.958500> }
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<36.588800,-1.535000,18.207300>}
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<36.588800,-1.535000,15.958500>}
box{<0,0,-0.203200><2.248800,0.035000,0.203200> rotate<0,-90.000000,0> translate<36.588800,-1.535000,15.958500> }
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<36.697100,-1.535000,8.297100>}
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<36.897100,-1.535000,8.097100>}
box{<0,0,-0.203200><0.282843,0.035000,0.203200> rotate<0,44.997030,0> translate<36.697100,-1.535000,8.297100> }
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<36.697100,-1.535000,15.697100>}
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<36.897100,-1.535000,15.497100>}
box{<0,0,-0.203200><0.282843,0.035000,0.203200> rotate<0,44.997030,0> translate<36.697100,-1.535000,15.697100> }
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<36.868600,-1.535000,13.004800>}
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<38.891300,-1.535000,13.004800>}
box{<0,0,-0.203200><2.022700,0.035000,0.203200> rotate<0,0.000000,0> translate<36.868600,-1.535000,13.004800> }
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<36.897100,-1.535000,8.097100>}
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<37.158500,-1.535000,7.988800>}
box{<0,0,-0.203200><0.282947,0.035000,0.203200> rotate<0,22.503112,0> translate<36.897100,-1.535000,8.097100> }
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<36.897100,-1.535000,15.497100>}
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<37.158500,-1.535000,15.388800>}
box{<0,0,-0.203200><0.282947,0.035000,0.203200> rotate<0,22.503112,0> translate<36.897100,-1.535000,15.497100> }
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<37.158500,-1.535000,7.988800>}
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<38.607400,-1.535000,7.988800>}
box{<0,0,-0.203200><1.448900,0.035000,0.203200> rotate<0,0.000000,0> translate<37.158500,-1.535000,7.988800> }
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<37.158500,-1.535000,15.388800>}
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<38.453800,-1.535000,15.388800>}
box{<0,0,-0.203200><1.295300,0.035000,0.203200> rotate<0,0.000000,0> translate<37.158500,-1.535000,15.388800> }
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<37.261900,-1.535000,12.598400>}
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<39.524400,-1.535000,12.598400>}
box{<0,0,-0.203200><2.262500,0.035000,0.203200> rotate<0,0.000000,0> translate<37.261900,-1.535000,12.598400> }
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<37.636600,-1.535000,12.211100>}
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<41.241400,-1.535000,12.211100>}
box{<0,0,-0.203200><3.604800,0.035000,0.203200> rotate<0,0.000000,0> translate<37.636600,-1.535000,12.211100> }
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<38.453800,-1.535000,13.732100>}
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<38.658700,-1.535000,13.237400>}
box{<0,0,-0.203200><0.535455,0.035000,0.203200> rotate<0,67.496677,0> translate<38.453800,-1.535000,13.732100> }
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<38.453800,-1.535000,15.388800>}
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<38.453800,-1.535000,13.732100>}
box{<0,0,-0.203200><1.656700,0.035000,0.203200> rotate<0,-90.000000,0> translate<38.453800,-1.535000,13.732100> }
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<38.607400,-1.535000,7.988800>}
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<40.637400,-1.535000,5.958700>}
box{<0,0,-0.203200><2.870924,0.035000,0.203200> rotate<0,44.998441,0> translate<38.607400,-1.535000,7.988800> }
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<38.642400,-1.535000,4.122000>}
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<38.855100,-1.535000,3.608600>}
box{<0,0,-0.203200><0.555717,0.035000,0.203200> rotate<0,67.491473,0> translate<38.642400,-1.535000,4.122000> }
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<38.642400,-1.535000,4.677900>}
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<38.642400,-1.535000,4.122000>}
box{<0,0,-0.203200><0.555900,0.035000,0.203200> rotate<0,-90.000000,0> translate<38.642400,-1.535000,4.122000> }
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<38.642400,-1.535000,4.677900>}
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<38.855100,-1.535000,5.191300>}
box{<0,0,-0.203200><0.555717,0.035000,0.203200> rotate<0,-67.491473,0> translate<38.642400,-1.535000,4.677900> }
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<38.658700,-1.535000,13.237400>}
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<39.037400,-1.535000,12.858700>}
box{<0,0,-0.203200><0.535563,0.035000,0.203200> rotate<0,44.997030,0> translate<38.658700,-1.535000,13.237400> }
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<38.849800,-1.535000,19.753800>}
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<38.992600,-1.535000,19.611100>}
box{<0,0,-0.203200><0.201879,0.035000,0.203200> rotate<0,44.976963,0> translate<38.849800,-1.535000,19.753800> }
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<38.849800,-1.535000,19.753800>}
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<39.267800,-1.535000,19.753800>}
box{<0,0,-0.203200><0.418000,0.035000,0.203200> rotate<0,0.000000,0> translate<38.849800,-1.535000,19.753800> }
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<38.855100,-1.535000,3.608600>}
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<39.248000,-1.535000,3.215700>}
box{<0,0,-0.203200><0.555645,0.035000,0.203200> rotate<0,44.997030,0> translate<38.855100,-1.535000,3.608600> }
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<38.855100,-1.535000,5.191300>}
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<39.248000,-1.535000,5.584200>}
box{<0,0,-0.203200><0.555645,0.035000,0.203200> rotate<0,-44.997030,0> translate<38.855100,-1.535000,5.191300> }
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<38.992600,-1.535000,19.611100>}
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<41.241400,-1.535000,19.611100>}
box{<0,0,-0.203200><2.248800,0.035000,0.203200> rotate<0,0.000000,0> translate<38.992600,-1.535000,19.611100> }
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<39.037400,-1.535000,12.858700>}
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<39.137400,-1.535000,12.758700>}
box{<0,0,-0.203200><0.141421,0.035000,0.203200> rotate<0,44.997030,0> translate<39.037400,-1.535000,12.858700> }
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<39.137400,-1.535000,12.758700>}
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<39.632100,-1.535000,12.553800>}
box{<0,0,-0.203200><0.535455,0.035000,0.203200> rotate<0,22.497383,0> translate<39.137400,-1.535000,12.758700> }
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<39.248000,-1.535000,3.215700>}
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<39.761400,-1.535000,3.003000>}
box{<0,0,-0.203200><0.555717,0.035000,0.203200> rotate<0,22.502588,0> translate<39.248000,-1.535000,3.215700> }
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<39.248000,-1.535000,5.584200>}
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<39.761400,-1.535000,5.796900>}
box{<0,0,-0.203200><0.555717,0.035000,0.203200> rotate<0,-22.502588,0> translate<39.248000,-1.535000,5.584200> }
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<39.267800,-1.535000,19.753800>}
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<39.762500,-1.535000,19.958700>}
box{<0,0,-0.203200><0.535455,0.035000,0.203200> rotate<0,-22.497383,0> translate<39.267800,-1.535000,19.753800> }
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<39.632100,-1.535000,12.553800>}
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<40.167800,-1.535000,12.553800>}
box{<0,0,-0.203200><0.535700,0.035000,0.203200> rotate<0,0.000000,0> translate<39.632100,-1.535000,12.553800> }
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<39.653600,-1.535000,19.913600>}
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<43.588800,-1.535000,19.913600>}
box{<0,0,-0.203200><3.935200,0.035000,0.203200> rotate<0,0.000000,0> translate<39.653600,-1.535000,19.913600> }
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<39.761400,-1.535000,3.003000>}
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<42.298500,-1.535000,3.003000>}
box{<0,0,-0.203200><2.537100,0.035000,0.203200> rotate<0,0.000000,0> translate<39.761400,-1.535000,3.003000> }
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<39.761400,-1.535000,5.796900>}
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<41.028200,-1.535000,5.796900>}
box{<0,0,-0.203200><1.266800,0.035000,0.203200> rotate<0,0.000000,0> translate<39.761400,-1.535000,5.796900> }
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<39.762500,-1.535000,19.958700>}
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<40.141200,-1.535000,20.337400>}
box{<0,0,-0.203200><0.535563,0.035000,0.203200> rotate<0,-44.997030,0> translate<39.762500,-1.535000,19.958700> }
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<40.123800,-1.535000,20.320000>}
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<40.805200,-1.535000,20.320000>}
box{<0,0,-0.203200><0.681400,0.035000,0.203200> rotate<0,0.000000,0> translate<40.123800,-1.535000,20.320000> }
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<40.141200,-1.535000,20.337400>}
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<40.346100,-1.535000,20.832100>}
box{<0,0,-0.203200><0.535455,0.035000,0.203200> rotate<0,-67.496677,0> translate<40.141200,-1.535000,20.337400> }
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<40.167800,-1.535000,12.553800>}
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<43.553800,-1.535000,12.553800>}
box{<0,0,-0.203200><3.386000,0.035000,0.203200> rotate<0,0.000000,0> translate<40.167800,-1.535000,12.553800> }
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<40.302300,-1.535000,20.726400>}
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<40.693600,-1.535000,20.726400>}
box{<0,0,-0.203200><0.391300,0.035000,0.203200> rotate<0,0.000000,0> translate<40.302300,-1.535000,20.726400> }
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<40.346100,-1.535000,20.832100>}
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<40.346100,-1.535000,23.932100>}
box{<0,0,-0.203200><3.100000,0.035000,0.203200> rotate<0,90.000000,0> translate<40.346100,-1.535000,23.932100> }
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<40.346100,-1.535000,21.132800>}
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<40.693600,-1.535000,21.132800>}
box{<0,0,-0.203200><0.347500,0.035000,0.203200> rotate<0,0.000000,0> translate<40.346100,-1.535000,21.132800> }
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<40.346100,-1.535000,21.539200>}
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<40.693600,-1.535000,21.539200>}
box{<0,0,-0.203200><0.347500,0.035000,0.203200> rotate<0,0.000000,0> translate<40.346100,-1.535000,21.539200> }
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<40.346100,-1.535000,21.945600>}
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<40.693600,-1.535000,21.945600>}
box{<0,0,-0.203200><0.347500,0.035000,0.203200> rotate<0,0.000000,0> translate<40.346100,-1.535000,21.945600> }
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<40.346100,-1.535000,22.352000>}
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<40.693600,-1.535000,22.352000>}
box{<0,0,-0.203200><0.347500,0.035000,0.203200> rotate<0,0.000000,0> translate<40.346100,-1.535000,22.352000> }
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<40.346100,-1.535000,22.758400>}
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<40.693600,-1.535000,22.758400>}
box{<0,0,-0.203200><0.347500,0.035000,0.203200> rotate<0,0.000000,0> translate<40.346100,-1.535000,22.758400> }
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<40.346100,-1.535000,23.164800>}
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<40.693600,-1.535000,23.164800>}
box{<0,0,-0.203200><0.347500,0.035000,0.203200> rotate<0,0.000000,0> translate<40.346100,-1.535000,23.164800> }
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<40.346100,-1.535000,23.571200>}
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<40.693600,-1.535000,23.571200>}
box{<0,0,-0.203200><0.347500,0.035000,0.203200> rotate<0,0.000000,0> translate<40.346100,-1.535000,23.571200> }
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<40.346100,-1.535000,23.932100>}
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<40.346100,-1.535000,24.164800>}
box{<0,0,-0.203200><0.232700,0.035000,0.203200> rotate<0,90.000000,0> translate<40.346100,-1.535000,24.164800> }
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<40.346100,-1.535000,23.977600>}
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<40.703000,-1.535000,23.977600>}
box{<0,0,-0.203200><0.356900,0.035000,0.203200> rotate<0,0.000000,0> translate<40.346100,-1.535000,23.977600> }
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<40.346100,-1.535000,24.164800>}
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<40.833000,-1.535000,24.164800>}
box{<0,0,-0.203200><0.486900,0.035000,0.203200> rotate<0,0.000000,0> translate<40.346100,-1.535000,24.164800> }
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<40.637400,-1.535000,5.958700>}
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<41.028200,-1.535000,5.796900>}
box{<0,0,-0.203200><0.422970,0.035000,0.203200> rotate<0,22.489172,0> translate<40.637400,-1.535000,5.958700> }
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<40.693600,-1.535000,20.431600>}
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<40.931600,-1.535000,20.193600>}
box{<0,0,-0.203200><0.336583,0.035000,0.203200> rotate<0,44.997030,0> translate<40.693600,-1.535000,20.431600> }
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<40.693600,-1.535000,23.968300>}
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<40.693600,-1.535000,20.431600>}
box{<0,0,-0.203200><3.536700,0.035000,0.203200> rotate<0,-90.000000,0> translate<40.693600,-1.535000,20.431600> }
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<40.693600,-1.535000,23.968300>}
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<40.930700,-1.535000,24.205300>}
box{<0,0,-0.203200><0.335239,0.035000,0.203200> rotate<0,-44.984946,0> translate<40.693600,-1.535000,23.968300> }
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<40.833000,-1.535000,24.164800>}
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<40.930700,-1.535000,24.205300>}
box{<0,0,-0.203200><0.105762,0.035000,0.203200> rotate<0,-22.514197,0> translate<40.833000,-1.535000,24.164800> }
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<40.931600,-1.535000,20.193600>}
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<43.068300,-1.535000,20.193600>}
box{<0,0,-0.203200><2.136700,0.035000,0.203200> rotate<0,0.000000,0> translate<40.931600,-1.535000,20.193600> }
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<41.146100,-1.535000,15.246100>}
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<41.146100,-1.535000,15.388800>}
box{<0,0,-0.203200><0.142700,0.035000,0.203200> rotate<0,90.000000,0> translate<41.146100,-1.535000,15.388800> }
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<41.146100,-1.535000,15.246100>}
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<43.588800,-1.535000,15.246100>}
box{<0,0,-0.203200><2.442700,0.035000,0.203200> rotate<0,0.000000,0> translate<41.146100,-1.535000,15.246100> }
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<41.146100,-1.535000,15.388800>}
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<41.241400,-1.535000,15.388800>}
box{<0,0,-0.203200><0.095300,0.035000,0.203200> rotate<0,0.000000,0> translate<41.146100,-1.535000,15.388800> }
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<41.241400,-1.535000,12.211100>}
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<41.502800,-1.535000,12.102800>}
box{<0,0,-0.203200><0.282947,0.035000,0.203200> rotate<0,22.503112,0> translate<41.241400,-1.535000,12.211100> }
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<41.241400,-1.535000,15.388800>}
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<41.502800,-1.535000,15.497100>}
box{<0,0,-0.203200><0.282947,0.035000,0.203200> rotate<0,-22.503112,0> translate<41.241400,-1.535000,15.388800> }
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<41.241400,-1.535000,19.611100>}
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<41.502800,-1.535000,19.502800>}
box{<0,0,-0.203200><0.282947,0.035000,0.203200> rotate<0,22.503112,0> translate<41.241400,-1.535000,19.611100> }
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<41.287600,-1.535000,12.192000>}
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<43.553800,-1.535000,12.192000>}
box{<0,0,-0.203200><2.266200,0.035000,0.203200> rotate<0,0.000000,0> translate<41.287600,-1.535000,12.192000> }
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<41.372700,-1.535000,15.443200>}
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<43.589600,-1.535000,15.443200>}
box{<0,0,-0.203200><2.216900,0.035000,0.203200> rotate<0,0.000000,0> translate<41.372700,-1.535000,15.443200> }
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<41.492200,-1.535000,19.507200>}
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<43.692900,-1.535000,19.507200>}
box{<0,0,-0.203200><2.200700,0.035000,0.203200> rotate<0,0.000000,0> translate<41.492200,-1.535000,19.507200> }
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<41.502800,-1.535000,12.102800>}
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<41.702800,-1.535000,11.902800>}
box{<0,0,-0.203200><0.282843,0.035000,0.203200> rotate<0,44.997030,0> translate<41.502800,-1.535000,12.102800> }
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<41.502800,-1.535000,15.497100>}
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<41.702800,-1.535000,15.697100>}
box{<0,0,-0.203200><0.282843,0.035000,0.203200> rotate<0,-44.997030,0> translate<41.502800,-1.535000,15.497100> }
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<41.502800,-1.535000,19.502800>}
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<41.702800,-1.535000,19.302800>}
box{<0,0,-0.203200><0.282843,0.035000,0.203200> rotate<0,44.997030,0> translate<41.502800,-1.535000,19.502800> }
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<41.702800,-1.535000,11.902800>}
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<41.811100,-1.535000,11.641400>}
box{<0,0,-0.203200><0.282947,0.035000,0.203200> rotate<0,67.490948,0> translate<41.702800,-1.535000,11.902800> }
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<41.702800,-1.535000,15.697100>}
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<41.811100,-1.535000,15.958500>}
box{<0,0,-0.203200><0.282947,0.035000,0.203200> rotate<0,-67.490948,0> translate<41.702800,-1.535000,15.697100> }
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<41.702800,-1.535000,19.302800>}
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<41.811100,-1.535000,19.041400>}
box{<0,0,-0.203200><0.282947,0.035000,0.203200> rotate<0,67.490948,0> translate<41.702800,-1.535000,19.302800> }
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<41.751400,-1.535000,11.785600>}
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<43.553800,-1.535000,11.785600>}
box{<0,0,-0.203200><1.802400,0.035000,0.203200> rotate<0,0.000000,0> translate<41.751400,-1.535000,11.785600> }
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<41.765900,-1.535000,15.849600>}
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<43.843900,-1.535000,15.849600>}
box{<0,0,-0.203200><2.078000,0.035000,0.203200> rotate<0,0.000000,0> translate<41.765900,-1.535000,15.849600> }
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<41.786500,-1.535000,19.100800>}
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<53.738800,-1.535000,19.100800>}
box{<0,0,-0.203200><11.952300,0.035000,0.203200> rotate<0,0.000000,0> translate<41.786500,-1.535000,19.100800> }
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<41.811100,-1.535000,8.592500>}
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<41.811100,-1.535000,11.641400>}
box{<0,0,-0.203200><3.048900,0.035000,0.203200> rotate<0,90.000000,0> translate<41.811100,-1.535000,11.641400> }
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<41.811100,-1.535000,8.592500>}
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<41.957600,-1.535000,8.446100>}
box{<0,0,-0.203200><0.207112,0.035000,0.203200> rotate<0,44.977470,0> translate<41.811100,-1.535000,8.592500> }
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<41.811100,-1.535000,8.940800>}
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<43.253800,-1.535000,8.940800>}
box{<0,0,-0.203200><1.442700,0.035000,0.203200> rotate<0,0.000000,0> translate<41.811100,-1.535000,8.940800> }
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<41.811100,-1.535000,9.347200>}
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<43.253800,-1.535000,9.347200>}
box{<0,0,-0.203200><1.442700,0.035000,0.203200> rotate<0,0.000000,0> translate<41.811100,-1.535000,9.347200> }
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<41.811100,-1.535000,9.753600>}
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<43.253800,-1.535000,9.753600>}
box{<0,0,-0.203200><1.442700,0.035000,0.203200> rotate<0,0.000000,0> translate<41.811100,-1.535000,9.753600> }
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<41.811100,-1.535000,10.160000>}
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<43.253800,-1.535000,10.160000>}
box{<0,0,-0.203200><1.442700,0.035000,0.203200> rotate<0,0.000000,0> translate<41.811100,-1.535000,10.160000> }
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<41.811100,-1.535000,10.566400>}
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<43.253800,-1.535000,10.566400>}
box{<0,0,-0.203200><1.442700,0.035000,0.203200> rotate<0,0.000000,0> translate<41.811100,-1.535000,10.566400> }
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<41.811100,-1.535000,10.972800>}
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<43.253800,-1.535000,10.972800>}
box{<0,0,-0.203200><1.442700,0.035000,0.203200> rotate<0,0.000000,0> translate<41.811100,-1.535000,10.972800> }
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<41.811100,-1.535000,11.379200>}
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<43.341400,-1.535000,11.379200>}
box{<0,0,-0.203200><1.530300,0.035000,0.203200> rotate<0,0.000000,0> translate<41.811100,-1.535000,11.379200> }
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<41.811100,-1.535000,15.958500>}
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<41.811100,-1.535000,19.041400>}
box{<0,0,-0.203200><3.082900,0.035000,0.203200> rotate<0,90.000000,0> translate<41.811100,-1.535000,19.041400> }
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<41.811100,-1.535000,16.256000>}
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<45.959400,-1.535000,16.256000>}
box{<0,0,-0.203200><4.148300,0.035000,0.203200> rotate<0,0.000000,0> translate<41.811100,-1.535000,16.256000> }
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<41.811100,-1.535000,16.662400>}
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<54.558600,-1.535000,16.662400>}
box{<0,0,-0.203200><12.747500,0.035000,0.203200> rotate<0,0.000000,0> translate<41.811100,-1.535000,16.662400> }
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<41.811100,-1.535000,17.068800>}
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<55.148700,-1.535000,17.068800>}
box{<0,0,-0.203200><13.337600,0.035000,0.203200> rotate<0,0.000000,0> translate<41.811100,-1.535000,17.068800> }
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<41.811100,-1.535000,17.475200>}
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<54.420900,-1.535000,17.475200>}
box{<0,0,-0.203200><12.609800,0.035000,0.203200> rotate<0,0.000000,0> translate<41.811100,-1.535000,17.475200> }
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<41.811100,-1.535000,17.881600>}
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<54.030100,-1.535000,17.881600>}
box{<0,0,-0.203200><12.219000,0.035000,0.203200> rotate<0,0.000000,0> translate<41.811100,-1.535000,17.881600> }
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<41.811100,-1.535000,18.288000>}
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<53.861700,-1.535000,18.288000>}
box{<0,0,-0.203200><12.050600,0.035000,0.203200> rotate<0,0.000000,0> translate<41.811100,-1.535000,18.288000> }
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<41.811100,-1.535000,18.694400>}
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<53.738800,-1.535000,18.694400>}
box{<0,0,-0.203200><11.927700,0.035000,0.203200> rotate<0,0.000000,0> translate<41.811100,-1.535000,18.694400> }
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<41.869300,-1.535000,8.534400>}
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<43.418500,-1.535000,8.534400>}
box{<0,0,-0.203200><1.549200,0.035000,0.203200> rotate<0,0.000000,0> translate<41.869300,-1.535000,8.534400> }
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<41.957600,-1.535000,8.446100>}
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<43.167800,-1.535000,8.446100>}
box{<0,0,-0.203200><1.210200,0.035000,0.203200> rotate<0,0.000000,0> translate<41.957600,-1.535000,8.446100> }
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<42.298500,-1.535000,3.003000>}
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<42.811900,-1.535000,3.215700>}
box{<0,0,-0.203200><0.555717,0.035000,0.203200> rotate<0,-22.502588,0> translate<42.298500,-1.535000,3.003000> }
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<42.402300,-1.535000,5.753800>}
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<42.811900,-1.535000,5.584200>}
box{<0,0,-0.203200><0.443324,0.035000,0.203200> rotate<0,22.491127,0> translate<42.402300,-1.535000,5.753800> }
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<42.402300,-1.535000,5.753800>}
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<43.167800,-1.535000,5.753800>}
box{<0,0,-0.203200><0.765500,0.035000,0.203200> rotate<0,0.000000,0> translate<42.402300,-1.535000,5.753800> }
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<42.557400,-1.535000,5.689600>}
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<46.945300,-1.535000,5.689600>}
box{<0,0,-0.203200><4.387900,0.035000,0.203200> rotate<0,0.000000,0> translate<42.557400,-1.535000,5.689600> }
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<42.811900,-1.535000,3.215700>}
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<43.204800,-1.535000,3.608600>}
box{<0,0,-0.203200><0.555645,0.035000,0.203200> rotate<0,-44.997030,0> translate<42.811900,-1.535000,3.215700> }
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<42.811900,-1.535000,5.584200>}
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<43.204800,-1.535000,5.191300>}
box{<0,0,-0.203200><0.555645,0.035000,0.203200> rotate<0,44.997030,0> translate<42.811900,-1.535000,5.584200> }
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<42.847400,-1.535000,3.251200>}
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<58.014600,-1.535000,3.251200>}
box{<0,0,-0.203200><15.167200,0.035000,0.203200> rotate<0,0.000000,0> translate<42.847400,-1.535000,3.251200> }
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<43.068300,-1.535000,20.193600>}
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<43.306300,-1.535000,20.431600>}
box{<0,0,-0.203200><0.336583,0.035000,0.203200> rotate<0,-44.997030,0> translate<43.068300,-1.535000,20.193600> }
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<43.112900,-1.535000,5.283200>}
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<46.602600,-1.535000,5.283200>}
box{<0,0,-0.203200><3.489700,0.035000,0.203200> rotate<0,0.000000,0> translate<43.112900,-1.535000,5.283200> }
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<43.167800,-1.535000,5.753800>}
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<43.662500,-1.535000,5.958700>}
box{<0,0,-0.203200><0.535455,0.035000,0.203200> rotate<0,-22.497383,0> translate<43.167800,-1.535000,5.753800> }
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<43.167800,-1.535000,8.446100>}
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<43.649700,-1.535000,8.246400>}
box{<0,0,-0.203200><0.521639,0.035000,0.203200> rotate<0,22.507697,0> translate<43.167800,-1.535000,8.446100> }
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<43.194700,-1.535000,20.320000>}
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<43.588800,-1.535000,20.320000>}
box{<0,0,-0.203200><0.394100,0.035000,0.203200> rotate<0,0.000000,0> translate<43.194700,-1.535000,20.320000> }
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<43.204800,-1.535000,3.608600>}
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<43.259000,-1.535000,3.739600>}
box{<0,0,-0.203200><0.141770,0.035000,0.203200> rotate<0,-67.518685,0> translate<43.204800,-1.535000,3.608600> }
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<43.204800,-1.535000,5.191300>}
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<43.259000,-1.535000,5.060300>}
box{<0,0,-0.203200><0.141770,0.035000,0.203200> rotate<0,67.518685,0> translate<43.204800,-1.535000,5.191300> }
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<43.225000,-1.535000,3.657600>}
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<47.173700,-1.535000,3.657600>}
box{<0,0,-0.203200><3.948700,0.035000,0.203200> rotate<0,0.000000,0> translate<43.225000,-1.535000,3.657600> }
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<43.253800,-1.535000,8.932100>}
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<43.458700,-1.535000,8.437400>}
box{<0,0,-0.203200><0.535455,0.035000,0.203200> rotate<0,67.496677,0> translate<43.253800,-1.535000,8.932100> }
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<43.253800,-1.535000,11.167800>}
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<43.253800,-1.535000,8.932100>}
box{<0,0,-0.203200><2.235700,0.035000,0.203200> rotate<0,-90.000000,0> translate<43.253800,-1.535000,8.932100> }
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<43.253800,-1.535000,11.167800>}
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<43.458700,-1.535000,11.662500>}
box{<0,0,-0.203200><0.535455,0.035000,0.203200> rotate<0,-67.496677,0> translate<43.253800,-1.535000,11.167800> }
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<43.259000,-1.535000,3.739600>}
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<46.841600,-1.535000,3.739600>}
box{<0,0,-0.203200><3.582600,0.035000,0.203200> rotate<0,0.000000,0> translate<43.259000,-1.535000,3.739600> }
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<43.259000,-1.535000,5.060300>}
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<46.510200,-1.535000,5.060300>}
box{<0,0,-0.203200><3.251200,0.035000,0.203200> rotate<0,0.000000,0> translate<43.259000,-1.535000,5.060300> }
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<43.306300,-1.535000,20.431600>}
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<43.306300,-1.535000,23.367600>}
box{<0,0,-0.203200><2.936000,0.035000,0.203200> rotate<0,90.000000,0> translate<43.306300,-1.535000,23.367600> }
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<43.306300,-1.535000,20.726400>}
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<43.588800,-1.535000,20.726400>}
box{<0,0,-0.203200><0.282500,0.035000,0.203200> rotate<0,0.000000,0> translate<43.306300,-1.535000,20.726400> }
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<43.306300,-1.535000,21.132800>}
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<43.588800,-1.535000,21.132800>}
box{<0,0,-0.203200><0.282500,0.035000,0.203200> rotate<0,0.000000,0> translate<43.306300,-1.535000,21.132800> }
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<43.306300,-1.535000,21.539200>}
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<43.588800,-1.535000,21.539200>}
box{<0,0,-0.203200><0.282500,0.035000,0.203200> rotate<0,0.000000,0> translate<43.306300,-1.535000,21.539200> }
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<43.306300,-1.535000,21.945600>}
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<43.588800,-1.535000,21.945600>}
box{<0,0,-0.203200><0.282500,0.035000,0.203200> rotate<0,0.000000,0> translate<43.306300,-1.535000,21.945600> }
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<43.306300,-1.535000,22.352000>}
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<43.588800,-1.535000,22.352000>}
box{<0,0,-0.203200><0.282500,0.035000,0.203200> rotate<0,0.000000,0> translate<43.306300,-1.535000,22.352000> }
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<43.306300,-1.535000,22.758400>}
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<43.588800,-1.535000,22.758400>}
box{<0,0,-0.203200><0.282500,0.035000,0.203200> rotate<0,0.000000,0> translate<43.306300,-1.535000,22.758400> }
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<43.306300,-1.535000,23.164800>}
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<43.509000,-1.535000,23.164800>}
box{<0,0,-0.203200><0.202700,0.035000,0.203200> rotate<0,0.000000,0> translate<43.306300,-1.535000,23.164800> }
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<43.306300,-1.535000,23.367600>}
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<43.524900,-1.535000,23.148900>}
box{<0,0,-0.203200><0.309218,0.035000,0.203200> rotate<0,45.010131,0> translate<43.306300,-1.535000,23.367600> }
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<43.458700,-1.535000,8.437400>}
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<43.649700,-1.535000,8.246400>}
box{<0,0,-0.203200><0.270115,0.035000,0.203200> rotate<0,44.997030,0> translate<43.458700,-1.535000,8.437400> }
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<43.458700,-1.535000,11.662500>}
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<43.553800,-1.535000,11.757600>}
box{<0,0,-0.203200><0.134492,0.035000,0.203200> rotate<0,-44.997030,0> translate<43.458700,-1.535000,11.662500> }
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<43.524900,-1.535000,23.148900>}
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<43.688200,-1.535000,23.081300>}
box{<0,0,-0.203200><0.176739,0.035000,0.203200> rotate<0,22.486214,0> translate<43.524900,-1.535000,23.148900> }
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<43.553800,-1.535000,12.553800>}
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<43.553800,-1.535000,11.757600>}
box{<0,0,-0.203200><0.796200,0.035000,0.203200> rotate<0,-90.000000,0> translate<43.553800,-1.535000,11.757600> }
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<43.588800,-1.535000,15.441400>}
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<43.588800,-1.535000,15.246100>}
box{<0,0,-0.203200><0.195300,0.035000,0.203200> rotate<0,-90.000000,0> translate<43.588800,-1.535000,15.246100> }
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<43.588800,-1.535000,15.441400>}
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<43.697100,-1.535000,15.702800>}
box{<0,0,-0.203200><0.282947,0.035000,0.203200> rotate<0,-67.490948,0> translate<43.588800,-1.535000,15.441400> }
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<43.588800,-1.535000,19.758500>}
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<43.697100,-1.535000,19.497100>}
box{<0,0,-0.203200><0.282947,0.035000,0.203200> rotate<0,67.490948,0> translate<43.588800,-1.535000,19.758500> }
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<43.588800,-1.535000,22.841400>}
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<43.588800,-1.535000,19.758500>}
box{<0,0,-0.203200><3.082900,0.035000,0.203200> rotate<0,-90.000000,0> translate<43.588800,-1.535000,19.758500> }
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<43.588800,-1.535000,22.841400>}
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<43.688200,-1.535000,23.081300>}
box{<0,0,-0.203200><0.259677,0.035000,0.203200> rotate<0,-67.489396,0> translate<43.588800,-1.535000,22.841400> }
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<43.662500,-1.535000,5.958700>}
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<44.041200,-1.535000,6.337400>}
box{<0,0,-0.203200><0.535563,0.035000,0.203200> rotate<0,-44.997030,0> translate<43.662500,-1.535000,5.958700> }
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<43.697100,-1.535000,15.702800>}
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<43.897100,-1.535000,15.902800>}
box{<0,0,-0.203200><0.282843,0.035000,0.203200> rotate<0,-44.997030,0> translate<43.697100,-1.535000,15.702800> }
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<43.697100,-1.535000,19.497100>}
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<43.897100,-1.535000,19.297100>}
box{<0,0,-0.203200><0.282843,0.035000,0.203200> rotate<0,44.997030,0> translate<43.697100,-1.535000,19.497100> }
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<43.799800,-1.535000,6.096000>}
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<47.153500,-1.535000,6.096000>}
box{<0,0,-0.203200><3.353700,0.035000,0.203200> rotate<0,0.000000,0> translate<43.799800,-1.535000,6.096000> }
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<43.850200,-1.535000,8.053400>}
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<44.041200,-1.535000,7.862500>}
box{<0,0,-0.203200><0.270044,0.035000,0.203200> rotate<0,44.982028,0> translate<43.850200,-1.535000,8.053400> }
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<43.850200,-1.535000,8.053400>}
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<44.332100,-1.535000,7.853800>}
box{<0,0,-0.203200><0.521601,0.035000,0.203200> rotate<0,22.497549,0> translate<43.850200,-1.535000,8.053400> }
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<43.897100,-1.535000,15.902800>}
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<44.158500,-1.535000,16.011100>}
box{<0,0,-0.203200><0.282947,0.035000,0.203200> rotate<0,-22.503112,0> translate<43.897100,-1.535000,15.902800> }
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<43.897100,-1.535000,19.297100>}
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<44.158500,-1.535000,19.188800>}
box{<0,0,-0.203200><0.282947,0.035000,0.203200> rotate<0,22.503112,0> translate<43.897100,-1.535000,19.297100> }
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<44.041200,-1.535000,6.337400>}
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<44.246100,-1.535000,6.832100>}
box{<0,0,-0.203200><0.535455,0.035000,0.203200> rotate<0,-67.496677,0> translate<44.041200,-1.535000,6.337400> }
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<44.041200,-1.535000,7.862500>}
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<44.246100,-1.535000,7.367800>}
box{<0,0,-0.203200><0.535455,0.035000,0.203200> rotate<0,67.496677,0> translate<44.041200,-1.535000,7.862500> }
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<44.099600,-1.535000,7.721600>}
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<46.450500,-1.535000,7.721600>}
box{<0,0,-0.203200><2.350900,0.035000,0.203200> rotate<0,0.000000,0> translate<44.099600,-1.535000,7.721600> }
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<44.109500,-1.535000,6.502400>}
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<46.609500,-1.535000,6.502400>}
box{<0,0,-0.203200><2.500000,0.035000,0.203200> rotate<0,0.000000,0> translate<44.109500,-1.535000,6.502400> }
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<44.158500,-1.535000,16.011100>}
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<45.694200,-1.535000,16.011100>}
box{<0,0,-0.203200><1.535700,0.035000,0.203200> rotate<0,0.000000,0> translate<44.158500,-1.535000,16.011100> }
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<44.158500,-1.535000,19.188800>}
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<48.241400,-1.535000,19.188800>}
box{<0,0,-0.203200><4.082900,0.035000,0.203200> rotate<0,0.000000,0> translate<44.158500,-1.535000,19.188800> }
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<44.246100,-1.535000,6.832100>}
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<44.246100,-1.535000,7.367800>}
box{<0,0,-0.203200><0.535700,0.035000,0.203200> rotate<0,90.000000,0> translate<44.246100,-1.535000,7.367800> }
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<44.246100,-1.535000,6.908800>}
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<46.412900,-1.535000,6.908800>}
box{<0,0,-0.203200><2.166800,0.035000,0.203200> rotate<0,0.000000,0> translate<44.246100,-1.535000,6.908800> }
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<44.246100,-1.535000,7.315200>}
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<48.348500,-1.535000,7.315200>}
box{<0,0,-0.203200><4.102400,0.035000,0.203200> rotate<0,0.000000,0> translate<44.246100,-1.535000,7.315200> }
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<44.332100,-1.535000,7.853800>}
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<44.867800,-1.535000,7.853800>}
box{<0,0,-0.203200><0.535700,0.035000,0.203200> rotate<0,0.000000,0> translate<44.332100,-1.535000,7.853800> }
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<44.867800,-1.535000,7.853800>}
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<45.362500,-1.535000,8.058700>}
box{<0,0,-0.203200><0.535455,0.035000,0.203200> rotate<0,-22.497383,0> translate<44.867800,-1.535000,7.853800> }
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<45.362500,-1.535000,8.058700>}
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<45.741200,-1.535000,8.437400>}
box{<0,0,-0.203200><0.535563,0.035000,0.203200> rotate<0,-44.997030,0> translate<45.362500,-1.535000,8.058700> }
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<45.431800,-1.535000,8.128000>}
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<46.700000,-1.535000,8.128000>}
box{<0,0,-0.203200><1.268200,0.035000,0.203200> rotate<0,0.000000,0> translate<45.431800,-1.535000,8.128000> }
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<45.694200,-1.535000,16.011100>}
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<46.083300,-1.535000,16.370500>}
box{<0,0,-0.203200><0.529686,0.035000,0.203200> rotate<0,-42.724912,0> translate<45.694200,-1.535000,16.011100> }
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<45.741200,-1.535000,8.437400>}
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<45.946100,-1.535000,8.932100>}
box{<0,0,-0.203200><0.535455,0.035000,0.203200> rotate<0,-67.496677,0> translate<45.741200,-1.535000,8.437400> }
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<45.781300,-1.535000,8.534400>}
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<47.372100,-1.535000,8.534400>}
box{<0,0,-0.203200><1.590800,0.035000,0.203200> rotate<0,0.000000,0> translate<45.781300,-1.535000,8.534400> }
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<45.946100,-1.535000,8.932100>}
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<45.946100,-1.535000,9.213700>}
box{<0,0,-0.203200><0.281600,0.035000,0.203200> rotate<0,90.000000,0> translate<45.946100,-1.535000,9.213700> }
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<45.946100,-1.535000,8.940800>}
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<61.288800,-1.535000,8.940800>}
box{<0,0,-0.203200><15.342700,0.035000,0.203200> rotate<0,0.000000,0> translate<45.946100,-1.535000,8.940800> }
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<45.946100,-1.535000,9.213700>}
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<46.332100,-1.535000,9.053800>}
box{<0,0,-0.203200><0.417809,0.035000,0.203200> rotate<0,22.500234,0> translate<45.946100,-1.535000,9.213700> }
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<46.083300,-1.535000,16.370500>}
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<46.585800,-1.535000,16.555700>}
box{<0,0,-0.203200><0.535542,0.035000,0.203200> rotate<0,-20.230393,0> translate<46.083300,-1.535000,16.370500> }
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<46.246100,-1.535000,11.710400>}
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<46.246100,-1.535000,11.788800>}
box{<0,0,-0.203200><0.078400,0.035000,0.203200> rotate<0,90.000000,0> translate<46.246100,-1.535000,11.788800> }
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<46.246100,-1.535000,11.710400>}
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<46.332100,-1.535000,11.746100>}
box{<0,0,-0.203200><0.093115,0.035000,0.203200> rotate<0,-22.542645,0> translate<46.246100,-1.535000,11.710400> }
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<46.246100,-1.535000,11.785600>}
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<53.875800,-1.535000,11.785600>}
box{<0,0,-0.203200><7.629700,0.035000,0.203200> rotate<0,0.000000,0> translate<46.246100,-1.535000,11.785600> }
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<46.246100,-1.535000,11.788800>}
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<48.241400,-1.535000,11.788800>}
box{<0,0,-0.203200><1.995300,0.035000,0.203200> rotate<0,0.000000,0> translate<46.246100,-1.535000,11.788800> }
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<46.332100,-1.535000,9.053800>}
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<52.467800,-1.535000,9.053800>}
box{<0,0,-0.203200><6.135700,0.035000,0.203200> rotate<0,0.000000,0> translate<46.332100,-1.535000,9.053800> }
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<46.332100,-1.535000,11.746100>}
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<52.467800,-1.535000,11.746100>}
box{<0,0,-0.203200><6.135700,0.035000,0.203200> rotate<0,0.000000,0> translate<46.332100,-1.535000,11.746100> }
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<46.368000,-1.535000,7.017300>}
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<46.561300,-1.535000,6.550600>}
box{<0,0,-0.203200><0.505147,0.035000,0.203200> rotate<0,67.496957,0> translate<46.368000,-1.535000,7.017300> }
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<46.368000,-1.535000,7.219000>}
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<46.368000,-1.535000,7.017300>}
box{<0,0,-0.203200><0.201700,0.035000,0.203200> rotate<0,-90.000000,0> translate<46.368000,-1.535000,7.017300> }
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<46.368000,-1.535000,7.219000>}
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<48.348500,-1.535000,7.219000>}
box{<0,0,-0.203200><1.980500,0.035000,0.203200> rotate<0,0.000000,0> translate<46.368000,-1.535000,7.219000> }
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<46.368000,-1.535000,7.320800>}
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<48.348500,-1.535000,7.320800>}
box{<0,0,-0.203200><1.980500,0.035000,0.203200> rotate<0,0.000000,0> translate<46.368000,-1.535000,7.320800> }
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<46.368000,-1.535000,7.522600>}
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<46.368000,-1.535000,7.320800>}
box{<0,0,-0.203200><0.201800,0.035000,0.203200> rotate<0,-90.000000,0> translate<46.368000,-1.535000,7.320800> }
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<46.368000,-1.535000,7.522600>}
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<46.561300,-1.535000,7.989300>}
box{<0,0,-0.203200><0.505147,0.035000,0.203200> rotate<0,-67.496957,0> translate<46.368000,-1.535000,7.522600> }
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<46.510200,-1.535000,5.060300>}
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<46.647500,-1.535000,5.391800>}
box{<0,0,-0.203200><0.358809,0.035000,0.203200> rotate<0,-67.497285,0> translate<46.510200,-1.535000,5.060300> }
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<46.561300,-1.535000,6.550600>}
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<46.918600,-1.535000,6.193300>}
box{<0,0,-0.203200><0.505299,0.035000,0.203200> rotate<0,44.997030,0> translate<46.561300,-1.535000,6.550600> }
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<46.561300,-1.535000,7.989300>}
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<46.918600,-1.535000,8.346600>}
box{<0,0,-0.203200><0.505299,0.035000,0.203200> rotate<0,-44.997030,0> translate<46.561300,-1.535000,7.989300> }
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<46.585800,-1.535000,16.555700>}
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<47.121000,-1.535000,16.534400>}
box{<0,0,-0.203200><0.535624,0.035000,0.203200> rotate<0,2.278916,0> translate<46.585800,-1.535000,16.555700> }
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<46.634700,-1.535000,23.411100>}
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<46.701200,-1.535000,23.477500>}
box{<0,0,-0.203200><0.093975,0.035000,0.203200> rotate<0,-44.953921,0> translate<46.634700,-1.535000,23.411100> }
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<46.634700,-1.535000,23.411100>}
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<48.241400,-1.535000,23.411100>}
box{<0,0,-0.203200><1.606700,0.035000,0.203200> rotate<0,0.000000,0> translate<46.634700,-1.535000,23.411100> }
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<46.647500,-1.535000,5.391800>}
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<46.976100,-1.535000,5.720400>}
box{<0,0,-0.203200><0.464711,0.035000,0.203200> rotate<0,-44.997030,0> translate<46.647500,-1.535000,5.391800> }
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<46.701200,-1.535000,23.477500>}
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<46.768300,-1.535000,23.639600>}
box{<0,0,-0.203200><0.175439,0.035000,0.203200> rotate<0,-67.508826,0> translate<46.701200,-1.535000,23.477500> }
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<46.739900,-1.535000,23.571200>}
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<49.659900,-1.535000,23.571200>}
box{<0,0,-0.203200><2.920000,0.035000,0.203200> rotate<0,0.000000,0> translate<46.739900,-1.535000,23.571200> }
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<46.768300,-1.535000,23.639600>}
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<49.631600,-1.535000,23.639600>}
box{<0,0,-0.203200><2.863300,0.035000,0.203200> rotate<0,0.000000,0> translate<46.768300,-1.535000,23.639600> }
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<46.841600,-1.535000,3.739600>}
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<46.913600,-1.535000,3.726800>}
box{<0,0,-0.203200><0.073129,0.035000,0.203200> rotate<0,10.079933,0> translate<46.841600,-1.535000,3.739600> }
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<46.913600,-1.535000,3.726800>}
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<46.971700,-1.535000,3.739600>}
box{<0,0,-0.203200><0.059493,0.035000,0.203200> rotate<0,-12.423529,0> translate<46.913600,-1.535000,3.726800> }
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<46.918600,-1.535000,6.193300>}
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<47.385300,-1.535000,6.000000>}
box{<0,0,-0.203200><0.505147,0.035000,0.203200> rotate<0,22.497104,0> translate<46.918600,-1.535000,6.193300> }
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<46.918600,-1.535000,8.346600>}
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<47.385300,-1.535000,8.539900>}
box{<0,0,-0.203200><0.505147,0.035000,0.203200> rotate<0,-22.497104,0> translate<46.918600,-1.535000,8.346600> }
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<46.971700,-1.535000,3.739600>}
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<46.976100,-1.535000,3.739600>}
box{<0,0,-0.203200><0.004400,0.035000,0.203200> rotate<0,0.000000,0> translate<46.971700,-1.535000,3.739600> }
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<46.976100,-1.535000,3.739500>}
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<47.405500,-1.535000,3.561600>}
box{<0,0,-0.203200><0.464793,0.035000,0.203200> rotate<0,22.502694,0> translate<46.976100,-1.535000,3.739500> }
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<46.976100,-1.535000,3.739600>}
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<46.976100,-1.535000,3.739500>}
box{<0,0,-0.203200><0.000100,0.035000,0.203200> rotate<0,-90.000000,0> translate<46.976100,-1.535000,3.739500> }
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<46.976100,-1.535000,5.720400>}
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<47.405500,-1.535000,5.898300>}
box{<0,0,-0.203200><0.464793,0.035000,0.203200> rotate<0,-22.502694,0> translate<46.976100,-1.535000,5.720400> }
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<47.121000,-1.535000,16.534400>}
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<47.607200,-1.535000,16.310000>}
box{<0,0,-0.203200><0.535487,0.035000,0.203200> rotate<0,24.773506,0> translate<47.121000,-1.535000,16.534400> }
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<47.385300,-1.535000,6.000000>}
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<48.349000,-1.535000,6.000000>}
box{<0,0,-0.203200><0.963700,0.035000,0.203200> rotate<0,0.000000,0> translate<47.385300,-1.535000,6.000000> }
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<47.385300,-1.535000,8.539900>}
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<48.349000,-1.535000,8.539900>}
box{<0,0,-0.203200><0.963700,0.035000,0.203200> rotate<0,0.000000,0> translate<47.385300,-1.535000,8.539900> }
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<47.405500,-1.535000,3.561600>}
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<49.394400,-1.535000,3.561600>}
box{<0,0,-0.203200><1.988900,0.035000,0.203200> rotate<0,0.000000,0> translate<47.405500,-1.535000,3.561600> }
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<47.405500,-1.535000,5.898300>}
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<49.394400,-1.535000,5.898300>}
box{<0,0,-0.203200><1.988900,0.035000,0.203200> rotate<0,0.000000,0> translate<47.405500,-1.535000,5.898300> }
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<47.607200,-1.535000,16.310000>}
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<47.883200,-1.535000,16.011100>}
box{<0,0,-0.203200><0.406838,0.035000,0.203200> rotate<0,47.277937,0> translate<47.607200,-1.535000,16.310000> }
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<47.657100,-1.535000,16.256000>}
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<49.814600,-1.535000,16.256000>}
box{<0,0,-0.203200><2.157500,0.035000,0.203200> rotate<0,0.000000,0> translate<47.657100,-1.535000,16.256000> }
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<47.883200,-1.535000,16.011100>}
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<48.241400,-1.535000,16.011100>}
box{<0,0,-0.203200><0.358200,0.035000,0.203200> rotate<0,0.000000,0> translate<47.883200,-1.535000,16.011100> }
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<48.241400,-1.535000,11.788800>}
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<48.502800,-1.535000,11.897100>}
box{<0,0,-0.203200><0.282947,0.035000,0.203200> rotate<0,-22.503112,0> translate<48.241400,-1.535000,11.788800> }
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<48.241400,-1.535000,16.011100>}
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<48.502800,-1.535000,15.902800>}
box{<0,0,-0.203200><0.282947,0.035000,0.203200> rotate<0,22.503112,0> translate<48.241400,-1.535000,16.011100> }
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<48.241400,-1.535000,19.188800>}
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<48.502800,-1.535000,19.297100>}
box{<0,0,-0.203200><0.282947,0.035000,0.203200> rotate<0,-22.503112,0> translate<48.241400,-1.535000,19.188800> }
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<48.241400,-1.535000,23.411100>}
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<48.502800,-1.535000,23.302800>}
box{<0,0,-0.203200><0.282947,0.035000,0.203200> rotate<0,22.503112,0> translate<48.241400,-1.535000,23.411100> }
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<48.348500,-1.535000,7.320800>}
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<48.348500,-1.535000,7.219000>}
box{<0,0,-0.203200><0.101800,0.035000,0.203200> rotate<0,-90.000000,0> translate<48.348500,-1.535000,7.219000> }
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<48.349000,-1.535000,6.000000>}
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<48.349000,-1.535000,7.218500>}
box{<0,0,-0.203200><1.218500,0.035000,0.203200> rotate<0,90.000000,0> translate<48.349000,-1.535000,7.218500> }
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<48.349000,-1.535000,6.096000>}
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<48.450800,-1.535000,6.096000>}
box{<0,0,-0.203200><0.101800,0.035000,0.203200> rotate<0,0.000000,0> translate<48.349000,-1.535000,6.096000> }
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<48.349000,-1.535000,6.502400>}
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<48.450800,-1.535000,6.502400>}
box{<0,0,-0.203200><0.101800,0.035000,0.203200> rotate<0,0.000000,0> translate<48.349000,-1.535000,6.502400> }
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<48.349000,-1.535000,6.908800>}
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<48.450800,-1.535000,6.908800>}
box{<0,0,-0.203200><0.101800,0.035000,0.203200> rotate<0,0.000000,0> translate<48.349000,-1.535000,6.908800> }
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<48.349000,-1.535000,7.218500>}
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<48.450800,-1.535000,7.218500>}
box{<0,0,-0.203200><0.101800,0.035000,0.203200> rotate<0,0.000000,0> translate<48.349000,-1.535000,7.218500> }
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<48.349000,-1.535000,7.321400>}
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<48.349000,-1.535000,8.539900>}
box{<0,0,-0.203200><1.218500,0.035000,0.203200> rotate<0,90.000000,0> translate<48.349000,-1.535000,8.539900> }
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<48.349000,-1.535000,7.321400>}
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<48.450800,-1.535000,7.321400>}
box{<0,0,-0.203200><0.101800,0.035000,0.203200> rotate<0,0.000000,0> translate<48.349000,-1.535000,7.321400> }
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<48.349000,-1.535000,7.721600>}
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<48.450800,-1.535000,7.721600>}
box{<0,0,-0.203200><0.101800,0.035000,0.203200> rotate<0,0.000000,0> translate<48.349000,-1.535000,7.721600> }
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<48.349000,-1.535000,8.128000>}
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<48.450800,-1.535000,8.128000>}
box{<0,0,-0.203200><0.101800,0.035000,0.203200> rotate<0,0.000000,0> translate<48.349000,-1.535000,8.128000> }
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<48.349000,-1.535000,8.534400>}
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<48.450800,-1.535000,8.534400>}
box{<0,0,-0.203200><0.101800,0.035000,0.203200> rotate<0,0.000000,0> translate<48.349000,-1.535000,8.534400> }
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<48.450800,-1.535000,6.000000>}
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<49.414600,-1.535000,6.000000>}
box{<0,0,-0.203200><0.963800,0.035000,0.203200> rotate<0,0.000000,0> translate<48.450800,-1.535000,6.000000> }
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<48.450800,-1.535000,7.218500>}
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<48.450800,-1.535000,6.000000>}
box{<0,0,-0.203200><1.218500,0.035000,0.203200> rotate<0,-90.000000,0> translate<48.450800,-1.535000,6.000000> }
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<48.450800,-1.535000,8.539900>}
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<48.450800,-1.535000,7.321400>}
box{<0,0,-0.203200><1.218500,0.035000,0.203200> rotate<0,-90.000000,0> translate<48.450800,-1.535000,7.321400> }
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<48.450800,-1.535000,8.539900>}
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<49.414600,-1.535000,8.539900>}
box{<0,0,-0.203200><0.963800,0.035000,0.203200> rotate<0,0.000000,0> translate<48.450800,-1.535000,8.539900> }
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<48.502800,-1.535000,11.897100>}
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<48.702800,-1.535000,12.097100>}
box{<0,0,-0.203200><0.282843,0.035000,0.203200> rotate<0,-44.997030,0> translate<48.502800,-1.535000,11.897100> }
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<48.502800,-1.535000,15.902800>}
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<48.702800,-1.535000,15.702800>}
box{<0,0,-0.203200><0.282843,0.035000,0.203200> rotate<0,44.997030,0> translate<48.502800,-1.535000,15.902800> }
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<48.502800,-1.535000,19.297100>}
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<48.702800,-1.535000,19.497100>}
box{<0,0,-0.203200><0.282843,0.035000,0.203200> rotate<0,-44.997030,0> translate<48.502800,-1.535000,19.297100> }
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<48.502800,-1.535000,23.302800>}
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<48.702800,-1.535000,23.102800>}
box{<0,0,-0.203200><0.282843,0.035000,0.203200> rotate<0,44.997030,0> translate<48.502800,-1.535000,23.302800> }
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<48.556000,-1.535000,15.849600>}
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<49.245900,-1.535000,15.849600>}
box{<0,0,-0.203200><0.689900,0.035000,0.203200> rotate<0,0.000000,0> translate<48.556000,-1.535000,15.849600> }
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<48.640800,-1.535000,23.164800>}
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<50.011400,-1.535000,23.164800>}
box{<0,0,-0.203200><1.370600,0.035000,0.203200> rotate<0,0.000000,0> translate<48.640800,-1.535000,23.164800> }
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<48.702800,-1.535000,12.097100>}
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<48.811100,-1.535000,12.358500>}
box{<0,0,-0.203200><0.282947,0.035000,0.203200> rotate<0,-67.490948,0> translate<48.702800,-1.535000,12.097100> }
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<48.702800,-1.535000,15.702800>}
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<48.811100,-1.535000,15.441400>}
box{<0,0,-0.203200><0.282947,0.035000,0.203200> rotate<0,67.490948,0> translate<48.702800,-1.535000,15.702800> }
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<48.702800,-1.535000,19.497100>}
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<48.811100,-1.535000,19.758500>}
box{<0,0,-0.203200><0.282947,0.035000,0.203200> rotate<0,-67.490948,0> translate<48.702800,-1.535000,19.497100> }
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<48.702800,-1.535000,23.102800>}
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<48.811100,-1.535000,22.841400>}
box{<0,0,-0.203200><0.282947,0.035000,0.203200> rotate<0,67.490948,0> translate<48.702800,-1.535000,23.102800> }
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<48.706900,-1.535000,19.507200>}
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<53.793600,-1.535000,19.507200>}
box{<0,0,-0.203200><5.086700,0.035000,0.203200> rotate<0,0.000000,0> translate<48.706900,-1.535000,19.507200> }
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<48.742100,-1.535000,12.192000>}
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<54.048200,-1.535000,12.192000>}
box{<0,0,-0.203200><5.306100,0.035000,0.203200> rotate<0,0.000000,0> translate<48.742100,-1.535000,12.192000> }
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<48.810400,-1.535000,15.443200>}
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<48.839500,-1.535000,15.443200>}
box{<0,0,-0.203200><0.029100,0.035000,0.203200> rotate<0,0.000000,0> translate<48.810400,-1.535000,15.443200> }
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<48.811100,-1.535000,12.358500>}
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<48.811100,-1.535000,12.553800>}
box{<0,0,-0.203200><0.195300,0.035000,0.203200> rotate<0,90.000000,0> translate<48.811100,-1.535000,12.553800> }
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<48.811100,-1.535000,12.553800>}
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<48.932100,-1.535000,12.553800>}
box{<0,0,-0.203200><0.121000,0.035000,0.203200> rotate<0,0.000000,0> translate<48.811100,-1.535000,12.553800> }
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<48.811100,-1.535000,15.414800>}
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<48.811100,-1.535000,15.441400>}
box{<0,0,-0.203200><0.026600,0.035000,0.203200> rotate<0,90.000000,0> translate<48.811100,-1.535000,15.441400> }
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<48.811100,-1.535000,15.414800>}
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<49.537400,-1.535000,16.141200>}
box{<0,0,-0.203200><1.027214,0.035000,0.203200> rotate<0,-45.000974,0> translate<48.811100,-1.535000,15.414800> }
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<48.811100,-1.535000,19.758500>}
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<48.811100,-1.535000,19.953800>}
box{<0,0,-0.203200><0.195300,0.035000,0.203200> rotate<0,90.000000,0> translate<48.811100,-1.535000,19.953800> }
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<48.811100,-1.535000,19.913600>}
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<53.962000,-1.535000,19.913600>}
box{<0,0,-0.203200><5.150900,0.035000,0.203200> rotate<0,0.000000,0> translate<48.811100,-1.535000,19.913600> }
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<48.811100,-1.535000,19.953800>}
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<51.967800,-1.535000,19.953800>}
box{<0,0,-0.203200><3.156700,0.035000,0.203200> rotate<0,0.000000,0> translate<48.811100,-1.535000,19.953800> }
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<48.811100,-1.535000,22.646100>}
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<48.811100,-1.535000,22.841400>}
box{<0,0,-0.203200><0.195300,0.035000,0.203200> rotate<0,90.000000,0> translate<48.811100,-1.535000,22.841400> }
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<48.811100,-1.535000,22.646100>}
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<51.967800,-1.535000,22.646100>}
box{<0,0,-0.203200><3.156700,0.035000,0.203200> rotate<0,0.000000,0> translate<48.811100,-1.535000,22.646100> }
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<48.811100,-1.535000,22.758400>}
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<53.738800,-1.535000,22.758400>}
box{<0,0,-0.203200><4.927700,0.035000,0.203200> rotate<0,0.000000,0> translate<48.811100,-1.535000,22.758400> }
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<48.932100,-1.535000,12.553800>}
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<49.467800,-1.535000,12.553800>}
box{<0,0,-0.203200><0.535700,0.035000,0.203200> rotate<0,0.000000,0> translate<48.932100,-1.535000,12.553800> }
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<49.394400,-1.535000,3.561600>}
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<49.823800,-1.535000,3.739500>}
box{<0,0,-0.203200><0.464793,0.035000,0.203200> rotate<0,-22.502694,0> translate<49.394400,-1.535000,3.561600> }
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<49.394400,-1.535000,5.898300>}
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<49.823800,-1.535000,5.720400>}
box{<0,0,-0.203200><0.464793,0.035000,0.203200> rotate<0,22.502694,0> translate<49.394400,-1.535000,5.898300> }
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<49.414600,-1.535000,6.000000>}
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<49.881300,-1.535000,6.193300>}
box{<0,0,-0.203200><0.505147,0.035000,0.203200> rotate<0,-22.497104,0> translate<49.414600,-1.535000,6.000000> }
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<49.414600,-1.535000,8.539900>}
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<49.881300,-1.535000,8.346600>}
box{<0,0,-0.203200><0.505147,0.035000,0.203200> rotate<0,22.497104,0> translate<49.414600,-1.535000,8.539900> }
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<49.427900,-1.535000,8.534400>}
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<50.753000,-1.535000,8.534400>}
box{<0,0,-0.203200><1.325100,0.035000,0.203200> rotate<0,0.000000,0> translate<49.427900,-1.535000,8.534400> }
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<49.467800,-1.535000,12.553800>}
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<49.962500,-1.535000,12.758700>}
box{<0,0,-0.203200><0.535455,0.035000,0.203200> rotate<0,-22.497383,0> translate<49.467800,-1.535000,12.553800> }
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<49.537400,-1.535000,16.141200>}
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<50.032100,-1.535000,16.346100>}
box{<0,0,-0.203200><0.535455,0.035000,0.203200> rotate<0,-22.497383,0> translate<49.537400,-1.535000,16.141200> }
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<49.575400,-1.535000,12.598400>}
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<54.454600,-1.535000,12.598400>}
box{<0,0,-0.203200><4.879200,0.035000,0.203200> rotate<0,0.000000,0> translate<49.575400,-1.535000,12.598400> }
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<49.626100,-1.535000,3.657600>}
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<58.384100,-1.535000,3.657600>}
box{<0,0,-0.203200><8.758000,0.035000,0.203200> rotate<0,0.000000,0> translate<49.626100,-1.535000,3.657600> }
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<49.631600,-1.535000,23.639600>}
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<49.698700,-1.535000,23.477500>}
box{<0,0,-0.203200><0.175439,0.035000,0.203200> rotate<0,67.508826,0> translate<49.631600,-1.535000,23.639600> }
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<49.646300,-1.535000,6.096000>}
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<61.288800,-1.535000,6.096000>}
box{<0,0,-0.203200><11.642500,0.035000,0.203200> rotate<0,0.000000,0> translate<49.646300,-1.535000,6.096000> }
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<49.698700,-1.535000,23.477500>}
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<50.027300,-1.535000,23.148900>}
box{<0,0,-0.203200><0.464711,0.035000,0.203200> rotate<0,44.997030,0> translate<49.698700,-1.535000,23.477500> }
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<49.823800,-1.535000,3.739500>}
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<50.152400,-1.535000,4.068100>}
box{<0,0,-0.203200><0.464711,0.035000,0.203200> rotate<0,-44.997030,0> translate<49.823800,-1.535000,3.739500> }
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<49.823800,-1.535000,5.720400>}
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<50.152400,-1.535000,5.391800>}
box{<0,0,-0.203200><0.464711,0.035000,0.203200> rotate<0,44.997030,0> translate<49.823800,-1.535000,5.720400> }
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<49.854600,-1.535000,5.689600>}
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<61.288800,-1.535000,5.689600>}
box{<0,0,-0.203200><11.434200,0.035000,0.203200> rotate<0,0.000000,0> translate<49.854600,-1.535000,5.689600> }
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<49.881300,-1.535000,6.193300>}
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<50.238600,-1.535000,6.550600>}
box{<0,0,-0.203200><0.505299,0.035000,0.203200> rotate<0,-44.997030,0> translate<49.881300,-1.535000,6.193300> }
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<49.881300,-1.535000,8.346600>}
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<50.157900,-1.535000,8.069900>}
box{<0,0,-0.203200><0.391242,0.035000,0.203200> rotate<0,45.007385,0> translate<49.881300,-1.535000,8.346600> }
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<49.962500,-1.535000,12.758700>}
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<50.857600,-1.535000,13.653800>}
box{<0,0,-0.203200><1.265863,0.035000,0.203200> rotate<0,-44.997030,0> translate<49.962500,-1.535000,12.758700> }
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<50.027300,-1.535000,23.148900>}
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<50.456700,-1.535000,22.971000>}
box{<0,0,-0.203200><0.464793,0.035000,0.203200> rotate<0,22.502694,0> translate<50.027300,-1.535000,23.148900> }
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<50.032100,-1.535000,16.346100>}
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<50.567800,-1.535000,16.346100>}
box{<0,0,-0.203200><0.535700,0.035000,0.203200> rotate<0,0.000000,0> translate<50.032100,-1.535000,16.346100> }
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<50.099900,-1.535000,8.128000>}
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<50.203800,-1.535000,8.128000>}
box{<0,0,-0.203200><0.103900,0.035000,0.203200> rotate<0,0.000000,0> translate<50.099900,-1.535000,8.128000> }
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<50.148300,-1.535000,4.064000>}
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<58.931600,-1.535000,4.064000>}
box{<0,0,-0.203200><8.783300,0.035000,0.203200> rotate<0,0.000000,0> translate<50.148300,-1.535000,4.064000> }
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<50.152400,-1.535000,4.068100>}
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<50.330300,-1.535000,4.497500>}
box{<0,0,-0.203200><0.464793,0.035000,0.203200> rotate<0,-67.491366,0> translate<50.152400,-1.535000,4.068100> }
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<50.152400,-1.535000,5.391800>}
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<50.330300,-1.535000,4.962400>}
box{<0,0,-0.203200><0.464793,0.035000,0.203200> rotate<0,67.491366,0> translate<50.152400,-1.535000,5.391800> }
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<50.157900,-1.535000,8.069900>}
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<50.166400,-1.535000,8.090600>}
box{<0,0,-0.203200><0.022377,0.035000,0.203200> rotate<0,-67.671107,0> translate<50.157900,-1.535000,8.069900> }
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<50.166400,-1.535000,8.090600>}
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<50.509300,-1.535000,8.433500>}
box{<0,0,-0.203200><0.484934,0.035000,0.203200> rotate<0,-44.997030,0> translate<50.166400,-1.535000,8.090600> }
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<50.190400,-1.535000,6.502400>}
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<50.373300,-1.535000,6.502400>}
box{<0,0,-0.203200><0.182900,0.035000,0.203200> rotate<0,0.000000,0> translate<50.190400,-1.535000,6.502400> }
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<50.197400,-1.535000,5.283200>}
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<61.288800,-1.535000,5.283200>}
box{<0,0,-0.203200><11.091400,0.035000,0.203200> rotate<0,0.000000,0> translate<50.197400,-1.535000,5.283200> }
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<50.208600,-1.535000,13.004800>}
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<55.230400,-1.535000,13.004800>}
box{<0,0,-0.203200><5.021800,0.035000,0.203200> rotate<0,0.000000,0> translate<50.208600,-1.535000,13.004800> }
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<50.238600,-1.535000,6.550600>}
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<50.263900,-1.535000,6.611900>}
box{<0,0,-0.203200><0.066316,0.035000,0.203200> rotate<0,-67.568411,0> translate<50.238600,-1.535000,6.550600> }
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<50.263900,-1.535000,6.611900>}
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<50.509300,-1.535000,6.366400>}
box{<0,0,-0.203200><0.347119,0.035000,0.203200> rotate<0,45.008701,0> translate<50.263900,-1.535000,6.611900> }
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<50.319000,-1.535000,4.470400>}
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<61.288800,-1.535000,4.470400>}
box{<0,0,-0.203200><10.969800,0.035000,0.203200> rotate<0,0.000000,0> translate<50.319000,-1.535000,4.470400> }
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<50.330300,-1.535000,4.497500>}
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<50.330300,-1.535000,4.962400>}
box{<0,0,-0.203200><0.464900,0.035000,0.203200> rotate<0,90.000000,0> translate<50.330300,-1.535000,4.962400> }
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<50.330300,-1.535000,4.876800>}
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<61.288800,-1.535000,4.876800>}
box{<0,0,-0.203200><10.958500,0.035000,0.203200> rotate<0,0.000000,0> translate<50.330300,-1.535000,4.876800> }
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<50.456700,-1.535000,22.971000>}
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<52.445600,-1.535000,22.971000>}
box{<0,0,-0.203200><1.988900,0.035000,0.203200> rotate<0,0.000000,0> translate<50.456700,-1.535000,22.971000> }
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<50.509300,-1.535000,6.366400>}
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<50.957400,-1.535000,6.180800>}
box{<0,0,-0.203200><0.485016,0.035000,0.203200> rotate<0,22.497522,0> translate<50.509300,-1.535000,6.366400> }
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<50.509300,-1.535000,8.433500>}
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<50.957400,-1.535000,8.619100>}
box{<0,0,-0.203200><0.485016,0.035000,0.203200> rotate<0,-22.497522,0> translate<50.509300,-1.535000,8.433500> }
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<50.567800,-1.535000,16.346100>}
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<51.503800,-1.535000,16.346100>}
box{<0,0,-0.203200><0.936000,0.035000,0.203200> rotate<0,0.000000,0> translate<50.567800,-1.535000,16.346100> }
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<50.615000,-1.535000,13.411200>}
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<54.524900,-1.535000,13.411200>}
box{<0,0,-0.203200><3.909900,0.035000,0.203200> rotate<0,0.000000,0> translate<50.615000,-1.535000,13.411200> }
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<50.857600,-1.535000,13.653800>}
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<52.067800,-1.535000,13.653800>}
box{<0,0,-0.203200><1.210200,0.035000,0.203200> rotate<0,0.000000,0> translate<50.857600,-1.535000,13.653800> }
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<50.957400,-1.535000,6.180800>}
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<51.442500,-1.535000,6.180800>}
box{<0,0,-0.203200><0.485100,0.035000,0.203200> rotate<0,0.000000,0> translate<50.957400,-1.535000,6.180800> }
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<50.957400,-1.535000,8.619100>}
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<51.442500,-1.535000,8.619100>}
box{<0,0,-0.203200><0.485100,0.035000,0.203200> rotate<0,0.000000,0> translate<50.957400,-1.535000,8.619100> }
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<51.442500,-1.535000,6.180800>}
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<51.890600,-1.535000,6.366400>}
box{<0,0,-0.203200><0.485016,0.035000,0.203200> rotate<0,-22.497522,0> translate<51.442500,-1.535000,6.180800> }
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<51.442500,-1.535000,8.619100>}
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<51.890600,-1.535000,8.433500>}
box{<0,0,-0.203200><0.485016,0.035000,0.203200> rotate<0,22.497522,0> translate<51.442500,-1.535000,8.619100> }
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<51.503800,-1.535000,16.346100>}
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<51.552100,-1.535000,16.366100>}
box{<0,0,-0.203200><0.052277,0.035000,0.203200> rotate<0,-22.491919,0> translate<51.503800,-1.535000,16.346100> }
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<51.552100,-1.535000,16.366100>}
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<54.262300,-1.535000,16.366100>}
box{<0,0,-0.203200><2.710200,0.035000,0.203200> rotate<0,0.000000,0> translate<51.552100,-1.535000,16.366100> }
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<51.647000,-1.535000,8.534400>}
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<61.288800,-1.535000,8.534400>}
box{<0,0,-0.203200><9.641800,0.035000,0.203200> rotate<0,0.000000,0> translate<51.647000,-1.535000,8.534400> }
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<51.890600,-1.535000,6.366400>}
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<52.233500,-1.535000,6.709300>}
box{<0,0,-0.203200><0.484934,0.035000,0.203200> rotate<0,-44.997030,0> translate<51.890600,-1.535000,6.366400> }
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<51.890600,-1.535000,8.433500>}
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<52.233500,-1.535000,8.090600>}
box{<0,0,-0.203200><0.484934,0.035000,0.203200> rotate<0,44.997030,0> translate<51.890600,-1.535000,8.433500> }
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<51.967800,-1.535000,19.953800>}
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<52.462500,-1.535000,20.158700>}
box{<0,0,-0.203200><0.535455,0.035000,0.203200> rotate<0,-22.497383,0> translate<51.967800,-1.535000,19.953800> }
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<51.967800,-1.535000,22.646100>}
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<52.462500,-1.535000,22.441200>}
box{<0,0,-0.203200><0.535455,0.035000,0.203200> rotate<0,22.497383,0> translate<51.967800,-1.535000,22.646100> }
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<52.026600,-1.535000,6.502400>}
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<61.288800,-1.535000,6.502400>}
box{<0,0,-0.203200><9.262200,0.035000,0.203200> rotate<0,0.000000,0> translate<52.026600,-1.535000,6.502400> }
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<52.067800,-1.535000,13.653800>}
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<52.116000,-1.535000,13.673800>}
box{<0,0,-0.203200><0.052185,0.035000,0.203200> rotate<0,-22.533921,0> translate<52.067800,-1.535000,13.653800> }
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<52.116000,-1.535000,13.673800>}
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<54.262300,-1.535000,13.673800>}
box{<0,0,-0.203200><2.146300,0.035000,0.203200> rotate<0,0.000000,0> translate<52.116000,-1.535000,13.673800> }
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<52.196100,-1.535000,8.128000>}
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<61.288800,-1.535000,8.128000>}
box{<0,0,-0.203200><9.092700,0.035000,0.203200> rotate<0,0.000000,0> translate<52.196100,-1.535000,8.128000> }
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<52.233500,-1.535000,6.709300>}
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<52.419100,-1.535000,7.157400>}
box{<0,0,-0.203200><0.485016,0.035000,0.203200> rotate<0,-67.496538,0> translate<52.233500,-1.535000,6.709300> }
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<52.233500,-1.535000,8.090600>}
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<52.419100,-1.535000,7.642500>}
box{<0,0,-0.203200><0.485016,0.035000,0.203200> rotate<0,67.496538,0> translate<52.233500,-1.535000,8.090600> }
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<52.316100,-1.535000,6.908800>}
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<61.288800,-1.535000,6.908800>}
box{<0,0,-0.203200><8.972700,0.035000,0.203200> rotate<0,0.000000,0> translate<52.316100,-1.535000,6.908800> }
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<52.386400,-1.535000,7.721600>}
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<61.288800,-1.535000,7.721600>}
box{<0,0,-0.203200><8.902400,0.035000,0.203200> rotate<0,0.000000,0> translate<52.386400,-1.535000,7.721600> }
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<52.419100,-1.535000,7.157400>}
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<52.419100,-1.535000,7.642500>}
box{<0,0,-0.203200><0.485100,0.035000,0.203200> rotate<0,90.000000,0> translate<52.419100,-1.535000,7.642500> }
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<52.419100,-1.535000,7.315200>}
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<61.288800,-1.535000,7.315200>}
box{<0,0,-0.203200><8.869700,0.035000,0.203200> rotate<0,0.000000,0> translate<52.419100,-1.535000,7.315200> }
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<52.445600,-1.535000,22.971000>}
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<52.875000,-1.535000,23.148900>}
box{<0,0,-0.203200><0.464793,0.035000,0.203200> rotate<0,-22.502694,0> translate<52.445600,-1.535000,22.971000> }
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<52.462500,-1.535000,20.158700>}
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<52.841200,-1.535000,20.537400>}
box{<0,0,-0.203200><0.535563,0.035000,0.203200> rotate<0,-44.997030,0> translate<52.462500,-1.535000,20.158700> }
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<52.462500,-1.535000,22.441200>}
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<52.841200,-1.535000,22.062500>}
box{<0,0,-0.203200><0.535563,0.035000,0.203200> rotate<0,44.997030,0> translate<52.462500,-1.535000,22.441200> }
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<52.467800,-1.535000,9.053800>}
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<52.962500,-1.535000,9.258700>}
box{<0,0,-0.203200><0.535455,0.035000,0.203200> rotate<0,-22.497383,0> translate<52.467800,-1.535000,9.053800> }
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<52.467800,-1.535000,11.746100>}
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<52.962500,-1.535000,11.541200>}
box{<0,0,-0.203200><0.535455,0.035000,0.203200> rotate<0,22.497383,0> translate<52.467800,-1.535000,11.746100> }
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<52.551700,-1.535000,22.352000>}
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<53.818600,-1.535000,22.352000>}
box{<0,0,-0.203200><1.266900,0.035000,0.203200> rotate<0,0.000000,0> translate<52.551700,-1.535000,22.352000> }
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<52.623800,-1.535000,20.320000>}
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<54.256200,-1.535000,20.320000>}
box{<0,0,-0.203200><1.632400,0.035000,0.203200> rotate<0,0.000000,0> translate<52.623800,-1.535000,20.320000> }
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<52.841200,-1.535000,20.537400>}
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<53.046100,-1.535000,21.032100>}
box{<0,0,-0.203200><0.535455,0.035000,0.203200> rotate<0,-67.496677,0> translate<52.841200,-1.535000,20.537400> }
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<52.841200,-1.535000,22.062500>}
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<53.046100,-1.535000,21.567800>}
box{<0,0,-0.203200><0.535455,0.035000,0.203200> rotate<0,67.496677,0> translate<52.841200,-1.535000,22.062500> }
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<52.875000,-1.535000,23.148900>}
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<53.203600,-1.535000,23.477500>}
box{<0,0,-0.203200><0.464711,0.035000,0.203200> rotate<0,-44.997030,0> translate<52.875000,-1.535000,23.148900> }
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<52.889700,-1.535000,21.945600>}
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<53.987000,-1.535000,21.945600>}
box{<0,0,-0.203200><1.097300,0.035000,0.203200> rotate<0,0.000000,0> translate<52.889700,-1.535000,21.945600> }
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<52.890900,-1.535000,23.164800>}
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<53.738800,-1.535000,23.164800>}
box{<0,0,-0.203200><0.847900,0.035000,0.203200> rotate<0,0.000000,0> translate<52.890900,-1.535000,23.164800> }
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<52.919400,-1.535000,20.726400>}
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<54.751300,-1.535000,20.726400>}
box{<0,0,-0.203200><1.831900,0.035000,0.203200> rotate<0,0.000000,0> translate<52.919400,-1.535000,20.726400> }
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<52.962500,-1.535000,9.258700>}
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<53.341200,-1.535000,9.637400>}
box{<0,0,-0.203200><0.535563,0.035000,0.203200> rotate<0,-44.997030,0> translate<52.962500,-1.535000,9.258700> }
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<52.962500,-1.535000,11.541200>}
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<53.341200,-1.535000,11.162500>}
box{<0,0,-0.203200><0.535563,0.035000,0.203200> rotate<0,44.997030,0> translate<52.962500,-1.535000,11.541200> }
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<53.046100,-1.535000,21.032100>}
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<53.046100,-1.535000,21.567800>}
box{<0,0,-0.203200><0.535700,0.035000,0.203200> rotate<0,90.000000,0> translate<53.046100,-1.535000,21.567800> }
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<53.046100,-1.535000,21.132800>}
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<54.897700,-1.535000,21.132800>}
box{<0,0,-0.203200><1.851600,0.035000,0.203200> rotate<0,0.000000,0> translate<53.046100,-1.535000,21.132800> }
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<53.046100,-1.535000,21.539200>}
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<54.316900,-1.535000,21.539200>}
box{<0,0,-0.203200><1.270800,0.035000,0.203200> rotate<0,0.000000,0> translate<53.046100,-1.535000,21.539200> }
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<53.051000,-1.535000,9.347200>}
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<54.669800,-1.535000,9.347200>}
box{<0,0,-0.203200><1.618800,0.035000,0.203200> rotate<0,0.000000,0> translate<53.051000,-1.535000,9.347200> }
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<53.124500,-1.535000,11.379200>}
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<53.738800,-1.535000,11.379200>}
box{<0,0,-0.203200><0.614300,0.035000,0.203200> rotate<0,0.000000,0> translate<53.124500,-1.535000,11.379200> }
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<53.138100,-1.535000,24.866600>}
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<53.203600,-1.535000,24.801200>}
box{<0,0,-0.203200><0.092560,0.035000,0.203200> rotate<0,44.953262,0> translate<53.138100,-1.535000,24.866600> }
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<53.138100,-1.535000,24.866600>}
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<54.679000,-1.535000,24.866600>}
box{<0,0,-0.203200><1.540900,0.035000,0.203200> rotate<0,0.000000,0> translate<53.138100,-1.535000,24.866600> }
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<53.203600,-1.535000,23.477500>}
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<53.381500,-1.535000,23.906900>}
box{<0,0,-0.203200><0.464793,0.035000,0.203200> rotate<0,-67.491366,0> translate<53.203600,-1.535000,23.477500> }
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<53.203600,-1.535000,24.801200>}
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<53.381500,-1.535000,24.371800>}
box{<0,0,-0.203200><0.464793,0.035000,0.203200> rotate<0,67.491366,0> translate<53.203600,-1.535000,24.801200> }
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<53.208100,-1.535000,24.790400>}
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<55.002300,-1.535000,24.790400>}
box{<0,0,-0.203200><1.794200,0.035000,0.203200> rotate<0,0.000000,0> translate<53.208100,-1.535000,24.790400> }
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<53.242400,-1.535000,23.571200>}
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<53.836700,-1.535000,23.571200>}
box{<0,0,-0.203200><0.594300,0.035000,0.203200> rotate<0,0.000000,0> translate<53.242400,-1.535000,23.571200> }
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<53.341200,-1.535000,9.637400>}
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<53.546100,-1.535000,10.132100>}
box{<0,0,-0.203200><0.535455,0.035000,0.203200> rotate<0,-67.496677,0> translate<53.341200,-1.535000,9.637400> }
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<53.341200,-1.535000,11.162500>}
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<53.546100,-1.535000,10.667800>}
box{<0,0,-0.203200><0.535455,0.035000,0.203200> rotate<0,67.496677,0> translate<53.341200,-1.535000,11.162500> }
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<53.376500,-1.535000,24.384000>}
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<54.360200,-1.535000,24.384000>}
box{<0,0,-0.203200><0.983700,0.035000,0.203200> rotate<0,0.000000,0> translate<53.376500,-1.535000,24.384000> }
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<53.381500,-1.535000,23.906900>}
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<53.381500,-1.535000,24.371800>}
box{<0,0,-0.203200><0.464900,0.035000,0.203200> rotate<0,90.000000,0> translate<53.381500,-1.535000,24.371800> }
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<53.381500,-1.535000,23.977600>}
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<54.005000,-1.535000,23.977600>}
box{<0,0,-0.203200><0.623500,0.035000,0.203200> rotate<0,0.000000,0> translate<53.381500,-1.535000,23.977600> }
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<53.389300,-1.535000,9.753600>}
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<54.222500,-1.535000,9.753600>}
box{<0,0,-0.203200><0.833200,0.035000,0.203200> rotate<0,0.000000,0> translate<53.389300,-1.535000,9.753600> }
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<53.419800,-1.535000,10.972800>}
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<53.738800,-1.535000,10.972800>}
box{<0,0,-0.203200><0.319000,0.035000,0.203200> rotate<0,0.000000,0> translate<53.419800,-1.535000,10.972800> }
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<53.546100,-1.535000,10.132100>}
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<53.546100,-1.535000,10.667800>}
box{<0,0,-0.203200><0.535700,0.035000,0.203200> rotate<0,90.000000,0> translate<53.546100,-1.535000,10.667800> }
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<53.546100,-1.535000,10.160000>}
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<53.947900,-1.535000,10.160000>}
box{<0,0,-0.203200><0.401800,0.035000,0.203200> rotate<0,0.000000,0> translate<53.546100,-1.535000,10.160000> }
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<53.546100,-1.535000,10.566400>}
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<53.779500,-1.535000,10.566400>}
box{<0,0,-0.203200><0.233400,0.035000,0.203200> rotate<0,0.000000,0> translate<53.546100,-1.535000,10.566400> }
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<53.738800,-1.535000,10.664800>}
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<54.041200,-1.535000,9.934900>}
box{<0,0,-0.203200><0.790063,0.035000,0.203200> rotate<0,67.491156,0> translate<53.738800,-1.535000,10.664800> }
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<53.738800,-1.535000,11.455100>}
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<53.738800,-1.535000,10.664800>}
box{<0,0,-0.203200><0.790300,0.035000,0.203200> rotate<0,-90.000000,0> translate<53.738800,-1.535000,10.664800> }
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<53.738800,-1.535000,11.455100>}
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<54.041200,-1.535000,12.185000>}
box{<0,0,-0.203200><0.790063,0.035000,0.203200> rotate<0,-67.491156,0> translate<53.738800,-1.535000,11.455100> }
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<53.738800,-1.535000,18.584800>}
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<54.041200,-1.535000,17.854900>}
box{<0,0,-0.203200><0.790063,0.035000,0.203200> rotate<0,67.491156,0> translate<53.738800,-1.535000,18.584800> }
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<53.738800,-1.535000,19.375100>}
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<53.738800,-1.535000,18.584800>}
box{<0,0,-0.203200><0.790300,0.035000,0.203200> rotate<0,-90.000000,0> translate<53.738800,-1.535000,18.584800> }
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<53.738800,-1.535000,19.375100>}
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<54.041200,-1.535000,20.105000>}
box{<0,0,-0.203200><0.790063,0.035000,0.203200> rotate<0,-67.491156,0> translate<53.738800,-1.535000,19.375100> }
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<53.738800,-1.535000,22.544800>}
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<54.041200,-1.535000,21.814900>}
box{<0,0,-0.203200><0.790063,0.035000,0.203200> rotate<0,67.491156,0> translate<53.738800,-1.535000,22.544800> }
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<53.738800,-1.535000,23.335100>}
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<53.738800,-1.535000,22.544800>}
box{<0,0,-0.203200><0.790300,0.035000,0.203200> rotate<0,-90.000000,0> translate<53.738800,-1.535000,22.544800> }
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<53.738800,-1.535000,23.335100>}
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<54.041200,-1.535000,24.065000>}
box{<0,0,-0.203200><0.790063,0.035000,0.203200> rotate<0,-67.491156,0> translate<53.738800,-1.535000,23.335100> }
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<54.041200,-1.535000,9.934900>}
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<54.599900,-1.535000,9.376200>}
box{<0,0,-0.203200><0.790121,0.035000,0.203200> rotate<0,44.997030,0> translate<54.041200,-1.535000,9.934900> }
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<54.041200,-1.535000,12.185000>}
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<54.599900,-1.535000,12.743700>}
box{<0,0,-0.203200><0.790121,0.035000,0.203200> rotate<0,-44.997030,0> translate<54.041200,-1.535000,12.185000> }
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<54.041200,-1.535000,17.854900>}
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<54.599900,-1.535000,17.296200>}
box{<0,0,-0.203200><0.790121,0.035000,0.203200> rotate<0,44.997030,0> translate<54.041200,-1.535000,17.854900> }
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<54.041200,-1.535000,20.105000>}
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<54.599900,-1.535000,20.663700>}
box{<0,0,-0.203200><0.790121,0.035000,0.203200> rotate<0,-44.997030,0> translate<54.041200,-1.535000,20.105000> }
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<54.041200,-1.535000,21.814900>}
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<54.599900,-1.535000,21.256200>}
box{<0,0,-0.203200><0.790121,0.035000,0.203200> rotate<0,44.997030,0> translate<54.041200,-1.535000,21.814900> }
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<54.041200,-1.535000,24.065000>}
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<54.599900,-1.535000,24.623700>}
box{<0,0,-0.203200><0.790121,0.035000,0.203200> rotate<0,-44.997030,0> translate<54.041200,-1.535000,24.065000> }
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<54.262300,-1.535000,13.673800>}
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<54.599900,-1.535000,13.336200>}
box{<0,0,-0.203200><0.477438,0.035000,0.203200> rotate<0,44.997030,0> translate<54.262300,-1.535000,13.673800> }
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<54.262300,-1.535000,16.366100>}
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<54.599900,-1.535000,16.703700>}
box{<0,0,-0.203200><0.477438,0.035000,0.203200> rotate<0,-44.997030,0> translate<54.262300,-1.535000,16.366100> }
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<54.599900,-1.535000,9.376200>}
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<55.329800,-1.535000,9.073800>}
box{<0,0,-0.203200><0.790063,0.035000,0.203200> rotate<0,22.502905,0> translate<54.599900,-1.535000,9.376200> }
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<54.599900,-1.535000,12.743700>}
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<55.315100,-1.535000,13.039900>}
box{<0,0,-0.203200><0.774109,0.035000,0.203200> rotate<0,-22.495401,0> translate<54.599900,-1.535000,12.743700> }
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<54.599900,-1.535000,13.336200>}
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<55.315100,-1.535000,13.039900>}
box{<0,0,-0.203200><0.774148,0.035000,0.203200> rotate<0,22.502239,0> translate<54.599900,-1.535000,13.336200> }
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<54.599900,-1.535000,16.703700>}
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<55.315100,-1.535000,16.999900>}
box{<0,0,-0.203200><0.774109,0.035000,0.203200> rotate<0,-22.495401,0> translate<54.599900,-1.535000,16.703700> }
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<54.599900,-1.535000,17.296200>}
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<55.315100,-1.535000,16.999900>}
box{<0,0,-0.203200><0.774148,0.035000,0.203200> rotate<0,22.502239,0> translate<54.599900,-1.535000,17.296200> }
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<54.599900,-1.535000,20.663700>}
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<55.315100,-1.535000,20.959900>}
box{<0,0,-0.203200><0.774109,0.035000,0.203200> rotate<0,-22.495401,0> translate<54.599900,-1.535000,20.663700> }
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<54.599900,-1.535000,21.256200>}
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<55.315100,-1.535000,20.959900>}
box{<0,0,-0.203200><0.774148,0.035000,0.203200> rotate<0,22.502239,0> translate<54.599900,-1.535000,21.256200> }
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<54.599900,-1.535000,24.623700>}
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<55.329800,-1.535000,24.926100>}
box{<0,0,-0.203200><0.790063,0.035000,0.203200> rotate<0,-22.502905,0> translate<54.599900,-1.535000,24.623700> }
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<54.679000,-1.535000,24.866600>}
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<55.120900,-1.535000,24.866600>}
box{<0,0,-0.203200><0.441900,0.035000,0.203200> rotate<0,0.000000,0> translate<54.679000,-1.535000,24.866600> }
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<55.120900,-1.535000,24.866600>}
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<56.787500,-1.535000,26.533200>}
box{<0,0,-0.203200><2.356928,0.035000,0.203200> rotate<0,-44.997030,0> translate<55.120900,-1.535000,24.866600> }
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<55.329800,-1.535000,9.073800>}
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<58.670100,-1.535000,9.073800>}
box{<0,0,-0.203200><3.340300,0.035000,0.203200> rotate<0,0.000000,0> translate<55.329800,-1.535000,9.073800> }
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<55.329800,-1.535000,24.926100>}
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<58.670100,-1.535000,24.926100>}
box{<0,0,-0.203200><3.340300,0.035000,0.203200> rotate<0,0.000000,0> translate<55.329800,-1.535000,24.926100> }
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<55.451100,-1.535000,25.196800>}
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<61.288800,-1.535000,25.196800>}
box{<0,0,-0.203200><5.837700,0.035000,0.203200> rotate<0,0.000000,0> translate<55.451100,-1.535000,25.196800> }
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<55.857500,-1.535000,25.603200>}
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<61.288800,-1.535000,25.603200>}
box{<0,0,-0.203200><5.431300,0.035000,0.203200> rotate<0,0.000000,0> translate<55.857500,-1.535000,25.603200> }
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<56.263900,-1.535000,26.009600>}
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<58.858000,-1.535000,26.009600>}
box{<0,0,-0.203200><2.594100,0.035000,0.203200> rotate<0,0.000000,0> translate<56.263900,-1.535000,26.009600> }
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<56.670300,-1.535000,26.416000>}
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<58.331400,-1.535000,26.416000>}
box{<0,0,-0.203200><1.661100,0.035000,0.203200> rotate<0,0.000000,0> translate<56.670300,-1.535000,26.416000> }
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<56.787500,-1.535000,26.533200>}
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<57.311800,-1.535000,26.533200>}
box{<0,0,-0.203200><0.524300,0.035000,0.203200> rotate<0,0.000000,0> translate<56.787500,-1.535000,26.533200> }
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<57.311800,-1.535000,26.533200>}
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<57.862300,-1.535000,27.083800>}
box{<0,0,-0.203200><0.778595,0.035000,0.203200> rotate<0,-45.002233,0> translate<57.311800,-1.535000,26.533200> }
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<57.600900,-1.535000,26.822400>}
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<57.970400,-1.535000,26.822400>}
box{<0,0,-0.203200><0.369500,0.035000,0.203200> rotate<0,0.000000,0> translate<57.600900,-1.535000,26.822400> }
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<57.688800,-1.535000,1.545200>}
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<58.034400,-1.535000,0.711100>}
box{<0,0,-0.203200><0.902863,0.035000,0.203200> rotate<0,67.489421,0> translate<57.688800,-1.535000,1.545200> }
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<57.688800,-1.535000,2.464700>}
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<57.688800,-1.535000,1.545200>}
box{<0,0,-0.203200><0.919500,0.035000,0.203200> rotate<0,-90.000000,0> translate<57.688800,-1.535000,1.545200> }
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<57.688800,-1.535000,2.464700>}
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<58.040600,-1.535000,3.314100>}
box{<0,0,-0.203200><0.919371,0.035000,0.203200> rotate<0,-67.497445,0> translate<57.688800,-1.535000,2.464700> }
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<57.862300,-1.535000,27.083800>}
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<58.002600,-1.535000,26.744800>}
box{<0,0,-0.203200><0.366886,0.035000,0.203200> rotate<0,67.512627,0> translate<57.862300,-1.535000,27.083800> }
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<58.002600,-1.535000,26.744800>}
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<58.652800,-1.535000,26.094600>}
box{<0,0,-0.203200><0.919522,0.035000,0.203200> rotate<0,44.997030,0> translate<58.002600,-1.535000,26.744800> }
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<58.040600,-1.535000,3.314100>}
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<58.690800,-1.535000,3.964300>}
box{<0,0,-0.203200><0.919522,0.035000,0.203200> rotate<0,-44.997030,0> translate<58.040600,-1.535000,3.314100> }
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<58.652800,-1.535000,26.094600>}
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<59.502200,-1.535000,25.742800>}
box{<0,0,-0.203200><0.919371,0.035000,0.203200> rotate<0,22.496615,0> translate<58.652800,-1.535000,26.094600> }
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<58.670100,-1.535000,9.073800>}
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<59.400000,-1.535000,9.376200>}
box{<0,0,-0.203200><0.790063,0.035000,0.203200> rotate<0,-22.502905,0> translate<58.670100,-1.535000,9.073800> }
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<58.670100,-1.535000,24.926100>}
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<59.400000,-1.535000,24.623700>}
box{<0,0,-0.203200><0.790063,0.035000,0.203200> rotate<0,22.502905,0> translate<58.670100,-1.535000,24.926100> }
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<58.684800,-1.535000,13.039900>}
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<59.400000,-1.535000,12.743700>}
box{<0,0,-0.203200><0.774109,0.035000,0.203200> rotate<0,22.495401,0> translate<58.684800,-1.535000,13.039900> }
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<58.684800,-1.535000,13.039900>}
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<59.400000,-1.535000,13.336200>}
box{<0,0,-0.203200><0.774148,0.035000,0.203200> rotate<0,-22.502239,0> translate<58.684800,-1.535000,13.039900> }
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<58.684800,-1.535000,16.999900>}
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<59.400000,-1.535000,16.703700>}
box{<0,0,-0.203200><0.774109,0.035000,0.203200> rotate<0,22.495401,0> translate<58.684800,-1.535000,16.999900> }
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<58.684800,-1.535000,16.999900>}
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<59.400000,-1.535000,17.296200>}
box{<0,0,-0.203200><0.774148,0.035000,0.203200> rotate<0,-22.502239,0> translate<58.684800,-1.535000,16.999900> }
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<58.684800,-1.535000,20.959900>}
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<59.400000,-1.535000,20.663700>}
box{<0,0,-0.203200><0.774109,0.035000,0.203200> rotate<0,22.495401,0> translate<58.684800,-1.535000,20.959900> }
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<58.684800,-1.535000,20.959900>}
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<59.400000,-1.535000,21.256200>}
box{<0,0,-0.203200><0.774148,0.035000,0.203200> rotate<0,-22.502239,0> translate<58.684800,-1.535000,20.959900> }
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<58.690800,-1.535000,3.964300>}
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<59.540200,-1.535000,4.316100>}
box{<0,0,-0.203200><0.919371,0.035000,0.203200> rotate<0,-22.496615,0> translate<58.690800,-1.535000,3.964300> }
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<58.769600,-1.535000,13.004800>}
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<61.288800,-1.535000,13.004800>}
box{<0,0,-0.203200><2.519200,0.035000,0.203200> rotate<0,0.000000,0> translate<58.769600,-1.535000,13.004800> }
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<58.851100,-1.535000,17.068800>}
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<61.288800,-1.535000,17.068800>}
box{<0,0,-0.203200><2.437700,0.035000,0.203200> rotate<0,0.000000,0> translate<58.851100,-1.535000,17.068800> }
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<58.997700,-1.535000,24.790400>}
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<61.288800,-1.535000,24.790400>}
box{<0,0,-0.203200><2.291100,0.035000,0.203200> rotate<0,0.000000,0> translate<58.997700,-1.535000,24.790400> }
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<59.102100,-1.535000,21.132800>}
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<61.288800,-1.535000,21.132800>}
box{<0,0,-0.203200><2.186700,0.035000,0.203200> rotate<0,0.000000,0> translate<59.102100,-1.535000,21.132800> }
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<59.248700,-1.535000,20.726400>}
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<61.288800,-1.535000,20.726400>}
box{<0,0,-0.203200><2.040100,0.035000,0.203200> rotate<0,0.000000,0> translate<59.248700,-1.535000,20.726400> }
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<59.330000,-1.535000,9.347200>}
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<61.288800,-1.535000,9.347200>}
box{<0,0,-0.203200><1.958800,0.035000,0.203200> rotate<0,0.000000,0> translate<59.330000,-1.535000,9.347200> }
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<59.400000,-1.535000,9.376200>}
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<59.958700,-1.535000,9.934900>}
box{<0,0,-0.203200><0.790121,0.035000,0.203200> rotate<0,-44.997030,0> translate<59.400000,-1.535000,9.376200> }
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<59.400000,-1.535000,12.743700>}
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<59.958700,-1.535000,12.185000>}
box{<0,0,-0.203200><0.790121,0.035000,0.203200> rotate<0,44.997030,0> translate<59.400000,-1.535000,12.743700> }
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<59.400000,-1.535000,13.336200>}
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<59.958700,-1.535000,13.894900>}
box{<0,0,-0.203200><0.790121,0.035000,0.203200> rotate<0,-44.997030,0> translate<59.400000,-1.535000,13.336200> }
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<59.400000,-1.535000,16.703700>}
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<59.958700,-1.535000,16.145000>}
box{<0,0,-0.203200><0.790121,0.035000,0.203200> rotate<0,44.997030,0> translate<59.400000,-1.535000,16.703700> }
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<59.400000,-1.535000,17.296200>}
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<59.958700,-1.535000,17.854900>}
box{<0,0,-0.203200><0.790121,0.035000,0.203200> rotate<0,-44.997030,0> translate<59.400000,-1.535000,17.296200> }
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<59.400000,-1.535000,20.663700>}
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<59.958700,-1.535000,20.105000>}
box{<0,0,-0.203200><0.790121,0.035000,0.203200> rotate<0,44.997030,0> translate<59.400000,-1.535000,20.663700> }
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<59.400000,-1.535000,21.256200>}
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<59.958700,-1.535000,21.814900>}
box{<0,0,-0.203200><0.790121,0.035000,0.203200> rotate<0,-44.997030,0> translate<59.400000,-1.535000,21.256200> }
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<59.400000,-1.535000,24.623700>}
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<59.958700,-1.535000,24.065000>}
box{<0,0,-0.203200><0.790121,0.035000,0.203200> rotate<0,44.997030,0> translate<59.400000,-1.535000,24.623700> }
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<59.441300,-1.535000,16.662400>}
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<61.288800,-1.535000,16.662400>}
box{<0,0,-0.203200><1.847500,0.035000,0.203200> rotate<0,0.000000,0> translate<59.441300,-1.535000,16.662400> }
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<59.475000,-1.535000,13.411200>}
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<61.288800,-1.535000,13.411200>}
box{<0,0,-0.203200><1.813800,0.035000,0.203200> rotate<0,0.000000,0> translate<59.475000,-1.535000,13.411200> }
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<59.502200,-1.535000,25.742800>}
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<60.421700,-1.535000,25.742800>}
box{<0,0,-0.203200><0.919500,0.035000,0.203200> rotate<0,0.000000,0> translate<59.502200,-1.535000,25.742800> }
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<59.540200,-1.535000,4.316100>}
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<60.459700,-1.535000,4.316100>}
box{<0,0,-0.203200><0.919500,0.035000,0.203200> rotate<0,0.000000,0> translate<59.540200,-1.535000,4.316100> }
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<59.545300,-1.535000,12.598400>}
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<61.288800,-1.535000,12.598400>}
box{<0,0,-0.203200><1.743500,0.035000,0.203200> rotate<0,0.000000,0> translate<59.545300,-1.535000,12.598400> }
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<59.579000,-1.535000,17.475200>}
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<61.288800,-1.535000,17.475200>}
box{<0,0,-0.203200><1.709800,0.035000,0.203200> rotate<0,0.000000,0> translate<59.579000,-1.535000,17.475200> }
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<59.639700,-1.535000,24.384000>}
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<61.288800,-1.535000,24.384000>}
box{<0,0,-0.203200><1.649100,0.035000,0.203200> rotate<0,0.000000,0> translate<59.639700,-1.535000,24.384000> }
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<59.683000,-1.535000,21.539200>}
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<61.288800,-1.535000,21.539200>}
box{<0,0,-0.203200><1.605800,0.035000,0.203200> rotate<0,0.000000,0> translate<59.683000,-1.535000,21.539200> }
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<59.743700,-1.535000,20.320000>}
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<61.288800,-1.535000,20.320000>}
box{<0,0,-0.203200><1.545100,0.035000,0.203200> rotate<0,0.000000,0> translate<59.743700,-1.535000,20.320000> }
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<59.777400,-1.535000,9.753600>}
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<61.288800,-1.535000,9.753600>}
box{<0,0,-0.203200><1.511400,0.035000,0.203200> rotate<0,0.000000,0> translate<59.777400,-1.535000,9.753600> }
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<59.847700,-1.535000,16.256000>}
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<61.288800,-1.535000,16.256000>}
box{<0,0,-0.203200><1.441100,0.035000,0.203200> rotate<0,0.000000,0> translate<59.847700,-1.535000,16.256000> }
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<59.881400,-1.535000,13.817600>}
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<61.288800,-1.535000,13.817600>}
box{<0,0,-0.203200><1.407400,0.035000,0.203200> rotate<0,0.000000,0> translate<59.881400,-1.535000,13.817600> }
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<59.951700,-1.535000,12.192000>}
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<61.288800,-1.535000,12.192000>}
box{<0,0,-0.203200><1.337100,0.035000,0.203200> rotate<0,0.000000,0> translate<59.951700,-1.535000,12.192000> }
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<59.958700,-1.535000,9.934900>}
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<60.261100,-1.535000,10.664800>}
box{<0,0,-0.203200><0.790063,0.035000,0.203200> rotate<0,-67.491156,0> translate<59.958700,-1.535000,9.934900> }
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<59.958700,-1.535000,12.185000>}
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<60.261100,-1.535000,11.455100>}
box{<0,0,-0.203200><0.790063,0.035000,0.203200> rotate<0,67.491156,0> translate<59.958700,-1.535000,12.185000> }
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<59.958700,-1.535000,13.894900>}
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<60.261100,-1.535000,14.624800>}
box{<0,0,-0.203200><0.790063,0.035000,0.203200> rotate<0,-67.491156,0> translate<59.958700,-1.535000,13.894900> }
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<59.958700,-1.535000,16.145000>}
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<60.261100,-1.535000,15.415100>}
box{<0,0,-0.203200><0.790063,0.035000,0.203200> rotate<0,67.491156,0> translate<59.958700,-1.535000,16.145000> }
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<59.958700,-1.535000,17.854900>}
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<60.261100,-1.535000,18.584800>}
box{<0,0,-0.203200><0.790063,0.035000,0.203200> rotate<0,-67.491156,0> translate<59.958700,-1.535000,17.854900> }
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<59.958700,-1.535000,20.105000>}
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<60.261100,-1.535000,19.375100>}
box{<0,0,-0.203200><0.790063,0.035000,0.203200> rotate<0,67.491156,0> translate<59.958700,-1.535000,20.105000> }
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<59.958700,-1.535000,21.814900>}
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<60.261100,-1.535000,22.544800>}
box{<0,0,-0.203200><0.790063,0.035000,0.203200> rotate<0,-67.491156,0> translate<59.958700,-1.535000,21.814900> }
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<59.958700,-1.535000,24.065000>}
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<60.261100,-1.535000,23.335100>}
box{<0,0,-0.203200><0.790063,0.035000,0.203200> rotate<0,67.491156,0> translate<59.958700,-1.535000,24.065000> }
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<59.969700,-1.535000,17.881600>}
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<61.288800,-1.535000,17.881600>}
box{<0,0,-0.203200><1.319100,0.035000,0.203200> rotate<0,0.000000,0> translate<59.969700,-1.535000,17.881600> }
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<59.995000,-1.535000,23.977600>}
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<61.288800,-1.535000,23.977600>}
box{<0,0,-0.203200><1.293800,0.035000,0.203200> rotate<0,0.000000,0> translate<59.995000,-1.535000,23.977600> }
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<60.012800,-1.535000,21.945600>}
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<61.288800,-1.535000,21.945600>}
box{<0,0,-0.203200><1.276000,0.035000,0.203200> rotate<0,0.000000,0> translate<60.012800,-1.535000,21.945600> }
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<60.038000,-1.535000,19.913600>}
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<61.288800,-1.535000,19.913600>}
box{<0,0,-0.203200><1.250800,0.035000,0.203200> rotate<0,0.000000,0> translate<60.038000,-1.535000,19.913600> }
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<60.051900,-1.535000,10.160000>}
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<61.288800,-1.535000,10.160000>}
box{<0,0,-0.203200><1.236900,0.035000,0.203200> rotate<0,0.000000,0> translate<60.051900,-1.535000,10.160000> }
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<60.081100,-1.535000,15.849600>}
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<61.288800,-1.535000,15.849600>}
box{<0,0,-0.203200><1.207700,0.035000,0.203200> rotate<0,0.000000,0> translate<60.081100,-1.535000,15.849600> }
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<60.095000,-1.535000,14.224000>}
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<61.288800,-1.535000,14.224000>}
box{<0,0,-0.203200><1.193800,0.035000,0.203200> rotate<0,0.000000,0> translate<60.095000,-1.535000,14.224000> }
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<60.124200,-1.535000,11.785600>}
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<61.288800,-1.535000,11.785600>}
box{<0,0,-0.203200><1.164600,0.035000,0.203200> rotate<0,0.000000,0> translate<60.124200,-1.535000,11.785600> }
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<60.138100,-1.535000,18.288000>}
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<61.288800,-1.535000,18.288000>}
box{<0,0,-0.203200><1.150700,0.035000,0.203200> rotate<0,0.000000,0> translate<60.138100,-1.535000,18.288000> }
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<60.163300,-1.535000,23.571200>}
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<61.288800,-1.535000,23.571200>}
box{<0,0,-0.203200><1.125500,0.035000,0.203200> rotate<0,0.000000,0> translate<60.163300,-1.535000,23.571200> }
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<60.181200,-1.535000,22.352000>}
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<61.288800,-1.535000,22.352000>}
box{<0,0,-0.203200><1.107600,0.035000,0.203200> rotate<0,0.000000,0> translate<60.181200,-1.535000,22.352000> }
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<60.206400,-1.535000,19.507200>}
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<61.288800,-1.535000,19.507200>}
box{<0,0,-0.203200><1.082400,0.035000,0.203200> rotate<0,0.000000,0> translate<60.206400,-1.535000,19.507200> }
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<60.220300,-1.535000,10.566400>}
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<61.288800,-1.535000,10.566400>}
box{<0,0,-0.203200><1.068500,0.035000,0.203200> rotate<0,0.000000,0> translate<60.220300,-1.535000,10.566400> }
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<60.249500,-1.535000,15.443200>}
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<61.288800,-1.535000,15.443200>}
box{<0,0,-0.203200><1.039300,0.035000,0.203200> rotate<0,0.000000,0> translate<60.249500,-1.535000,15.443200> }
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<60.261100,-1.535000,10.664800>}
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<60.261100,-1.535000,11.455100>}
box{<0,0,-0.203200><0.790300,0.035000,0.203200> rotate<0,90.000000,0> translate<60.261100,-1.535000,11.455100> }
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<60.261100,-1.535000,10.972800>}
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<61.288800,-1.535000,10.972800>}
box{<0,0,-0.203200><1.027700,0.035000,0.203200> rotate<0,0.000000,0> translate<60.261100,-1.535000,10.972800> }
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<60.261100,-1.535000,11.379200>}
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<61.288800,-1.535000,11.379200>}
box{<0,0,-0.203200><1.027700,0.035000,0.203200> rotate<0,0.000000,0> translate<60.261100,-1.535000,11.379200> }
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<60.261100,-1.535000,14.624800>}
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<60.261100,-1.535000,15.415100>}
box{<0,0,-0.203200><0.790300,0.035000,0.203200> rotate<0,90.000000,0> translate<60.261100,-1.535000,15.415100> }
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<60.261100,-1.535000,14.630400>}
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<61.288800,-1.535000,14.630400>}
box{<0,0,-0.203200><1.027700,0.035000,0.203200> rotate<0,0.000000,0> translate<60.261100,-1.535000,14.630400> }
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<60.261100,-1.535000,15.036800>}
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<61.288800,-1.535000,15.036800>}
box{<0,0,-0.203200><1.027700,0.035000,0.203200> rotate<0,0.000000,0> translate<60.261100,-1.535000,15.036800> }
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<60.261100,-1.535000,18.584800>}
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<60.261100,-1.535000,19.375100>}
box{<0,0,-0.203200><0.790300,0.035000,0.203200> rotate<0,90.000000,0> translate<60.261100,-1.535000,19.375100> }
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<60.261100,-1.535000,18.694400>}
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<61.288800,-1.535000,18.694400>}
box{<0,0,-0.203200><1.027700,0.035000,0.203200> rotate<0,0.000000,0> translate<60.261100,-1.535000,18.694400> }
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<60.261100,-1.535000,19.100800>}
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<61.288800,-1.535000,19.100800>}
box{<0,0,-0.203200><1.027700,0.035000,0.203200> rotate<0,0.000000,0> translate<60.261100,-1.535000,19.100800> }
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<60.261100,-1.535000,22.544800>}
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<60.261100,-1.535000,23.335100>}
box{<0,0,-0.203200><0.790300,0.035000,0.203200> rotate<0,90.000000,0> translate<60.261100,-1.535000,23.335100> }
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<60.261100,-1.535000,22.758400>}
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<61.288800,-1.535000,22.758400>}
box{<0,0,-0.203200><1.027700,0.035000,0.203200> rotate<0,0.000000,0> translate<60.261100,-1.535000,22.758400> }
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<60.261100,-1.535000,23.164800>}
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<61.288800,-1.535000,23.164800>}
box{<0,0,-0.203200><1.027700,0.035000,0.203200> rotate<0,0.000000,0> translate<60.261100,-1.535000,23.164800> }
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<60.421700,-1.535000,25.742800>}
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<61.271100,-1.535000,26.094600>}
box{<0,0,-0.203200><0.919371,0.035000,0.203200> rotate<0,-22.496615,0> translate<60.421700,-1.535000,25.742800> }
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<60.459700,-1.535000,4.316100>}
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<61.288800,-1.535000,3.972600>}
box{<0,0,-0.203200><0.897440,0.035000,0.203200> rotate<0,22.502970,0> translate<60.459700,-1.535000,4.316100> }
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<61.065800,-1.535000,26.009600>}
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<61.288800,-1.535000,26.009600>}
box{<0,0,-0.203200><0.223000,0.035000,0.203200> rotate<0,0.000000,0> translate<61.065800,-1.535000,26.009600> }
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<61.068200,-1.535000,4.064000>}
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<61.288800,-1.535000,4.064000>}
box{<0,0,-0.203200><0.220600,0.035000,0.203200> rotate<0,0.000000,0> translate<61.068200,-1.535000,4.064000> }
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<61.271100,-1.535000,26.094600>}
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<61.288800,-1.535000,26.112400>}
box{<0,0,-0.203200><0.025102,0.035000,0.203200> rotate<0,-45.158416,0> translate<61.271100,-1.535000,26.094600> }
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<61.288800,-1.535000,26.112400>}
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<61.288800,-1.535000,3.972600>}
box{<0,0,-0.203200><22.139800,0.035000,0.203200> rotate<0,-90.000000,0> translate<61.288800,-1.535000,3.972600> }
texture{col_pol}
}
#end
union{
cylinder{<41.030000,0.038000,4.400000><41.030000,-1.538000,4.400000>0.660400}
cylinder{<18.170000,0.038000,4.400000><18.170000,-1.538000,4.400000>0.660400}
cylinder{<48.400000,0.038000,7.270000><48.400000,-1.538000,7.270000>0.508000}
cylinder{<48.400000,0.038000,4.730000><48.400000,-1.538000,4.730000>0.508000}
cylinder{<56.870000,0.038000,27.600000><56.870000,-1.538000,27.600000>0.406400}
cylinder{<54.330000,0.038000,27.600000><54.330000,-1.538000,27.600000>0.406400}
cylinder{<-9.600000,0.038000,27.060000><-9.600000,-1.538000,27.060000>0.508000}
cylinder{<-12.140000,0.038000,27.060000><-12.140000,-1.538000,27.060000>0.508000}
cylinder{<-7.060000,0.038000,27.060000><-7.060000,-1.538000,27.060000>0.508000}
cylinder{<23.860000,0.038000,26.400000><23.860000,-1.538000,26.400000>0.508000}
cylinder{<26.400000,0.038000,26.400000><26.400000,-1.538000,26.400000>0.508000}
cylinder{<28.940000,0.038000,26.400000><28.940000,-1.538000,26.400000>0.508000}
cylinder{<35.460000,0.038000,26.400000><35.460000,-1.538000,26.400000>0.508000}
cylinder{<38.000000,0.038000,26.400000><38.000000,-1.538000,26.400000>0.508000}
cylinder{<40.540000,0.038000,26.400000><40.540000,-1.538000,26.400000>0.508000}
cylinder{<44.948800,0.038000,28.660600><44.948800,-1.538000,28.660600>0.508000}
cylinder{<51.451200,0.038000,28.660600><51.451200,-1.538000,28.660600>0.508000}
cylinder{<44.948800,0.038000,24.139400><44.948800,-1.538000,24.139400>0.508000}
cylinder{<51.451200,0.038000,24.139400><51.451200,-1.538000,24.139400>0.508000}
cylinder{<9.730000,0.038000,11.190000><9.730000,-1.538000,11.190000>0.508000}
cylinder{<12.270000,0.038000,11.190000><12.270000,-1.538000,11.190000>0.508000}
cylinder{<9.730000,0.038000,13.730000><9.730000,-1.538000,13.730000>0.508000}
cylinder{<12.270000,0.038000,13.730000><12.270000,-1.538000,13.730000>0.508000}
cylinder{<9.730000,0.038000,16.270000><9.730000,-1.538000,16.270000>0.508000}
cylinder{<12.270000,0.038000,16.270000><12.270000,-1.538000,16.270000>0.508000}
cylinder{<9.730000,0.038000,18.810000><9.730000,-1.538000,18.810000>0.508000}
cylinder{<12.270000,0.038000,18.810000><12.270000,-1.538000,18.810000>0.508000}
cylinder{<14.600000,0.038000,27.780000><14.600000,-1.538000,27.780000>0.850000}
cylinder{<14.600000,0.038000,23.820000><14.600000,-1.538000,23.820000>0.850000}
cylinder{<57.000000,0.038000,11.060000><57.000000,-1.538000,11.060000>0.850000}
cylinder{<57.000000,0.038000,15.020000><57.000000,-1.538000,15.020000>0.850000}
cylinder{<57.000000,0.038000,18.980000><57.000000,-1.538000,18.980000>0.850000}
cylinder{<57.000000,0.038000,22.940000><57.000000,-1.538000,22.940000>0.850000}
//Holes(fast)/Vias
cylinder{<-0.500000,0.038000,18.600000><-0.500000,-1.538000,18.600000>0.127000 }
cylinder{<-8.900000,0.038000,24.300000><-8.900000,-1.538000,24.300000>0.127000 }
cylinder{<8.500000,0.038000,9.000000><8.500000,-1.538000,9.000000>0.127000 }
cylinder{<-10.400000,0.038000,15.500000><-10.400000,-1.538000,15.500000>0.127000 }
cylinder{<27.100000,0.038000,16.100000><27.100000,-1.538000,16.100000>0.127000 }
cylinder{<14.500000,0.038000,21.600000><14.500000,-1.538000,21.600000>0.127000 }
cylinder{<14.500000,0.038000,18.000000><14.500000,-1.538000,18.000000>0.127000 }
cylinder{<42.900000,0.038000,16.600000><42.900000,-1.538000,16.600000>0.127000 }
cylinder{<51.200000,0.038000,7.400000><51.200000,-1.538000,7.400000>0.254000 }
cylinder{<19.100000,0.038000,22.800000><19.100000,-1.538000,22.800000>0.254000 }
cylinder{<28.900000,0.038000,22.900000><28.900000,-1.538000,22.900000>0.254000 }
cylinder{<33.400000,0.038000,22.900000><33.400000,-1.538000,22.900000>0.254000 }
cylinder{<17.800000,0.038000,21.700000><17.800000,-1.538000,21.700000>0.127000 }
cylinder{<17.800000,0.038000,20.000000><17.800000,-1.538000,20.000000>0.127000 }
cylinder{<10.800000,0.038000,6.700000><10.800000,-1.538000,6.700000>0.127000 }
cylinder{<10.700000,0.038000,23.600000><10.700000,-1.538000,23.600000>0.127000 }
cylinder{<3.500000,0.038000,19.700000><3.500000,-1.538000,19.700000>0.127000 }
cylinder{<19.800000,0.038000,13.400000><19.800000,-1.538000,13.400000>0.127000 }
cylinder{<10.700000,0.038000,21.100000><10.700000,-1.538000,21.100000>0.127000 }
cylinder{<-3.100000,0.038000,15.400000><-3.100000,-1.538000,15.400000>0.127000 }
cylinder{<-9.300000,0.038000,15.400000><-9.300000,-1.538000,15.400000>0.127000 }
cylinder{<-11.700000,0.038000,23.400000><-11.700000,-1.538000,23.400000>0.127000 }
cylinder{<-14.000000,0.038000,18.100000><-14.000000,-1.538000,18.100000>0.127000 }
cylinder{<-7.000000,0.038000,21.300000><-7.000000,-1.538000,21.300000>0.127000 }
cylinder{<3.100000,0.038000,13.000000><3.100000,-1.538000,13.000000>0.127000 }
cylinder{<1.900000,0.038000,22.600000><1.900000,-1.538000,22.600000>0.127000 }
cylinder{<-12.300000,0.038000,20.300000><-12.300000,-1.538000,20.300000>0.127000 }
cylinder{<-8.000000,0.038000,20.500000><-8.000000,-1.538000,20.500000>0.127000 }
cylinder{<-3.900000,0.038000,16.400000><-3.900000,-1.538000,16.400000>0.127000 }
cylinder{<-5.200000,0.038000,18.600000><-5.200000,-1.538000,18.600000>0.127000 }
cylinder{<4.100000,0.038000,6.500000><4.100000,-1.538000,6.500000>0.127000 }
cylinder{<5.900000,0.038000,6.500000><5.900000,-1.538000,6.500000>0.127000 }
cylinder{<32.100000,0.038000,20.400000><32.100000,-1.538000,20.400000>0.127000 }
cylinder{<32.200000,0.038000,29.100000><32.200000,-1.538000,29.100000>0.127000 }
cylinder{<25.100000,0.038000,5.100000><25.100000,-1.538000,5.100000>0.127000 }
cylinder{<19.400000,0.038000,9.800000><19.400000,-1.538000,9.800000>0.127000 }
cylinder{<35.000000,0.038000,13.000000><35.000000,-1.538000,13.000000>0.254000 }
cylinder{<42.900000,0.038000,7.100000><42.900000,-1.538000,7.100000>0.254000 }
cylinder{<39.000000,0.038000,21.100000><39.000000,-1.538000,21.100000>0.254000 }
cylinder{<46.600000,0.038000,10.400000><46.600000,-1.538000,10.400000>0.254000 }
cylinder{<52.200000,0.038000,10.400000><52.200000,-1.538000,10.400000>0.254000 }
cylinder{<51.700000,0.038000,21.300000><51.700000,-1.538000,21.300000>0.254000 }
cylinder{<-14.000000,0.038000,16.800000><-14.000000,-1.538000,16.800000>0.127000 }
cylinder{<-9.300000,0.038000,16.800000><-9.300000,-1.538000,16.800000>0.127000 }
cylinder{<7.400000,0.038000,22.200000><7.400000,-1.538000,22.200000>0.127000 }
cylinder{<5.000000,0.038000,3.500000><5.000000,-1.538000,3.500000>0.127000 }
cylinder{<44.600000,0.038000,9.200000><44.600000,-1.538000,9.200000>0.254000 }
cylinder{<50.300000,0.038000,15.000000><50.300000,-1.538000,15.000000>0.254000 }
//Holes(fast)/Board
texture{col_hls}
}
#if(pcb_silkscreen=on)
//Silk Screen
union{
cylinder{<0,0,0><0,0.036000,0>0.038100 translate<58.017500,0.000000,25.969600>}
cylinder{<0,0,0><0,0.036000,0>0.038100 translate<57.894700,0.000000,26.092500>}
box{<0,0,-0.038100><0.173736,0.036000,0.038100> rotate<0,45.020348,0> translate<57.894700,0.000000,26.092500> }
cylinder{<0,0,0><0,0.036000,0>0.038100 translate<57.894700,0.000000,26.092500>}
cylinder{<0,0,0><0,0.036000,0>0.038100 translate<57.648900,0.000000,26.092500>}
box{<0,0,-0.038100><0.245800,0.036000,0.038100> rotate<0,0.000000,0> translate<57.648900,0.000000,26.092500> }
cylinder{<0,0,0><0,0.036000,0>0.038100 translate<57.648900,0.000000,26.092500>}
cylinder{<0,0,0><0,0.036000,0>0.038100 translate<57.526100,0.000000,25.969600>}
box{<0,0,-0.038100><0.173736,0.036000,0.038100> rotate<0,-45.020348,0> translate<57.526100,0.000000,25.969600> }
cylinder{<0,0,0><0,0.036000,0>0.038100 translate<57.526100,0.000000,25.969600>}
cylinder{<0,0,0><0,0.036000,0>0.038100 translate<57.526100,0.000000,25.846700>}
box{<0,0,-0.038100><0.122900,0.036000,0.038100> rotate<0,-90.000000,0> translate<57.526100,0.000000,25.846700> }
cylinder{<0,0,0><0,0.036000,0>0.038100 translate<57.526100,0.000000,25.846700>}
cylinder{<0,0,0><0,0.036000,0>0.038100 translate<57.648900,0.000000,25.723900>}
box{<0,0,-0.038100><0.173665,0.036000,0.038100> rotate<0,44.997030,0> translate<57.526100,0.000000,25.846700> }
cylinder{<0,0,0><0,0.036000,0>0.038100 translate<57.648900,0.000000,25.723900>}
cylinder{<0,0,0><0,0.036000,0>0.038100 translate<57.894700,0.000000,25.723900>}
box{<0,0,-0.038100><0.245800,0.036000,0.038100> rotate<0,0.000000,0> translate<57.648900,0.000000,25.723900> }
cylinder{<0,0,0><0,0.036000,0>0.038100 translate<57.894700,0.000000,25.723900>}
cylinder{<0,0,0><0,0.036000,0>0.038100 translate<58.017500,0.000000,25.601000>}
box{<0,0,-0.038100><0.173736,0.036000,0.038100> rotate<0,45.020348,0> translate<57.894700,0.000000,25.723900> }
cylinder{<0,0,0><0,0.036000,0>0.038100 translate<58.017500,0.000000,25.601000>}
cylinder{<0,0,0><0,0.036000,0>0.038100 translate<58.017500,0.000000,25.478100>}
box{<0,0,-0.038100><0.122900,0.036000,0.038100> rotate<0,-90.000000,0> translate<58.017500,0.000000,25.478100> }
cylinder{<0,0,0><0,0.036000,0>0.038100 translate<58.017500,0.000000,25.478100>}
cylinder{<0,0,0><0,0.036000,0>0.038100 translate<57.894700,0.000000,25.355300>}
box{<0,0,-0.038100><0.173665,0.036000,0.038100> rotate<0,-44.997030,0> translate<57.894700,0.000000,25.355300> }
cylinder{<0,0,0><0,0.036000,0>0.038100 translate<57.894700,0.000000,25.355300>}
cylinder{<0,0,0><0,0.036000,0>0.038100 translate<57.648900,0.000000,25.355300>}
box{<0,0,-0.038100><0.245800,0.036000,0.038100> rotate<0,0.000000,0> translate<57.648900,0.000000,25.355300> }
cylinder{<0,0,0><0,0.036000,0>0.038100 translate<57.648900,0.000000,25.355300>}
cylinder{<0,0,0><0,0.036000,0>0.038100 translate<57.526100,0.000000,25.478100>}
box{<0,0,-0.038100><0.173665,0.036000,0.038100> rotate<0,44.997030,0> translate<57.526100,0.000000,25.478100> }
cylinder{<0,0,0><0,0.036000,0>0.038100 translate<58.274500,0.000000,25.355300>}
cylinder{<0,0,0><0,0.036000,0>0.038100 translate<58.274500,0.000000,26.092500>}
box{<0,0,-0.038100><0.737200,0.036000,0.038100> rotate<0,90.000000,0> translate<58.274500,0.000000,26.092500> }
cylinder{<0,0,0><0,0.036000,0>0.038100 translate<58.274500,0.000000,25.723900>}
cylinder{<0,0,0><0,0.036000,0>0.038100 translate<58.765900,0.000000,25.723900>}
box{<0,0,-0.038100><0.491400,0.036000,0.038100> rotate<0,0.000000,0> translate<58.274500,0.000000,25.723900> }
cylinder{<0,0,0><0,0.036000,0>0.038100 translate<58.765900,0.000000,26.092500>}
cylinder{<0,0,0><0,0.036000,0>0.038100 translate<58.765900,0.000000,25.355300>}
box{<0,0,-0.038100><0.737200,0.036000,0.038100> rotate<0,-90.000000,0> translate<58.765900,0.000000,25.355300> }
cylinder{<0,0,0><0,0.036000,0>0.038100 translate<59.391500,0.000000,26.092500>}
cylinder{<0,0,0><0,0.036000,0>0.038100 translate<59.145700,0.000000,26.092500>}
box{<0,0,-0.038100><0.245800,0.036000,0.038100> rotate<0,0.000000,0> translate<59.145700,0.000000,26.092500> }
cylinder{<0,0,0><0,0.036000,0>0.038100 translate<59.145700,0.000000,26.092500>}
cylinder{<0,0,0><0,0.036000,0>0.038100 translate<59.022900,0.000000,25.969600>}
box{<0,0,-0.038100><0.173736,0.036000,0.038100> rotate<0,-45.020348,0> translate<59.022900,0.000000,25.969600> }
cylinder{<0,0,0><0,0.036000,0>0.038100 translate<59.022900,0.000000,25.969600>}
cylinder{<0,0,0><0,0.036000,0>0.038100 translate<59.022900,0.000000,25.478100>}
box{<0,0,-0.038100><0.491500,0.036000,0.038100> rotate<0,-90.000000,0> translate<59.022900,0.000000,25.478100> }
cylinder{<0,0,0><0,0.036000,0>0.038100 translate<59.022900,0.000000,25.478100>}
cylinder{<0,0,0><0,0.036000,0>0.038100 translate<59.145700,0.000000,25.355300>}
box{<0,0,-0.038100><0.173665,0.036000,0.038100> rotate<0,44.997030,0> translate<59.022900,0.000000,25.478100> }
cylinder{<0,0,0><0,0.036000,0>0.038100 translate<59.145700,0.000000,25.355300>}
cylinder{<0,0,0><0,0.036000,0>0.038100 translate<59.391500,0.000000,25.355300>}
box{<0,0,-0.038100><0.245800,0.036000,0.038100> rotate<0,0.000000,0> translate<59.145700,0.000000,25.355300> }
cylinder{<0,0,0><0,0.036000,0>0.038100 translate<59.391500,0.000000,25.355300>}
cylinder{<0,0,0><0,0.036000,0>0.038100 translate<59.514300,0.000000,25.478100>}
box{<0,0,-0.038100><0.173665,0.036000,0.038100> rotate<0,-44.997030,0> translate<59.391500,0.000000,25.355300> }
cylinder{<0,0,0><0,0.036000,0>0.038100 translate<59.514300,0.000000,25.478100>}
cylinder{<0,0,0><0,0.036000,0>0.038100 translate<59.514300,0.000000,25.969600>}
box{<0,0,-0.038100><0.491500,0.036000,0.038100> rotate<0,90.000000,0> translate<59.514300,0.000000,25.969600> }
cylinder{<0,0,0><0,0.036000,0>0.038100 translate<59.514300,0.000000,25.969600>}
cylinder{<0,0,0><0,0.036000,0>0.038100 translate<59.391500,0.000000,26.092500>}
box{<0,0,-0.038100><0.173736,0.036000,0.038100> rotate<0,45.020348,0> translate<59.391500,0.000000,26.092500> }
cylinder{<0,0,0><0,0.036000,0>0.038100 translate<60.139900,0.000000,26.092500>}
cylinder{<0,0,0><0,0.036000,0>0.038100 translate<59.894100,0.000000,26.092500>}
box{<0,0,-0.038100><0.245800,0.036000,0.038100> rotate<0,0.000000,0> translate<59.894100,0.000000,26.092500> }
cylinder{<0,0,0><0,0.036000,0>0.038100 translate<59.894100,0.000000,26.092500>}
cylinder{<0,0,0><0,0.036000,0>0.038100 translate<59.771300,0.000000,25.969600>}
box{<0,0,-0.038100><0.173736,0.036000,0.038100> rotate<0,-45.020348,0> translate<59.771300,0.000000,25.969600> }
cylinder{<0,0,0><0,0.036000,0>0.038100 translate<59.771300,0.000000,25.969600>}
cylinder{<0,0,0><0,0.036000,0>0.038100 translate<59.771300,0.000000,25.478100>}
box{<0,0,-0.038100><0.491500,0.036000,0.038100> rotate<0,-90.000000,0> translate<59.771300,0.000000,25.478100> }
cylinder{<0,0,0><0,0.036000,0>0.038100 translate<59.771300,0.000000,25.478100>}
cylinder{<0,0,0><0,0.036000,0>0.038100 translate<59.894100,0.000000,25.355300>}
box{<0,0,-0.038100><0.173665,0.036000,0.038100> rotate<0,44.997030,0> translate<59.771300,0.000000,25.478100> }
cylinder{<0,0,0><0,0.036000,0>0.038100 translate<59.894100,0.000000,25.355300>}
cylinder{<0,0,0><0,0.036000,0>0.038100 translate<60.139900,0.000000,25.355300>}
box{<0,0,-0.038100><0.245800,0.036000,0.038100> rotate<0,0.000000,0> translate<59.894100,0.000000,25.355300> }
cylinder{<0,0,0><0,0.036000,0>0.038100 translate<60.139900,0.000000,25.355300>}
cylinder{<0,0,0><0,0.036000,0>0.038100 translate<60.262700,0.000000,25.478100>}
box{<0,0,-0.038100><0.173665,0.036000,0.038100> rotate<0,-44.997030,0> translate<60.139900,0.000000,25.355300> }
cylinder{<0,0,0><0,0.036000,0>0.038100 translate<60.262700,0.000000,25.478100>}
cylinder{<0,0,0><0,0.036000,0>0.038100 translate<60.262700,0.000000,25.969600>}
box{<0,0,-0.038100><0.491500,0.036000,0.038100> rotate<0,90.000000,0> translate<60.262700,0.000000,25.969600> }
cylinder{<0,0,0><0,0.036000,0>0.038100 translate<60.262700,0.000000,25.969600>}
cylinder{<0,0,0><0,0.036000,0>0.038100 translate<60.139900,0.000000,26.092500>}
box{<0,0,-0.038100><0.173736,0.036000,0.038100> rotate<0,45.020348,0> translate<60.139900,0.000000,26.092500> }
cylinder{<0,0,0><0,0.036000,0>0.038100 translate<60.765400,0.000000,25.355300>}
cylinder{<0,0,0><0,0.036000,0>0.038100 translate<60.765400,0.000000,26.092500>}
box{<0,0,-0.038100><0.737200,0.036000,0.038100> rotate<0,90.000000,0> translate<60.765400,0.000000,26.092500> }
cylinder{<0,0,0><0,0.036000,0>0.038100 translate<60.519700,0.000000,26.092500>}
cylinder{<0,0,0><0,0.036000,0>0.038100 translate<61.011100,0.000000,26.092500>}
box{<0,0,-0.038100><0.491400,0.036000,0.038100> rotate<0,0.000000,0> translate<60.519700,0.000000,26.092500> }
cylinder{<0,0,0><0,0.036000,0>0.038100 translate<56.430400,0.000000,9.069600>}
cylinder{<0,0,0><0,0.036000,0>0.038100 translate<56.307600,0.000000,9.192500>}
box{<0,0,-0.038100><0.173736,0.036000,0.038100> rotate<0,45.020348,0> translate<56.307600,0.000000,9.192500> }
cylinder{<0,0,0><0,0.036000,0>0.038100 translate<56.307600,0.000000,9.192500>}
cylinder{<0,0,0><0,0.036000,0>0.038100 translate<56.061800,0.000000,9.192500>}
box{<0,0,-0.038100><0.245800,0.036000,0.038100> rotate<0,0.000000,0> translate<56.061800,0.000000,9.192500> }
cylinder{<0,0,0><0,0.036000,0>0.038100 translate<56.061800,0.000000,9.192500>}
cylinder{<0,0,0><0,0.036000,0>0.038100 translate<55.939000,0.000000,9.069600>}
box{<0,0,-0.038100><0.173736,0.036000,0.038100> rotate<0,-45.020348,0> translate<55.939000,0.000000,9.069600> }
cylinder{<0,0,0><0,0.036000,0>0.038100 translate<55.939000,0.000000,9.069600>}
cylinder{<0,0,0><0,0.036000,0>0.038100 translate<55.939000,0.000000,8.578100>}
box{<0,0,-0.038100><0.491500,0.036000,0.038100> rotate<0,-90.000000,0> translate<55.939000,0.000000,8.578100> }
cylinder{<0,0,0><0,0.036000,0>0.038100 translate<55.939000,0.000000,8.578100>}
cylinder{<0,0,0><0,0.036000,0>0.038100 translate<56.061800,0.000000,8.455300>}
box{<0,0,-0.038100><0.173665,0.036000,0.038100> rotate<0,44.997030,0> translate<55.939000,0.000000,8.578100> }
cylinder{<0,0,0><0,0.036000,0>0.038100 translate<56.061800,0.000000,8.455300>}
cylinder{<0,0,0><0,0.036000,0>0.038100 translate<56.307600,0.000000,8.455300>}
box{<0,0,-0.038100><0.245800,0.036000,0.038100> rotate<0,0.000000,0> translate<56.061800,0.000000,8.455300> }
cylinder{<0,0,0><0,0.036000,0>0.038100 translate<56.307600,0.000000,8.455300>}
cylinder{<0,0,0><0,0.036000,0>0.038100 translate<56.430400,0.000000,8.578100>}
box{<0,0,-0.038100><0.173665,0.036000,0.038100> rotate<0,-44.997030,0> translate<56.307600,0.000000,8.455300> }
cylinder{<0,0,0><0,0.036000,0>0.038100 translate<56.687400,0.000000,8.455300>}
cylinder{<0,0,0><0,0.036000,0>0.038100 translate<56.687400,0.000000,9.192500>}
box{<0,0,-0.038100><0.737200,0.036000,0.038100> rotate<0,90.000000,0> translate<56.687400,0.000000,9.192500> }
cylinder{<0,0,0><0,0.036000,0>0.038100 translate<56.687400,0.000000,8.823900>}
cylinder{<0,0,0><0,0.036000,0>0.038100 translate<57.178800,0.000000,8.823900>}
box{<0,0,-0.038100><0.491400,0.036000,0.038100> rotate<0,0.000000,0> translate<56.687400,0.000000,8.823900> }
cylinder{<0,0,0><0,0.036000,0>0.038100 translate<57.178800,0.000000,9.192500>}
cylinder{<0,0,0><0,0.036000,0>0.038100 translate<57.178800,0.000000,8.455300>}
box{<0,0,-0.038100><0.737200,0.036000,0.038100> rotate<0,-90.000000,0> translate<57.178800,0.000000,8.455300> }
cylinder{<0,0,0><0,0.036000,0>0.038100 translate<57.435800,0.000000,8.455300>}
cylinder{<0,0,0><0,0.036000,0>0.038100 translate<57.681500,0.000000,8.455300>}
box{<0,0,-0.038100><0.245700,0.036000,0.038100> rotate<0,0.000000,0> translate<57.435800,0.000000,8.455300> }
cylinder{<0,0,0><0,0.036000,0>0.038100 translate<57.558600,0.000000,8.455300>}
cylinder{<0,0,0><0,0.036000,0>0.038100 translate<57.558600,0.000000,9.192500>}
box{<0,0,-0.038100><0.737200,0.036000,0.038100> rotate<0,90.000000,0> translate<57.558600,0.000000,9.192500> }
cylinder{<0,0,0><0,0.036000,0>0.038100 translate<57.435800,0.000000,9.192500>}
cylinder{<0,0,0><0,0.036000,0>0.038100 translate<57.681500,0.000000,9.192500>}
box{<0,0,-0.038100><0.245700,0.036000,0.038100> rotate<0,0.000000,0> translate<57.435800,0.000000,9.192500> }
cylinder{<0,0,0><0,0.036000,0>0.038100 translate<57.934700,0.000000,8.455300>}
cylinder{<0,0,0><0,0.036000,0>0.038100 translate<57.934700,0.000000,9.192500>}
box{<0,0,-0.038100><0.737200,0.036000,0.038100> rotate<0,90.000000,0> translate<57.934700,0.000000,9.192500> }
cylinder{<0,0,0><0,0.036000,0>0.038100 translate<57.934700,0.000000,9.192500>}
cylinder{<0,0,0><0,0.036000,0>0.038100 translate<58.303300,0.000000,9.192500>}
box{<0,0,-0.038100><0.368600,0.036000,0.038100> rotate<0,0.000000,0> translate<57.934700,0.000000,9.192500> }
cylinder{<0,0,0><0,0.036000,0>0.038100 translate<58.303300,0.000000,9.192500>}
cylinder{<0,0,0><0,0.036000,0>0.038100 translate<58.426100,0.000000,9.069600>}
box{<0,0,-0.038100><0.173736,0.036000,0.038100> rotate<0,45.020348,0> translate<58.303300,0.000000,9.192500> }
cylinder{<0,0,0><0,0.036000,0>0.038100 translate<58.426100,0.000000,9.069600>}
cylinder{<0,0,0><0,0.036000,0>0.038100 translate<58.426100,0.000000,8.823900>}
box{<0,0,-0.038100><0.245700,0.036000,0.038100> rotate<0,-90.000000,0> translate<58.426100,0.000000,8.823900> }
cylinder{<0,0,0><0,0.036000,0>0.038100 translate<58.426100,0.000000,8.823900>}
cylinder{<0,0,0><0,0.036000,0>0.038100 translate<58.303300,0.000000,8.701000>}
box{<0,0,-0.038100><0.173736,0.036000,0.038100> rotate<0,-45.020348,0> translate<58.303300,0.000000,8.701000> }
cylinder{<0,0,0><0,0.036000,0>0.038100 translate<58.303300,0.000000,8.701000>}
cylinder{<0,0,0><0,0.036000,0>0.038100 translate<57.934700,0.000000,8.701000>}
box{<0,0,-0.038100><0.368600,0.036000,0.038100> rotate<0,0.000000,0> translate<57.934700,0.000000,8.701000> }
cylinder{<0,0,0><0,0.036000,0>0.038100 translate<9.081100,0.000000,22.054200>}
cylinder{<0,0,0><0,0.036000,0>0.038100 translate<9.081100,0.000000,22.545600>}
box{<0,0,-0.038100><0.491400,0.036000,0.038100> rotate<0,90.000000,0> translate<9.081100,0.000000,22.545600> }
cylinder{<0,0,0><0,0.036000,0>0.038100 translate<8.835400,0.000000,24.042400>}
cylinder{<0,0,0><0,0.036000,0>0.038100 translate<8.712500,0.000000,23.919600>}
box{<0,0,-0.038100><0.173736,0.036000,0.038100> rotate<0,-44.973712,0> translate<8.712500,0.000000,23.919600> }
cylinder{<0,0,0><0,0.036000,0>0.038100 translate<8.712500,0.000000,23.919600>}
cylinder{<0,0,0><0,0.036000,0>0.038100 translate<8.712500,0.000000,23.673800>}
box{<0,0,-0.038100><0.245800,0.036000,0.038100> rotate<0,-90.000000,0> translate<8.712500,0.000000,23.673800> }
cylinder{<0,0,0><0,0.036000,0>0.038100 translate<8.712500,0.000000,23.673800>}
cylinder{<0,0,0><0,0.036000,0>0.038100 translate<8.835400,0.000000,23.551000>}
box{<0,0,-0.038100><0.173736,0.036000,0.038100> rotate<0,44.973712,0> translate<8.712500,0.000000,23.673800> }
cylinder{<0,0,0><0,0.036000,0>0.038100 translate<8.835400,0.000000,23.551000>}
cylinder{<0,0,0><0,0.036000,0>0.038100 translate<9.326900,0.000000,23.551000>}
box{<0,0,-0.038100><0.491500,0.036000,0.038100> rotate<0,0.000000,0> translate<8.835400,0.000000,23.551000> }
cylinder{<0,0,0><0,0.036000,0>0.038100 translate<9.326900,0.000000,23.551000>}
cylinder{<0,0,0><0,0.036000,0>0.038100 translate<9.449700,0.000000,23.673800>}
box{<0,0,-0.038100><0.173665,0.036000,0.038100> rotate<0,-44.997030,0> translate<9.326900,0.000000,23.551000> }
cylinder{<0,0,0><0,0.036000,0>0.038100 translate<9.449700,0.000000,23.673800>}
cylinder{<0,0,0><0,0.036000,0>0.038100 translate<9.449700,0.000000,23.919600>}
box{<0,0,-0.038100><0.245800,0.036000,0.038100> rotate<0,90.000000,0> translate<9.449700,0.000000,23.919600> }
cylinder{<0,0,0><0,0.036000,0>0.038100 translate<9.449700,0.000000,23.919600>}
cylinder{<0,0,0><0,0.036000,0>0.038100 translate<9.326900,0.000000,24.042400>}
box{<0,0,-0.038100><0.173665,0.036000,0.038100> rotate<0,44.997030,0> translate<9.326900,0.000000,24.042400> }
cylinder{<0,0,0><0,0.036000,0>0.038100 translate<9.449700,0.000000,24.299400>}
cylinder{<0,0,0><0,0.036000,0>0.038100 translate<8.958300,0.000000,24.299400>}
box{<0,0,-0.038100><0.491400,0.036000,0.038100> rotate<0,0.000000,0> translate<8.958300,0.000000,24.299400> }
cylinder{<0,0,0><0,0.036000,0>0.038100 translate<8.958300,0.000000,24.299400>}
cylinder{<0,0,0><0,0.036000,0>0.038100 translate<8.712500,0.000000,24.545100>}
box{<0,0,-0.038100><0.347543,0.036000,0.038100> rotate<0,44.985374,0> translate<8.712500,0.000000,24.545100> }
cylinder{<0,0,0><0,0.036000,0>0.038100 translate<8.712500,0.000000,24.545100>}
cylinder{<0,0,0><0,0.036000,0>0.038100 translate<8.958300,0.000000,24.790800>}
box{<0,0,-0.038100><0.347543,0.036000,0.038100> rotate<0,-44.985374,0> translate<8.712500,0.000000,24.545100> }
cylinder{<0,0,0><0,0.036000,0>0.038100 translate<8.958300,0.000000,24.790800>}
cylinder{<0,0,0><0,0.036000,0>0.038100 translate<9.449700,0.000000,24.790800>}
box{<0,0,-0.038100><0.491400,0.036000,0.038100> rotate<0,0.000000,0> translate<8.958300,0.000000,24.790800> }
cylinder{<0,0,0><0,0.036000,0>0.038100 translate<9.081100,0.000000,24.299400>}
cylinder{<0,0,0><0,0.036000,0>0.038100 translate<9.081100,0.000000,24.790800>}
box{<0,0,-0.038100><0.491400,0.036000,0.038100> rotate<0,90.000000,0> translate<9.081100,0.000000,24.790800> }
cylinder{<0,0,0><0,0.036000,0>0.038100 translate<9.449700,0.000000,25.047800>}
cylinder{<0,0,0><0,0.036000,0>0.038100 translate<8.712500,0.000000,25.047800>}
box{<0,0,-0.038100><0.737200,0.036000,0.038100> rotate<0,0.000000,0> translate<8.712500,0.000000,25.047800> }
cylinder{<0,0,0><0,0.036000,0>0.038100 translate<8.712500,0.000000,25.047800>}
cylinder{<0,0,0><0,0.036000,0>0.038100 translate<8.712500,0.000000,25.416400>}
box{<0,0,-0.038100><0.368600,0.036000,0.038100> rotate<0,90.000000,0> translate<8.712500,0.000000,25.416400> }
cylinder{<0,0,0><0,0.036000,0>0.038100 translate<8.712500,0.000000,25.416400>}
cylinder{<0,0,0><0,0.036000,0>0.038100 translate<8.835400,0.000000,25.539200>}
box{<0,0,-0.038100><0.173736,0.036000,0.038100> rotate<0,-44.973712,0> translate<8.712500,0.000000,25.416400> }
cylinder{<0,0,0><0,0.036000,0>0.038100 translate<8.835400,0.000000,25.539200>}
cylinder{<0,0,0><0,0.036000,0>0.038100 translate<9.081100,0.000000,25.539200>}
box{<0,0,-0.038100><0.245700,0.036000,0.038100> rotate<0,0.000000,0> translate<8.835400,0.000000,25.539200> }
cylinder{<0,0,0><0,0.036000,0>0.038100 translate<9.081100,0.000000,25.539200>}
cylinder{<0,0,0><0,0.036000,0>0.038100 translate<9.204000,0.000000,25.416400>}
box{<0,0,-0.038100><0.173736,0.036000,0.038100> rotate<0,44.973712,0> translate<9.081100,0.000000,25.539200> }
cylinder{<0,0,0><0,0.036000,0>0.038100 translate<9.204000,0.000000,25.416400>}
cylinder{<0,0,0><0,0.036000,0>0.038100 translate<9.204000,0.000000,25.047800>}
box{<0,0,-0.038100><0.368600,0.036000,0.038100> rotate<0,-90.000000,0> translate<9.204000,0.000000,25.047800> }
cylinder{<0,0,0><0,0.036000,0>0.038100 translate<9.081100,0.000000,26.544600>}
cylinder{<0,0,0><0,0.036000,0>0.038100 translate<9.081100,0.000000,27.036000>}
box{<0,0,-0.038100><0.491400,0.036000,0.038100> rotate<0,90.000000,0> translate<9.081100,0.000000,27.036000> }
cylinder{<0,0,0><0,0.036000,0>0.038100 translate<8.835400,0.000000,26.790300>}
cylinder{<0,0,0><0,0.036000,0>0.038100 translate<9.326900,0.000000,26.790300>}
box{<0,0,-0.038100><0.491500,0.036000,0.038100> rotate<0,0.000000,0> translate<8.835400,0.000000,26.790300> }
cylinder{<0,0,0><0,0.036000,0>0.038100 translate<51.988100,0.000000,2.173100>}
cylinder{<0,0,0><0,0.036000,0>0.038100 translate<51.988100,0.000000,2.910300>}
box{<0,0,-0.038100><0.737200,0.036000,0.038100> rotate<0,90.000000,0> translate<51.988100,0.000000,2.910300> }
cylinder{<0,0,0><0,0.036000,0>0.038100 translate<51.988100,0.000000,2.910300>}
cylinder{<0,0,0><0,0.036000,0>0.038100 translate<52.356700,0.000000,2.910300>}
box{<0,0,-0.038100><0.368600,0.036000,0.038100> rotate<0,0.000000,0> translate<51.988100,0.000000,2.910300> }
cylinder{<0,0,0><0,0.036000,0>0.038100 translate<52.356700,0.000000,2.910300>}
cylinder{<0,0,0><0,0.036000,0>0.038100 translate<52.479500,0.000000,2.787400>}
box{<0,0,-0.038100><0.173736,0.036000,0.038100> rotate<0,45.020348,0> translate<52.356700,0.000000,2.910300> }
cylinder{<0,0,0><0,0.036000,0>0.038100 translate<52.479500,0.000000,2.787400>}
cylinder{<0,0,0><0,0.036000,0>0.038100 translate<52.479500,0.000000,2.664500>}
box{<0,0,-0.038100><0.122900,0.036000,0.038100> rotate<0,-90.000000,0> translate<52.479500,0.000000,2.664500> }
cylinder{<0,0,0><0,0.036000,0>0.038100 translate<52.479500,0.000000,2.664500>}
cylinder{<0,0,0><0,0.036000,0>0.038100 translate<52.356700,0.000000,2.541700>}
box{<0,0,-0.038100><0.173665,0.036000,0.038100> rotate<0,-44.997030,0> translate<52.356700,0.000000,2.541700> }
cylinder{<0,0,0><0,0.036000,0>0.038100 translate<52.356700,0.000000,2.541700>}
cylinder{<0,0,0><0,0.036000,0>0.038100 translate<52.479500,0.000000,2.418800>}
box{<0,0,-0.038100><0.173736,0.036000,0.038100> rotate<0,45.020348,0> translate<52.356700,0.000000,2.541700> }
cylinder{<0,0,0><0,0.036000,0>0.038100 translate<52.479500,0.000000,2.418800>}
cylinder{<0,0,0><0,0.036000,0>0.038100 translate<52.479500,0.000000,2.295900>}
box{<0,0,-0.038100><0.122900,0.036000,0.038100> rotate<0,-90.000000,0> translate<52.479500,0.000000,2.295900> }
cylinder{<0,0,0><0,0.036000,0>0.038100 translate<52.479500,0.000000,2.295900>}
cylinder{<0,0,0><0,0.036000,0>0.038100 translate<52.356700,0.000000,2.173100>}
box{<0,0,-0.038100><0.173665,0.036000,0.038100> rotate<0,-44.997030,0> translate<52.356700,0.000000,2.173100> }
cylinder{<0,0,0><0,0.036000,0>0.038100 translate<52.356700,0.000000,2.173100>}
cylinder{<0,0,0><0,0.036000,0>0.038100 translate<51.988100,0.000000,2.173100>}
box{<0,0,-0.038100><0.368600,0.036000,0.038100> rotate<0,0.000000,0> translate<51.988100,0.000000,2.173100> }
cylinder{<0,0,0><0,0.036000,0>0.038100 translate<51.988100,0.000000,2.541700>}
cylinder{<0,0,0><0,0.036000,0>0.038100 translate<52.356700,0.000000,2.541700>}
box{<0,0,-0.038100><0.368600,0.036000,0.038100> rotate<0,0.000000,0> translate<51.988100,0.000000,2.541700> }
cylinder{<0,0,0><0,0.036000,0>0.038100 translate<52.736500,0.000000,2.173100>}
cylinder{<0,0,0><0,0.036000,0>0.038100 translate<52.736500,0.000000,2.664500>}
box{<0,0,-0.038100><0.491400,0.036000,0.038100> rotate<0,90.000000,0> translate<52.736500,0.000000,2.664500> }
cylinder{<0,0,0><0,0.036000,0>0.038100 translate<52.736500,0.000000,2.664500>}
cylinder{<0,0,0><0,0.036000,0>0.038100 translate<52.982200,0.000000,2.910300>}
box{<0,0,-0.038100><0.347543,0.036000,0.038100> rotate<0,-45.008687,0> translate<52.736500,0.000000,2.664500> }
cylinder{<0,0,0><0,0.036000,0>0.038100 translate<52.982200,0.000000,2.910300>}
cylinder{<0,0,0><0,0.036000,0>0.038100 translate<53.227900,0.000000,2.664500>}
box{<0,0,-0.038100><0.347543,0.036000,0.038100> rotate<0,45.008687,0> translate<52.982200,0.000000,2.910300> }
cylinder{<0,0,0><0,0.036000,0>0.038100 translate<53.227900,0.000000,2.664500>}
cylinder{<0,0,0><0,0.036000,0>0.038100 translate<53.227900,0.000000,2.173100>}
box{<0,0,-0.038100><0.491400,0.036000,0.038100> rotate<0,-90.000000,0> translate<53.227900,0.000000,2.173100> }
cylinder{<0,0,0><0,0.036000,0>0.038100 translate<52.736500,0.000000,2.541700>}
cylinder{<0,0,0><0,0.036000,0>0.038100 translate<53.227900,0.000000,2.541700>}
box{<0,0,-0.038100><0.491400,0.036000,0.038100> rotate<0,0.000000,0> translate<52.736500,0.000000,2.541700> }
cylinder{<0,0,0><0,0.036000,0>0.038100 translate<53.730600,0.000000,2.173100>}
cylinder{<0,0,0><0,0.036000,0>0.038100 translate<53.730600,0.000000,2.910300>}
box{<0,0,-0.038100><0.737200,0.036000,0.038100> rotate<0,90.000000,0> translate<53.730600,0.000000,2.910300> }
cylinder{<0,0,0><0,0.036000,0>0.038100 translate<53.484900,0.000000,2.910300>}
cylinder{<0,0,0><0,0.036000,0>0.038100 translate<53.976300,0.000000,2.910300>}
box{<0,0,-0.038100><0.491400,0.036000,0.038100> rotate<0,0.000000,0> translate<53.484900,0.000000,2.910300> }
cylinder{<0,0,0><0,0.036000,0>0.038100 translate<54.724700,0.000000,2.910300>}
cylinder{<0,0,0><0,0.036000,0>0.038100 translate<54.233300,0.000000,2.910300>}
box{<0,0,-0.038100><0.491400,0.036000,0.038100> rotate<0,0.000000,0> translate<54.233300,0.000000,2.910300> }
cylinder{<0,0,0><0,0.036000,0>0.038100 translate<54.233300,0.000000,2.910300>}
cylinder{<0,0,0><0,0.036000,0>0.038100 translate<54.233300,0.000000,2.173100>}
box{<0,0,-0.038100><0.737200,0.036000,0.038100> rotate<0,-90.000000,0> translate<54.233300,0.000000,2.173100> }
cylinder{<0,0,0><0,0.036000,0>0.038100 translate<54.233300,0.000000,2.173100>}
cylinder{<0,0,0><0,0.036000,0>0.038100 translate<54.724700,0.000000,2.173100>}
box{<0,0,-0.038100><0.491400,0.036000,0.038100> rotate<0,0.000000,0> translate<54.233300,0.000000,2.173100> }
cylinder{<0,0,0><0,0.036000,0>0.038100 translate<54.233300,0.000000,2.541700>}
cylinder{<0,0,0><0,0.036000,0>0.038100 translate<54.479000,0.000000,2.541700>}
box{<0,0,-0.038100><0.245700,0.036000,0.038100> rotate<0,0.000000,0> translate<54.233300,0.000000,2.541700> }
cylinder{<0,0,0><0,0.036000,0>0.038100 translate<54.981700,0.000000,2.173100>}
cylinder{<0,0,0><0,0.036000,0>0.038100 translate<54.981700,0.000000,2.910300>}
box{<0,0,-0.038100><0.737200,0.036000,0.038100> rotate<0,90.000000,0> translate<54.981700,0.000000,2.910300> }
cylinder{<0,0,0><0,0.036000,0>0.038100 translate<54.981700,0.000000,2.910300>}
cylinder{<0,0,0><0,0.036000,0>0.038100 translate<55.350300,0.000000,2.910300>}
box{<0,0,-0.038100><0.368600,0.036000,0.038100> rotate<0,0.000000,0> translate<54.981700,0.000000,2.910300> }
cylinder{<0,0,0><0,0.036000,0>0.038100 translate<55.350300,0.000000,2.910300>}
cylinder{<0,0,0><0,0.036000,0>0.038100 translate<55.473100,0.000000,2.787400>}
box{<0,0,-0.038100><0.173736,0.036000,0.038100> rotate<0,45.020348,0> translate<55.350300,0.000000,2.910300> }
cylinder{<0,0,0><0,0.036000,0>0.038100 translate<55.473100,0.000000,2.787400>}
cylinder{<0,0,0><0,0.036000,0>0.038100 translate<55.473100,0.000000,2.541700>}
box{<0,0,-0.038100><0.245700,0.036000,0.038100> rotate<0,-90.000000,0> translate<55.473100,0.000000,2.541700> }
cylinder{<0,0,0><0,0.036000,0>0.038100 translate<55.473100,0.000000,2.541700>}
cylinder{<0,0,0><0,0.036000,0>0.038100 translate<55.350300,0.000000,2.418800>}
box{<0,0,-0.038100><0.173736,0.036000,0.038100> rotate<0,-45.020348,0> translate<55.350300,0.000000,2.418800> }
cylinder{<0,0,0><0,0.036000,0>0.038100 translate<55.350300,0.000000,2.418800>}
cylinder{<0,0,0><0,0.036000,0>0.038100 translate<54.981700,0.000000,2.418800>}
box{<0,0,-0.038100><0.368600,0.036000,0.038100> rotate<0,0.000000,0> translate<54.981700,0.000000,2.418800> }
cylinder{<0,0,0><0,0.036000,0>0.038100 translate<55.227400,0.000000,2.418800>}
cylinder{<0,0,0><0,0.036000,0>0.038100 translate<55.473100,0.000000,2.173100>}
box{<0,0,-0.038100><0.347472,0.036000,0.038100> rotate<0,44.997030,0> translate<55.227400,0.000000,2.418800> }
cylinder{<0,0,0><0,0.036000,0>0.038100 translate<55.730100,0.000000,2.173100>}
cylinder{<0,0,0><0,0.036000,0>0.038100 translate<55.975800,0.000000,2.173100>}
box{<0,0,-0.038100><0.245700,0.036000,0.038100> rotate<0,0.000000,0> translate<55.730100,0.000000,2.173100> }
cylinder{<0,0,0><0,0.036000,0>0.038100 translate<55.852900,0.000000,2.173100>}
cylinder{<0,0,0><0,0.036000,0>0.038100 translate<55.852900,0.000000,2.910300>}
box{<0,0,-0.038100><0.737200,0.036000,0.038100> rotate<0,90.000000,0> translate<55.852900,0.000000,2.910300> }
cylinder{<0,0,0><0,0.036000,0>0.038100 translate<55.730100,0.000000,2.910300>}
cylinder{<0,0,0><0,0.036000,0>0.038100 translate<55.975800,0.000000,2.910300>}
box{<0,0,-0.038100><0.245700,0.036000,0.038100> rotate<0,0.000000,0> translate<55.730100,0.000000,2.910300> }
cylinder{<0,0,0><0,0.036000,0>0.038100 translate<56.229000,0.000000,2.173100>}
cylinder{<0,0,0><0,0.036000,0>0.038100 translate<56.229000,0.000000,2.664500>}
box{<0,0,-0.038100><0.491400,0.036000,0.038100> rotate<0,90.000000,0> translate<56.229000,0.000000,2.664500> }
cylinder{<0,0,0><0,0.036000,0>0.038100 translate<56.229000,0.000000,2.664500>}
cylinder{<0,0,0><0,0.036000,0>0.038100 translate<56.474700,0.000000,2.910300>}
box{<0,0,-0.038100><0.347543,0.036000,0.038100> rotate<0,-45.008687,0> translate<56.229000,0.000000,2.664500> }
cylinder{<0,0,0><0,0.036000,0>0.038100 translate<56.474700,0.000000,2.910300>}
cylinder{<0,0,0><0,0.036000,0>0.038100 translate<56.720400,0.000000,2.664500>}
box{<0,0,-0.038100><0.347543,0.036000,0.038100> rotate<0,45.008687,0> translate<56.474700,0.000000,2.910300> }
cylinder{<0,0,0><0,0.036000,0>0.038100 translate<56.720400,0.000000,2.664500>}
cylinder{<0,0,0><0,0.036000,0>0.038100 translate<56.720400,0.000000,2.173100>}
box{<0,0,-0.038100><0.491400,0.036000,0.038100> rotate<0,-90.000000,0> translate<56.720400,0.000000,2.173100> }
cylinder{<0,0,0><0,0.036000,0>0.038100 translate<56.229000,0.000000,2.541700>}
cylinder{<0,0,0><0,0.036000,0>0.038100 translate<56.720400,0.000000,2.541700>}
box{<0,0,-0.038100><0.491400,0.036000,0.038100> rotate<0,0.000000,0> translate<56.229000,0.000000,2.541700> }
cylinder{<0,0,0><0,0.036000,0>0.050800 translate<7.754900,0.000000,4.380800>}
cylinder{<0,0,0><0,0.036000,0>0.050800 translate<7.754900,0.000000,5.550100>}
box{<0,0,-0.050800><1.169300,0.036000,0.050800> rotate<0,90.000000,0> translate<7.754900,0.000000,5.550100> }
cylinder{<0,0,0><0,0.036000,0>0.050800 translate<7.754900,0.000000,5.550100>}
cylinder{<0,0,0><0,0.036000,0>0.050800 translate<8.339500,0.000000,5.550100>}
box{<0,0,-0.050800><0.584600,0.036000,0.050800> rotate<0,0.000000,0> translate<7.754900,0.000000,5.550100> }
cylinder{<0,0,0><0,0.036000,0>0.050800 translate<8.339500,0.000000,5.550100>}
cylinder{<0,0,0><0,0.036000,0>0.050800 translate<8.534400,0.000000,5.355200>}
box{<0,0,-0.050800><0.275630,0.036000,0.050800> rotate<0,44.997030,0> translate<8.339500,0.000000,5.550100> }
cylinder{<0,0,0><0,0.036000,0>0.050800 translate<8.534400,0.000000,5.355200>}
cylinder{<0,0,0><0,0.036000,0>0.050800 translate<8.534400,0.000000,4.965400>}
box{<0,0,-0.050800><0.389800,0.036000,0.050800> rotate<0,-90.000000,0> translate<8.534400,0.000000,4.965400> }
cylinder{<0,0,0><0,0.036000,0>0.050800 translate<8.534400,0.000000,4.965400>}
cylinder{<0,0,0><0,0.036000,0>0.050800 translate<8.339500,0.000000,4.770500>}
box{<0,0,-0.050800><0.275630,0.036000,0.050800> rotate<0,-44.997030,0> translate<8.339500,0.000000,4.770500> }
cylinder{<0,0,0><0,0.036000,0>0.050800 translate<8.339500,0.000000,4.770500>}
cylinder{<0,0,0><0,0.036000,0>0.050800 translate<7.754900,0.000000,4.770500>}
box{<0,0,-0.050800><0.584600,0.036000,0.050800> rotate<0,0.000000,0> translate<7.754900,0.000000,4.770500> }
cylinder{<0,0,0><0,0.036000,0>0.050800 translate<8.144600,0.000000,4.770500>}
cylinder{<0,0,0><0,0.036000,0>0.050800 translate<8.534400,0.000000,4.380800>}
box{<0,0,-0.050800><0.551190,0.036000,0.050800> rotate<0,44.989680,0> translate<8.144600,0.000000,4.770500> }
cylinder{<0,0,0><0,0.036000,0>0.050800 translate<9.119000,0.000000,4.380800>}
cylinder{<0,0,0><0,0.036000,0>0.050800 translate<9.508800,0.000000,4.380800>}
box{<0,0,-0.050800><0.389800,0.036000,0.050800> rotate<0,0.000000,0> translate<9.119000,0.000000,4.380800> }
cylinder{<0,0,0><0,0.036000,0>0.050800 translate<9.508800,0.000000,4.380800>}
cylinder{<0,0,0><0,0.036000,0>0.050800 translate<9.703700,0.000000,4.575600>}
box{<0,0,-0.050800><0.275560,0.036000,0.050800> rotate<0,-44.982329,0> translate<9.508800,0.000000,4.380800> }
cylinder{<0,0,0><0,0.036000,0>0.050800 translate<9.703700,0.000000,4.575600>}
cylinder{<0,0,0><0,0.036000,0>0.050800 translate<9.703700,0.000000,4.965400>}
box{<0,0,-0.050800><0.389800,0.036000,0.050800> rotate<0,90.000000,0> translate<9.703700,0.000000,4.965400> }
cylinder{<0,0,0><0,0.036000,0>0.050800 translate<9.703700,0.000000,4.965400>}
cylinder{<0,0,0><0,0.036000,0>0.050800 translate<9.508800,0.000000,5.160300>}
box{<0,0,-0.050800><0.275630,0.036000,0.050800> rotate<0,44.997030,0> translate<9.508800,0.000000,5.160300> }
cylinder{<0,0,0><0,0.036000,0>0.050800 translate<9.508800,0.000000,5.160300>}
cylinder{<0,0,0><0,0.036000,0>0.050800 translate<9.119000,0.000000,5.160300>}
box{<0,0,-0.050800><0.389800,0.036000,0.050800> rotate<0,0.000000,0> translate<9.119000,0.000000,5.160300> }
cylinder{<0,0,0><0,0.036000,0>0.050800 translate<9.119000,0.000000,5.160300>}
cylinder{<0,0,0><0,0.036000,0>0.050800 translate<8.924200,0.000000,4.965400>}
box{<0,0,-0.050800><0.275560,0.036000,0.050800> rotate<0,-45.011732,0> translate<8.924200,0.000000,4.965400> }
cylinder{<0,0,0><0,0.036000,0>0.050800 translate<8.924200,0.000000,4.965400>}
cylinder{<0,0,0><0,0.036000,0>0.050800 translate<8.924200,0.000000,4.575600>}
box{<0,0,-0.050800><0.389800,0.036000,0.050800> rotate<0,-90.000000,0> translate<8.924200,0.000000,4.575600> }
cylinder{<0,0,0><0,0.036000,0>0.050800 translate<8.924200,0.000000,4.575600>}
cylinder{<0,0,0><0,0.036000,0>0.050800 translate<9.119000,0.000000,4.380800>}
box{<0,0,-0.050800><0.275489,0.036000,0.050800> rotate<0,44.997030,0> translate<8.924200,0.000000,4.575600> }
cylinder{<0,0,0><0,0.036000,0>0.050800 translate<10.093500,0.000000,5.550100>}
cylinder{<0,0,0><0,0.036000,0>0.050800 translate<10.093500,0.000000,4.380800>}
box{<0,0,-0.050800><1.169300,0.036000,0.050800> rotate<0,-90.000000,0> translate<10.093500,0.000000,4.380800> }
cylinder{<0,0,0><0,0.036000,0>0.050800 translate<10.093500,0.000000,4.380800>}
cylinder{<0,0,0><0,0.036000,0>0.050800 translate<10.678100,0.000000,4.380800>}
box{<0,0,-0.050800><0.584600,0.036000,0.050800> rotate<0,0.000000,0> translate<10.093500,0.000000,4.380800> }
cylinder{<0,0,0><0,0.036000,0>0.050800 translate<10.678100,0.000000,4.380800>}
cylinder{<0,0,0><0,0.036000,0>0.050800 translate<10.873000,0.000000,4.575600>}
box{<0,0,-0.050800><0.275560,0.036000,0.050800> rotate<0,-44.982329,0> translate<10.678100,0.000000,4.380800> }
cylinder{<0,0,0><0,0.036000,0>0.050800 translate<10.873000,0.000000,4.575600>}
cylinder{<0,0,0><0,0.036000,0>0.050800 translate<10.873000,0.000000,4.965400>}
box{<0,0,-0.050800><0.389800,0.036000,0.050800> rotate<0,90.000000,0> translate<10.873000,0.000000,4.965400> }
cylinder{<0,0,0><0,0.036000,0>0.050800 translate<10.873000,0.000000,4.965400>}
cylinder{<0,0,0><0,0.036000,0>0.050800 translate<10.678100,0.000000,5.160300>}
box{<0,0,-0.050800><0.275630,0.036000,0.050800> rotate<0,44.997030,0> translate<10.678100,0.000000,5.160300> }
cylinder{<0,0,0><0,0.036000,0>0.050800 translate<10.678100,0.000000,5.160300>}
cylinder{<0,0,0><0,0.036000,0>0.050800 translate<10.093500,0.000000,5.160300>}
box{<0,0,-0.050800><0.584600,0.036000,0.050800> rotate<0,0.000000,0> translate<10.093500,0.000000,5.160300> }
cylinder{<0,0,0><0,0.036000,0>0.050800 translate<11.457600,0.000000,4.380800>}
cylinder{<0,0,0><0,0.036000,0>0.050800 translate<11.847400,0.000000,4.380800>}
box{<0,0,-0.050800><0.389800,0.036000,0.050800> rotate<0,0.000000,0> translate<11.457600,0.000000,4.380800> }
cylinder{<0,0,0><0,0.036000,0>0.050800 translate<11.847400,0.000000,4.380800>}
cylinder{<0,0,0><0,0.036000,0>0.050800 translate<12.042300,0.000000,4.575600>}
box{<0,0,-0.050800><0.275560,0.036000,0.050800> rotate<0,-44.982329,0> translate<11.847400,0.000000,4.380800> }
cylinder{<0,0,0><0,0.036000,0>0.050800 translate<12.042300,0.000000,4.575600>}
cylinder{<0,0,0><0,0.036000,0>0.050800 translate<12.042300,0.000000,4.965400>}
box{<0,0,-0.050800><0.389800,0.036000,0.050800> rotate<0,90.000000,0> translate<12.042300,0.000000,4.965400> }
cylinder{<0,0,0><0,0.036000,0>0.050800 translate<12.042300,0.000000,4.965400>}
cylinder{<0,0,0><0,0.036000,0>0.050800 translate<11.847400,0.000000,5.160300>}
box{<0,0,-0.050800><0.275630,0.036000,0.050800> rotate<0,44.997030,0> translate<11.847400,0.000000,5.160300> }
cylinder{<0,0,0><0,0.036000,0>0.050800 translate<11.847400,0.000000,5.160300>}
cylinder{<0,0,0><0,0.036000,0>0.050800 translate<11.457600,0.000000,5.160300>}
box{<0,0,-0.050800><0.389800,0.036000,0.050800> rotate<0,0.000000,0> translate<11.457600,0.000000,5.160300> }
cylinder{<0,0,0><0,0.036000,0>0.050800 translate<11.457600,0.000000,5.160300>}
cylinder{<0,0,0><0,0.036000,0>0.050800 translate<11.262800,0.000000,4.965400>}
box{<0,0,-0.050800><0.275560,0.036000,0.050800> rotate<0,-45.011732,0> translate<11.262800,0.000000,4.965400> }
cylinder{<0,0,0><0,0.036000,0>0.050800 translate<11.262800,0.000000,4.965400>}
cylinder{<0,0,0><0,0.036000,0>0.050800 translate<11.262800,0.000000,4.575600>}
box{<0,0,-0.050800><0.389800,0.036000,0.050800> rotate<0,-90.000000,0> translate<11.262800,0.000000,4.575600> }
cylinder{<0,0,0><0,0.036000,0>0.050800 translate<11.262800,0.000000,4.575600>}
cylinder{<0,0,0><0,0.036000,0>0.050800 translate<11.457600,0.000000,4.380800>}
box{<0,0,-0.050800><0.275489,0.036000,0.050800> rotate<0,44.997030,0> translate<11.262800,0.000000,4.575600> }
cylinder{<0,0,0><0,0.036000,0>0.050800 translate<12.432100,0.000000,4.380800>}
cylinder{<0,0,0><0,0.036000,0>0.050800 translate<12.432100,0.000000,5.550100>}
box{<0,0,-0.050800><1.169300,0.036000,0.050800> rotate<0,90.000000,0> translate<12.432100,0.000000,5.550100> }
cylinder{<0,0,0><0,0.036000,0>0.050800 translate<12.432100,0.000000,5.550100>}
cylinder{<0,0,0><0,0.036000,0>0.050800 translate<13.211600,0.000000,5.550100>}
box{<0,0,-0.050800><0.779500,0.036000,0.050800> rotate<0,0.000000,0> translate<12.432100,0.000000,5.550100> }
cylinder{<0,0,0><0,0.036000,0>0.050800 translate<12.432100,0.000000,4.965400>}
cylinder{<0,0,0><0,0.036000,0>0.050800 translate<12.821800,0.000000,4.965400>}
box{<0,0,-0.050800><0.389700,0.036000,0.050800> rotate<0,0.000000,0> translate<12.432100,0.000000,4.965400> }
cylinder{<0,0,0><0,0.036000,0>0.050800 translate<14.380900,0.000000,5.550100>}
cylinder{<0,0,0><0,0.036000,0>0.050800 translate<13.601400,0.000000,5.550100>}
box{<0,0,-0.050800><0.779500,0.036000,0.050800> rotate<0,0.000000,0> translate<13.601400,0.000000,5.550100> }
cylinder{<0,0,0><0,0.036000,0>0.050800 translate<13.601400,0.000000,5.550100>}
cylinder{<0,0,0><0,0.036000,0>0.050800 translate<13.601400,0.000000,4.380800>}
box{<0,0,-0.050800><1.169300,0.036000,0.050800> rotate<0,-90.000000,0> translate<13.601400,0.000000,4.380800> }
cylinder{<0,0,0><0,0.036000,0>0.050800 translate<13.601400,0.000000,4.380800>}
cylinder{<0,0,0><0,0.036000,0>0.050800 translate<14.380900,0.000000,4.380800>}
box{<0,0,-0.050800><0.779500,0.036000,0.050800> rotate<0,0.000000,0> translate<13.601400,0.000000,4.380800> }
cylinder{<0,0,0><0,0.036000,0>0.050800 translate<13.601400,0.000000,4.965400>}
cylinder{<0,0,0><0,0.036000,0>0.050800 translate<13.991100,0.000000,4.965400>}
box{<0,0,-0.050800><0.389700,0.036000,0.050800> rotate<0,0.000000,0> translate<13.601400,0.000000,4.965400> }
cylinder{<0,0,0><0,0.036000,0>0.050800 translate<14.770700,0.000000,4.380800>}
cylinder{<0,0,0><0,0.036000,0>0.050800 translate<15.160400,0.000000,4.380800>}
box{<0,0,-0.050800><0.389700,0.036000,0.050800> rotate<0,0.000000,0> translate<14.770700,0.000000,4.380800> }
cylinder{<0,0,0><0,0.036000,0>0.050800 translate<14.965500,0.000000,4.380800>}
cylinder{<0,0,0><0,0.036000,0>0.050800 translate<14.965500,0.000000,5.550100>}
box{<0,0,-0.050800><1.169300,0.036000,0.050800> rotate<0,90.000000,0> translate<14.965500,0.000000,5.550100> }
cylinder{<0,0,0><0,0.036000,0>0.050800 translate<14.770700,0.000000,5.550100>}
cylinder{<0,0,0><0,0.036000,0>0.050800 translate<15.160400,0.000000,5.550100>}
box{<0,0,-0.050800><0.389700,0.036000,0.050800> rotate<0,0.000000,0> translate<14.770700,0.000000,5.550100> }
cylinder{<0,0,0><0,0.036000,0>0.038100 translate<10.038100,0.000000,2.938100>}
cylinder{<0,0,0><0,0.036000,0>0.038100 translate<10.038100,0.000000,3.675300>}
box{<0,0,-0.038100><0.737200,0.036000,0.038100> rotate<0,90.000000,0> translate<10.038100,0.000000,3.675300> }
cylinder{<0,0,0><0,0.036000,0>0.038100 translate<10.038100,0.000000,3.675300>}
cylinder{<0,0,0><0,0.036000,0>0.038100 translate<10.529500,0.000000,3.675300>}
box{<0,0,-0.038100><0.491400,0.036000,0.038100> rotate<0,0.000000,0> translate<10.038100,0.000000,3.675300> }
cylinder{<0,0,0><0,0.036000,0>0.038100 translate<10.038100,0.000000,3.306700>}
cylinder{<0,0,0><0,0.036000,0>0.038100 translate<10.283800,0.000000,3.306700>}
box{<0,0,-0.038100><0.245700,0.036000,0.038100> rotate<0,0.000000,0> translate<10.038100,0.000000,3.306700> }
cylinder{<0,0,0><0,0.036000,0>0.038100 translate<10.786500,0.000000,3.429500>}
cylinder{<0,0,0><0,0.036000,0>0.038100 translate<11.032200,0.000000,3.675300>}
box{<0,0,-0.038100><0.347543,0.036000,0.038100> rotate<0,-45.008687,0> translate<10.786500,0.000000,3.429500> }
cylinder{<0,0,0><0,0.036000,0>0.038100 translate<11.032200,0.000000,3.675300>}
cylinder{<0,0,0><0,0.036000,0>0.038100 translate<11.032200,0.000000,2.938100>}
box{<0,0,-0.038100><0.737200,0.036000,0.038100> rotate<0,-90.000000,0> translate<11.032200,0.000000,2.938100> }
cylinder{<0,0,0><0,0.036000,0>0.038100 translate<10.786500,0.000000,2.938100>}
cylinder{<0,0,0><0,0.036000,0>0.038100 translate<11.277900,0.000000,2.938100>}
box{<0,0,-0.038100><0.491400,0.036000,0.038100> rotate<0,0.000000,0> translate<10.786500,0.000000,2.938100> }
cylinder{<0,0,0><0,0.036000,0>0.038100 translate<11.534900,0.000000,3.552400>}
cylinder{<0,0,0><0,0.036000,0>0.038100 translate<11.657700,0.000000,3.675300>}
box{<0,0,-0.038100><0.173736,0.036000,0.038100> rotate<0,-45.020348,0> translate<11.534900,0.000000,3.552400> }
cylinder{<0,0,0><0,0.036000,0>0.038100 translate<11.657700,0.000000,3.675300>}
cylinder{<0,0,0><0,0.036000,0>0.038100 translate<11.903500,0.000000,3.675300>}
box{<0,0,-0.038100><0.245800,0.036000,0.038100> rotate<0,0.000000,0> translate<11.657700,0.000000,3.675300> }
cylinder{<0,0,0><0,0.036000,0>0.038100 translate<11.903500,0.000000,3.675300>}
cylinder{<0,0,0><0,0.036000,0>0.038100 translate<12.026300,0.000000,3.552400>}
box{<0,0,-0.038100><0.173736,0.036000,0.038100> rotate<0,45.020348,0> translate<11.903500,0.000000,3.675300> }
cylinder{<0,0,0><0,0.036000,0>0.038100 translate<12.026300,0.000000,3.552400>}
cylinder{<0,0,0><0,0.036000,0>0.038100 translate<12.026300,0.000000,3.429500>}
box{<0,0,-0.038100><0.122900,0.036000,0.038100> rotate<0,-90.000000,0> translate<12.026300,0.000000,3.429500> }
cylinder{<0,0,0><0,0.036000,0>0.038100 translate<12.026300,0.000000,3.429500>}
cylinder{<0,0,0><0,0.036000,0>0.038100 translate<11.903500,0.000000,3.306700>}
box{<0,0,-0.038100><0.173665,0.036000,0.038100> rotate<0,-44.997030,0> translate<11.903500,0.000000,3.306700> }
cylinder{<0,0,0><0,0.036000,0>0.038100 translate<11.903500,0.000000,3.306700>}
cylinder{<0,0,0><0,0.036000,0>0.038100 translate<12.026300,0.000000,3.183800>}
box{<0,0,-0.038100><0.173736,0.036000,0.038100> rotate<0,45.020348,0> translate<11.903500,0.000000,3.306700> }
cylinder{<0,0,0><0,0.036000,0>0.038100 translate<12.026300,0.000000,3.183800>}
cylinder{<0,0,0><0,0.036000,0>0.038100 translate<12.026300,0.000000,3.060900>}
box{<0,0,-0.038100><0.122900,0.036000,0.038100> rotate<0,-90.000000,0> translate<12.026300,0.000000,3.060900> }
cylinder{<0,0,0><0,0.036000,0>0.038100 translate<12.026300,0.000000,3.060900>}
cylinder{<0,0,0><0,0.036000,0>0.038100 translate<11.903500,0.000000,2.938100>}
box{<0,0,-0.038100><0.173665,0.036000,0.038100> rotate<0,-44.997030,0> translate<11.903500,0.000000,2.938100> }
cylinder{<0,0,0><0,0.036000,0>0.038100 translate<11.903500,0.000000,2.938100>}
cylinder{<0,0,0><0,0.036000,0>0.038100 translate<11.657700,0.000000,2.938100>}
box{<0,0,-0.038100><0.245800,0.036000,0.038100> rotate<0,0.000000,0> translate<11.657700,0.000000,2.938100> }
cylinder{<0,0,0><0,0.036000,0>0.038100 translate<11.657700,0.000000,2.938100>}
cylinder{<0,0,0><0,0.036000,0>0.038100 translate<11.534900,0.000000,3.060900>}
box{<0,0,-0.038100><0.173665,0.036000,0.038100> rotate<0,44.997030,0> translate<11.534900,0.000000,3.060900> }
cylinder{<0,0,0><0,0.036000,0>0.038100 translate<11.534900,0.000000,3.060900>}
cylinder{<0,0,0><0,0.036000,0>0.038100 translate<11.534900,0.000000,3.183800>}
box{<0,0,-0.038100><0.122900,0.036000,0.038100> rotate<0,90.000000,0> translate<11.534900,0.000000,3.183800> }
cylinder{<0,0,0><0,0.036000,0>0.038100 translate<11.534900,0.000000,3.183800>}
cylinder{<0,0,0><0,0.036000,0>0.038100 translate<11.657700,0.000000,3.306700>}
box{<0,0,-0.038100><0.173736,0.036000,0.038100> rotate<0,-45.020348,0> translate<11.534900,0.000000,3.183800> }
cylinder{<0,0,0><0,0.036000,0>0.038100 translate<11.657700,0.000000,3.306700>}
cylinder{<0,0,0><0,0.036000,0>0.038100 translate<11.534900,0.000000,3.429500>}
box{<0,0,-0.038100><0.173665,0.036000,0.038100> rotate<0,44.997030,0> translate<11.534900,0.000000,3.429500> }
cylinder{<0,0,0><0,0.036000,0>0.038100 translate<11.534900,0.000000,3.429500>}
cylinder{<0,0,0><0,0.036000,0>0.038100 translate<11.534900,0.000000,3.552400>}
box{<0,0,-0.038100><0.122900,0.036000,0.038100> rotate<0,90.000000,0> translate<11.534900,0.000000,3.552400> }
cylinder{<0,0,0><0,0.036000,0>0.038100 translate<11.657700,0.000000,3.306700>}
cylinder{<0,0,0><0,0.036000,0>0.038100 translate<11.903500,0.000000,3.306700>}
box{<0,0,-0.038100><0.245800,0.036000,0.038100> rotate<0,0.000000,0> translate<11.657700,0.000000,3.306700> }
cylinder{<0,0,0><0,0.036000,0>0.038100 translate<12.283300,0.000000,3.060900>}
cylinder{<0,0,0><0,0.036000,0>0.038100 translate<12.283300,0.000000,3.552400>}
box{<0,0,-0.038100><0.491500,0.036000,0.038100> rotate<0,90.000000,0> translate<12.283300,0.000000,3.552400> }
cylinder{<0,0,0><0,0.036000,0>0.038100 translate<12.283300,0.000000,3.552400>}
cylinder{<0,0,0><0,0.036000,0>0.038100 translate<12.406100,0.000000,3.675300>}
box{<0,0,-0.038100><0.173736,0.036000,0.038100> rotate<0,-45.020348,0> translate<12.283300,0.000000,3.552400> }
cylinder{<0,0,0><0,0.036000,0>0.038100 translate<12.406100,0.000000,3.675300>}
cylinder{<0,0,0><0,0.036000,0>0.038100 translate<12.651900,0.000000,3.675300>}
box{<0,0,-0.038100><0.245800,0.036000,0.038100> rotate<0,0.000000,0> translate<12.406100,0.000000,3.675300> }
cylinder{<0,0,0><0,0.036000,0>0.038100 translate<12.651900,0.000000,3.675300>}
cylinder{<0,0,0><0,0.036000,0>0.038100 translate<12.774700,0.000000,3.552400>}
box{<0,0,-0.038100><0.173736,0.036000,0.038100> rotate<0,45.020348,0> translate<12.651900,0.000000,3.675300> }
cylinder{<0,0,0><0,0.036000,0>0.038100 translate<12.774700,0.000000,3.552400>}
cylinder{<0,0,0><0,0.036000,0>0.038100 translate<12.774700,0.000000,3.060900>}
box{<0,0,-0.038100><0.491500,0.036000,0.038100> rotate<0,-90.000000,0> translate<12.774700,0.000000,3.060900> }
cylinder{<0,0,0><0,0.036000,0>0.038100 translate<12.774700,0.000000,3.060900>}
cylinder{<0,0,0><0,0.036000,0>0.038100 translate<12.651900,0.000000,2.938100>}
box{<0,0,-0.038100><0.173665,0.036000,0.038100> rotate<0,-44.997030,0> translate<12.651900,0.000000,2.938100> }
cylinder{<0,0,0><0,0.036000,0>0.038100 translate<12.651900,0.000000,2.938100>}
cylinder{<0,0,0><0,0.036000,0>0.038100 translate<12.406100,0.000000,2.938100>}
box{<0,0,-0.038100><0.245800,0.036000,0.038100> rotate<0,0.000000,0> translate<12.406100,0.000000,2.938100> }
cylinder{<0,0,0><0,0.036000,0>0.038100 translate<12.406100,0.000000,2.938100>}
cylinder{<0,0,0><0,0.036000,0>0.038100 translate<12.283300,0.000000,3.060900>}
box{<0,0,-0.038100><0.173665,0.036000,0.038100> rotate<0,44.997030,0> translate<12.283300,0.000000,3.060900> }
cylinder{<0,0,0><0,0.036000,0>0.038100 translate<12.283300,0.000000,3.060900>}
cylinder{<0,0,0><0,0.036000,0>0.038100 translate<12.774700,0.000000,3.552400>}
box{<0,0,-0.038100><0.695015,0.036000,0.038100> rotate<0,-45.002859,0> translate<12.283300,0.000000,3.060900> }
cylinder{<0,0,0><0,0.036000,0>0.038100 translate<13.031700,0.000000,3.306700>}
cylinder{<0,0,0><0,0.036000,0>0.038100 translate<13.523100,0.000000,3.306700>}
box{<0,0,-0.038100><0.491400,0.036000,0.038100> rotate<0,0.000000,0> translate<13.031700,0.000000,3.306700> }
cylinder{<0,0,0><0,0.036000,0>0.038100 translate<14.271500,0.000000,2.938100>}
cylinder{<0,0,0><0,0.036000,0>0.038100 translate<13.780100,0.000000,2.938100>}
box{<0,0,-0.038100><0.491400,0.036000,0.038100> rotate<0,0.000000,0> translate<13.780100,0.000000,2.938100> }
cylinder{<0,0,0><0,0.036000,0>0.038100 translate<13.780100,0.000000,2.938100>}
cylinder{<0,0,0><0,0.036000,0>0.038100 translate<14.271500,0.000000,3.429500>}
box{<0,0,-0.038100><0.694945,0.036000,0.038100> rotate<0,-44.997030,0> translate<13.780100,0.000000,2.938100> }
cylinder{<0,0,0><0,0.036000,0>0.038100 translate<14.271500,0.000000,3.429500>}
cylinder{<0,0,0><0,0.036000,0>0.038100 translate<14.271500,0.000000,3.552400>}
box{<0,0,-0.038100><0.122900,0.036000,0.038100> rotate<0,90.000000,0> translate<14.271500,0.000000,3.552400> }
cylinder{<0,0,0><0,0.036000,0>0.038100 translate<14.271500,0.000000,3.552400>}
cylinder{<0,0,0><0,0.036000,0>0.038100 translate<14.148700,0.000000,3.675300>}
box{<0,0,-0.038100><0.173736,0.036000,0.038100> rotate<0,45.020348,0> translate<14.148700,0.000000,3.675300> }
cylinder{<0,0,0><0,0.036000,0>0.038100 translate<14.148700,0.000000,3.675300>}
cylinder{<0,0,0><0,0.036000,0>0.038100 translate<13.902900,0.000000,3.675300>}
box{<0,0,-0.038100><0.245800,0.036000,0.038100> rotate<0,0.000000,0> translate<13.902900,0.000000,3.675300> }
cylinder{<0,0,0><0,0.036000,0>0.038100 translate<13.902900,0.000000,3.675300>}
cylinder{<0,0,0><0,0.036000,0>0.038100 translate<13.780100,0.000000,3.552400>}
box{<0,0,-0.038100><0.173736,0.036000,0.038100> rotate<0,-45.020348,0> translate<13.780100,0.000000,3.552400> }
cylinder{<0,0,0><0,0.036000,0>0.038100 translate<14.528500,0.000000,3.060900>}
cylinder{<0,0,0><0,0.036000,0>0.038100 translate<14.528500,0.000000,3.552400>}
box{<0,0,-0.038100><0.491500,0.036000,0.038100> rotate<0,90.000000,0> translate<14.528500,0.000000,3.552400> }
cylinder{<0,0,0><0,0.036000,0>0.038100 translate<14.528500,0.000000,3.552400>}
cylinder{<0,0,0><0,0.036000,0>0.038100 translate<14.651300,0.000000,3.675300>}
box{<0,0,-0.038100><0.173736,0.036000,0.038100> rotate<0,-45.020348,0> translate<14.528500,0.000000,3.552400> }
cylinder{<0,0,0><0,0.036000,0>0.038100 translate<14.651300,0.000000,3.675300>}
cylinder{<0,0,0><0,0.036000,0>0.038100 translate<14.897100,0.000000,3.675300>}
box{<0,0,-0.038100><0.245800,0.036000,0.038100> rotate<0,0.000000,0> translate<14.651300,0.000000,3.675300> }
cylinder{<0,0,0><0,0.036000,0>0.038100 translate<14.897100,0.000000,3.675300>}
cylinder{<0,0,0><0,0.036000,0>0.038100 translate<15.019900,0.000000,3.552400>}
box{<0,0,-0.038100><0.173736,0.036000,0.038100> rotate<0,45.020348,0> translate<14.897100,0.000000,3.675300> }
cylinder{<0,0,0><0,0.036000,0>0.038100 translate<15.019900,0.000000,3.552400>}
cylinder{<0,0,0><0,0.036000,0>0.038100 translate<15.019900,0.000000,3.060900>}
box{<0,0,-0.038100><0.491500,0.036000,0.038100> rotate<0,-90.000000,0> translate<15.019900,0.000000,3.060900> }
cylinder{<0,0,0><0,0.036000,0>0.038100 translate<15.019900,0.000000,3.060900>}
cylinder{<0,0,0><0,0.036000,0>0.038100 translate<14.897100,0.000000,2.938100>}
box{<0,0,-0.038100><0.173665,0.036000,0.038100> rotate<0,-44.997030,0> translate<14.897100,0.000000,2.938100> }
cylinder{<0,0,0><0,0.036000,0>0.038100 translate<14.897100,0.000000,2.938100>}
cylinder{<0,0,0><0,0.036000,0>0.038100 translate<14.651300,0.000000,2.938100>}
box{<0,0,-0.038100><0.245800,0.036000,0.038100> rotate<0,0.000000,0> translate<14.651300,0.000000,2.938100> }
cylinder{<0,0,0><0,0.036000,0>0.038100 translate<14.651300,0.000000,2.938100>}
cylinder{<0,0,0><0,0.036000,0>0.038100 translate<14.528500,0.000000,3.060900>}
box{<0,0,-0.038100><0.173665,0.036000,0.038100> rotate<0,44.997030,0> translate<14.528500,0.000000,3.060900> }
cylinder{<0,0,0><0,0.036000,0>0.038100 translate<14.528500,0.000000,3.060900>}
cylinder{<0,0,0><0,0.036000,0>0.038100 translate<15.019900,0.000000,3.552400>}
box{<0,0,-0.038100><0.695015,0.036000,0.038100> rotate<0,-45.002859,0> translate<14.528500,0.000000,3.060900> }
cylinder{<0,0,0><0,0.036000,0>0.038100 translate<15.276900,0.000000,3.429500>}
cylinder{<0,0,0><0,0.036000,0>0.038100 translate<15.522600,0.000000,3.675300>}
box{<0,0,-0.038100><0.347543,0.036000,0.038100> rotate<0,-45.008687,0> translate<15.276900,0.000000,3.429500> }
cylinder{<0,0,0><0,0.036000,0>0.038100 translate<15.522600,0.000000,3.675300>}
cylinder{<0,0,0><0,0.036000,0>0.038100 translate<15.522600,0.000000,2.938100>}
box{<0,0,-0.038100><0.737200,0.036000,0.038100> rotate<0,-90.000000,0> translate<15.522600,0.000000,2.938100> }
cylinder{<0,0,0><0,0.036000,0>0.038100 translate<15.276900,0.000000,2.938100>}
cylinder{<0,0,0><0,0.036000,0>0.038100 translate<15.768300,0.000000,2.938100>}
box{<0,0,-0.038100><0.491400,0.036000,0.038100> rotate<0,0.000000,0> translate<15.276900,0.000000,2.938100> }
cylinder{<0,0,0><0,0.036000,0>0.038100 translate<16.025300,0.000000,3.060900>}
cylinder{<0,0,0><0,0.036000,0>0.038100 translate<16.025300,0.000000,3.552400>}
box{<0,0,-0.038100><0.491500,0.036000,0.038100> rotate<0,90.000000,0> translate<16.025300,0.000000,3.552400> }
cylinder{<0,0,0><0,0.036000,0>0.038100 translate<16.025300,0.000000,3.552400>}
cylinder{<0,0,0><0,0.036000,0>0.038100 translate<16.148100,0.000000,3.675300>}
box{<0,0,-0.038100><0.173736,0.036000,0.038100> rotate<0,-45.020348,0> translate<16.025300,0.000000,3.552400> }
cylinder{<0,0,0><0,0.036000,0>0.038100 translate<16.148100,0.000000,3.675300>}
cylinder{<0,0,0><0,0.036000,0>0.038100 translate<16.393900,0.000000,3.675300>}
box{<0,0,-0.038100><0.245800,0.036000,0.038100> rotate<0,0.000000,0> translate<16.148100,0.000000,3.675300> }
cylinder{<0,0,0><0,0.036000,0>0.038100 translate<16.393900,0.000000,3.675300>}
cylinder{<0,0,0><0,0.036000,0>0.038100 translate<16.516700,0.000000,3.552400>}
box{<0,0,-0.038100><0.173736,0.036000,0.038100> rotate<0,45.020348,0> translate<16.393900,0.000000,3.675300> }
cylinder{<0,0,0><0,0.036000,0>0.038100 translate<16.516700,0.000000,3.552400>}
cylinder{<0,0,0><0,0.036000,0>0.038100 translate<16.516700,0.000000,3.060900>}
box{<0,0,-0.038100><0.491500,0.036000,0.038100> rotate<0,-90.000000,0> translate<16.516700,0.000000,3.060900> }
cylinder{<0,0,0><0,0.036000,0>0.038100 translate<16.516700,0.000000,3.060900>}
cylinder{<0,0,0><0,0.036000,0>0.038100 translate<16.393900,0.000000,2.938100>}
box{<0,0,-0.038100><0.173665,0.036000,0.038100> rotate<0,-44.997030,0> translate<16.393900,0.000000,2.938100> }
cylinder{<0,0,0><0,0.036000,0>0.038100 translate<16.393900,0.000000,2.938100>}
cylinder{<0,0,0><0,0.036000,0>0.038100 translate<16.148100,0.000000,2.938100>}
box{<0,0,-0.038100><0.245800,0.036000,0.038100> rotate<0,0.000000,0> translate<16.148100,0.000000,2.938100> }
cylinder{<0,0,0><0,0.036000,0>0.038100 translate<16.148100,0.000000,2.938100>}
cylinder{<0,0,0><0,0.036000,0>0.038100 translate<16.025300,0.000000,3.060900>}
box{<0,0,-0.038100><0.173665,0.036000,0.038100> rotate<0,44.997030,0> translate<16.025300,0.000000,3.060900> }
cylinder{<0,0,0><0,0.036000,0>0.038100 translate<16.025300,0.000000,3.060900>}
cylinder{<0,0,0><0,0.036000,0>0.038100 translate<16.516700,0.000000,3.552400>}
box{<0,0,-0.038100><0.695015,0.036000,0.038100> rotate<0,-45.002859,0> translate<16.025300,0.000000,3.060900> }
cylinder{<0,0,0><0,0.036000,0>0.038100 translate<12.943100,0.000000,1.753100>}
cylinder{<0,0,0><0,0.036000,0>0.038100 translate<12.943100,0.000000,2.244500>}
box{<0,0,-0.038100><0.491400,0.036000,0.038100> rotate<0,90.000000,0> translate<12.943100,0.000000,2.244500> }
cylinder{<0,0,0><0,0.036000,0>0.038100 translate<12.943100,0.000000,1.998800>}
cylinder{<0,0,0><0,0.036000,0>0.038100 translate<13.188800,0.000000,2.244500>}
box{<0,0,-0.038100><0.347472,0.036000,0.038100> rotate<0,-44.997030,0> translate<12.943100,0.000000,1.998800> }
cylinder{<0,0,0><0,0.036000,0>0.038100 translate<13.188800,0.000000,2.244500>}
cylinder{<0,0,0><0,0.036000,0>0.038100 translate<13.311700,0.000000,2.244500>}
box{<0,0,-0.038100><0.122900,0.036000,0.038100> rotate<0,0.000000,0> translate<13.188800,0.000000,2.244500> }
cylinder{<0,0,0><0,0.036000,0>0.038100 translate<13.935300,0.000000,1.753100>}
cylinder{<0,0,0><0,0.036000,0>0.038100 translate<13.689500,0.000000,1.753100>}
box{<0,0,-0.038100><0.245800,0.036000,0.038100> rotate<0,0.000000,0> translate<13.689500,0.000000,1.753100> }
cylinder{<0,0,0><0,0.036000,0>0.038100 translate<13.689500,0.000000,1.753100>}
cylinder{<0,0,0><0,0.036000,0>0.038100 translate<13.566700,0.000000,1.875900>}
box{<0,0,-0.038100><0.173665,0.036000,0.038100> rotate<0,44.997030,0> translate<13.566700,0.000000,1.875900> }
cylinder{<0,0,0><0,0.036000,0>0.038100 translate<13.566700,0.000000,1.875900>}
cylinder{<0,0,0><0,0.036000,0>0.038100 translate<13.566700,0.000000,2.121700>}
box{<0,0,-0.038100><0.245800,0.036000,0.038100> rotate<0,90.000000,0> translate<13.566700,0.000000,2.121700> }
cylinder{<0,0,0><0,0.036000,0>0.038100 translate<13.566700,0.000000,2.121700>}
cylinder{<0,0,0><0,0.036000,0>0.038100 translate<13.689500,0.000000,2.244500>}
box{<0,0,-0.038100><0.173665,0.036000,0.038100> rotate<0,-44.997030,0> translate<13.566700,0.000000,2.121700> }
cylinder{<0,0,0><0,0.036000,0>0.038100 translate<13.689500,0.000000,2.244500>}
cylinder{<0,0,0><0,0.036000,0>0.038100 translate<13.935300,0.000000,2.244500>}
box{<0,0,-0.038100><0.245800,0.036000,0.038100> rotate<0,0.000000,0> translate<13.689500,0.000000,2.244500> }
cylinder{<0,0,0><0,0.036000,0>0.038100 translate<13.935300,0.000000,2.244500>}
cylinder{<0,0,0><0,0.036000,0>0.038100 translate<14.058100,0.000000,2.121700>}
box{<0,0,-0.038100><0.173665,0.036000,0.038100> rotate<0,44.997030,0> translate<13.935300,0.000000,2.244500> }
cylinder{<0,0,0><0,0.036000,0>0.038100 translate<14.058100,0.000000,2.121700>}
cylinder{<0,0,0><0,0.036000,0>0.038100 translate<14.058100,0.000000,1.998800>}
box{<0,0,-0.038100><0.122900,0.036000,0.038100> rotate<0,-90.000000,0> translate<14.058100,0.000000,1.998800> }
cylinder{<0,0,0><0,0.036000,0>0.038100 translate<14.058100,0.000000,1.998800>}
cylinder{<0,0,0><0,0.036000,0>0.038100 translate<13.566700,0.000000,1.998800>}
box{<0,0,-0.038100><0.491400,0.036000,0.038100> rotate<0,0.000000,0> translate<13.566700,0.000000,1.998800> }
cylinder{<0,0,0><0,0.036000,0>0.038100 translate<14.315100,0.000000,2.244500>}
cylinder{<0,0,0><0,0.036000,0>0.038100 translate<14.560800,0.000000,1.753100>}
box{<0,0,-0.038100><0.549402,0.036000,0.038100> rotate<0,63.430762,0> translate<14.315100,0.000000,2.244500> }
cylinder{<0,0,0><0,0.036000,0>0.038100 translate<14.560800,0.000000,1.753100>}
cylinder{<0,0,0><0,0.036000,0>0.038100 translate<14.806500,0.000000,2.244500>}
box{<0,0,-0.038100><0.549402,0.036000,0.038100> rotate<0,-63.430762,0> translate<14.560800,0.000000,1.753100> }
cylinder{<0,0,0><0,0.036000,0>0.038100 translate<15.063500,0.000000,1.753100>}
cylinder{<0,0,0><0,0.036000,0>0.038100 translate<15.063500,0.000000,1.875900>}
box{<0,0,-0.038100><0.122800,0.036000,0.038100> rotate<0,90.000000,0> translate<15.063500,0.000000,1.875900> }
cylinder{<0,0,0><0,0.036000,0>0.038100 translate<15.063500,0.000000,1.875900>}
cylinder{<0,0,0><0,0.036000,0>0.038100 translate<15.186300,0.000000,1.875900>}
box{<0,0,-0.038100><0.122800,0.036000,0.038100> rotate<0,0.000000,0> translate<15.063500,0.000000,1.875900> }
cylinder{<0,0,0><0,0.036000,0>0.038100 translate<15.186300,0.000000,1.875900>}
cylinder{<0,0,0><0,0.036000,0>0.038100 translate<15.186300,0.000000,1.753100>}
box{<0,0,-0.038100><0.122800,0.036000,0.038100> rotate<0,-90.000000,0> translate<15.186300,0.000000,1.753100> }
cylinder{<0,0,0><0,0.036000,0>0.038100 translate<15.186300,0.000000,1.753100>}
cylinder{<0,0,0><0,0.036000,0>0.038100 translate<15.063500,0.000000,1.753100>}
box{<0,0,-0.038100><0.122800,0.036000,0.038100> rotate<0,0.000000,0> translate<15.063500,0.000000,1.753100> }
cylinder{<0,0,0><0,0.036000,0>0.038100 translate<15.437700,0.000000,1.753100>}
cylinder{<0,0,0><0,0.036000,0>0.038100 translate<15.437700,0.000000,2.490300>}
box{<0,0,-0.038100><0.737200,0.036000,0.038100> rotate<0,90.000000,0> translate<15.437700,0.000000,2.490300> }
cylinder{<0,0,0><0,0.036000,0>0.038100 translate<15.437700,0.000000,2.490300>}
cylinder{<0,0,0><0,0.036000,0>0.038100 translate<15.806300,0.000000,2.490300>}
box{<0,0,-0.038100><0.368600,0.036000,0.038100> rotate<0,0.000000,0> translate<15.437700,0.000000,2.490300> }
cylinder{<0,0,0><0,0.036000,0>0.038100 translate<15.806300,0.000000,2.490300>}
cylinder{<0,0,0><0,0.036000,0>0.038100 translate<15.929100,0.000000,2.367400>}
box{<0,0,-0.038100><0.173736,0.036000,0.038100> rotate<0,45.020348,0> translate<15.806300,0.000000,2.490300> }
cylinder{<0,0,0><0,0.036000,0>0.038100 translate<15.929100,0.000000,2.367400>}
cylinder{<0,0,0><0,0.036000,0>0.038100 translate<15.929100,0.000000,2.244500>}
box{<0,0,-0.038100><0.122900,0.036000,0.038100> rotate<0,-90.000000,0> translate<15.929100,0.000000,2.244500> }
cylinder{<0,0,0><0,0.036000,0>0.038100 translate<15.929100,0.000000,2.244500>}
cylinder{<0,0,0><0,0.036000,0>0.038100 translate<15.806300,0.000000,2.121700>}
box{<0,0,-0.038100><0.173665,0.036000,0.038100> rotate<0,-44.997030,0> translate<15.806300,0.000000,2.121700> }
cylinder{<0,0,0><0,0.036000,0>0.038100 translate<15.806300,0.000000,2.121700>}
cylinder{<0,0,0><0,0.036000,0>0.038100 translate<15.929100,0.000000,1.998800>}
box{<0,0,-0.038100><0.173736,0.036000,0.038100> rotate<0,45.020348,0> translate<15.806300,0.000000,2.121700> }
cylinder{<0,0,0><0,0.036000,0>0.038100 translate<15.929100,0.000000,1.998800>}
cylinder{<0,0,0><0,0.036000,0>0.038100 translate<15.929100,0.000000,1.875900>}
box{<0,0,-0.038100><0.122900,0.036000,0.038100> rotate<0,-90.000000,0> translate<15.929100,0.000000,1.875900> }
cylinder{<0,0,0><0,0.036000,0>0.038100 translate<15.929100,0.000000,1.875900>}
cylinder{<0,0,0><0,0.036000,0>0.038100 translate<15.806300,0.000000,1.753100>}
box{<0,0,-0.038100><0.173665,0.036000,0.038100> rotate<0,-44.997030,0> translate<15.806300,0.000000,1.753100> }
cylinder{<0,0,0><0,0.036000,0>0.038100 translate<15.806300,0.000000,1.753100>}
cylinder{<0,0,0><0,0.036000,0>0.038100 translate<15.437700,0.000000,1.753100>}
box{<0,0,-0.038100><0.368600,0.036000,0.038100> rotate<0,0.000000,0> translate<15.437700,0.000000,1.753100> }
cylinder{<0,0,0><0,0.036000,0>0.038100 translate<15.437700,0.000000,2.121700>}
cylinder{<0,0,0><0,0.036000,0>0.038100 translate<15.806300,0.000000,2.121700>}
box{<0,0,-0.038100><0.368600,0.036000,0.038100> rotate<0,0.000000,0> translate<15.437700,0.000000,2.121700> }
cylinder{<0,0,0><0,0.036000,0>0.050000 translate<43.085900,0.000000,0.672300>}
cylinder{<0,0,0><0,0.036000,0>0.050000 translate<47.085900,0.000000,0.672300>}
box{<0,0,-0.050000><4.000000,0.036000,0.050000> rotate<0,0.000000,0> translate<43.085900,0.000000,0.672300> }
cylinder{<0,0,0><0,0.036000,0>0.050000 translate<47.085900,0.000000,0.672300>}
cylinder{<0,0,0><0,0.036000,0>0.050000 translate<45.150600,0.000000,4.092900>}
box{<0,0,-0.050000><3.930126,0.036000,0.050000> rotate<0,60.495783,0> translate<45.150600,0.000000,4.092900> }
cylinder{<0,0,0><0,0.036000,0>0.050000 translate<45.150600,0.000000,4.092900>}
cylinder{<0,0,0><0,0.036000,0>0.050000 translate<43.085900,0.000000,0.672300>}
box{<0,0,-0.050000><3.995434,0.036000,0.050000> rotate<0,-58.880616,0> translate<43.085900,0.000000,0.672300> }
cylinder{<0,0,0><0,0.036000,0>0.050000 translate<43.708400,0.000000,0.992800>}
cylinder{<0,0,0><0,0.036000,0>0.050000 translate<46.469600,0.000000,0.992800>}
box{<0,0,-0.050000><2.761200,0.036000,0.050000> rotate<0,0.000000,0> translate<43.708400,0.000000,0.992800> }
cylinder{<0,0,0><0,0.036000,0>0.050000 translate<46.469600,0.000000,0.992800>}
cylinder{<0,0,0><0,0.036000,0>0.050000 translate<45.126000,0.000000,3.353300>}
box{<0,0,-0.050000><2.716104,0.036000,0.050000> rotate<0,60.347423,0> translate<45.126000,0.000000,3.353300> }
cylinder{<0,0,0><0,0.036000,0>0.050000 translate<45.126000,0.000000,3.353300>}
cylinder{<0,0,0><0,0.036000,0>0.050000 translate<43.708400,0.000000,0.992800>}
box{<0,0,-0.050000><2.753461,0.036000,0.050000> rotate<0,-59.009153,0> translate<43.708400,0.000000,0.992800> }
cylinder{<0,0,0><0,0.036000,0>0.050000 translate<45.002700,0.000000,3.014300>}
cylinder{<0,0,0><0,0.036000,0>0.050000 translate<45.286200,0.000000,2.884900>}
box{<0,0,-0.050000><0.311635,0.036000,0.050000> rotate<0,24.532110,0> translate<45.002700,0.000000,3.014300> }
cylinder{<0,0,0><0,0.036000,0>0.050000 translate<45.286200,0.000000,2.884900>}
cylinder{<0,0,0><0,0.036000,0>0.050000 translate<44.891700,0.000000,2.022000>}
box{<0,0,-0.050000><0.948803,0.036000,0.050000> rotate<0,-65.426784,0> translate<44.891700,0.000000,2.022000> }
cylinder{<0,0,0><0,0.036000,0>0.050000 translate<44.891700,0.000000,2.022000>}
cylinder{<0,0,0><0,0.036000,0>0.050000 translate<45.434100,0.000000,2.305600>}
box{<0,0,-0.050000><0.612068,0.036000,0.050000> rotate<0,-27.601508,0> translate<44.891700,0.000000,2.022000> }
cylinder{<0,0,0><0,0.036000,0>0.050000 translate<45.434100,0.000000,2.305600>}
cylinder{<0,0,0><0,0.036000,0>0.050000 translate<45.119800,0.000000,1.313300>}
box{<0,0,-0.050000><1.040886,0.036000,0.050000> rotate<0,-72.420201,0> translate<45.119800,0.000000,1.313300> }
cylinder{<0,0,0><0,0.036000,0>0.050000 translate<45.119800,0.000000,1.313300>}
cylinder{<0,0,0><0,0.036000,0>0.050000 translate<45.236900,0.000000,1.307100>}
box{<0,0,-0.050000><0.117264,0.036000,0.050000> rotate<0,3.030564,0> translate<45.119800,0.000000,1.313300> }
cylinder{<0,0,0><0,0.036000,0>0.050000 translate<45.236900,0.000000,1.307100>}
cylinder{<0,0,0><0,0.036000,0>0.050000 translate<45.002700,0.000000,1.079000>}
box{<0,0,-0.050000><0.326924,0.036000,0.050000> rotate<0,-44.241112,0> translate<45.002700,0.000000,1.079000> }
cylinder{<0,0,0><0,0.036000,0>0.050000 translate<45.002700,0.000000,1.079000>}
cylinder{<0,0,0><0,0.036000,0>0.050000 translate<44.891700,0.000000,1.399600>}
box{<0,0,-0.050000><0.339272,0.036000,0.050000> rotate<0,70.898146,0> translate<44.891700,0.000000,1.399600> }
cylinder{<0,0,0><0,0.036000,0>0.050000 translate<44.891700,0.000000,1.399600>}
cylinder{<0,0,0><0,0.036000,0>0.050000 translate<45.015000,0.000000,1.350200>}
box{<0,0,-0.050000><0.132828,0.036000,0.050000> rotate<0,21.832009,0> translate<44.891700,0.000000,1.399600> }
cylinder{<0,0,0><0,0.036000,0>0.050000 translate<45.015000,0.000000,1.350200>}
cylinder{<0,0,0><0,0.036000,0>0.050000 translate<45.132100,0.000000,1.991200>}
box{<0,0,-0.050000><0.651608,0.036000,0.050000> rotate<0,-79.641923,0> translate<45.015000,0.000000,1.350200> }
cylinder{<0,0,0><0,0.036000,0>0.050000 translate<45.132100,0.000000,1.991200>}
cylinder{<0,0,0><0,0.036000,0>0.050000 translate<44.595900,0.000000,1.726200>}
box{<0,0,-0.050000><0.598110,0.036000,0.050000> rotate<0,-26.297702,0> translate<44.595900,0.000000,1.726200> }
cylinder{<0,0,0><0,0.036000,0>0.050000 translate<44.595900,0.000000,1.726200>}
cylinder{<0,0,0><0,0.036000,0>0.050000 translate<45.002700,0.000000,3.014300>}
box{<0,0,-0.050000><1.350810,0.036000,0.050000> rotate<0,-72.468371,0> translate<44.595900,0.000000,1.726200> }
//C1 silk screen
cylinder{<0,0,0><0,0.036000,0>0.050800 translate<46.765000,0.000000,14.613000>}
cylinder{<0,0,0><0,0.036000,0>0.050800 translate<44.835000,0.000000,14.613000>}
box{<0,0,-0.050800><1.930000,0.036000,0.050800> rotate<0,0.000000,0> translate<44.835000,0.000000,14.613000> }
cylinder{<0,0,0><0,0.036000,0>0.050800 translate<46.765000,0.000000,16.187000>}
cylinder{<0,0,0><0,0.036000,0>0.050800 translate<44.835000,0.000000,16.187000>}
box{<0,0,-0.050800><1.930000,0.036000,0.050800> rotate<0,0.000000,0> translate<44.835000,0.000000,16.187000> }
box{<-0.375000,0,-0.850000><0.375000,0.036000,0.850000> rotate<0,-180.000000,0> translate<47.126600,0.000000,15.400900>}
box{<-0.375000,0,-0.850000><0.375000,0.036000,0.850000> rotate<0,-180.000000,0> translate<44.473300,0.000000,15.399100>}
//C2 silk screen
cylinder{<0,0,0><0,0.036000,0>0.050800 translate<23.875000,0.000000,17.575000>}
cylinder{<0,0,0><0,0.036000,0>0.050800 translate<29.325000,0.000000,17.575000>}
box{<0,0,-0.050800><5.450000,0.036000,0.050800> rotate<0,0.000000,0> translate<23.875000,0.000000,17.575000> }
cylinder{<0,0,0><0,0.036000,0>0.050800 translate<29.325000,0.000000,11.425000>}
cylinder{<0,0,0><0,0.036000,0>0.050800 translate<23.875000,0.000000,11.425000>}
box{<0,0,-0.050800><5.450000,0.036000,0.050800> rotate<0,0.000000,0> translate<23.875000,0.000000,11.425000> }
box{<-0.300000,0,-3.150000><0.300000,0.036000,3.150000> rotate<0,-0.000000,0> translate<24.100000,0.000000,14.500000>}
box{<-0.300000,0,-3.150000><0.300000,0.036000,3.150000> rotate<0,-0.000000,0> translate<29.100000,0.000000,14.500000>}
//C3 silk screen
cylinder{<0,0,0><0,0.036000,0>0.050800 translate<-2.956000,0.000000,19.032000>}
cylinder{<0,0,0><0,0.036000,0>0.050800 translate<-2.244000,0.000000,19.032000>}
box{<0,0,-0.050800><0.712000,0.036000,0.050800> rotate<0,0.000000,0> translate<-2.956000,0.000000,19.032000> }
cylinder{<0,0,0><0,0.036000,0>0.050800 translate<-2.956000,0.000000,18.181000>}
cylinder{<0,0,0><0,0.036000,0>0.050800 translate<-2.244000,0.000000,18.181000>}
box{<0,0,-0.050800><0.712000,0.036000,0.050800> rotate<0,0.000000,0> translate<-2.956000,0.000000,18.181000> }
box{<-0.250000,0,-0.475000><0.250000,0.036000,0.475000> rotate<0,-0.000000,0> translate<-3.188100,0.000000,18.605100>}
box{<-0.250000,0,-0.475000><0.250000,0.036000,0.475000> rotate<0,-0.000000,0> translate<-2.019700,0.000000,18.605100>}
//C4 silk screen
cylinder{<0,0,0><0,0.036000,0>0.050800 translate<0.635000,0.000000,10.387000>}
cylinder{<0,0,0><0,0.036000,0>0.050800 translate<2.565000,0.000000,10.387000>}
box{<0,0,-0.050800><1.930000,0.036000,0.050800> rotate<0,0.000000,0> translate<0.635000,0.000000,10.387000> }
cylinder{<0,0,0><0,0.036000,0>0.050800 translate<0.635000,0.000000,8.813000>}
cylinder{<0,0,0><0,0.036000,0>0.050800 translate<2.565000,0.000000,8.813000>}
box{<0,0,-0.050800><1.930000,0.036000,0.050800> rotate<0,0.000000,0> translate<0.635000,0.000000,8.813000> }
box{<-0.375000,0,-0.850000><0.375000,0.036000,0.850000> rotate<0,-0.000000,0> translate<0.273200,0.000000,9.599100>}
box{<-0.375000,0,-0.850000><0.375000,0.036000,0.850000> rotate<0,-0.000000,0> translate<2.926700,0.000000,9.600900>}
//D1 silk screen
cylinder{<0,0,0><0,0.036000,0>0.050800 translate<4.960600,0.000000,19.495000>}
cylinder{<0,0,0><0,0.036000,0>0.050800 translate<-2.160600,0.000000,19.495000>}
box{<0,0,-0.050800><7.121200,0.036000,0.050800> rotate<0,0.000000,0> translate<-2.160600,0.000000,19.495000> }
cylinder{<0,0,0><0,0.036000,0>0.050800 translate<4.960600,0.000000,25.705000>}
cylinder{<0,0,0><0,0.036000,0>0.050800 translate<-2.160600,0.000000,25.705000>}
box{<0,0,-0.050800><7.121200,0.036000,0.050800> rotate<0,0.000000,0> translate<-2.160600,0.000000,25.705000> }
cylinder{<0,0,0><0,0.036000,0>0.050800 translate<4.960600,0.000000,25.705000>}
cylinder{<0,0,0><0,0.036000,0>0.050800 translate<4.960600,0.000000,19.495000>}
box{<0,0,-0.050800><6.210000,0.036000,0.050800> rotate<0,-90.000000,0> translate<4.960600,0.000000,19.495000> }
cylinder{<0,0,0><0,0.036000,0>0.050800 translate<-2.160600,0.000000,25.705000>}
cylinder{<0,0,0><0,0.036000,0>0.050800 translate<-2.160600,0.000000,19.495000>}
box{<0,0,-0.050800><6.210000,0.036000,0.050800> rotate<0,-90.000000,0> translate<-2.160600,0.000000,19.495000> }
cylinder{<0,0,0><0,0.036000,0>0.101600 translate<0.857000,0.000000,21.600000>}
cylinder{<0,0,0><0,0.036000,0>0.101600 translate<2.230000,0.000000,22.600000>}
box{<0,0,-0.101600><1.698567,0.036000,0.101600> rotate<0,-36.064673,0> translate<0.857000,0.000000,21.600000> }
cylinder{<0,0,0><0,0.036000,0>0.101600 translate<2.230000,0.000000,22.600000>}
cylinder{<0,0,0><0,0.036000,0>0.101600 translate<0.857000,0.000000,23.600000>}
box{<0,0,-0.101600><1.698567,0.036000,0.101600> rotate<0,36.064673,0> translate<0.857000,0.000000,23.600000> }
cylinder{<0,0,0><0,0.036000,0>0.101600 translate<0.857000,0.000000,23.600000>}
cylinder{<0,0,0><0,0.036000,0>0.101600 translate<0.857000,0.000000,21.600000>}
box{<0,0,-0.101600><2.000000,0.036000,0.101600> rotate<0,-90.000000,0> translate<0.857000,0.000000,21.600000> }
box{<-0.266700,0,-1.092200><0.266700,0.036000,1.092200> rotate<0,-180.000000,0> translate<5.227300,0.000000,22.600000>}
box{<-0.266700,0,-1.092200><0.266700,0.036000,1.092200> rotate<0,-180.000000,0> translate<-2.427300,0.000000,22.600000>}
box{<-0.625000,0,-3.100000><0.625000,0.036000,3.100000> rotate<0,-180.000000,0> translate<2.875000,0.000000,22.600000>}
//D2 silk screen
cylinder{<0,0,0><0,0.036000,0>0.050800 translate<0.060600,-1.536000,24.505000>}
cylinder{<0,0,0><0,0.036000,0>0.050800 translate<-4.460600,-1.536000,24.505000>}
box{<0,0,-0.050800><4.521200,0.036000,0.050800> rotate<0,0.000000,0> translate<-4.460600,-1.536000,24.505000> }
cylinder{<0,0,0><0,0.036000,0>0.050800 translate<0.060600,-1.536000,20.695000>}
cylinder{<0,0,0><0,0.036000,0>0.050800 translate<-4.460600,-1.536000,20.695000>}
box{<0,0,-0.050800><4.521200,0.036000,0.050800> rotate<0,0.000000,0> translate<-4.460600,-1.536000,20.695000> }
cylinder{<0,0,0><0,0.036000,0>0.050800 translate<0.060600,-1.536000,20.695000>}
cylinder{<0,0,0><0,0.036000,0>0.050800 translate<0.060600,-1.536000,24.505000>}
box{<0,0,-0.050800><3.810000,0.036000,0.050800> rotate<0,90.000000,0> translate<0.060600,-1.536000,24.505000> }
cylinder{<0,0,0><0,0.036000,0>0.050800 translate<-4.460600,-1.536000,20.695000>}
cylinder{<0,0,0><0,0.036000,0>0.050800 translate<-4.460600,-1.536000,24.505000>}
box{<0,0,-0.050800><3.810000,0.036000,0.050800> rotate<0,90.000000,0> translate<-4.460600,-1.536000,24.505000> }
cylinder{<0,0,0><0,0.036000,0>0.101600 translate<-2.393000,-1.536000,23.600000>}
cylinder{<0,0,0><0,0.036000,0>0.101600 translate<-1.370000,-1.536000,22.600000>}
box{<0,0,-0.101600><1.430569,0.036000,0.101600> rotate<0,44.345691,0> translate<-2.393000,-1.536000,23.600000> }
cylinder{<0,0,0><0,0.036000,0>0.101600 translate<-1.370000,-1.536000,22.600000>}
cylinder{<0,0,0><0,0.036000,0>0.101600 translate<-2.393000,-1.536000,21.600000>}
box{<0,0,-0.101600><1.430569,0.036000,0.101600> rotate<0,-44.345691,0> translate<-2.393000,-1.536000,21.600000> }
cylinder{<0,0,0><0,0.036000,0>0.101600 translate<-2.393000,-1.536000,21.600000>}
cylinder{<0,0,0><0,0.036000,0>0.101600 translate<-2.393000,-1.536000,23.600000>}
box{<0,0,-0.101600><2.000000,0.036000,0.101600> rotate<0,90.000000,0> translate<-2.393000,-1.536000,23.600000> }
box{<-0.266700,0,-1.092200><0.266700,0.036000,1.092200> rotate<0,-180.000000,0> translate<0.327300,-1.536000,22.600000>}
box{<-0.266700,0,-1.092200><0.266700,0.036000,1.092200> rotate<0,-180.000000,0> translate<-4.727300,-1.536000,22.600000>}
box{<-0.275000,0,-1.900000><0.275000,0.036000,1.900000> rotate<0,-180.000000,0> translate<-1.125000,-1.536000,22.600000>}
//D3 silk screen
cylinder{<0,0,0><0,0.036000,0>0.050800 translate<36.095000,-1.536000,17.360600>}
cylinder{<0,0,0><0,0.036000,0>0.050800 translate<36.095000,-1.536000,10.239400>}
box{<0,0,-0.050800><7.121200,0.036000,0.050800> rotate<0,-90.000000,0> translate<36.095000,-1.536000,10.239400> }
cylinder{<0,0,0><0,0.036000,0>0.050800 translate<42.305000,-1.536000,17.360600>}
cylinder{<0,0,0><0,0.036000,0>0.050800 translate<42.305000,-1.536000,10.239400>}
box{<0,0,-0.050800><7.121200,0.036000,0.050800> rotate<0,-90.000000,0> translate<42.305000,-1.536000,10.239400> }
cylinder{<0,0,0><0,0.036000,0>0.050800 translate<42.305000,-1.536000,17.360600>}
cylinder{<0,0,0><0,0.036000,0>0.050800 translate<36.095000,-1.536000,17.360600>}
box{<0,0,-0.050800><6.210000,0.036000,0.050800> rotate<0,0.000000,0> translate<36.095000,-1.536000,17.360600> }
cylinder{<0,0,0><0,0.036000,0>0.050800 translate<42.305000,-1.536000,10.239400>}
cylinder{<0,0,0><0,0.036000,0>0.050800 translate<36.095000,-1.536000,10.239400>}
box{<0,0,-0.050800><6.210000,0.036000,0.050800> rotate<0,0.000000,0> translate<36.095000,-1.536000,10.239400> }
cylinder{<0,0,0><0,0.036000,0>0.101600 translate<38.200000,-1.536000,13.257000>}
cylinder{<0,0,0><0,0.036000,0>0.101600 translate<39.200000,-1.536000,14.630000>}
box{<0,0,-0.101600><1.698567,0.036000,0.101600> rotate<0,-53.929387,0> translate<38.200000,-1.536000,13.257000> }
cylinder{<0,0,0><0,0.036000,0>0.101600 translate<39.200000,-1.536000,14.630000>}
cylinder{<0,0,0><0,0.036000,0>0.101600 translate<40.200000,-1.536000,13.257000>}
box{<0,0,-0.101600><1.698567,0.036000,0.101600> rotate<0,53.929387,0> translate<39.200000,-1.536000,14.630000> }
cylinder{<0,0,0><0,0.036000,0>0.101600 translate<40.200000,-1.536000,13.257000>}
cylinder{<0,0,0><0,0.036000,0>0.101600 translate<38.200000,-1.536000,13.257000>}
box{<0,0,-0.101600><2.000000,0.036000,0.101600> rotate<0,0.000000,0> translate<38.200000,-1.536000,13.257000> }
box{<-0.266700,0,-1.092200><0.266700,0.036000,1.092200> rotate<0,-270.000000,0> translate<39.200000,-1.536000,17.627300>}
box{<-0.266700,0,-1.092200><0.266700,0.036000,1.092200> rotate<0,-270.000000,0> translate<39.200000,-1.536000,9.972700>}
box{<-0.625000,0,-3.100000><0.625000,0.036000,3.100000> rotate<0,-270.000000,0> translate<39.200000,-1.536000,15.275000>}
//D4 silk screen
cylinder{<0,0,0><0,0.036000,0>0.050800 translate<49.305000,-1.536000,14.039400>}
cylinder{<0,0,0><0,0.036000,0>0.050800 translate<49.305000,-1.536000,21.160600>}
box{<0,0,-0.050800><7.121200,0.036000,0.050800> rotate<0,90.000000,0> translate<49.305000,-1.536000,21.160600> }
cylinder{<0,0,0><0,0.036000,0>0.050800 translate<43.095000,-1.536000,14.039400>}
cylinder{<0,0,0><0,0.036000,0>0.050800 translate<43.095000,-1.536000,21.160600>}
box{<0,0,-0.050800><7.121200,0.036000,0.050800> rotate<0,90.000000,0> translate<43.095000,-1.536000,21.160600> }
cylinder{<0,0,0><0,0.036000,0>0.050800 translate<43.095000,-1.536000,14.039400>}
cylinder{<0,0,0><0,0.036000,0>0.050800 translate<49.305000,-1.536000,14.039400>}
box{<0,0,-0.050800><6.210000,0.036000,0.050800> rotate<0,0.000000,0> translate<43.095000,-1.536000,14.039400> }
cylinder{<0,0,0><0,0.036000,0>0.050800 translate<43.095000,-1.536000,21.160600>}
cylinder{<0,0,0><0,0.036000,0>0.050800 translate<49.305000,-1.536000,21.160600>}
box{<0,0,-0.050800><6.210000,0.036000,0.050800> rotate<0,0.000000,0> translate<43.095000,-1.536000,21.160600> }
cylinder{<0,0,0><0,0.036000,0>0.101600 translate<47.200000,-1.536000,18.143000>}
cylinder{<0,0,0><0,0.036000,0>0.101600 translate<46.200000,-1.536000,16.770000>}
box{<0,0,-0.101600><1.698567,0.036000,0.101600> rotate<0,-53.929387,0> translate<46.200000,-1.536000,16.770000> }
cylinder{<0,0,0><0,0.036000,0>0.101600 translate<46.200000,-1.536000,16.770000>}
cylinder{<0,0,0><0,0.036000,0>0.101600 translate<45.200000,-1.536000,18.143000>}
box{<0,0,-0.101600><1.698567,0.036000,0.101600> rotate<0,53.929387,0> translate<45.200000,-1.536000,18.143000> }
cylinder{<0,0,0><0,0.036000,0>0.101600 translate<45.200000,-1.536000,18.143000>}
cylinder{<0,0,0><0,0.036000,0>0.101600 translate<47.200000,-1.536000,18.143000>}
box{<0,0,-0.101600><2.000000,0.036000,0.101600> rotate<0,0.000000,0> translate<45.200000,-1.536000,18.143000> }
box{<-0.266700,0,-1.092200><0.266700,0.036000,1.092200> rotate<0,-90.000000,0> translate<46.200000,-1.536000,13.772700>}
box{<-0.266700,0,-1.092200><0.266700,0.036000,1.092200> rotate<0,-90.000000,0> translate<46.200000,-1.536000,21.427300>}
box{<-0.625000,0,-3.100000><0.625000,0.036000,3.100000> rotate<0,-90.000000,0> translate<46.200000,-1.536000,16.125000>}
//F1 silk screen
cylinder{<0,0,0><0,0.036000,0>0.076200 translate<41.030000,0.000000,2.495000>}
cylinder{<0,0,0><0,0.036000,0>0.076200 translate<42.300000,0.000000,2.495000>}
box{<0,0,-0.076200><1.270000,0.036000,0.076200> rotate<0,0.000000,0> translate<41.030000,0.000000,2.495000> }
cylinder{<0,0,0><0,0.036000,0>0.076200 translate<42.300000,0.000000,6.305000>}
cylinder{<0,0,0><0,0.036000,0>0.076200 translate<42.300000,0.000000,2.495000>}
box{<0,0,-0.076200><3.810000,0.036000,0.076200> rotate<0,-90.000000,0> translate<42.300000,0.000000,2.495000> }
cylinder{<0,0,0><0,0.036000,0>0.076200 translate<42.300000,0.000000,6.305000>}
cylinder{<0,0,0><0,0.036000,0>0.076200 translate<41.030000,0.000000,6.305000>}
box{<0,0,-0.076200><1.270000,0.036000,0.076200> rotate<0,0.000000,0> translate<41.030000,0.000000,6.305000> }
cylinder{<0,0,0><0,0.036000,0>0.076200 translate<40.395000,0.000000,1.860000>}
cylinder{<0,0,0><0,0.036000,0>0.076200 translate<40.395000,0.000000,0.590000>}
box{<0,0,-0.076200><1.270000,0.036000,0.076200> rotate<0,-90.000000,0> translate<40.395000,0.000000,0.590000> }
cylinder{<0,0,0><0,0.036000,0>0.076200 translate<40.395000,0.000000,0.590000>}
cylinder{<0,0,0><0,0.036000,0>0.076200 translate<36.585000,0.000000,0.590000>}
box{<0,0,-0.076200><3.810000,0.036000,0.076200> rotate<0,0.000000,0> translate<36.585000,0.000000,0.590000> }
cylinder{<0,0,0><0,0.036000,0>0.076200 translate<36.585000,0.000000,1.860000>}
cylinder{<0,0,0><0,0.036000,0>0.076200 translate<36.585000,0.000000,0.590000>}
box{<0,0,-0.076200><1.270000,0.036000,0.076200> rotate<0,-90.000000,0> translate<36.585000,0.000000,0.590000> }
cylinder{<0,0,0><0,0.036000,0>0.076200 translate<40.395000,0.000000,6.940000>}
cylinder{<0,0,0><0,0.036000,0>0.076200 translate<40.395000,0.000000,8.210000>}
box{<0,0,-0.076200><1.270000,0.036000,0.076200> rotate<0,90.000000,0> translate<40.395000,0.000000,8.210000> }
cylinder{<0,0,0><0,0.036000,0>0.076200 translate<36.585000,0.000000,8.210000>}
cylinder{<0,0,0><0,0.036000,0>0.076200 translate<40.395000,0.000000,8.210000>}
box{<0,0,-0.076200><3.810000,0.036000,0.076200> rotate<0,0.000000,0> translate<36.585000,0.000000,8.210000> }
cylinder{<0,0,0><0,0.036000,0>0.076200 translate<36.585000,0.000000,8.210000>}
cylinder{<0,0,0><0,0.036000,0>0.076200 translate<36.585000,0.000000,6.940000>}
box{<0,0,-0.076200><1.270000,0.036000,0.076200> rotate<0,-90.000000,0> translate<36.585000,0.000000,6.940000> }
cylinder{<0,0,0><0,0.036000,0>0.076200 translate<35.950000,0.000000,2.495000>}
cylinder{<0,0,0><0,0.036000,0>0.076200 translate<34.680000,0.000000,2.495000>}
box{<0,0,-0.076200><1.270000,0.036000,0.076200> rotate<0,0.000000,0> translate<34.680000,0.000000,2.495000> }
cylinder{<0,0,0><0,0.036000,0>0.076200 translate<34.680000,0.000000,2.495000>}
cylinder{<0,0,0><0,0.036000,0>0.076200 translate<34.680000,0.000000,2.876000>}
box{<0,0,-0.076200><0.381000,0.036000,0.076200> rotate<0,90.000000,0> translate<34.680000,0.000000,2.876000> }
cylinder{<0,0,0><0,0.036000,0>0.076200 translate<34.680000,0.000000,6.305000>}
cylinder{<0,0,0><0,0.036000,0>0.076200 translate<35.950000,0.000000,6.305000>}
box{<0,0,-0.076200><1.270000,0.036000,0.076200> rotate<0,0.000000,0> translate<34.680000,0.000000,6.305000> }
cylinder{<0,0,0><0,0.036000,0>0.076200 translate<23.250000,0.000000,2.495000>}
cylinder{<0,0,0><0,0.036000,0>0.076200 translate<24.520000,0.000000,2.495000>}
box{<0,0,-0.076200><1.270000,0.036000,0.076200> rotate<0,0.000000,0> translate<23.250000,0.000000,2.495000> }
cylinder{<0,0,0><0,0.036000,0>0.076200 translate<24.520000,0.000000,6.305000>}
cylinder{<0,0,0><0,0.036000,0>0.076200 translate<24.520000,0.000000,5.924000>}
box{<0,0,-0.076200><0.381000,0.036000,0.076200> rotate<0,-90.000000,0> translate<24.520000,0.000000,5.924000> }
cylinder{<0,0,0><0,0.036000,0>0.076200 translate<24.520000,0.000000,6.305000>}
cylinder{<0,0,0><0,0.036000,0>0.076200 translate<23.250000,0.000000,6.305000>}
box{<0,0,-0.076200><1.270000,0.036000,0.076200> rotate<0,0.000000,0> translate<23.250000,0.000000,6.305000> }
cylinder{<0,0,0><0,0.036000,0>0.076200 translate<22.615000,0.000000,1.860000>}
cylinder{<0,0,0><0,0.036000,0>0.076200 translate<22.615000,0.000000,0.590000>}
box{<0,0,-0.076200><1.270000,0.036000,0.076200> rotate<0,-90.000000,0> translate<22.615000,0.000000,0.590000> }
cylinder{<0,0,0><0,0.036000,0>0.076200 translate<22.615000,0.000000,0.590000>}
cylinder{<0,0,0><0,0.036000,0>0.076200 translate<18.805000,0.000000,0.590000>}
box{<0,0,-0.076200><3.810000,0.036000,0.076200> rotate<0,0.000000,0> translate<18.805000,0.000000,0.590000> }
cylinder{<0,0,0><0,0.036000,0>0.076200 translate<18.805000,0.000000,1.860000>}
cylinder{<0,0,0><0,0.036000,0>0.076200 translate<18.805000,0.000000,0.590000>}
box{<0,0,-0.076200><1.270000,0.036000,0.076200> rotate<0,-90.000000,0> translate<18.805000,0.000000,0.590000> }
cylinder{<0,0,0><0,0.036000,0>0.076200 translate<22.615000,0.000000,6.940000>}
cylinder{<0,0,0><0,0.036000,0>0.076200 translate<22.615000,0.000000,8.210000>}
box{<0,0,-0.076200><1.270000,0.036000,0.076200> rotate<0,90.000000,0> translate<22.615000,0.000000,8.210000> }
cylinder{<0,0,0><0,0.036000,0>0.076200 translate<18.805000,0.000000,8.210000>}
cylinder{<0,0,0><0,0.036000,0>0.076200 translate<22.615000,0.000000,8.210000>}
box{<0,0,-0.076200><3.810000,0.036000,0.076200> rotate<0,0.000000,0> translate<18.805000,0.000000,8.210000> }
cylinder{<0,0,0><0,0.036000,0>0.076200 translate<18.805000,0.000000,8.210000>}
cylinder{<0,0,0><0,0.036000,0>0.076200 translate<18.805000,0.000000,6.940000>}
box{<0,0,-0.076200><1.270000,0.036000,0.076200> rotate<0,-90.000000,0> translate<18.805000,0.000000,6.940000> }
cylinder{<0,0,0><0,0.036000,0>0.076200 translate<18.170000,0.000000,2.495000>}
cylinder{<0,0,0><0,0.036000,0>0.076200 translate<16.900000,0.000000,2.495000>}
box{<0,0,-0.076200><1.270000,0.036000,0.076200> rotate<0,0.000000,0> translate<16.900000,0.000000,2.495000> }
cylinder{<0,0,0><0,0.036000,0>0.076200 translate<16.900000,0.000000,2.495000>}
cylinder{<0,0,0><0,0.036000,0>0.076200 translate<16.900000,0.000000,6.305000>}
box{<0,0,-0.076200><3.810000,0.036000,0.076200> rotate<0,90.000000,0> translate<16.900000,0.000000,6.305000> }
cylinder{<0,0,0><0,0.036000,0>0.076200 translate<16.900000,0.000000,6.305000>}
cylinder{<0,0,0><0,0.036000,0>0.076200 translate<18.170000,0.000000,6.305000>}
box{<0,0,-0.076200><1.270000,0.036000,0.076200> rotate<0,0.000000,0> translate<16.900000,0.000000,6.305000> }
cylinder{<0,0,0><0,0.036000,0>0.076200 translate<35.315000,0.000000,1.606000>}
cylinder{<0,0,0><0,0.036000,0>0.076200 translate<23.885000,0.000000,1.606000>}
box{<0,0,-0.076200><11.430000,0.036000,0.076200> rotate<0,0.000000,0> translate<23.885000,0.000000,1.606000> }
cylinder{<0,0,0><0,0.036000,0>0.076200 translate<35.315000,0.000000,7.194000>}
cylinder{<0,0,0><0,0.036000,0>0.076200 translate<23.885000,0.000000,7.194000>}
box{<0,0,-0.076200><11.430000,0.036000,0.076200> rotate<0,0.000000,0> translate<23.885000,0.000000,7.194000> }
cylinder{<0,0,0><0,0.036000,0>0.076200 translate<34.680000,0.000000,2.876000>}
cylinder{<0,0,0><0,0.036000,0>0.076200 translate<24.520000,0.000000,5.924000>}
box{<0,0,-0.076200><10.607351,0.036000,0.076200> rotate<0,16.698142,0> translate<24.520000,0.000000,5.924000> }
cylinder{<0,0,0><0,0.036000,0>0.076200 translate<34.680000,0.000000,2.876000>}
cylinder{<0,0,0><0,0.036000,0>0.076200 translate<34.680000,0.000000,6.305000>}
box{<0,0,-0.076200><3.429000,0.036000,0.076200> rotate<0,90.000000,0> translate<34.680000,0.000000,6.305000> }
cylinder{<0,0,0><0,0.036000,0>0.076200 translate<24.520000,0.000000,5.924000>}
cylinder{<0,0,0><0,0.036000,0>0.076200 translate<24.520000,0.000000,2.495000>}
box{<0,0,-0.076200><3.429000,0.036000,0.076200> rotate<0,-90.000000,0> translate<24.520000,0.000000,2.495000> }
difference{
cylinder{<29.600000,0,4.400000><29.600000,0.036000,4.400000>0.203200 translate<0,0.000000,0>}
cylinder{<29.600000,-0.1,4.400000><29.600000,0.135000,4.400000>0.050800 translate<0,0.000000,0>}}
box{<-0.635000,0,-0.635000><0.635000,0.036000,0.635000> rotate<0,-180.000000,0> translate<41.030000,0.000000,1.860000>}
box{<-0.635000,0,-0.635000><0.635000,0.036000,0.635000> rotate<0,-180.000000,0> translate<41.030000,0.000000,6.940000>}
box{<-0.317500,0,-2.540000><0.317500,0.036000,2.540000> rotate<0,-180.000000,0> translate<40.712500,0.000000,4.400000>}
box{<-0.635000,0,-0.635000><0.635000,0.036000,0.635000> rotate<0,-180.000000,0> translate<35.950000,0.000000,1.860000>}
box{<-0.635000,0,-0.635000><0.635000,0.036000,0.635000> rotate<0,-180.000000,0> translate<35.950000,0.000000,6.940000>}
box{<-0.317500,0,-2.540000><0.317500,0.036000,2.540000> rotate<0,-180.000000,0> translate<36.267500,0.000000,4.400000>}
box{<-1.905000,0,-0.889000><1.905000,0.036000,0.889000> rotate<0,-180.000000,0> translate<38.490000,0.000000,2.749000>}
box{<-1.905000,0,-0.889000><1.905000,0.036000,0.889000> rotate<0,-180.000000,0> translate<38.490000,0.000000,6.051000>}
box{<-0.635000,0,-0.635000><0.635000,0.036000,0.635000> rotate<0,-180.000000,0> translate<23.250000,0.000000,1.860000>}
box{<-0.635000,0,-0.635000><0.635000,0.036000,0.635000> rotate<0,-180.000000,0> translate<23.250000,0.000000,6.940000>}
box{<-0.317500,0,-2.540000><0.317500,0.036000,2.540000> rotate<0,-180.000000,0> translate<22.932500,0.000000,4.400000>}
box{<-0.635000,0,-0.635000><0.635000,0.036000,0.635000> rotate<0,-180.000000,0> translate<18.170000,0.000000,1.860000>}
box{<-0.635000,0,-0.635000><0.635000,0.036000,0.635000> rotate<0,-180.000000,0> translate<18.170000,0.000000,6.940000>}
box{<-0.317500,0,-2.540000><0.317500,0.036000,2.540000> rotate<0,-180.000000,0> translate<18.487500,0.000000,4.400000>}
box{<-1.905000,0,-0.889000><1.905000,0.036000,0.889000> rotate<0,-180.000000,0> translate<20.710000,0.000000,2.749000>}
box{<-1.905000,0,-0.889000><1.905000,0.036000,0.889000> rotate<0,-180.000000,0> translate<20.710000,0.000000,6.051000>}
//IC1 silk screen
cylinder{<0,0,0><0,0.036000,0>0.063500 translate<23.600000,-1.536000,4.000000>}
cylinder{<0,0,0><0,0.036000,0>0.063500 translate<23.600000,-1.536000,8.800000>}
box{<0,0,-0.063500><4.800000,0.036000,0.063500> rotate<0,90.000000,0> translate<23.600000,-1.536000,8.800000> }
cylinder{<0,0,0><0,0.036000,0>0.063500 translate<23.600000,-1.536000,8.800000>}
cylinder{<0,0,0><0,0.036000,0>0.063500 translate<30.400000,-1.536000,8.800000>}
box{<0,0,-0.063500><6.800000,0.036000,0.063500> rotate<0,0.000000,0> translate<23.600000,-1.536000,8.800000> }
cylinder{<0,0,0><0,0.036000,0>0.063500 translate<30.400000,-1.536000,8.800000>}
cylinder{<0,0,0><0,0.036000,0>0.063500 translate<30.400000,-1.536000,4.000000>}
box{<0,0,-0.063500><4.800000,0.036000,0.063500> rotate<0,-90.000000,0> translate<30.400000,-1.536000,4.000000> }
cylinder{<0,0,0><0,0.036000,0>0.063500 translate<30.400000,-1.536000,4.000000>}
cylinder{<0,0,0><0,0.036000,0>0.063500 translate<23.600000,-1.536000,4.000000>}
box{<0,0,-0.063500><6.800000,0.036000,0.063500> rotate<0,0.000000,0> translate<23.600000,-1.536000,4.000000> }
object{ARC(0.635000,0.127000,180.000000,360.000000,0.036000) translate<28.905000,-1.536000,8.305000>}
cylinder{<0,0,0><0,0.036000,0>0.063500 translate<29.540000,-1.536000,8.305000>}
cylinder{<0,0,0><0,0.036000,0>0.063500 translate<29.540000,-1.536000,8.740000>}
box{<0,0,-0.063500><0.435000,0.036000,0.063500> rotate<0,90.000000,0> translate<29.540000,-1.536000,8.740000> }
cylinder{<0,0,0><0,0.036000,0>0.063500 translate<28.270000,-1.536000,8.305000>}
cylinder{<0,0,0><0,0.036000,0>0.063500 translate<28.270000,-1.536000,8.740000>}
box{<0,0,-0.063500><0.435000,0.036000,0.063500> rotate<0,90.000000,0> translate<28.270000,-1.536000,8.740000> }
box{<-0.437500,0,-1.075000><0.437500,0.036000,1.075000> rotate<0,-90.000000,0> translate<31.550000,-1.536000,5.112500>}
box{<-0.437500,0,-1.075000><0.437500,0.036000,1.075000> rotate<0,-90.000000,0> translate<31.550000,-1.536000,7.652500>}
box{<-0.437500,0,-1.075000><0.437500,0.036000,1.075000> rotate<0,-270.000000,0> translate<22.450000,-1.536000,5.147500>}
box{<-0.437500,0,-1.075000><0.437500,0.036000,1.075000> rotate<0,-270.000000,0> translate<22.450000,-1.536000,7.687500>}
//IC2 silk screen
cylinder{<0,0,0><0,0.036000,0>0.101600 translate<4.900000,-1.536000,20.600000>}
cylinder{<0,0,0><0,0.036000,0>0.101600 translate<8.200000,-1.536000,20.600000>}
box{<0,0,-0.101600><3.300000,0.036000,0.101600> rotate<0,0.000000,0> translate<4.900000,-1.536000,20.600000> }
cylinder{<0,0,0><0,0.036000,0>0.101600 translate<8.200000,-1.536000,20.600000>}
cylinder{<0,0,0><0,0.036000,0>0.101600 translate<8.700000,-1.536000,20.600000>}
box{<0,0,-0.101600><0.500000,0.036000,0.101600> rotate<0,0.000000,0> translate<8.200000,-1.536000,20.600000> }
cylinder{<0,0,0><0,0.036000,0>0.101600 translate<8.700000,-1.536000,20.600000>}
cylinder{<0,0,0><0,0.036000,0>0.101600 translate<8.700000,-1.536000,25.400000>}
box{<0,0,-0.101600><4.800000,0.036000,0.101600> rotate<0,90.000000,0> translate<8.700000,-1.536000,25.400000> }
cylinder{<0,0,0><0,0.036000,0>0.101600 translate<8.700000,-1.536000,25.400000>}
cylinder{<0,0,0><0,0.036000,0>0.101600 translate<8.200000,-1.536000,25.400000>}
box{<0,0,-0.101600><0.500000,0.036000,0.101600> rotate<0,0.000000,0> translate<8.200000,-1.536000,25.400000> }
cylinder{<0,0,0><0,0.036000,0>0.101600 translate<8.200000,-1.536000,25.400000>}
cylinder{<0,0,0><0,0.036000,0>0.101600 translate<4.900000,-1.536000,25.400000>}
box{<0,0,-0.101600><3.300000,0.036000,0.101600> rotate<0,0.000000,0> translate<4.900000,-1.536000,25.400000> }
cylinder{<0,0,0><0,0.036000,0>0.101600 translate<4.900000,-1.536000,25.400000>}
cylinder{<0,0,0><0,0.036000,0>0.101600 translate<4.900000,-1.536000,20.600000>}
box{<0,0,-0.101600><4.800000,0.036000,0.101600> rotate<0,-90.000000,0> translate<4.900000,-1.536000,20.600000> }
cylinder{<0,0,0><0,0.036000,0>0.101600 translate<8.200000,-1.536000,20.600000>}
cylinder{<0,0,0><0,0.036000,0>0.101600 translate<8.200000,-1.536000,25.400000>}
box{<0,0,-0.101600><4.800000,0.036000,0.101600> rotate<0,90.000000,0> translate<8.200000,-1.536000,25.400000> }
box{<-0.245000,0,-0.550000><0.245000,0.036000,0.550000> rotate<0,-270.000000,0> translate<9.350000,-1.536000,24.905000>}
box{<-0.245000,0,-0.550000><0.245000,0.036000,0.550000> rotate<0,-270.000000,0> translate<9.350000,-1.536000,23.635000>}
box{<-0.245000,0,-0.550000><0.245000,0.036000,0.550000> rotate<0,-270.000000,0> translate<9.350000,-1.536000,22.365000>}
box{<-0.245000,0,-0.550000><0.245000,0.036000,0.550000> rotate<0,-270.000000,0> translate<9.350000,-1.536000,21.095000>}
box{<-0.245000,0,-0.550000><0.245000,0.036000,0.550000> rotate<0,-270.000000,0> translate<4.250000,-1.536000,21.095000>}
box{<-0.245000,0,-0.550000><0.245000,0.036000,0.550000> rotate<0,-270.000000,0> translate<4.250000,-1.536000,22.365000>}
box{<-0.245000,0,-0.550000><0.245000,0.036000,0.550000> rotate<0,-270.000000,0> translate<4.250000,-1.536000,23.635000>}
box{<-0.245000,0,-0.550000><0.245000,0.036000,0.550000> rotate<0,-270.000000,0> translate<4.250000,-1.536000,24.905000>}
//IC3 silk screen
cylinder{<0,0,0><0,0.036000,0>0.063500 translate<23.600000,-1.536000,15.300000>}
cylinder{<0,0,0><0,0.036000,0>0.063500 translate<23.600000,-1.536000,20.100000>}
box{<0,0,-0.063500><4.800000,0.036000,0.063500> rotate<0,90.000000,0> translate<23.600000,-1.536000,20.100000> }
cylinder{<0,0,0><0,0.036000,0>0.063500 translate<23.600000,-1.536000,20.100000>}
cylinder{<0,0,0><0,0.036000,0>0.063500 translate<30.400000,-1.536000,20.100000>}
box{<0,0,-0.063500><6.800000,0.036000,0.063500> rotate<0,0.000000,0> translate<23.600000,-1.536000,20.100000> }
cylinder{<0,0,0><0,0.036000,0>0.063500 translate<30.400000,-1.536000,20.100000>}
cylinder{<0,0,0><0,0.036000,0>0.063500 translate<30.400000,-1.536000,15.300000>}
box{<0,0,-0.063500><4.800000,0.036000,0.063500> rotate<0,-90.000000,0> translate<30.400000,-1.536000,15.300000> }
cylinder{<0,0,0><0,0.036000,0>0.063500 translate<30.400000,-1.536000,15.300000>}
cylinder{<0,0,0><0,0.036000,0>0.063500 translate<23.600000,-1.536000,15.300000>}
box{<0,0,-0.063500><6.800000,0.036000,0.063500> rotate<0,0.000000,0> translate<23.600000,-1.536000,15.300000> }
object{ARC(0.635000,0.127000,180.000000,360.000000,0.036000) translate<28.905000,-1.536000,19.605000>}
cylinder{<0,0,0><0,0.036000,0>0.063500 translate<29.540000,-1.536000,19.605000>}
cylinder{<0,0,0><0,0.036000,0>0.063500 translate<29.540000,-1.536000,20.040000>}
box{<0,0,-0.063500><0.435000,0.036000,0.063500> rotate<0,90.000000,0> translate<29.540000,-1.536000,20.040000> }
cylinder{<0,0,0><0,0.036000,0>0.063500 translate<28.270000,-1.536000,19.605000>}
cylinder{<0,0,0><0,0.036000,0>0.063500 translate<28.270000,-1.536000,20.040000>}
box{<0,0,-0.063500><0.435000,0.036000,0.063500> rotate<0,90.000000,0> translate<28.270000,-1.536000,20.040000> }
box{<-0.437500,0,-1.075000><0.437500,0.036000,1.075000> rotate<0,-90.000000,0> translate<31.550000,-1.536000,16.412500>}
box{<-0.437500,0,-1.075000><0.437500,0.036000,1.075000> rotate<0,-90.000000,0> translate<31.550000,-1.536000,18.952500>}
box{<-0.437500,0,-1.075000><0.437500,0.036000,1.075000> rotate<0,-270.000000,0> translate<22.450000,-1.536000,16.447500>}
box{<-0.437500,0,-1.075000><0.437500,0.036000,1.075000> rotate<0,-270.000000,0> translate<22.450000,-1.536000,18.987500>}
//IC4 silk screen
cylinder{<0,0,0><0,0.036000,0>0.063500 translate<23.700000,-1.536000,9.700000>}
cylinder{<0,0,0><0,0.036000,0>0.063500 translate<23.700000,-1.536000,14.500000>}
box{<0,0,-0.063500><4.800000,0.036000,0.063500> rotate<0,90.000000,0> translate<23.700000,-1.536000,14.500000> }
cylinder{<0,0,0><0,0.036000,0>0.063500 translate<23.700000,-1.536000,14.500000>}
cylinder{<0,0,0><0,0.036000,0>0.063500 translate<30.500000,-1.536000,14.500000>}
box{<0,0,-0.063500><6.800000,0.036000,0.063500> rotate<0,0.000000,0> translate<23.700000,-1.536000,14.500000> }
cylinder{<0,0,0><0,0.036000,0>0.063500 translate<30.500000,-1.536000,14.500000>}
cylinder{<0,0,0><0,0.036000,0>0.063500 translate<30.500000,-1.536000,9.700000>}
box{<0,0,-0.063500><4.800000,0.036000,0.063500> rotate<0,-90.000000,0> translate<30.500000,-1.536000,9.700000> }
cylinder{<0,0,0><0,0.036000,0>0.063500 translate<30.500000,-1.536000,9.700000>}
cylinder{<0,0,0><0,0.036000,0>0.063500 translate<23.700000,-1.536000,9.700000>}
box{<0,0,-0.063500><6.800000,0.036000,0.063500> rotate<0,0.000000,0> translate<23.700000,-1.536000,9.700000> }
object{ARC(0.635000,0.127000,180.000000,360.000000,0.036000) translate<29.005000,-1.536000,14.005000>}
cylinder{<0,0,0><0,0.036000,0>0.063500 translate<29.640000,-1.536000,14.005000>}
cylinder{<0,0,0><0,0.036000,0>0.063500 translate<29.640000,-1.536000,14.440000>}
box{<0,0,-0.063500><0.435000,0.036000,0.063500> rotate<0,90.000000,0> translate<29.640000,-1.536000,14.440000> }
cylinder{<0,0,0><0,0.036000,0>0.063500 translate<28.370000,-1.536000,14.005000>}
cylinder{<0,0,0><0,0.036000,0>0.063500 translate<28.370000,-1.536000,14.440000>}
box{<0,0,-0.063500><0.435000,0.036000,0.063500> rotate<0,90.000000,0> translate<28.370000,-1.536000,14.440000> }
box{<-0.437500,0,-1.075000><0.437500,0.036000,1.075000> rotate<0,-90.000000,0> translate<31.650000,-1.536000,10.812500>}
box{<-0.437500,0,-1.075000><0.437500,0.036000,1.075000> rotate<0,-90.000000,0> translate<31.650000,-1.536000,13.352500>}
box{<-0.437500,0,-1.075000><0.437500,0.036000,1.075000> rotate<0,-270.000000,0> translate<22.550000,-1.536000,10.847500>}
box{<-0.437500,0,-1.075000><0.437500,0.036000,1.075000> rotate<0,-270.000000,0> translate<22.550000,-1.536000,13.387500>}
//J1 silk screen
cylinder{<0,0,0><0,0.036000,0>0.127000 translate<53.480000,0.000000,8.540000>}
cylinder{<0,0,0><0,0.036000,0>0.127000 translate<50.305000,0.000000,8.540000>}
box{<0,0,-0.127000><3.175000,0.036000,0.127000> rotate<0,0.000000,0> translate<50.305000,0.000000,8.540000> }
cylinder{<0,0,0><0,0.036000,0>0.127000 translate<50.305000,0.000000,8.540000>}
cylinder{<0,0,0><0,0.036000,0>0.127000 translate<50.305000,0.000000,7.905000>}
box{<0,0,-0.127000><0.635000,0.036000,0.127000> rotate<0,-90.000000,0> translate<50.305000,0.000000,7.905000> }
cylinder{<0,0,0><0,0.036000,0>0.127000 translate<50.305000,0.000000,7.905000>}
cylinder{<0,0,0><0,0.036000,0>0.127000 translate<50.940000,0.000000,7.905000>}
box{<0,0,-0.127000><0.635000,0.036000,0.127000> rotate<0,0.000000,0> translate<50.305000,0.000000,7.905000> }
cylinder{<0,0,0><0,0.036000,0>0.127000 translate<50.940000,0.000000,7.905000>}
cylinder{<0,0,0><0,0.036000,0>0.127000 translate<50.940000,0.000000,6.635000>}
box{<0,0,-0.127000><1.270000,0.036000,0.127000> rotate<0,-90.000000,0> translate<50.940000,0.000000,6.635000> }
cylinder{<0,0,0><0,0.036000,0>0.127000 translate<50.940000,0.000000,6.635000>}
cylinder{<0,0,0><0,0.036000,0>0.127000 translate<50.305000,0.000000,6.635000>}
box{<0,0,-0.127000><0.635000,0.036000,0.127000> rotate<0,0.000000,0> translate<50.305000,0.000000,6.635000> }
cylinder{<0,0,0><0,0.036000,0>0.127000 translate<50.305000,0.000000,6.635000>}
cylinder{<0,0,0><0,0.036000,0>0.127000 translate<50.305000,0.000000,5.365000>}
box{<0,0,-0.127000><1.270000,0.036000,0.127000> rotate<0,-90.000000,0> translate<50.305000,0.000000,5.365000> }
cylinder{<0,0,0><0,0.036000,0>0.127000 translate<50.305000,0.000000,5.365000>}
cylinder{<0,0,0><0,0.036000,0>0.127000 translate<50.940000,0.000000,5.365000>}
box{<0,0,-0.127000><0.635000,0.036000,0.127000> rotate<0,0.000000,0> translate<50.305000,0.000000,5.365000> }
cylinder{<0,0,0><0,0.036000,0>0.127000 translate<50.940000,0.000000,5.365000>}
cylinder{<0,0,0><0,0.036000,0>0.127000 translate<50.940000,0.000000,4.095000>}
box{<0,0,-0.127000><1.270000,0.036000,0.127000> rotate<0,-90.000000,0> translate<50.940000,0.000000,4.095000> }
cylinder{<0,0,0><0,0.036000,0>0.127000 translate<50.940000,0.000000,4.095000>}
cylinder{<0,0,0><0,0.036000,0>0.127000 translate<50.305000,0.000000,4.095000>}
box{<0,0,-0.127000><0.635000,0.036000,0.127000> rotate<0,0.000000,0> translate<50.305000,0.000000,4.095000> }
cylinder{<0,0,0><0,0.036000,0>0.127000 translate<50.305000,0.000000,4.095000>}
cylinder{<0,0,0><0,0.036000,0>0.127000 translate<50.305000,0.000000,3.460000>}
box{<0,0,-0.127000><0.635000,0.036000,0.127000> rotate<0,-90.000000,0> translate<50.305000,0.000000,3.460000> }
cylinder{<0,0,0><0,0.036000,0>0.127000 translate<50.305000,0.000000,3.460000>}
cylinder{<0,0,0><0,0.036000,0>0.127000 translate<53.480000,0.000000,3.460000>}
box{<0,0,-0.127000><3.175000,0.036000,0.127000> rotate<0,0.000000,0> translate<50.305000,0.000000,3.460000> }
cylinder{<0,0,0><0,0.036000,0>0.127000 translate<53.480000,0.000000,3.460000>}
cylinder{<0,0,0><0,0.036000,0>0.127000 translate<53.480000,0.000000,4.349000>}
box{<0,0,-0.127000><0.889000,0.036000,0.127000> rotate<0,90.000000,0> translate<53.480000,0.000000,4.349000> }
cylinder{<0,0,0><0,0.036000,0>0.127000 translate<53.480000,0.000000,4.349000>}
cylinder{<0,0,0><0,0.036000,0>0.127000 translate<53.480000,0.000000,7.270000>}
box{<0,0,-0.127000><2.921000,0.036000,0.127000> rotate<0,90.000000,0> translate<53.480000,0.000000,7.270000> }
cylinder{<0,0,0><0,0.036000,0>0.127000 translate<53.480000,0.000000,7.270000>}
cylinder{<0,0,0><0,0.036000,0>0.127000 translate<53.480000,0.000000,8.540000>}
box{<0,0,-0.127000><1.270000,0.036000,0.127000> rotate<0,90.000000,0> translate<53.480000,0.000000,8.540000> }
cylinder{<0,0,0><0,0.036000,0>0.127000 translate<53.480000,0.000000,7.270000>}
cylinder{<0,0,0><0,0.036000,0>0.127000 translate<61.481000,0.000000,7.270000>}
box{<0,0,-0.127000><8.001000,0.036000,0.127000> rotate<0,0.000000,0> translate<53.480000,0.000000,7.270000> }
cylinder{<0,0,0><0,0.036000,0>0.127000 translate<61.481000,0.000000,7.270000>}
cylinder{<0,0,0><0,0.036000,0>0.127000 translate<61.989000,0.000000,7.016000>}
box{<0,0,-0.127000><0.567961,0.036000,0.127000> rotate<0,26.563298,0> translate<61.481000,0.000000,7.270000> }
cylinder{<0,0,0><0,0.036000,0>0.127000 translate<61.989000,0.000000,7.016000>}
cylinder{<0,0,0><0,0.036000,0>0.127000 translate<61.989000,0.000000,5.949200>}
box{<0,0,-0.127000><1.066800,0.036000,0.127000> rotate<0,-90.000000,0> translate<61.989000,0.000000,5.949200> }
cylinder{<0,0,0><0,0.036000,0>0.127000 translate<53.480000,0.000000,4.730000>}
cylinder{<0,0,0><0,0.036000,0>0.127000 translate<61.481000,0.000000,4.730000>}
box{<0,0,-0.127000><8.001000,0.036000,0.127000> rotate<0,0.000000,0> translate<53.480000,0.000000,4.730000> }
cylinder{<0,0,0><0,0.036000,0>0.127000 translate<61.481000,0.000000,4.730000>}
cylinder{<0,0,0><0,0.036000,0>0.127000 translate<61.989000,0.000000,4.984000>}
box{<0,0,-0.127000><0.567961,0.036000,0.127000> rotate<0,-26.563298,0> translate<61.481000,0.000000,4.730000> }
cylinder{<0,0,0><0,0.036000,0>0.127000 translate<61.989000,0.000000,4.984000>}
cylinder{<0,0,0><0,0.036000,0>0.127000 translate<61.989000,0.000000,5.949200>}
box{<0,0,-0.127000><0.965200,0.036000,0.127000> rotate<0,90.000000,0> translate<61.989000,0.000000,5.949200> }
cylinder{<0,0,0><0,0.036000,0>0.127000 translate<61.989000,0.000000,5.949200>}
cylinder{<0,0,0><0,0.036000,0>0.127000 translate<61.989000,0.000000,6.050800>}
box{<0,0,-0.127000><0.101600,0.036000,0.127000> rotate<0,90.000000,0> translate<61.989000,0.000000,6.050800> }
cylinder{<0,0,0><0,0.036000,0>0.127000 translate<53.480000,0.000000,7.270000>}
cylinder{<0,0,0><0,0.036000,0>0.127000 translate<53.480000,0.000000,6.889000>}
box{<0,0,-0.127000><0.381000,0.036000,0.127000> rotate<0,-90.000000,0> translate<53.480000,0.000000,6.889000> }
cylinder{<0,0,0><0,0.036000,0>0.127000 translate<53.480000,0.000000,4.349000>}
cylinder{<0,0,0><0,0.036000,0>0.127000 translate<59.703000,0.000000,4.349000>}
box{<0,0,-0.127000><6.223000,0.036000,0.127000> rotate<0,0.000000,0> translate<53.480000,0.000000,4.349000> }
cylinder{<0,0,0><0,0.036000,0>0.127000 translate<59.703000,0.000000,4.349000>}
cylinder{<0,0,0><0,0.036000,0>0.127000 translate<60.058600,0.000000,4.679200>}
box{<0,0,-0.127000><0.485266,0.036000,0.127000> rotate<0,-42.876074,0> translate<59.703000,0.000000,4.349000> }
cylinder{<0,0,0><0,0.036000,0>0.127000 translate<53.480000,0.000000,7.651000>}
cylinder{<0,0,0><0,0.036000,0>0.127000 translate<59.703000,0.000000,7.651000>}
box{<0,0,-0.127000><6.223000,0.036000,0.127000> rotate<0,0.000000,0> translate<53.480000,0.000000,7.651000> }
cylinder{<0,0,0><0,0.036000,0>0.127000 translate<59.703000,0.000000,7.651000>}
cylinder{<0,0,0><0,0.036000,0>0.127000 translate<60.058600,0.000000,7.320800>}
box{<0,0,-0.127000><0.485266,0.036000,0.127000> rotate<0,42.876074,0> translate<59.703000,0.000000,7.651000> }
box{<-0.254000,0,-0.254000><0.254000,0.036000,0.254000> rotate<0,-90.000000,0> translate<48.400000,0.000000,7.270000>}
box{<-0.254000,0,-0.254000><0.254000,0.036000,0.254000> rotate<0,-90.000000,0> translate<48.400000,0.000000,4.730000>}
box{<-0.254000,0,-1.117600><0.254000,0.036000,1.117600> rotate<0,-90.000000,0> translate<49.797000,0.000000,7.270000>}
box{<-0.254000,0,-1.117600><0.254000,0.036000,1.117600> rotate<0,-90.000000,0> translate<49.797000,0.000000,4.730000>}
//L1 silk screen
cylinder{<0,0,0><0,0.036000,0>0.101600 translate<-15.500000,0.000000,13.500000>}
cylinder{<0,0,0><0,0.036000,0>0.101600 translate<-2.475000,0.000000,13.500000>}
box{<0,0,-0.101600><13.025000,0.036000,0.101600> rotate<0,0.000000,0> translate<-15.500000,0.000000,13.500000> }
cylinder{<0,0,0><0,0.036000,0>0.101600 translate<-2.475000,0.000000,13.500000>}
cylinder{<0,0,0><0,0.036000,0>0.101600 translate<-2.475000,0.000000,0.500000>}
box{<0,0,-0.101600><13.000000,0.036000,0.101600> rotate<0,-90.000000,0> translate<-2.475000,0.000000,0.500000> }
cylinder{<0,0,0><0,0.036000,0>0.101600 translate<-2.475000,0.000000,0.500000>}
cylinder{<0,0,0><0,0.036000,0>0.101600 translate<-15.500000,0.000000,0.500000>}
box{<0,0,-0.101600><13.025000,0.036000,0.101600> rotate<0,0.000000,0> translate<-15.500000,0.000000,0.500000> }
cylinder{<0,0,0><0,0.036000,0>0.101600 translate<-15.500000,0.000000,0.500000>}
cylinder{<0,0,0><0,0.036000,0>0.101600 translate<-15.500000,0.000000,13.500000>}
box{<0,0,-0.101600><13.000000,0.036000,0.101600> rotate<0,90.000000,0> translate<-15.500000,0.000000,13.500000> }
object{ARC(0.535000,1.016000,35.329783,237.394106,0.036000) translate<-13.237100,0.000000,11.233700>}
object{ARC(0.535000,1.016000,125.329783,327.394106,0.036000) translate<-13.233700,0.000000,2.762900>}
object{ARC(0.535000,1.016000,215.329783,417.394106,0.036000) translate<-4.762900,0.000000,2.766300>}
object{ARC(0.535000,1.016000,305.329783,507.394106,0.036000) translate<-4.766300,0.000000,11.237100>}
object{ARC(5.900000,0.203200,33.716463,89.926195,0.036000) translate<-9.007600,0.000000,7.000000>}
object{ARC(5.900000,0.203200,89.908715,146.865317,0.036000) translate<-9.009400,0.000000,7.000000>}
object{ARC(5.900000,0.203200,213.716463,269.926195,0.036000) translate<-8.992400,0.000000,7.000000>}
object{ARC(5.900000,0.203200,269.908715,326.865317,0.036000) translate<-8.990600,0.000000,7.000000>}
difference{
cylinder{<-9.000000,0,7.000000><-9.000000,0.036000,7.000000>6.001600 translate<0,0.000000,0>}
cylinder{<-9.000000,-0.1,7.000000><-9.000000,0.135000,7.000000>5.798400 translate<0,0.000000,0>}}
//LED1 silk screen
cylinder{<0,0,0><0,0.036000,0>0.127000 translate<54.025200,0.000000,28.870000>}
cylinder{<0,0,0><0,0.036000,0>0.127000 translate<54.025200,0.000000,26.330000>}
box{<0,0,-0.127000><2.540000,0.036000,0.127000> rotate<0,-90.000000,0> translate<54.025200,0.000000,26.330000> }
object{ARC(1.524000,0.152400,320.196354,360.000000,0.036000) translate<55.600000,0.000000,27.600000>}
object{ARC(1.524000,0.152400,359.996240,401.629793,0.036000) translate<55.600000,0.000000,27.600100>}
object{ARC(1.524000,0.152400,180.000000,220.601295,0.036000) translate<55.600000,0.000000,27.600000>}
object{ARC(1.524000,0.152400,140.196354,180.000000,0.036000) translate<55.600000,0.000000,27.600000>}
object{ARC(1.524000,0.152400,215.538115,270.000000,0.036000) translate<55.600000,0.000000,27.600000>}
object{ARC(1.524000,0.152400,270.000000,323.130102,0.036000) translate<55.600000,0.000000,27.600000>}
object{ARC(1.524000,0.152400,90.000000,142.126995,0.036000) translate<55.600000,0.000000,27.600000>}
object{ARC(1.524000,0.152400,37.873005,90.000000,0.036000) translate<55.600000,0.000000,27.600000>}
object{ARC(0.635000,0.152400,270.000000,360.000000,0.036000) translate<55.600000,0.000000,27.600000>}
object{ARC(1.016000,0.152400,270.000000,360.000000,0.036000) translate<55.600000,0.000000,27.600000>}
object{ARC(0.635000,0.152400,90.000000,180.000000,0.036000) translate<55.600000,0.000000,27.600000>}
object{ARC(1.016000,0.152400,90.000000,180.000000,0.036000) translate<55.600000,0.000000,27.600000>}
object{ARC(2.032000,0.254000,219.807015,270.000000,0.036000) translate<55.600000,0.000000,27.600000>}
object{ARC(2.032000,0.254000,270.002820,331.929172,0.036000) translate<55.599900,0.000000,27.600000>}
object{ARC(2.032000,0.254000,90.000000,139.762648,0.036000) translate<55.600000,0.000000,27.600000>}
object{ARC(2.032000,0.254000,29.746980,90.002820,0.036000) translate<55.600100,0.000000,27.600000>}
object{ARC(2.032000,0.254000,331.698289,360.000000,0.036000) translate<55.600000,0.000000,27.600000>}
object{ARC(2.032000,0.254000,359.997180,391.605470,0.036000) translate<55.600000,0.000000,27.600100>}
//Q1 silk screen
cylinder{<0,0,0><0,0.036000,0>0.076200 translate<-4.901000,0.000000,25.282000>}
cylinder{<0,0,0><0,0.036000,0>0.076200 translate<-4.647000,0.000000,25.536000>}
box{<0,0,-0.076200><0.359210,0.036000,0.076200> rotate<0,-44.997030,0> translate<-4.901000,0.000000,25.282000> }
cylinder{<0,0,0><0,0.036000,0>0.076200 translate<-4.901000,0.000000,25.282000>}
cylinder{<0,0,0><0,0.036000,0>0.076200 translate<-14.299000,0.000000,25.282000>}
box{<0,0,-0.076200><9.398000,0.036000,0.076200> rotate<0,0.000000,0> translate<-14.299000,0.000000,25.282000> }
cylinder{<0,0,0><0,0.036000,0>0.076200 translate<-14.553000,0.000000,25.536000>}
cylinder{<0,0,0><0,0.036000,0>0.076200 translate<-14.299000,0.000000,25.282000>}
box{<0,0,-0.076200><0.359210,0.036000,0.076200> rotate<0,44.997030,0> translate<-14.553000,0.000000,25.536000> }
cylinder{<0,0,0><0,0.036000,0>0.076200 translate<-4.520000,0.000000,28.457000>}
cylinder{<0,0,0><0,0.036000,0>0.076200 translate<-4.647000,0.000000,25.536000>}
box{<0,0,-0.076200><2.923760,0.036000,0.076200> rotate<0,-87.504672,0> translate<-4.647000,0.000000,25.536000> }
cylinder{<0,0,0><0,0.036000,0>0.076200 translate<-14.553000,0.000000,25.536000>}
cylinder{<0,0,0><0,0.036000,0>0.076200 translate<-14.680000,0.000000,28.457000>}
box{<0,0,-0.076200><2.923760,0.036000,0.076200> rotate<0,87.504672,0> translate<-14.680000,0.000000,28.457000> }
difference{
cylinder{<-14.095800,0,25.891600><-14.095800,0.036000,25.891600>0.254000 translate<0,0.000000,0>}
cylinder{<-14.095800,-0.1,25.891600><-14.095800,0.135000,25.891600>0.254000 translate<0,0.000000,0>}}
box{<-5.334000,0,-0.381000><5.334000,0.036000,0.381000> rotate<0,-0.000000,0> translate<-9.600000,0.000000,29.219000>}
box{<-0.952500,0,-0.254000><0.952500,0.036000,0.254000> rotate<0,-0.000000,0> translate<-13.981500,0.000000,28.584000>}
box{<-0.381000,0,-0.254000><0.381000,0.036000,0.254000> rotate<0,-0.000000,0> translate<-10.870000,0.000000,28.584000>}
box{<-0.889000,0,-0.254000><0.889000,0.036000,0.254000> rotate<0,-0.000000,0> translate<-12.140000,0.000000,28.584000>}
box{<-0.381000,0,-0.254000><0.381000,0.036000,0.254000> rotate<0,-0.000000,0> translate<-8.330000,0.000000,28.584000>}
box{<-0.952500,0,-0.254000><0.952500,0.036000,0.254000> rotate<0,-0.000000,0> translate<-5.218500,0.000000,28.584000>}
box{<-0.889000,0,-0.254000><0.889000,0.036000,0.254000> rotate<0,-0.000000,0> translate<-9.600000,0.000000,28.584000>}
box{<-0.889000,0,-0.254000><0.889000,0.036000,0.254000> rotate<0,-0.000000,0> translate<-7.060000,0.000000,28.584000>}
//Q2 silk screen
cylinder{<0,0,0><0,0.036000,0>0.127000 translate<21.777200,0.000000,25.028400>}
cylinder{<0,0,0><0,0.036000,0>0.127000 translate<31.022800,0.000000,25.028400>}
box{<0,0,-0.127000><9.245600,0.036000,0.127000> rotate<0,0.000000,0> translate<21.777200,0.000000,25.028400> }
cylinder{<0,0,0><0,0.036000,0>0.127000 translate<31.022800,0.000000,25.028400>}
cylinder{<0,0,0><0,0.036000,0>0.127000 translate<31.530800,0.000000,26.552400>}
box{<0,0,-0.127000><1.606437,0.036000,0.127000> rotate<0,-71.560328,0> translate<31.022800,0.000000,25.028400> }
cylinder{<0,0,0><0,0.036000,0>0.127000 translate<31.530800,0.000000,26.552400>}
cylinder{<0,0,0><0,0.036000,0>0.127000 translate<31.530800,0.000000,28.152600>}
box{<0,0,-0.127000><1.600200,0.036000,0.127000> rotate<0,90.000000,0> translate<31.530800,0.000000,28.152600> }
cylinder{<0,0,0><0,0.036000,0>0.127000 translate<21.269200,0.000000,28.152600>}
cylinder{<0,0,0><0,0.036000,0>0.127000 translate<21.269200,0.000000,26.552400>}
box{<0,0,-0.127000><1.600200,0.036000,0.127000> rotate<0,-90.000000,0> translate<21.269200,0.000000,26.552400> }
cylinder{<0,0,0><0,0.036000,0>0.127000 translate<21.269200,0.000000,26.552400>}
cylinder{<0,0,0><0,0.036000,0>0.127000 translate<21.777200,0.000000,25.028400>}
box{<0,0,-0.127000><1.606437,0.036000,0.127000> rotate<0,71.560328,0> translate<21.269200,0.000000,26.552400> }
cylinder{<0,0,0><0,0.036000,0>0.127000 translate<21.269200,0.000000,28.152600>}
cylinder{<0,0,0><0,0.036000,0>0.127000 translate<31.530800,0.000000,28.152600>}
box{<0,0,-0.127000><10.261600,0.036000,0.127000> rotate<0,0.000000,0> translate<21.269200,0.000000,28.152600> }
box{<-5.283200,0,-0.711200><5.283200,0.036000,0.711200> rotate<0,-0.000000,0> translate<26.400000,0.000000,28.889200>}
//Q3 silk screen
cylinder{<0,0,0><0,0.036000,0>0.127000 translate<33.377200,0.000000,25.028400>}
cylinder{<0,0,0><0,0.036000,0>0.127000 translate<42.622800,0.000000,25.028400>}
box{<0,0,-0.127000><9.245600,0.036000,0.127000> rotate<0,0.000000,0> translate<33.377200,0.000000,25.028400> }
cylinder{<0,0,0><0,0.036000,0>0.127000 translate<42.622800,0.000000,25.028400>}
cylinder{<0,0,0><0,0.036000,0>0.127000 translate<43.130800,0.000000,26.552400>}
box{<0,0,-0.127000><1.606437,0.036000,0.127000> rotate<0,-71.560328,0> translate<42.622800,0.000000,25.028400> }
cylinder{<0,0,0><0,0.036000,0>0.127000 translate<43.130800,0.000000,26.552400>}
cylinder{<0,0,0><0,0.036000,0>0.127000 translate<43.130800,0.000000,28.152600>}
box{<0,0,-0.127000><1.600200,0.036000,0.127000> rotate<0,90.000000,0> translate<43.130800,0.000000,28.152600> }
cylinder{<0,0,0><0,0.036000,0>0.127000 translate<32.869200,0.000000,28.152600>}
cylinder{<0,0,0><0,0.036000,0>0.127000 translate<32.869200,0.000000,26.552400>}
box{<0,0,-0.127000><1.600200,0.036000,0.127000> rotate<0,-90.000000,0> translate<32.869200,0.000000,26.552400> }
cylinder{<0,0,0><0,0.036000,0>0.127000 translate<32.869200,0.000000,26.552400>}
cylinder{<0,0,0><0,0.036000,0>0.127000 translate<33.377200,0.000000,25.028400>}
box{<0,0,-0.127000><1.606437,0.036000,0.127000> rotate<0,71.560328,0> translate<32.869200,0.000000,26.552400> }
cylinder{<0,0,0><0,0.036000,0>0.127000 translate<32.869200,0.000000,28.152600>}
cylinder{<0,0,0><0,0.036000,0>0.127000 translate<43.130800,0.000000,28.152600>}
box{<0,0,-0.127000><10.261600,0.036000,0.127000> rotate<0,0.000000,0> translate<32.869200,0.000000,28.152600> }
box{<-5.283200,0,-0.711200><5.283200,0.036000,0.711200> rotate<0,-0.000000,0> translate<38.000000,0.000000,28.889200>}
//R1 silk screen
cylinder{<0,0,0><0,0.036000,0>0.076200 translate<-16.235000,0.000000,17.711200>}
cylinder{<0,0,0><0,0.036000,0>0.076200 translate<-16.235000,0.000000,16.288800>}
box{<0,0,-0.076200><1.422400,0.036000,0.076200> rotate<0,-90.000000,0> translate<-16.235000,0.000000,16.288800> }
cylinder{<0,0,0><0,0.036000,0>0.076200 translate<-14.965000,0.000000,17.711200>}
cylinder{<0,0,0><0,0.036000,0>0.076200 translate<-14.965000,0.000000,16.288800>}
box{<0,0,-0.076200><1.422400,0.036000,0.076200> rotate<0,-90.000000,0> translate<-14.965000,0.000000,16.288800> }
box{<-0.177800,0,-0.711200><0.177800,0.036000,0.711200> rotate<0,-90.000000,0> translate<-15.600000,0.000000,16.136400>}
box{<-0.177800,0,-0.711200><0.177800,0.036000,0.711200> rotate<0,-90.000000,0> translate<-15.600000,0.000000,17.863600>}
//R2 silk screen
cylinder{<0,0,0><0,0.036000,0>0.076200 translate<-13.688800,0.000000,22.735000>}
cylinder{<0,0,0><0,0.036000,0>0.076200 translate<-15.111200,0.000000,22.735000>}
box{<0,0,-0.076200><1.422400,0.036000,0.076200> rotate<0,0.000000,0> translate<-15.111200,0.000000,22.735000> }
cylinder{<0,0,0><0,0.036000,0>0.076200 translate<-13.688800,0.000000,21.465000>}
cylinder{<0,0,0><0,0.036000,0>0.076200 translate<-15.111200,0.000000,21.465000>}
box{<0,0,-0.076200><1.422400,0.036000,0.076200> rotate<0,0.000000,0> translate<-15.111200,0.000000,21.465000> }
box{<-0.177800,0,-0.711200><0.177800,0.036000,0.711200> rotate<0,-0.000000,0> translate<-15.263600,0.000000,22.100000>}
box{<-0.177800,0,-0.711200><0.177800,0.036000,0.711200> rotate<0,-0.000000,0> translate<-13.536400,0.000000,22.100000>}
//R3 silk screen
cylinder{<0,0,0><0,0.036000,0>0.076200 translate<1.088800,0.000000,5.765000>}
cylinder{<0,0,0><0,0.036000,0>0.076200 translate<2.511200,0.000000,5.765000>}
box{<0,0,-0.076200><1.422400,0.036000,0.076200> rotate<0,0.000000,0> translate<1.088800,0.000000,5.765000> }
cylinder{<0,0,0><0,0.036000,0>0.076200 translate<1.088800,0.000000,7.035000>}
cylinder{<0,0,0><0,0.036000,0>0.076200 translate<2.511200,0.000000,7.035000>}
box{<0,0,-0.076200><1.422400,0.036000,0.076200> rotate<0,0.000000,0> translate<1.088800,0.000000,7.035000> }
box{<-0.177800,0,-0.711200><0.177800,0.036000,0.711200> rotate<0,-180.000000,0> translate<2.663600,0.000000,6.400000>}
box{<-0.177800,0,-0.711200><0.177800,0.036000,0.711200> rotate<0,-180.000000,0> translate<0.936400,0.000000,6.400000>}
//R4 silk screen
cylinder{<0,0,0><0,0.036000,0>0.076200 translate<49.362000,0.000000,17.527000>}
cylinder{<0,0,0><0,0.036000,0>0.076200 translate<44.613000,0.000000,17.527000>}
box{<0,0,-0.076200><4.749000,0.036000,0.076200> rotate<0,0.000000,0> translate<44.613000,0.000000,17.527000> }
cylinder{<0,0,0><0,0.036000,0>0.076200 translate<49.362000,0.000000,20.473000>}
cylinder{<0,0,0><0,0.036000,0>0.076200 translate<44.613000,0.000000,20.473000>}
box{<0,0,-0.076200><4.749000,0.036000,0.076200> rotate<0,0.000000,0> translate<44.613000,0.000000,20.473000> }
box{<-0.424900,0,-1.550000><0.424900,0.036000,1.550000> rotate<0,-180.000000,0> translate<49.775300,0.000000,18.999400>}
box{<-0.424900,0,-1.550000><0.424900,0.036000,1.550000> rotate<0,-180.000000,0> translate<44.212900,0.000000,18.999400>}
//R5 silk screen
cylinder{<0,0,0><0,0.036000,0>0.076200 translate<1.511200,-1.536000,11.965000>}
cylinder{<0,0,0><0,0.036000,0>0.076200 translate<0.088800,-1.536000,11.965000>}
box{<0,0,-0.076200><1.422400,0.036000,0.076200> rotate<0,0.000000,0> translate<0.088800,-1.536000,11.965000> }
cylinder{<0,0,0><0,0.036000,0>0.076200 translate<1.511200,-1.536000,13.235000>}
cylinder{<0,0,0><0,0.036000,0>0.076200 translate<0.088800,-1.536000,13.235000>}
box{<0,0,-0.076200><1.422400,0.036000,0.076200> rotate<0,0.000000,0> translate<0.088800,-1.536000,13.235000> }
box{<-0.177800,0,-0.711200><0.177800,0.036000,0.711200> rotate<0,-0.000000,0> translate<-0.063600,-1.536000,12.600000>}
box{<-0.177800,0,-0.711200><0.177800,0.036000,0.711200> rotate<0,-0.000000,0> translate<1.663600,-1.536000,12.600000>}
//R6 silk screen
cylinder{<0,0,0><0,0.036000,0>0.076200 translate<-7.288800,0.000000,23.435000>}
cylinder{<0,0,0><0,0.036000,0>0.076200 translate<-8.711200,0.000000,23.435000>}
box{<0,0,-0.076200><1.422400,0.036000,0.076200> rotate<0,0.000000,0> translate<-8.711200,0.000000,23.435000> }
cylinder{<0,0,0><0,0.036000,0>0.076200 translate<-7.288800,0.000000,22.165000>}
cylinder{<0,0,0><0,0.036000,0>0.076200 translate<-8.711200,0.000000,22.165000>}
box{<0,0,-0.076200><1.422400,0.036000,0.076200> rotate<0,0.000000,0> translate<-8.711200,0.000000,22.165000> }
box{<-0.177800,0,-0.711200><0.177800,0.036000,0.711200> rotate<0,-0.000000,0> translate<-8.863600,0.000000,22.800000>}
box{<-0.177800,0,-0.711200><0.177800,0.036000,0.711200> rotate<0,-0.000000,0> translate<-7.136400,0.000000,22.800000>}
//R7 silk screen
cylinder{<0,0,0><0,0.036000,0>0.076200 translate<6.165000,0.000000,4.311200>}
cylinder{<0,0,0><0,0.036000,0>0.076200 translate<6.165000,0.000000,2.888800>}
box{<0,0,-0.076200><1.422400,0.036000,0.076200> rotate<0,-90.000000,0> translate<6.165000,0.000000,2.888800> }
cylinder{<0,0,0><0,0.036000,0>0.076200 translate<7.435000,0.000000,4.311200>}
cylinder{<0,0,0><0,0.036000,0>0.076200 translate<7.435000,0.000000,2.888800>}
box{<0,0,-0.076200><1.422400,0.036000,0.076200> rotate<0,-90.000000,0> translate<7.435000,0.000000,2.888800> }
box{<-0.177800,0,-0.711200><0.177800,0.036000,0.711200> rotate<0,-90.000000,0> translate<6.800000,0.000000,2.736400>}
box{<-0.177800,0,-0.711200><0.177800,0.036000,0.711200> rotate<0,-90.000000,0> translate<6.800000,0.000000,4.463600>}
//R8 silk screen
cylinder{<0,0,0><0,0.036000,0>0.076200 translate<5.965000,0.000000,9.511200>}
cylinder{<0,0,0><0,0.036000,0>0.076200 translate<5.965000,0.000000,8.088800>}
box{<0,0,-0.076200><1.422400,0.036000,0.076200> rotate<0,-90.000000,0> translate<5.965000,0.000000,8.088800> }
cylinder{<0,0,0><0,0.036000,0>0.076200 translate<7.235000,0.000000,9.511200>}
cylinder{<0,0,0><0,0.036000,0>0.076200 translate<7.235000,0.000000,8.088800>}
box{<0,0,-0.076200><1.422400,0.036000,0.076200> rotate<0,-90.000000,0> translate<7.235000,0.000000,8.088800> }
box{<-0.177800,0,-0.711200><0.177800,0.036000,0.711200> rotate<0,-90.000000,0> translate<6.600000,0.000000,7.936400>}
box{<-0.177800,0,-0.711200><0.177800,0.036000,0.711200> rotate<0,-90.000000,0> translate<6.600000,0.000000,9.663600>}
//R9 silk screen
cylinder{<0,0,0><0,0.036000,0>0.076200 translate<15.788800,-1.536000,10.935000>}
cylinder{<0,0,0><0,0.036000,0>0.076200 translate<17.211200,-1.536000,10.935000>}
box{<0,0,-0.076200><1.422400,0.036000,0.076200> rotate<0,0.000000,0> translate<15.788800,-1.536000,10.935000> }
cylinder{<0,0,0><0,0.036000,0>0.076200 translate<15.788800,-1.536000,9.665000>}
cylinder{<0,0,0><0,0.036000,0>0.076200 translate<17.211200,-1.536000,9.665000>}
box{<0,0,-0.076200><1.422400,0.036000,0.076200> rotate<0,0.000000,0> translate<15.788800,-1.536000,9.665000> }
box{<-0.177800,0,-0.711200><0.177800,0.036000,0.711200> rotate<0,-180.000000,0> translate<17.363600,-1.536000,10.300000>}
box{<-0.177800,0,-0.711200><0.177800,0.036000,0.711200> rotate<0,-180.000000,0> translate<15.636400,-1.536000,10.300000>}
//R10 silk screen
cylinder{<0,0,0><0,0.036000,0>0.076200 translate<36.838000,-1.536000,20.727000>}
cylinder{<0,0,0><0,0.036000,0>0.076200 translate<41.587000,-1.536000,20.727000>}
box{<0,0,-0.076200><4.749000,0.036000,0.076200> rotate<0,0.000000,0> translate<36.838000,-1.536000,20.727000> }
cylinder{<0,0,0><0,0.036000,0>0.076200 translate<36.838000,-1.536000,23.673000>}
cylinder{<0,0,0><0,0.036000,0>0.076200 translate<41.587000,-1.536000,23.673000>}
box{<0,0,-0.076200><4.749000,0.036000,0.076200> rotate<0,0.000000,0> translate<36.838000,-1.536000,23.673000> }
box{<-0.424900,0,-1.550000><0.424900,0.036000,1.550000> rotate<0,-0.000000,0> translate<36.424500,-1.536000,22.199400>}
box{<-0.424900,0,-1.550000><0.424900,0.036000,1.550000> rotate<0,-0.000000,0> translate<41.987100,-1.536000,22.199400>}
//R11 silk screen
cylinder{<0,0,0><0,0.036000,0>0.076200 translate<17.811200,-1.536000,16.365000>}
cylinder{<0,0,0><0,0.036000,0>0.076200 translate<16.388800,-1.536000,16.365000>}
box{<0,0,-0.076200><1.422400,0.036000,0.076200> rotate<0,0.000000,0> translate<16.388800,-1.536000,16.365000> }
cylinder{<0,0,0><0,0.036000,0>0.076200 translate<17.811200,-1.536000,17.635000>}
cylinder{<0,0,0><0,0.036000,0>0.076200 translate<16.388800,-1.536000,17.635000>}
box{<0,0,-0.076200><1.422400,0.036000,0.076200> rotate<0,0.000000,0> translate<16.388800,-1.536000,17.635000> }
box{<-0.177800,0,-0.711200><0.177800,0.036000,0.711200> rotate<0,-0.000000,0> translate<16.236400,-1.536000,17.000000>}
box{<-0.177800,0,-0.711200><0.177800,0.036000,0.711200> rotate<0,-0.000000,0> translate<17.963600,-1.536000,17.000000>}
//R12 silk screen
cylinder{<0,0,0><0,0.036000,0>0.076200 translate<11.835000,-1.536000,5.111200>}
cylinder{<0,0,0><0,0.036000,0>0.076200 translate<11.835000,-1.536000,3.688800>}
box{<0,0,-0.076200><1.422400,0.036000,0.076200> rotate<0,-90.000000,0> translate<11.835000,-1.536000,3.688800> }
cylinder{<0,0,0><0,0.036000,0>0.076200 translate<10.565000,-1.536000,5.111200>}
cylinder{<0,0,0><0,0.036000,0>0.076200 translate<10.565000,-1.536000,3.688800>}
box{<0,0,-0.076200><1.422400,0.036000,0.076200> rotate<0,-90.000000,0> translate<10.565000,-1.536000,3.688800> }
box{<-0.177800,0,-0.711200><0.177800,0.036000,0.711200> rotate<0,-90.000000,0> translate<11.200000,-1.536000,3.536400>}
box{<-0.177800,0,-0.711200><0.177800,0.036000,0.711200> rotate<0,-90.000000,0> translate<11.200000,-1.536000,5.263600>}
//R13 silk screen
cylinder{<0,0,0><0,0.036000,0>0.076200 translate<20.435000,-1.536000,27.511200>}
cylinder{<0,0,0><0,0.036000,0>0.076200 translate<20.435000,-1.536000,26.088800>}
box{<0,0,-0.076200><1.422400,0.036000,0.076200> rotate<0,-90.000000,0> translate<20.435000,-1.536000,26.088800> }
cylinder{<0,0,0><0,0.036000,0>0.076200 translate<19.165000,-1.536000,27.511200>}
cylinder{<0,0,0><0,0.036000,0>0.076200 translate<19.165000,-1.536000,26.088800>}
box{<0,0,-0.076200><1.422400,0.036000,0.076200> rotate<0,-90.000000,0> translate<19.165000,-1.536000,26.088800> }
box{<-0.177800,0,-0.711200><0.177800,0.036000,0.711200> rotate<0,-90.000000,0> translate<19.800000,-1.536000,25.936400>}
box{<-0.177800,0,-0.711200><0.177800,0.036000,0.711200> rotate<0,-90.000000,0> translate<19.800000,-1.536000,27.663600>}
//R14 silk screen
cylinder{<0,0,0><0,0.036000,0>0.076200 translate<32.635000,-1.536000,27.511200>}
cylinder{<0,0,0><0,0.036000,0>0.076200 translate<32.635000,-1.536000,26.088800>}
box{<0,0,-0.076200><1.422400,0.036000,0.076200> rotate<0,-90.000000,0> translate<32.635000,-1.536000,26.088800> }
cylinder{<0,0,0><0,0.036000,0>0.076200 translate<31.365000,-1.536000,27.511200>}
cylinder{<0,0,0><0,0.036000,0>0.076200 translate<31.365000,-1.536000,26.088800>}
box{<0,0,-0.076200><1.422400,0.036000,0.076200> rotate<0,-90.000000,0> translate<31.365000,-1.536000,26.088800> }
box{<-0.177800,0,-0.711200><0.177800,0.036000,0.711200> rotate<0,-90.000000,0> translate<32.000000,-1.536000,25.936400>}
box{<-0.177800,0,-0.711200><0.177800,0.036000,0.711200> rotate<0,-90.000000,0> translate<32.000000,-1.536000,27.663600>}
//R15 silk screen
cylinder{<0,0,0><0,0.036000,0>0.076200 translate<17.511200,-1.536000,12.965000>}
cylinder{<0,0,0><0,0.036000,0>0.076200 translate<16.088800,-1.536000,12.965000>}
box{<0,0,-0.076200><1.422400,0.036000,0.076200> rotate<0,0.000000,0> translate<16.088800,-1.536000,12.965000> }
cylinder{<0,0,0><0,0.036000,0>0.076200 translate<17.511200,-1.536000,14.235000>}
cylinder{<0,0,0><0,0.036000,0>0.076200 translate<16.088800,-1.536000,14.235000>}
box{<0,0,-0.076200><1.422400,0.036000,0.076200> rotate<0,0.000000,0> translate<16.088800,-1.536000,14.235000> }
box{<-0.177800,0,-0.711200><0.177800,0.036000,0.711200> rotate<0,-0.000000,0> translate<15.936400,-1.536000,13.600000>}
box{<-0.177800,0,-0.711200><0.177800,0.036000,0.711200> rotate<0,-0.000000,0> translate<17.663600,-1.536000,13.600000>}
//R16 silk screen
cylinder{<0,0,0><0,0.036000,0>0.076200 translate<4.688800,0.000000,12.165000>}
cylinder{<0,0,0><0,0.036000,0>0.076200 translate<6.111200,0.000000,12.165000>}
box{<0,0,-0.076200><1.422400,0.036000,0.076200> rotate<0,0.000000,0> translate<4.688800,0.000000,12.165000> }
cylinder{<0,0,0><0,0.036000,0>0.076200 translate<4.688800,0.000000,13.435000>}
cylinder{<0,0,0><0,0.036000,0>0.076200 translate<6.111200,0.000000,13.435000>}
box{<0,0,-0.076200><1.422400,0.036000,0.076200> rotate<0,0.000000,0> translate<4.688800,0.000000,13.435000> }
box{<-0.177800,0,-0.711200><0.177800,0.036000,0.711200> rotate<0,-180.000000,0> translate<6.263600,0.000000,12.800000>}
box{<-0.177800,0,-0.711200><0.177800,0.036000,0.711200> rotate<0,-180.000000,0> translate<4.536400,0.000000,12.800000>}
//S1 silk screen
cylinder{<0,0,0><0,0.036000,0>0.076200 translate<51.502000,0.000000,25.638000>}
cylinder{<0,0,0><0,0.036000,0>0.076200 translate<51.248000,0.000000,25.638000>}
box{<0,0,-0.076200><0.254000,0.036000,0.076200> rotate<0,0.000000,0> translate<51.248000,0.000000,25.638000> }
cylinder{<0,0,0><0,0.036000,0>0.076200 translate<51.502000,0.000000,25.638000>}
cylinder{<0,0,0><0,0.036000,0>0.076200 translate<51.502000,0.000000,27.162000>}
box{<0,0,-0.076200><1.524000,0.036000,0.076200> rotate<0,90.000000,0> translate<51.502000,0.000000,27.162000> }
cylinder{<0,0,0><0,0.036000,0>0.076200 translate<51.248000,0.000000,27.162000>}
cylinder{<0,0,0><0,0.036000,0>0.076200 translate<51.502000,0.000000,27.162000>}
box{<0,0,-0.076200><0.254000,0.036000,0.076200> rotate<0,0.000000,0> translate<51.248000,0.000000,27.162000> }
cylinder{<0,0,0><0,0.036000,0>0.076200 translate<51.248000,0.000000,27.416000>}
cylinder{<0,0,0><0,0.036000,0>0.076200 translate<51.248000,0.000000,28.940000>}
box{<0,0,-0.076200><1.524000,0.036000,0.076200> rotate<0,90.000000,0> translate<51.248000,0.000000,28.940000> }
cylinder{<0,0,0><0,0.036000,0>0.076200 translate<44.898000,0.000000,27.162000>}
cylinder{<0,0,0><0,0.036000,0>0.076200 translate<45.152000,0.000000,27.162000>}
box{<0,0,-0.076200><0.254000,0.036000,0.076200> rotate<0,0.000000,0> translate<44.898000,0.000000,27.162000> }
cylinder{<0,0,0><0,0.036000,0>0.076200 translate<44.898000,0.000000,27.162000>}
cylinder{<0,0,0><0,0.036000,0>0.076200 translate<44.898000,0.000000,25.638000>}
box{<0,0,-0.076200><1.524000,0.036000,0.076200> rotate<0,-90.000000,0> translate<44.898000,0.000000,25.638000> }
cylinder{<0,0,0><0,0.036000,0>0.076200 translate<45.152000,0.000000,25.638000>}
cylinder{<0,0,0><0,0.036000,0>0.076200 translate<44.898000,0.000000,25.638000>}
box{<0,0,-0.076200><0.254000,0.036000,0.076200> rotate<0,0.000000,0> translate<44.898000,0.000000,25.638000> }
cylinder{<0,0,0><0,0.036000,0>0.076200 translate<51.248000,0.000000,28.940000>}
cylinder{<0,0,0><0,0.036000,0>0.076200 translate<50.740000,0.000000,29.448000>}
box{<0,0,-0.076200><0.718420,0.036000,0.076200> rotate<0,44.997030,0> translate<50.740000,0.000000,29.448000> }
cylinder{<0,0,0><0,0.036000,0>0.076200 translate<50.740000,0.000000,23.352000>}
cylinder{<0,0,0><0,0.036000,0>0.076200 translate<51.248000,0.000000,23.860000>}
box{<0,0,-0.076200><0.718420,0.036000,0.076200> rotate<0,-44.997030,0> translate<50.740000,0.000000,23.352000> }
cylinder{<0,0,0><0,0.036000,0>0.076200 translate<51.248000,0.000000,23.860000>}
cylinder{<0,0,0><0,0.036000,0>0.076200 translate<51.248000,0.000000,25.384000>}
box{<0,0,-0.076200><1.524000,0.036000,0.076200> rotate<0,90.000000,0> translate<51.248000,0.000000,25.384000> }
cylinder{<0,0,0><0,0.036000,0>0.076200 translate<45.660000,0.000000,29.448000>}
cylinder{<0,0,0><0,0.036000,0>0.076200 translate<45.152000,0.000000,28.940000>}
box{<0,0,-0.076200><0.718420,0.036000,0.076200> rotate<0,-44.997030,0> translate<45.152000,0.000000,28.940000> }
cylinder{<0,0,0><0,0.036000,0>0.076200 translate<45.152000,0.000000,28.940000>}
cylinder{<0,0,0><0,0.036000,0>0.076200 translate<45.152000,0.000000,27.416000>}
box{<0,0,-0.076200><1.524000,0.036000,0.076200> rotate<0,-90.000000,0> translate<45.152000,0.000000,27.416000> }
cylinder{<0,0,0><0,0.036000,0>0.076200 translate<45.660000,0.000000,23.352000>}
cylinder{<0,0,0><0,0.036000,0>0.076200 translate<45.152000,0.000000,23.860000>}
box{<0,0,-0.076200><0.718420,0.036000,0.076200> rotate<0,44.997030,0> translate<45.152000,0.000000,23.860000> }
cylinder{<0,0,0><0,0.036000,0>0.076200 translate<45.152000,0.000000,23.860000>}
cylinder{<0,0,0><0,0.036000,0>0.076200 translate<45.152000,0.000000,25.384000>}
box{<0,0,-0.076200><1.524000,0.036000,0.076200> rotate<0,90.000000,0> translate<45.152000,0.000000,25.384000> }
cylinder{<0,0,0><0,0.036000,0>0.025400 translate<46.930000,0.000000,27.670000>}
cylinder{<0,0,0><0,0.036000,0>0.025400 translate<46.930000,0.000000,25.130000>}
box{<0,0,-0.025400><2.540000,0.036000,0.025400> rotate<0,-90.000000,0> translate<46.930000,0.000000,25.130000> }
cylinder{<0,0,0><0,0.036000,0>0.025400 translate<49.470000,0.000000,25.130000>}
cylinder{<0,0,0><0,0.036000,0>0.025400 translate<46.930000,0.000000,25.130000>}
box{<0,0,-0.025400><2.540000,0.036000,0.025400> rotate<0,0.000000,0> translate<46.930000,0.000000,25.130000> }
cylinder{<0,0,0><0,0.036000,0>0.025400 translate<49.470000,0.000000,25.130000>}
cylinder{<0,0,0><0,0.036000,0>0.025400 translate<49.470000,0.000000,27.670000>}
box{<0,0,-0.025400><2.540000,0.036000,0.025400> rotate<0,90.000000,0> translate<49.470000,0.000000,27.670000> }
cylinder{<0,0,0><0,0.036000,0>0.025400 translate<46.930000,0.000000,27.670000>}
cylinder{<0,0,0><0,0.036000,0>0.025400 translate<49.470000,0.000000,27.670000>}
box{<0,0,-0.025400><2.540000,0.036000,0.025400> rotate<0,0.000000,0> translate<46.930000,0.000000,27.670000> }
cylinder{<0,0,0><0,0.036000,0>0.025400 translate<46.930000,0.000000,29.448000>}
cylinder{<0,0,0><0,0.036000,0>0.025400 translate<46.930000,0.000000,29.194000>}
box{<0,0,-0.025400><0.254000,0.036000,0.025400> rotate<0,-90.000000,0> translate<46.930000,0.000000,29.194000> }
cylinder{<0,0,0><0,0.036000,0>0.025400 translate<49.470000,0.000000,29.194000>}
cylinder{<0,0,0><0,0.036000,0>0.025400 translate<46.930000,0.000000,29.194000>}
box{<0,0,-0.025400><2.540000,0.036000,0.025400> rotate<0,0.000000,0> translate<46.930000,0.000000,29.194000> }
cylinder{<0,0,0><0,0.036000,0>0.025400 translate<49.470000,0.000000,29.194000>}
cylinder{<0,0,0><0,0.036000,0>0.025400 translate<49.470000,0.000000,29.448000>}
box{<0,0,-0.025400><0.254000,0.036000,0.025400> rotate<0,90.000000,0> translate<49.470000,0.000000,29.448000> }
cylinder{<0,0,0><0,0.036000,0>0.025400 translate<49.343000,0.000000,23.606000>}
cylinder{<0,0,0><0,0.036000,0>0.025400 translate<46.930000,0.000000,23.606000>}
box{<0,0,-0.025400><2.413000,0.036000,0.025400> rotate<0,0.000000,0> translate<46.930000,0.000000,23.606000> }
cylinder{<0,0,0><0,0.036000,0>0.025400 translate<49.343000,0.000000,23.606000>}
cylinder{<0,0,0><0,0.036000,0>0.025400 translate<49.343000,0.000000,23.352000>}
box{<0,0,-0.025400><0.254000,0.036000,0.025400> rotate<0,-90.000000,0> translate<49.343000,0.000000,23.352000> }
cylinder{<0,0,0><0,0.036000,0>0.025400 translate<46.930000,0.000000,23.606000>}
cylinder{<0,0,0><0,0.036000,0>0.025400 translate<46.930000,0.000000,23.352000>}
box{<0,0,-0.025400><0.254000,0.036000,0.025400> rotate<0,-90.000000,0> translate<46.930000,0.000000,23.352000> }
cylinder{<0,0,0><0,0.036000,0>0.076200 translate<50.740000,0.000000,23.352000>}
cylinder{<0,0,0><0,0.036000,0>0.076200 translate<50.359000,0.000000,23.352000>}
box{<0,0,-0.076200><0.381000,0.036000,0.076200> rotate<0,0.000000,0> translate<50.359000,0.000000,23.352000> }
cylinder{<0,0,0><0,0.036000,0>0.076200 translate<45.660000,0.000000,23.352000>}
cylinder{<0,0,0><0,0.036000,0>0.076200 translate<46.041000,0.000000,23.352000>}
box{<0,0,-0.076200><0.381000,0.036000,0.076200> rotate<0,0.000000,0> translate<45.660000,0.000000,23.352000> }
cylinder{<0,0,0><0,0.036000,0>0.076200 translate<46.041000,0.000000,23.352000>}
cylinder{<0,0,0><0,0.036000,0>0.076200 translate<46.930000,0.000000,23.352000>}
box{<0,0,-0.076200><0.889000,0.036000,0.076200> rotate<0,0.000000,0> translate<46.041000,0.000000,23.352000> }
cylinder{<0,0,0><0,0.036000,0>0.076200 translate<45.660000,0.000000,29.448000>}
cylinder{<0,0,0><0,0.036000,0>0.076200 translate<46.041000,0.000000,29.448000>}
box{<0,0,-0.076200><0.381000,0.036000,0.076200> rotate<0,0.000000,0> translate<45.660000,0.000000,29.448000> }
cylinder{<0,0,0><0,0.036000,0>0.076200 translate<50.740000,0.000000,29.448000>}
cylinder{<0,0,0><0,0.036000,0>0.076200 translate<50.359000,0.000000,29.448000>}
box{<0,0,-0.076200><0.381000,0.036000,0.076200> rotate<0,0.000000,0> translate<50.359000,0.000000,29.448000> }
cylinder{<0,0,0><0,0.036000,0>0.076200 translate<50.359000,0.000000,29.448000>}
cylinder{<0,0,0><0,0.036000,0>0.076200 translate<49.470000,0.000000,29.448000>}
box{<0,0,-0.076200><0.889000,0.036000,0.076200> rotate<0,0.000000,0> translate<49.470000,0.000000,29.448000> }
cylinder{<0,0,0><0,0.036000,0>0.076200 translate<49.470000,0.000000,29.448000>}
cylinder{<0,0,0><0,0.036000,0>0.076200 translate<46.930000,0.000000,29.448000>}
box{<0,0,-0.076200><2.540000,0.036000,0.076200> rotate<0,0.000000,0> translate<46.930000,0.000000,29.448000> }
cylinder{<0,0,0><0,0.036000,0>0.076200 translate<46.930000,0.000000,29.448000>}
cylinder{<0,0,0><0,0.036000,0>0.076200 translate<46.041000,0.000000,29.448000>}
box{<0,0,-0.076200><0.889000,0.036000,0.076200> rotate<0,0.000000,0> translate<46.041000,0.000000,29.448000> }
cylinder{<0,0,0><0,0.036000,0>0.076200 translate<46.930000,0.000000,23.352000>}
cylinder{<0,0,0><0,0.036000,0>0.076200 translate<49.343000,0.000000,23.352000>}
box{<0,0,-0.076200><2.413000,0.036000,0.076200> rotate<0,0.000000,0> translate<46.930000,0.000000,23.352000> }
cylinder{<0,0,0><0,0.036000,0>0.076200 translate<49.343000,0.000000,23.352000>}
cylinder{<0,0,0><0,0.036000,0>0.076200 translate<50.359000,0.000000,23.352000>}
box{<0,0,-0.076200><1.016000,0.036000,0.076200> rotate<0,0.000000,0> translate<49.343000,0.000000,23.352000> }
cylinder{<0,0,0><0,0.036000,0>0.076200 translate<51.248000,0.000000,25.638000>}
cylinder{<0,0,0><0,0.036000,0>0.076200 translate<51.248000,0.000000,25.384000>}
box{<0,0,-0.076200><0.254000,0.036000,0.076200> rotate<0,-90.000000,0> translate<51.248000,0.000000,25.384000> }
cylinder{<0,0,0><0,0.036000,0>0.076200 translate<51.248000,0.000000,27.162000>}
cylinder{<0,0,0><0,0.036000,0>0.076200 translate<51.248000,0.000000,27.416000>}
box{<0,0,-0.076200><0.254000,0.036000,0.076200> rotate<0,90.000000,0> translate<51.248000,0.000000,27.416000> }
cylinder{<0,0,0><0,0.036000,0>0.076200 translate<45.152000,0.000000,25.638000>}
cylinder{<0,0,0><0,0.036000,0>0.076200 translate<45.152000,0.000000,25.384000>}
box{<0,0,-0.076200><0.254000,0.036000,0.076200> rotate<0,-90.000000,0> translate<45.152000,0.000000,25.384000> }
cylinder{<0,0,0><0,0.036000,0>0.076200 translate<45.152000,0.000000,27.162000>}
cylinder{<0,0,0><0,0.036000,0>0.076200 translate<45.152000,0.000000,27.416000>}
box{<0,0,-0.076200><0.254000,0.036000,0.076200> rotate<0,90.000000,0> translate<45.152000,0.000000,27.416000> }
cylinder{<0,0,0><0,0.036000,0>0.076200 translate<46.930000,0.000000,24.241000>}
cylinder{<0,0,0><0,0.036000,0>0.076200 translate<49.470000,0.000000,24.241000>}
box{<0,0,-0.076200><2.540000,0.036000,0.076200> rotate<0,0.000000,0> translate<46.930000,0.000000,24.241000> }
cylinder{<0,0,0><0,0.036000,0>0.076200 translate<49.470000,0.000000,28.686000>}
cylinder{<0,0,0><0,0.036000,0>0.076200 translate<46.930000,0.000000,28.686000>}
box{<0,0,-0.076200><2.540000,0.036000,0.076200> rotate<0,0.000000,0> translate<46.930000,0.000000,28.686000> }
cylinder{<0,0,0><0,0.036000,0>0.076200 translate<45.787000,0.000000,27.670000>}
cylinder{<0,0,0><0,0.036000,0>0.076200 translate<45.787000,0.000000,26.908000>}
box{<0,0,-0.076200><0.762000,0.036000,0.076200> rotate<0,-90.000000,0> translate<45.787000,0.000000,26.908000> }
cylinder{<0,0,0><0,0.036000,0>0.076200 translate<45.787000,0.000000,25.892000>}
cylinder{<0,0,0><0,0.036000,0>0.076200 translate<45.787000,0.000000,25.130000>}
box{<0,0,-0.076200><0.762000,0.036000,0.076200> rotate<0,-90.000000,0> translate<45.787000,0.000000,25.130000> }
cylinder{<0,0,0><0,0.036000,0>0.076200 translate<45.787000,0.000000,26.908000>}
cylinder{<0,0,0><0,0.036000,0>0.076200 translate<46.041000,0.000000,26.019000>}
box{<0,0,-0.076200><0.924574,0.036000,0.076200> rotate<0,74.049717,0> translate<45.787000,0.000000,26.908000> }
difference{
cylinder{<48.200000,0,26.400000><48.200000,0.036000,26.400000>1.854200 translate<0,0.000000,0>}
cylinder{<48.200000,-0.1,26.400000><48.200000,0.135000,26.400000>1.701800 translate<0,0.000000,0>}}
difference{
cylinder{<46.041000,0,24.241000><46.041000,0.036000,24.241000>0.584200 translate<0,0.000000,0>}
cylinder{<46.041000,-0.1,24.241000><46.041000,0.135000,24.241000>0.431800 translate<0,0.000000,0>}}
difference{
cylinder{<50.359000,0,24.368000><50.359000,0.036000,24.368000>0.584200 translate<0,0.000000,0>}
cylinder{<50.359000,-0.1,24.368000><50.359000,0.135000,24.368000>0.431800 translate<0,0.000000,0>}}
difference{
cylinder{<50.359000,0,28.559000><50.359000,0.036000,28.559000>0.584200 translate<0,0.000000,0>}
cylinder{<50.359000,-0.1,28.559000><50.359000,0.135000,28.559000>0.431800 translate<0,0.000000,0>}}
difference{
cylinder{<46.041000,0,28.559000><46.041000,0.036000,28.559000>0.584200 translate<0,0.000000,0>}
cylinder{<46.041000,-0.1,28.559000><46.041000,0.135000,28.559000>0.431800 translate<0,0.000000,0>}}
difference{
cylinder{<48.200000,0,26.400000><48.200000,0.036000,26.400000>0.660400 translate<0,0.000000,0>}
cylinder{<48.200000,-0.1,26.400000><48.200000,0.135000,26.400000>0.609600 translate<0,0.000000,0>}}
difference{
cylinder{<48.200000,0,26.400000><48.200000,0.036000,26.400000>0.330200 translate<0,0.000000,0>}
cylinder{<48.200000,-0.1,26.400000><48.200000,0.135000,26.400000>0.177800 translate<0,0.000000,0>}}
//SV1 silk screen
cylinder{<0,0,0><0,0.036000,0>0.076200 translate<13.540000,-1.536000,10.555000>}
cylinder{<0,0,0><0,0.036000,0>0.076200 translate<13.540000,-1.536000,11.825000>}
box{<0,0,-0.076200><1.270000,0.036000,0.076200> rotate<0,90.000000,0> translate<13.540000,-1.536000,11.825000> }
cylinder{<0,0,0><0,0.036000,0>0.076200 translate<13.540000,-1.536000,11.825000>}
cylinder{<0,0,0><0,0.036000,0>0.076200 translate<12.905000,-1.536000,12.460000>}
box{<0,0,-0.076200><0.898026,0.036000,0.076200> rotate<0,44.997030,0> translate<12.905000,-1.536000,12.460000> }
cylinder{<0,0,0><0,0.036000,0>0.076200 translate<12.905000,-1.536000,12.460000>}
cylinder{<0,0,0><0,0.036000,0>0.076200 translate<13.540000,-1.536000,13.095000>}
box{<0,0,-0.076200><0.898026,0.036000,0.076200> rotate<0,-44.997030,0> translate<12.905000,-1.536000,12.460000> }
cylinder{<0,0,0><0,0.036000,0>0.076200 translate<13.540000,-1.536000,13.095000>}
cylinder{<0,0,0><0,0.036000,0>0.076200 translate<13.540000,-1.536000,14.365000>}
box{<0,0,-0.076200><1.270000,0.036000,0.076200> rotate<0,90.000000,0> translate<13.540000,-1.536000,14.365000> }
cylinder{<0,0,0><0,0.036000,0>0.076200 translate<13.540000,-1.536000,14.365000>}
cylinder{<0,0,0><0,0.036000,0>0.076200 translate<12.905000,-1.536000,15.000000>}
box{<0,0,-0.076200><0.898026,0.036000,0.076200> rotate<0,44.997030,0> translate<12.905000,-1.536000,15.000000> }
cylinder{<0,0,0><0,0.036000,0>0.076200 translate<13.540000,-1.536000,10.555000>}
cylinder{<0,0,0><0,0.036000,0>0.076200 translate<12.905000,-1.536000,9.920000>}
box{<0,0,-0.076200><0.898026,0.036000,0.076200> rotate<0,-44.997030,0> translate<12.905000,-1.536000,9.920000> }
cylinder{<0,0,0><0,0.036000,0>0.076200 translate<12.905000,-1.536000,15.000000>}
cylinder{<0,0,0><0,0.036000,0>0.076200 translate<13.540000,-1.536000,15.635000>}
box{<0,0,-0.076200><0.898026,0.036000,0.076200> rotate<0,-44.997030,0> translate<12.905000,-1.536000,15.000000> }
cylinder{<0,0,0><0,0.036000,0>0.076200 translate<13.540000,-1.536000,15.635000>}
cylinder{<0,0,0><0,0.036000,0>0.076200 translate<13.540000,-1.536000,16.905000>}
box{<0,0,-0.076200><1.270000,0.036000,0.076200> rotate<0,90.000000,0> translate<13.540000,-1.536000,16.905000> }
cylinder{<0,0,0><0,0.036000,0>0.076200 translate<13.540000,-1.536000,16.905000>}
cylinder{<0,0,0><0,0.036000,0>0.076200 translate<12.905000,-1.536000,17.540000>}
box{<0,0,-0.076200><0.898026,0.036000,0.076200> rotate<0,44.997030,0> translate<12.905000,-1.536000,17.540000> }
cylinder{<0,0,0><0,0.036000,0>0.076200 translate<13.540000,-1.536000,18.175000>}
cylinder{<0,0,0><0,0.036000,0>0.076200 translate<13.540000,-1.536000,19.445000>}
box{<0,0,-0.076200><1.270000,0.036000,0.076200> rotate<0,90.000000,0> translate<13.540000,-1.536000,19.445000> }
cylinder{<0,0,0><0,0.036000,0>0.076200 translate<13.540000,-1.536000,18.175000>}
cylinder{<0,0,0><0,0.036000,0>0.076200 translate<12.905000,-1.536000,17.540000>}
box{<0,0,-0.076200><0.898026,0.036000,0.076200> rotate<0,-44.997030,0> translate<12.905000,-1.536000,17.540000> }
cylinder{<0,0,0><0,0.036000,0>0.076200 translate<13.540000,-1.536000,19.445000>}
cylinder{<0,0,0><0,0.036000,0>0.076200 translate<12.905000,-1.536000,20.080000>}
box{<0,0,-0.076200><0.898026,0.036000,0.076200> rotate<0,44.997030,0> translate<12.905000,-1.536000,20.080000> }
cylinder{<0,0,0><0,0.036000,0>0.076200 translate<12.905000,-1.536000,20.080000>}
cylinder{<0,0,0><0,0.036000,0>0.076200 translate<9.095000,-1.536000,20.080000>}
box{<0,0,-0.076200><3.810000,0.036000,0.076200> rotate<0,0.000000,0> translate<9.095000,-1.536000,20.080000> }
cylinder{<0,0,0><0,0.036000,0>0.076200 translate<9.095000,-1.536000,12.460000>}
cylinder{<0,0,0><0,0.036000,0>0.076200 translate<8.460000,-1.536000,11.825000>}
box{<0,0,-0.076200><0.898026,0.036000,0.076200> rotate<0,-44.997030,0> translate<8.460000,-1.536000,11.825000> }
cylinder{<0,0,0><0,0.036000,0>0.076200 translate<9.095000,-1.536000,15.000000>}
cylinder{<0,0,0><0,0.036000,0>0.076200 translate<8.460000,-1.536000,14.365000>}
box{<0,0,-0.076200><0.898026,0.036000,0.076200> rotate<0,-44.997030,0> translate<8.460000,-1.536000,14.365000> }
cylinder{<0,0,0><0,0.036000,0>0.076200 translate<8.460000,-1.536000,14.365000>}
cylinder{<0,0,0><0,0.036000,0>0.076200 translate<8.460000,-1.536000,13.095000>}
box{<0,0,-0.076200><1.270000,0.036000,0.076200> rotate<0,-90.000000,0> translate<8.460000,-1.536000,13.095000> }
cylinder{<0,0,0><0,0.036000,0>0.076200 translate<8.460000,-1.536000,13.095000>}
cylinder{<0,0,0><0,0.036000,0>0.076200 translate<9.095000,-1.536000,12.460000>}
box{<0,0,-0.076200><0.898026,0.036000,0.076200> rotate<0,44.997030,0> translate<8.460000,-1.536000,13.095000> }
cylinder{<0,0,0><0,0.036000,0>0.076200 translate<12.905000,-1.536000,9.920000>}
cylinder{<0,0,0><0,0.036000,0>0.076200 translate<9.095000,-1.536000,9.920000>}
box{<0,0,-0.076200><3.810000,0.036000,0.076200> rotate<0,0.000000,0> translate<9.095000,-1.536000,9.920000> }
cylinder{<0,0,0><0,0.036000,0>0.076200 translate<9.095000,-1.536000,9.920000>}
cylinder{<0,0,0><0,0.036000,0>0.076200 translate<8.460000,-1.536000,10.555000>}
box{<0,0,-0.076200><0.898026,0.036000,0.076200> rotate<0,44.997030,0> translate<8.460000,-1.536000,10.555000> }
cylinder{<0,0,0><0,0.036000,0>0.076200 translate<8.460000,-1.536000,11.825000>}
cylinder{<0,0,0><0,0.036000,0>0.076200 translate<8.460000,-1.536000,10.555000>}
box{<0,0,-0.076200><1.270000,0.036000,0.076200> rotate<0,-90.000000,0> translate<8.460000,-1.536000,10.555000> }
cylinder{<0,0,0><0,0.036000,0>0.076200 translate<9.095000,-1.536000,17.540000>}
cylinder{<0,0,0><0,0.036000,0>0.076200 translate<8.460000,-1.536000,16.905000>}
box{<0,0,-0.076200><0.898026,0.036000,0.076200> rotate<0,-44.997030,0> translate<8.460000,-1.536000,16.905000> }
cylinder{<0,0,0><0,0.036000,0>0.076200 translate<8.460000,-1.536000,16.905000>}
cylinder{<0,0,0><0,0.036000,0>0.076200 translate<8.460000,-1.536000,15.635000>}
box{<0,0,-0.076200><1.270000,0.036000,0.076200> rotate<0,-90.000000,0> translate<8.460000,-1.536000,15.635000> }
cylinder{<0,0,0><0,0.036000,0>0.076200 translate<8.460000,-1.536000,15.635000>}
cylinder{<0,0,0><0,0.036000,0>0.076200 translate<9.095000,-1.536000,15.000000>}
box{<0,0,-0.076200><0.898026,0.036000,0.076200> rotate<0,44.997030,0> translate<8.460000,-1.536000,15.635000> }
cylinder{<0,0,0><0,0.036000,0>0.076200 translate<9.095000,-1.536000,17.540000>}
cylinder{<0,0,0><0,0.036000,0>0.076200 translate<8.460000,-1.536000,18.175000>}
box{<0,0,-0.076200><0.898026,0.036000,0.076200> rotate<0,44.997030,0> translate<8.460000,-1.536000,18.175000> }
cylinder{<0,0,0><0,0.036000,0>0.076200 translate<8.460000,-1.536000,19.445000>}
cylinder{<0,0,0><0,0.036000,0>0.076200 translate<8.460000,-1.536000,18.175000>}
box{<0,0,-0.076200><1.270000,0.036000,0.076200> rotate<0,-90.000000,0> translate<8.460000,-1.536000,18.175000> }
cylinder{<0,0,0><0,0.036000,0>0.076200 translate<9.095000,-1.536000,20.080000>}
cylinder{<0,0,0><0,0.036000,0>0.076200 translate<8.460000,-1.536000,19.445000>}
box{<0,0,-0.076200><0.898026,0.036000,0.076200> rotate<0,-44.997030,0> translate<8.460000,-1.536000,19.445000> }
box{<-0.254000,0,-0.254000><0.254000,0.036000,0.254000> rotate<0,-90.000000,0> translate<9.730000,-1.536000,13.730000>}
box{<-0.254000,0,-0.254000><0.254000,0.036000,0.254000> rotate<0,-90.000000,0> translate<9.730000,-1.536000,11.190000>}
box{<-0.254000,0,-0.254000><0.254000,0.036000,0.254000> rotate<0,-90.000000,0> translate<9.730000,-1.536000,16.270000>}
box{<-0.254000,0,-0.254000><0.254000,0.036000,0.254000> rotate<0,-90.000000,0> translate<9.730000,-1.536000,18.810000>}
box{<-0.254000,0,-0.254000><0.254000,0.036000,0.254000> rotate<0,-90.000000,0> translate<12.270000,-1.536000,11.190000>}
box{<-0.254000,0,-0.254000><0.254000,0.036000,0.254000> rotate<0,-90.000000,0> translate<12.270000,-1.536000,13.730000>}
box{<-0.254000,0,-0.254000><0.254000,0.036000,0.254000> rotate<0,-90.000000,0> translate<12.270000,-1.536000,16.270000>}
box{<-0.254000,0,-0.254000><0.254000,0.036000,0.254000> rotate<0,-90.000000,0> translate<12.270000,-1.536000,18.810000>}
//T1 silk screen
cylinder{<0,0,0><0,0.036000,0>0.063500 translate<-9.377600,-1.536000,20.239600>}
cylinder{<0,0,0><0,0.036000,0>0.063500 translate<-9.377600,-1.536000,21.560400>}
box{<0,0,-0.063500><1.320800,0.036000,0.063500> rotate<0,90.000000,0> translate<-9.377600,-1.536000,21.560400> }
cylinder{<0,0,0><0,0.036000,0>0.063500 translate<-9.377600,-1.536000,21.560400>}
cylinder{<0,0,0><0,0.036000,0>0.063500 translate<-12.222400,-1.536000,21.560400>}
box{<0,0,-0.063500><2.844800,0.036000,0.063500> rotate<0,0.000000,0> translate<-12.222400,-1.536000,21.560400> }
cylinder{<0,0,0><0,0.036000,0>0.063500 translate<-12.222400,-1.536000,21.560400>}
cylinder{<0,0,0><0,0.036000,0>0.063500 translate<-12.222400,-1.536000,20.239600>}
box{<0,0,-0.063500><1.320800,0.036000,0.063500> rotate<0,-90.000000,0> translate<-12.222400,-1.536000,20.239600> }
cylinder{<0,0,0><0,0.036000,0>0.063500 translate<-12.222400,-1.536000,20.239600>}
cylinder{<0,0,0><0,0.036000,0>0.063500 translate<-9.377600,-1.536000,20.239600>}
box{<0,0,-0.063500><2.844800,0.036000,0.063500> rotate<0,0.000000,0> translate<-12.222400,-1.536000,20.239600> }
box{<-0.228600,0,-0.292100><0.228600,0.036000,0.292100> rotate<0,-0.000000,0> translate<-10.800000,-1.536000,19.896700>}
box{<-0.228600,0,-0.292100><0.228600,0.036000,0.292100> rotate<0,-0.000000,0> translate<-9.860200,-1.536000,21.903300>}
box{<-0.228600,0,-0.292100><0.228600,0.036000,0.292100> rotate<0,-0.000000,0> translate<-11.739800,-1.536000,21.903300>}
//U1 silk screen
cylinder{<0,0,0><0,0.036000,0>0.101600 translate<-7.700000,0.000000,15.000000>}
cylinder{<0,0,0><0,0.036000,0>0.101600 translate<-11.000000,0.000000,15.000000>}
box{<0,0,-0.101600><3.300000,0.036000,0.101600> rotate<0,0.000000,0> translate<-11.000000,0.000000,15.000000> }
cylinder{<0,0,0><0,0.036000,0>0.101600 translate<-11.000000,0.000000,15.000000>}
cylinder{<0,0,0><0,0.036000,0>0.101600 translate<-11.500000,0.000000,15.000000>}
box{<0,0,-0.101600><0.500000,0.036000,0.101600> rotate<0,0.000000,0> translate<-11.500000,0.000000,15.000000> }
cylinder{<0,0,0><0,0.036000,0>0.101600 translate<-11.500000,0.000000,15.000000>}
cylinder{<0,0,0><0,0.036000,0>0.101600 translate<-11.500000,0.000000,19.800000>}
box{<0,0,-0.101600><4.800000,0.036000,0.101600> rotate<0,90.000000,0> translate<-11.500000,0.000000,19.800000> }
cylinder{<0,0,0><0,0.036000,0>0.101600 translate<-11.500000,0.000000,19.800000>}
cylinder{<0,0,0><0,0.036000,0>0.101600 translate<-11.000000,0.000000,19.800000>}
box{<0,0,-0.101600><0.500000,0.036000,0.101600> rotate<0,0.000000,0> translate<-11.500000,0.000000,19.800000> }
cylinder{<0,0,0><0,0.036000,0>0.101600 translate<-11.000000,0.000000,19.800000>}
cylinder{<0,0,0><0,0.036000,0>0.101600 translate<-7.700000,0.000000,19.800000>}
box{<0,0,-0.101600><3.300000,0.036000,0.101600> rotate<0,0.000000,0> translate<-11.000000,0.000000,19.800000> }
cylinder{<0,0,0><0,0.036000,0>0.101600 translate<-7.700000,0.000000,19.800000>}
cylinder{<0,0,0><0,0.036000,0>0.101600 translate<-7.700000,0.000000,15.000000>}
box{<0,0,-0.101600><4.800000,0.036000,0.101600> rotate<0,-90.000000,0> translate<-7.700000,0.000000,15.000000> }
cylinder{<0,0,0><0,0.036000,0>0.101600 translate<-11.000000,0.000000,15.000000>}
cylinder{<0,0,0><0,0.036000,0>0.101600 translate<-11.000000,0.000000,19.800000>}
box{<0,0,-0.101600><4.800000,0.036000,0.101600> rotate<0,90.000000,0> translate<-11.000000,0.000000,19.800000> }
box{<-0.245000,0,-0.550000><0.245000,0.036000,0.550000> rotate<0,-270.000000,0> translate<-12.150000,0.000000,19.305000>}
box{<-0.245000,0,-0.550000><0.245000,0.036000,0.550000> rotate<0,-270.000000,0> translate<-12.150000,0.000000,18.035000>}
box{<-0.245000,0,-0.550000><0.245000,0.036000,0.550000> rotate<0,-270.000000,0> translate<-12.150000,0.000000,16.765000>}
box{<-0.245000,0,-0.550000><0.245000,0.036000,0.550000> rotate<0,-270.000000,0> translate<-12.150000,0.000000,15.495000>}
box{<-0.245000,0,-0.550000><0.245000,0.036000,0.550000> rotate<0,-270.000000,0> translate<-7.050000,0.000000,15.495000>}
box{<-0.245000,0,-0.550000><0.245000,0.036000,0.550000> rotate<0,-270.000000,0> translate<-7.050000,0.000000,16.765000>}
box{<-0.245000,0,-0.550000><0.245000,0.036000,0.550000> rotate<0,-270.000000,0> translate<-7.050000,0.000000,18.035000>}
box{<-0.245000,0,-0.550000><0.245000,0.036000,0.550000> rotate<0,-270.000000,0> translate<-7.050000,0.000000,19.305000>}
//X1 silk screen
cylinder{<0,0,0><0,0.036000,0>0.101600 translate<19.550000,0.000000,21.990000>}
cylinder{<0,0,0><0,0.036000,0>0.101600 translate<19.550000,0.000000,23.790000>}
box{<0,0,-0.101600><1.800000,0.036000,0.101600> rotate<0,90.000000,0> translate<19.550000,0.000000,23.790000> }
cylinder{<0,0,0><0,0.036000,0>0.101600 translate<19.550000,0.000000,23.790000>}
cylinder{<0,0,0><0,0.036000,0>0.101600 translate<19.550000,0.000000,27.785000>}
box{<0,0,-0.101600><3.995000,0.036000,0.101600> rotate<0,90.000000,0> translate<19.550000,0.000000,27.785000> }
cylinder{<0,0,0><0,0.036000,0>0.101600 translate<19.550000,0.000000,27.785000>}
cylinder{<0,0,0><0,0.036000,0>0.101600 translate<19.550000,0.000000,29.585000>}
box{<0,0,-0.101600><1.800000,0.036000,0.101600> rotate<0,90.000000,0> translate<19.550000,0.000000,29.585000> }
cylinder{<0,0,0><0,0.036000,0>0.101600 translate<19.550000,0.000000,29.585000>}
cylinder{<0,0,0><0,0.036000,0>0.101600 translate<9.775000,0.000000,29.585000>}
box{<0,0,-0.101600><9.775000,0.036000,0.101600> rotate<0,0.000000,0> translate<9.775000,0.000000,29.585000> }
cylinder{<0,0,0><0,0.036000,0>0.101600 translate<9.775000,0.000000,29.585000>}
cylinder{<0,0,0><0,0.036000,0>0.101600 translate<9.775000,0.000000,21.990000>}
box{<0,0,-0.101600><7.595000,0.036000,0.101600> rotate<0,-90.000000,0> translate<9.775000,0.000000,21.990000> }
cylinder{<0,0,0><0,0.036000,0>0.101600 translate<9.775000,0.000000,21.990000>}
cylinder{<0,0,0><0,0.036000,0>0.101600 translate<19.550000,0.000000,21.990000>}
box{<0,0,-0.101600><9.775000,0.036000,0.101600> rotate<0,0.000000,0> translate<9.775000,0.000000,21.990000> }
cylinder{<0,0,0><0,0.036000,0>0.101600 translate<17.125000,0.000000,27.785000>}
cylinder{<0,0,0><0,0.036000,0>0.101600 translate<17.125000,0.000000,23.790000>}
box{<0,0,-0.101600><3.995000,0.036000,0.101600> rotate<0,-90.000000,0> translate<17.125000,0.000000,23.790000> }
cylinder{<0,0,0><0,0.036000,0>0.101600 translate<17.125000,0.000000,23.790000>}
cylinder{<0,0,0><0,0.036000,0>0.101600 translate<19.550000,0.000000,23.790000>}
box{<0,0,-0.101600><2.425000,0.036000,0.101600> rotate<0,0.000000,0> translate<17.125000,0.000000,23.790000> }
cylinder{<0,0,0><0,0.036000,0>0.101600 translate<17.125000,0.000000,27.785000>}
cylinder{<0,0,0><0,0.036000,0>0.101600 translate<19.550000,0.000000,27.785000>}
box{<0,0,-0.101600><2.425000,0.036000,0.101600> rotate<0,0.000000,0> translate<17.125000,0.000000,27.785000> }
//X2 silk screen
cylinder{<0,0,0><0,0.036000,0>0.101600 translate<52.050000,0.000000,24.770000>}
cylinder{<0,0,0><0,0.036000,0>0.101600 translate<52.050000,0.000000,22.970000>}
box{<0,0,-0.101600><1.800000,0.036000,0.101600> rotate<0,-90.000000,0> translate<52.050000,0.000000,22.970000> }
cylinder{<0,0,0><0,0.036000,0>0.101600 translate<52.050000,0.000000,22.970000>}
cylinder{<0,0,0><0,0.036000,0>0.101600 translate<52.050000,0.000000,11.055000>}
box{<0,0,-0.101600><11.915000,0.036000,0.101600> rotate<0,-90.000000,0> translate<52.050000,0.000000,11.055000> }
cylinder{<0,0,0><0,0.036000,0>0.101600 translate<52.050000,0.000000,11.055000>}
cylinder{<0,0,0><0,0.036000,0>0.101600 translate<52.050000,0.000000,9.255000>}
box{<0,0,-0.101600><1.800000,0.036000,0.101600> rotate<0,-90.000000,0> translate<52.050000,0.000000,9.255000> }
cylinder{<0,0,0><0,0.036000,0>0.101600 translate<52.050000,0.000000,9.255000>}
cylinder{<0,0,0><0,0.036000,0>0.101600 translate<61.825000,0.000000,9.255000>}
box{<0,0,-0.101600><9.775000,0.036000,0.101600> rotate<0,0.000000,0> translate<52.050000,0.000000,9.255000> }
cylinder{<0,0,0><0,0.036000,0>0.101600 translate<61.825000,0.000000,9.255000>}
cylinder{<0,0,0><0,0.036000,0>0.101600 translate<61.825000,0.000000,24.770000>}
box{<0,0,-0.101600><15.515000,0.036000,0.101600> rotate<0,90.000000,0> translate<61.825000,0.000000,24.770000> }
cylinder{<0,0,0><0,0.036000,0>0.101600 translate<61.825000,0.000000,24.770000>}
cylinder{<0,0,0><0,0.036000,0>0.101600 translate<52.050000,0.000000,24.770000>}
box{<0,0,-0.101600><9.775000,0.036000,0.101600> rotate<0,0.000000,0> translate<52.050000,0.000000,24.770000> }
cylinder{<0,0,0><0,0.036000,0>0.101600 translate<54.475000,0.000000,11.055000>}
cylinder{<0,0,0><0,0.036000,0>0.101600 translate<54.475000,0.000000,22.970000>}
box{<0,0,-0.101600><11.915000,0.036000,0.101600> rotate<0,90.000000,0> translate<54.475000,0.000000,22.970000> }
cylinder{<0,0,0><0,0.036000,0>0.101600 translate<54.475000,0.000000,22.970000>}
cylinder{<0,0,0><0,0.036000,0>0.101600 translate<52.050000,0.000000,22.970000>}
box{<0,0,-0.101600><2.425000,0.036000,0.101600> rotate<0,0.000000,0> translate<52.050000,0.000000,22.970000> }
cylinder{<0,0,0><0,0.036000,0>0.101600 translate<54.475000,0.000000,11.055000>}
cylinder{<0,0,0><0,0.036000,0>0.101600 translate<52.050000,0.000000,11.055000>}
box{<0,0,-0.101600><2.425000,0.036000,0.101600> rotate<0,0.000000,0> translate<52.050000,0.000000,11.055000> }
texture{col_slk}
}
#end
translate<mac_x_ver,mac_y_ver,mac_z_ver>
rotate<mac_x_rot,mac_y_rot,mac_z_rot>
}//End union
#end

#if(use_file_as_inc=off)
object{  KICK(-22.000000,0,-15.000000,pcb_rotate_x,pcb_rotate_y,pcb_rotate_z)
#if(pcb_upsidedown=on)
rotate pcb_rotdir*180
#end
}
#end


//Parts not found in 3dpack.dat or 3dusrpac.dat are:
