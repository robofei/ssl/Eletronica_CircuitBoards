//POVRay-File created by 3d41.ulp v20110101
//C:/Users/jgurzoni/Documents/Futebol Robos/F180/PCBs/kick revB2/kick-2010_revB2.brd
//9/18/2013 12:02:49 PM

#version 3.5;

//Set to on if the file should be used as .inc
#local use_file_as_inc = off;
#if(use_file_as_inc=off)


//changes the apperance of resistors (1 Blob / 0 real)
#declare global_res_shape = 1;
//randomize color of resistors 1=random 0=same color
#declare global_res_colselect = 0;
//Number of the color for the resistors
//0=Green, 1="normal color" 2=Blue 3=Brown
#declare global_res_col = 1;
//Set to on if you want to render the PCB upside-down
#declare pcb_upsidedown = off;
//Set to x or z to rotate around the corresponding axis (referring to pcb_upsidedown)
#declare pcb_rotdir = x;
//Set the length off short pins over the PCB
#declare pin_length = 2.5;
#declare global_diode_bend_radius = 1;
#declare global_res_bend_radius = 1;
#declare global_solder = on;

#declare global_show_screws = on;
#declare global_show_washers = on;
#declare global_show_nuts = on;

#declare global_use_radiosity = on;

#declare global_ambient_mul = 1;
#declare global_ambient_mul_emit = 0;

//Animation
#declare global_anim = off;
#local global_anim_showcampath = no;

#declare global_fast_mode = off;

#declare col_preset = 2;
#declare pin_short = on;

#declare e3d_environment = off;

#local cam_x = 0;
#local cam_y = 256;
#local cam_z = -69;
#local cam_a = 20;
#local cam_look_x = 0;
#local cam_look_y = -3;
#local cam_look_z = 0;

#local pcb_rotate_x = 0;
#local pcb_rotate_y = 0;
#local pcb_rotate_z = 0;

#local pcb_board = on;
#local pcb_parts = on;
#local pcb_wire_bridges = off;
#if(global_fast_mode=off)
	#local pcb_polygons = on;
	#local pcb_silkscreen = on;
	#local pcb_wires = on;
	#local pcb_pads_smds = on;
#else
	#local pcb_polygons = off;
	#local pcb_silkscreen = off;
	#local pcb_wires = off;
	#local pcb_pads_smds = off;
#end

#local lgt1_pos_x = 30;
#local lgt1_pos_y = 45;
#local lgt1_pos_z = 15;
#local lgt1_intense = 0.734286;
#local lgt2_pos_x = -30;
#local lgt2_pos_y = 45;
#local lgt2_pos_z = 15;
#local lgt2_intense = 0.734286;
#local lgt3_pos_x = 30;
#local lgt3_pos_y = 45;
#local lgt3_pos_z = -10;
#local lgt3_intense = 0.734286;
#local lgt4_pos_x = -30;
#local lgt4_pos_y = 45;
#local lgt4_pos_z = -10;
#local lgt4_intense = 0.734286;

//Do not change these values
#declare pcb_height = 1.500000;
#declare pcb_cuheight = 0.035000;
#declare pcb_x_size = 80.000000;
#declare pcb_y_size = 30.000000;
#declare pcb_layer1_used = 1;
#declare pcb_layer16_used = 1;
#declare inc_testmode = off;
#declare global_seed=seed(159);
#declare global_pcb_layer_dis = array[16]
{
	0.000000,
	0.000000,
	0.000000,
	0.000000,
	0.000000,
	0.000000,
	0.000000,
	0.000000,
	0.000000,
	0.000000,
	0.000000,
	0.000000,
	0.000000,
	0.000000,
	0.000000,
	1.535000,
}
#declare global_pcb_real_hole = 2.000000;

#include "e3d_tools.inc"
#include "e3d_user.inc"

global_settings{charset utf8}    

#declare col_brd = texture{pigment{rgb<0.084314,0.184314,0.309804>}}
#declare col_wrs = texture{pigment{rgb<0.368627,0.352941,0.054902>}}
#declare col_pds = texture{pigment{Gray70}}
#declare col_hls = texture{pigment{Black}}
#declare col_bgr = Gray50;
#declare col_slk = texture{pigment{White}}
#declare col_thl = texture{pigment{Gray70}}
#declare col_pol = texture{pigment{rgb<0.368627,0.352941,0.054902>}}


#if(e3d_environment=on)
sky_sphere {pigment {Navy}
pigment {bozo turbulence 0.65 octaves 7 omega 0.7 lambda 2
color_map {
[0.0 0.1 color rgb <0.85, 0.85, 0.85> color rgb <0.75, 0.75, 0.75>]
[0.1 0.5 color rgb <0.75, 0.75, 0.75> color rgbt <1, 1, 1, 1>]
[0.5 1.0 color rgbt <1, 1, 1, 1> color rgbt <1, 1, 1, 1>]}
scale <0.1, 0.5, 0.1>} rotate -90*x}
plane{y, -10.0-max(pcb_x_size,pcb_y_size)*abs(max(sin((pcb_rotate_x/180)*pi),sin((pcb_rotate_z/180)*pi)))
texture{T_Chrome_2D
normal{waves 0.1 frequency 3000.0 scale 3000.0}} translate<0,0,0>}
#end

//Animation data
#if(global_anim=on)
#declare global_anim_showcampath = no;
#end

#if((global_anim=on)|(global_anim_showcampath=yes))
#declare global_anim_npoints_cam_flight=0;
#warning "No/not enough Animation Data available (min. 3 points) (Flight path)"
#end

#if((global_anim=on)|(global_anim_showcampath=yes))
#declare global_anim_npoints_cam_view=0;
#warning "No/not enough Animation Data available (min. 3 points) (View path)"
#end

#if((global_anim=on)|(global_anim_showcampath=yes))
#end

#if((global_anim_showcampath=yes)&(global_anim=off))
#end
#if(global_anim=on)
camera
{
	location global_anim_spline_cam_flight(clock)
	#if(global_anim_npoints_cam_view>2)
		look_at global_anim_spline_cam_view(clock)
	#else
		look_at global_anim_spline_cam_flight(clock+0.01)-<0,-0.01,0>
	#end
	angle 45
}
light_source
{
	global_anim_spline_cam_flight(clock)
	color rgb <1,1,1>
	spotlight point_at 
	#if(global_anim_npoints_cam_view>2)
		global_anim_spline_cam_view(clock)
	#else
		global_anim_spline_cam_flight(clock+0.01)-<0,-0.01,0>
	#end
	radius 35 falloff  40
}
#else
camera
{
	location <cam_x,cam_y,cam_z>
	look_at <cam_look_x,cam_look_y,cam_look_z>
	angle cam_a
	//translates the camera that <0,0,0> is over the Eagle <0,0>
	//translate<-40.000000,0,-15.000000>
}
#end

background{col_bgr}
light_source{<lgt1_pos_x,lgt1_pos_y,lgt1_pos_z> White*lgt1_intense}
light_source{<lgt2_pos_x,lgt2_pos_y,lgt2_pos_z> White*lgt2_intense}
light_source{<lgt3_pos_x,lgt3_pos_y,lgt3_pos_z> White*lgt3_intense}
light_source{<lgt4_pos_x,lgt4_pos_y,lgt4_pos_z> White*lgt4_intense}
#end


#macro KICK_2010_REVB2(mac_x_ver,mac_y_ver,mac_z_ver,mac_x_rot,mac_y_rot,mac_z_rot)
union{
#if(pcb_board = on)
difference{
union{
//Board
prism{-1.500000,0.000000,8
<-18.000000,0.000000><62.000000,0.000000>
<62.000000,0.000000><62.000000,30.000000>
<62.000000,30.000000><-18.000000,30.000000>
<-18.000000,30.000000><-18.000000,0.000000>
texture{col_brd}}
}//End union(PCB)
//Holes(real)/Parts
//Holes(real)/Board
cylinder{<2.000000,1,2.000000><2.000000,-5,2.000000>1.600000 texture{col_hls}}
cylinder{<60.000000,1,2.005000><60.000000,-5,2.005000>1.600000 texture{col_hls}}
cylinder{<59.962000,1,28.054000><59.962000,-5,28.054000>1.600000 texture{col_hls}}
cylinder{<2.000000,1,28.000000><2.000000,-5,28.000000>1.600000 texture{col_hls}}
//Holes(real)/Vias
}//End difference(reale Bohrungen/Durchbrüche)
#end
#if(pcb_parts=on)//Parts
union{
#ifndef(pack_C1) #declare global_pack_C1=yes; object {CAP_SMD_CHIP_1206()translate<0,0,0> rotate<0,0.000000,0>rotate<0,-180.000000,0> rotate<0,0,0> translate<45.800000,0.000000,15.400000>translate<0,0.035000,0> }#end		//SMD Capacitor 1206 C1 100Kpf/250V C1206
#ifndef(pack_C2) #declare global_pack_C2=yes; object {CAP_SMD_CHIP_2220()translate<0,0,0> rotate<0,0.000000,0>rotate<0,0.000000,0> rotate<0,0,0> translate<26.600000,0.000000,14.500000>translate<0,0.035000,0> }#end		//SMD Capacitor 2220 C2 1uF/200V C2225K
#ifndef(pack_C3) #declare global_pack_C3=yes; object {CAP_SMD_CHIP_0603()translate<0,0,0> rotate<0,0.000000,0>rotate<0,0.000000,0> rotate<0,0,0> translate<-0.371400,0.000000,16.203000>translate<0,0.035000,0> }#end		//SMD Capacitor 0603 C3 1nF/50V C0603
#ifndef(pack_C4) #declare global_pack_C4=yes; object {CAP_SMD_CHIP_1206()translate<0,0,0> rotate<0,0.000000,0>rotate<0,-90.000000,0> rotate<0,0,0> translate<2.338000,0.000000,12.141000>translate<0,0.035000,0> }#end		//SMD Capacitor 1206 C4 10nF/250V C1206
#ifndef(pack_C5) #declare global_pack_C5=yes; object {CAP_SMD_CHIP_0603()translate<0,0,0> rotate<0,0.000000,0>rotate<0,-180.000000,0> rotate<0,0,0> translate<7.652400,0.000000,1.980000>translate<0,0.035000,0> }#end		//SMD Capacitor 0603 C5 10nF/50V C0603
#ifndef(pack_D1) #declare global_pack_D1=yes; object {DIODE_SMD_SMC()translate<0,0,0> rotate<0,0.000000,0>rotate<0,-180.000000,0> rotate<0,0,0> translate<1.400000,0.000000,20.568000>translate<0,0.035000,0> }#end		//DO214 BA D1 MBRS3201T3G SMC
#ifndef(pack_D2) #declare global_pack_D2=yes; object {DIODE_SMD_SMB()translate<0,-0,0> rotate<0,0.000000,0>rotate<0,0.000000,0> rotate<0,0,180> translate<-2.200000,-1.500000,22.803200>translate<0,-0.035000,0> }#end		//DO214 AA D2 MBRS130LT3 SMB
#ifndef(pack_D3) #declare global_pack_D3=yes; object {DIODE_SMD_SMC()translate<0,-0,0> rotate<0,0.000000,0>rotate<0,-270.000000,0> rotate<0,0,180> translate<39.200000,-1.500000,12.530000>translate<0,-0.035000,0> }#end		//DO214 BA D3 MBRS3201T3G SMC
#ifndef(pack_D4) #declare global_pack_D4=yes; object {DIODE_SMD_SMC()translate<0,-0,0> rotate<0,0.000000,0>rotate<0,-90.000000,0> rotate<0,0,180> translate<46.200000,-1.500000,17.600000>translate<0,-0.035000,0> }#end		//DO214 BA D4 MBRS3201T3G SMC
#ifndef(pack_D5) #declare global_pack_D5=yes; object {DIODE_SMD_SMC()translate<0,-0,0> rotate<0,0.000000,0>rotate<0,-180.000000,0> rotate<0,0,180> translate<-11.125200,-1.500000,3.860800>translate<0,-0.035000,0> }#end		//DO214 BA D5 MBRS3201T3G SMC
#ifndef(pack_F1) #declare global_pack_F1=yes; object {SPC_FUSE1("2A",)translate<0,0,0> rotate<0,0.000000,0>rotate<0,-180.000000,0> rotate<0,0,0> translate<29.600000,0.000000,4.400000>}#end		//Fuse Holder (special.lib) F1 2A FUSE
#ifndef(pack_IC2) #declare global_pack_IC2=yes; object {IC_SMD_SO8("TC4426","",)translate<0,-0,0> rotate<0,0.000000,0>rotate<0,-270.000000,0> rotate<0,0,180> translate<6.800000,-1.500000,23.000000>translate<0,-0.035000,0> }#end		//SMD IC SO8 Package IC2 TC4426 SO08
#ifndef(pack_J1) #declare global_pack_J1=yes; object {CON_MOLEX_PSL2W()translate<0,0,0> rotate<0,0.000000,0>rotate<0,-90.000000,0> rotate<0,0,0> translate<48.400000,0.000000,6.000000>}#end		//PC mount header J1 22-05-7028-02 7395-02
#ifndef(pack_L1) #declare global_pack_L1=yes; object {SPC_L_WE_PD_L("47uH/2A",)translate<0,0,0> rotate<0,0.000000,0>rotate<0,0.000000,0> rotate<0,0,0> translate<-9.000000,0.000000,7.000000>}#end		//Inductor WE-PD Wuerth Elektronik L1 47uH/2A DR125
#ifndef(pack_Q1) #declare global_pack_Q1=yes; object {TR_TO220_3_V("IRF740",)translate<0,0,0> rotate<0,0.000000,0>rotate<0,0.000000,0> rotate<0,0,0> translate<-9.600000,0.000000,29.600000>}#end		//TO220 vertical straight leads Q1 IRF740 TO220BV
#ifndef(pack_Q2) #declare global_pack_Q2=yes; object {TR_TO220_3_V("IRFSL4127",)translate<0,0,0> rotate<0,0.000000,0>rotate<0,0.000000,0> rotate<0,0,0> translate<26.400000,0.000000,26.400000>}#end		//TO220 vertical straight leads Q2 IRFSL4127 TO262-V
#ifndef(pack_Q3) #declare global_pack_Q3=yes; object {TR_TO220_3_V("IRFSL4127",)translate<0,0,0> rotate<0,0.000000,0>rotate<0,0.000000,0> rotate<0,0,0> translate<38.000000,0.000000,26.400000>}#end		//TO220 vertical straight leads Q3 IRFSL4127 TO262-V
#ifndef(pack_R1) #declare global_pack_R1=yes; object {RES_SMD_CHIP_2512("1R5",)translate<0,-0,0> rotate<0,0.000000,0>rotate<0,-270.000000,0> rotate<0,0,180> translate<-13.847400,-1.500000,11.702000>translate<0,-0.035000,0> }#end		//SMD Resistor 2512 R1 0.15R R2512
#ifndef(pack_R2) #declare global_pack_R2=yes; object {RES_SMD_CHIP_0805("470",)translate<0,0,0> rotate<0,0.000000,0>rotate<0,0.000000,0> rotate<0,0,0> translate<-14.400000,0.000000,22.100000>translate<0,0.035000,0> }#end		//SMD Resistor 0805 R2 47R M0805
#ifndef(pack_R3) #declare global_pack_R3=yes; object {RES_SMD_CHIP_0805("102",)translate<0,0,0> rotate<0,0.000000,0>rotate<0,-270.000000,0> rotate<0,0,0> translate<2.390000,0.000000,7.623000>translate<0,0.035000,0> }#end		//SMD Resistor 0805 R3 1K M0805
#ifndef(pack_R4) #declare global_pack_R4=yes; object {RES_SMD_CHIP_2512("332",)translate<0,0,0> rotate<0,0.000000,0>rotate<0,-180.000000,0> rotate<0,0,0> translate<47.000000,0.000000,19.000000>translate<0,0.035000,0> }#end		//SMD Resistor 2512 R4 3K3 2W R2512
#ifndef(pack_R5) #declare global_pack_R5=yes; object {RES_SMD_CHIP_0805("102",)translate<0,-0,0> rotate<0,0.000000,0>rotate<0,-180.000000,0> rotate<0,0,180> translate<-2.375000,-1.500000,27.205000>translate<0,-0.035000,0> }#end		//SMD Resistor 0805 R5 1K M0805
#ifndef(pack_R6) #declare global_pack_R6=yes; object {RES_SMD_CHIP_0805("102",)translate<0,0,0> rotate<0,0.000000,0>rotate<0,0.000000,0> rotate<0,0,0> translate<-9.500000,0.000000,22.200000>translate<0,0.035000,0> }#end		//SMD Resistor 0805 R6 1K M0805
#ifndef(pack_R7) #declare global_pack_R7=yes; object {RES_SMD_CHIP_0805("154",)translate<0,0,0> rotate<0,0.000000,0>rotate<0,0.000000,0> rotate<0,0,0> translate<12.236000,0.000000,3.106000>translate<0,0.035000,0> }#end		//SMD Resistor 0805 R7 150K 1% M0805
#ifndef(pack_R8) #declare global_pack_R8=yes; object {RES_SMD_CHIP_0805("122",)translate<0,0,0> rotate<0,0.000000,0>rotate<0,-180.000000,0> rotate<0,0,0> translate<7.710000,0.000000,4.060000>translate<0,0.035000,0> }#end		//SMD Resistor 0805 R8 1K2 1% M0805
#ifndef(pack_R9) #declare global_pack_R9=yes; object {RES_SMD_CHIP_0805("391",)translate<0,-0,0> rotate<0,0.000000,0>rotate<0,0.000000,0> rotate<0,0,180> translate<16.500000,-1.500000,10.528600>translate<0,-0.035000,0> }#end		//SMD Resistor 0805 R9 390R M0805
#ifndef(pack_R10) #declare global_pack_R10=yes; object {RES_SMD_CHIP_0805("391",)translate<0,-0,0> rotate<0,0.000000,0>rotate<0,-180.000000,0> rotate<0,0,180> translate<5.588000,-1.500000,12.065000>translate<0,-0.035000,0> }#end		//SMD Resistor 0805 R10 390R M0805
#ifndef(pack_R11) #declare global_pack_R11=yes; object {RES_SMD_CHIP_0805("122",)translate<0,-0,0> rotate<0,0.000000,0>rotate<0,0.000000,0> rotate<0,0,180> translate<5.842000,-1.500000,14.605000>translate<0,-0.035000,0> }#end		//SMD Resistor 0805 R11 1k2 M0805
#ifndef(pack_R12) #declare global_pack_R12=yes; object {RES_SMD_CHIP_0805("122",)translate<0,-0,0> rotate<0,0.000000,0>rotate<0,-90.000000,0> rotate<0,0,180> translate<13.232000,-1.500000,7.194000>translate<0,-0.035000,0> }#end		//SMD Resistor 0805 R12 1K2 M0805
#ifndef(pack_R13) #declare global_pack_R13=yes; object {RES_SMD_CHIP_0805("102",)translate<0,-0,0> rotate<0,0.000000,0>rotate<0,-90.000000,0> rotate<0,0,180> translate<19.800000,-1.500000,26.800000>translate<0,-0.035000,0> }#end		//SMD Resistor 0805 R13 1K M0805
#ifndef(pack_R14) #declare global_pack_R14=yes; object {RES_SMD_CHIP_0805("102",)translate<0,-0,0> rotate<0,0.000000,0>rotate<0,-180.000000,0> rotate<0,0,180> translate<34.432000,-1.500000,22.820000>translate<0,-0.035000,0> }#end		//SMD Resistor 0805 R14 1K M0805
#ifndef(pack_R15) #declare global_pack_R15=yes; object {RES_SMD_CHIP_0805("391",)translate<0,-0,0> rotate<0,0.000000,0>rotate<0,-180.000000,0> rotate<0,0,180> translate<16.800000,-1.500000,13.600000>translate<0,-0.035000,0> }#end		//SMD Resistor 0805 R15 390R M0805
#ifndef(pack_R16) #declare global_pack_R16=yes; object {RES_SMD_CHIP_0805("122",)translate<0,0,0> rotate<0,0.000000,0>rotate<0,-90.000000,0> rotate<0,0,0> translate<5.815000,0.000000,14.285000>translate<0,0.035000,0> }#end		//SMD Resistor 0805 R16 1K2 M0805
#ifndef(pack_R17) #declare global_pack_R17=yes; object {RES_SMD_CHIP_0805("103",)translate<0,-0,0> rotate<0,0.000000,0>rotate<0,0.000000,0> rotate<0,0,180> translate<-0.635000,-1.500000,17.145000>translate<0,-0.035000,0> }#end		//SMD Resistor 0805 R17 10k M0805
#ifndef(pack_R18) #declare global_pack_R18=yes; object {RES_SMD_CHIP_0805("222",)translate<0,-0,0> rotate<0,0.000000,0>rotate<0,0.000000,0> rotate<0,0,180> translate<-0.635000,-1.500000,13.970000>translate<0,-0.035000,0> }#end		//SMD Resistor 0805 R18 2k2 M0805
#ifndef(pack_S1) #declare global_pack_S1=yes; object {SWITCH_B3F_10XX1()translate<0,0,0> rotate<0,0.000000,0>rotate<0,0.000000,0> rotate<0,0,0> translate<48.200000,0.000000,26.400000>}#end		//Tactile Switch-Omron S1  B3F-10XX
#ifndef(pack_SV1) #declare global_pack_SV1=yes; object {CON_PH_2X4()translate<0,-0,0> rotate<0,0.000000,0>rotate<0,-90.000000,0> rotate<0,0,180> translate<11.000000,-1.500000,15.000000>}#end		//Header 2,54mm Grid 4Pin 2Row (jumper.lib) SV1  MA04-2
#ifndef(pack_T1) #declare global_pack_T1=yes; object {IC_SMD_SOT23("MMBT3906","",)translate<0,-0,0> rotate<0,0.000000,0>rotate<0,-180.000000,0> rotate<0,0,180> translate<-10.800000,-1.500000,20.900000>translate<0,-0.035000,0> }#end		//SOT23 T1 MMBT3906 SOT23-BEC
#ifndef(pack_T2) #declare global_pack_T2=yes; object {IC_SMD_SOT23("MMBT3906","",)translate<0,-0,0> rotate<0,0.000000,0>rotate<0,0.000000,0> rotate<0,0,180> translate<-5.105400,-1.500000,15.417800>translate<0,-0.035000,0> }#end		//SOT23 T2 MMBT3906 SOT23-BEC
#ifndef(pack_U1) #declare global_pack_U1=yes; object {IC_SMD_SO8("MC34063AD","",)translate<0,0,0> rotate<0,0.000000,0>rotate<0,-270.000000,0> rotate<0,0,0> translate<-9.600000,0.000000,17.400000>translate<0,0.035000,0> }#end		//SMD IC SO8 Package U1 MC34063AD SOIC8
#ifndef(pack_X1) #declare global_pack_X1=yes; object {CON_MOLEX_PSL2G()translate<0,0,2.54> rotate<0,180.000000,0>rotate<0,-270.000000,0> rotate<0,0,0> translate<14.600000,0.000000,25.800000>}#end		//PC mount header X1  KK-156-2
#ifndef(pack_X2) #declare global_pack_X2=yes; object {CON_MOLEX_PSL4G()translate<0,0,2.54> rotate<0,180.000000,0>rotate<0,-90.000000,0> rotate<0,0,0> translate<57.000000,0.000000,17.000000>}#end		//PC mount header X2  KK-156-4
}//End union
#end
#if(pcb_pads_smds=on)
//Pads&SMD/Parts
object{TOOLS_PCB_SMD(1.600000,1.800000,0.037000,0) rotate<0,-180.000000,0> texture{col_pds} translate<47.200000,0.000000,15.400000>}
object{TOOLS_PCB_SMD(1.600000,1.800000,0.037000,0) rotate<0,-180.000000,0> texture{col_pds} translate<44.400000,0.000000,15.400000>}
object{TOOLS_PCB_SMD(1.850000,6.800000,0.037000,0) rotate<0,-0.000000,0> texture{col_pds} translate<24.050000,0.000000,14.500000>}
object{TOOLS_PCB_SMD(1.850000,6.800000,0.037000,0) rotate<0,-0.000000,0> texture{col_pds} translate<29.150000,0.000000,14.500000>}
object{TOOLS_PCB_SMD(1.100000,1.000000,0.037000,0) rotate<0,-0.000000,0> texture{col_pds} translate<-1.221400,0.000000,16.203000>}
object{TOOLS_PCB_SMD(1.100000,1.000000,0.037000,0) rotate<0,-0.000000,0> texture{col_pds} translate<0.478600,0.000000,16.203000>}
object{TOOLS_PCB_SMD(1.600000,1.800000,0.037000,0) rotate<0,-90.000000,0> texture{col_pds} translate<2.338000,0.000000,10.741000>}
object{TOOLS_PCB_SMD(1.600000,1.800000,0.037000,0) rotate<0,-90.000000,0> texture{col_pds} translate<2.338000,0.000000,13.541000>}
object{TOOLS_PCB_SMD(1.100000,1.000000,0.037000,0) rotate<0,-180.000000,0> texture{col_pds} translate<8.502400,0.000000,1.980000>}
object{TOOLS_PCB_SMD(1.100000,1.000000,0.037000,0) rotate<0,-180.000000,0> texture{col_pds} translate<6.802400,0.000000,1.980000>}
object{TOOLS_PCB_SMD(2.800000,3.800000,0.037000,0) rotate<0,-180.000000,0> texture{col_pds} translate<-2.300000,0.000000,20.568000>}
object{TOOLS_PCB_SMD(2.800000,3.800000,0.037000,0) rotate<0,-180.000000,0> texture{col_pds} translate<5.100000,0.000000,20.568000>}
object{TOOLS_PCB_SMD(2.400000,2.400000,0.037000,0) rotate<0,-180.000000,0> texture{col_pds} translate<-4.400000,-1.537000,22.803200>}
object{TOOLS_PCB_SMD(2.400000,2.400000,0.037000,0) rotate<0,-180.000000,0> texture{col_pds} translate<0.000000,-1.537000,22.803200>}
object{TOOLS_PCB_SMD(2.800000,3.800000,0.037000,0) rotate<0,-270.000000,0> texture{col_pds} translate<39.200000,-1.537000,8.830000>}
object{TOOLS_PCB_SMD(2.800000,3.800000,0.037000,0) rotate<0,-270.000000,0> texture{col_pds} translate<39.200000,-1.537000,16.230000>}
object{TOOLS_PCB_SMD(2.800000,3.800000,0.037000,0) rotate<0,-90.000000,0> texture{col_pds} translate<46.200000,-1.537000,21.300000>}
object{TOOLS_PCB_SMD(2.800000,3.800000,0.037000,0) rotate<0,-90.000000,0> texture{col_pds} translate<46.200000,-1.537000,13.900000>}
object{TOOLS_PCB_SMD(2.800000,3.800000,0.037000,0) rotate<0,-0.000000,0> texture{col_pds} translate<-7.425200,-1.537000,3.860800>}
object{TOOLS_PCB_SMD(2.800000,3.800000,0.037000,0) rotate<0,-0.000000,0> texture{col_pds} translate<-14.825200,-1.537000,3.860800>}
#ifndef(global_pack_F1) #local global_tmp=0; #else #local global_tmp=100; #end object{TOOLS_PCB_VIA(1.981200,1.320800,1,16,3+global_tmp,100) rotate<0,-180.000000,0>translate<41.030000,0,4.400000> texture{col_thl}}
#ifndef(global_pack_F1) #local global_tmp=0; #else #local global_tmp=100; #end object{TOOLS_PCB_VIA(1.981200,1.320800,1,16,3+global_tmp,100) rotate<0,-180.000000,0>translate<18.170000,0,4.400000> texture{col_thl}}
object{TOOLS_PCB_SMD(1.400000,1.800000,0.037000,0) rotate<0,-90.000000,0> texture{col_pds} translate<32.080000,-1.537000,7.670000>}
object{TOOLS_PCB_SMD(1.400000,1.800000,0.037000,0) rotate<0,-90.000000,0> texture{col_pds} translate<32.080000,-1.537000,5.130000>}
object{TOOLS_PCB_SMD(1.400000,1.800000,0.037000,0) rotate<0,-90.000000,0> texture{col_pds} translate<21.920000,-1.537000,5.130000>}
object{TOOLS_PCB_SMD(1.400000,1.800000,0.037000,0) rotate<0,-90.000000,0> texture{col_pds} translate<21.920000,-1.537000,7.670000>}
object{TOOLS_PCB_SMD(0.720000,1.780000,0.037000,0) rotate<0,-270.000000,0> texture{col_pds} translate<9.150000,-1.537000,24.905000>}
object{TOOLS_PCB_SMD(0.720000,1.780000,0.037000,0) rotate<0,-270.000000,0> texture{col_pds} translate<9.150000,-1.537000,23.635000>}
object{TOOLS_PCB_SMD(0.720000,1.780000,0.037000,0) rotate<0,-270.000000,0> texture{col_pds} translate<9.150000,-1.537000,22.365000>}
object{TOOLS_PCB_SMD(0.720000,1.780000,0.037000,0) rotate<0,-270.000000,0> texture{col_pds} translate<9.150000,-1.537000,21.095000>}
object{TOOLS_PCB_SMD(0.720000,1.780000,0.037000,0) rotate<0,-270.000000,0> texture{col_pds} translate<4.450000,-1.537000,21.095000>}
object{TOOLS_PCB_SMD(0.720000,1.780000,0.037000,0) rotate<0,-270.000000,0> texture{col_pds} translate<4.450000,-1.537000,22.365000>}
object{TOOLS_PCB_SMD(0.720000,1.780000,0.037000,0) rotate<0,-270.000000,0> texture{col_pds} translate<4.450000,-1.537000,23.635000>}
object{TOOLS_PCB_SMD(0.720000,1.780000,0.037000,0) rotate<0,-270.000000,0> texture{col_pds} translate<4.450000,-1.537000,24.905000>}
object{TOOLS_PCB_SMD(1.400000,1.800000,0.037000,0) rotate<0,-90.000000,0> texture{col_pds} translate<1.143000,-1.537000,11.455400>}
object{TOOLS_PCB_SMD(1.400000,1.800000,0.037000,0) rotate<0,-90.000000,0> texture{col_pds} translate<1.143000,-1.537000,8.915400>}
object{TOOLS_PCB_SMD(1.400000,1.800000,0.037000,0) rotate<0,-90.000000,0> texture{col_pds} translate<-9.017000,-1.537000,8.915400>}
object{TOOLS_PCB_SMD(1.400000,1.800000,0.037000,0) rotate<0,-90.000000,0> texture{col_pds} translate<-9.017000,-1.537000,11.455400>}
object{TOOLS_PCB_SMD(1.400000,1.800000,0.037000,0) rotate<0,-90.000000,0> texture{col_pds} translate<32.180000,-1.537000,13.370000>}
object{TOOLS_PCB_SMD(1.400000,1.800000,0.037000,0) rotate<0,-90.000000,0> texture{col_pds} translate<32.180000,-1.537000,10.830000>}
object{TOOLS_PCB_SMD(1.400000,1.800000,0.037000,0) rotate<0,-90.000000,0> texture{col_pds} translate<22.020000,-1.537000,10.830000>}
object{TOOLS_PCB_SMD(1.400000,1.800000,0.037000,0) rotate<0,-90.000000,0> texture{col_pds} translate<22.020000,-1.537000,13.370000>}
#ifndef(global_pack_J1) #local global_tmp=0; #else #local global_tmp=100; #end object{TOOLS_PCB_VIA(1.524000,1.016000,1,16,3+global_tmp,100) rotate<0,-180.000000,0>translate<48.400000,0,7.270000> texture{col_thl}}
#ifndef(global_pack_J1) #local global_tmp=0; #else #local global_tmp=100; #end object{TOOLS_PCB_VIA(1.524000,1.016000,1,16,3+global_tmp,100) rotate<0,-180.000000,0>translate<48.400000,0,4.730000> texture{col_thl}}
object{TOOLS_PCB_SMD(3.850000,5.500000,0.037000,0) rotate<0,-0.000000,0> texture{col_pds} translate<-13.975000,0.000000,7.000000>}
object{TOOLS_PCB_SMD(3.850000,5.500000,0.037000,0) rotate<0,-0.000000,0> texture{col_pds} translate<-4.025000,0.000000,7.000000>}
#ifndef(global_pack_Q1) #local global_tmp=0; #else #local global_tmp=100; #end object{TOOLS_PCB_VIA(1.524000,1.016000,1,16,3+global_tmp,100) rotate<0,-90.000000,0>translate<-9.600000,0,27.060000> texture{col_thl}}
#ifndef(global_pack_Q1) #local global_tmp=0; #else #local global_tmp=100; #end object{TOOLS_PCB_VIA(1.524000,1.016000,1,16,3+global_tmp,100) rotate<0,-90.000000,0>translate<-12.140000,0,27.060000> texture{col_thl}}
#ifndef(global_pack_Q1) #local global_tmp=0; #else #local global_tmp=100; #end object{TOOLS_PCB_VIA(1.524000,1.016000,1,16,3+global_tmp,100) rotate<0,-90.000000,0>translate<-7.060000,0,27.060000> texture{col_thl}}
#ifndef(global_pack_Q2) #local global_tmp=0; #else #local global_tmp=100; #end object{TOOLS_PCB_VIA(1.524000,1.016000,1,16,3+global_tmp,100) rotate<0,-90.000000,0>translate<23.860000,0,26.400000> texture{col_thl}}
#ifndef(global_pack_Q2) #local global_tmp=0; #else #local global_tmp=100; #end object{TOOLS_PCB_VIA(1.524000,1.016000,1,16,3+global_tmp,100) rotate<0,-90.000000,0>translate<26.400000,0,26.400000> texture{col_thl}}
#ifndef(global_pack_Q2) #local global_tmp=0; #else #local global_tmp=100; #end object{TOOLS_PCB_VIA(1.524000,1.016000,1,16,3+global_tmp,100) rotate<0,-90.000000,0>translate<28.940000,0,26.400000> texture{col_thl}}
#ifndef(global_pack_Q3) #local global_tmp=0; #else #local global_tmp=100; #end object{TOOLS_PCB_VIA(1.524000,1.016000,1,16,3+global_tmp,100) rotate<0,-90.000000,0>translate<35.460000,0,26.400000> texture{col_thl}}
#ifndef(global_pack_Q3) #local global_tmp=0; #else #local global_tmp=100; #end object{TOOLS_PCB_VIA(1.524000,1.016000,1,16,3+global_tmp,100) rotate<0,-90.000000,0>translate<38.000000,0,26.400000> texture{col_thl}}
#ifndef(global_pack_Q3) #local global_tmp=0; #else #local global_tmp=100; #end object{TOOLS_PCB_VIA(1.524000,1.016000,1,16,3+global_tmp,100) rotate<0,-90.000000,0>translate<40.540000,0,26.400000> texture{col_thl}}
object{TOOLS_PCB_SMD(1.800000,3.200000,0.037000,0) rotate<0,-270.000000,0> texture{col_pds} translate<-13.847400,-1.537000,14.502000>}
object{TOOLS_PCB_SMD(1.800000,3.200000,0.037000,0) rotate<0,-270.000000,0> texture{col_pds} translate<-13.847400,-1.537000,8.902000>}
object{TOOLS_PCB_SMD(1.300000,1.600000,0.037000,0) rotate<0,-0.000000,0> texture{col_pds} translate<-15.350000,0.000000,22.100000>}
object{TOOLS_PCB_SMD(1.300000,1.600000,0.037000,0) rotate<0,-0.000000,0> texture{col_pds} translate<-13.450000,0.000000,22.100000>}
object{TOOLS_PCB_SMD(1.300000,1.600000,0.037000,0) rotate<0,-270.000000,0> texture{col_pds} translate<2.390000,0.000000,8.573000>}
object{TOOLS_PCB_SMD(1.300000,1.600000,0.037000,0) rotate<0,-270.000000,0> texture{col_pds} translate<2.390000,0.000000,6.673000>}
object{TOOLS_PCB_SMD(1.800000,3.200000,0.037000,0) rotate<0,-180.000000,0> texture{col_pds} translate<49.800000,0.000000,19.000000>}
object{TOOLS_PCB_SMD(1.800000,3.200000,0.037000,0) rotate<0,-180.000000,0> texture{col_pds} translate<44.200000,0.000000,19.000000>}
object{TOOLS_PCB_SMD(1.300000,1.600000,0.037000,0) rotate<0,-0.000000,0> texture{col_pds} translate<-3.325000,-1.537000,27.205000>}
object{TOOLS_PCB_SMD(1.300000,1.600000,0.037000,0) rotate<0,-0.000000,0> texture{col_pds} translate<-1.425000,-1.537000,27.205000>}
object{TOOLS_PCB_SMD(1.300000,1.600000,0.037000,0) rotate<0,-0.000000,0> texture{col_pds} translate<-10.450000,0.000000,22.200000>}
object{TOOLS_PCB_SMD(1.300000,1.600000,0.037000,0) rotate<0,-0.000000,0> texture{col_pds} translate<-8.550000,0.000000,22.200000>}
object{TOOLS_PCB_SMD(1.300000,1.600000,0.037000,0) rotate<0,-0.000000,0> texture{col_pds} translate<11.286000,0.000000,3.106000>}
object{TOOLS_PCB_SMD(1.300000,1.600000,0.037000,0) rotate<0,-0.000000,0> texture{col_pds} translate<13.186000,0.000000,3.106000>}
object{TOOLS_PCB_SMD(1.300000,1.600000,0.037000,0) rotate<0,-180.000000,0> texture{col_pds} translate<8.660000,0.000000,4.060000>}
object{TOOLS_PCB_SMD(1.300000,1.600000,0.037000,0) rotate<0,-180.000000,0> texture{col_pds} translate<6.760000,0.000000,4.060000>}
object{TOOLS_PCB_SMD(1.300000,1.600000,0.037000,0) rotate<0,-180.000000,0> texture{col_pds} translate<17.450000,-1.537000,10.528600>}
object{TOOLS_PCB_SMD(1.300000,1.600000,0.037000,0) rotate<0,-180.000000,0> texture{col_pds} translate<15.550000,-1.537000,10.528600>}
object{TOOLS_PCB_SMD(1.300000,1.600000,0.037000,0) rotate<0,-0.000000,0> texture{col_pds} translate<4.638000,-1.537000,12.065000>}
object{TOOLS_PCB_SMD(1.300000,1.600000,0.037000,0) rotate<0,-0.000000,0> texture{col_pds} translate<6.538000,-1.537000,12.065000>}
object{TOOLS_PCB_SMD(1.300000,1.600000,0.037000,0) rotate<0,-180.000000,0> texture{col_pds} translate<6.792000,-1.537000,14.605000>}
object{TOOLS_PCB_SMD(1.300000,1.600000,0.037000,0) rotate<0,-180.000000,0> texture{col_pds} translate<4.892000,-1.537000,14.605000>}
object{TOOLS_PCB_SMD(1.300000,1.600000,0.037000,0) rotate<0,-90.000000,0> texture{col_pds} translate<13.232000,-1.537000,6.244000>}
object{TOOLS_PCB_SMD(1.300000,1.600000,0.037000,0) rotate<0,-90.000000,0> texture{col_pds} translate<13.232000,-1.537000,8.144000>}
object{TOOLS_PCB_SMD(1.300000,1.600000,0.037000,0) rotate<0,-90.000000,0> texture{col_pds} translate<19.800000,-1.537000,25.850000>}
object{TOOLS_PCB_SMD(1.300000,1.600000,0.037000,0) rotate<0,-90.000000,0> texture{col_pds} translate<19.800000,-1.537000,27.750000>}
object{TOOLS_PCB_SMD(1.300000,1.600000,0.037000,0) rotate<0,-0.000000,0> texture{col_pds} translate<33.482000,-1.537000,22.820000>}
object{TOOLS_PCB_SMD(1.300000,1.600000,0.037000,0) rotate<0,-0.000000,0> texture{col_pds} translate<35.382000,-1.537000,22.820000>}
object{TOOLS_PCB_SMD(1.300000,1.600000,0.037000,0) rotate<0,-0.000000,0> texture{col_pds} translate<15.850000,-1.537000,13.600000>}
object{TOOLS_PCB_SMD(1.300000,1.600000,0.037000,0) rotate<0,-0.000000,0> texture{col_pds} translate<17.750000,-1.537000,13.600000>}
object{TOOLS_PCB_SMD(1.300000,1.600000,0.037000,0) rotate<0,-90.000000,0> texture{col_pds} translate<5.815000,0.000000,13.335000>}
object{TOOLS_PCB_SMD(1.300000,1.600000,0.037000,0) rotate<0,-90.000000,0> texture{col_pds} translate<5.815000,0.000000,15.235000>}
object{TOOLS_PCB_SMD(1.300000,1.600000,0.037000,0) rotate<0,-180.000000,0> texture{col_pds} translate<0.315000,-1.537000,17.145000>}
object{TOOLS_PCB_SMD(1.300000,1.600000,0.037000,0) rotate<0,-180.000000,0> texture{col_pds} translate<-1.585000,-1.537000,17.145000>}
object{TOOLS_PCB_SMD(1.300000,1.600000,0.037000,0) rotate<0,-180.000000,0> texture{col_pds} translate<0.315000,-1.537000,13.970000>}
object{TOOLS_PCB_SMD(1.300000,1.600000,0.037000,0) rotate<0,-180.000000,0> texture{col_pds} translate<-1.585000,-1.537000,13.970000>}
#ifndef(global_pack_S1) #local global_tmp=0; #else #local global_tmp=100; #end object{TOOLS_PCB_VIA(1.524000,1.016000,1,16,3+global_tmp,100) rotate<0,-0.000000,0>translate<44.948800,0,28.660600> texture{col_thl}}
#ifndef(global_pack_S1) #local global_tmp=0; #else #local global_tmp=100; #end object{TOOLS_PCB_VIA(1.524000,1.016000,1,16,3+global_tmp,100) rotate<0,-0.000000,0>translate<51.451200,0,28.660600> texture{col_thl}}
#ifndef(global_pack_S1) #local global_tmp=0; #else #local global_tmp=100; #end object{TOOLS_PCB_VIA(1.524000,1.016000,1,16,3+global_tmp,100) rotate<0,-0.000000,0>translate<44.948800,0,24.139400> texture{col_thl}}
#ifndef(global_pack_S1) #local global_tmp=0; #else #local global_tmp=100; #end object{TOOLS_PCB_VIA(1.524000,1.016000,1,16,3+global_tmp,100) rotate<0,-0.000000,0>translate<51.451200,0,24.139400> texture{col_thl}}
#ifndef(global_pack_SV1) #local global_tmp=0; #else #local global_tmp=100; #end object{TOOLS_PCB_VIA(1.524000,1.016000,1,16,2+global_tmp,0) rotate<0,-90.000000,0>translate<9.730000,0,11.190000> texture{col_thl}}
#ifndef(global_pack_SV1) #local global_tmp=0; #else #local global_tmp=100; #end object{TOOLS_PCB_VIA(1.524000,1.016000,1,16,2+global_tmp,0) rotate<0,-90.000000,0>translate<12.270000,0,11.190000> texture{col_thl}}
#ifndef(global_pack_SV1) #local global_tmp=0; #else #local global_tmp=100; #end object{TOOLS_PCB_VIA(1.524000,1.016000,1,16,2+global_tmp,0) rotate<0,-90.000000,0>translate<9.730000,0,13.730000> texture{col_thl}}
#ifndef(global_pack_SV1) #local global_tmp=0; #else #local global_tmp=100; #end object{TOOLS_PCB_VIA(1.524000,1.016000,1,16,2+global_tmp,0) rotate<0,-90.000000,0>translate<12.270000,0,13.730000> texture{col_thl}}
#ifndef(global_pack_SV1) #local global_tmp=0; #else #local global_tmp=100; #end object{TOOLS_PCB_VIA(1.524000,1.016000,1,16,2+global_tmp,0) rotate<0,-90.000000,0>translate<9.730000,0,16.270000> texture{col_thl}}
#ifndef(global_pack_SV1) #local global_tmp=0; #else #local global_tmp=100; #end object{TOOLS_PCB_VIA(1.524000,1.016000,1,16,2+global_tmp,0) rotate<0,-90.000000,0>translate<12.270000,0,16.270000> texture{col_thl}}
#ifndef(global_pack_SV1) #local global_tmp=0; #else #local global_tmp=100; #end object{TOOLS_PCB_VIA(1.524000,1.016000,1,16,2+global_tmp,0) rotate<0,-90.000000,0>translate<9.730000,0,18.810000> texture{col_thl}}
#ifndef(global_pack_SV1) #local global_tmp=0; #else #local global_tmp=100; #end object{TOOLS_PCB_VIA(1.524000,1.016000,1,16,2+global_tmp,0) rotate<0,-90.000000,0>translate<12.270000,0,18.810000> texture{col_thl}}
object{TOOLS_PCB_SMD(1.000000,1.400000,0.037000,0) rotate<0,-0.000000,0> texture{col_pds} translate<-11.750000,-1.537000,22.000000>}
object{TOOLS_PCB_SMD(1.000000,1.400000,0.037000,0) rotate<0,-0.000000,0> texture{col_pds} translate<-10.800000,-1.537000,19.800000>}
object{TOOLS_PCB_SMD(1.000000,1.400000,0.037000,0) rotate<0,-0.000000,0> texture{col_pds} translate<-9.850000,-1.537000,22.000000>}
object{TOOLS_PCB_SMD(1.000000,1.400000,0.037000,0) rotate<0,-180.000000,0> texture{col_pds} translate<-4.155400,-1.537000,14.317800>}
object{TOOLS_PCB_SMD(1.000000,1.400000,0.037000,0) rotate<0,-180.000000,0> texture{col_pds} translate<-5.105400,-1.537000,16.517800>}
object{TOOLS_PCB_SMD(1.000000,1.400000,0.037000,0) rotate<0,-180.000000,0> texture{col_pds} translate<-6.055400,-1.537000,14.317800>}
object{TOOLS_PCB_SMD(0.600000,2.200000,0.037000,0) rotate<0,-270.000000,0> texture{col_pds} translate<-12.200000,0.000000,19.305000>}
object{TOOLS_PCB_SMD(0.600000,2.200000,0.037000,0) rotate<0,-270.000000,0> texture{col_pds} translate<-12.200000,0.000000,18.035000>}
object{TOOLS_PCB_SMD(0.600000,2.200000,0.037000,0) rotate<0,-270.000000,0> texture{col_pds} translate<-12.200000,0.000000,16.765000>}
object{TOOLS_PCB_SMD(0.600000,2.200000,0.037000,0) rotate<0,-270.000000,0> texture{col_pds} translate<-12.200000,0.000000,15.495000>}
object{TOOLS_PCB_SMD(0.600000,2.200000,0.037000,0) rotate<0,-270.000000,0> texture{col_pds} translate<-7.000000,0.000000,15.495000>}
object{TOOLS_PCB_SMD(0.600000,2.200000,0.037000,0) rotate<0,-270.000000,0> texture{col_pds} translate<-7.000000,0.000000,16.765000>}
object{TOOLS_PCB_SMD(0.600000,2.200000,0.037000,0) rotate<0,-270.000000,0> texture{col_pds} translate<-7.000000,0.000000,18.035000>}
object{TOOLS_PCB_SMD(0.600000,2.200000,0.037000,0) rotate<0,-270.000000,0> texture{col_pds} translate<-7.000000,0.000000,19.305000>}
#ifndef(global_pack_X1) #local global_tmp=0; #else #local global_tmp=100; #end object{TOOLS_PCB_VIA(2.550000,1.700000,1,16,3+global_tmp,100) rotate<0,-0.000000,0>translate<14.600000,0,27.780000> texture{col_thl}}
#ifndef(global_pack_X1) #local global_tmp=0; #else #local global_tmp=100; #end object{TOOLS_PCB_VIA(2.550000,1.700000,1,16,3+global_tmp,100) rotate<0,-0.000000,0>translate<14.600000,0,23.820000> texture{col_thl}}
#ifndef(global_pack_X2) #local global_tmp=0; #else #local global_tmp=100; #end object{TOOLS_PCB_VIA(2.550000,1.700000,1,16,3+global_tmp,100) rotate<0,-180.000000,0>translate<57.000000,0,11.060000> texture{col_thl}}
#ifndef(global_pack_X2) #local global_tmp=0; #else #local global_tmp=100; #end object{TOOLS_PCB_VIA(2.550000,1.700000,1,16,3+global_tmp,100) rotate<0,-180.000000,0>translate<57.000000,0,15.020000> texture{col_thl}}
#ifndef(global_pack_X2) #local global_tmp=0; #else #local global_tmp=100; #end object{TOOLS_PCB_VIA(2.550000,1.700000,1,16,3+global_tmp,100) rotate<0,-180.000000,0>translate<57.000000,0,18.980000> texture{col_thl}}
#ifndef(global_pack_X2) #local global_tmp=0; #else #local global_tmp=100; #end object{TOOLS_PCB_VIA(2.550000,1.700000,1,16,3+global_tmp,100) rotate<0,-180.000000,0>translate<57.000000,0,22.940000> texture{col_thl}}
//Pads/Vias
object{TOOLS_PCB_VIA(0.762000,0.254000,1,16,1,0) translate<2.289000,0,15.981000> texture{col_thl}}
object{TOOLS_PCB_VIA(0.762000,0.254000,1,16,1,0) translate<5.425000,0,2.523000> texture{col_thl}}
object{TOOLS_PCB_VIA(0.762000,0.254000,1,16,1,0) translate<-14.997400,0,16.770000> texture{col_thl}}
object{TOOLS_PCB_VIA(0.762000,0.254000,1,16,1,0) translate<27.100000,0,16.100000> texture{col_thl}}
object{TOOLS_PCB_VIA(0.762000,0.254000,1,16,1,0) translate<15.008000,0,21.295200> texture{col_thl}}
object{TOOLS_PCB_VIA(0.762000,0.254000,1,16,1,0) translate<15.008000,0,18.000000> texture{col_thl}}
object{TOOLS_PCB_VIA(0.762000,0.254000,1,16,1,0) translate<42.900000,0,16.600000> texture{col_thl}}
object{TOOLS_PCB_VIA(0.762000,0.254000,1,16,1,0) translate<-9.400000,0,20.600000> texture{col_thl}}
object{TOOLS_PCB_VIA(1.108000,0.600000,1,16,1,0) translate<51.200000,0,7.400000> texture{col_thl}}
object{TOOLS_PCB_VIA(0.762000,0.254000,1,16,1,0) translate<-5.130800,0,17.805400> texture{col_thl}}
object{TOOLS_PCB_VIA(0.762000,0.254000,1,16,1,0) translate<10.800000,0,8.224000> texture{col_thl}}
object{TOOLS_PCB_VIA(0.762000,0.254000,1,16,1,0) translate<10.801600,0,23.600000> texture{col_thl}}
object{TOOLS_PCB_VIA(0.762000,0.254000,1,16,1,0) translate<4.262000,0,17.538000> texture{col_thl}}
object{TOOLS_PCB_VIA(0.762000,0.254000,1,16,1,0) translate<19.800000,0,13.400000> texture{col_thl}}
object{TOOLS_PCB_VIA(0.762000,0.254000,1,16,1,0) translate<10.700000,0,21.100000> texture{col_thl}}
object{TOOLS_PCB_VIA(0.762000,0.254000,1,16,1,0) translate<1.040000,0,19.210000> texture{col_thl}}
object{TOOLS_PCB_VIA(0.762000,0.254000,1,16,1,0) translate<-9.325400,0,16.111200> texture{col_thl}}
object{TOOLS_PCB_VIA(0.762000,0.254000,1,16,1,0) translate<-11.700000,0,23.400000> texture{col_thl}}
object{TOOLS_PCB_VIA(0.762000,0.254000,1,16,1,0) translate<-14.000000,0,18.100000> texture{col_thl}}
object{TOOLS_PCB_VIA(0.762000,0.254000,1,16,1,0) translate<-6.771400,0,21.782600> texture{col_thl}}
object{TOOLS_PCB_VIA(0.762000,0.254000,1,16,1,0) translate<-12.300000,0,20.300000> texture{col_thl}}
object{TOOLS_PCB_VIA(0.762000,0.254000,1,16,1,0) translate<-8.000000,0,20.500000> texture{col_thl}}
object{TOOLS_PCB_VIA(0.762000,0.254000,1,16,1,0) translate<25.100000,0,5.100000> texture{col_thl}}
object{TOOLS_PCB_VIA(0.762000,0.254000,1,16,1,0) translate<19.400000,0,9.800000> texture{col_thl}}
object{TOOLS_PCB_VIA(1.108000,0.600000,1,16,1,0) translate<35.000000,0,11.730000> texture{col_thl}}
object{TOOLS_PCB_VIA(1.108000,0.600000,1,16,1,0) translate<42.900000,0,7.100000> texture{col_thl}}
object{TOOLS_PCB_VIA(0.762000,0.254000,1,16,1,0) translate<7.400000,0,22.200000> texture{col_thl}}
object{TOOLS_PCB_VIA(0.762000,0.254000,1,16,1,0) translate<9.144000,0,6.096000> texture{col_thl}}
object{TOOLS_PCB_VIA(1.108000,0.600000,1,16,1,0) translate<-14.224000,0,11.557000> texture{col_thl}}
object{TOOLS_PCB_VIA(1.108000,0.600000,1,16,1,0) translate<44.600000,0,9.200000> texture{col_thl}}
#end
#if(pcb_wires=on)
union{
//Signals
cylinder{<0,0,0><0,0.035000,0>0.254000 translate<-16.510000,-1.535000,5.545600>}
cylinder{<0,0,0><0,0.035000,0>0.254000 translate<-16.510000,-1.535000,11.839400>}
box{<0,0,-0.254000><6.293800,0.035000,0.254000> rotate<0,90.000000,0> translate<-16.510000,-1.535000,11.839400> }
cylinder{<0,0,0><0,0.035000,0>0.254000 translate<-16.383000,0.000000,18.415000>}
cylinder{<0,0,0><0,0.035000,0>0.254000 translate<-16.383000,0.000000,13.716000>}
box{<0,0,-0.254000><4.699000,0.035000,0.254000> rotate<0,-90.000000,0> translate<-16.383000,0.000000,13.716000> }
cylinder{<0,0,0><0,0.035000,0>0.254000 translate<-16.383000,0.000000,20.447000>}
cylinder{<0,0,0><0,0.035000,0>0.254000 translate<-16.383000,0.000000,18.415000>}
box{<0,0,-0.254000><2.032000,0.035000,0.254000> rotate<0,-90.000000,0> translate<-16.383000,0.000000,18.415000> }
cylinder{<0,0,0><0,0.035000,0>0.254000 translate<-16.383000,0.000000,18.415000>}
cylinder{<0,0,0><0,0.035000,0>0.254000 translate<-15.493000,0.000000,19.305000>}
box{<0,0,-0.254000><1.258650,0.035000,0.254000> rotate<0,-44.997030,0> translate<-16.383000,0.000000,18.415000> }
cylinder{<0,0,0><0,0.035000,0>0.254000 translate<-16.383000,0.000000,20.447000>}
cylinder{<0,0,0><0,0.035000,0>0.254000 translate<-15.240000,0.000000,21.590000>}
box{<0,0,-0.254000><1.616446,0.035000,0.254000> rotate<0,-44.997030,0> translate<-16.383000,0.000000,20.447000> }
cylinder{<0,0,0><0,0.035000,0>0.254000 translate<-15.350000,0.000000,22.100000>}
cylinder{<0,0,0><0,0.035000,0>0.254000 translate<-15.240000,0.000000,21.590000>}
box{<0,0,-0.254000><0.521728,0.035000,0.254000> rotate<0,77.823405,0> translate<-15.350000,0.000000,22.100000> }
cylinder{<0,0,0><0,0.035000,0>0.254000 translate<-16.510000,-1.535000,5.545600>}
cylinder{<0,0,0><0,0.035000,0>0.254000 translate<-14.825200,-1.535000,3.860800>}
box{<0,0,-0.254000><2.382667,0.035000,0.254000> rotate<0,44.997030,0> translate<-16.510000,-1.535000,5.545600> }
cylinder{<0,0,0><0,0.035000,0>0.254000 translate<-14.825200,-1.535000,3.860800>}
cylinder{<0,0,0><0,0.035000,0>0.254000 translate<-14.749000,-1.535000,4.589000>}
box{<0,0,-0.254000><0.732176,0.035000,0.254000> rotate<0,-84.020674,0> translate<-14.825200,-1.535000,3.860800> }
cylinder{<0,0,0><0,0.035000,0>0.254000 translate<-14.605000,0.000000,8.255000>}
cylinder{<0,0,0><0,0.035000,0>0.254000 translate<-14.605000,0.000000,7.630000>}
box{<0,0,-0.254000><0.625000,0.035000,0.254000> rotate<0,-90.000000,0> translate<-14.605000,0.000000,7.630000> }
cylinder{<0,0,0><0,0.035000,0>0.254000 translate<-16.383000,0.000000,13.716000>}
cylinder{<0,0,0><0,0.035000,0>0.254000 translate<-14.224000,0.000000,11.557000>}
box{<0,0,-0.254000><3.053287,0.035000,0.254000> rotate<0,44.997030,0> translate<-16.383000,0.000000,13.716000> }
cylinder{<0,0,0><0,0.035000,0>0.254000 translate<-14.605000,0.000000,7.630000>}
cylinder{<0,0,0><0,0.035000,0>0.254000 translate<-13.975000,0.000000,7.000000>}
box{<0,0,-0.254000><0.890955,0.035000,0.254000> rotate<0,44.997030,0> translate<-14.605000,0.000000,7.630000> }
cylinder{<0,0,0><0,0.035000,0>0.254000 translate<-14.605000,0.000000,8.255000>}
cylinder{<0,0,0><0,0.035000,0>0.254000 translate<-13.970000,0.000000,8.890000>}
box{<0,0,-0.254000><0.898026,0.035000,0.254000> rotate<0,-44.997030,0> translate<-14.605000,0.000000,8.255000> }
cylinder{<0,0,0><0,0.035000,0>0.254000 translate<-14.224000,0.000000,11.557000>}
cylinder{<0,0,0><0,0.035000,0>0.254000 translate<-13.970000,0.000000,11.303000>}
box{<0,0,-0.254000><0.359210,0.035000,0.254000> rotate<0,44.997030,0> translate<-14.224000,0.000000,11.557000> }
cylinder{<0,0,0><0,0.035000,0>0.254000 translate<-13.970000,0.000000,8.890000>}
cylinder{<0,0,0><0,0.035000,0>0.254000 translate<-13.970000,0.000000,11.303000>}
box{<0,0,-0.254000><2.413000,0.035000,0.254000> rotate<0,90.000000,0> translate<-13.970000,0.000000,11.303000> }
cylinder{<0,0,0><0,0.035000,0>0.254000 translate<-13.970000,-1.535000,14.605000>}
cylinder{<0,0,0><0,0.035000,0>0.254000 translate<-13.970000,-1.535000,14.269000>}
box{<0,0,-0.254000><0.336000,0.035000,0.254000> rotate<0,-90.000000,0> translate<-13.970000,-1.535000,14.269000> }
cylinder{<0,0,0><0,0.035000,0>0.127000 translate<-14.000000,-1.535000,18.100000>}
cylinder{<0,0,0><0,0.035000,0>0.127000 translate<-13.900000,-1.535000,18.200000>}
box{<0,0,-0.127000><0.141421,0.035000,0.127000> rotate<0,-44.997030,0> translate<-14.000000,-1.535000,18.100000> }
cylinder{<0,0,0><0,0.035000,0>0.127000 translate<-13.900000,-1.535000,18.200000>}
cylinder{<0,0,0><0,0.035000,0>0.127000 translate<-13.900000,-1.535000,21.406000>}
box{<0,0,-0.127000><3.206000,0.035000,0.127000> rotate<0,90.000000,0> translate<-13.900000,-1.535000,21.406000> }
cylinder{<0,0,0><0,0.035000,0>0.304800 translate<-13.847400,-1.535000,8.902000>}
cylinder{<0,0,0><0,0.035000,0>0.304800 translate<-13.847400,-1.535000,11.129600>}
box{<0,0,-0.304800><2.227600,0.035000,0.304800> rotate<0,90.000000,0> translate<-13.847400,-1.535000,11.129600> }
cylinder{<0,0,0><0,0.035000,0>0.254000 translate<-16.510000,-1.535000,11.839400>}
cylinder{<0,0,0><0,0.035000,0>0.254000 translate<-13.847400,-1.535000,14.502000>}
box{<0,0,-0.254000><3.765485,0.035000,0.254000> rotate<0,-44.997030,0> translate<-16.510000,-1.535000,11.839400> }
cylinder{<0,0,0><0,0.035000,0>0.254000 translate<-13.970000,-1.535000,14.269000>}
cylinder{<0,0,0><0,0.035000,0>0.254000 translate<-13.847400,-1.535000,14.502000>}
box{<0,0,-0.254000><0.263286,0.035000,0.254000> rotate<0,-62.243433,0> translate<-13.970000,-1.535000,14.269000> }
cylinder{<0,0,0><0,0.035000,0>0.254000 translate<-13.847400,-1.535000,14.502000>}
cylinder{<0,0,0><0,0.035000,0>0.254000 translate<-13.847400,-1.535000,14.727600>}
box{<0,0,-0.254000><0.225600,0.035000,0.254000> rotate<0,90.000000,0> translate<-13.847400,-1.535000,14.727600> }
cylinder{<0,0,0><0,0.035000,0>0.304800 translate<-14.224000,-1.535000,11.557000>}
cylinder{<0,0,0><0,0.035000,0>0.304800 translate<-13.822000,-1.535000,11.155000>}
box{<0,0,-0.304800><0.568514,0.035000,0.304800> rotate<0,44.997030,0> translate<-14.224000,-1.535000,11.557000> }
cylinder{<0,0,0><0,0.035000,0>0.304800 translate<-13.847400,-1.535000,11.129600>}
cylinder{<0,0,0><0,0.035000,0>0.304800 translate<-13.822000,-1.535000,11.155000>}
box{<0,0,-0.304800><0.035921,0.035000,0.304800> rotate<0,-44.997030,0> translate<-13.847400,-1.535000,11.129600> }
cylinder{<0,0,0><0,0.035000,0>0.254000 translate<-14.997400,0.000000,16.770000>}
cylinder{<0,0,0><0,0.035000,0>0.254000 translate<-13.722400,0.000000,15.495000>}
box{<0,0,-0.254000><1.803122,0.035000,0.254000> rotate<0,44.997030,0> translate<-14.997400,0.000000,16.770000> }
cylinder{<0,0,0><0,0.035000,0>0.254000 translate<-13.847400,-1.535000,14.502000>}
cylinder{<0,0,0><0,0.035000,0>0.254000 translate<-13.688600,-1.535000,14.343200>}
box{<0,0,-0.254000><0.224577,0.035000,0.254000> rotate<0,44.997030,0> translate<-13.847400,-1.535000,14.502000> }
cylinder{<0,0,0><0,0.035000,0>0.127000 translate<-13.900000,-1.535000,21.406000>}
cylinder{<0,0,0><0,0.035000,0>0.127000 translate<-13.462000,-1.535000,21.844000>}
box{<0,0,-0.127000><0.619426,0.035000,0.127000> rotate<0,-44.997030,0> translate<-13.900000,-1.535000,21.406000> }
cylinder{<0,0,0><0,0.035000,0>0.127000 translate<-13.100000,0.000000,22.050000>}
cylinder{<0,0,0><0,0.035000,0>0.127000 translate<-13.100000,0.000000,21.200000>}
box{<0,0,-0.127000><0.850000,0.035000,0.127000> rotate<0,-90.000000,0> translate<-13.100000,0.000000,21.200000> }
cylinder{<0,0,0><0,0.035000,0>0.127000 translate<-13.450000,0.000000,22.100000>}
cylinder{<0,0,0><0,0.035000,0>0.127000 translate<-13.100000,0.000000,22.050000>}
box{<0,0,-0.127000><0.353553,0.035000,0.127000> rotate<0,8.129566,0> translate<-13.450000,0.000000,22.100000> }
cylinder{<0,0,0><0,0.035000,0>0.127000 translate<-12.400000,-1.535000,20.200000>}
cylinder{<0,0,0><0,0.035000,0>0.127000 translate<-12.400000,-1.535000,19.700000>}
box{<0,0,-0.127000><0.500000,0.035000,0.127000> rotate<0,-90.000000,0> translate<-12.400000,-1.535000,19.700000> }
cylinder{<0,0,0><0,0.035000,0>0.127000 translate<-12.400000,-1.535000,20.200000>}
cylinder{<0,0,0><0,0.035000,0>0.127000 translate<-12.300000,-1.535000,20.300000>}
box{<0,0,-0.127000><0.141421,0.035000,0.127000> rotate<0,-44.997030,0> translate<-12.400000,-1.535000,20.200000> }
cylinder{<0,0,0><0,0.035000,0>0.127000 translate<-13.100000,0.000000,21.200000>}
cylinder{<0,0,0><0,0.035000,0>0.127000 translate<-12.300000,0.000000,20.400000>}
box{<0,0,-0.127000><1.131371,0.035000,0.127000> rotate<0,44.997030,0> translate<-13.100000,0.000000,21.200000> }
cylinder{<0,0,0><0,0.035000,0>0.254000 translate<-14.000000,0.000000,18.100000>}
cylinder{<0,0,0><0,0.035000,0>0.254000 translate<-12.265000,0.000000,18.100000>}
box{<0,0,-0.254000><1.735000,0.035000,0.254000> rotate<0,0.000000,0> translate<-14.000000,0.000000,18.100000> }
cylinder{<0,0,0><0,0.035000,0>0.254000 translate<-13.722400,0.000000,15.495000>}
cylinder{<0,0,0><0,0.035000,0>0.254000 translate<-12.200000,0.000000,15.495000>}
box{<0,0,-0.254000><1.522400,0.035000,0.254000> rotate<0,0.000000,0> translate<-13.722400,0.000000,15.495000> }
cylinder{<0,0,0><0,0.035000,0>0.254000 translate<-12.265000,0.000000,18.100000>}
cylinder{<0,0,0><0,0.035000,0>0.254000 translate<-12.200000,0.000000,18.035000>}
box{<0,0,-0.254000><0.091924,0.035000,0.254000> rotate<0,44.997030,0> translate<-12.265000,0.000000,18.100000> }
cylinder{<0,0,0><0,0.035000,0>0.254000 translate<-15.493000,0.000000,19.305000>}
cylinder{<0,0,0><0,0.035000,0>0.254000 translate<-12.200000,0.000000,19.305000>}
box{<0,0,-0.254000><3.293000,0.035000,0.254000> rotate<0,0.000000,0> translate<-15.493000,0.000000,19.305000> }
cylinder{<0,0,0><0,0.035000,0>0.127000 translate<-12.300000,0.000000,20.300000>}
cylinder{<0,0,0><0,0.035000,0>0.127000 translate<-12.200000,0.000000,20.400000>}
box{<0,0,-0.127000><0.141421,0.035000,0.127000> rotate<0,-44.997030,0> translate<-12.300000,0.000000,20.300000> }
cylinder{<0,0,0><0,0.035000,0>0.127000 translate<-12.300000,0.000000,20.400000>}
cylinder{<0,0,0><0,0.035000,0>0.127000 translate<-12.200000,0.000000,20.400000>}
box{<0,0,-0.127000><0.100000,0.035000,0.127000> rotate<0,0.000000,0> translate<-12.300000,0.000000,20.400000> }
cylinder{<0,0,0><0,0.035000,0>0.127000 translate<-12.200000,-1.535000,26.300000>}
cylinder{<0,0,0><0,0.035000,0>0.127000 translate<-12.200000,-1.535000,27.000000>}
box{<0,0,-0.127000><0.700000,0.035000,0.127000> rotate<0,90.000000,0> translate<-12.200000,-1.535000,27.000000> }
cylinder{<0,0,0><0,0.035000,0>0.127000 translate<-12.200000,-1.535000,27.000000>}
cylinder{<0,0,0><0,0.035000,0>0.127000 translate<-12.140000,-1.535000,27.060000>}
box{<0,0,-0.127000><0.084853,0.035000,0.127000> rotate<0,-44.997030,0> translate<-12.200000,-1.535000,27.000000> }
cylinder{<0,0,0><0,0.035000,0>0.127000 translate<-13.462000,-1.535000,21.844000>}
cylinder{<0,0,0><0,0.035000,0>0.127000 translate<-11.906000,-1.535000,21.844000>}
box{<0,0,-0.127000><1.556000,0.035000,0.127000> rotate<0,0.000000,0> translate<-13.462000,-1.535000,21.844000> }
cylinder{<0,0,0><0,0.035000,0>0.254000 translate<-13.847400,-1.535000,14.727600>}
cylinder{<0,0,0><0,0.035000,0>0.254000 translate<-11.775000,-1.535000,16.800000>}
box{<0,0,-0.254000><2.930816,0.035000,0.254000> rotate<0,-44.997030,0> translate<-13.847400,-1.535000,14.727600> }
cylinder{<0,0,0><0,0.035000,0>0.254000 translate<-11.775000,-1.535000,16.800000>}
cylinder{<0,0,0><0,0.035000,0>0.254000 translate<-11.775000,-1.535000,16.825400>}
box{<0,0,-0.254000><0.025400,0.035000,0.254000> rotate<0,90.000000,0> translate<-11.775000,-1.535000,16.825400> }
cylinder{<0,0,0><0,0.035000,0>0.127000 translate<-11.906000,-1.535000,21.844000>}
cylinder{<0,0,0><0,0.035000,0>0.127000 translate<-11.750000,-1.535000,22.000000>}
box{<0,0,-0.127000><0.220617,0.035000,0.127000> rotate<0,-44.997030,0> translate<-11.906000,-1.535000,21.844000> }
cylinder{<0,0,0><0,0.035000,0>0.127000 translate<-11.750000,-1.535000,22.000000>}
cylinder{<0,0,0><0,0.035000,0>0.127000 translate<-11.700000,-1.535000,22.050000>}
box{<0,0,-0.127000><0.070711,0.035000,0.127000> rotate<0,-44.997030,0> translate<-11.750000,-1.535000,22.000000> }
cylinder{<0,0,0><0,0.035000,0>0.127000 translate<-11.700000,-1.535000,23.400000>}
cylinder{<0,0,0><0,0.035000,0>0.127000 translate<-11.700000,-1.535000,22.050000>}
box{<0,0,-0.127000><1.350000,0.035000,0.127000> rotate<0,-90.000000,0> translate<-11.700000,-1.535000,22.050000> }
cylinder{<0,0,0><0,0.035000,0>0.254000 translate<-11.700000,0.000000,23.400000>}
cylinder{<0,0,0><0,0.035000,0>0.254000 translate<-11.600000,0.000000,23.500000>}
box{<0,0,-0.254000><0.141421,0.035000,0.254000> rotate<0,-44.997030,0> translate<-11.700000,0.000000,23.400000> }
cylinder{<0,0,0><0,0.035000,0>0.127000 translate<-12.200000,0.000000,15.495000>}
cylinder{<0,0,0><0,0.035000,0>0.127000 translate<-11.431294,0.000000,15.498844>}
box{<0,0,-0.127000><0.768716,0.035000,0.127000> rotate<0,-0.286474,0> translate<-12.200000,0.000000,15.495000> }
cylinder{<0,0,0><0,0.035000,0>0.127000 translate<-12.400000,-1.535000,19.700000>}
cylinder{<0,0,0><0,0.035000,0>0.127000 translate<-11.400000,-1.535000,18.700000>}
box{<0,0,-0.127000><1.414214,0.035000,0.127000> rotate<0,44.997030,0> translate<-12.400000,-1.535000,19.700000> }
cylinder{<0,0,0><0,0.035000,0>0.254000 translate<-11.775000,-1.535000,16.825400>}
cylinder{<0,0,0><0,0.035000,0>0.254000 translate<-11.353800,-1.535000,17.246600>}
box{<0,0,-0.254000><0.595667,0.035000,0.254000> rotate<0,-44.997030,0> translate<-11.775000,-1.535000,16.825400> }
cylinder{<0,0,0><0,0.035000,0>0.127000 translate<-11.431294,0.000000,15.498844>}
cylinder{<0,0,0><0,0.035000,0>0.127000 translate<-11.200000,0.000000,15.500000>}
box{<0,0,-0.127000><0.231297,0.035000,0.127000> rotate<0,-0.286403,0> translate<-11.431294,0.000000,15.498844> }
cylinder{<0,0,0><0,0.035000,0>0.127000 translate<-12.200000,0.000000,16.765000>}
cylinder{<0,0,0><0,0.035000,0>0.127000 translate<-11.200000,0.000000,16.800000>}
box{<0,0,-0.127000><1.000612,0.035000,0.127000> rotate<0,-2.004402,0> translate<-12.200000,0.000000,16.765000> }
cylinder{<0,0,0><0,0.035000,0>0.254000 translate<-12.200000,0.000000,19.305000>}
cylinder{<0,0,0><0,0.035000,0>0.254000 translate<-11.200000,0.000000,19.300000>}
box{<0,0,-0.254000><1.000012,0.035000,0.254000> rotate<0,0.286458,0> translate<-12.200000,0.000000,19.305000> }
cylinder{<0,0,0><0,0.035000,0>0.254000 translate<-10.450000,0.000000,22.200000>}
cylinder{<0,0,0><0,0.035000,0>0.254000 translate<-10.450000,0.000000,21.650000>}
box{<0,0,-0.254000><0.550000,0.035000,0.254000> rotate<0,-90.000000,0> translate<-10.450000,0.000000,21.650000> }
cylinder{<0,0,0><0,0.035000,0>0.127000 translate<-11.200000,0.000000,16.800000>}
cylinder{<0,0,0><0,0.035000,0>0.127000 translate<-10.014200,0.000000,16.800000>}
box{<0,0,-0.127000><1.185800,0.035000,0.127000> rotate<0,0.000000,0> translate<-11.200000,0.000000,16.800000> }
cylinder{<0,0,0><0,0.035000,0>0.254000 translate<-10.800000,-1.535000,19.800000>}
cylinder{<0,0,0><0,0.035000,0>0.254000 translate<-10.000000,-1.535000,20.600000>}
box{<0,0,-0.254000><1.131371,0.035000,0.254000> rotate<0,-44.997030,0> translate<-10.800000,-1.535000,19.800000> }
cylinder{<0,0,0><0,0.035000,0>0.127000 translate<-9.850000,-1.535000,22.000000>}
cylinder{<0,0,0><0,0.035000,0>0.127000 translate<-9.850000,-1.535000,22.408000>}
box{<0,0,-0.127000><0.408000,0.035000,0.127000> rotate<0,90.000000,0> translate<-9.850000,-1.535000,22.408000> }
cylinder{<0,0,0><0,0.035000,0>0.127000 translate<-11.400000,-1.535000,18.700000>}
cylinder{<0,0,0><0,0.035000,0>0.127000 translate<-9.800000,-1.535000,18.700000>}
box{<0,0,-0.127000><1.600000,0.035000,0.127000> rotate<0,0.000000,0> translate<-11.400000,-1.535000,18.700000> }
cylinder{<0,0,0><0,0.035000,0>0.127000 translate<-9.850000,-1.535000,22.000000>}
cylinder{<0,0,0><0,0.035000,0>0.127000 translate<-9.800000,-1.535000,23.500000>}
box{<0,0,-0.127000><1.500833,0.035000,0.127000> rotate<0,-88.085034,0> translate<-9.850000,-1.535000,22.000000> }
cylinder{<0,0,0><0,0.035000,0>0.127000 translate<-12.200000,-1.535000,26.300000>}
cylinder{<0,0,0><0,0.035000,0>0.127000 translate<-9.800000,-1.535000,23.900000>}
box{<0,0,-0.127000><3.394113,0.035000,0.127000> rotate<0,44.997030,0> translate<-12.200000,-1.535000,26.300000> }
cylinder{<0,0,0><0,0.035000,0>0.127000 translate<-9.800000,-1.535000,23.500000>}
cylinder{<0,0,0><0,0.035000,0>0.127000 translate<-9.800000,-1.535000,23.900000>}
box{<0,0,-0.127000><0.400000,0.035000,0.127000> rotate<0,90.000000,0> translate<-9.800000,-1.535000,23.900000> }
cylinder{<0,0,0><0,0.035000,0>0.254000 translate<-11.200000,0.000000,19.300000>}
cylinder{<0,0,0><0,0.035000,0>0.254000 translate<-9.700000,0.000000,19.300000>}
box{<0,0,-0.254000><1.500000,0.035000,0.254000> rotate<0,0.000000,0> translate<-11.200000,0.000000,19.300000> }
cylinder{<0,0,0><0,0.035000,0>0.254000 translate<-9.600000,0.000000,19.200000>}
cylinder{<0,0,0><0,0.035000,0>0.254000 translate<-9.600000,0.000000,18.000000>}
box{<0,0,-0.254000><1.200000,0.035000,0.254000> rotate<0,-90.000000,0> translate<-9.600000,0.000000,18.000000> }
cylinder{<0,0,0><0,0.035000,0>0.254000 translate<-9.700000,0.000000,19.300000>}
cylinder{<0,0,0><0,0.035000,0>0.254000 translate<-9.600000,0.000000,19.200000>}
box{<0,0,-0.254000><0.141421,0.035000,0.254000> rotate<0,44.997030,0> translate<-9.700000,0.000000,19.300000> }
cylinder{<0,0,0><0,0.035000,0>0.635000 translate<-9.600000,0.000000,27.060000>}
cylinder{<0,0,0><0,0.035000,0>0.635000 translate<-9.600000,0.000000,26.300000>}
box{<0,0,-0.635000><0.760000,0.035000,0.635000> rotate<0,-90.000000,0> translate<-9.600000,0.000000,26.300000> }
cylinder{<0,0,0><0,0.035000,0>0.254000 translate<-11.600000,0.000000,23.500000>}
cylinder{<0,0,0><0,0.035000,0>0.254000 translate<-9.500000,0.000000,23.500000>}
box{<0,0,-0.254000><2.100000,0.035000,0.254000> rotate<0,0.000000,0> translate<-11.600000,0.000000,23.500000> }
cylinder{<0,0,0><0,0.035000,0>0.254000 translate<-10.450000,0.000000,21.650000>}
cylinder{<0,0,0><0,0.035000,0>0.254000 translate<-9.400000,0.000000,20.600000>}
box{<0,0,-0.254000><1.484924,0.035000,0.254000> rotate<0,44.997030,0> translate<-10.450000,0.000000,21.650000> }
cylinder{<0,0,0><0,0.035000,0>0.254000 translate<-10.000000,-1.535000,20.600000>}
cylinder{<0,0,0><0,0.035000,0>0.254000 translate<-9.400000,-1.535000,20.600000>}
box{<0,0,-0.254000><0.600000,0.035000,0.254000> rotate<0,0.000000,0> translate<-10.000000,-1.535000,20.600000> }
cylinder{<0,0,0><0,0.035000,0>0.127000 translate<-10.014200,0.000000,16.800000>}
cylinder{<0,0,0><0,0.035000,0>0.127000 translate<-9.325400,0.000000,16.111200>}
box{<0,0,-0.127000><0.974110,0.035000,0.127000> rotate<0,44.997030,0> translate<-10.014200,0.000000,16.800000> }
cylinder{<0,0,0><0,0.035000,0>0.254000 translate<-11.353800,-1.535000,17.246600>}
cylinder{<0,0,0><0,0.035000,0>0.254000 translate<-9.181397,-1.535000,17.246600>}
box{<0,0,-0.254000><2.172403,0.035000,0.254000> rotate<0,0.000000,0> translate<-11.353800,-1.535000,17.246600> }
cylinder{<0,0,0><0,0.035000,0>0.254000 translate<-9.500000,0.000000,23.500000>}
cylinder{<0,0,0><0,0.035000,0>0.254000 translate<-9.100000,0.000000,23.100000>}
box{<0,0,-0.254000><0.565685,0.035000,0.254000> rotate<0,44.997030,0> translate<-9.500000,0.000000,23.500000> }
cylinder{<0,0,0><0,0.035000,0>0.254000 translate<-9.100000,0.000000,23.100000>}
cylinder{<0,0,0><0,0.035000,0>0.254000 translate<-8.800000,0.000000,23.100000>}
box{<0,0,-0.254000><0.300000,0.035000,0.254000> rotate<0,0.000000,0> translate<-9.100000,0.000000,23.100000> }
cylinder{<0,0,0><0,0.035000,0>0.127000 translate<-9.850000,-1.535000,22.408000>}
cylinder{<0,0,0><0,0.035000,0>0.127000 translate<-8.636000,-1.535000,23.622000>}
box{<0,0,-0.127000><1.716855,0.035000,0.127000> rotate<0,-44.997030,0> translate<-9.850000,-1.535000,22.408000> }
cylinder{<0,0,0><0,0.035000,0>0.254000 translate<-8.800000,0.000000,23.100000>}
cylinder{<0,0,0><0,0.035000,0>0.254000 translate<-8.550000,0.000000,22.850000>}
box{<0,0,-0.254000><0.353553,0.035000,0.254000> rotate<0,44.997030,0> translate<-8.800000,0.000000,23.100000> }
cylinder{<0,0,0><0,0.035000,0>0.254000 translate<-8.550000,0.000000,22.200000>}
cylinder{<0,0,0><0,0.035000,0>0.254000 translate<-8.550000,0.000000,22.850000>}
box{<0,0,-0.254000><0.650000,0.035000,0.254000> rotate<0,90.000000,0> translate<-8.550000,0.000000,22.850000> }
cylinder{<0,0,0><0,0.035000,0>0.254000 translate<-9.600000,0.000000,18.000000>}
cylinder{<0,0,0><0,0.035000,0>0.254000 translate<-8.200000,0.000000,18.000000>}
box{<0,0,-0.254000><1.400000,0.035000,0.254000> rotate<0,0.000000,0> translate<-9.600000,0.000000,18.000000> }
cylinder{<0,0,0><0,0.035000,0>0.254000 translate<-8.200000,0.000000,18.000000>}
cylinder{<0,0,0><0,0.035000,0>0.254000 translate<-8.100000,0.000000,18.100000>}
box{<0,0,-0.254000><0.141421,0.035000,0.254000> rotate<0,-44.997030,0> translate<-8.200000,0.000000,18.000000> }
cylinder{<0,0,0><0,0.035000,0>0.254000 translate<-8.100000,0.000000,18.100000>}
cylinder{<0,0,0><0,0.035000,0>0.254000 translate<-8.000000,0.000000,18.100000>}
box{<0,0,-0.254000><0.100000,0.035000,0.254000> rotate<0,0.000000,0> translate<-8.100000,0.000000,18.100000> }
cylinder{<0,0,0><0,0.035000,0>0.127000 translate<-8.000000,0.000000,20.500000>}
cylinder{<0,0,0><0,0.035000,0>0.127000 translate<-8.000000,0.000000,19.600000>}
box{<0,0,-0.127000><0.900000,0.035000,0.127000> rotate<0,-90.000000,0> translate<-8.000000,0.000000,19.600000> }
cylinder{<0,0,0><0,0.035000,0>0.127000 translate<-9.800000,-1.535000,18.700000>}
cylinder{<0,0,0><0,0.035000,0>0.127000 translate<-8.000000,-1.535000,20.500000>}
box{<0,0,-0.127000><2.545584,0.035000,0.127000> rotate<0,-44.997030,0> translate<-9.800000,-1.535000,18.700000> }
cylinder{<0,0,0><0,0.035000,0>0.635000 translate<-9.600000,0.000000,26.300000>}
cylinder{<0,0,0><0,0.035000,0>0.635000 translate<-7.700000,0.000000,24.400000>}
box{<0,0,-0.635000><2.687006,0.035000,0.635000> rotate<0,44.997030,0> translate<-9.600000,0.000000,26.300000> }
cylinder{<0,0,0><0,0.035000,0>0.254000 translate<-8.550000,0.000000,22.200000>}
cylinder{<0,0,0><0,0.035000,0>0.254000 translate<-7.188800,0.000000,22.200000>}
box{<0,0,-0.254000><1.361200,0.035000,0.254000> rotate<0,0.000000,0> translate<-8.550000,0.000000,22.200000> }
cylinder{<0,0,0><0,0.035000,0>0.127000 translate<-8.636000,-1.535000,23.622000>}
cylinder{<0,0,0><0,0.035000,0>0.127000 translate<-7.078000,-1.535000,23.622000>}
box{<0,0,-0.127000><1.558000,0.035000,0.127000> rotate<0,0.000000,0> translate<-8.636000,-1.535000,23.622000> }
cylinder{<0,0,0><0,0.035000,0>0.254000 translate<-8.000000,0.000000,18.100000>}
cylinder{<0,0,0><0,0.035000,0>0.254000 translate<-7.000000,0.000000,18.035000>}
box{<0,0,-0.254000><1.002110,0.035000,0.254000> rotate<0,3.718749,0> translate<-8.000000,0.000000,18.100000> }
cylinder{<0,0,0><0,0.035000,0>0.127000 translate<-8.000000,0.000000,19.600000>}
cylinder{<0,0,0><0,0.035000,0>0.127000 translate<-7.000000,0.000000,19.305000>}
box{<0,0,-0.127000><1.042605,0.035000,0.127000> rotate<0,16.434975,0> translate<-8.000000,0.000000,19.600000> }
cylinder{<0,0,0><0,0.035000,0>0.254000 translate<-7.188800,0.000000,22.200000>}
cylinder{<0,0,0><0,0.035000,0>0.254000 translate<-6.771400,0.000000,21.782600>}
box{<0,0,-0.254000><0.590293,0.035000,0.254000> rotate<0,44.997030,0> translate<-7.188800,0.000000,22.200000> }
cylinder{<0,0,0><0,0.035000,0>0.254000 translate<-6.771400,-1.535000,21.782600>}
cylinder{<0,0,0><0,0.035000,0>0.254000 translate<-6.767000,-1.535000,21.782600>}
box{<0,0,-0.254000><0.004400,0.035000,0.254000> rotate<0,0.000000,0> translate<-6.771400,-1.535000,21.782600> }
cylinder{<0,0,0><0,0.035000,0>0.127000 translate<-7.000000,0.000000,15.495000>}
cylinder{<0,0,0><0,0.035000,0>0.127000 translate<-6.500000,0.000000,14.995000>}
box{<0,0,-0.127000><0.707107,0.035000,0.127000> rotate<0,44.997030,0> translate<-7.000000,0.000000,15.495000> }
cylinder{<0,0,0><0,0.035000,0>0.127000 translate<-6.500000,0.000000,3.800000>}
cylinder{<0,0,0><0,0.035000,0>0.127000 translate<-6.500000,0.000000,14.995000>}
box{<0,0,-0.127000><11.195000,0.035000,0.127000> rotate<0,90.000000,0> translate<-6.500000,0.000000,14.995000> }
cylinder{<0,0,0><0,0.035000,0>0.127000 translate<-9.325400,-1.535000,16.111200>}
cylinder{<0,0,0><0,0.035000,0>0.127000 translate<-6.226600,-1.535000,19.210000>}
box{<0,0,-0.127000><4.382365,0.035000,0.127000> rotate<0,-44.997030,0> translate<-9.325400,-1.535000,16.111200> }
cylinder{<0,0,0><0,0.035000,0>0.254000 translate<-9.181397,-1.535000,17.246600>}
cylinder{<0,0,0><0,0.035000,0>0.254000 translate<-6.225556,-1.535000,20.202441>}
box{<0,0,-0.254000><4.180190,0.035000,0.254000> rotate<0,-44.997030,0> translate<-9.181397,-1.535000,17.246600> }
cylinder{<0,0,0><0,0.035000,0>0.635000 translate<-7.700000,0.000000,24.400000>}
cylinder{<0,0,0><0,0.035000,0>0.635000 translate<-6.132000,0.000000,24.400000>}
box{<0,0,-0.635000><1.568000,0.035000,0.635000> rotate<0,0.000000,0> translate<-7.700000,0.000000,24.400000> }
cylinder{<0,0,0><0,0.035000,0>0.127000 translate<-6.500000,0.000000,3.800000>}
cylinder{<0,0,0><0,0.035000,0>0.127000 translate<-6.100000,0.000000,3.400000>}
box{<0,0,-0.127000><0.565685,0.035000,0.127000> rotate<0,44.997030,0> translate<-6.500000,0.000000,3.800000> }
cylinder{<0,0,0><0,0.035000,0>0.254000 translate<-13.688600,-1.535000,14.343200>}
cylinder{<0,0,0><0,0.035000,0>0.254000 translate<-6.055400,-1.535000,14.317800>}
box{<0,0,-0.254000><7.633242,0.035000,0.254000> rotate<0,0.190642,0> translate<-13.688600,-1.535000,14.343200> }
cylinder{<0,0,0><0,0.035000,0>0.254000 translate<-6.055400,-1.535000,14.317800>}
cylinder{<0,0,0><0,0.035000,0>0.254000 translate<-6.049000,-1.535000,14.121000>}
box{<0,0,-0.254000><0.196904,0.035000,0.254000> rotate<0,88.131562,0> translate<-6.055400,-1.535000,14.317800> }
cylinder{<0,0,0><0,0.035000,0>0.254000 translate<-6.055400,-1.535000,14.317800>}
cylinder{<0,0,0><0,0.035000,0>0.254000 translate<-5.892800,-1.535000,14.206000>}
box{<0,0,-0.254000><0.197327,0.035000,0.254000> rotate<0,34.509236,0> translate<-6.055400,-1.535000,14.317800> }
cylinder{<0,0,0><0,0.035000,0>0.254000 translate<-5.892800,-1.535000,13.131800>}
cylinder{<0,0,0><0,0.035000,0>0.254000 translate<-5.892800,-1.535000,14.206000>}
box{<0,0,-0.254000><1.074200,0.035000,0.254000> rotate<0,90.000000,0> translate<-5.892800,-1.535000,14.206000> }
cylinder{<0,0,0><0,0.035000,0>0.127000 translate<-7.078000,-1.535000,23.622000>}
cylinder{<0,0,0><0,0.035000,0>0.127000 translate<-5.884200,-1.535000,24.815800>}
box{<0,0,-0.127000><1.688288,0.035000,0.127000> rotate<0,-44.997030,0> translate<-7.078000,-1.535000,23.622000> }
cylinder{<0,0,0><0,0.035000,0>0.254000 translate<-5.892800,-1.535000,13.131800>}
cylinder{<0,0,0><0,0.035000,0>0.254000 translate<-5.740400,-1.535000,12.979400>}
box{<0,0,-0.254000><0.215526,0.035000,0.254000> rotate<0,44.997030,0> translate<-5.892800,-1.535000,13.131800> }
cylinder{<0,0,0><0,0.035000,0>0.254000 translate<-7.425200,-1.535000,3.860800>}
cylinder{<0,0,0><0,0.035000,0>0.254000 translate<-5.571000,-1.535000,5.715000>}
box{<0,0,-0.254000><2.622235,0.035000,0.254000> rotate<0,-44.997030,0> translate<-7.425200,-1.535000,3.860800> }
cylinder{<0,0,0><0,0.035000,0>0.254000 translate<-6.767000,-1.535000,21.782600>}
cylinder{<0,0,0><0,0.035000,0>0.254000 translate<-5.511800,-1.535000,23.037800>}
box{<0,0,-0.254000><1.775121,0.035000,0.254000> rotate<0,-44.997030,0> translate<-6.767000,-1.535000,21.782600> }
cylinder{<0,0,0><0,0.035000,0>0.254000 translate<-7.000000,0.000000,16.765000>}
cylinder{<0,0,0><0,0.035000,0>0.254000 translate<-5.460000,0.000000,16.765000>}
box{<0,0,-0.254000><1.540000,0.035000,0.254000> rotate<0,0.000000,0> translate<-7.000000,0.000000,16.765000> }
cylinder{<0,0,0><0,0.035000,0>0.304800 translate<-5.130800,-1.535000,17.805400>}
cylinder{<0,0,0><0,0.035000,0>0.304800 translate<-5.105400,-1.535000,17.780000>}
box{<0,0,-0.304800><0.035921,0.035000,0.304800> rotate<0,44.997030,0> translate<-5.130800,-1.535000,17.805400> }
cylinder{<0,0,0><0,0.035000,0>0.304800 translate<-5.105400,-1.535000,16.517800>}
cylinder{<0,0,0><0,0.035000,0>0.304800 translate<-5.105400,-1.535000,17.780000>}
box{<0,0,-0.304800><1.262200,0.035000,0.304800> rotate<0,90.000000,0> translate<-5.105400,-1.535000,17.780000> }
cylinder{<0,0,0><0,0.035000,0>0.254000 translate<-5.460000,0.000000,16.765000>}
cylinder{<0,0,0><0,0.035000,0>0.254000 translate<-5.080000,0.000000,17.145000>}
box{<0,0,-0.254000><0.537401,0.035000,0.254000> rotate<0,-44.997030,0> translate<-5.460000,0.000000,16.765000> }
cylinder{<0,0,0><0,0.035000,0>0.254000 translate<-5.130800,0.000000,17.805400>}
cylinder{<0,0,0><0,0.035000,0>0.254000 translate<-5.080000,0.000000,17.145000>}
box{<0,0,-0.254000><0.662351,0.035000,0.254000> rotate<0,85.595645,0> translate<-5.130800,0.000000,17.805400> }
cylinder{<0,0,0><0,0.035000,0>0.254000 translate<-5.511800,-1.535000,23.037800>}
cylinder{<0,0,0><0,0.035000,0>0.254000 translate<-4.634600,-1.535000,23.037800>}
box{<0,0,-0.254000><0.877200,0.035000,0.254000> rotate<0,0.000000,0> translate<-5.511800,-1.535000,23.037800> }
cylinder{<0,0,0><0,0.035000,0>0.254000 translate<-4.634600,-1.535000,23.037800>}
cylinder{<0,0,0><0,0.035000,0>0.254000 translate<-4.400000,-1.535000,22.803200>}
box{<0,0,-0.254000><0.331775,0.035000,0.254000> rotate<0,44.997030,0> translate<-4.634600,-1.535000,23.037800> }
cylinder{<0,0,0><0,0.035000,0>0.127000 translate<-4.155400,-1.535000,14.317800>}
cylinder{<0,0,0><0,0.035000,0>0.127000 translate<-4.130000,-1.535000,14.600000>}
box{<0,0,-0.127000><0.283341,0.035000,0.127000> rotate<0,-84.851232,0> translate<-4.155400,-1.535000,14.317800> }
cylinder{<0,0,0><0,0.035000,0>0.635000 translate<-3.810000,0.000000,18.542000>}
cylinder{<0,0,0><0,0.035000,0>0.635000 translate<-3.810000,0.000000,6.604000>}
box{<0,0,-0.635000><11.938000,0.035000,0.635000> rotate<0,-90.000000,0> translate<-3.810000,0.000000,6.604000> }
cylinder{<0,0,0><0,0.035000,0>0.635000 translate<-3.810000,0.000000,6.604000>}
cylinder{<0,0,0><0,0.035000,0>0.635000 translate<-3.302000,0.000000,6.096000>}
box{<0,0,-0.635000><0.718420,0.035000,0.635000> rotate<0,44.997030,0> translate<-3.810000,0.000000,6.604000> }
cylinder{<0,0,0><0,0.035000,0>0.635000 translate<-4.025000,0.000000,7.000000>}
cylinder{<0,0,0><0,0.035000,0>0.635000 translate<-3.302000,0.000000,6.277000>}
box{<0,0,-0.635000><1.022476,0.035000,0.635000> rotate<0,44.997030,0> translate<-4.025000,0.000000,7.000000> }
cylinder{<0,0,0><0,0.035000,0>0.635000 translate<-3.302000,0.000000,6.096000>}
cylinder{<0,0,0><0,0.035000,0>0.635000 translate<-3.302000,0.000000,6.277000>}
box{<0,0,-0.635000><0.181000,0.035000,0.635000> rotate<0,90.000000,0> translate<-3.302000,0.000000,6.277000> }
cylinder{<0,0,0><0,0.035000,0>0.254000 translate<-3.302000,0.000000,6.277000>}
cylinder{<0,0,0><0,0.035000,0>0.254000 translate<-2.906000,0.000000,6.673000>}
box{<0,0,-0.254000><0.560029,0.035000,0.254000> rotate<0,-44.997030,0> translate<-3.302000,0.000000,6.277000> }
cylinder{<0,0,0><0,0.035000,0>0.254000 translate<-5.740400,-1.535000,12.979400>}
cylinder{<0,0,0><0,0.035000,0>0.254000 translate<-2.616200,-1.535000,12.979400>}
box{<0,0,-0.254000><3.124200,0.035000,0.254000> rotate<0,0.000000,0> translate<-5.740400,-1.535000,12.979400> }
cylinder{<0,0,0><0,0.035000,0>0.635000 translate<-3.810000,0.000000,18.542000>}
cylinder{<0,0,0><0,0.035000,0>0.635000 translate<-2.300000,0.000000,20.052000>}
box{<0,0,-0.635000><2.135462,0.035000,0.635000> rotate<0,-44.997030,0> translate<-3.810000,0.000000,18.542000> }
cylinder{<0,0,0><0,0.035000,0>0.635000 translate<-2.300000,0.000000,20.568000>}
cylinder{<0,0,0><0,0.035000,0>0.635000 translate<-2.300000,0.000000,20.052000>}
box{<0,0,-0.635000><0.516000,0.035000,0.635000> rotate<0,-90.000000,0> translate<-2.300000,0.000000,20.052000> }
cylinder{<0,0,0><0,0.035000,0>0.635000 translate<-6.132000,0.000000,24.400000>}
cylinder{<0,0,0><0,0.035000,0>0.635000 translate<-2.300000,0.000000,20.568000>}
box{<0,0,-0.635000><5.419266,0.035000,0.635000> rotate<0,44.997030,0> translate<-6.132000,0.000000,24.400000> }
cylinder{<0,0,0><0,0.035000,0>0.254000 translate<-2.616200,-1.535000,12.979400>}
cylinder{<0,0,0><0,0.035000,0>0.254000 translate<-1.625600,-1.535000,13.970000>}
box{<0,0,-0.254000><1.400920,0.035000,0.254000> rotate<0,-44.997030,0> translate<-2.616200,-1.535000,12.979400> }
cylinder{<0,0,0><0,0.035000,0>0.254000 translate<-1.625600,-1.535000,13.970000>}
cylinder{<0,0,0><0,0.035000,0>0.254000 translate<-1.585000,-1.535000,13.970000>}
box{<0,0,-0.254000><0.040600,0.035000,0.254000> rotate<0,0.000000,0> translate<-1.625600,-1.535000,13.970000> }
cylinder{<0,0,0><0,0.035000,0>0.127000 translate<-4.130000,-1.535000,14.600000>}
cylinder{<0,0,0><0,0.035000,0>0.127000 translate<-1.585000,-1.535000,17.145000>}
box{<0,0,-0.127000><3.599174,0.035000,0.127000> rotate<0,-44.997030,0> translate<-4.130000,-1.535000,14.600000> }
cylinder{<0,0,0><0,0.035000,0>0.127000 translate<-9.017000,-1.535000,11.455400>}
cylinder{<0,0,0><0,0.035000,0>0.127000 translate<-1.549400,-1.535000,11.455400>}
box{<0,0,-0.127000><7.467600,0.035000,0.127000> rotate<0,0.000000,0> translate<-9.017000,-1.535000,11.455400> }
cylinder{<0,0,0><0,0.035000,0>0.127000 translate<-5.884200,-1.535000,24.815800>}
cylinder{<0,0,0><0,0.035000,0>0.127000 translate<-1.168400,-1.535000,24.815800>}
box{<0,0,-0.127000><4.715800,0.035000,0.127000> rotate<0,0.000000,0> translate<-5.884200,-1.535000,24.815800> }
cylinder{<0,0,0><0,0.035000,0>0.254000 translate<-1.221400,0.000000,16.203000>}
cylinder{<0,0,0><0,0.035000,0>0.254000 translate<-0.990600,0.000000,16.433800>}
box{<0,0,-0.254000><0.326400,0.035000,0.254000> rotate<0,-44.997030,0> translate<-1.221400,0.000000,16.203000> }
cylinder{<0,0,0><0,0.035000,0>0.254000 translate<-0.990600,0.000000,17.170400>}
cylinder{<0,0,0><0,0.035000,0>0.254000 translate<-0.990600,0.000000,16.433800>}
box{<0,0,-0.254000><0.736600,0.035000,0.254000> rotate<0,-90.000000,0> translate<-0.990600,0.000000,16.433800> }
cylinder{<0,0,0><0,0.035000,0>0.127000 translate<-6.100000,0.000000,3.400000>}
cylinder{<0,0,0><0,0.035000,0>0.127000 translate<-0.400000,0.000000,3.400000>}
box{<0,0,-0.127000><5.700000,0.035000,0.127000> rotate<0,0.000000,0> translate<-6.100000,0.000000,3.400000> }
cylinder{<0,0,0><0,0.035000,0>0.127000 translate<-1.549400,-1.535000,11.455400>}
cylinder{<0,0,0><0,0.035000,0>0.127000 translate<-0.330200,-1.535000,12.674600>}
box{<0,0,-0.127000><1.724209,0.035000,0.127000> rotate<0,-44.997030,0> translate<-1.549400,-1.535000,11.455400> }
cylinder{<0,0,0><0,0.035000,0>0.127000 translate<-0.800000,-1.535000,23.700000>}
cylinder{<0,0,0><0,0.035000,0>0.127000 translate<0.000000,-1.535000,22.803200>}
box{<0,0,-0.127000><1.201770,0.035000,0.127000> rotate<0,48.261917,0> translate<-0.800000,-1.535000,23.700000> }
cylinder{<0,0,0><0,0.035000,0>0.127000 translate<0.000000,-1.535000,23.647400>}
cylinder{<0,0,0><0,0.035000,0>0.127000 translate<0.000000,-1.535000,22.803200>}
box{<0,0,-0.127000><0.844200,0.035000,0.127000> rotate<0,-90.000000,0> translate<0.000000,-1.535000,22.803200> }
cylinder{<0,0,0><0,0.035000,0>0.127000 translate<0.000000,-1.535000,25.780000>}
cylinder{<0,0,0><0,0.035000,0>0.127000 translate<0.000000,-1.535000,22.803200>}
box{<0,0,-0.127000><2.976800,0.035000,0.127000> rotate<0,-90.000000,0> translate<0.000000,-1.535000,22.803200> }
cylinder{<0,0,0><0,0.035000,0>0.127000 translate<-1.168400,-1.535000,24.815800>}
cylinder{<0,0,0><0,0.035000,0>0.127000 translate<0.000000,-1.535000,23.647400>}
box{<0,0,-0.127000><1.652367,0.035000,0.127000> rotate<0,44.997030,0> translate<-1.168400,-1.535000,24.815800> }
cylinder{<0,0,0><0,0.035000,0>0.127000 translate<-1.425000,-1.535000,27.205000>}
cylinder{<0,0,0><0,0.035000,0>0.127000 translate<0.000000,-1.535000,25.780000>}
box{<0,0,-0.127000><2.015254,0.035000,0.127000> rotate<0,44.997030,0> translate<-1.425000,-1.535000,27.205000> }
cylinder{<0,0,0><0,0.035000,0>0.127000 translate<-0.330200,-1.535000,12.674600>}
cylinder{<0,0,0><0,0.035000,0>0.127000 translate<0.050800,-1.535000,12.674600>}
box{<0,0,-0.127000><0.381000,0.035000,0.127000> rotate<0,0.000000,0> translate<-0.330200,-1.535000,12.674600> }
cylinder{<0,0,0><0,0.035000,0>0.127000 translate<0.050800,-1.535000,12.674600>}
cylinder{<0,0,0><0,0.035000,0>0.127000 translate<0.304800,-1.535000,12.928600>}
box{<0,0,-0.127000><0.359210,0.035000,0.127000> rotate<0,-44.997030,0> translate<0.050800,-1.535000,12.674600> }
cylinder{<0,0,0><0,0.035000,0>0.127000 translate<0.304800,-1.535000,12.928600>}
cylinder{<0,0,0><0,0.035000,0>0.127000 translate<0.304800,-1.535000,13.959800>}
box{<0,0,-0.127000><1.031200,0.035000,0.127000> rotate<0,90.000000,0> translate<0.304800,-1.535000,13.959800> }
cylinder{<0,0,0><0,0.035000,0>0.127000 translate<0.304800,-1.535000,13.959800>}
cylinder{<0,0,0><0,0.035000,0>0.127000 translate<0.315000,-1.535000,13.970000>}
box{<0,0,-0.127000><0.014425,0.035000,0.127000> rotate<0,-44.997030,0> translate<0.304800,-1.535000,13.959800> }
cylinder{<0,0,0><0,0.035000,0>0.127000 translate<0.315000,-1.535000,13.548400>}
cylinder{<0,0,0><0,0.035000,0>0.127000 translate<0.315000,-1.535000,13.970000>}
box{<0,0,-0.127000><0.421600,0.035000,0.127000> rotate<0,90.000000,0> translate<0.315000,-1.535000,13.970000> }
cylinder{<0,0,0><0,0.035000,0>0.127000 translate<0.315000,-1.535000,13.970000>}
cylinder{<0,0,0><0,0.035000,0>0.127000 translate<0.315000,-1.535000,17.145000>}
box{<0,0,-0.127000><3.175000,0.035000,0.127000> rotate<0,90.000000,0> translate<0.315000,-1.535000,17.145000> }
cylinder{<0,0,0><0,0.035000,0>0.254000 translate<-0.990600,0.000000,17.170400>}
cylinder{<0,0,0><0,0.035000,0>0.254000 translate<1.040000,0.000000,19.201000>}
box{<0,0,-0.254000><2.871702,0.035000,0.254000> rotate<0,-44.997030,0> translate<-0.990600,0.000000,17.170400> }
cylinder{<0,0,0><0,0.035000,0>0.254000 translate<1.040000,0.000000,19.210000>}
cylinder{<0,0,0><0,0.035000,0>0.254000 translate<1.040000,0.000000,19.201000>}
box{<0,0,-0.254000><0.009000,0.035000,0.254000> rotate<0,-90.000000,0> translate<1.040000,0.000000,19.201000> }
cylinder{<0,0,0><0,0.035000,0>0.127000 translate<-6.226600,-1.535000,19.210000>}
cylinder{<0,0,0><0,0.035000,0>0.127000 translate<1.040000,-1.535000,19.210000>}
box{<0,0,-0.127000><7.266600,0.035000,0.127000> rotate<0,0.000000,0> translate<-6.226600,-1.535000,19.210000> }
cylinder{<0,0,0><0,0.035000,0>0.254000 translate<1.143000,-1.535000,8.915400>}
cylinder{<0,0,0><0,0.035000,0>0.254000 translate<1.168400,-1.535000,8.940800>}
box{<0,0,-0.254000><0.035921,0.035000,0.254000> rotate<0,-44.997030,0> translate<1.143000,-1.535000,8.915400> }
cylinder{<0,0,0><0,0.035000,0>0.127000 translate<-0.400000,0.000000,3.400000>}
cylinder{<0,0,0><0,0.035000,0>0.127000 translate<1.335000,0.000000,5.135000>}
box{<0,0,-0.127000><2.453661,0.035000,0.127000> rotate<0,-44.997030,0> translate<-0.400000,0.000000,3.400000> }
cylinder{<0,0,0><0,0.035000,0>0.254000 translate<-6.225556,-1.535000,20.202441>}
cylinder{<0,0,0><0,0.035000,0>0.254000 translate<1.371400,-1.535000,20.276200>}
box{<0,0,-0.254000><7.597314,0.035000,0.254000> rotate<0,-0.556235,0> translate<-6.225556,-1.535000,20.202441> }
cylinder{<0,0,0><0,0.035000,0>0.127000 translate<1.143000,-1.535000,11.455400>}
cylinder{<0,0,0><0,0.035000,0>0.127000 translate<1.752600,-1.535000,12.065000>}
box{<0,0,-0.127000><0.862105,0.035000,0.127000> rotate<0,-44.997030,0> translate<1.143000,-1.535000,11.455400> }
cylinder{<0,0,0><0,0.035000,0>0.254000 translate<0.478600,0.000000,16.203000>}
cylinder{<0,0,0><0,0.035000,0>0.254000 translate<2.067000,0.000000,16.203000>}
box{<0,0,-0.254000><1.588400,0.035000,0.254000> rotate<0,0.000000,0> translate<0.478600,0.000000,16.203000> }
cylinder{<0,0,0><0,0.035000,0>0.254000 translate<2.067000,0.000000,16.203000>}
cylinder{<0,0,0><0,0.035000,0>0.254000 translate<2.289000,0.000000,15.981000>}
box{<0,0,-0.254000><0.313955,0.035000,0.254000> rotate<0,44.997030,0> translate<2.067000,0.000000,16.203000> }
cylinder{<0,0,0><0,0.035000,0>0.254000 translate<2.289000,0.000000,13.590000>}
cylinder{<0,0,0><0,0.035000,0>0.254000 translate<2.289000,0.000000,15.981000>}
box{<0,0,-0.254000><2.391000,0.035000,0.254000> rotate<0,90.000000,0> translate<2.289000,0.000000,15.981000> }
cylinder{<0,0,0><0,0.035000,0>0.254000 translate<1.168400,-1.535000,8.940800>}
cylinder{<0,0,0><0,0.035000,0>0.254000 translate<2.336800,-1.535000,8.940800>}
box{<0,0,-0.254000><1.168400,0.035000,0.254000> rotate<0,0.000000,0> translate<1.168400,-1.535000,8.940800> }
cylinder{<0,0,0><0,0.035000,0>0.254000 translate<2.338000,0.000000,8.625000>}
cylinder{<0,0,0><0,0.035000,0>0.254000 translate<2.338000,0.000000,10.741000>}
box{<0,0,-0.254000><2.116000,0.035000,0.254000> rotate<0,90.000000,0> translate<2.338000,0.000000,10.741000> }
cylinder{<0,0,0><0,0.035000,0>0.254000 translate<2.289000,0.000000,13.590000>}
cylinder{<0,0,0><0,0.035000,0>0.254000 translate<2.338000,0.000000,13.541000>}
box{<0,0,-0.254000><0.069296,0.035000,0.254000> rotate<0,44.997030,0> translate<2.289000,0.000000,13.590000> }
cylinder{<0,0,0><0,0.035000,0>0.254000 translate<-2.906000,0.000000,6.673000>}
cylinder{<0,0,0><0,0.035000,0>0.254000 translate<2.390000,0.000000,6.673000>}
box{<0,0,-0.254000><5.296000,0.035000,0.254000> rotate<0,0.000000,0> translate<-2.906000,0.000000,6.673000> }
cylinder{<0,0,0><0,0.035000,0>0.254000 translate<2.338000,0.000000,8.625000>}
cylinder{<0,0,0><0,0.035000,0>0.254000 translate<2.390000,0.000000,8.573000>}
box{<0,0,-0.254000><0.073539,0.035000,0.254000> rotate<0,44.997030,0> translate<2.338000,0.000000,8.625000> }
cylinder{<0,0,0><0,0.035000,0>0.127000 translate<2.500000,-1.535000,20.300000>}
cylinder{<0,0,0><0,0.035000,0>0.127000 translate<2.500000,-1.535000,17.566000>}
box{<0,0,-0.127000><2.734000,0.035000,0.127000> rotate<0,-90.000000,0> translate<2.500000,-1.535000,17.566000> }
cylinder{<0,0,0><0,0.035000,0>0.254000 translate<1.371400,-1.535000,20.276200>}
cylinder{<0,0,0><0,0.035000,0>0.254000 translate<2.997200,-1.535000,21.902000>}
box{<0,0,-0.254000><2.299228,0.035000,0.254000> rotate<0,-44.997030,0> translate<1.371400,-1.535000,20.276200> }
cylinder{<0,0,0><0,0.035000,0>0.254000 translate<2.997200,-1.535000,21.902000>}
cylinder{<0,0,0><0,0.035000,0>0.254000 translate<3.225800,-1.535000,22.123400>}
box{<0,0,-0.254000><0.318239,0.035000,0.254000> rotate<0,-44.080436,0> translate<2.997200,-1.535000,21.902000> }
cylinder{<0,0,0><0,0.035000,0>0.127000 translate<2.500000,-1.535000,20.300000>}
cylinder{<0,0,0><0,0.035000,0>0.127000 translate<3.300000,-1.535000,21.100000>}
box{<0,0,-0.127000><1.131371,0.035000,0.127000> rotate<0,-44.997030,0> translate<2.500000,-1.535000,20.300000> }
cylinder{<0,0,0><0,0.035000,0>0.127000 translate<2.500000,-1.535000,17.566000>}
cylinder{<0,0,0><0,0.035000,0>0.127000 translate<3.302000,-1.535000,16.764000>}
box{<0,0,-0.127000><1.134199,0.035000,0.127000> rotate<0,44.997030,0> translate<2.500000,-1.535000,17.566000> }
cylinder{<0,0,0><0,0.035000,0>0.254000 translate<2.336800,-1.535000,8.940800>}
cylinder{<0,0,0><0,0.035000,0>0.254000 translate<3.378200,-1.535000,9.982200>}
box{<0,0,-0.254000><1.472762,0.035000,0.254000> rotate<0,-44.997030,0> translate<2.336800,-1.535000,8.940800> }
cylinder{<0,0,0><0,0.035000,0>0.254000 translate<3.225800,-1.535000,22.123400>}
cylinder{<0,0,0><0,0.035000,0>0.254000 translate<3.402400,-1.535000,22.300000>}
box{<0,0,-0.254000><0.249750,0.035000,0.254000> rotate<0,-44.997030,0> translate<3.225800,-1.535000,22.123400> }
cylinder{<0,0,0><0,0.035000,0>0.127000 translate<3.300000,-1.535000,21.100000>}
cylinder{<0,0,0><0,0.035000,0>0.127000 translate<3.600000,-1.535000,21.100000>}
box{<0,0,-0.127000><0.300000,0.035000,0.127000> rotate<0,0.000000,0> translate<3.300000,-1.535000,21.100000> }
cylinder{<0,0,0><0,0.035000,0>0.254000 translate<-5.571000,-1.535000,5.715000>}
cylinder{<0,0,0><0,0.035000,0>0.254000 translate<3.810000,-1.535000,5.715000>}
box{<0,0,-0.254000><9.381000,0.035000,0.254000> rotate<0,0.000000,0> translate<-5.571000,-1.535000,5.715000> }
cylinder{<0,0,0><0,0.035000,0>0.127000 translate<1.335000,0.000000,5.135000>}
cylinder{<0,0,0><0,0.035000,0>0.127000 translate<3.865000,0.000000,5.135000>}
box{<0,0,-0.127000><2.530000,0.035000,0.127000> rotate<0,0.000000,0> translate<1.335000,0.000000,5.135000> }
cylinder{<0,0,0><0,0.035000,0>0.127000 translate<4.262000,0.000000,17.538000>}
cylinder{<0,0,0><0,0.035000,0>0.127000 translate<4.308000,0.000000,17.238000>}
box{<0,0,-0.127000><0.303506,0.035000,0.127000> rotate<0,81.277179,0> translate<4.262000,0.000000,17.538000> }
cylinder{<0,0,0><0,0.035000,0>0.127000 translate<4.308000,0.000000,15.482000>}
cylinder{<0,0,0><0,0.035000,0>0.127000 translate<4.308000,0.000000,17.238000>}
box{<0,0,-0.127000><1.756000,0.035000,0.127000> rotate<0,90.000000,0> translate<4.308000,0.000000,17.238000> }
cylinder{<0,0,0><0,0.035000,0>0.127000 translate<4.262000,-1.535000,17.538000>}
cylinder{<0,0,0><0,0.035000,0>0.127000 translate<4.318000,-1.535000,17.594000>}
box{<0,0,-0.127000><0.079196,0.035000,0.127000> rotate<0,-44.997030,0> translate<4.262000,-1.535000,17.538000> }
cylinder{<0,0,0><0,0.035000,0>0.127000 translate<4.318000,-1.535000,18.389600>}
cylinder{<0,0,0><0,0.035000,0>0.127000 translate<4.318000,-1.535000,17.594000>}
box{<0,0,-0.127000><0.795600,0.035000,0.127000> rotate<0,-90.000000,0> translate<4.318000,-1.535000,17.594000> }
cylinder{<0,0,0><0,0.035000,0>0.254000 translate<3.402400,-1.535000,22.300000>}
cylinder{<0,0,0><0,0.035000,0>0.254000 translate<4.385000,-1.535000,22.300000>}
box{<0,0,-0.254000><0.982600,0.035000,0.254000> rotate<0,0.000000,0> translate<3.402400,-1.535000,22.300000> }
cylinder{<0,0,0><0,0.035000,0>0.127000 translate<3.865000,0.000000,5.135000>}
cylinder{<0,0,0><0,0.035000,0>0.127000 translate<4.445000,0.000000,5.715000>}
box{<0,0,-0.127000><0.820244,0.035000,0.127000> rotate<0,-44.997030,0> translate<3.865000,0.000000,5.135000> }
cylinder{<0,0,0><0,0.035000,0>0.127000 translate<3.600000,-1.535000,21.100000>}
cylinder{<0,0,0><0,0.035000,0>0.127000 translate<4.450000,-1.535000,21.095000>}
box{<0,0,-0.127000><0.850015,0.035000,0.127000> rotate<0,0.337008,0> translate<3.600000,-1.535000,21.100000> }
cylinder{<0,0,0><0,0.035000,0>0.254000 translate<4.385000,-1.535000,22.300000>}
cylinder{<0,0,0><0,0.035000,0>0.254000 translate<4.450000,-1.535000,22.365000>}
box{<0,0,-0.254000><0.091924,0.035000,0.254000> rotate<0,-44.997030,0> translate<4.385000,-1.535000,22.300000> }
cylinder{<0,0,0><0,0.035000,0>0.127000 translate<1.752600,-1.535000,12.065000>}
cylinder{<0,0,0><0,0.035000,0>0.127000 translate<4.638000,-1.535000,12.065000>}
box{<0,0,-0.127000><2.885400,0.035000,0.127000> rotate<0,0.000000,0> translate<1.752600,-1.535000,12.065000> }
cylinder{<0,0,0><0,0.035000,0>0.127000 translate<4.826000,-1.535000,13.970000>}
cylinder{<0,0,0><0,0.035000,0>0.127000 translate<4.826000,-1.535000,14.539000>}
box{<0,0,-0.127000><0.569000,0.035000,0.127000> rotate<0,90.000000,0> translate<4.826000,-1.535000,14.539000> }
cylinder{<0,0,0><0,0.035000,0>0.127000 translate<4.826000,-1.535000,14.539000>}
cylinder{<0,0,0><0,0.035000,0>0.127000 translate<4.892000,-1.535000,14.605000>}
box{<0,0,-0.127000><0.093338,0.035000,0.127000> rotate<0,-44.997030,0> translate<4.826000,-1.535000,14.539000> }
cylinder{<0,0,0><0,0.035000,0>0.127000 translate<3.302000,-1.535000,16.764000>}
cylinder{<0,0,0><0,0.035000,0>0.127000 translate<4.972000,-1.535000,16.764000>}
box{<0,0,-0.127000><1.670000,0.035000,0.127000> rotate<0,0.000000,0> translate<3.302000,-1.535000,16.764000> }
cylinder{<0,0,0><0,0.035000,0>0.127000 translate<4.972000,-1.535000,16.764000>}
cylinder{<0,0,0><0,0.035000,0>0.127000 translate<4.972000,-1.535000,16.783000>}
box{<0,0,-0.127000><0.019000,0.035000,0.127000> rotate<0,90.000000,0> translate<4.972000,-1.535000,16.783000> }
cylinder{<0,0,0><0,0.035000,0>0.635000 translate<5.000000,0.000000,23.200000>}
cylinder{<0,0,0><0,0.035000,0>0.635000 translate<5.000000,0.000000,20.668000>}
box{<0,0,-0.635000><2.532000,0.035000,0.635000> rotate<0,-90.000000,0> translate<5.000000,0.000000,20.668000> }
cylinder{<0,0,0><0,0.035000,0>0.635000 translate<5.000000,0.000000,20.668000>}
cylinder{<0,0,0><0,0.035000,0>0.635000 translate<5.100000,0.000000,20.568000>}
box{<0,0,-0.635000><0.141421,0.035000,0.635000> rotate<0,44.997030,0> translate<5.000000,0.000000,20.668000> }
cylinder{<0,0,0><0,0.035000,0>0.254000 translate<3.810000,-1.535000,5.715000>}
cylinder{<0,0,0><0,0.035000,0>0.254000 translate<5.125000,-1.535000,4.400000>}
box{<0,0,-0.254000><1.859691,0.035000,0.254000> rotate<0,44.997030,0> translate<3.810000,-1.535000,5.715000> }
cylinder{<0,0,0><0,0.035000,0>0.127000 translate<4.972000,-1.535000,16.783000>}
cylinder{<0,0,0><0,0.035000,0>0.127000 translate<5.130800,-1.535000,16.941800>}
box{<0,0,-0.127000><0.224577,0.035000,0.127000> rotate<0,-44.997030,0> translate<4.972000,-1.535000,16.783000> }
cylinder{<0,0,0><0,0.035000,0>0.127000 translate<5.130800,-1.535000,16.941800>}
cylinder{<0,0,0><0,0.035000,0>0.127000 translate<5.130800,-1.535000,17.430800>}
box{<0,0,-0.127000><0.489000,0.035000,0.127000> rotate<0,90.000000,0> translate<5.130800,-1.535000,17.430800> }
cylinder{<0,0,0><0,0.035000,0>0.127000 translate<4.450000,-1.535000,23.635000>}
cylinder{<0,0,0><0,0.035000,0>0.127000 translate<5.300000,-1.535000,23.600000>}
box{<0,0,-0.127000><0.850720,0.035000,0.127000> rotate<0,2.357750,0> translate<4.450000,-1.535000,23.635000> }
cylinder{<0,0,0><0,0.035000,0>0.127000 translate<4.826000,-1.535000,13.970000>}
cylinder{<0,0,0><0,0.035000,0>0.127000 translate<5.461000,-1.535000,13.335000>}
box{<0,0,-0.127000><0.898026,0.035000,0.127000> rotate<0,44.997030,0> translate<4.826000,-1.535000,13.970000> }
cylinder{<0,0,0><0,0.035000,0>0.127000 translate<4.308000,0.000000,15.482000>}
cylinder{<0,0,0><0,0.035000,0>0.127000 translate<5.815000,0.000000,15.235000>}
box{<0,0,-0.127000><1.527108,0.035000,0.127000> rotate<0,9.307505,0> translate<4.308000,0.000000,15.482000> }
cylinder{<0,0,0><0,0.035000,0>0.127000 translate<5.425000,0.000000,2.523000>}
cylinder{<0,0,0><0,0.035000,0>0.127000 translate<5.968000,0.000000,1.980000>}
box{<0,0,-0.127000><0.767918,0.035000,0.127000> rotate<0,44.997030,0> translate<5.425000,0.000000,2.523000> }
cylinder{<0,0,0><0,0.035000,0>0.127000 translate<5.461000,-1.535000,13.335000>}
cylinder{<0,0,0><0,0.035000,0>0.127000 translate<6.096000,-1.535000,13.335000>}
box{<0,0,-0.127000><0.635000,0.035000,0.127000> rotate<0,0.000000,0> translate<5.461000,-1.535000,13.335000> }
cylinder{<0,0,0><0,0.035000,0>0.127000 translate<5.300000,-1.535000,23.600000>}
cylinder{<0,0,0><0,0.035000,0>0.127000 translate<6.400000,-1.535000,23.600000>}
box{<0,0,-0.127000><1.100000,0.035000,0.127000> rotate<0,0.000000,0> translate<5.300000,-1.535000,23.600000> }
cylinder{<0,0,0><0,0.035000,0>0.127000 translate<6.400000,-1.535000,23.600000>}
cylinder{<0,0,0><0,0.035000,0>0.127000 translate<6.500000,-1.535000,23.700000>}
box{<0,0,-0.127000><0.141421,0.035000,0.127000> rotate<0,-44.997030,0> translate<6.400000,-1.535000,23.600000> }
cylinder{<0,0,0><0,0.035000,0>0.127000 translate<6.096000,-1.535000,13.335000>}
cylinder{<0,0,0><0,0.035000,0>0.127000 translate<6.538000,-1.535000,12.893000>}
box{<0,0,-0.127000><0.625082,0.035000,0.127000> rotate<0,44.997030,0> translate<6.096000,-1.535000,13.335000> }
cylinder{<0,0,0><0,0.035000,0>0.127000 translate<6.538000,-1.535000,12.065000>}
cylinder{<0,0,0><0,0.035000,0>0.127000 translate<6.538000,-1.535000,12.893000>}
box{<0,0,-0.127000><0.828000,0.035000,0.127000> rotate<0,90.000000,0> translate<6.538000,-1.535000,12.893000> }
cylinder{<0,0,0><0,0.035000,0>0.127000 translate<6.500000,-1.535000,23.700000>}
cylinder{<0,0,0><0,0.035000,0>0.127000 translate<6.600000,-1.535000,23.700000>}
box{<0,0,-0.127000><0.100000,0.035000,0.127000> rotate<0,0.000000,0> translate<6.500000,-1.535000,23.700000> }
cylinder{<0,0,0><0,0.035000,0>0.127000 translate<6.600000,-1.535000,23.700000>}
cylinder{<0,0,0><0,0.035000,0>0.127000 translate<6.700000,-1.535000,23.800000>}
box{<0,0,-0.127000><0.141421,0.035000,0.127000> rotate<0,-44.997030,0> translate<6.600000,-1.535000,23.700000> }
cylinder{<0,0,0><0,0.035000,0>0.127000 translate<5.425000,0.000000,2.523000>}
cylinder{<0,0,0><0,0.035000,0>0.127000 translate<6.760000,0.000000,3.858000>}
box{<0,0,-0.127000><1.887975,0.035000,0.127000> rotate<0,-44.997030,0> translate<5.425000,0.000000,2.523000> }
cylinder{<0,0,0><0,0.035000,0>0.127000 translate<6.760000,0.000000,4.060000>}
cylinder{<0,0,0><0,0.035000,0>0.127000 translate<6.760000,0.000000,3.858000>}
box{<0,0,-0.127000><0.202000,0.035000,0.127000> rotate<0,-90.000000,0> translate<6.760000,0.000000,3.858000> }
cylinder{<0,0,0><0,0.035000,0>0.127000 translate<6.792000,-1.535000,16.063000>}
cylinder{<0,0,0><0,0.035000,0>0.127000 translate<6.792000,-1.535000,14.605000>}
box{<0,0,-0.127000><1.458000,0.035000,0.127000> rotate<0,-90.000000,0> translate<6.792000,-1.535000,14.605000> }
cylinder{<0,0,0><0,0.035000,0>0.127000 translate<6.700000,-1.535000,23.800000>}
cylinder{<0,0,0><0,0.035000,0>0.127000 translate<6.800000,-1.535000,23.800000>}
box{<0,0,-0.127000><0.100000,0.035000,0.127000> rotate<0,0.000000,0> translate<6.700000,-1.535000,23.800000> }
cylinder{<0,0,0><0,0.035000,0>0.127000 translate<5.968000,0.000000,1.980000>}
cylinder{<0,0,0><0,0.035000,0>0.127000 translate<6.802400,0.000000,1.980000>}
box{<0,0,-0.127000><0.834400,0.035000,0.127000> rotate<0,0.000000,0> translate<5.968000,0.000000,1.980000> }
cylinder{<0,0,0><0,0.035000,0>0.127000 translate<6.800000,-1.535000,23.800000>}
cylinder{<0,0,0><0,0.035000,0>0.127000 translate<6.900000,-1.535000,23.900000>}
box{<0,0,-0.127000><0.141421,0.035000,0.127000> rotate<0,-44.997030,0> translate<6.800000,-1.535000,23.800000> }
cylinder{<0,0,0><0,0.035000,0>0.254000 translate<5.815000,0.000000,13.335000>}
cylinder{<0,0,0><0,0.035000,0>0.254000 translate<6.985000,0.000000,12.165000>}
box{<0,0,-0.254000><1.654630,0.035000,0.254000> rotate<0,44.997030,0> translate<5.815000,0.000000,13.335000> }
cylinder{<0,0,0><0,0.035000,0>0.254000 translate<6.985000,0.000000,10.160000>}
cylinder{<0,0,0><0,0.035000,0>0.254000 translate<6.985000,0.000000,12.165000>}
box{<0,0,-0.254000><2.005000,0.035000,0.254000> rotate<0,90.000000,0> translate<6.985000,0.000000,12.165000> }
cylinder{<0,0,0><0,0.035000,0>0.127000 translate<6.900000,-1.535000,23.900000>}
cylinder{<0,0,0><0,0.035000,0>0.127000 translate<7.000000,-1.535000,23.900000>}
box{<0,0,-0.127000><0.100000,0.035000,0.127000> rotate<0,0.000000,0> translate<6.900000,-1.535000,23.900000> }
cylinder{<0,0,0><0,0.035000,0>0.127000 translate<4.318000,-1.535000,18.389600>}
cylinder{<0,0,0><0,0.035000,0>0.127000 translate<7.023400,-1.535000,21.095000>}
box{<0,0,-0.127000><3.826013,0.035000,0.127000> rotate<0,-44.997030,0> translate<4.318000,-1.535000,18.389600> }
cylinder{<0,0,0><0,0.035000,0>0.127000 translate<7.000000,-1.535000,23.900000>}
cylinder{<0,0,0><0,0.035000,0>0.127000 translate<7.100000,-1.535000,24.000000>}
box{<0,0,-0.127000><0.141421,0.035000,0.127000> rotate<0,-44.997030,0> translate<7.000000,-1.535000,23.900000> }
cylinder{<0,0,0><0,0.035000,0>0.127000 translate<7.100000,-1.535000,24.000000>}
cylinder{<0,0,0><0,0.035000,0>0.127000 translate<7.200000,-1.535000,24.000000>}
box{<0,0,-0.127000><0.100000,0.035000,0.127000> rotate<0,0.000000,0> translate<7.100000,-1.535000,24.000000> }
cylinder{<0,0,0><0,0.035000,0>0.254000 translate<4.450000,-1.535000,22.365000>}
cylinder{<0,0,0><0,0.035000,0>0.254000 translate<7.235000,-1.535000,22.365000>}
box{<0,0,-0.254000><2.785000,0.035000,0.254000> rotate<0,0.000000,0> translate<4.450000,-1.535000,22.365000> }
cylinder{<0,0,0><0,0.035000,0>0.127000 translate<7.200000,-1.535000,24.000000>}
cylinder{<0,0,0><0,0.035000,0>0.127000 translate<7.300000,-1.535000,24.100000>}
box{<0,0,-0.127000><0.141421,0.035000,0.127000> rotate<0,-44.997030,0> translate<7.200000,-1.535000,24.000000> }
cylinder{<0,0,0><0,0.035000,0>0.127000 translate<7.300000,-1.535000,24.100000>}
cylinder{<0,0,0><0,0.035000,0>0.127000 translate<7.300000,-1.535000,25.800000>}
box{<0,0,-0.127000><1.700000,0.035000,0.127000> rotate<0,90.000000,0> translate<7.300000,-1.535000,25.800000> }
cylinder{<0,0,0><0,0.035000,0>0.254000 translate<7.235000,-1.535000,22.365000>}
cylinder{<0,0,0><0,0.035000,0>0.254000 translate<7.400000,-1.535000,22.200000>}
box{<0,0,-0.254000><0.233345,0.035000,0.254000> rotate<0,44.997030,0> translate<7.235000,-1.535000,22.365000> }
cylinder{<0,0,0><0,0.035000,0>0.127000 translate<7.300000,-1.535000,25.800000>}
cylinder{<0,0,0><0,0.035000,0>0.127000 translate<7.400000,-1.535000,25.900000>}
box{<0,0,-0.127000><0.141421,0.035000,0.127000> rotate<0,-44.997030,0> translate<7.300000,-1.535000,25.800000> }
cylinder{<0,0,0><0,0.035000,0>0.127000 translate<7.400000,-1.535000,25.900000>}
cylinder{<0,0,0><0,0.035000,0>0.127000 translate<7.400000,-1.535000,26.000000>}
box{<0,0,-0.127000><0.100000,0.035000,0.127000> rotate<0,90.000000,0> translate<7.400000,-1.535000,26.000000> }
cylinder{<0,0,0><0,0.035000,0>0.254000 translate<5.815000,0.000000,13.335000>}
cylinder{<0,0,0><0,0.035000,0>0.254000 translate<7.500000,0.000000,13.950000>}
box{<0,0,-0.254000><1.793725,0.035000,0.254000> rotate<0,-20.050009,0> translate<5.815000,0.000000,13.335000> }
cylinder{<0,0,0><0,0.035000,0>0.254000 translate<7.500000,0.000000,22.100000>}
cylinder{<0,0,0><0,0.035000,0>0.254000 translate<7.500000,0.000000,13.950000>}
box{<0,0,-0.254000><8.150000,0.035000,0.254000> rotate<0,-90.000000,0> translate<7.500000,0.000000,13.950000> }
cylinder{<0,0,0><0,0.035000,0>0.254000 translate<7.400000,0.000000,22.200000>}
cylinder{<0,0,0><0,0.035000,0>0.254000 translate<7.500000,0.000000,22.100000>}
box{<0,0,-0.254000><0.141421,0.035000,0.254000> rotate<0,44.997030,0> translate<7.400000,0.000000,22.200000> }
cylinder{<0,0,0><0,0.035000,0>0.127000 translate<7.400000,-1.535000,26.000000>}
cylinder{<0,0,0><0,0.035000,0>0.127000 translate<7.500000,-1.535000,26.100000>}
box{<0,0,-0.127000><0.141421,0.035000,0.127000> rotate<0,-44.997030,0> translate<7.400000,-1.535000,26.000000> }
cylinder{<0,0,0><0,0.035000,0>0.127000 translate<7.500000,-1.535000,26.100000>}
cylinder{<0,0,0><0,0.035000,0>0.127000 translate<7.500000,-1.535000,26.200000>}
box{<0,0,-0.127000><0.100000,0.035000,0.127000> rotate<0,90.000000,0> translate<7.500000,-1.535000,26.200000> }
cylinder{<0,0,0><0,0.035000,0>0.127000 translate<7.500000,-1.535000,26.200000>}
cylinder{<0,0,0><0,0.035000,0>0.127000 translate<7.600000,-1.535000,26.300000>}
box{<0,0,-0.127000><0.141421,0.035000,0.127000> rotate<0,-44.997030,0> translate<7.500000,-1.535000,26.200000> }
cylinder{<0,0,0><0,0.035000,0>0.127000 translate<4.445000,0.000000,5.715000>}
cylinder{<0,0,0><0,0.035000,0>0.127000 translate<7.620000,0.000000,5.715000>}
box{<0,0,-0.127000><3.175000,0.035000,0.127000> rotate<0,0.000000,0> translate<4.445000,0.000000,5.715000> }
cylinder{<0,0,0><0,0.035000,0>0.127000 translate<5.130800,-1.535000,17.430800>}
cylinder{<0,0,0><0,0.035000,0>0.127000 translate<7.700000,-1.535000,20.000000>}
box{<0,0,-0.127000><3.633397,0.035000,0.127000> rotate<0,-44.997030,0> translate<5.130800,-1.535000,17.430800> }
cylinder{<0,0,0><0,0.035000,0>0.127000 translate<6.538000,-1.535000,12.065000>}
cylinder{<0,0,0><0,0.035000,0>0.127000 translate<8.001000,-1.535000,13.528000>}
box{<0,0,-0.127000><2.068994,0.035000,0.127000> rotate<0,-44.997030,0> translate<6.538000,-1.535000,12.065000> }
cylinder{<0,0,0><0,0.035000,0>0.127000 translate<8.001000,-1.535000,13.528000>}
cylinder{<0,0,0><0,0.035000,0>0.127000 translate<8.001000,-1.535000,14.224000>}
box{<0,0,-0.127000><0.696000,0.035000,0.127000> rotate<0,90.000000,0> translate<8.001000,-1.535000,14.224000> }
cylinder{<0,0,0><0,0.035000,0>0.127000 translate<8.200000,0.000000,10.600000>}
cylinder{<0,0,0><0,0.035000,0>0.127000 translate<8.200000,0.000000,21.100000>}
box{<0,0,-0.127000><10.500000,0.035000,0.127000> rotate<0,90.000000,0> translate<8.200000,0.000000,21.100000> }
cylinder{<0,0,0><0,0.035000,0>0.127000 translate<8.255000,0.000000,5.080000>}
cylinder{<0,0,0><0,0.035000,0>0.127000 translate<8.255000,0.000000,4.465000>}
box{<0,0,-0.127000><0.615000,0.035000,0.127000> rotate<0,-90.000000,0> translate<8.255000,0.000000,4.465000> }
cylinder{<0,0,0><0,0.035000,0>0.127000 translate<7.620000,0.000000,5.715000>}
cylinder{<0,0,0><0,0.035000,0>0.127000 translate<8.255000,0.000000,5.080000>}
box{<0,0,-0.127000><0.898026,0.035000,0.127000> rotate<0,44.997030,0> translate<7.620000,0.000000,5.715000> }
cylinder{<0,0,0><0,0.035000,0>0.127000 translate<6.792000,-1.535000,16.063000>}
cylinder{<0,0,0><0,0.035000,0>0.127000 translate<8.255000,-1.535000,17.526000>}
box{<0,0,-0.127000><2.068994,0.035000,0.127000> rotate<0,-44.997030,0> translate<6.792000,-1.535000,16.063000> }
cylinder{<0,0,0><0,0.035000,0>0.127000 translate<8.502400,0.000000,1.980000>}
cylinder{<0,0,0><0,0.035000,0>0.127000 translate<8.502400,0.000000,3.902400>}
box{<0,0,-0.127000><1.922400,0.035000,0.127000> rotate<0,90.000000,0> translate<8.502400,0.000000,3.902400> }
cylinder{<0,0,0><0,0.035000,0>0.254000 translate<3.378200,-1.535000,9.982200>}
cylinder{<0,0,0><0,0.035000,0>0.254000 translate<8.509000,-1.535000,9.982200>}
box{<0,0,-0.254000><5.130800,0.035000,0.254000> rotate<0,0.000000,0> translate<3.378200,-1.535000,9.982200> }
cylinder{<0,0,0><0,0.035000,0>0.127000 translate<8.255000,0.000000,3.655000>}
cylinder{<0,0,0><0,0.035000,0>0.127000 translate<8.660000,0.000000,4.060000>}
box{<0,0,-0.127000><0.572756,0.035000,0.127000> rotate<0,-44.997030,0> translate<8.255000,0.000000,3.655000> }
cylinder{<0,0,0><0,0.035000,0>0.127000 translate<8.255000,0.000000,4.465000>}
cylinder{<0,0,0><0,0.035000,0>0.127000 translate<8.660000,0.000000,4.060000>}
box{<0,0,-0.127000><0.572756,0.035000,0.127000> rotate<0,44.997030,0> translate<8.255000,0.000000,4.465000> }
cylinder{<0,0,0><0,0.035000,0>0.127000 translate<8.350000,0.000000,3.750000>}
cylinder{<0,0,0><0,0.035000,0>0.127000 translate<8.660000,0.000000,4.060000>}
box{<0,0,-0.127000><0.438406,0.035000,0.127000> rotate<0,-44.997030,0> translate<8.350000,0.000000,3.750000> }
cylinder{<0,0,0><0,0.035000,0>0.127000 translate<8.502400,0.000000,3.902400>}
cylinder{<0,0,0><0,0.035000,0>0.127000 translate<8.660000,0.000000,4.060000>}
box{<0,0,-0.127000><0.222880,0.035000,0.127000> rotate<0,-44.997030,0> translate<8.502400,0.000000,3.902400> }
cylinder{<0,0,0><0,0.035000,0>0.127000 translate<8.001000,-1.535000,14.224000>}
cylinder{<0,0,0><0,0.035000,0>0.127000 translate<8.763000,-1.535000,14.986000>}
box{<0,0,-0.127000><1.077631,0.035000,0.127000> rotate<0,-44.997030,0> translate<8.001000,-1.535000,14.224000> }
cylinder{<0,0,0><0,0.035000,0>0.254000 translate<6.985000,0.000000,10.160000>}
cylinder{<0,0,0><0,0.035000,0>0.254000 translate<8.890000,0.000000,8.255000>}
box{<0,0,-0.254000><2.694077,0.035000,0.254000> rotate<0,44.997030,0> translate<6.985000,0.000000,10.160000> }
cylinder{<0,0,0><0,0.035000,0>0.254000 translate<8.890000,0.000000,6.350000>}
cylinder{<0,0,0><0,0.035000,0>0.254000 translate<8.890000,0.000000,8.255000>}
box{<0,0,-0.254000><1.905000,0.035000,0.254000> rotate<0,90.000000,0> translate<8.890000,0.000000,8.255000> }
cylinder{<0,0,0><0,0.035000,0>0.254000 translate<8.890000,0.000000,6.350000>}
cylinder{<0,0,0><0,0.035000,0>0.254000 translate<9.144000,0.000000,6.096000>}
box{<0,0,-0.254000><0.359210,0.035000,0.254000> rotate<0,44.997030,0> translate<8.890000,0.000000,6.350000> }
cylinder{<0,0,0><0,0.035000,0>0.127000 translate<7.023400,-1.535000,21.095000>}
cylinder{<0,0,0><0,0.035000,0>0.127000 translate<9.150000,-1.535000,21.095000>}
box{<0,0,-0.127000><2.126600,0.035000,0.127000> rotate<0,0.000000,0> translate<7.023400,-1.535000,21.095000> }
cylinder{<0,0,0><0,0.035000,0>0.635000 translate<5.000000,0.000000,23.200000>}
cylinder{<0,0,0><0,0.035000,0>0.635000 translate<9.580000,0.000000,27.780000>}
box{<0,0,-0.635000><6.477098,0.035000,0.635000> rotate<0,-44.997030,0> translate<5.000000,0.000000,23.200000> }
cylinder{<0,0,0><0,0.035000,0>0.254000 translate<8.509000,-1.535000,9.982200>}
cylinder{<0,0,0><0,0.035000,0>0.254000 translate<9.716800,-1.535000,11.190000>}
box{<0,0,-0.254000><1.708087,0.035000,0.254000> rotate<0,-44.997030,0> translate<8.509000,-1.535000,9.982200> }
cylinder{<0,0,0><0,0.035000,0>0.254000 translate<9.716800,-1.535000,11.190000>}
cylinder{<0,0,0><0,0.035000,0>0.254000 translate<9.730000,-1.535000,11.190000>}
box{<0,0,-0.254000><0.013200,0.035000,0.254000> rotate<0,0.000000,0> translate<9.716800,-1.535000,11.190000> }
cylinder{<0,0,0><0,0.035000,0>0.127000 translate<9.150000,-1.535000,21.095000>}
cylinder{<0,0,0><0,0.035000,0>0.127000 translate<10.000000,-1.535000,21.100000>}
box{<0,0,-0.127000><0.850015,0.035000,0.127000> rotate<0,-0.337008,0> translate<9.150000,-1.535000,21.095000> }
cylinder{<0,0,0><0,0.035000,0>0.127000 translate<9.150000,-1.535000,23.635000>}
cylinder{<0,0,0><0,0.035000,0>0.127000 translate<10.000000,-1.535000,23.600000>}
box{<0,0,-0.127000><0.850720,0.035000,0.127000> rotate<0,2.357750,0> translate<9.150000,-1.535000,23.635000> }
cylinder{<0,0,0><0,0.035000,0>0.127000 translate<8.200000,0.000000,10.600000>}
cylinder{<0,0,0><0,0.035000,0>0.127000 translate<10.038000,0.000000,8.762000>}
box{<0,0,-0.127000><2.599325,0.035000,0.127000> rotate<0,44.997030,0> translate<8.200000,0.000000,10.600000> }
cylinder{<0,0,0><0,0.035000,0>0.127000 translate<8.660000,0.000000,4.060000>}
cylinder{<0,0,0><0,0.035000,0>0.127000 translate<10.332000,0.000000,4.060000>}
box{<0,0,-0.127000><1.672000,0.035000,0.127000> rotate<0,0.000000,0> translate<8.660000,0.000000,4.060000> }
cylinder{<0,0,0><0,0.035000,0>0.127000 translate<7.600000,-1.535000,26.300000>}
cylinder{<0,0,0><0,0.035000,0>0.127000 translate<10.400000,-1.535000,26.300000>}
box{<0,0,-0.127000><2.800000,0.035000,0.127000> rotate<0,0.000000,0> translate<7.600000,-1.535000,26.300000> }
cylinder{<0,0,0><0,0.035000,0>0.127000 translate<10.400000,-1.535000,26.300000>}
cylinder{<0,0,0><0,0.035000,0>0.127000 translate<10.500000,-1.535000,26.200000>}
box{<0,0,-0.127000><0.141421,0.035000,0.127000> rotate<0,44.997030,0> translate<10.400000,-1.535000,26.300000> }
cylinder{<0,0,0><0,0.035000,0>0.127000 translate<10.600000,-1.535000,26.200000>}
cylinder{<0,0,0><0,0.035000,0>0.127000 translate<10.600000,-1.535000,26.100000>}
box{<0,0,-0.127000><0.100000,0.035000,0.127000> rotate<0,-90.000000,0> translate<10.600000,-1.535000,26.100000> }
cylinder{<0,0,0><0,0.035000,0>0.127000 translate<10.500000,-1.535000,26.200000>}
cylinder{<0,0,0><0,0.035000,0>0.127000 translate<10.600000,-1.535000,26.200000>}
box{<0,0,-0.127000><0.100000,0.035000,0.127000> rotate<0,0.000000,0> translate<10.500000,-1.535000,26.200000> }
cylinder{<0,0,0><0,0.035000,0>0.127000 translate<10.000000,-1.535000,21.100000>}
cylinder{<0,0,0><0,0.035000,0>0.127000 translate<10.700000,-1.535000,21.100000>}
box{<0,0,-0.127000><0.700000,0.035000,0.127000> rotate<0,0.000000,0> translate<10.000000,-1.535000,21.100000> }
cylinder{<0,0,0><0,0.035000,0>0.127000 translate<10.600000,-1.535000,26.100000>}
cylinder{<0,0,0><0,0.035000,0>0.127000 translate<10.700000,-1.535000,26.000000>}
box{<0,0,-0.127000><0.141421,0.035000,0.127000> rotate<0,44.997030,0> translate<10.600000,-1.535000,26.100000> }
cylinder{<0,0,0><0,0.035000,0>0.127000 translate<10.038000,0.000000,8.762000>}
cylinder{<0,0,0><0,0.035000,0>0.127000 translate<10.800000,0.000000,8.224000>}
box{<0,0,-0.127000><0.932785,0.035000,0.127000> rotate<0,35.221172,0> translate<10.038000,0.000000,8.762000> }
cylinder{<0,0,0><0,0.035000,0>0.127000 translate<10.800000,-1.535000,26.000000>}
cylinder{<0,0,0><0,0.035000,0>0.127000 translate<10.800000,-1.535000,25.900000>}
box{<0,0,-0.127000><0.100000,0.035000,0.127000> rotate<0,-90.000000,0> translate<10.800000,-1.535000,25.900000> }
cylinder{<0,0,0><0,0.035000,0>0.127000 translate<10.700000,-1.535000,26.000000>}
cylinder{<0,0,0><0,0.035000,0>0.127000 translate<10.800000,-1.535000,26.000000>}
box{<0,0,-0.127000><0.100000,0.035000,0.127000> rotate<0,0.000000,0> translate<10.700000,-1.535000,26.000000> }
cylinder{<0,0,0><0,0.035000,0>0.127000 translate<8.200000,0.000000,21.100000>}
cylinder{<0,0,0><0,0.035000,0>0.127000 translate<10.801600,0.000000,23.600000>}
box{<0,0,-0.127000><3.608091,0.035000,0.127000> rotate<0,-43.856192,0> translate<8.200000,0.000000,21.100000> }
cylinder{<0,0,0><0,0.035000,0>0.127000 translate<10.000000,-1.535000,23.600000>}
cylinder{<0,0,0><0,0.035000,0>0.127000 translate<10.801600,-1.535000,23.600000>}
box{<0,0,-0.127000><0.801600,0.035000,0.127000> rotate<0,0.000000,0> translate<10.000000,-1.535000,23.600000> }
cylinder{<0,0,0><0,0.035000,0>0.127000 translate<10.800000,0.000000,23.500000>}
cylinder{<0,0,0><0,0.035000,0>0.127000 translate<10.801600,0.000000,23.600000>}
box{<0,0,-0.127000><0.100013,0.035000,0.127000> rotate<0,-89.077467,0> translate<10.800000,0.000000,23.500000> }
cylinder{<0,0,0><0,0.035000,0>0.127000 translate<10.800000,-1.535000,25.900000>}
cylinder{<0,0,0><0,0.035000,0>0.127000 translate<10.900000,-1.535000,25.800000>}
box{<0,0,-0.127000><0.141421,0.035000,0.127000> rotate<0,44.997030,0> translate<10.800000,-1.535000,25.900000> }
cylinder{<0,0,0><0,0.035000,0>0.127000 translate<8.255000,-1.535000,17.526000>}
cylinder{<0,0,0><0,0.035000,0>0.127000 translate<10.986000,-1.535000,17.526000>}
box{<0,0,-0.127000><2.731000,0.035000,0.127000> rotate<0,0.000000,0> translate<8.255000,-1.535000,17.526000> }
cylinder{<0,0,0><0,0.035000,0>0.127000 translate<7.700000,-1.535000,20.000000>}
cylinder{<0,0,0><0,0.035000,0>0.127000 translate<11.033800,-1.535000,20.000000>}
box{<0,0,-0.127000><3.333800,0.035000,0.127000> rotate<0,0.000000,0> translate<7.700000,-1.535000,20.000000> }
cylinder{<0,0,0><0,0.035000,0>0.127000 translate<10.332000,0.000000,4.060000>}
cylinder{<0,0,0><0,0.035000,0>0.127000 translate<11.286000,0.000000,3.106000>}
box{<0,0,-0.127000><1.349160,0.035000,0.127000> rotate<0,44.997030,0> translate<10.332000,0.000000,4.060000> }
cylinder{<0,0,0><0,0.035000,0>0.127000 translate<11.200000,0.000000,3.020000>}
cylinder{<0,0,0><0,0.035000,0>0.127000 translate<11.286000,0.000000,3.106000>}
box{<0,0,-0.127000><0.121622,0.035000,0.127000> rotate<0,-44.997030,0> translate<11.200000,0.000000,3.020000> }
cylinder{<0,0,0><0,0.035000,0>0.127000 translate<11.033800,-1.535000,20.000000>}
cylinder{<0,0,0><0,0.035000,0>0.127000 translate<11.709400,-1.535000,20.675600>}
box{<0,0,-0.127000><0.955443,0.035000,0.127000> rotate<0,-44.997030,0> translate<11.033800,-1.535000,20.000000> }
cylinder{<0,0,0><0,0.035000,0>0.127000 translate<10.986000,-1.535000,17.526000>}
cylinder{<0,0,0><0,0.035000,0>0.127000 translate<12.270000,-1.535000,18.810000>}
box{<0,0,-0.127000><1.815850,0.035000,0.127000> rotate<0,-44.997030,0> translate<10.986000,-1.535000,17.526000> }
cylinder{<0,0,0><0,0.035000,0>0.127000 translate<12.270000,0.000000,16.270000>}
cylinder{<0,0,0><0,0.035000,0>0.127000 translate<12.340000,0.000000,16.200000>}
box{<0,0,-0.127000><0.098995,0.035000,0.127000> rotate<0,44.997030,0> translate<12.270000,0.000000,16.270000> }
cylinder{<0,0,0><0,0.035000,0>0.127000 translate<12.340000,0.000000,16.200000>}
cylinder{<0,0,0><0,0.035000,0>0.127000 translate<13.000000,0.000000,16.200000>}
box{<0,0,-0.127000><0.660000,0.035000,0.127000> rotate<0,0.000000,0> translate<12.340000,0.000000,16.200000> }
cylinder{<0,0,0><0,0.035000,0>0.254000 translate<9.144000,-1.535000,6.096000>}
cylinder{<0,0,0><0,0.035000,0>0.254000 translate<13.084000,-1.535000,6.096000>}
box{<0,0,-0.254000><3.940000,0.035000,0.254000> rotate<0,0.000000,0> translate<9.144000,-1.535000,6.096000> }
cylinder{<0,0,0><0,0.035000,0>0.127000 translate<10.800000,-1.535000,8.224000>}
cylinder{<0,0,0><0,0.035000,0>0.127000 translate<13.152000,-1.535000,8.224000>}
box{<0,0,-0.127000><2.352000,0.035000,0.127000> rotate<0,0.000000,0> translate<10.800000,-1.535000,8.224000> }
cylinder{<0,0,0><0,0.035000,0>0.127000 translate<13.186000,0.000000,3.106000>}
cylinder{<0,0,0><0,0.035000,0>0.127000 translate<13.186000,0.000000,2.943000>}
box{<0,0,-0.127000><0.163000,0.035000,0.127000> rotate<0,-90.000000,0> translate<13.186000,0.000000,2.943000> }
cylinder{<0,0,0><0,0.035000,0>0.254000 translate<13.084000,-1.535000,6.096000>}
cylinder{<0,0,0><0,0.035000,0>0.254000 translate<13.232000,-1.535000,6.244000>}
box{<0,0,-0.254000><0.209304,0.035000,0.254000> rotate<0,-44.997030,0> translate<13.084000,-1.535000,6.096000> }
cylinder{<0,0,0><0,0.035000,0>0.127000 translate<13.152000,-1.535000,8.224000>}
cylinder{<0,0,0><0,0.035000,0>0.127000 translate<13.232000,-1.535000,8.144000>}
box{<0,0,-0.127000><0.113137,0.035000,0.127000> rotate<0,44.997030,0> translate<13.152000,-1.535000,8.224000> }
cylinder{<0,0,0><0,0.035000,0>0.127000 translate<13.232000,-1.535000,8.144000>}
cylinder{<0,0,0><0,0.035000,0>0.127000 translate<13.248000,-1.535000,8.128000>}
box{<0,0,-0.127000><0.022627,0.035000,0.127000> rotate<0,44.997030,0> translate<13.232000,-1.535000,8.144000> }
cylinder{<0,0,0><0,0.035000,0>0.127000 translate<10.700000,0.000000,21.100000>}
cylinder{<0,0,0><0,0.035000,0>0.127000 translate<13.600000,0.000000,21.100000>}
box{<0,0,-0.127000><2.900000,0.035000,0.127000> rotate<0,0.000000,0> translate<10.700000,0.000000,21.100000> }
cylinder{<0,0,0><0,0.035000,0>0.127000 translate<13.700000,0.000000,21.000000>}
cylinder{<0,0,0><0,0.035000,0>0.127000 translate<13.700000,0.000000,17.542000>}
box{<0,0,-0.127000><3.458000,0.035000,0.127000> rotate<0,-90.000000,0> translate<13.700000,0.000000,17.542000> }
cylinder{<0,0,0><0,0.035000,0>0.127000 translate<13.600000,0.000000,21.100000>}
cylinder{<0,0,0><0,0.035000,0>0.127000 translate<13.700000,0.000000,21.000000>}
box{<0,0,-0.127000><0.141421,0.035000,0.127000> rotate<0,44.997030,0> translate<13.600000,0.000000,21.100000> }
cylinder{<0,0,0><0,0.035000,0>0.127000 translate<11.709400,-1.535000,20.675600>}
cylinder{<0,0,0><0,0.035000,0>0.127000 translate<14.071600,-1.535000,20.675600>}
box{<0,0,-0.127000><2.362200,0.035000,0.127000> rotate<0,0.000000,0> translate<11.709400,-1.535000,20.675600> }
cylinder{<0,0,0><0,0.035000,0>0.127000 translate<13.186000,0.000000,2.943000>}
cylinder{<0,0,0><0,0.035000,0>0.127000 translate<14.097000,0.000000,2.032000>}
box{<0,0,-0.127000><1.288349,0.035000,0.127000> rotate<0,44.997030,0> translate<13.186000,0.000000,2.943000> }
cylinder{<0,0,0><0,0.035000,0>0.127000 translate<8.763000,-1.535000,14.986000>}
cylinder{<0,0,0><0,0.035000,0>0.127000 translate<14.097000,-1.535000,14.986000>}
box{<0,0,-0.127000><5.334000,0.035000,0.127000> rotate<0,0.000000,0> translate<8.763000,-1.535000,14.986000> }
cylinder{<0,0,0><0,0.035000,0>0.127000 translate<14.071600,-1.535000,20.675600>}
cylinder{<0,0,0><0,0.035000,0>0.127000 translate<14.579600,-1.535000,20.167600>}
box{<0,0,-0.127000><0.718420,0.035000,0.127000> rotate<0,44.997030,0> translate<14.071600,-1.535000,20.675600> }
cylinder{<0,0,0><0,0.035000,0>0.635000 translate<9.580000,0.000000,27.780000>}
cylinder{<0,0,0><0,0.035000,0>0.635000 translate<14.600000,0.000000,27.780000>}
box{<0,0,-0.635000><5.020000,0.035000,0.635000> rotate<0,0.000000,0> translate<9.580000,0.000000,27.780000> }
cylinder{<0,0,0><0,0.035000,0>0.127000 translate<12.270000,-1.535000,11.190000>}
cylinder{<0,0,0><0,0.035000,0>0.127000 translate<14.680000,-1.535000,13.600000>}
box{<0,0,-0.127000><3.408255,0.035000,0.127000> rotate<0,-44.997030,0> translate<12.270000,-1.535000,11.190000> }
cylinder{<0,0,0><0,0.035000,0>0.406400 translate<15.008000,0.000000,18.000000>}
cylinder{<0,0,0><0,0.035000,0>0.406400 translate<15.008000,0.000000,21.295200>}
box{<0,0,-0.406400><3.295200,0.035000,0.406400> rotate<0,90.000000,0> translate<15.008000,0.000000,21.295200> }
cylinder{<0,0,0><0,0.035000,0>0.127000 translate<14.680000,-1.535000,13.600000>}
cylinder{<0,0,0><0,0.035000,0>0.127000 translate<15.240000,-1.535000,13.600000>}
box{<0,0,-0.127000><0.560000,0.035000,0.127000> rotate<0,0.000000,0> translate<14.680000,-1.535000,13.600000> }
cylinder{<0,0,0><0,0.035000,0>0.127000 translate<15.240000,-1.535000,13.843000>}
cylinder{<0,0,0><0,0.035000,0>0.127000 translate<15.240000,-1.535000,13.600000>}
box{<0,0,-0.127000><0.243000,0.035000,0.127000> rotate<0,-90.000000,0> translate<15.240000,-1.535000,13.600000> }
cylinder{<0,0,0><0,0.035000,0>0.127000 translate<14.097000,-1.535000,14.986000>}
cylinder{<0,0,0><0,0.035000,0>0.127000 translate<15.240000,-1.535000,13.843000>}
box{<0,0,-0.127000><1.616446,0.035000,0.127000> rotate<0,44.997030,0> translate<14.097000,-1.535000,14.986000> }
cylinder{<0,0,0><0,0.035000,0>0.127000 translate<13.248000,-1.535000,8.128000>}
cylinder{<0,0,0><0,0.035000,0>0.127000 translate<15.494000,-1.535000,8.128000>}
box{<0,0,-0.127000><2.246000,0.035000,0.127000> rotate<0,0.000000,0> translate<13.248000,-1.535000,8.128000> }
cylinder{<0,0,0><0,0.035000,0>0.127000 translate<15.850000,-1.535000,13.600000>}
cylinder{<0,0,0><0,0.035000,0>0.127000 translate<15.850000,-1.535000,12.450000>}
box{<0,0,-0.127000><1.150000,0.035000,0.127000> rotate<0,-90.000000,0> translate<15.850000,-1.535000,12.450000> }
cylinder{<0,0,0><0,0.035000,0>0.127000 translate<15.240000,-1.535000,13.600000>}
cylinder{<0,0,0><0,0.035000,0>0.127000 translate<15.850000,-1.535000,13.600000>}
box{<0,0,-0.127000><0.610000,0.035000,0.127000> rotate<0,0.000000,0> translate<15.240000,-1.535000,13.600000> }
cylinder{<0,0,0><0,0.035000,0>0.127000 translate<15.550000,-1.535000,10.528600>}
cylinder{<0,0,0><0,0.035000,0>0.127000 translate<15.900000,-1.535000,10.100000>}
box{<0,0,-0.127000><0.553352,0.035000,0.127000> rotate<0,50.761154,0> translate<15.550000,-1.535000,10.528600> }
cylinder{<0,0,0><0,0.035000,0>0.127000 translate<15.494000,-1.535000,8.128000>}
cylinder{<0,0,0><0,0.035000,0>0.127000 translate<15.952000,-1.535000,7.670000>}
box{<0,0,-0.127000><0.647710,0.035000,0.127000> rotate<0,44.997030,0> translate<15.494000,-1.535000,8.128000> }
cylinder{<0,0,0><0,0.035000,0>0.127000 translate<15.850000,-1.535000,12.450000>}
cylinder{<0,0,0><0,0.035000,0>0.127000 translate<16.100000,-1.535000,12.200000>}
box{<0,0,-0.127000><0.353553,0.035000,0.127000> rotate<0,44.997030,0> translate<15.850000,-1.535000,12.450000> }
cylinder{<0,0,0><0,0.035000,0>0.127000 translate<15.900000,-1.535000,10.100000>}
cylinder{<0,0,0><0,0.035000,0>0.127000 translate<16.906800,-1.535000,9.093200>}
box{<0,0,-0.127000><1.423830,0.035000,0.127000> rotate<0,44.997030,0> translate<15.900000,-1.535000,10.100000> }
cylinder{<0,0,0><0,0.035000,0>0.127000 translate<10.900000,-1.535000,25.800000>}
cylinder{<0,0,0><0,0.035000,0>0.127000 translate<17.500000,-1.535000,25.800000>}
box{<0,0,-0.127000><6.600000,0.035000,0.127000> rotate<0,0.000000,0> translate<10.900000,-1.535000,25.800000> }
cylinder{<0,0,0><0,0.035000,0>0.127000 translate<16.100000,-1.535000,12.200000>}
cylinder{<0,0,0><0,0.035000,0>0.127000 translate<17.600000,-1.535000,12.200000>}
box{<0,0,-0.127000><1.500000,0.035000,0.127000> rotate<0,0.000000,0> translate<16.100000,-1.535000,12.200000> }
cylinder{<0,0,0><0,0.035000,0>0.127000 translate<17.500000,-1.535000,25.800000>}
cylinder{<0,0,0><0,0.035000,0>0.127000 translate<17.600000,-1.535000,25.900000>}
box{<0,0,-0.127000><0.141421,0.035000,0.127000> rotate<0,-44.997030,0> translate<17.500000,-1.535000,25.800000> }
cylinder{<0,0,0><0,0.035000,0>0.127000 translate<13.700000,0.000000,17.542000>}
cylinder{<0,0,0><0,0.035000,0>0.127000 translate<17.780000,0.000000,13.462000>}
box{<0,0,-0.127000><5.769991,0.035000,0.127000> rotate<0,44.997030,0> translate<13.700000,0.000000,17.542000> }
cylinder{<0,0,0><0,0.035000,0>0.127000 translate<17.450000,-1.535000,10.528600>}
cylinder{<0,0,0><0,0.035000,0>0.127000 translate<17.800000,-1.535000,10.850000>}
box{<0,0,-0.127000><0.475182,0.035000,0.127000> rotate<0,-42.558006,0> translate<17.450000,-1.535000,10.528600> }
cylinder{<0,0,0><0,0.035000,0>0.127000 translate<17.800000,-1.535000,12.000000>}
cylinder{<0,0,0><0,0.035000,0>0.127000 translate<17.800000,-1.535000,10.850000>}
box{<0,0,-0.127000><1.150000,0.035000,0.127000> rotate<0,-90.000000,0> translate<17.800000,-1.535000,10.850000> }
cylinder{<0,0,0><0,0.035000,0>0.127000 translate<17.600000,-1.535000,12.200000>}
cylinder{<0,0,0><0,0.035000,0>0.127000 translate<17.800000,-1.535000,12.000000>}
box{<0,0,-0.127000><0.282843,0.035000,0.127000> rotate<0,44.997030,0> translate<17.600000,-1.535000,12.200000> }
cylinder{<0,0,0><0,0.035000,0>0.635000 translate<14.600000,-1.535000,23.820000>}
cylinder{<0,0,0><0,0.035000,0>0.635000 translate<18.080000,-1.535000,23.820000>}
box{<0,0,-0.635000><3.480000,0.035000,0.635000> rotate<0,0.000000,0> translate<14.600000,-1.535000,23.820000> }
cylinder{<0,0,0><0,0.035000,0>0.635000 translate<14.600000,0.000000,27.780000>}
cylinder{<0,0,0><0,0.035000,0>0.635000 translate<18.120000,0.000000,27.780000>}
box{<0,0,-0.635000><3.520000,0.035000,0.635000> rotate<0,0.000000,0> translate<14.600000,0.000000,27.780000> }
cylinder{<0,0,0><0,0.035000,0>0.254000 translate<5.125000,-1.535000,4.400000>}
cylinder{<0,0,0><0,0.035000,0>0.254000 translate<18.170000,-1.535000,4.400000>}
box{<0,0,-0.254000><13.045000,0.035000,0.254000> rotate<0,0.000000,0> translate<5.125000,-1.535000,4.400000> }
cylinder{<0,0,0><0,0.035000,0>0.127000 translate<17.750000,-1.535000,13.600000>}
cylinder{<0,0,0><0,0.035000,0>0.127000 translate<18.300000,-1.535000,14.200000>}
box{<0,0,-0.127000><0.813941,0.035000,0.127000> rotate<0,-47.486419,0> translate<17.750000,-1.535000,13.600000> }
cylinder{<0,0,0><0,0.035000,0>0.127000 translate<16.906800,-1.535000,9.093200>}
cylinder{<0,0,0><0,0.035000,0>0.127000 translate<18.313400,-1.535000,9.093200>}
box{<0,0,-0.127000><1.406600,0.035000,0.127000> rotate<0,0.000000,0> translate<16.906800,-1.535000,9.093200> }
cylinder{<0,0,0><0,0.035000,0>0.127000 translate<18.313400,-1.535000,9.093200>}
cylinder{<0,0,0><0,0.035000,0>0.127000 translate<18.567400,-1.535000,8.839200>}
box{<0,0,-0.127000><0.359210,0.035000,0.127000> rotate<0,44.997030,0> translate<18.313400,-1.535000,9.093200> }
cylinder{<0,0,0><0,0.035000,0>0.127000 translate<17.600000,-1.535000,25.900000>}
cylinder{<0,0,0><0,0.035000,0>0.127000 translate<19.100000,-1.535000,25.900000>}
box{<0,0,-0.127000><1.500000,0.035000,0.127000> rotate<0,0.000000,0> translate<17.600000,-1.535000,25.900000> }
cylinder{<0,0,0><0,0.035000,0>0.635000 translate<18.080000,-1.535000,23.820000>}
cylinder{<0,0,0><0,0.035000,0>0.635000 translate<19.200000,-1.535000,22.700000>}
box{<0,0,-0.635000><1.583919,0.035000,0.635000> rotate<0,44.997030,0> translate<18.080000,-1.535000,23.820000> }
cylinder{<0,0,0><0,0.035000,0>0.127000 translate<18.300000,-1.535000,14.200000>}
cylinder{<0,0,0><0,0.035000,0>0.127000 translate<19.300000,-1.535000,14.200000>}
box{<0,0,-0.127000><1.000000,0.035000,0.127000> rotate<0,0.000000,0> translate<18.300000,-1.535000,14.200000> }
cylinder{<0,0,0><0,0.035000,0>0.127000 translate<13.000000,0.000000,16.200000>}
cylinder{<0,0,0><0,0.035000,0>0.127000 translate<19.400000,0.000000,9.800000>}
box{<0,0,-0.127000><9.050967,0.035000,0.127000> rotate<0,44.997030,0> translate<13.000000,0.000000,16.200000> }
cylinder{<0,0,0><0,0.035000,0>0.127000 translate<19.300000,-1.535000,14.200000>}
cylinder{<0,0,0><0,0.035000,0>0.127000 translate<19.400000,-1.535000,14.300000>}
box{<0,0,-0.127000><0.141421,0.035000,0.127000> rotate<0,-44.997030,0> translate<19.300000,-1.535000,14.200000> }
cylinder{<0,0,0><0,0.035000,0>0.127000 translate<17.780000,0.000000,13.462000>}
cylinder{<0,0,0><0,0.035000,0>0.127000 translate<19.738000,0.000000,13.462000>}
box{<0,0,-0.127000><1.958000,0.035000,0.127000> rotate<0,0.000000,0> translate<17.780000,0.000000,13.462000> }
cylinder{<0,0,0><0,0.035000,0>0.127000 translate<19.738000,0.000000,13.462000>}
cylinder{<0,0,0><0,0.035000,0>0.127000 translate<19.800000,0.000000,13.400000>}
box{<0,0,-0.127000><0.087681,0.035000,0.127000> rotate<0,44.997030,0> translate<19.738000,0.000000,13.462000> }
cylinder{<0,0,0><0,0.035000,0>0.127000 translate<19.100000,-1.535000,25.900000>}
cylinder{<0,0,0><0,0.035000,0>0.127000 translate<19.800000,-1.535000,25.850000>}
box{<0,0,-0.127000><0.701783,0.035000,0.127000> rotate<0,4.085347,0> translate<19.100000,-1.535000,25.900000> }
cylinder{<0,0,0><0,0.035000,0>0.127000 translate<19.400000,-1.535000,14.300000>}
cylinder{<0,0,0><0,0.035000,0>0.127000 translate<20.000000,-1.535000,14.300000>}
box{<0,0,-0.127000><0.600000,0.035000,0.127000> rotate<0,0.000000,0> translate<19.400000,-1.535000,14.300000> }
cylinder{<0,0,0><0,0.035000,0>0.127000 translate<20.000000,-1.535000,14.300000>}
cylinder{<0,0,0><0,0.035000,0>0.127000 translate<20.100000,-1.535000,14.400000>}
box{<0,0,-0.127000><0.141421,0.035000,0.127000> rotate<0,-44.997030,0> translate<20.000000,-1.535000,14.300000> }
cylinder{<0,0,0><0,0.035000,0>0.127000 translate<20.100000,-1.535000,14.400000>}
cylinder{<0,0,0><0,0.035000,0>0.127000 translate<20.100000,-1.535000,14.500000>}
box{<0,0,-0.127000><0.100000,0.035000,0.127000> rotate<0,90.000000,0> translate<20.100000,-1.535000,14.500000> }
cylinder{<0,0,0><0,0.035000,0>0.127000 translate<20.100000,-1.535000,14.500000>}
cylinder{<0,0,0><0,0.035000,0>0.127000 translate<20.200000,-1.535000,14.500000>}
box{<0,0,-0.127000><0.100000,0.035000,0.127000> rotate<0,0.000000,0> translate<20.100000,-1.535000,14.500000> }
cylinder{<0,0,0><0,0.035000,0>0.127000 translate<20.200000,-1.535000,14.500000>}
cylinder{<0,0,0><0,0.035000,0>0.127000 translate<20.300000,-1.535000,14.600000>}
box{<0,0,-0.127000><0.141421,0.035000,0.127000> rotate<0,-44.997030,0> translate<20.200000,-1.535000,14.500000> }
cylinder{<0,0,0><0,0.035000,0>0.127000 translate<18.567400,-1.535000,8.839200>}
cylinder{<0,0,0><0,0.035000,0>0.127000 translate<20.320000,-1.535000,8.839200>}
box{<0,0,-0.127000><1.752600,0.035000,0.127000> rotate<0,0.000000,0> translate<18.567400,-1.535000,8.839200> }
cylinder{<0,0,0><0,0.035000,0>0.127000 translate<19.800000,-1.535000,27.750000>}
cylinder{<0,0,0><0,0.035000,0>0.127000 translate<20.500000,-1.535000,27.800000>}
box{<0,0,-0.127000><0.701783,0.035000,0.127000> rotate<0,-4.085347,0> translate<19.800000,-1.535000,27.750000> }
cylinder{<0,0,0><0,0.035000,0>0.127000 translate<20.320000,-1.535000,8.839200>}
cylinder{<0,0,0><0,0.035000,0>0.127000 translate<20.580800,-1.535000,9.100000>}
box{<0,0,-0.127000><0.368827,0.035000,0.127000> rotate<0,-44.997030,0> translate<20.320000,-1.535000,8.839200> }
cylinder{<0,0,0><0,0.035000,0>0.127000 translate<12.270000,0.000000,13.730000>}
cylinder{<0,0,0><0,0.035000,0>0.127000 translate<20.900000,0.000000,5.100000>}
box{<0,0,-0.127000><12.204663,0.035000,0.127000> rotate<0,44.997030,0> translate<12.270000,0.000000,13.730000> }
cylinder{<0,0,0><0,0.035000,0>0.127000 translate<20.900000,-1.535000,9.800000>}
cylinder{<0,0,0><0,0.035000,0>0.127000 translate<20.900000,-1.535000,9.700000>}
box{<0,0,-0.127000><0.100000,0.035000,0.127000> rotate<0,-90.000000,0> translate<20.900000,-1.535000,9.700000> }
cylinder{<0,0,0><0,0.035000,0>0.127000 translate<19.400000,-1.535000,9.800000>}
cylinder{<0,0,0><0,0.035000,0>0.127000 translate<20.900000,-1.535000,9.800000>}
box{<0,0,-0.127000><1.500000,0.035000,0.127000> rotate<0,0.000000,0> translate<19.400000,-1.535000,9.800000> }
cylinder{<0,0,0><0,0.035000,0>0.127000 translate<19.800000,-1.535000,13.400000>}
cylinder{<0,0,0><0,0.035000,0>0.127000 translate<21.200000,-1.535000,13.400000>}
box{<0,0,-0.127000><1.400000,0.035000,0.127000> rotate<0,0.000000,0> translate<19.800000,-1.535000,13.400000> }
cylinder{<0,0,0><0,0.035000,0>0.635000 translate<21.700000,0.000000,10.500000>}
cylinder{<0,0,0><0,0.035000,0>0.635000 translate<21.700000,0.000000,14.300000>}
box{<0,0,-0.635000><3.800000,0.035000,0.635000> rotate<0,90.000000,0> translate<21.700000,0.000000,14.300000> }
cylinder{<0,0,0><0,0.035000,0>0.635000 translate<18.120000,0.000000,27.780000>}
cylinder{<0,0,0><0,0.035000,0>0.635000 translate<21.700000,0.000000,24.200000>}
box{<0,0,-0.635000><5.062885,0.035000,0.635000> rotate<0,44.997030,0> translate<18.120000,0.000000,27.780000> }
cylinder{<0,0,0><0,0.035000,0>0.635000 translate<21.700000,0.000000,14.300000>}
cylinder{<0,0,0><0,0.035000,0>0.635000 translate<21.700000,0.000000,24.200000>}
box{<0,0,-0.635000><9.900000,0.035000,0.635000> rotate<0,90.000000,0> translate<21.700000,0.000000,24.200000> }
cylinder{<0,0,0><0,0.035000,0>0.127000 translate<20.500000,-1.535000,27.800000>}
cylinder{<0,0,0><0,0.035000,0>0.127000 translate<21.800000,-1.535000,27.800000>}
box{<0,0,-0.127000><1.300000,0.035000,0.127000> rotate<0,0.000000,0> translate<20.500000,-1.535000,27.800000> }
cylinder{<0,0,0><0,0.035000,0>0.127000 translate<21.900000,-1.535000,27.700000>}
cylinder{<0,0,0><0,0.035000,0>0.127000 translate<21.900000,-1.535000,27.600000>}
box{<0,0,-0.127000><0.100000,0.035000,0.127000> rotate<0,-90.000000,0> translate<21.900000,-1.535000,27.600000> }
cylinder{<0,0,0><0,0.035000,0>0.127000 translate<21.800000,-1.535000,27.800000>}
cylinder{<0,0,0><0,0.035000,0>0.127000 translate<21.900000,-1.535000,27.700000>}
box{<0,0,-0.127000><0.141421,0.035000,0.127000> rotate<0,44.997030,0> translate<21.800000,-1.535000,27.800000> }
cylinder{<0,0,0><0,0.035000,0>0.127000 translate<15.952000,-1.535000,7.670000>}
cylinder{<0,0,0><0,0.035000,0>0.127000 translate<21.920000,-1.535000,7.670000>}
box{<0,0,-0.127000><5.968000,0.035000,0.127000> rotate<0,0.000000,0> translate<15.952000,-1.535000,7.670000> }
cylinder{<0,0,0><0,0.035000,0>0.127000 translate<22.000000,-1.535000,27.500000>}
cylinder{<0,0,0><0,0.035000,0>0.127000 translate<22.000000,-1.535000,27.400000>}
box{<0,0,-0.127000><0.100000,0.035000,0.127000> rotate<0,-90.000000,0> translate<22.000000,-1.535000,27.400000> }
cylinder{<0,0,0><0,0.035000,0>0.127000 translate<21.900000,-1.535000,27.600000>}
cylinder{<0,0,0><0,0.035000,0>0.127000 translate<22.000000,-1.535000,27.500000>}
box{<0,0,-0.127000><0.141421,0.035000,0.127000> rotate<0,44.997030,0> translate<21.900000,-1.535000,27.600000> }
cylinder{<0,0,0><0,0.035000,0>0.127000 translate<21.200000,-1.535000,13.400000>}
cylinder{<0,0,0><0,0.035000,0>0.127000 translate<22.020000,-1.535000,13.370000>}
box{<0,0,-0.127000><0.820549,0.035000,0.127000> rotate<0,2.095114,0> translate<21.200000,-1.535000,13.400000> }
cylinder{<0,0,0><0,0.035000,0>0.127000 translate<22.100000,-1.535000,27.300000>}
cylinder{<0,0,0><0,0.035000,0>0.127000 translate<22.100000,-1.535000,27.200000>}
box{<0,0,-0.127000><0.100000,0.035000,0.127000> rotate<0,-90.000000,0> translate<22.100000,-1.535000,27.200000> }
cylinder{<0,0,0><0,0.035000,0>0.127000 translate<22.000000,-1.535000,27.400000>}
cylinder{<0,0,0><0,0.035000,0>0.127000 translate<22.100000,-1.535000,27.300000>}
box{<0,0,-0.127000><0.141421,0.035000,0.127000> rotate<0,44.997030,0> translate<22.000000,-1.535000,27.400000> }
cylinder{<0,0,0><0,0.035000,0>0.635000 translate<21.700000,0.000000,10.500000>}
cylinder{<0,0,0><0,0.035000,0>0.635000 translate<23.000000,0.000000,9.200000>}
box{<0,0,-0.635000><1.838478,0.035000,0.635000> rotate<0,44.997030,0> translate<21.700000,0.000000,10.500000> }
cylinder{<0,0,0><0,0.035000,0>0.127000 translate<22.100000,-1.535000,27.200000>}
cylinder{<0,0,0><0,0.035000,0>0.127000 translate<23.100000,-1.535000,27.200000>}
box{<0,0,-0.127000><1.000000,0.035000,0.127000> rotate<0,0.000000,0> translate<22.100000,-1.535000,27.200000> }
cylinder{<0,0,0><0,0.035000,0>0.127000 translate<20.580800,-1.535000,9.100000>}
cylinder{<0,0,0><0,0.035000,0>0.127000 translate<23.400000,-1.535000,9.100000>}
box{<0,0,-0.127000><2.819200,0.035000,0.127000> rotate<0,0.000000,0> translate<20.580800,-1.535000,9.100000> }
cylinder{<0,0,0><0,0.035000,0>0.127000 translate<23.400000,-1.535000,9.000000>}
cylinder{<0,0,0><0,0.035000,0>0.127000 translate<23.400000,-1.535000,9.100000>}
box{<0,0,-0.127000><0.100000,0.035000,0.127000> rotate<0,90.000000,0> translate<23.400000,-1.535000,9.100000> }
cylinder{<0,0,0><0,0.035000,0>0.127000 translate<23.400000,-1.535000,9.100000>}
cylinder{<0,0,0><0,0.035000,0>0.127000 translate<23.600000,-1.535000,9.100000>}
box{<0,0,-0.127000><0.200000,0.035000,0.127000> rotate<0,0.000000,0> translate<23.400000,-1.535000,9.100000> }
cylinder{<0,0,0><0,0.035000,0>0.127000 translate<23.700000,-1.535000,9.000000>}
cylinder{<0,0,0><0,0.035000,0>0.127000 translate<23.700000,-1.535000,8.900000>}
box{<0,0,-0.127000><0.100000,0.035000,0.127000> rotate<0,-90.000000,0> translate<23.700000,-1.535000,8.900000> }
cylinder{<0,0,0><0,0.035000,0>0.127000 translate<23.600000,-1.535000,9.100000>}
cylinder{<0,0,0><0,0.035000,0>0.127000 translate<23.700000,-1.535000,9.000000>}
box{<0,0,-0.127000><0.141421,0.035000,0.127000> rotate<0,44.997030,0> translate<23.600000,-1.535000,9.100000> }
cylinder{<0,0,0><0,0.035000,0>0.127000 translate<23.800000,-1.535000,8.800000>}
cylinder{<0,0,0><0,0.035000,0>0.127000 translate<23.800000,-1.535000,8.200000>}
box{<0,0,-0.127000><0.600000,0.035000,0.127000> rotate<0,-90.000000,0> translate<23.800000,-1.535000,8.200000> }
cylinder{<0,0,0><0,0.035000,0>0.127000 translate<23.700000,-1.535000,8.900000>}
cylinder{<0,0,0><0,0.035000,0>0.127000 translate<23.800000,-1.535000,8.800000>}
box{<0,0,-0.127000><0.141421,0.035000,0.127000> rotate<0,44.997030,0> translate<23.700000,-1.535000,8.900000> }
cylinder{<0,0,0><0,0.035000,0>0.127000 translate<20.300000,-1.535000,14.600000>}
cylinder{<0,0,0><0,0.035000,0>0.127000 translate<23.800000,-1.535000,14.600000>}
box{<0,0,-0.127000><3.500000,0.035000,0.127000> rotate<0,0.000000,0> translate<20.300000,-1.535000,14.600000> }
cylinder{<0,0,0><0,0.035000,0>0.635000 translate<21.700000,0.000000,14.300000>}
cylinder{<0,0,0><0,0.035000,0>0.635000 translate<23.850000,0.000000,14.300000>}
box{<0,0,-0.635000><2.150000,0.035000,0.635000> rotate<0,0.000000,0> translate<21.700000,0.000000,14.300000> }
cylinder{<0,0,0><0,0.035000,0>0.127000 translate<23.100000,-1.535000,27.200000>}
cylinder{<0,0,0><0,0.035000,0>0.127000 translate<23.860000,-1.535000,26.400000>}
box{<0,0,-0.127000><1.103449,0.035000,0.127000> rotate<0,46.465734,0> translate<23.100000,-1.535000,27.200000> }
cylinder{<0,0,0><0,0.035000,0>0.127000 translate<23.800000,-1.535000,8.200000>}
cylinder{<0,0,0><0,0.035000,0>0.127000 translate<23.900000,-1.535000,8.100000>}
box{<0,0,-0.127000><0.141421,0.035000,0.127000> rotate<0,44.997030,0> translate<23.800000,-1.535000,8.200000> }
cylinder{<0,0,0><0,0.035000,0>0.127000 translate<23.900000,-1.535000,14.500000>}
cylinder{<0,0,0><0,0.035000,0>0.127000 translate<23.900000,-1.535000,13.900000>}
box{<0,0,-0.127000><0.600000,0.035000,0.127000> rotate<0,-90.000000,0> translate<23.900000,-1.535000,13.900000> }
cylinder{<0,0,0><0,0.035000,0>0.127000 translate<23.800000,-1.535000,14.600000>}
cylinder{<0,0,0><0,0.035000,0>0.127000 translate<23.900000,-1.535000,14.500000>}
box{<0,0,-0.127000><0.141421,0.035000,0.127000> rotate<0,44.997030,0> translate<23.800000,-1.535000,14.600000> }
cylinder{<0,0,0><0,0.035000,0>0.127000 translate<23.900000,-1.535000,8.100000>}
cylinder{<0,0,0><0,0.035000,0>0.127000 translate<24.000000,-1.535000,8.100000>}
box{<0,0,-0.127000><0.100000,0.035000,0.127000> rotate<0,0.000000,0> translate<23.900000,-1.535000,8.100000> }
cylinder{<0,0,0><0,0.035000,0>0.127000 translate<20.900000,-1.535000,9.700000>}
cylinder{<0,0,0><0,0.035000,0>0.127000 translate<24.000000,-1.535000,9.700000>}
box{<0,0,-0.127000><3.100000,0.035000,0.127000> rotate<0,0.000000,0> translate<20.900000,-1.535000,9.700000> }
cylinder{<0,0,0><0,0.035000,0>0.127000 translate<23.900000,-1.535000,13.900000>}
cylinder{<0,0,0><0,0.035000,0>0.127000 translate<24.000000,-1.535000,13.800000>}
box{<0,0,-0.127000><0.141421,0.035000,0.127000> rotate<0,44.997030,0> translate<23.900000,-1.535000,13.900000> }
cylinder{<0,0,0><0,0.035000,0>0.635000 translate<23.850000,0.000000,14.300000>}
cylinder{<0,0,0><0,0.035000,0>0.635000 translate<24.050000,0.000000,14.500000>}
box{<0,0,-0.635000><0.282843,0.035000,0.635000> rotate<0,-44.997030,0> translate<23.850000,0.000000,14.300000> }
cylinder{<0,0,0><0,0.035000,0>0.127000 translate<24.000000,-1.535000,8.100000>}
cylinder{<0,0,0><0,0.035000,0>0.127000 translate<24.100000,-1.535000,8.000000>}
box{<0,0,-0.127000><0.141421,0.035000,0.127000> rotate<0,44.997030,0> translate<24.000000,-1.535000,8.100000> }
cylinder{<0,0,0><0,0.035000,0>0.127000 translate<24.000000,-1.535000,9.700000>}
cylinder{<0,0,0><0,0.035000,0>0.127000 translate<24.100000,-1.535000,9.800000>}
box{<0,0,-0.127000><0.141421,0.035000,0.127000> rotate<0,-44.997030,0> translate<24.000000,-1.535000,9.700000> }
cylinder{<0,0,0><0,0.035000,0>0.127000 translate<24.000000,-1.535000,13.800000>}
cylinder{<0,0,0><0,0.035000,0>0.127000 translate<24.100000,-1.535000,13.800000>}
box{<0,0,-0.127000><0.100000,0.035000,0.127000> rotate<0,0.000000,0> translate<24.000000,-1.535000,13.800000> }
cylinder{<0,0,0><0,0.035000,0>0.127000 translate<24.100000,-1.535000,8.000000>}
cylinder{<0,0,0><0,0.035000,0>0.127000 translate<24.200000,-1.535000,8.000000>}
box{<0,0,-0.127000><0.100000,0.035000,0.127000> rotate<0,0.000000,0> translate<24.100000,-1.535000,8.000000> }
cylinder{<0,0,0><0,0.035000,0>0.127000 translate<24.100000,-1.535000,13.800000>}
cylinder{<0,0,0><0,0.035000,0>0.127000 translate<24.200000,-1.535000,13.700000>}
box{<0,0,-0.127000><0.141421,0.035000,0.127000> rotate<0,44.997030,0> translate<24.100000,-1.535000,13.800000> }
cylinder{<0,0,0><0,0.035000,0>0.127000 translate<24.200000,-1.535000,8.000000>}
cylinder{<0,0,0><0,0.035000,0>0.127000 translate<24.300000,-1.535000,7.900000>}
box{<0,0,-0.127000><0.141421,0.035000,0.127000> rotate<0,44.997030,0> translate<24.200000,-1.535000,8.000000> }
cylinder{<0,0,0><0,0.035000,0>0.127000 translate<24.200000,-1.535000,13.700000>}
cylinder{<0,0,0><0,0.035000,0>0.127000 translate<24.300000,-1.535000,13.700000>}
box{<0,0,-0.127000><0.100000,0.035000,0.127000> rotate<0,0.000000,0> translate<24.200000,-1.535000,13.700000> }
cylinder{<0,0,0><0,0.035000,0>0.127000 translate<24.300000,-1.535000,7.900000>}
cylinder{<0,0,0><0,0.035000,0>0.127000 translate<24.400000,-1.535000,7.900000>}
box{<0,0,-0.127000><0.100000,0.035000,0.127000> rotate<0,0.000000,0> translate<24.300000,-1.535000,7.900000> }
cylinder{<0,0,0><0,0.035000,0>0.127000 translate<24.300000,-1.535000,13.700000>}
cylinder{<0,0,0><0,0.035000,0>0.127000 translate<24.400000,-1.535000,13.600000>}
box{<0,0,-0.127000><0.141421,0.035000,0.127000> rotate<0,44.997030,0> translate<24.300000,-1.535000,13.700000> }
cylinder{<0,0,0><0,0.035000,0>0.127000 translate<24.400000,-1.535000,7.900000>}
cylinder{<0,0,0><0,0.035000,0>0.127000 translate<24.500000,-1.535000,7.800000>}
box{<0,0,-0.127000><0.141421,0.035000,0.127000> rotate<0,44.997030,0> translate<24.400000,-1.535000,7.900000> }
cylinder{<0,0,0><0,0.035000,0>0.127000 translate<24.400000,-1.535000,13.600000>}
cylinder{<0,0,0><0,0.035000,0>0.127000 translate<24.500000,-1.535000,13.600000>}
box{<0,0,-0.127000><0.100000,0.035000,0.127000> rotate<0,0.000000,0> translate<24.400000,-1.535000,13.600000> }
cylinder{<0,0,0><0,0.035000,0>0.127000 translate<24.500000,-1.535000,7.800000>}
cylinder{<0,0,0><0,0.035000,0>0.127000 translate<24.600000,-1.535000,7.800000>}
box{<0,0,-0.127000><0.100000,0.035000,0.127000> rotate<0,0.000000,0> translate<24.500000,-1.535000,7.800000> }
cylinder{<0,0,0><0,0.035000,0>0.127000 translate<24.500000,-1.535000,13.600000>}
cylinder{<0,0,0><0,0.035000,0>0.127000 translate<24.600000,-1.535000,13.500000>}
box{<0,0,-0.127000><0.141421,0.035000,0.127000> rotate<0,44.997030,0> translate<24.500000,-1.535000,13.600000> }
cylinder{<0,0,0><0,0.035000,0>0.127000 translate<24.600000,-1.535000,7.800000>}
cylinder{<0,0,0><0,0.035000,0>0.127000 translate<24.700000,-1.535000,7.700000>}
box{<0,0,-0.127000><0.141421,0.035000,0.127000> rotate<0,44.997030,0> translate<24.600000,-1.535000,7.800000> }
cylinder{<0,0,0><0,0.035000,0>0.127000 translate<24.600000,-1.535000,13.500000>}
cylinder{<0,0,0><0,0.035000,0>0.127000 translate<24.700000,-1.535000,13.500000>}
box{<0,0,-0.127000><0.100000,0.035000,0.127000> rotate<0,0.000000,0> translate<24.600000,-1.535000,13.500000> }
cylinder{<0,0,0><0,0.035000,0>0.127000 translate<24.700000,-1.535000,13.500000>}
cylinder{<0,0,0><0,0.035000,0>0.127000 translate<24.800000,-1.535000,13.400000>}
box{<0,0,-0.127000><0.141421,0.035000,0.127000> rotate<0,44.997030,0> translate<24.700000,-1.535000,13.500000> }
cylinder{<0,0,0><0,0.035000,0>0.127000 translate<20.900000,0.000000,5.100000>}
cylinder{<0,0,0><0,0.035000,0>0.127000 translate<25.100000,0.000000,5.100000>}
box{<0,0,-0.127000><4.200000,0.035000,0.127000> rotate<0,0.000000,0> translate<20.900000,0.000000,5.100000> }
cylinder{<0,0,0><0,0.035000,0>0.127000 translate<14.097000,0.000000,2.032000>}
cylinder{<0,0,0><0,0.035000,0>0.127000 translate<25.527000,0.000000,2.032000>}
box{<0,0,-0.127000><11.430000,0.035000,0.127000> rotate<0,0.000000,0> translate<14.097000,0.000000,2.032000> }
cylinder{<0,0,0><0,0.035000,0>0.635000 translate<26.400000,0.000000,22.100000>}
cylinder{<0,0,0><0,0.035000,0>0.635000 translate<26.400000,0.000000,26.400000>}
box{<0,0,-0.635000><4.300000,0.035000,0.635000> rotate<0,90.000000,0> translate<26.400000,0.000000,26.400000> }
cylinder{<0,0,0><0,0.035000,0>0.635000 translate<23.000000,0.000000,9.200000>}
cylinder{<0,0,0><0,0.035000,0>0.635000 translate<26.800000,0.000000,9.200000>}
box{<0,0,-0.635000><3.800000,0.035000,0.635000> rotate<0,0.000000,0> translate<23.000000,0.000000,9.200000> }
cylinder{<0,0,0><0,0.035000,0>0.127000 translate<25.527000,0.000000,2.032000>}
cylinder{<0,0,0><0,0.035000,0>0.127000 translate<27.305000,0.000000,3.810000>}
box{<0,0,-0.127000><2.514472,0.035000,0.127000> rotate<0,-44.997030,0> translate<25.527000,0.000000,2.032000> }
cylinder{<0,0,0><0,0.035000,0>0.127000 translate<26.800000,0.000000,9.200000>}
cylinder{<0,0,0><0,0.035000,0>0.127000 translate<27.305000,0.000000,8.695000>}
box{<0,0,-0.127000><0.714178,0.035000,0.127000> rotate<0,44.997030,0> translate<26.800000,0.000000,9.200000> }
cylinder{<0,0,0><0,0.035000,0>0.127000 translate<27.305000,0.000000,3.810000>}
cylinder{<0,0,0><0,0.035000,0>0.127000 translate<27.305000,0.000000,8.695000>}
box{<0,0,-0.127000><4.885000,0.035000,0.127000> rotate<0,90.000000,0> translate<27.305000,0.000000,8.695000> }
cylinder{<0,0,0><0,0.035000,0>0.635000 translate<26.400000,0.000000,22.100000>}
cylinder{<0,0,0><0,0.035000,0>0.635000 translate<27.700000,0.000000,20.800000>}
box{<0,0,-0.635000><1.838478,0.035000,0.635000> rotate<0,44.997030,0> translate<26.400000,0.000000,22.100000> }
cylinder{<0,0,0><0,0.035000,0>0.635000 translate<19.200000,-1.535000,22.700000>}
cylinder{<0,0,0><0,0.035000,0>0.635000 translate<28.200000,-1.535000,22.700000>}
box{<0,0,-0.635000><9.000000,0.035000,0.635000> rotate<0,0.000000,0> translate<19.200000,-1.535000,22.700000> }
cylinder{<0,0,0><0,0.035000,0>0.127000 translate<14.579600,-1.535000,20.167600>}
cylinder{<0,0,0><0,0.035000,0>0.127000 translate<28.362875,-1.535000,20.167600>}
box{<0,0,-0.127000><13.783275,0.035000,0.127000> rotate<0,0.000000,0> translate<14.579600,-1.535000,20.167600> }
cylinder{<0,0,0><0,0.035000,0>0.254000 translate<27.100000,0.000000,16.100000>}
cylinder{<0,0,0><0,0.035000,0>0.254000 translate<28.858000,0.000000,16.100000>}
box{<0,0,-0.254000><1.758000,0.035000,0.254000> rotate<0,0.000000,0> translate<27.100000,0.000000,16.100000> }
cylinder{<0,0,0><0,0.035000,0>0.635000 translate<28.200000,-1.535000,22.700000>}
cylinder{<0,0,0><0,0.035000,0>0.635000 translate<28.900000,-1.535000,23.400000>}
box{<0,0,-0.635000><0.989949,0.035000,0.635000> rotate<0,-44.997030,0> translate<28.200000,-1.535000,22.700000> }
cylinder{<0,0,0><0,0.035000,0>0.635000 translate<28.900000,-1.535000,23.400000>}
cylinder{<0,0,0><0,0.035000,0>0.635000 translate<28.900000,-1.535000,26.360000>}
box{<0,0,-0.635000><2.960000,0.035000,0.635000> rotate<0,90.000000,0> translate<28.900000,-1.535000,26.360000> }
cylinder{<0,0,0><0,0.035000,0>0.635000 translate<28.900000,-1.535000,26.360000>}
cylinder{<0,0,0><0,0.035000,0>0.635000 translate<28.940000,-1.535000,26.400000>}
box{<0,0,-0.635000><0.056569,0.035000,0.635000> rotate<0,-44.997030,0> translate<28.900000,-1.535000,26.360000> }
cylinder{<0,0,0><0,0.035000,0>0.127000 translate<28.362875,-1.535000,20.167600>}
cylinder{<0,0,0><0,0.035000,0>0.127000 translate<29.144769,-1.535000,20.949494>}
box{<0,0,-0.127000><1.105765,0.035000,0.127000> rotate<0,-44.997030,0> translate<28.362875,-1.535000,20.167600> }
cylinder{<0,0,0><0,0.035000,0>0.127000 translate<28.778200,-1.535000,20.599400>}
cylinder{<0,0,0><0,0.035000,0>0.127000 translate<29.144769,-1.535000,20.949494>}
box{<0,0,-0.127000><0.506891,0.035000,0.127000> rotate<0,-43.680205,0> translate<28.778200,-1.535000,20.599400> }
cylinder{<0,0,0><0,0.035000,0>0.254000 translate<28.700000,0.000000,14.500000>}
cylinder{<0,0,0><0,0.035000,0>0.254000 translate<29.150000,0.000000,14.500000>}
box{<0,0,-0.254000><0.450000,0.035000,0.254000> rotate<0,0.000000,0> translate<28.700000,0.000000,14.500000> }
cylinder{<0,0,0><0,0.035000,0>0.254000 translate<29.150000,0.000000,14.500000>}
cylinder{<0,0,0><0,0.035000,0>0.254000 translate<29.210000,0.000000,14.560000>}
box{<0,0,-0.254000><0.084853,0.035000,0.254000> rotate<0,-44.997030,0> translate<29.150000,0.000000,14.500000> }
cylinder{<0,0,0><0,0.035000,0>0.254000 translate<29.210000,0.000000,15.748000>}
cylinder{<0,0,0><0,0.035000,0>0.254000 translate<29.210000,0.000000,14.560000>}
box{<0,0,-0.254000><1.188000,0.035000,0.254000> rotate<0,-90.000000,0> translate<29.210000,0.000000,14.560000> }
cylinder{<0,0,0><0,0.035000,0>0.254000 translate<28.858000,0.000000,16.100000>}
cylinder{<0,0,0><0,0.035000,0>0.254000 translate<29.210000,0.000000,15.748000>}
box{<0,0,-0.254000><0.497803,0.035000,0.254000> rotate<0,44.997030,0> translate<28.858000,0.000000,16.100000> }
cylinder{<0,0,0><0,0.035000,0>0.254000 translate<29.210000,0.000000,15.748000>}
cylinder{<0,0,0><0,0.035000,0>0.254000 translate<29.210000,0.000000,16.002000>}
box{<0,0,-0.254000><0.254000,0.035000,0.254000> rotate<0,90.000000,0> translate<29.210000,0.000000,16.002000> }
cylinder{<0,0,0><0,0.035000,0>0.127000 translate<24.100000,-1.535000,9.800000>}
cylinder{<0,0,0><0,0.035000,0>0.127000 translate<29.500000,-1.535000,9.800000>}
box{<0,0,-0.127000><5.400000,0.035000,0.127000> rotate<0,0.000000,0> translate<24.100000,-1.535000,9.800000> }
cylinder{<0,0,0><0,0.035000,0>0.127000 translate<29.500000,-1.535000,9.800000>}
cylinder{<0,0,0><0,0.035000,0>0.127000 translate<29.600000,-1.535000,9.900000>}
box{<0,0,-0.127000><0.141421,0.035000,0.127000> rotate<0,-44.997030,0> translate<29.500000,-1.535000,9.800000> }
cylinder{<0,0,0><0,0.035000,0>0.127000 translate<29.600000,-1.535000,9.900000>}
cylinder{<0,0,0><0,0.035000,0>0.127000 translate<29.700000,-1.535000,9.900000>}
box{<0,0,-0.127000><0.100000,0.035000,0.127000> rotate<0,0.000000,0> translate<29.600000,-1.535000,9.900000> }
cylinder{<0,0,0><0,0.035000,0>0.127000 translate<29.700000,-1.535000,9.900000>}
cylinder{<0,0,0><0,0.035000,0>0.127000 translate<29.800000,-1.535000,10.000000>}
box{<0,0,-0.127000><0.141421,0.035000,0.127000> rotate<0,-44.997030,0> translate<29.700000,-1.535000,9.900000> }
cylinder{<0,0,0><0,0.035000,0>0.127000 translate<29.800000,-1.535000,10.000000>}
cylinder{<0,0,0><0,0.035000,0>0.127000 translate<29.900000,-1.535000,10.000000>}
box{<0,0,-0.127000><0.100000,0.035000,0.127000> rotate<0,0.000000,0> translate<29.800000,-1.535000,10.000000> }
cylinder{<0,0,0><0,0.035000,0>0.127000 translate<29.900000,-1.535000,10.000000>}
cylinder{<0,0,0><0,0.035000,0>0.127000 translate<30.000000,-1.535000,10.100000>}
box{<0,0,-0.127000><0.141421,0.035000,0.127000> rotate<0,-44.997030,0> translate<29.900000,-1.535000,10.000000> }
cylinder{<0,0,0><0,0.035000,0>0.127000 translate<30.000000,-1.535000,10.100000>}
cylinder{<0,0,0><0,0.035000,0>0.127000 translate<30.100000,-1.535000,10.100000>}
box{<0,0,-0.127000><0.100000,0.035000,0.127000> rotate<0,0.000000,0> translate<30.000000,-1.535000,10.100000> }
cylinder{<0,0,0><0,0.035000,0>0.127000 translate<30.100000,-1.535000,10.100000>}
cylinder{<0,0,0><0,0.035000,0>0.127000 translate<30.200000,-1.535000,10.200000>}
box{<0,0,-0.127000><0.141421,0.035000,0.127000> rotate<0,-44.997030,0> translate<30.100000,-1.535000,10.100000> }
cylinder{<0,0,0><0,0.035000,0>0.127000 translate<30.200000,-1.535000,10.200000>}
cylinder{<0,0,0><0,0.035000,0>0.127000 translate<30.200000,-1.535000,10.700000>}
box{<0,0,-0.127000><0.500000,0.035000,0.127000> rotate<0,90.000000,0> translate<30.200000,-1.535000,10.700000> }
cylinder{<0,0,0><0,0.035000,0>0.127000 translate<30.200000,-1.535000,10.700000>}
cylinder{<0,0,0><0,0.035000,0>0.127000 translate<30.300000,-1.535000,10.800000>}
box{<0,0,-0.127000><0.141421,0.035000,0.127000> rotate<0,-44.997030,0> translate<30.200000,-1.535000,10.700000> }
cylinder{<0,0,0><0,0.035000,0>0.127000 translate<29.144769,-1.535000,20.949494>}
cylinder{<0,0,0><0,0.035000,0>0.127000 translate<31.038800,-1.535000,22.758400>}
box{<0,0,-0.127000><2.619064,0.035000,0.127000> rotate<0,-43.680205,0> translate<29.144769,-1.535000,20.949494> }
cylinder{<0,0,0><0,0.035000,0>0.127000 translate<25.100000,-1.535000,5.100000>}
cylinder{<0,0,0><0,0.035000,0>0.127000 translate<31.200000,-1.535000,5.100000>}
box{<0,0,-0.127000><6.100000,0.035000,0.127000> rotate<0,0.000000,0> translate<25.100000,-1.535000,5.100000> }
cylinder{<0,0,0><0,0.035000,0>0.127000 translate<24.700000,-1.535000,7.700000>}
cylinder{<0,0,0><0,0.035000,0>0.127000 translate<31.200000,-1.535000,7.700000>}
box{<0,0,-0.127000><6.500000,0.035000,0.127000> rotate<0,0.000000,0> translate<24.700000,-1.535000,7.700000> }
cylinder{<0,0,0><0,0.035000,0>0.635000 translate<27.700000,0.000000,20.800000>}
cylinder{<0,0,0><0,0.035000,0>0.635000 translate<31.270000,0.000000,20.800000>}
box{<0,0,-0.635000><3.570000,0.035000,0.635000> rotate<0,0.000000,0> translate<27.700000,0.000000,20.800000> }
cylinder{<0,0,0><0,0.035000,0>0.127000 translate<30.300000,-1.535000,10.800000>}
cylinder{<0,0,0><0,0.035000,0>0.127000 translate<31.300000,-1.535000,10.800000>}
box{<0,0,-0.127000><1.000000,0.035000,0.127000> rotate<0,0.000000,0> translate<30.300000,-1.535000,10.800000> }
cylinder{<0,0,0><0,0.035000,0>0.127000 translate<24.800000,-1.535000,13.400000>}
cylinder{<0,0,0><0,0.035000,0>0.127000 translate<31.300000,-1.535000,13.400000>}
box{<0,0,-0.127000><6.500000,0.035000,0.127000> rotate<0,0.000000,0> translate<24.800000,-1.535000,13.400000> }
cylinder{<0,0,0><0,0.035000,0>0.127000 translate<31.200000,-1.535000,5.100000>}
cylinder{<0,0,0><0,0.035000,0>0.127000 translate<32.080000,-1.535000,5.130000>}
box{<0,0,-0.127000><0.880511,0.035000,0.127000> rotate<0,-1.952380,0> translate<31.200000,-1.535000,5.100000> }
cylinder{<0,0,0><0,0.035000,0>0.127000 translate<31.200000,-1.535000,7.700000>}
cylinder{<0,0,0><0,0.035000,0>0.127000 translate<32.080000,-1.535000,7.670000>}
box{<0,0,-0.127000><0.880511,0.035000,0.127000> rotate<0,1.952380,0> translate<31.200000,-1.535000,7.700000> }
cylinder{<0,0,0><0,0.035000,0>0.127000 translate<31.300000,-1.535000,10.800000>}
cylinder{<0,0,0><0,0.035000,0>0.127000 translate<32.180000,-1.535000,10.830000>}
box{<0,0,-0.127000><0.880511,0.035000,0.127000> rotate<0,-1.952380,0> translate<31.300000,-1.535000,10.800000> }
cylinder{<0,0,0><0,0.035000,0>0.127000 translate<31.300000,-1.535000,13.400000>}
cylinder{<0,0,0><0,0.035000,0>0.127000 translate<32.180000,-1.535000,13.370000>}
box{<0,0,-0.127000><0.880511,0.035000,0.127000> rotate<0,1.952380,0> translate<31.300000,-1.535000,13.400000> }
cylinder{<0,0,0><0,0.035000,0>0.635000 translate<28.940000,0.000000,26.400000>}
cylinder{<0,0,0><0,0.035000,0>0.635000 translate<32.540000,0.000000,22.800000>}
box{<0,0,-0.635000><5.091169,0.035000,0.635000> rotate<0,44.997030,0> translate<28.940000,0.000000,26.400000> }
cylinder{<0,0,0><0,0.035000,0>0.127000 translate<31.038800,-1.535000,22.758400>}
cylinder{<0,0,0><0,0.035000,0>0.127000 translate<33.420400,-1.535000,22.758400>}
box{<0,0,-0.127000><2.381600,0.035000,0.127000> rotate<0,0.000000,0> translate<31.038800,-1.535000,22.758400> }
cylinder{<0,0,0><0,0.035000,0>0.127000 translate<33.420400,-1.535000,22.758400>}
cylinder{<0,0,0><0,0.035000,0>0.127000 translate<33.482000,-1.535000,22.820000>}
box{<0,0,-0.127000><0.087116,0.035000,0.127000> rotate<0,-44.997030,0> translate<33.420400,-1.535000,22.758400> }
cylinder{<0,0,0><0,0.035000,0>0.635000 translate<35.000000,0.000000,11.730000>}
cylinder{<0,0,0><0,0.035000,0>0.635000 translate<35.000000,0.000000,14.800000>}
box{<0,0,-0.635000><3.070000,0.035000,0.635000> rotate<0,90.000000,0> translate<35.000000,0.000000,14.800000> }
cylinder{<0,0,0><0,0.035000,0>0.635000 translate<35.000000,0.000000,14.800000>}
cylinder{<0,0,0><0,0.035000,0>0.635000 translate<35.052000,0.000000,14.852000>}
box{<0,0,-0.635000><0.073539,0.035000,0.635000> rotate<0,-44.997030,0> translate<35.000000,0.000000,14.800000> }
cylinder{<0,0,0><0,0.035000,0>0.635000 translate<35.052000,0.000000,17.018000>}
cylinder{<0,0,0><0,0.035000,0>0.635000 translate<35.052000,0.000000,14.852000>}
box{<0,0,-0.635000><2.166000,0.035000,0.635000> rotate<0,-90.000000,0> translate<35.052000,0.000000,14.852000> }
cylinder{<0,0,0><0,0.035000,0>0.635000 translate<31.270000,0.000000,20.800000>}
cylinder{<0,0,0><0,0.035000,0>0.635000 translate<35.052000,0.000000,17.018000>}
box{<0,0,-0.635000><5.348556,0.035000,0.635000> rotate<0,44.997030,0> translate<31.270000,0.000000,20.800000> }
cylinder{<0,0,0><0,0.035000,0>0.127000 translate<35.382000,-1.535000,22.820000>}
cylinder{<0,0,0><0,0.035000,0>0.127000 translate<35.600000,-1.535000,23.038000>}
box{<0,0,-0.127000><0.308299,0.035000,0.127000> rotate<0,-44.997030,0> translate<35.382000,-1.535000,22.820000> }
cylinder{<0,0,0><0,0.035000,0>0.127000 translate<35.600000,-1.535000,26.260000>}
cylinder{<0,0,0><0,0.035000,0>0.127000 translate<35.600000,-1.535000,23.038000>}
box{<0,0,-0.127000><3.222000,0.035000,0.127000> rotate<0,-90.000000,0> translate<35.600000,-1.535000,23.038000> }
cylinder{<0,0,0><0,0.035000,0>0.127000 translate<35.460000,-1.535000,26.400000>}
cylinder{<0,0,0><0,0.035000,0>0.127000 translate<35.600000,-1.535000,26.260000>}
box{<0,0,-0.127000><0.197990,0.035000,0.127000> rotate<0,44.997030,0> translate<35.460000,-1.535000,26.400000> }
cylinder{<0,0,0><0,0.035000,0>0.635000 translate<35.000000,-1.535000,11.730000>}
cylinder{<0,0,0><0,0.035000,0>0.635000 translate<36.300000,-1.535000,11.730000>}
box{<0,0,-0.635000><1.300000,0.035000,0.635000> rotate<0,0.000000,0> translate<35.000000,-1.535000,11.730000> }
cylinder{<0,0,0><0,0.035000,0>0.635000 translate<32.540000,0.000000,22.800000>}
cylinder{<0,0,0><0,0.035000,0>0.635000 translate<38.000000,0.000000,22.800000>}
box{<0,0,-0.635000><5.460000,0.035000,0.635000> rotate<0,0.000000,0> translate<32.540000,0.000000,22.800000> }
cylinder{<0,0,0><0,0.035000,0>0.635000 translate<38.000000,-1.535000,26.400000>}
cylinder{<0,0,0><0,0.035000,0>0.635000 translate<38.100000,-1.535000,26.300000>}
box{<0,0,-0.635000><0.141421,0.035000,0.635000> rotate<0,44.997030,0> translate<38.000000,-1.535000,26.400000> }
cylinder{<0,0,0><0,0.035000,0>0.635000 translate<38.100000,-1.535000,25.000000>}
cylinder{<0,0,0><0,0.035000,0>0.635000 translate<38.100000,-1.535000,26.300000>}
box{<0,0,-0.635000><1.300000,0.035000,0.635000> rotate<0,90.000000,0> translate<38.100000,-1.535000,26.300000> }
cylinder{<0,0,0><0,0.035000,0>0.635000 translate<36.300000,-1.535000,11.730000>}
cylinder{<0,0,0><0,0.035000,0>0.635000 translate<39.200000,-1.535000,8.830000>}
box{<0,0,-0.635000><4.101219,0.035000,0.635000> rotate<0,44.997030,0> translate<36.300000,-1.535000,11.730000> }
cylinder{<0,0,0><0,0.035000,0>0.635000 translate<39.200000,-1.535000,16.230000>}
cylinder{<0,0,0><0,0.035000,0>0.635000 translate<39.200000,-1.535000,16.200000>}
box{<0,0,-0.635000><0.030000,0.035000,0.635000> rotate<0,-90.000000,0> translate<39.200000,-1.535000,16.200000> }
cylinder{<0,0,0><0,0.035000,0>0.635000 translate<38.000000,0.000000,22.800000>}
cylinder{<0,0,0><0,0.035000,0>0.635000 translate<40.540000,0.000000,25.340000>}
box{<0,0,-0.635000><3.592102,0.035000,0.635000> rotate<0,-44.997030,0> translate<38.000000,0.000000,22.800000> }
cylinder{<0,0,0><0,0.035000,0>0.635000 translate<40.540000,0.000000,26.400000>}
cylinder{<0,0,0><0,0.035000,0>0.635000 translate<40.540000,0.000000,25.340000>}
box{<0,0,-0.635000><1.060000,0.035000,0.635000> rotate<0,-90.000000,0> translate<40.540000,0.000000,25.340000> }
cylinder{<0,0,0><0,0.035000,0>0.635000 translate<40.540000,0.000000,26.400000>}
cylinder{<0,0,0><0,0.035000,0>0.635000 translate<41.025600,0.000000,25.914400>}
box{<0,0,-0.635000><0.686742,0.035000,0.635000> rotate<0,44.997030,0> translate<40.540000,0.000000,26.400000> }
cylinder{<0,0,0><0,0.035000,0>0.635000 translate<41.025600,0.000000,22.774400>}
cylinder{<0,0,0><0,0.035000,0>0.635000 translate<41.025600,0.000000,25.914400>}
box{<0,0,-0.635000><3.140000,0.035000,0.635000> rotate<0,90.000000,0> translate<41.025600,0.000000,25.914400> }
cylinder{<0,0,0><0,0.035000,0>0.635000 translate<41.100000,0.000000,19.100000>}
cylinder{<0,0,0><0,0.035000,0>0.635000 translate<41.100000,0.000000,14.800000>}
box{<0,0,-0.635000><4.300000,0.035000,0.635000> rotate<0,-90.000000,0> translate<41.100000,0.000000,14.800000> }
cylinder{<0,0,0><0,0.035000,0>0.635000 translate<41.100000,0.000000,22.594800>}
cylinder{<0,0,0><0,0.035000,0>0.635000 translate<41.100000,0.000000,19.100000>}
box{<0,0,-0.635000><3.494800,0.035000,0.635000> rotate<0,-90.000000,0> translate<41.100000,0.000000,19.100000> }
cylinder{<0,0,0><0,0.035000,0>0.635000 translate<39.200000,-1.535000,8.830000>}
cylinder{<0,0,0><0,0.035000,0>0.635000 translate<41.170000,-1.535000,8.830000>}
box{<0,0,-0.635000><1.970000,0.035000,0.635000> rotate<0,0.000000,0> translate<39.200000,-1.535000,8.830000> }
cylinder{<0,0,0><0,0.035000,0>0.635000 translate<41.100000,0.000000,19.100000>}
cylinder{<0,0,0><0,0.035000,0>0.635000 translate<41.200000,0.000000,19.000000>}
box{<0,0,-0.635000><0.141421,0.035000,0.635000> rotate<0,44.997030,0> translate<41.100000,0.000000,19.100000> }
cylinder{<0,0,0><0,0.035000,0>0.635000 translate<38.100000,-1.535000,25.000000>}
cylinder{<0,0,0><0,0.035000,0>0.635000 translate<41.800000,-1.535000,21.300000>}
box{<0,0,-0.635000><5.232590,0.035000,0.635000> rotate<0,44.997030,0> translate<38.100000,-1.535000,25.000000> }
cylinder{<0,0,0><0,0.035000,0>0.635000 translate<39.200000,-1.535000,16.200000>}
cylinder{<0,0,0><0,0.035000,0>0.635000 translate<41.900000,-1.535000,13.500000>}
box{<0,0,-0.635000><3.818377,0.035000,0.635000> rotate<0,44.997030,0> translate<39.200000,-1.535000,16.200000> }
cylinder{<0,0,0><0,0.035000,0>0.635000 translate<41.100000,0.000000,14.800000>}
cylinder{<0,0,0><0,0.035000,0>0.635000 translate<42.725600,0.000000,13.174400>}
box{<0,0,-0.635000><2.298946,0.035000,0.635000> rotate<0,44.997030,0> translate<41.100000,0.000000,14.800000> }
cylinder{<0,0,0><0,0.035000,0>0.635000 translate<42.900000,0.000000,7.100000>}
cylinder{<0,0,0><0,0.035000,0>0.635000 translate<42.900000,0.000000,7.000000>}
box{<0,0,-0.635000><0.100000,0.035000,0.635000> rotate<0,-90.000000,0> translate<42.900000,0.000000,7.000000> }
cylinder{<0,0,0><0,0.035000,0>0.635000 translate<41.170000,-1.535000,8.830000>}
cylinder{<0,0,0><0,0.035000,0>0.635000 translate<42.900000,-1.535000,7.100000>}
box{<0,0,-0.635000><2.446589,0.035000,0.635000> rotate<0,44.997030,0> translate<41.170000,-1.535000,8.830000> }
cylinder{<0,0,0><0,0.035000,0>0.127000 translate<42.900000,0.000000,16.600000>}
cylinder{<0,0,0><0,0.035000,0>0.127000 translate<42.900000,0.000000,16.200000>}
box{<0,0,-0.127000><0.400000,0.035000,0.127000> rotate<0,-90.000000,0> translate<42.900000,0.000000,16.200000> }
cylinder{<0,0,0><0,0.035000,0>0.127000 translate<42.900000,0.000000,16.200000>}
cylinder{<0,0,0><0,0.035000,0>0.127000 translate<43.700000,0.000000,16.200000>}
box{<0,0,-0.127000><0.800000,0.035000,0.127000> rotate<0,0.000000,0> translate<42.900000,0.000000,16.200000> }
cylinder{<0,0,0><0,0.035000,0>0.635000 translate<41.200000,0.000000,19.000000>}
cylinder{<0,0,0><0,0.035000,0>0.635000 translate<44.200000,0.000000,19.000000>}
box{<0,0,-0.635000><3.000000,0.035000,0.635000> rotate<0,0.000000,0> translate<41.200000,0.000000,19.000000> }
cylinder{<0,0,0><0,0.035000,0>0.127000 translate<43.700000,0.000000,16.200000>}
cylinder{<0,0,0><0,0.035000,0>0.127000 translate<44.400000,0.000000,15.400000>}
box{<0,0,-0.127000><1.063015,0.035000,0.127000> rotate<0,48.810853,0> translate<43.700000,0.000000,16.200000> }
cylinder{<0,0,0><0,0.035000,0>0.127000 translate<44.200000,0.000000,16.200000>}
cylinder{<0,0,0><0,0.035000,0>0.127000 translate<44.400000,0.000000,15.400000>}
box{<0,0,-0.127000><0.824621,0.035000,0.127000> rotate<0,75.958743,0> translate<44.200000,0.000000,16.200000> }
cylinder{<0,0,0><0,0.035000,0>0.635000 translate<26.800000,0.000000,9.200000>}
cylinder{<0,0,0><0,0.035000,0>0.635000 translate<44.600000,0.000000,9.200000>}
box{<0,0,-0.635000><17.800000,0.035000,0.635000> rotate<0,0.000000,0> translate<26.800000,0.000000,9.200000> }
cylinder{<0,0,0><0,0.035000,0>0.635000 translate<44.700000,0.000000,5.200000>}
cylinder{<0,0,0><0,0.035000,0>0.635000 translate<44.700000,0.000000,3.300000>}
box{<0,0,-0.635000><1.900000,0.035000,0.635000> rotate<0,-90.000000,0> translate<44.700000,0.000000,3.300000> }
cylinder{<0,0,0><0,0.035000,0>0.635000 translate<42.900000,0.000000,7.000000>}
cylinder{<0,0,0><0,0.035000,0>0.635000 translate<44.700000,0.000000,5.200000>}
box{<0,0,-0.635000><2.545584,0.035000,0.635000> rotate<0,44.997030,0> translate<42.900000,0.000000,7.000000> }
cylinder{<0,0,0><0,0.035000,0>0.635000 translate<44.700000,0.000000,3.300000>}
cylinder{<0,0,0><0,0.035000,0>0.635000 translate<45.600000,0.000000,2.400000>}
box{<0,0,-0.635000><1.272792,0.035000,0.635000> rotate<0,44.997030,0> translate<44.700000,0.000000,3.300000> }
cylinder{<0,0,0><0,0.035000,0>0.635000 translate<41.900000,-1.535000,13.500000>}
cylinder{<0,0,0><0,0.035000,0>0.635000 translate<45.800000,-1.535000,13.500000>}
box{<0,0,-0.635000><3.900000,0.035000,0.635000> rotate<0,0.000000,0> translate<41.900000,-1.535000,13.500000> }
cylinder{<0,0,0><0,0.035000,0>0.635000 translate<44.600000,-1.535000,9.200000>}
cylinder{<0,0,0><0,0.035000,0>0.635000 translate<46.200000,-1.535000,10.800000>}
box{<0,0,-0.635000><2.262742,0.035000,0.635000> rotate<0,-44.997030,0> translate<44.600000,-1.535000,9.200000> }
cylinder{<0,0,0><0,0.035000,0>0.635000 translate<45.800000,-1.535000,13.500000>}
cylinder{<0,0,0><0,0.035000,0>0.635000 translate<46.200000,-1.535000,13.900000>}
box{<0,0,-0.635000><0.565685,0.035000,0.635000> rotate<0,-44.997030,0> translate<45.800000,-1.535000,13.500000> }
cylinder{<0,0,0><0,0.035000,0>0.635000 translate<46.200000,-1.535000,10.800000>}
cylinder{<0,0,0><0,0.035000,0>0.635000 translate<46.200000,-1.535000,13.900000>}
box{<0,0,-0.635000><3.100000,0.035000,0.635000> rotate<0,90.000000,0> translate<46.200000,-1.535000,13.900000> }
cylinder{<0,0,0><0,0.035000,0>0.635000 translate<41.800000,-1.535000,21.300000>}
cylinder{<0,0,0><0,0.035000,0>0.635000 translate<46.200000,-1.535000,21.300000>}
box{<0,0,-0.635000><4.400000,0.035000,0.635000> rotate<0,0.000000,0> translate<41.800000,-1.535000,21.300000> }
cylinder{<0,0,0><0,0.035000,0>0.635000 translate<42.905200,0.000000,13.100000>}
cylinder{<0,0,0><0,0.035000,0>0.635000 translate<46.394800,0.000000,13.100000>}
box{<0,0,-0.635000><3.489600,0.035000,0.635000> rotate<0,0.000000,0> translate<42.905200,0.000000,13.100000> }
cylinder{<0,0,0><0,0.035000,0>0.127000 translate<44.948800,-1.535000,24.139400>}
cylinder{<0,0,0><0,0.035000,0>0.127000 translate<46.400000,-1.535000,24.300000>}
box{<0,0,-0.127000><1.460060,0.035000,0.127000> rotate<0,-6.314640,0> translate<44.948800,-1.535000,24.139400> }
cylinder{<0,0,0><0,0.035000,0>0.304800 translate<41.030000,-1.535000,4.400000>}
cylinder{<0,0,0><0,0.035000,0>0.304800 translate<46.900000,-1.535000,4.400000>}
box{<0,0,-0.304800><5.870000,0.035000,0.304800> rotate<0,0.000000,0> translate<41.030000,-1.535000,4.400000> }
cylinder{<0,0,0><0,0.035000,0>0.635000 translate<47.200000,0.000000,15.400000>}
cylinder{<0,0,0><0,0.035000,0>0.635000 translate<47.200000,0.000000,22.700000>}
box{<0,0,-0.635000><7.300000,0.035000,0.635000> rotate<0,90.000000,0> translate<47.200000,0.000000,22.700000> }
cylinder{<0,0,0><0,0.035000,0>0.635000 translate<47.200000,0.000000,22.700000>}
cylinder{<0,0,0><0,0.035000,0>0.635000 translate<47.300000,0.000000,22.800000>}
box{<0,0,-0.635000><0.141421,0.035000,0.635000> rotate<0,-44.997030,0> translate<47.200000,0.000000,22.700000> }
cylinder{<0,0,0><0,0.035000,0>0.635000 translate<47.300000,0.000000,22.800000>}
cylinder{<0,0,0><0,0.035000,0>0.635000 translate<47.700000,0.000000,22.800000>}
box{<0,0,-0.635000><0.400000,0.035000,0.635000> rotate<0,0.000000,0> translate<47.300000,0.000000,22.800000> }
cylinder{<0,0,0><0,0.035000,0>0.635000 translate<44.948800,0.000000,28.660600>}
cylinder{<0,0,0><0,0.035000,0>0.635000 translate<47.700000,0.000000,28.700000>}
box{<0,0,-0.635000><2.751482,0.035000,0.635000> rotate<0,-0.820424,0> translate<44.948800,0.000000,28.660600> }
cylinder{<0,0,0><0,0.035000,0>0.635000 translate<47.700000,0.000000,22.800000>}
cylinder{<0,0,0><0,0.035000,0>0.635000 translate<47.700000,0.000000,28.700000>}
box{<0,0,-0.635000><5.900000,0.035000,0.635000> rotate<0,90.000000,0> translate<47.700000,0.000000,28.700000> }
cylinder{<0,0,0><0,0.035000,0>0.304800 translate<46.900000,-1.535000,4.400000>}
cylinder{<0,0,0><0,0.035000,0>0.304800 translate<48.400000,-1.535000,4.730000>}
box{<0,0,-0.304800><1.535871,0.035000,0.304800> rotate<0,-12.406600,0> translate<46.900000,-1.535000,4.400000> }
cylinder{<0,0,0><0,0.035000,0>0.635000 translate<46.200000,-1.535000,13.900000>}
cylinder{<0,0,0><0,0.035000,0>0.635000 translate<49.200000,-1.535000,13.900000>}
box{<0,0,-0.635000><3.000000,0.035000,0.635000> rotate<0,0.000000,0> translate<46.200000,-1.535000,13.900000> }
cylinder{<0,0,0><0,0.035000,0>0.127000 translate<49.800000,0.000000,19.000000>}
cylinder{<0,0,0><0,0.035000,0>0.127000 translate<49.800000,0.000000,20.500000>}
box{<0,0,-0.127000><1.500000,0.035000,0.127000> rotate<0,90.000000,0> translate<49.800000,0.000000,20.500000> }
cylinder{<0,0,0><0,0.035000,0>0.635000 translate<49.800000,0.000000,19.000000>}
cylinder{<0,0,0><0,0.035000,0>0.635000 translate<49.800000,0.000000,22.488200>}
box{<0,0,-0.635000><3.488200,0.035000,0.635000> rotate<0,90.000000,0> translate<49.800000,0.000000,22.488200> }
cylinder{<0,0,0><0,0.035000,0>0.254000 translate<46.400000,-1.535000,24.300000>}
cylinder{<0,0,0><0,0.035000,0>0.254000 translate<50.000000,-1.535000,24.300000>}
box{<0,0,-0.254000><3.600000,0.035000,0.254000> rotate<0,0.000000,0> translate<46.400000,-1.535000,24.300000> }
cylinder{<0,0,0><0,0.035000,0>0.635000 translate<49.200000,-1.535000,13.900000>}
cylinder{<0,0,0><0,0.035000,0>0.635000 translate<50.300000,-1.535000,15.000000>}
box{<0,0,-0.635000><1.555635,0.035000,0.635000> rotate<0,-44.997030,0> translate<49.200000,-1.535000,13.900000> }
cylinder{<0,0,0><0,0.035000,0>0.635000 translate<46.574400,0.000000,13.025600>}
cylinder{<0,0,0><0,0.035000,0>0.635000 translate<50.800000,0.000000,8.700000>}
box{<0,0,-0.635000><6.047025,0.035000,0.635000> rotate<0,45.666987,0> translate<46.574400,0.000000,13.025600> }
cylinder{<0,0,0><0,0.035000,0>0.635000 translate<51.200000,0.000000,8.300000>}
cylinder{<0,0,0><0,0.035000,0>0.635000 translate<51.200000,0.000000,7.400000>}
box{<0,0,-0.635000><0.900000,0.035000,0.635000> rotate<0,-90.000000,0> translate<51.200000,0.000000,7.400000> }
cylinder{<0,0,0><0,0.035000,0>0.635000 translate<50.800000,0.000000,8.700000>}
cylinder{<0,0,0><0,0.035000,0>0.635000 translate<51.200000,0.000000,8.300000>}
box{<0,0,-0.635000><0.565685,0.035000,0.635000> rotate<0,44.997030,0> translate<50.800000,0.000000,8.700000> }
cylinder{<0,0,0><0,0.035000,0>0.635000 translate<48.514000,0.000000,7.366000>}
cylinder{<0,0,0><0,0.035000,0>0.635000 translate<51.308000,0.000000,7.366000>}
box{<0,0,-0.635000><2.794000,0.035000,0.635000> rotate<0,0.000000,0> translate<48.514000,0.000000,7.366000> }
cylinder{<0,0,0><0,0.035000,0>0.635000 translate<49.800000,0.000000,22.488200>}
cylinder{<0,0,0><0,0.035000,0>0.635000 translate<51.451200,0.000000,24.139400>}
box{<0,0,-0.635000><2.335149,0.035000,0.635000> rotate<0,-44.997030,0> translate<49.800000,0.000000,22.488200> }
cylinder{<0,0,0><0,0.035000,0>0.127000 translate<50.000000,-1.535000,24.300000>}
cylinder{<0,0,0><0,0.035000,0>0.127000 translate<51.451200,-1.535000,24.139400>}
box{<0,0,-0.127000><1.460060,0.035000,0.127000> rotate<0,6.314640,0> translate<50.000000,-1.535000,24.300000> }
cylinder{<0,0,0><0,0.035000,0>0.635000 translate<47.700000,0.000000,28.700000>}
cylinder{<0,0,0><0,0.035000,0>0.635000 translate<51.451200,0.000000,28.660600>}
box{<0,0,-0.635000><3.751407,0.035000,0.635000> rotate<0,0.601733,0> translate<47.700000,0.000000,28.700000> }
cylinder{<0,0,0><0,0.035000,0>0.635000 translate<50.300000,-1.535000,15.000000>}
cylinder{<0,0,0><0,0.035000,0>0.635000 translate<51.800000,-1.535000,15.000000>}
box{<0,0,-0.635000><1.500000,0.035000,0.635000> rotate<0,0.000000,0> translate<50.300000,-1.535000,15.000000> }
cylinder{<0,0,0><0,0.035000,0>0.635000 translate<51.800000,-1.535000,15.000000>}
cylinder{<0,0,0><0,0.035000,0>0.635000 translate<51.820000,-1.535000,15.020000>}
box{<0,0,-0.635000><0.028284,0.035000,0.635000> rotate<0,-44.997030,0> translate<51.800000,-1.535000,15.000000> }
cylinder{<0,0,0><0,0.035000,0>0.635000 translate<46.200000,-1.535000,21.300000>}
cylinder{<0,0,0><0,0.035000,0>0.635000 translate<52.700000,-1.535000,21.300000>}
box{<0,0,-0.635000><6.500000,0.035000,0.635000> rotate<0,0.000000,0> translate<46.200000,-1.535000,21.300000> }
cylinder{<0,0,0><0,0.035000,0>0.635000 translate<45.600000,0.000000,2.400000>}
cylinder{<0,0,0><0,0.035000,0>0.635000 translate<53.500000,0.000000,2.400000>}
box{<0,0,-0.635000><7.900000,0.035000,0.635000> rotate<0,0.000000,0> translate<45.600000,0.000000,2.400000> }
cylinder{<0,0,0><0,0.035000,0>0.635000 translate<52.700000,-1.535000,21.300000>}
cylinder{<0,0,0><0,0.035000,0>0.635000 translate<54.100000,-1.535000,22.700000>}
box{<0,0,-0.635000><1.979899,0.035000,0.635000> rotate<0,-44.997030,0> translate<52.700000,-1.535000,21.300000> }
cylinder{<0,0,0><0,0.035000,0>0.635000 translate<54.100000,-1.535000,22.700000>}
cylinder{<0,0,0><0,0.035000,0>0.635000 translate<54.260000,-1.535000,22.900000>}
box{<0,0,-0.635000><0.256125,0.035000,0.635000> rotate<0,-51.336803,0> translate<54.100000,-1.535000,22.700000> }
cylinder{<0,0,0><0,0.035000,0>0.635000 translate<47.200000,0.000000,15.400000>}
cylinder{<0,0,0><0,0.035000,0>0.635000 translate<56.620000,0.000000,15.400000>}
box{<0,0,-0.635000><9.420000,0.035000,0.635000> rotate<0,0.000000,0> translate<47.200000,0.000000,15.400000> }
cylinder{<0,0,0><0,0.035000,0>0.635000 translate<53.500000,0.000000,2.400000>}
cylinder{<0,0,0><0,0.035000,0>0.635000 translate<57.000000,0.000000,5.900000>}
box{<0,0,-0.635000><4.949747,0.035000,0.635000> rotate<0,-44.997030,0> translate<53.500000,0.000000,2.400000> }
cylinder{<0,0,0><0,0.035000,0>0.635000 translate<57.000000,0.000000,5.900000>}
cylinder{<0,0,0><0,0.035000,0>0.635000 translate<57.000000,0.000000,11.060000>}
box{<0,0,-0.635000><5.160000,0.035000,0.635000> rotate<0,90.000000,0> translate<57.000000,0.000000,11.060000> }
cylinder{<0,0,0><0,0.035000,0>0.635000 translate<51.820000,-1.535000,15.020000>}
cylinder{<0,0,0><0,0.035000,0>0.635000 translate<57.000000,-1.535000,15.020000>}
box{<0,0,-0.635000><5.180000,0.035000,0.635000> rotate<0,0.000000,0> translate<51.820000,-1.535000,15.020000> }
cylinder{<0,0,0><0,0.035000,0>0.635000 translate<56.620000,0.000000,15.400000>}
cylinder{<0,0,0><0,0.035000,0>0.635000 translate<57.000000,0.000000,15.020000>}
box{<0,0,-0.635000><0.537401,0.035000,0.635000> rotate<0,44.997030,0> translate<56.620000,0.000000,15.400000> }
cylinder{<0,0,0><0,0.035000,0>0.635000 translate<57.000000,-1.535000,15.020000>}
cylinder{<0,0,0><0,0.035000,0>0.635000 translate<57.000000,-1.535000,18.980000>}
box{<0,0,-0.635000><3.960000,0.035000,0.635000> rotate<0,90.000000,0> translate<57.000000,-1.535000,18.980000> }
cylinder{<0,0,0><0,0.035000,0>0.635000 translate<54.260000,-1.535000,22.900000>}
cylinder{<0,0,0><0,0.035000,0>0.635000 translate<57.000000,-1.535000,22.940000>}
box{<0,0,-0.635000><2.740292,0.035000,0.635000> rotate<0,-0.836320,0> translate<54.260000,-1.535000,22.900000> }
object{ARC(0.254000,1.270000,44.998366,89.997689,0.036000) translate<46.394791,0.000000,12.846000>}
object{ARC(0.254000,1.270000,224.998366,269.997689,0.036000) translate<42.905209,0.000000,13.354000>}
object{ARC(0.254000,1.270000,0.002311,45.001634,0.036000) translate<40.846000,0.000000,22.594791>}
//Text
//Rect
union{
texture{col_pds}
}
texture{col_wrs}
}
#end
#if(pcb_polygons=on)
union{
//Polygons
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<-18.000000,-1.535000,0.200000>}
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<62.000000,-1.535000,0.200000>}
box{<0,0,-0.203200><80.000000,0.035000,0.203200> rotate<0,0.000000,0> translate<-18.000000,-1.535000,0.200000> }
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<-18.000000,-1.535000,29.800000>}
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<-18.000000,-1.535000,0.200000>}
box{<0,0,-0.203200><29.600000,0.035000,0.203200> rotate<0,-90.000000,0> translate<-18.000000,-1.535000,0.200000> }
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<-18.000000,-1.535000,29.800000>}
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<62.000000,-1.535000,30.000000>}
box{<0,0,-0.203200><80.000250,0.035000,0.203200> rotate<0,-0.143230,0> translate<-18.000000,-1.535000,29.800000> }
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<62.000000,-1.535000,0.200000>}
cylinder{<0,0,0><0,0.035000,0>0.203200 translate<62.000000,-1.535000,30.000000>}
box{<0,0,-0.203200><29.800000,0.035000,0.203200> rotate<0,90.000000,0> translate<62.000000,-1.535000,30.000000> }
texture{col_pol}
}
#end
union{
cylinder{<41.030000,0.038000,4.400000><41.030000,-1.538000,4.400000>0.660400}
cylinder{<18.170000,0.038000,4.400000><18.170000,-1.538000,4.400000>0.660400}
cylinder{<48.400000,0.038000,7.270000><48.400000,-1.538000,7.270000>0.508000}
cylinder{<48.400000,0.038000,4.730000><48.400000,-1.538000,4.730000>0.508000}
cylinder{<-9.600000,0.038000,27.060000><-9.600000,-1.538000,27.060000>0.508000}
cylinder{<-12.140000,0.038000,27.060000><-12.140000,-1.538000,27.060000>0.508000}
cylinder{<-7.060000,0.038000,27.060000><-7.060000,-1.538000,27.060000>0.508000}
cylinder{<23.860000,0.038000,26.400000><23.860000,-1.538000,26.400000>0.508000}
cylinder{<26.400000,0.038000,26.400000><26.400000,-1.538000,26.400000>0.508000}
cylinder{<28.940000,0.038000,26.400000><28.940000,-1.538000,26.400000>0.508000}
cylinder{<35.460000,0.038000,26.400000><35.460000,-1.538000,26.400000>0.508000}
cylinder{<38.000000,0.038000,26.400000><38.000000,-1.538000,26.400000>0.508000}
cylinder{<40.540000,0.038000,26.400000><40.540000,-1.538000,26.400000>0.508000}
cylinder{<44.948800,0.038000,28.660600><44.948800,-1.538000,28.660600>0.508000}
cylinder{<51.451200,0.038000,28.660600><51.451200,-1.538000,28.660600>0.508000}
cylinder{<44.948800,0.038000,24.139400><44.948800,-1.538000,24.139400>0.508000}
cylinder{<51.451200,0.038000,24.139400><51.451200,-1.538000,24.139400>0.508000}
cylinder{<9.730000,0.038000,11.190000><9.730000,-1.538000,11.190000>0.508000}
cylinder{<12.270000,0.038000,11.190000><12.270000,-1.538000,11.190000>0.508000}
cylinder{<9.730000,0.038000,13.730000><9.730000,-1.538000,13.730000>0.508000}
cylinder{<12.270000,0.038000,13.730000><12.270000,-1.538000,13.730000>0.508000}
cylinder{<9.730000,0.038000,16.270000><9.730000,-1.538000,16.270000>0.508000}
cylinder{<12.270000,0.038000,16.270000><12.270000,-1.538000,16.270000>0.508000}
cylinder{<9.730000,0.038000,18.810000><9.730000,-1.538000,18.810000>0.508000}
cylinder{<12.270000,0.038000,18.810000><12.270000,-1.538000,18.810000>0.508000}
cylinder{<14.600000,0.038000,27.780000><14.600000,-1.538000,27.780000>0.850000}
cylinder{<14.600000,0.038000,23.820000><14.600000,-1.538000,23.820000>0.850000}
cylinder{<57.000000,0.038000,11.060000><57.000000,-1.538000,11.060000>0.850000}
cylinder{<57.000000,0.038000,15.020000><57.000000,-1.538000,15.020000>0.850000}
cylinder{<57.000000,0.038000,18.980000><57.000000,-1.538000,18.980000>0.850000}
cylinder{<57.000000,0.038000,22.940000><57.000000,-1.538000,22.940000>0.850000}
//Holes(fast)/Vias
cylinder{<2.289000,0.038000,15.981000><2.289000,-1.538000,15.981000>0.127000 }
cylinder{<5.425000,0.038000,2.523000><5.425000,-1.538000,2.523000>0.127000 }
cylinder{<-14.997400,0.038000,16.770000><-14.997400,-1.538000,16.770000>0.127000 }
cylinder{<27.100000,0.038000,16.100000><27.100000,-1.538000,16.100000>0.127000 }
cylinder{<15.008000,0.038000,21.295200><15.008000,-1.538000,21.295200>0.127000 }
cylinder{<15.008000,0.038000,18.000000><15.008000,-1.538000,18.000000>0.127000 }
cylinder{<42.900000,0.038000,16.600000><42.900000,-1.538000,16.600000>0.127000 }
cylinder{<-9.400000,0.038000,20.600000><-9.400000,-1.538000,20.600000>0.127000 }
cylinder{<51.200000,0.038000,7.400000><51.200000,-1.538000,7.400000>0.300000 }
cylinder{<-5.130800,0.038000,17.805400><-5.130800,-1.538000,17.805400>0.127000 }
cylinder{<10.800000,0.038000,8.224000><10.800000,-1.538000,8.224000>0.127000 }
cylinder{<10.801600,0.038000,23.600000><10.801600,-1.538000,23.600000>0.127000 }
cylinder{<4.262000,0.038000,17.538000><4.262000,-1.538000,17.538000>0.127000 }
cylinder{<19.800000,0.038000,13.400000><19.800000,-1.538000,13.400000>0.127000 }
cylinder{<10.700000,0.038000,21.100000><10.700000,-1.538000,21.100000>0.127000 }
cylinder{<1.040000,0.038000,19.210000><1.040000,-1.538000,19.210000>0.127000 }
cylinder{<-9.325400,0.038000,16.111200><-9.325400,-1.538000,16.111200>0.127000 }
cylinder{<-11.700000,0.038000,23.400000><-11.700000,-1.538000,23.400000>0.127000 }
cylinder{<-14.000000,0.038000,18.100000><-14.000000,-1.538000,18.100000>0.127000 }
cylinder{<-6.771400,0.038000,21.782600><-6.771400,-1.538000,21.782600>0.127000 }
cylinder{<-12.300000,0.038000,20.300000><-12.300000,-1.538000,20.300000>0.127000 }
cylinder{<-8.000000,0.038000,20.500000><-8.000000,-1.538000,20.500000>0.127000 }
cylinder{<25.100000,0.038000,5.100000><25.100000,-1.538000,5.100000>0.127000 }
cylinder{<19.400000,0.038000,9.800000><19.400000,-1.538000,9.800000>0.127000 }
cylinder{<35.000000,0.038000,11.730000><35.000000,-1.538000,11.730000>0.300000 }
cylinder{<42.900000,0.038000,7.100000><42.900000,-1.538000,7.100000>0.300000 }
cylinder{<7.400000,0.038000,22.200000><7.400000,-1.538000,22.200000>0.127000 }
cylinder{<9.144000,0.038000,6.096000><9.144000,-1.538000,6.096000>0.127000 }
cylinder{<-14.224000,0.038000,11.557000><-14.224000,-1.538000,11.557000>0.300000 }
cylinder{<44.600000,0.038000,9.200000><44.600000,-1.538000,9.200000>0.300000 }
//Holes(fast)/Board
texture{col_hls}
}
#if(pcb_silkscreen=on)
//Silk Screen
union{
cylinder{<0,0,0><0,0.036000,0>0.038100 translate<58.017506,0.000000,25.969656>}
cylinder{<0,0,0><0,0.036000,0>0.038100 translate<57.894634,0.000000,26.092528>}
box{<0,0,-0.038100><0.173767,0.036000,0.038100> rotate<0,44.997030,0> translate<57.894634,0.000000,26.092528> }
cylinder{<0,0,0><0,0.036000,0>0.038100 translate<57.894634,0.000000,26.092528>}
cylinder{<0,0,0><0,0.036000,0>0.038100 translate<57.648891,0.000000,26.092528>}
box{<0,0,-0.038100><0.245744,0.036000,0.038100> rotate<0,0.000000,0> translate<57.648891,0.000000,26.092528> }
cylinder{<0,0,0><0,0.036000,0>0.038100 translate<57.648891,0.000000,26.092528>}
cylinder{<0,0,0><0,0.036000,0>0.038100 translate<57.526022,0.000000,25.969656>}
box{<0,0,-0.038100><0.173765,0.036000,0.038100> rotate<0,-44.997759,0> translate<57.526022,0.000000,25.969656> }
cylinder{<0,0,0><0,0.036000,0>0.038100 translate<57.526022,0.000000,25.969656>}
cylinder{<0,0,0><0,0.036000,0>0.038100 translate<57.526022,0.000000,25.846784>}
box{<0,0,-0.038100><0.122872,0.036000,0.038100> rotate<0,-90.000000,0> translate<57.526022,0.000000,25.846784> }
cylinder{<0,0,0><0,0.036000,0>0.038100 translate<57.526022,0.000000,25.846784>}
cylinder{<0,0,0><0,0.036000,0>0.038100 translate<57.648891,0.000000,25.723913>}
box{<0,0,-0.038100><0.173765,0.036000,0.038100> rotate<0,44.997759,0> translate<57.526022,0.000000,25.846784> }
cylinder{<0,0,0><0,0.036000,0>0.038100 translate<57.648891,0.000000,25.723913>}
cylinder{<0,0,0><0,0.036000,0>0.038100 translate<57.894634,0.000000,25.723913>}
box{<0,0,-0.038100><0.245744,0.036000,0.038100> rotate<0,0.000000,0> translate<57.648891,0.000000,25.723913> }
cylinder{<0,0,0><0,0.036000,0>0.038100 translate<57.894634,0.000000,25.723913>}
cylinder{<0,0,0><0,0.036000,0>0.038100 translate<58.017506,0.000000,25.601041>}
box{<0,0,-0.038100><0.173767,0.036000,0.038100> rotate<0,44.997030,0> translate<57.894634,0.000000,25.723913> }
cylinder{<0,0,0><0,0.036000,0>0.038100 translate<58.017506,0.000000,25.601041>}
cylinder{<0,0,0><0,0.036000,0>0.038100 translate<58.017506,0.000000,25.478169>}
box{<0,0,-0.038100><0.122872,0.036000,0.038100> rotate<0,-90.000000,0> translate<58.017506,0.000000,25.478169> }
cylinder{<0,0,0><0,0.036000,0>0.038100 translate<58.017506,0.000000,25.478169>}
cylinder{<0,0,0><0,0.036000,0>0.038100 translate<57.894634,0.000000,25.355300>}
box{<0,0,-0.038100><0.173765,0.036000,0.038100> rotate<0,-44.996302,0> translate<57.894634,0.000000,25.355300> }
cylinder{<0,0,0><0,0.036000,0>0.038100 translate<57.894634,0.000000,25.355300>}
cylinder{<0,0,0><0,0.036000,0>0.038100 translate<57.648891,0.000000,25.355300>}
box{<0,0,-0.038100><0.245744,0.036000,0.038100> rotate<0,0.000000,0> translate<57.648891,0.000000,25.355300> }
cylinder{<0,0,0><0,0.036000,0>0.038100 translate<57.648891,0.000000,25.355300>}
cylinder{<0,0,0><0,0.036000,0>0.038100 translate<57.526022,0.000000,25.478169>}
box{<0,0,-0.038100><0.173763,0.036000,0.038100> rotate<0,44.997030,0> translate<57.526022,0.000000,25.478169> }
cylinder{<0,0,0><0,0.036000,0>0.038100 translate<58.274438,0.000000,25.355300>}
cylinder{<0,0,0><0,0.036000,0>0.038100 translate<58.274438,0.000000,26.092528>}
box{<0,0,-0.038100><0.737228,0.036000,0.038100> rotate<0,90.000000,0> translate<58.274438,0.000000,26.092528> }
cylinder{<0,0,0><0,0.036000,0>0.038100 translate<58.274438,0.000000,25.723913>}
cylinder{<0,0,0><0,0.036000,0>0.038100 translate<58.765922,0.000000,25.723913>}
box{<0,0,-0.038100><0.491484,0.036000,0.038100> rotate<0,0.000000,0> translate<58.274438,0.000000,25.723913> }
cylinder{<0,0,0><0,0.036000,0>0.038100 translate<58.765922,0.000000,26.092528>}
cylinder{<0,0,0><0,0.036000,0>0.038100 translate<58.765922,0.000000,25.355300>}
box{<0,0,-0.038100><0.737228,0.036000,0.038100> rotate<0,-90.000000,0> translate<58.765922,0.000000,25.355300> }
cylinder{<0,0,0><0,0.036000,0>0.038100 translate<59.391466,0.000000,26.092528>}
cylinder{<0,0,0><0,0.036000,0>0.038100 translate<59.145722,0.000000,26.092528>}
box{<0,0,-0.038100><0.245744,0.036000,0.038100> rotate<0,0.000000,0> translate<59.145722,0.000000,26.092528> }
cylinder{<0,0,0><0,0.036000,0>0.038100 translate<59.145722,0.000000,26.092528>}
cylinder{<0,0,0><0,0.036000,0>0.038100 translate<59.022853,0.000000,25.969656>}
box{<0,0,-0.038100><0.173765,0.036000,0.038100> rotate<0,-44.997759,0> translate<59.022853,0.000000,25.969656> }
cylinder{<0,0,0><0,0.036000,0>0.038100 translate<59.022853,0.000000,25.969656>}
cylinder{<0,0,0><0,0.036000,0>0.038100 translate<59.022853,0.000000,25.478169>}
box{<0,0,-0.038100><0.491487,0.036000,0.038100> rotate<0,-90.000000,0> translate<59.022853,0.000000,25.478169> }
cylinder{<0,0,0><0,0.036000,0>0.038100 translate<59.022853,0.000000,25.478169>}
cylinder{<0,0,0><0,0.036000,0>0.038100 translate<59.145722,0.000000,25.355300>}
box{<0,0,-0.038100><0.173763,0.036000,0.038100> rotate<0,44.997030,0> translate<59.022853,0.000000,25.478169> }
cylinder{<0,0,0><0,0.036000,0>0.038100 translate<59.145722,0.000000,25.355300>}
cylinder{<0,0,0><0,0.036000,0>0.038100 translate<59.391466,0.000000,25.355300>}
box{<0,0,-0.038100><0.245744,0.036000,0.038100> rotate<0,0.000000,0> translate<59.145722,0.000000,25.355300> }
cylinder{<0,0,0><0,0.036000,0>0.038100 translate<59.391466,0.000000,25.355300>}
cylinder{<0,0,0><0,0.036000,0>0.038100 translate<59.514338,0.000000,25.478169>}
box{<0,0,-0.038100><0.173765,0.036000,0.038100> rotate<0,-44.996302,0> translate<59.391466,0.000000,25.355300> }
cylinder{<0,0,0><0,0.036000,0>0.038100 translate<59.514338,0.000000,25.478169>}
cylinder{<0,0,0><0,0.036000,0>0.038100 translate<59.514338,0.000000,25.969656>}
box{<0,0,-0.038100><0.491487,0.036000,0.038100> rotate<0,90.000000,0> translate<59.514338,0.000000,25.969656> }
cylinder{<0,0,0><0,0.036000,0>0.038100 translate<59.514338,0.000000,25.969656>}
cylinder{<0,0,0><0,0.036000,0>0.038100 translate<59.391466,0.000000,26.092528>}
box{<0,0,-0.038100><0.173767,0.036000,0.038100> rotate<0,44.997030,0> translate<59.391466,0.000000,26.092528> }
cylinder{<0,0,0><0,0.036000,0>0.038100 translate<60.139881,0.000000,26.092528>}
cylinder{<0,0,0><0,0.036000,0>0.038100 translate<59.894137,0.000000,26.092528>}
box{<0,0,-0.038100><0.245744,0.036000,0.038100> rotate<0,0.000000,0> translate<59.894137,0.000000,26.092528> }
cylinder{<0,0,0><0,0.036000,0>0.038100 translate<59.894137,0.000000,26.092528>}
cylinder{<0,0,0><0,0.036000,0>0.038100 translate<59.771269,0.000000,25.969656>}
box{<0,0,-0.038100><0.173765,0.036000,0.038100> rotate<0,-44.997759,0> translate<59.771269,0.000000,25.969656> }
cylinder{<0,0,0><0,0.036000,0>0.038100 translate<59.771269,0.000000,25.969656>}
cylinder{<0,0,0><0,0.036000,0>0.038100 translate<59.771269,0.000000,25.478169>}
box{<0,0,-0.038100><0.491487,0.036000,0.038100> rotate<0,-90.000000,0> translate<59.771269,0.000000,25.478169> }
cylinder{<0,0,0><0,0.036000,0>0.038100 translate<59.771269,0.000000,25.478169>}
cylinder{<0,0,0><0,0.036000,0>0.038100 translate<59.894137,0.000000,25.355300>}
box{<0,0,-0.038100><0.173763,0.036000,0.038100> rotate<0,44.997030,0> translate<59.771269,0.000000,25.478169> }
cylinder{<0,0,0><0,0.036000,0>0.038100 translate<59.894137,0.000000,25.355300>}
cylinder{<0,0,0><0,0.036000,0>0.038100 translate<60.139881,0.000000,25.355300>}
box{<0,0,-0.038100><0.245744,0.036000,0.038100> rotate<0,0.000000,0> translate<59.894137,0.000000,25.355300> }
cylinder{<0,0,0><0,0.036000,0>0.038100 translate<60.139881,0.000000,25.355300>}
cylinder{<0,0,0><0,0.036000,0>0.038100 translate<60.262753,0.000000,25.478169>}
box{<0,0,-0.038100><0.173765,0.036000,0.038100> rotate<0,-44.996302,0> translate<60.139881,0.000000,25.355300> }
cylinder{<0,0,0><0,0.036000,0>0.038100 translate<60.262753,0.000000,25.478169>}
cylinder{<0,0,0><0,0.036000,0>0.038100 translate<60.262753,0.000000,25.969656>}
box{<0,0,-0.038100><0.491487,0.036000,0.038100> rotate<0,90.000000,0> translate<60.262753,0.000000,25.969656> }
cylinder{<0,0,0><0,0.036000,0>0.038100 translate<60.262753,0.000000,25.969656>}
cylinder{<0,0,0><0,0.036000,0>0.038100 translate<60.139881,0.000000,26.092528>}
box{<0,0,-0.038100><0.173767,0.036000,0.038100> rotate<0,44.997030,0> translate<60.139881,0.000000,26.092528> }
cylinder{<0,0,0><0,0.036000,0>0.038100 translate<60.765425,0.000000,25.355300>}
cylinder{<0,0,0><0,0.036000,0>0.038100 translate<60.765425,0.000000,26.092528>}
box{<0,0,-0.038100><0.737228,0.036000,0.038100> rotate<0,90.000000,0> translate<60.765425,0.000000,26.092528> }
cylinder{<0,0,0><0,0.036000,0>0.038100 translate<60.519684,0.000000,26.092528>}
cylinder{<0,0,0><0,0.036000,0>0.038100 translate<61.011169,0.000000,26.092528>}
box{<0,0,-0.038100><0.491484,0.036000,0.038100> rotate<0,0.000000,0> translate<60.519684,0.000000,26.092528> }
cylinder{<0,0,0><0,0.036000,0>0.038100 translate<58.125794,0.000000,8.693456>}
cylinder{<0,0,0><0,0.036000,0>0.038100 translate<58.002922,0.000000,8.816328>}
box{<0,0,-0.038100><0.173767,0.036000,0.038100> rotate<0,44.997030,0> translate<58.002922,0.000000,8.816328> }
cylinder{<0,0,0><0,0.036000,0>0.038100 translate<58.002922,0.000000,8.816328>}
cylinder{<0,0,0><0,0.036000,0>0.038100 translate<57.757178,0.000000,8.816328>}
box{<0,0,-0.038100><0.245744,0.036000,0.038100> rotate<0,0.000000,0> translate<57.757178,0.000000,8.816328> }
cylinder{<0,0,0><0,0.036000,0>0.038100 translate<57.757178,0.000000,8.816328>}
cylinder{<0,0,0><0,0.036000,0>0.038100 translate<57.634309,0.000000,8.693456>}
box{<0,0,-0.038100><0.173765,0.036000,0.038100> rotate<0,-44.997759,0> translate<57.634309,0.000000,8.693456> }
cylinder{<0,0,0><0,0.036000,0>0.038100 translate<57.634309,0.000000,8.693456>}
cylinder{<0,0,0><0,0.036000,0>0.038100 translate<57.634309,0.000000,8.201969>}
box{<0,0,-0.038100><0.491487,0.036000,0.038100> rotate<0,-90.000000,0> translate<57.634309,0.000000,8.201969> }
cylinder{<0,0,0><0,0.036000,0>0.038100 translate<57.634309,0.000000,8.201969>}
cylinder{<0,0,0><0,0.036000,0>0.038100 translate<57.757178,0.000000,8.079100>}
box{<0,0,-0.038100><0.173763,0.036000,0.038100> rotate<0,44.997030,0> translate<57.634309,0.000000,8.201969> }
cylinder{<0,0,0><0,0.036000,0>0.038100 translate<57.757178,0.000000,8.079100>}
cylinder{<0,0,0><0,0.036000,0>0.038100 translate<58.002922,0.000000,8.079100>}
box{<0,0,-0.038100><0.245744,0.036000,0.038100> rotate<0,0.000000,0> translate<57.757178,0.000000,8.079100> }
cylinder{<0,0,0><0,0.036000,0>0.038100 translate<58.002922,0.000000,8.079100>}
cylinder{<0,0,0><0,0.036000,0>0.038100 translate<58.125794,0.000000,8.201969>}
box{<0,0,-0.038100><0.173765,0.036000,0.038100> rotate<0,-44.996302,0> translate<58.002922,0.000000,8.079100> }
cylinder{<0,0,0><0,0.036000,0>0.038100 translate<58.382725,0.000000,8.079100>}
cylinder{<0,0,0><0,0.036000,0>0.038100 translate<58.382725,0.000000,8.816328>}
box{<0,0,-0.038100><0.737228,0.036000,0.038100> rotate<0,90.000000,0> translate<58.382725,0.000000,8.816328> }
cylinder{<0,0,0><0,0.036000,0>0.038100 translate<58.382725,0.000000,8.447712>}
cylinder{<0,0,0><0,0.036000,0>0.038100 translate<58.874209,0.000000,8.447712>}
box{<0,0,-0.038100><0.491484,0.036000,0.038100> rotate<0,0.000000,0> translate<58.382725,0.000000,8.447712> }
cylinder{<0,0,0><0,0.036000,0>0.038100 translate<58.874209,0.000000,8.816328>}
cylinder{<0,0,0><0,0.036000,0>0.038100 translate<58.874209,0.000000,8.079100>}
box{<0,0,-0.038100><0.737228,0.036000,0.038100> rotate<0,-90.000000,0> translate<58.874209,0.000000,8.079100> }
cylinder{<0,0,0><0,0.036000,0>0.038100 translate<59.131141,0.000000,8.079100>}
cylinder{<0,0,0><0,0.036000,0>0.038100 translate<59.376881,0.000000,8.079100>}
box{<0,0,-0.038100><0.245741,0.036000,0.038100> rotate<0,0.000000,0> translate<59.131141,0.000000,8.079100> }
cylinder{<0,0,0><0,0.036000,0>0.038100 translate<59.254009,0.000000,8.079100>}
cylinder{<0,0,0><0,0.036000,0>0.038100 translate<59.254009,0.000000,8.816328>}
box{<0,0,-0.038100><0.737228,0.036000,0.038100> rotate<0,90.000000,0> translate<59.254009,0.000000,8.816328> }
cylinder{<0,0,0><0,0.036000,0>0.038100 translate<59.131141,0.000000,8.816328>}
cylinder{<0,0,0><0,0.036000,0>0.038100 translate<59.376881,0.000000,8.816328>}
box{<0,0,-0.038100><0.245741,0.036000,0.038100> rotate<0,0.000000,0> translate<59.131141,0.000000,8.816328> }
cylinder{<0,0,0><0,0.036000,0>0.038100 translate<59.630084,0.000000,8.079100>}
cylinder{<0,0,0><0,0.036000,0>0.038100 translate<59.630084,0.000000,8.816328>}
box{<0,0,-0.038100><0.737228,0.036000,0.038100> rotate<0,90.000000,0> translate<59.630084,0.000000,8.816328> }
cylinder{<0,0,0><0,0.036000,0>0.038100 translate<59.630084,0.000000,8.816328>}
cylinder{<0,0,0><0,0.036000,0>0.038100 translate<59.998697,0.000000,8.816328>}
box{<0,0,-0.038100><0.368612,0.036000,0.038100> rotate<0,0.000000,0> translate<59.630084,0.000000,8.816328> }
cylinder{<0,0,0><0,0.036000,0>0.038100 translate<59.998697,0.000000,8.816328>}
cylinder{<0,0,0><0,0.036000,0>0.038100 translate<60.121569,0.000000,8.693456>}
box{<0,0,-0.038100><0.173767,0.036000,0.038100> rotate<0,44.997030,0> translate<59.998697,0.000000,8.816328> }
cylinder{<0,0,0><0,0.036000,0>0.038100 translate<60.121569,0.000000,8.693456>}
cylinder{<0,0,0><0,0.036000,0>0.038100 translate<60.121569,0.000000,8.447712>}
box{<0,0,-0.038100><0.245744,0.036000,0.038100> rotate<0,-90.000000,0> translate<60.121569,0.000000,8.447712> }
cylinder{<0,0,0><0,0.036000,0>0.038100 translate<60.121569,0.000000,8.447712>}
cylinder{<0,0,0><0,0.036000,0>0.038100 translate<59.998697,0.000000,8.324841>}
box{<0,0,-0.038100><0.173767,0.036000,0.038100> rotate<0,-44.997030,0> translate<59.998697,0.000000,8.324841> }
cylinder{<0,0,0><0,0.036000,0>0.038100 translate<59.998697,0.000000,8.324841>}
cylinder{<0,0,0><0,0.036000,0>0.038100 translate<59.630084,0.000000,8.324841>}
box{<0,0,-0.038100><0.368612,0.036000,0.038100> rotate<0,0.000000,0> translate<59.630084,0.000000,8.324841> }
cylinder{<0,0,0><0,0.036000,0>0.038100 translate<6.981144,0.000000,27.545938>}
cylinder{<0,0,0><0,0.036000,0>0.038100 translate<6.858272,0.000000,27.423066>}
box{<0,0,-0.038100><0.173767,0.036000,0.038100> rotate<0,-44.997030,0> translate<6.858272,0.000000,27.423066> }
cylinder{<0,0,0><0,0.036000,0>0.038100 translate<6.858272,0.000000,27.423066>}
cylinder{<0,0,0><0,0.036000,0>0.038100 translate<6.858272,0.000000,27.177322>}
box{<0,0,-0.038100><0.245744,0.036000,0.038100> rotate<0,-90.000000,0> translate<6.858272,0.000000,27.177322> }
cylinder{<0,0,0><0,0.036000,0>0.038100 translate<6.858272,0.000000,27.177322>}
cylinder{<0,0,0><0,0.036000,0>0.038100 translate<6.981144,0.000000,27.054453>}
box{<0,0,-0.038100><0.173765,0.036000,0.038100> rotate<0,44.996302,0> translate<6.858272,0.000000,27.177322> }
cylinder{<0,0,0><0,0.036000,0>0.038100 translate<6.981144,0.000000,27.054453>}
cylinder{<0,0,0><0,0.036000,0>0.038100 translate<7.472631,0.000000,27.054453>}
box{<0,0,-0.038100><0.491487,0.036000,0.038100> rotate<0,0.000000,0> translate<6.981144,0.000000,27.054453> }
cylinder{<0,0,0><0,0.036000,0>0.038100 translate<7.472631,0.000000,27.054453>}
cylinder{<0,0,0><0,0.036000,0>0.038100 translate<7.595500,0.000000,27.177322>}
box{<0,0,-0.038100><0.173763,0.036000,0.038100> rotate<0,-44.997030,0> translate<7.472631,0.000000,27.054453> }
cylinder{<0,0,0><0,0.036000,0>0.038100 translate<7.595500,0.000000,27.177322>}
cylinder{<0,0,0><0,0.036000,0>0.038100 translate<7.595500,0.000000,27.423066>}
box{<0,0,-0.038100><0.245744,0.036000,0.038100> rotate<0,90.000000,0> translate<7.595500,0.000000,27.423066> }
cylinder{<0,0,0><0,0.036000,0>0.038100 translate<7.595500,0.000000,27.423066>}
cylinder{<0,0,0><0,0.036000,0>0.038100 translate<7.472631,0.000000,27.545938>}
box{<0,0,-0.038100><0.173765,0.036000,0.038100> rotate<0,44.997759,0> translate<7.472631,0.000000,27.545938> }
cylinder{<0,0,0><0,0.036000,0>0.038100 translate<7.595500,0.000000,27.802869>}
cylinder{<0,0,0><0,0.036000,0>0.038100 translate<7.104016,0.000000,27.802869>}
box{<0,0,-0.038100><0.491484,0.036000,0.038100> rotate<0,0.000000,0> translate<7.104016,0.000000,27.802869> }
cylinder{<0,0,0><0,0.036000,0>0.038100 translate<7.104016,0.000000,27.802869>}
cylinder{<0,0,0><0,0.036000,0>0.038100 translate<6.858272,0.000000,28.048609>}
box{<0,0,-0.038100><0.347532,0.036000,0.038100> rotate<0,44.996666,0> translate<6.858272,0.000000,28.048609> }
cylinder{<0,0,0><0,0.036000,0>0.038100 translate<6.858272,0.000000,28.048609>}
cylinder{<0,0,0><0,0.036000,0>0.038100 translate<7.104016,0.000000,28.294353>}
box{<0,0,-0.038100><0.347534,0.036000,0.038100> rotate<0,-44.997030,0> translate<6.858272,0.000000,28.048609> }
cylinder{<0,0,0><0,0.036000,0>0.038100 translate<7.104016,0.000000,28.294353>}
cylinder{<0,0,0><0,0.036000,0>0.038100 translate<7.595500,0.000000,28.294353>}
box{<0,0,-0.038100><0.491484,0.036000,0.038100> rotate<0,0.000000,0> translate<7.104016,0.000000,28.294353> }
cylinder{<0,0,0><0,0.036000,0>0.038100 translate<7.226888,0.000000,27.802869>}
cylinder{<0,0,0><0,0.036000,0>0.038100 translate<7.226888,0.000000,28.294353>}
box{<0,0,-0.038100><0.491484,0.036000,0.038100> rotate<0,90.000000,0> translate<7.226888,0.000000,28.294353> }
cylinder{<0,0,0><0,0.036000,0>0.038100 translate<7.595500,0.000000,28.551284>}
cylinder{<0,0,0><0,0.036000,0>0.038100 translate<6.858272,0.000000,28.551284>}
box{<0,0,-0.038100><0.737228,0.036000,0.038100> rotate<0,0.000000,0> translate<6.858272,0.000000,28.551284> }
cylinder{<0,0,0><0,0.036000,0>0.038100 translate<6.858272,0.000000,28.551284>}
cylinder{<0,0,0><0,0.036000,0>0.038100 translate<6.858272,0.000000,28.919897>}
box{<0,0,-0.038100><0.368613,0.036000,0.038100> rotate<0,90.000000,0> translate<6.858272,0.000000,28.919897> }
cylinder{<0,0,0><0,0.036000,0>0.038100 translate<6.858272,0.000000,28.919897>}
cylinder{<0,0,0><0,0.036000,0>0.038100 translate<6.981144,0.000000,29.042769>}
box{<0,0,-0.038100><0.173767,0.036000,0.038100> rotate<0,-44.997030,0> translate<6.858272,0.000000,28.919897> }
cylinder{<0,0,0><0,0.036000,0>0.038100 translate<6.981144,0.000000,29.042769>}
cylinder{<0,0,0><0,0.036000,0>0.038100 translate<7.226888,0.000000,29.042769>}
box{<0,0,-0.038100><0.245744,0.036000,0.038100> rotate<0,0.000000,0> translate<6.981144,0.000000,29.042769> }
cylinder{<0,0,0><0,0.036000,0>0.038100 translate<7.226888,0.000000,29.042769>}
cylinder{<0,0,0><0,0.036000,0>0.038100 translate<7.349759,0.000000,28.919897>}
box{<0,0,-0.038100><0.173767,0.036000,0.038100> rotate<0,44.997030,0> translate<7.226888,0.000000,29.042769> }
cylinder{<0,0,0><0,0.036000,0>0.038100 translate<7.349759,0.000000,28.919897>}
cylinder{<0,0,0><0,0.036000,0>0.038100 translate<7.349759,0.000000,28.551284>}
box{<0,0,-0.038100><0.368613,0.036000,0.038100> rotate<0,-90.000000,0> translate<7.349759,0.000000,28.551284> }
cylinder{<0,0,0><0,0.036000,0>0.038100 translate<49.702100,0.000000,0.699900>}
cylinder{<0,0,0><0,0.036000,0>0.038100 translate<49.702100,0.000000,1.437128>}
box{<0,0,-0.038100><0.737228,0.036000,0.038100> rotate<0,90.000000,0> translate<49.702100,0.000000,1.437128> }
cylinder{<0,0,0><0,0.036000,0>0.038100 translate<49.702100,0.000000,1.437128>}
cylinder{<0,0,0><0,0.036000,0>0.038100 translate<50.070712,0.000000,1.437128>}
box{<0,0,-0.038100><0.368612,0.036000,0.038100> rotate<0,0.000000,0> translate<49.702100,0.000000,1.437128> }
cylinder{<0,0,0><0,0.036000,0>0.038100 translate<50.070712,0.000000,1.437128>}
cylinder{<0,0,0><0,0.036000,0>0.038100 translate<50.193584,0.000000,1.314256>}
box{<0,0,-0.038100><0.173767,0.036000,0.038100> rotate<0,44.997030,0> translate<50.070712,0.000000,1.437128> }
cylinder{<0,0,0><0,0.036000,0>0.038100 translate<50.193584,0.000000,1.314256>}
cylinder{<0,0,0><0,0.036000,0>0.038100 translate<50.193584,0.000000,1.191384>}
box{<0,0,-0.038100><0.122872,0.036000,0.038100> rotate<0,-90.000000,0> translate<50.193584,0.000000,1.191384> }
cylinder{<0,0,0><0,0.036000,0>0.038100 translate<50.193584,0.000000,1.191384>}
cylinder{<0,0,0><0,0.036000,0>0.038100 translate<50.070712,0.000000,1.068512>}
box{<0,0,-0.038100><0.173767,0.036000,0.038100> rotate<0,-44.997030,0> translate<50.070712,0.000000,1.068512> }
cylinder{<0,0,0><0,0.036000,0>0.038100 translate<50.070712,0.000000,1.068512>}
cylinder{<0,0,0><0,0.036000,0>0.038100 translate<50.193584,0.000000,0.945641>}
box{<0,0,-0.038100><0.173767,0.036000,0.038100> rotate<0,44.997030,0> translate<50.070712,0.000000,1.068512> }
cylinder{<0,0,0><0,0.036000,0>0.038100 translate<50.193584,0.000000,0.945641>}
cylinder{<0,0,0><0,0.036000,0>0.038100 translate<50.193584,0.000000,0.822769>}
box{<0,0,-0.038100><0.122872,0.036000,0.038100> rotate<0,-90.000000,0> translate<50.193584,0.000000,0.822769> }
cylinder{<0,0,0><0,0.036000,0>0.038100 translate<50.193584,0.000000,0.822769>}
cylinder{<0,0,0><0,0.036000,0>0.038100 translate<50.070712,0.000000,0.699900>}
box{<0,0,-0.038100><0.173765,0.036000,0.038100> rotate<0,-44.996302,0> translate<50.070712,0.000000,0.699900> }
cylinder{<0,0,0><0,0.036000,0>0.038100 translate<50.070712,0.000000,0.699900>}
cylinder{<0,0,0><0,0.036000,0>0.038100 translate<49.702100,0.000000,0.699900>}
box{<0,0,-0.038100><0.368612,0.036000,0.038100> rotate<0,0.000000,0> translate<49.702100,0.000000,0.699900> }
cylinder{<0,0,0><0,0.036000,0>0.038100 translate<49.702100,0.000000,1.068512>}
cylinder{<0,0,0><0,0.036000,0>0.038100 translate<50.070712,0.000000,1.068512>}
box{<0,0,-0.038100><0.368612,0.036000,0.038100> rotate<0,0.000000,0> translate<49.702100,0.000000,1.068512> }
cylinder{<0,0,0><0,0.036000,0>0.038100 translate<50.450516,0.000000,0.699900>}
cylinder{<0,0,0><0,0.036000,0>0.038100 translate<50.450516,0.000000,1.191384>}
box{<0,0,-0.038100><0.491484,0.036000,0.038100> rotate<0,90.000000,0> translate<50.450516,0.000000,1.191384> }
cylinder{<0,0,0><0,0.036000,0>0.038100 translate<50.450516,0.000000,1.191384>}
cylinder{<0,0,0><0,0.036000,0>0.038100 translate<50.696256,0.000000,1.437128>}
box{<0,0,-0.038100><0.347532,0.036000,0.038100> rotate<0,-44.997394,0> translate<50.450516,0.000000,1.191384> }
cylinder{<0,0,0><0,0.036000,0>0.038100 translate<50.696256,0.000000,1.437128>}
cylinder{<0,0,0><0,0.036000,0>0.038100 translate<50.942000,0.000000,1.191384>}
box{<0,0,-0.038100><0.347534,0.036000,0.038100> rotate<0,44.997030,0> translate<50.696256,0.000000,1.437128> }
cylinder{<0,0,0><0,0.036000,0>0.038100 translate<50.942000,0.000000,1.191384>}
cylinder{<0,0,0><0,0.036000,0>0.038100 translate<50.942000,0.000000,0.699900>}
box{<0,0,-0.038100><0.491484,0.036000,0.038100> rotate<0,-90.000000,0> translate<50.942000,0.000000,0.699900> }
cylinder{<0,0,0><0,0.036000,0>0.038100 translate<50.450516,0.000000,1.068512>}
cylinder{<0,0,0><0,0.036000,0>0.038100 translate<50.942000,0.000000,1.068512>}
box{<0,0,-0.038100><0.491484,0.036000,0.038100> rotate<0,0.000000,0> translate<50.450516,0.000000,1.068512> }
cylinder{<0,0,0><0,0.036000,0>0.038100 translate<51.444672,0.000000,0.699900>}
cylinder{<0,0,0><0,0.036000,0>0.038100 translate<51.444672,0.000000,1.437128>}
box{<0,0,-0.038100><0.737228,0.036000,0.038100> rotate<0,90.000000,0> translate<51.444672,0.000000,1.437128> }
cylinder{<0,0,0><0,0.036000,0>0.038100 translate<51.198931,0.000000,1.437128>}
cylinder{<0,0,0><0,0.036000,0>0.038100 translate<51.690416,0.000000,1.437128>}
box{<0,0,-0.038100><0.491484,0.036000,0.038100> rotate<0,0.000000,0> translate<51.198931,0.000000,1.437128> }
cylinder{<0,0,0><0,0.036000,0>0.038100 translate<52.438831,0.000000,1.437128>}
cylinder{<0,0,0><0,0.036000,0>0.038100 translate<51.947347,0.000000,1.437128>}
box{<0,0,-0.038100><0.491484,0.036000,0.038100> rotate<0,0.000000,0> translate<51.947347,0.000000,1.437128> }
cylinder{<0,0,0><0,0.036000,0>0.038100 translate<51.947347,0.000000,1.437128>}
cylinder{<0,0,0><0,0.036000,0>0.038100 translate<51.947347,0.000000,0.699900>}
box{<0,0,-0.038100><0.737228,0.036000,0.038100> rotate<0,-90.000000,0> translate<51.947347,0.000000,0.699900> }
cylinder{<0,0,0><0,0.036000,0>0.038100 translate<51.947347,0.000000,0.699900>}
cylinder{<0,0,0><0,0.036000,0>0.038100 translate<52.438831,0.000000,0.699900>}
box{<0,0,-0.038100><0.491484,0.036000,0.038100> rotate<0,0.000000,0> translate<51.947347,0.000000,0.699900> }
cylinder{<0,0,0><0,0.036000,0>0.038100 translate<51.947347,0.000000,1.068512>}
cylinder{<0,0,0><0,0.036000,0>0.038100 translate<52.193088,0.000000,1.068512>}
box{<0,0,-0.038100><0.245741,0.036000,0.038100> rotate<0,0.000000,0> translate<51.947347,0.000000,1.068512> }
cylinder{<0,0,0><0,0.036000,0>0.038100 translate<52.695763,0.000000,0.699900>}
cylinder{<0,0,0><0,0.036000,0>0.038100 translate<52.695763,0.000000,1.437128>}
box{<0,0,-0.038100><0.737228,0.036000,0.038100> rotate<0,90.000000,0> translate<52.695763,0.000000,1.437128> }
cylinder{<0,0,0><0,0.036000,0>0.038100 translate<52.695763,0.000000,1.437128>}
cylinder{<0,0,0><0,0.036000,0>0.038100 translate<53.064375,0.000000,1.437128>}
box{<0,0,-0.038100><0.368613,0.036000,0.038100> rotate<0,0.000000,0> translate<52.695763,0.000000,1.437128> }
cylinder{<0,0,0><0,0.036000,0>0.038100 translate<53.064375,0.000000,1.437128>}
cylinder{<0,0,0><0,0.036000,0>0.038100 translate<53.187247,0.000000,1.314256>}
box{<0,0,-0.038100><0.173767,0.036000,0.038100> rotate<0,44.997030,0> translate<53.064375,0.000000,1.437128> }
cylinder{<0,0,0><0,0.036000,0>0.038100 translate<53.187247,0.000000,1.314256>}
cylinder{<0,0,0><0,0.036000,0>0.038100 translate<53.187247,0.000000,1.068512>}
box{<0,0,-0.038100><0.245744,0.036000,0.038100> rotate<0,-90.000000,0> translate<53.187247,0.000000,1.068512> }
cylinder{<0,0,0><0,0.036000,0>0.038100 translate<53.187247,0.000000,1.068512>}
cylinder{<0,0,0><0,0.036000,0>0.038100 translate<53.064375,0.000000,0.945641>}
box{<0,0,-0.038100><0.173767,0.036000,0.038100> rotate<0,-44.997030,0> translate<53.064375,0.000000,0.945641> }
cylinder{<0,0,0><0,0.036000,0>0.038100 translate<53.064375,0.000000,0.945641>}
cylinder{<0,0,0><0,0.036000,0>0.038100 translate<52.695763,0.000000,0.945641>}
box{<0,0,-0.038100><0.368613,0.036000,0.038100> rotate<0,0.000000,0> translate<52.695763,0.000000,0.945641> }
cylinder{<0,0,0><0,0.036000,0>0.038100 translate<52.941503,0.000000,0.945641>}
cylinder{<0,0,0><0,0.036000,0>0.038100 translate<53.187247,0.000000,0.699900>}
box{<0,0,-0.038100><0.347532,0.036000,0.038100> rotate<0,44.996666,0> translate<52.941503,0.000000,0.945641> }
cylinder{<0,0,0><0,0.036000,0>0.038100 translate<53.444178,0.000000,0.699900>}
cylinder{<0,0,0><0,0.036000,0>0.038100 translate<53.689919,0.000000,0.699900>}
box{<0,0,-0.038100><0.245741,0.036000,0.038100> rotate<0,0.000000,0> translate<53.444178,0.000000,0.699900> }
cylinder{<0,0,0><0,0.036000,0>0.038100 translate<53.567047,0.000000,0.699900>}
cylinder{<0,0,0><0,0.036000,0>0.038100 translate<53.567047,0.000000,1.437128>}
box{<0,0,-0.038100><0.737228,0.036000,0.038100> rotate<0,90.000000,0> translate<53.567047,0.000000,1.437128> }
cylinder{<0,0,0><0,0.036000,0>0.038100 translate<53.444178,0.000000,1.437128>}
cylinder{<0,0,0><0,0.036000,0>0.038100 translate<53.689919,0.000000,1.437128>}
box{<0,0,-0.038100><0.245741,0.036000,0.038100> rotate<0,0.000000,0> translate<53.444178,0.000000,1.437128> }
cylinder{<0,0,0><0,0.036000,0>0.038100 translate<53.943122,0.000000,0.699900>}
cylinder{<0,0,0><0,0.036000,0>0.038100 translate<53.943122,0.000000,1.191384>}
box{<0,0,-0.038100><0.491484,0.036000,0.038100> rotate<0,90.000000,0> translate<53.943122,0.000000,1.191384> }
cylinder{<0,0,0><0,0.036000,0>0.038100 translate<53.943122,0.000000,1.191384>}
cylinder{<0,0,0><0,0.036000,0>0.038100 translate<54.188862,0.000000,1.437128>}
box{<0,0,-0.038100><0.347532,0.036000,0.038100> rotate<0,-44.997394,0> translate<53.943122,0.000000,1.191384> }
cylinder{<0,0,0><0,0.036000,0>0.038100 translate<54.188862,0.000000,1.437128>}
cylinder{<0,0,0><0,0.036000,0>0.038100 translate<54.434606,0.000000,1.191384>}
box{<0,0,-0.038100><0.347534,0.036000,0.038100> rotate<0,44.997030,0> translate<54.188862,0.000000,1.437128> }
cylinder{<0,0,0><0,0.036000,0>0.038100 translate<54.434606,0.000000,1.191384>}
cylinder{<0,0,0><0,0.036000,0>0.038100 translate<54.434606,0.000000,0.699900>}
box{<0,0,-0.038100><0.491484,0.036000,0.038100> rotate<0,-90.000000,0> translate<54.434606,0.000000,0.699900> }
cylinder{<0,0,0><0,0.036000,0>0.038100 translate<53.943122,0.000000,1.068512>}
cylinder{<0,0,0><0,0.036000,0>0.038100 translate<54.434606,0.000000,1.068512>}
box{<0,0,-0.038100><0.491484,0.036000,0.038100> rotate<0,0.000000,0> translate<53.943122,0.000000,1.068512> }
cylinder{<0,0,0><0,0.036000,0>0.050800 translate<8.669206,0.000000,6.666800>}
cylinder{<0,0,0><0,0.036000,0>0.050800 translate<8.669206,0.000000,7.836197>}
box{<0,0,-0.050800><1.169397,0.036000,0.050800> rotate<0,90.000000,0> translate<8.669206,0.000000,7.836197> }
cylinder{<0,0,0><0,0.036000,0>0.050800 translate<8.669206,0.000000,7.836197>}
cylinder{<0,0,0><0,0.036000,0>0.050800 translate<9.253903,0.000000,7.836197>}
box{<0,0,-0.050800><0.584697,0.036000,0.050800> rotate<0,0.000000,0> translate<8.669206,0.000000,7.836197> }
cylinder{<0,0,0><0,0.036000,0>0.050800 translate<9.253903,0.000000,7.836197>}
cylinder{<0,0,0><0,0.036000,0>0.050800 translate<9.448803,0.000000,7.641297>}
box{<0,0,-0.050800><0.275630,0.036000,0.050800> rotate<0,44.997030,0> translate<9.253903,0.000000,7.836197> }
cylinder{<0,0,0><0,0.036000,0>0.050800 translate<9.448803,0.000000,7.641297>}
cylinder{<0,0,0><0,0.036000,0>0.050800 translate<9.448803,0.000000,7.251497>}
box{<0,0,-0.050800><0.389800,0.036000,0.050800> rotate<0,-90.000000,0> translate<9.448803,0.000000,7.251497> }
cylinder{<0,0,0><0,0.036000,0>0.050800 translate<9.448803,0.000000,7.251497>}
cylinder{<0,0,0><0,0.036000,0>0.050800 translate<9.253903,0.000000,7.056597>}
box{<0,0,-0.050800><0.275630,0.036000,0.050800> rotate<0,-44.997030,0> translate<9.253903,0.000000,7.056597> }
cylinder{<0,0,0><0,0.036000,0>0.050800 translate<9.253903,0.000000,7.056597>}
cylinder{<0,0,0><0,0.036000,0>0.050800 translate<8.669206,0.000000,7.056597>}
box{<0,0,-0.050800><0.584697,0.036000,0.050800> rotate<0,0.000000,0> translate<8.669206,0.000000,7.056597> }
cylinder{<0,0,0><0,0.036000,0>0.050800 translate<9.059003,0.000000,7.056597>}
cylinder{<0,0,0><0,0.036000,0>0.050800 translate<9.448803,0.000000,6.666800>}
box{<0,0,-0.050800><0.551258,0.036000,0.050800> rotate<0,44.996801,0> translate<9.059003,0.000000,7.056597> }
cylinder{<0,0,0><0,0.036000,0>0.050800 translate<10.033500,0.000000,6.666800>}
cylinder{<0,0,0><0,0.036000,0>0.050800 translate<10.423300,0.000000,6.666800>}
box{<0,0,-0.050800><0.389800,0.036000,0.050800> rotate<0,0.000000,0> translate<10.033500,0.000000,6.666800> }
cylinder{<0,0,0><0,0.036000,0>0.050800 translate<10.423300,0.000000,6.666800>}
cylinder{<0,0,0><0,0.036000,0>0.050800 translate<10.618200,0.000000,6.861697>}
box{<0,0,-0.050800><0.275628,0.036000,0.050800> rotate<0,-44.996571,0> translate<10.423300,0.000000,6.666800> }
cylinder{<0,0,0><0,0.036000,0>0.050800 translate<10.618200,0.000000,6.861697>}
cylinder{<0,0,0><0,0.036000,0>0.050800 translate<10.618200,0.000000,7.251497>}
box{<0,0,-0.050800><0.389800,0.036000,0.050800> rotate<0,90.000000,0> translate<10.618200,0.000000,7.251497> }
cylinder{<0,0,0><0,0.036000,0>0.050800 translate<10.618200,0.000000,7.251497>}
cylinder{<0,0,0><0,0.036000,0>0.050800 translate<10.423300,0.000000,7.446397>}
box{<0,0,-0.050800><0.275630,0.036000,0.050800> rotate<0,44.997030,0> translate<10.423300,0.000000,7.446397> }
cylinder{<0,0,0><0,0.036000,0>0.050800 translate<10.423300,0.000000,7.446397>}
cylinder{<0,0,0><0,0.036000,0>0.050800 translate<10.033500,0.000000,7.446397>}
box{<0,0,-0.050800><0.389800,0.036000,0.050800> rotate<0,0.000000,0> translate<10.033500,0.000000,7.446397> }
cylinder{<0,0,0><0,0.036000,0>0.050800 translate<10.033500,0.000000,7.446397>}
cylinder{<0,0,0><0,0.036000,0>0.050800 translate<9.838603,0.000000,7.251497>}
box{<0,0,-0.050800><0.275628,0.036000,0.050800> rotate<0,-44.997489,0> translate<9.838603,0.000000,7.251497> }
cylinder{<0,0,0><0,0.036000,0>0.050800 translate<9.838603,0.000000,7.251497>}
cylinder{<0,0,0><0,0.036000,0>0.050800 translate<9.838603,0.000000,6.861697>}
box{<0,0,-0.050800><0.389800,0.036000,0.050800> rotate<0,-90.000000,0> translate<9.838603,0.000000,6.861697> }
cylinder{<0,0,0><0,0.036000,0>0.050800 translate<9.838603,0.000000,6.861697>}
cylinder{<0,0,0><0,0.036000,0>0.050800 translate<10.033500,0.000000,6.666800>}
box{<0,0,-0.050800><0.275626,0.036000,0.050800> rotate<0,44.997030,0> translate<9.838603,0.000000,6.861697> }
cylinder{<0,0,0><0,0.036000,0>0.050800 translate<11.008000,0.000000,7.836197>}
cylinder{<0,0,0><0,0.036000,0>0.050800 translate<11.008000,0.000000,6.666800>}
box{<0,0,-0.050800><1.169397,0.036000,0.050800> rotate<0,-90.000000,0> translate<11.008000,0.000000,6.666800> }
cylinder{<0,0,0><0,0.036000,0>0.050800 translate<11.008000,0.000000,6.666800>}
cylinder{<0,0,0><0,0.036000,0>0.050800 translate<11.592697,0.000000,6.666800>}
box{<0,0,-0.050800><0.584697,0.036000,0.050800> rotate<0,0.000000,0> translate<11.008000,0.000000,6.666800> }
cylinder{<0,0,0><0,0.036000,0>0.050800 translate<11.592697,0.000000,6.666800>}
cylinder{<0,0,0><0,0.036000,0>0.050800 translate<11.787597,0.000000,6.861697>}
box{<0,0,-0.050800><0.275628,0.036000,0.050800> rotate<0,-44.996571,0> translate<11.592697,0.000000,6.666800> }
cylinder{<0,0,0><0,0.036000,0>0.050800 translate<11.787597,0.000000,6.861697>}
cylinder{<0,0,0><0,0.036000,0>0.050800 translate<11.787597,0.000000,7.251497>}
box{<0,0,-0.050800><0.389800,0.036000,0.050800> rotate<0,90.000000,0> translate<11.787597,0.000000,7.251497> }
cylinder{<0,0,0><0,0.036000,0>0.050800 translate<11.787597,0.000000,7.251497>}
cylinder{<0,0,0><0,0.036000,0>0.050800 translate<11.592697,0.000000,7.446397>}
box{<0,0,-0.050800><0.275630,0.036000,0.050800> rotate<0,44.997030,0> translate<11.592697,0.000000,7.446397> }
cylinder{<0,0,0><0,0.036000,0>0.050800 translate<11.592697,0.000000,7.446397>}
cylinder{<0,0,0><0,0.036000,0>0.050800 translate<11.008000,0.000000,7.446397>}
box{<0,0,-0.050800><0.584697,0.036000,0.050800> rotate<0,0.000000,0> translate<11.008000,0.000000,7.446397> }
cylinder{<0,0,0><0,0.036000,0>0.050800 translate<12.372294,0.000000,6.666800>}
cylinder{<0,0,0><0,0.036000,0>0.050800 translate<12.762094,0.000000,6.666800>}
box{<0,0,-0.050800><0.389800,0.036000,0.050800> rotate<0,0.000000,0> translate<12.372294,0.000000,6.666800> }
cylinder{<0,0,0><0,0.036000,0>0.050800 translate<12.762094,0.000000,6.666800>}
cylinder{<0,0,0><0,0.036000,0>0.050800 translate<12.956994,0.000000,6.861697>}
box{<0,0,-0.050800><0.275628,0.036000,0.050800> rotate<0,-44.996571,0> translate<12.762094,0.000000,6.666800> }
cylinder{<0,0,0><0,0.036000,0>0.050800 translate<12.956994,0.000000,6.861697>}
cylinder{<0,0,0><0,0.036000,0>0.050800 translate<12.956994,0.000000,7.251497>}
box{<0,0,-0.050800><0.389800,0.036000,0.050800> rotate<0,90.000000,0> translate<12.956994,0.000000,7.251497> }
cylinder{<0,0,0><0,0.036000,0>0.050800 translate<12.956994,0.000000,7.251497>}
cylinder{<0,0,0><0,0.036000,0>0.050800 translate<12.762094,0.000000,7.446397>}
box{<0,0,-0.050800><0.275630,0.036000,0.050800> rotate<0,44.997030,0> translate<12.762094,0.000000,7.446397> }
cylinder{<0,0,0><0,0.036000,0>0.050800 translate<12.762094,0.000000,7.446397>}
cylinder{<0,0,0><0,0.036000,0>0.050800 translate<12.372294,0.000000,7.446397>}
box{<0,0,-0.050800><0.389800,0.036000,0.050800> rotate<0,0.000000,0> translate<12.372294,0.000000,7.446397> }
cylinder{<0,0,0><0,0.036000,0>0.050800 translate<12.372294,0.000000,7.446397>}
cylinder{<0,0,0><0,0.036000,0>0.050800 translate<12.177397,0.000000,7.251497>}
box{<0,0,-0.050800><0.275628,0.036000,0.050800> rotate<0,-44.997489,0> translate<12.177397,0.000000,7.251497> }
cylinder{<0,0,0><0,0.036000,0>0.050800 translate<12.177397,0.000000,7.251497>}
cylinder{<0,0,0><0,0.036000,0>0.050800 translate<12.177397,0.000000,6.861697>}
box{<0,0,-0.050800><0.389800,0.036000,0.050800> rotate<0,-90.000000,0> translate<12.177397,0.000000,6.861697> }
cylinder{<0,0,0><0,0.036000,0>0.050800 translate<12.177397,0.000000,6.861697>}
cylinder{<0,0,0><0,0.036000,0>0.050800 translate<12.372294,0.000000,6.666800>}
box{<0,0,-0.050800><0.275626,0.036000,0.050800> rotate<0,44.997030,0> translate<12.177397,0.000000,6.861697> }
cylinder{<0,0,0><0,0.036000,0>0.050800 translate<13.346794,0.000000,6.666800>}
cylinder{<0,0,0><0,0.036000,0>0.050800 translate<13.346794,0.000000,7.836197>}
box{<0,0,-0.050800><1.169397,0.036000,0.050800> rotate<0,90.000000,0> translate<13.346794,0.000000,7.836197> }
cylinder{<0,0,0><0,0.036000,0>0.050800 translate<13.346794,0.000000,7.836197>}
cylinder{<0,0,0><0,0.036000,0>0.050800 translate<14.126391,0.000000,7.836197>}
box{<0,0,-0.050800><0.779597,0.036000,0.050800> rotate<0,0.000000,0> translate<13.346794,0.000000,7.836197> }
cylinder{<0,0,0><0,0.036000,0>0.050800 translate<13.346794,0.000000,7.251497>}
cylinder{<0,0,0><0,0.036000,0>0.050800 translate<13.736591,0.000000,7.251497>}
box{<0,0,-0.050800><0.389797,0.036000,0.050800> rotate<0,0.000000,0> translate<13.346794,0.000000,7.251497> }
cylinder{<0,0,0><0,0.036000,0>0.050800 translate<15.295788,0.000000,7.836197>}
cylinder{<0,0,0><0,0.036000,0>0.050800 translate<14.516191,0.000000,7.836197>}
box{<0,0,-0.050800><0.779597,0.036000,0.050800> rotate<0,0.000000,0> translate<14.516191,0.000000,7.836197> }
cylinder{<0,0,0><0,0.036000,0>0.050800 translate<14.516191,0.000000,7.836197>}
cylinder{<0,0,0><0,0.036000,0>0.050800 translate<14.516191,0.000000,6.666800>}
box{<0,0,-0.050800><1.169397,0.036000,0.050800> rotate<0,-90.000000,0> translate<14.516191,0.000000,6.666800> }
cylinder{<0,0,0><0,0.036000,0>0.050800 translate<14.516191,0.000000,6.666800>}
cylinder{<0,0,0><0,0.036000,0>0.050800 translate<15.295788,0.000000,6.666800>}
box{<0,0,-0.050800><0.779597,0.036000,0.050800> rotate<0,0.000000,0> translate<14.516191,0.000000,6.666800> }
cylinder{<0,0,0><0,0.036000,0>0.050800 translate<14.516191,0.000000,7.251497>}
cylinder{<0,0,0><0,0.036000,0>0.050800 translate<14.905988,0.000000,7.251497>}
box{<0,0,-0.050800><0.389797,0.036000,0.050800> rotate<0,0.000000,0> translate<14.516191,0.000000,7.251497> }
cylinder{<0,0,0><0,0.036000,0>0.050800 translate<15.685588,0.000000,6.666800>}
cylinder{<0,0,0><0,0.036000,0>0.050800 translate<16.075384,0.000000,6.666800>}
box{<0,0,-0.050800><0.389797,0.036000,0.050800> rotate<0,0.000000,0> translate<15.685588,0.000000,6.666800> }
cylinder{<0,0,0><0,0.036000,0>0.050800 translate<15.880484,0.000000,6.666800>}
cylinder{<0,0,0><0,0.036000,0>0.050800 translate<15.880484,0.000000,7.836197>}
box{<0,0,-0.050800><1.169397,0.036000,0.050800> rotate<0,90.000000,0> translate<15.880484,0.000000,7.836197> }
cylinder{<0,0,0><0,0.036000,0>0.050800 translate<15.685588,0.000000,7.836197>}
cylinder{<0,0,0><0,0.036000,0>0.050800 translate<16.075384,0.000000,7.836197>}
box{<0,0,-0.050800><0.389797,0.036000,0.050800> rotate<0,0.000000,0> translate<15.685588,0.000000,7.836197> }
cylinder{<0,0,0><0,0.036000,0>0.038100 translate<10.800100,0.000000,5.528900>}
cylinder{<0,0,0><0,0.036000,0>0.038100 translate<10.800100,0.000000,6.266128>}
box{<0,0,-0.038100><0.737228,0.036000,0.038100> rotate<0,90.000000,0> translate<10.800100,0.000000,6.266128> }
cylinder{<0,0,0><0,0.036000,0>0.038100 translate<10.800100,0.000000,6.266128>}
cylinder{<0,0,0><0,0.036000,0>0.038100 translate<11.291584,0.000000,6.266128>}
box{<0,0,-0.038100><0.491484,0.036000,0.038100> rotate<0,0.000000,0> translate<10.800100,0.000000,6.266128> }
cylinder{<0,0,0><0,0.036000,0>0.038100 translate<10.800100,0.000000,5.897513>}
cylinder{<0,0,0><0,0.036000,0>0.038100 translate<11.045841,0.000000,5.897513>}
box{<0,0,-0.038100><0.245741,0.036000,0.038100> rotate<0,0.000000,0> translate<10.800100,0.000000,5.897513> }
cylinder{<0,0,0><0,0.036000,0>0.038100 translate<11.548516,0.000000,6.020384>}
cylinder{<0,0,0><0,0.036000,0>0.038100 translate<11.794256,0.000000,6.266128>}
box{<0,0,-0.038100><0.347532,0.036000,0.038100> rotate<0,-44.997394,0> translate<11.548516,0.000000,6.020384> }
cylinder{<0,0,0><0,0.036000,0>0.038100 translate<11.794256,0.000000,6.266128>}
cylinder{<0,0,0><0,0.036000,0>0.038100 translate<11.794256,0.000000,5.528900>}
box{<0,0,-0.038100><0.737228,0.036000,0.038100> rotate<0,-90.000000,0> translate<11.794256,0.000000,5.528900> }
cylinder{<0,0,0><0,0.036000,0>0.038100 translate<11.548516,0.000000,5.528900>}
cylinder{<0,0,0><0,0.036000,0>0.038100 translate<12.040000,0.000000,5.528900>}
box{<0,0,-0.038100><0.491484,0.036000,0.038100> rotate<0,0.000000,0> translate<11.548516,0.000000,5.528900> }
cylinder{<0,0,0><0,0.036000,0>0.038100 translate<12.296931,0.000000,6.143256>}
cylinder{<0,0,0><0,0.036000,0>0.038100 translate<12.419800,0.000000,6.266128>}
box{<0,0,-0.038100><0.173765,0.036000,0.038100> rotate<0,-44.997759,0> translate<12.296931,0.000000,6.143256> }
cylinder{<0,0,0><0,0.036000,0>0.038100 translate<12.419800,0.000000,6.266128>}
cylinder{<0,0,0><0,0.036000,0>0.038100 translate<12.665544,0.000000,6.266128>}
box{<0,0,-0.038100><0.245744,0.036000,0.038100> rotate<0,0.000000,0> translate<12.419800,0.000000,6.266128> }
cylinder{<0,0,0><0,0.036000,0>0.038100 translate<12.665544,0.000000,6.266128>}
cylinder{<0,0,0><0,0.036000,0>0.038100 translate<12.788416,0.000000,6.143256>}
box{<0,0,-0.038100><0.173767,0.036000,0.038100> rotate<0,44.997030,0> translate<12.665544,0.000000,6.266128> }
cylinder{<0,0,0><0,0.036000,0>0.038100 translate<12.788416,0.000000,6.143256>}
cylinder{<0,0,0><0,0.036000,0>0.038100 translate<12.788416,0.000000,6.020384>}
box{<0,0,-0.038100><0.122872,0.036000,0.038100> rotate<0,-90.000000,0> translate<12.788416,0.000000,6.020384> }
cylinder{<0,0,0><0,0.036000,0>0.038100 translate<12.788416,0.000000,6.020384>}
cylinder{<0,0,0><0,0.036000,0>0.038100 translate<12.665544,0.000000,5.897513>}
box{<0,0,-0.038100><0.173767,0.036000,0.038100> rotate<0,-44.997030,0> translate<12.665544,0.000000,5.897513> }
cylinder{<0,0,0><0,0.036000,0>0.038100 translate<12.665544,0.000000,5.897513>}
cylinder{<0,0,0><0,0.036000,0>0.038100 translate<12.788416,0.000000,5.774641>}
box{<0,0,-0.038100><0.173767,0.036000,0.038100> rotate<0,44.997030,0> translate<12.665544,0.000000,5.897513> }
cylinder{<0,0,0><0,0.036000,0>0.038100 translate<12.788416,0.000000,5.774641>}
cylinder{<0,0,0><0,0.036000,0>0.038100 translate<12.788416,0.000000,5.651769>}
box{<0,0,-0.038100><0.122872,0.036000,0.038100> rotate<0,-90.000000,0> translate<12.788416,0.000000,5.651769> }
cylinder{<0,0,0><0,0.036000,0>0.038100 translate<12.788416,0.000000,5.651769>}
cylinder{<0,0,0><0,0.036000,0>0.038100 translate<12.665544,0.000000,5.528900>}
box{<0,0,-0.038100><0.173765,0.036000,0.038100> rotate<0,-44.996302,0> translate<12.665544,0.000000,5.528900> }
cylinder{<0,0,0><0,0.036000,0>0.038100 translate<12.665544,0.000000,5.528900>}
cylinder{<0,0,0><0,0.036000,0>0.038100 translate<12.419800,0.000000,5.528900>}
box{<0,0,-0.038100><0.245744,0.036000,0.038100> rotate<0,0.000000,0> translate<12.419800,0.000000,5.528900> }
cylinder{<0,0,0><0,0.036000,0>0.038100 translate<12.419800,0.000000,5.528900>}
cylinder{<0,0,0><0,0.036000,0>0.038100 translate<12.296931,0.000000,5.651769>}
box{<0,0,-0.038100><0.173763,0.036000,0.038100> rotate<0,44.997030,0> translate<12.296931,0.000000,5.651769> }
cylinder{<0,0,0><0,0.036000,0>0.038100 translate<12.296931,0.000000,5.651769>}
cylinder{<0,0,0><0,0.036000,0>0.038100 translate<12.296931,0.000000,5.774641>}
box{<0,0,-0.038100><0.122872,0.036000,0.038100> rotate<0,90.000000,0> translate<12.296931,0.000000,5.774641> }
cylinder{<0,0,0><0,0.036000,0>0.038100 translate<12.296931,0.000000,5.774641>}
cylinder{<0,0,0><0,0.036000,0>0.038100 translate<12.419800,0.000000,5.897513>}
box{<0,0,-0.038100><0.173765,0.036000,0.038100> rotate<0,-44.997759,0> translate<12.296931,0.000000,5.774641> }
cylinder{<0,0,0><0,0.036000,0>0.038100 translate<12.419800,0.000000,5.897513>}
cylinder{<0,0,0><0,0.036000,0>0.038100 translate<12.296931,0.000000,6.020384>}
box{<0,0,-0.038100><0.173765,0.036000,0.038100> rotate<0,44.997759,0> translate<12.296931,0.000000,6.020384> }
cylinder{<0,0,0><0,0.036000,0>0.038100 translate<12.296931,0.000000,6.020384>}
cylinder{<0,0,0><0,0.036000,0>0.038100 translate<12.296931,0.000000,6.143256>}
box{<0,0,-0.038100><0.122872,0.036000,0.038100> rotate<0,90.000000,0> translate<12.296931,0.000000,6.143256> }
cylinder{<0,0,0><0,0.036000,0>0.038100 translate<12.419800,0.000000,5.897513>}
cylinder{<0,0,0><0,0.036000,0>0.038100 translate<12.665544,0.000000,5.897513>}
box{<0,0,-0.038100><0.245744,0.036000,0.038100> rotate<0,0.000000,0> translate<12.419800,0.000000,5.897513> }
cylinder{<0,0,0><0,0.036000,0>0.038100 translate<13.045347,0.000000,5.651769>}
cylinder{<0,0,0><0,0.036000,0>0.038100 translate<13.045347,0.000000,6.143256>}
box{<0,0,-0.038100><0.491487,0.036000,0.038100> rotate<0,90.000000,0> translate<13.045347,0.000000,6.143256> }
cylinder{<0,0,0><0,0.036000,0>0.038100 translate<13.045347,0.000000,6.143256>}
cylinder{<0,0,0><0,0.036000,0>0.038100 translate<13.168216,0.000000,6.266128>}
box{<0,0,-0.038100><0.173765,0.036000,0.038100> rotate<0,-44.997759,0> translate<13.045347,0.000000,6.143256> }
cylinder{<0,0,0><0,0.036000,0>0.038100 translate<13.168216,0.000000,6.266128>}
cylinder{<0,0,0><0,0.036000,0>0.038100 translate<13.413959,0.000000,6.266128>}
box{<0,0,-0.038100><0.245744,0.036000,0.038100> rotate<0,0.000000,0> translate<13.168216,0.000000,6.266128> }
cylinder{<0,0,0><0,0.036000,0>0.038100 translate<13.413959,0.000000,6.266128>}
cylinder{<0,0,0><0,0.036000,0>0.038100 translate<13.536831,0.000000,6.143256>}
box{<0,0,-0.038100><0.173767,0.036000,0.038100> rotate<0,44.997030,0> translate<13.413959,0.000000,6.266128> }
cylinder{<0,0,0><0,0.036000,0>0.038100 translate<13.536831,0.000000,6.143256>}
cylinder{<0,0,0><0,0.036000,0>0.038100 translate<13.536831,0.000000,5.651769>}
box{<0,0,-0.038100><0.491487,0.036000,0.038100> rotate<0,-90.000000,0> translate<13.536831,0.000000,5.651769> }
cylinder{<0,0,0><0,0.036000,0>0.038100 translate<13.536831,0.000000,5.651769>}
cylinder{<0,0,0><0,0.036000,0>0.038100 translate<13.413959,0.000000,5.528900>}
box{<0,0,-0.038100><0.173765,0.036000,0.038100> rotate<0,-44.996302,0> translate<13.413959,0.000000,5.528900> }
cylinder{<0,0,0><0,0.036000,0>0.038100 translate<13.413959,0.000000,5.528900>}
cylinder{<0,0,0><0,0.036000,0>0.038100 translate<13.168216,0.000000,5.528900>}
box{<0,0,-0.038100><0.245744,0.036000,0.038100> rotate<0,0.000000,0> translate<13.168216,0.000000,5.528900> }
cylinder{<0,0,0><0,0.036000,0>0.038100 translate<13.168216,0.000000,5.528900>}
cylinder{<0,0,0><0,0.036000,0>0.038100 translate<13.045347,0.000000,5.651769>}
box{<0,0,-0.038100><0.173763,0.036000,0.038100> rotate<0,44.997030,0> translate<13.045347,0.000000,5.651769> }
cylinder{<0,0,0><0,0.036000,0>0.038100 translate<13.045347,0.000000,5.651769>}
cylinder{<0,0,0><0,0.036000,0>0.038100 translate<13.536831,0.000000,6.143256>}
box{<0,0,-0.038100><0.695066,0.036000,0.038100> rotate<0,-44.997212,0> translate<13.045347,0.000000,5.651769> }
cylinder{<0,0,0><0,0.036000,0>0.038100 translate<13.793763,0.000000,5.897513>}
cylinder{<0,0,0><0,0.036000,0>0.038100 translate<14.285247,0.000000,5.897513>}
box{<0,0,-0.038100><0.491484,0.036000,0.038100> rotate<0,0.000000,0> translate<13.793763,0.000000,5.897513> }
cylinder{<0,0,0><0,0.036000,0>0.038100 translate<15.033663,0.000000,5.528900>}
cylinder{<0,0,0><0,0.036000,0>0.038100 translate<14.542178,0.000000,5.528900>}
box{<0,0,-0.038100><0.491484,0.036000,0.038100> rotate<0,0.000000,0> translate<14.542178,0.000000,5.528900> }
cylinder{<0,0,0><0,0.036000,0>0.038100 translate<14.542178,0.000000,5.528900>}
cylinder{<0,0,0><0,0.036000,0>0.038100 translate<15.033663,0.000000,6.020384>}
box{<0,0,-0.038100><0.695064,0.036000,0.038100> rotate<0,-44.997030,0> translate<14.542178,0.000000,5.528900> }
cylinder{<0,0,0><0,0.036000,0>0.038100 translate<15.033663,0.000000,6.020384>}
cylinder{<0,0,0><0,0.036000,0>0.038100 translate<15.033663,0.000000,6.143256>}
box{<0,0,-0.038100><0.122872,0.036000,0.038100> rotate<0,90.000000,0> translate<15.033663,0.000000,6.143256> }
cylinder{<0,0,0><0,0.036000,0>0.038100 translate<15.033663,0.000000,6.143256>}
cylinder{<0,0,0><0,0.036000,0>0.038100 translate<14.910791,0.000000,6.266128>}
box{<0,0,-0.038100><0.173767,0.036000,0.038100> rotate<0,44.997030,0> translate<14.910791,0.000000,6.266128> }
cylinder{<0,0,0><0,0.036000,0>0.038100 translate<14.910791,0.000000,6.266128>}
cylinder{<0,0,0><0,0.036000,0>0.038100 translate<14.665047,0.000000,6.266128>}
box{<0,0,-0.038100><0.245744,0.036000,0.038100> rotate<0,0.000000,0> translate<14.665047,0.000000,6.266128> }
cylinder{<0,0,0><0,0.036000,0>0.038100 translate<14.665047,0.000000,6.266128>}
cylinder{<0,0,0><0,0.036000,0>0.038100 translate<14.542178,0.000000,6.143256>}
box{<0,0,-0.038100><0.173765,0.036000,0.038100> rotate<0,-44.997759,0> translate<14.542178,0.000000,6.143256> }
cylinder{<0,0,0><0,0.036000,0>0.038100 translate<15.290594,0.000000,5.651769>}
cylinder{<0,0,0><0,0.036000,0>0.038100 translate<15.290594,0.000000,6.143256>}
box{<0,0,-0.038100><0.491487,0.036000,0.038100> rotate<0,90.000000,0> translate<15.290594,0.000000,6.143256> }
cylinder{<0,0,0><0,0.036000,0>0.038100 translate<15.290594,0.000000,6.143256>}
cylinder{<0,0,0><0,0.036000,0>0.038100 translate<15.413463,0.000000,6.266128>}
box{<0,0,-0.038100><0.173765,0.036000,0.038100> rotate<0,-44.997759,0> translate<15.290594,0.000000,6.143256> }
cylinder{<0,0,0><0,0.036000,0>0.038100 translate<15.413463,0.000000,6.266128>}
cylinder{<0,0,0><0,0.036000,0>0.038100 translate<15.659206,0.000000,6.266128>}
box{<0,0,-0.038100><0.245744,0.036000,0.038100> rotate<0,0.000000,0> translate<15.413463,0.000000,6.266128> }
cylinder{<0,0,0><0,0.036000,0>0.038100 translate<15.659206,0.000000,6.266128>}
cylinder{<0,0,0><0,0.036000,0>0.038100 translate<15.782078,0.000000,6.143256>}
box{<0,0,-0.038100><0.173767,0.036000,0.038100> rotate<0,44.997030,0> translate<15.659206,0.000000,6.266128> }
cylinder{<0,0,0><0,0.036000,0>0.038100 translate<15.782078,0.000000,6.143256>}
cylinder{<0,0,0><0,0.036000,0>0.038100 translate<15.782078,0.000000,5.651769>}
box{<0,0,-0.038100><0.491487,0.036000,0.038100> rotate<0,-90.000000,0> translate<15.782078,0.000000,5.651769> }
cylinder{<0,0,0><0,0.036000,0>0.038100 translate<15.782078,0.000000,5.651769>}
cylinder{<0,0,0><0,0.036000,0>0.038100 translate<15.659206,0.000000,5.528900>}
box{<0,0,-0.038100><0.173765,0.036000,0.038100> rotate<0,-44.996302,0> translate<15.659206,0.000000,5.528900> }
cylinder{<0,0,0><0,0.036000,0>0.038100 translate<15.659206,0.000000,5.528900>}
cylinder{<0,0,0><0,0.036000,0>0.038100 translate<15.413463,0.000000,5.528900>}
box{<0,0,-0.038100><0.245744,0.036000,0.038100> rotate<0,0.000000,0> translate<15.413463,0.000000,5.528900> }
cylinder{<0,0,0><0,0.036000,0>0.038100 translate<15.413463,0.000000,5.528900>}
cylinder{<0,0,0><0,0.036000,0>0.038100 translate<15.290594,0.000000,5.651769>}
box{<0,0,-0.038100><0.173763,0.036000,0.038100> rotate<0,44.997030,0> translate<15.290594,0.000000,5.651769> }
cylinder{<0,0,0><0,0.036000,0>0.038100 translate<15.290594,0.000000,5.651769>}
cylinder{<0,0,0><0,0.036000,0>0.038100 translate<15.782078,0.000000,6.143256>}
box{<0,0,-0.038100><0.695066,0.036000,0.038100> rotate<0,-44.997212,0> translate<15.290594,0.000000,5.651769> }
cylinder{<0,0,0><0,0.036000,0>0.038100 translate<16.039009,0.000000,6.020384>}
cylinder{<0,0,0><0,0.036000,0>0.038100 translate<16.284750,0.000000,6.266128>}
box{<0,0,-0.038100><0.347532,0.036000,0.038100> rotate<0,-44.997394,0> translate<16.039009,0.000000,6.020384> }
cylinder{<0,0,0><0,0.036000,0>0.038100 translate<16.284750,0.000000,6.266128>}
cylinder{<0,0,0><0,0.036000,0>0.038100 translate<16.284750,0.000000,5.528900>}
box{<0,0,-0.038100><0.737228,0.036000,0.038100> rotate<0,-90.000000,0> translate<16.284750,0.000000,5.528900> }
cylinder{<0,0,0><0,0.036000,0>0.038100 translate<16.039009,0.000000,5.528900>}
cylinder{<0,0,0><0,0.036000,0>0.038100 translate<16.530494,0.000000,5.528900>}
box{<0,0,-0.038100><0.491484,0.036000,0.038100> rotate<0,0.000000,0> translate<16.039009,0.000000,5.528900> }
cylinder{<0,0,0><0,0.036000,0>0.038100 translate<16.787425,0.000000,5.651769>}
cylinder{<0,0,0><0,0.036000,0>0.038100 translate<16.787425,0.000000,6.143256>}
box{<0,0,-0.038100><0.491487,0.036000,0.038100> rotate<0,90.000000,0> translate<16.787425,0.000000,6.143256> }
cylinder{<0,0,0><0,0.036000,0>0.038100 translate<16.787425,0.000000,6.143256>}
cylinder{<0,0,0><0,0.036000,0>0.038100 translate<16.910294,0.000000,6.266128>}
box{<0,0,-0.038100><0.173765,0.036000,0.038100> rotate<0,-44.997759,0> translate<16.787425,0.000000,6.143256> }
cylinder{<0,0,0><0,0.036000,0>0.038100 translate<16.910294,0.000000,6.266128>}
cylinder{<0,0,0><0,0.036000,0>0.038100 translate<17.156038,0.000000,6.266128>}
box{<0,0,-0.038100><0.245744,0.036000,0.038100> rotate<0,0.000000,0> translate<16.910294,0.000000,6.266128> }
cylinder{<0,0,0><0,0.036000,0>0.038100 translate<17.156038,0.000000,6.266128>}
cylinder{<0,0,0><0,0.036000,0>0.038100 translate<17.278909,0.000000,6.143256>}
box{<0,0,-0.038100><0.173767,0.036000,0.038100> rotate<0,44.997030,0> translate<17.156038,0.000000,6.266128> }
cylinder{<0,0,0><0,0.036000,0>0.038100 translate<17.278909,0.000000,6.143256>}
cylinder{<0,0,0><0,0.036000,0>0.038100 translate<17.278909,0.000000,5.651769>}
box{<0,0,-0.038100><0.491487,0.036000,0.038100> rotate<0,-90.000000,0> translate<17.278909,0.000000,5.651769> }
cylinder{<0,0,0><0,0.036000,0>0.038100 translate<17.278909,0.000000,5.651769>}
cylinder{<0,0,0><0,0.036000,0>0.038100 translate<17.156038,0.000000,5.528900>}
box{<0,0,-0.038100><0.173765,0.036000,0.038100> rotate<0,-44.996302,0> translate<17.156038,0.000000,5.528900> }
cylinder{<0,0,0><0,0.036000,0>0.038100 translate<17.156038,0.000000,5.528900>}
cylinder{<0,0,0><0,0.036000,0>0.038100 translate<16.910294,0.000000,5.528900>}
box{<0,0,-0.038100><0.245744,0.036000,0.038100> rotate<0,0.000000,0> translate<16.910294,0.000000,5.528900> }
cylinder{<0,0,0><0,0.036000,0>0.038100 translate<16.910294,0.000000,5.528900>}
cylinder{<0,0,0><0,0.036000,0>0.038100 translate<16.787425,0.000000,5.651769>}
box{<0,0,-0.038100><0.173763,0.036000,0.038100> rotate<0,44.997030,0> translate<16.787425,0.000000,5.651769> }
cylinder{<0,0,0><0,0.036000,0>0.038100 translate<16.787425,0.000000,5.651769>}
cylinder{<0,0,0><0,0.036000,0>0.038100 translate<17.278909,0.000000,6.143256>}
box{<0,0,-0.038100><0.695066,0.036000,0.038100> rotate<0,-44.997212,0> translate<16.787425,0.000000,5.651769> }
cylinder{<0,0,0><0,0.036000,0>0.038100 translate<12.562100,0.000000,4.420100>}
cylinder{<0,0,0><0,0.036000,0>0.038100 translate<12.562100,0.000000,4.911584>}
box{<0,0,-0.038100><0.491484,0.036000,0.038100> rotate<0,90.000000,0> translate<12.562100,0.000000,4.911584> }
cylinder{<0,0,0><0,0.036000,0>0.038100 translate<12.562100,0.000000,4.665841>}
cylinder{<0,0,0><0,0.036000,0>0.038100 translate<12.807841,0.000000,4.911584>}
box{<0,0,-0.038100><0.347532,0.036000,0.038100> rotate<0,-44.997394,0> translate<12.562100,0.000000,4.665841> }
cylinder{<0,0,0><0,0.036000,0>0.038100 translate<12.807841,0.000000,4.911584>}
cylinder{<0,0,0><0,0.036000,0>0.038100 translate<12.930713,0.000000,4.911584>}
box{<0,0,-0.038100><0.122872,0.036000,0.038100> rotate<0,0.000000,0> translate<12.807841,0.000000,4.911584> }
cylinder{<0,0,0><0,0.036000,0>0.038100 translate<13.554391,0.000000,4.420100>}
cylinder{<0,0,0><0,0.036000,0>0.038100 translate<13.308647,0.000000,4.420100>}
box{<0,0,-0.038100><0.245744,0.036000,0.038100> rotate<0,0.000000,0> translate<13.308647,0.000000,4.420100> }
cylinder{<0,0,0><0,0.036000,0>0.038100 translate<13.308647,0.000000,4.420100>}
cylinder{<0,0,0><0,0.036000,0>0.038100 translate<13.185778,0.000000,4.542969>}
box{<0,0,-0.038100><0.173763,0.036000,0.038100> rotate<0,44.997030,0> translate<13.185778,0.000000,4.542969> }
cylinder{<0,0,0><0,0.036000,0>0.038100 translate<13.185778,0.000000,4.542969>}
cylinder{<0,0,0><0,0.036000,0>0.038100 translate<13.185778,0.000000,4.788712>}
box{<0,0,-0.038100><0.245744,0.036000,0.038100> rotate<0,90.000000,0> translate<13.185778,0.000000,4.788712> }
cylinder{<0,0,0><0,0.036000,0>0.038100 translate<13.185778,0.000000,4.788712>}
cylinder{<0,0,0><0,0.036000,0>0.038100 translate<13.308647,0.000000,4.911584>}
box{<0,0,-0.038100><0.173765,0.036000,0.038100> rotate<0,-44.997759,0> translate<13.185778,0.000000,4.788712> }
cylinder{<0,0,0><0,0.036000,0>0.038100 translate<13.308647,0.000000,4.911584>}
cylinder{<0,0,0><0,0.036000,0>0.038100 translate<13.554391,0.000000,4.911584>}
box{<0,0,-0.038100><0.245744,0.036000,0.038100> rotate<0,0.000000,0> translate<13.308647,0.000000,4.911584> }
cylinder{<0,0,0><0,0.036000,0>0.038100 translate<13.554391,0.000000,4.911584>}
cylinder{<0,0,0><0,0.036000,0>0.038100 translate<13.677263,0.000000,4.788712>}
box{<0,0,-0.038100><0.173767,0.036000,0.038100> rotate<0,44.997030,0> translate<13.554391,0.000000,4.911584> }
cylinder{<0,0,0><0,0.036000,0>0.038100 translate<13.677263,0.000000,4.788712>}
cylinder{<0,0,0><0,0.036000,0>0.038100 translate<13.677263,0.000000,4.665841>}
box{<0,0,-0.038100><0.122872,0.036000,0.038100> rotate<0,-90.000000,0> translate<13.677263,0.000000,4.665841> }
cylinder{<0,0,0><0,0.036000,0>0.038100 translate<13.677263,0.000000,4.665841>}
cylinder{<0,0,0><0,0.036000,0>0.038100 translate<13.185778,0.000000,4.665841>}
box{<0,0,-0.038100><0.491484,0.036000,0.038100> rotate<0,0.000000,0> translate<13.185778,0.000000,4.665841> }
cylinder{<0,0,0><0,0.036000,0>0.038100 translate<13.934194,0.000000,4.911584>}
cylinder{<0,0,0><0,0.036000,0>0.038100 translate<14.179934,0.000000,4.420100>}
box{<0,0,-0.038100><0.549496,0.036000,0.038100> rotate<0,63.430908,0> translate<13.934194,0.000000,4.911584> }
cylinder{<0,0,0><0,0.036000,0>0.038100 translate<14.179934,0.000000,4.420100>}
cylinder{<0,0,0><0,0.036000,0>0.038100 translate<14.425678,0.000000,4.911584>}
box{<0,0,-0.038100><0.549497,0.036000,0.038100> rotate<0,-63.430617,0> translate<14.179934,0.000000,4.420100> }
cylinder{<0,0,0><0,0.036000,0>0.038100 translate<14.682609,0.000000,4.420100>}
cylinder{<0,0,0><0,0.036000,0>0.038100 translate<14.682609,0.000000,4.542969>}
box{<0,0,-0.038100><0.122869,0.036000,0.038100> rotate<0,90.000000,0> translate<14.682609,0.000000,4.542969> }
cylinder{<0,0,0><0,0.036000,0>0.038100 translate<14.682609,0.000000,4.542969>}
cylinder{<0,0,0><0,0.036000,0>0.038100 translate<14.805478,0.000000,4.542969>}
box{<0,0,-0.038100><0.122869,0.036000,0.038100> rotate<0,0.000000,0> translate<14.682609,0.000000,4.542969> }
cylinder{<0,0,0><0,0.036000,0>0.038100 translate<14.805478,0.000000,4.542969>}
cylinder{<0,0,0><0,0.036000,0>0.038100 translate<14.805478,0.000000,4.420100>}
box{<0,0,-0.038100><0.122869,0.036000,0.038100> rotate<0,-90.000000,0> translate<14.805478,0.000000,4.420100> }
cylinder{<0,0,0><0,0.036000,0>0.038100 translate<14.805478,0.000000,4.420100>}
cylinder{<0,0,0><0,0.036000,0>0.038100 translate<14.682609,0.000000,4.420100>}
box{<0,0,-0.038100><0.122869,0.036000,0.038100> rotate<0,0.000000,0> translate<14.682609,0.000000,4.420100> }
cylinder{<0,0,0><0,0.036000,0>0.038100 translate<15.056816,0.000000,4.420100>}
cylinder{<0,0,0><0,0.036000,0>0.038100 translate<15.056816,0.000000,5.157328>}
box{<0,0,-0.038100><0.737228,0.036000,0.038100> rotate<0,90.000000,0> translate<15.056816,0.000000,5.157328> }
cylinder{<0,0,0><0,0.036000,0>0.038100 translate<15.056816,0.000000,5.157328>}
cylinder{<0,0,0><0,0.036000,0>0.038100 translate<15.425428,0.000000,5.157328>}
box{<0,0,-0.038100><0.368613,0.036000,0.038100> rotate<0,0.000000,0> translate<15.056816,0.000000,5.157328> }
cylinder{<0,0,0><0,0.036000,0>0.038100 translate<15.425428,0.000000,5.157328>}
cylinder{<0,0,0><0,0.036000,0>0.038100 translate<15.548300,0.000000,5.034456>}
box{<0,0,-0.038100><0.173767,0.036000,0.038100> rotate<0,44.997030,0> translate<15.425428,0.000000,5.157328> }
cylinder{<0,0,0><0,0.036000,0>0.038100 translate<15.548300,0.000000,5.034456>}
cylinder{<0,0,0><0,0.036000,0>0.038100 translate<15.548300,0.000000,4.911584>}
box{<0,0,-0.038100><0.122872,0.036000,0.038100> rotate<0,-90.000000,0> translate<15.548300,0.000000,4.911584> }
cylinder{<0,0,0><0,0.036000,0>0.038100 translate<15.548300,0.000000,4.911584>}
cylinder{<0,0,0><0,0.036000,0>0.038100 translate<15.425428,0.000000,4.788712>}
box{<0,0,-0.038100><0.173767,0.036000,0.038100> rotate<0,-44.997030,0> translate<15.425428,0.000000,4.788712> }
cylinder{<0,0,0><0,0.036000,0>0.038100 translate<15.425428,0.000000,4.788712>}
cylinder{<0,0,0><0,0.036000,0>0.038100 translate<15.548300,0.000000,4.665841>}
box{<0,0,-0.038100><0.173767,0.036000,0.038100> rotate<0,44.997030,0> translate<15.425428,0.000000,4.788712> }
cylinder{<0,0,0><0,0.036000,0>0.038100 translate<15.548300,0.000000,4.665841>}
cylinder{<0,0,0><0,0.036000,0>0.038100 translate<15.548300,0.000000,4.542969>}
box{<0,0,-0.038100><0.122872,0.036000,0.038100> rotate<0,-90.000000,0> translate<15.548300,0.000000,4.542969> }
cylinder{<0,0,0><0,0.036000,0>0.038100 translate<15.548300,0.000000,4.542969>}
cylinder{<0,0,0><0,0.036000,0>0.038100 translate<15.425428,0.000000,4.420100>}
box{<0,0,-0.038100><0.173765,0.036000,0.038100> rotate<0,-44.996302,0> translate<15.425428,0.000000,4.420100> }
cylinder{<0,0,0><0,0.036000,0>0.038100 translate<15.425428,0.000000,4.420100>}
cylinder{<0,0,0><0,0.036000,0>0.038100 translate<15.056816,0.000000,4.420100>}
box{<0,0,-0.038100><0.368613,0.036000,0.038100> rotate<0,0.000000,0> translate<15.056816,0.000000,4.420100> }
cylinder{<0,0,0><0,0.036000,0>0.038100 translate<15.056816,0.000000,4.788712>}
cylinder{<0,0,0><0,0.036000,0>0.038100 translate<15.425428,0.000000,4.788712>}
box{<0,0,-0.038100><0.368613,0.036000,0.038100> rotate<0,0.000000,0> translate<15.056816,0.000000,4.788712> }
cylinder{<0,0,0><0,0.036000,0>0.038100 translate<16.296716,0.000000,4.420100>}
cylinder{<0,0,0><0,0.036000,0>0.038100 translate<15.805231,0.000000,4.420100>}
box{<0,0,-0.038100><0.491484,0.036000,0.038100> rotate<0,0.000000,0> translate<15.805231,0.000000,4.420100> }
cylinder{<0,0,0><0,0.036000,0>0.038100 translate<15.805231,0.000000,4.420100>}
cylinder{<0,0,0><0,0.036000,0>0.038100 translate<16.296716,0.000000,4.911584>}
box{<0,0,-0.038100><0.695064,0.036000,0.038100> rotate<0,-44.997030,0> translate<15.805231,0.000000,4.420100> }
cylinder{<0,0,0><0,0.036000,0>0.038100 translate<16.296716,0.000000,4.911584>}
cylinder{<0,0,0><0,0.036000,0>0.038100 translate<16.296716,0.000000,5.034456>}
box{<0,0,-0.038100><0.122872,0.036000,0.038100> rotate<0,90.000000,0> translate<16.296716,0.000000,5.034456> }
cylinder{<0,0,0><0,0.036000,0>0.038100 translate<16.296716,0.000000,5.034456>}
cylinder{<0,0,0><0,0.036000,0>0.038100 translate<16.173844,0.000000,5.157328>}
box{<0,0,-0.038100><0.173767,0.036000,0.038100> rotate<0,44.997030,0> translate<16.173844,0.000000,5.157328> }
cylinder{<0,0,0><0,0.036000,0>0.038100 translate<16.173844,0.000000,5.157328>}
cylinder{<0,0,0><0,0.036000,0>0.038100 translate<15.928100,0.000000,5.157328>}
box{<0,0,-0.038100><0.245744,0.036000,0.038100> rotate<0,0.000000,0> translate<15.928100,0.000000,5.157328> }
cylinder{<0,0,0><0,0.036000,0>0.038100 translate<15.928100,0.000000,5.157328>}
cylinder{<0,0,0><0,0.036000,0>0.038100 translate<15.805231,0.000000,5.034456>}
box{<0,0,-0.038100><0.173765,0.036000,0.038100> rotate<0,-44.997759,0> translate<15.805231,0.000000,5.034456> }
cylinder{<0,0,0><0,0.036000,0>0.076200 translate<9.321800,0.000000,28.016959>}
cylinder{<0,0,0><0,0.036000,0>0.076200 translate<8.135453,0.000000,28.016959>}
box{<0,0,-0.076200><1.186347,0.036000,0.076200> rotate<0,0.000000,0> translate<8.135453,0.000000,28.016959> }
cylinder{<0,0,0><0,0.036000,0>0.076200 translate<8.728628,0.000000,28.610131>}
cylinder{<0,0,0><0,0.036000,0>0.076200 translate<8.728628,0.000000,27.423784>}
box{<0,0,-0.076200><1.186347,0.036000,0.076200> rotate<0,-90.000000,0> translate<8.728628,0.000000,27.423784> }
cylinder{<0,0,0><0,0.036000,0>0.076200 translate<8.941559,0.000000,22.428716>}
cylinder{<0,0,0><0,0.036000,0>0.076200 translate<8.941559,0.000000,23.615063>}
box{<0,0,-0.076200><1.186347,0.036000,0.076200> rotate<0,90.000000,0> translate<8.941559,0.000000,23.615063> }
cylinder{<0,0,0><0,0.036000,0>0.076200 translate<55.930284,0.000000,8.687559>}
cylinder{<0,0,0><0,0.036000,0>0.076200 translate<54.743938,0.000000,8.687559>}
box{<0,0,-0.076200><1.186347,0.036000,0.076200> rotate<0,0.000000,0> translate<54.743938,0.000000,8.687559> }
cylinder{<0,0,0><0,0.036000,0>0.076200 translate<55.803800,0.000000,3.124959>}
cylinder{<0,0,0><0,0.036000,0>0.076200 translate<54.617453,0.000000,3.124959>}
box{<0,0,-0.076200><1.186347,0.036000,0.076200> rotate<0,0.000000,0> translate<54.617453,0.000000,3.124959> }
cylinder{<0,0,0><0,0.036000,0>0.076200 translate<55.210628,0.000000,3.718131>}
cylinder{<0,0,0><0,0.036000,0>0.076200 translate<55.210628,0.000000,2.531784>}
box{<0,0,-0.076200><1.186347,0.036000,0.076200> rotate<0,-90.000000,0> translate<55.210628,0.000000,2.531784> }
cylinder{<0,0,0><0,0.036000,0>0.050000 translate<43.085900,0.000000,0.672300>}
cylinder{<0,0,0><0,0.036000,0>0.050000 translate<47.085900,0.000000,0.672300>}
box{<0,0,-0.050000><4.000000,0.036000,0.050000> rotate<0,0.000000,0> translate<43.085900,0.000000,0.672300> }
cylinder{<0,0,0><0,0.036000,0>0.050000 translate<47.085900,0.000000,0.672300>}
cylinder{<0,0,0><0,0.036000,0>0.050000 translate<45.150600,0.000000,4.092900>}
box{<0,0,-0.050000><3.930126,0.036000,0.050000> rotate<0,60.495783,0> translate<45.150600,0.000000,4.092900> }
cylinder{<0,0,0><0,0.036000,0>0.050000 translate<45.150600,0.000000,4.092900>}
cylinder{<0,0,0><0,0.036000,0>0.050000 translate<43.085900,0.000000,0.672300>}
box{<0,0,-0.050000><3.995434,0.036000,0.050000> rotate<0,-58.880616,0> translate<43.085900,0.000000,0.672300> }
cylinder{<0,0,0><0,0.036000,0>0.050000 translate<43.708400,0.000000,0.992800>}
cylinder{<0,0,0><0,0.036000,0>0.050000 translate<46.469600,0.000000,0.992800>}
box{<0,0,-0.050000><2.761200,0.036000,0.050000> rotate<0,0.000000,0> translate<43.708400,0.000000,0.992800> }
cylinder{<0,0,0><0,0.036000,0>0.050000 translate<46.469600,0.000000,0.992800>}
cylinder{<0,0,0><0,0.036000,0>0.050000 translate<45.126000,0.000000,3.353300>}
box{<0,0,-0.050000><2.716104,0.036000,0.050000> rotate<0,60.347423,0> translate<45.126000,0.000000,3.353300> }
cylinder{<0,0,0><0,0.036000,0>0.050000 translate<45.126000,0.000000,3.353300>}
cylinder{<0,0,0><0,0.036000,0>0.050000 translate<43.708400,0.000000,0.992800>}
box{<0,0,-0.050000><2.753461,0.036000,0.050000> rotate<0,-59.009153,0> translate<43.708400,0.000000,0.992800> }
cylinder{<0,0,0><0,0.036000,0>0.050000 translate<45.002700,0.000000,3.014300>}
cylinder{<0,0,0><0,0.036000,0>0.050000 translate<45.286200,0.000000,2.884900>}
box{<0,0,-0.050000><0.311635,0.036000,0.050000> rotate<0,24.532110,0> translate<45.002700,0.000000,3.014300> }
cylinder{<0,0,0><0,0.036000,0>0.050000 translate<45.286200,0.000000,2.884900>}
cylinder{<0,0,0><0,0.036000,0>0.050000 translate<44.891700,0.000000,2.022000>}
box{<0,0,-0.050000><0.948803,0.036000,0.050000> rotate<0,-65.426784,0> translate<44.891700,0.000000,2.022000> }
cylinder{<0,0,0><0,0.036000,0>0.050000 translate<44.891700,0.000000,2.022000>}
cylinder{<0,0,0><0,0.036000,0>0.050000 translate<45.434100,0.000000,2.305600>}
box{<0,0,-0.050000><0.612068,0.036000,0.050000> rotate<0,-27.601508,0> translate<44.891700,0.000000,2.022000> }
cylinder{<0,0,0><0,0.036000,0>0.050000 translate<45.434100,0.000000,2.305600>}
cylinder{<0,0,0><0,0.036000,0>0.050000 translate<45.119800,0.000000,1.313300>}
box{<0,0,-0.050000><1.040886,0.036000,0.050000> rotate<0,-72.420201,0> translate<45.119800,0.000000,1.313300> }
cylinder{<0,0,0><0,0.036000,0>0.050000 translate<45.119800,0.000000,1.313300>}
cylinder{<0,0,0><0,0.036000,0>0.050000 translate<45.236900,0.000000,1.307100>}
box{<0,0,-0.050000><0.117264,0.036000,0.050000> rotate<0,3.030564,0> translate<45.119800,0.000000,1.313300> }
cylinder{<0,0,0><0,0.036000,0>0.050000 translate<45.236900,0.000000,1.307100>}
cylinder{<0,0,0><0,0.036000,0>0.050000 translate<45.002700,0.000000,1.079000>}
box{<0,0,-0.050000><0.326924,0.036000,0.050000> rotate<0,-44.241112,0> translate<45.002700,0.000000,1.079000> }
cylinder{<0,0,0><0,0.036000,0>0.050000 translate<45.002700,0.000000,1.079000>}
cylinder{<0,0,0><0,0.036000,0>0.050000 translate<44.891700,0.000000,1.399600>}
box{<0,0,-0.050000><0.339272,0.036000,0.050000> rotate<0,70.898146,0> translate<44.891700,0.000000,1.399600> }
cylinder{<0,0,0><0,0.036000,0>0.050000 translate<44.891700,0.000000,1.399600>}
cylinder{<0,0,0><0,0.036000,0>0.050000 translate<45.015000,0.000000,1.350200>}
box{<0,0,-0.050000><0.132828,0.036000,0.050000> rotate<0,21.832009,0> translate<44.891700,0.000000,1.399600> }
cylinder{<0,0,0><0,0.036000,0>0.050000 translate<45.015000,0.000000,1.350200>}
cylinder{<0,0,0><0,0.036000,0>0.050000 translate<45.132100,0.000000,1.991200>}
box{<0,0,-0.050000><0.651608,0.036000,0.050000> rotate<0,-79.641923,0> translate<45.015000,0.000000,1.350200> }
cylinder{<0,0,0><0,0.036000,0>0.050000 translate<45.132100,0.000000,1.991200>}
cylinder{<0,0,0><0,0.036000,0>0.050000 translate<44.595900,0.000000,1.726200>}
box{<0,0,-0.050000><0.598110,0.036000,0.050000> rotate<0,-26.297702,0> translate<44.595900,0.000000,1.726200> }
cylinder{<0,0,0><0,0.036000,0>0.050000 translate<44.595900,0.000000,1.726200>}
cylinder{<0,0,0><0,0.036000,0>0.050000 translate<45.002700,0.000000,3.014300>}
box{<0,0,-0.050000><1.350810,0.036000,0.050000> rotate<0,-72.468371,0> translate<44.595900,0.000000,1.726200> }
//C1 silk screen
cylinder{<0,0,0><0,0.036000,0>0.050800 translate<46.765000,0.000000,14.613000>}
cylinder{<0,0,0><0,0.036000,0>0.050800 translate<44.835000,0.000000,14.613000>}
box{<0,0,-0.050800><1.930000,0.036000,0.050800> rotate<0,0.000000,0> translate<44.835000,0.000000,14.613000> }
cylinder{<0,0,0><0,0.036000,0>0.050800 translate<46.765000,0.000000,16.187000>}
cylinder{<0,0,0><0,0.036000,0>0.050800 translate<44.835000,0.000000,16.187000>}
box{<0,0,-0.050800><1.930000,0.036000,0.050800> rotate<0,0.000000,0> translate<44.835000,0.000000,16.187000> }
box{<-0.375050,0,-0.850000><0.375050,0.036000,0.850000> rotate<0,-180.000000,0> translate<47.126750,0.000000,15.400900>}
box{<-0.375050,0,-0.850000><0.375050,0.036000,0.850000> rotate<0,-180.000000,0> translate<44.473250,0.000000,15.399100>}
//C2 silk screen
cylinder{<0,0,0><0,0.036000,0>0.050800 translate<23.875000,0.000000,17.575000>}
cylinder{<0,0,0><0,0.036000,0>0.050800 translate<29.325000,0.000000,17.575000>}
box{<0,0,-0.050800><5.450000,0.036000,0.050800> rotate<0,0.000000,0> translate<23.875000,0.000000,17.575000> }
cylinder{<0,0,0><0,0.036000,0>0.050800 translate<29.325000,0.000000,11.425000>}
cylinder{<0,0,0><0,0.036000,0>0.050800 translate<23.875000,0.000000,11.425000>}
box{<0,0,-0.050800><5.450000,0.036000,0.050800> rotate<0,0.000000,0> translate<23.875000,0.000000,11.425000> }
box{<-0.300000,0,-3.150000><0.300000,0.036000,3.150000> rotate<0,-0.000000,0> translate<24.100000,0.000000,14.500000>}
box{<-0.300000,0,-3.150000><0.300000,0.036000,3.150000> rotate<0,-0.000000,0> translate<29.100000,0.000000,14.500000>}
//C3 silk screen
cylinder{<0,0,0><0,0.036000,0>0.050800 translate<-0.727400,0.000000,16.635000>}
cylinder{<0,0,0><0,0.036000,0>0.050800 translate<-0.015400,0.000000,16.635000>}
box{<0,0,-0.050800><0.712000,0.036000,0.050800> rotate<0,0.000000,0> translate<-0.727400,0.000000,16.635000> }
cylinder{<0,0,0><0,0.036000,0>0.050800 translate<-0.727400,0.000000,15.784000>}
cylinder{<0,0,0><0,0.036000,0>0.050800 translate<-0.015400,0.000000,15.784000>}
box{<0,0,-0.050800><0.712000,0.036000,0.050800> rotate<0,0.000000,0> translate<-0.727400,0.000000,15.784000> }
box{<-0.250050,0,-0.475000><0.250050,0.036000,0.475000> rotate<0,-0.000000,0> translate<-0.959550,0.000000,16.208100>}
box{<-0.250050,0,-0.475000><0.250050,0.036000,0.475000> rotate<0,-0.000000,0> translate<0.208850,0.000000,16.208100>}
//C4 silk screen
cylinder{<0,0,0><0,0.036000,0>0.050800 translate<1.551000,0.000000,11.176000>}
cylinder{<0,0,0><0,0.036000,0>0.050800 translate<1.551000,0.000000,13.106000>}
box{<0,0,-0.050800><1.930000,0.036000,0.050800> rotate<0,90.000000,0> translate<1.551000,0.000000,13.106000> }
cylinder{<0,0,0><0,0.036000,0>0.050800 translate<3.125000,0.000000,11.176000>}
cylinder{<0,0,0><0,0.036000,0>0.050800 translate<3.125000,0.000000,13.106000>}
box{<0,0,-0.050800><1.930000,0.036000,0.050800> rotate<0,90.000000,0> translate<3.125000,0.000000,13.106000> }
box{<-0.375050,0,-0.850000><0.375050,0.036000,0.850000> rotate<0,-90.000000,0> translate<2.338900,0.000000,10.814250>}
box{<-0.375050,0,-0.850000><0.375050,0.036000,0.850000> rotate<0,-90.000000,0> translate<2.337100,0.000000,13.467750>}
//C5 silk screen
cylinder{<0,0,0><0,0.036000,0>0.050800 translate<8.008400,0.000000,1.548000>}
cylinder{<0,0,0><0,0.036000,0>0.050800 translate<7.296400,0.000000,1.548000>}
box{<0,0,-0.050800><0.712000,0.036000,0.050800> rotate<0,0.000000,0> translate<7.296400,0.000000,1.548000> }
cylinder{<0,0,0><0,0.036000,0>0.050800 translate<8.008400,0.000000,2.399000>}
cylinder{<0,0,0><0,0.036000,0>0.050800 translate<7.296400,0.000000,2.399000>}
box{<0,0,-0.050800><0.712000,0.036000,0.050800> rotate<0,0.000000,0> translate<7.296400,0.000000,2.399000> }
box{<-0.250050,0,-0.475000><0.250050,0.036000,0.475000> rotate<0,-180.000000,0> translate<8.240550,0.000000,1.974900>}
box{<-0.250050,0,-0.475000><0.250050,0.036000,0.475000> rotate<0,-180.000000,0> translate<7.072150,0.000000,1.974900>}
//D1 silk screen
cylinder{<0,0,0><0,0.036000,0>0.050800 translate<4.960600,0.000000,17.463000>}
cylinder{<0,0,0><0,0.036000,0>0.050800 translate<-2.160600,0.000000,17.463000>}
box{<0,0,-0.050800><7.121200,0.036000,0.050800> rotate<0,0.000000,0> translate<-2.160600,0.000000,17.463000> }
cylinder{<0,0,0><0,0.036000,0>0.050800 translate<4.960600,0.000000,23.673000>}
cylinder{<0,0,0><0,0.036000,0>0.050800 translate<-2.160600,0.000000,23.673000>}
box{<0,0,-0.050800><7.121200,0.036000,0.050800> rotate<0,0.000000,0> translate<-2.160600,0.000000,23.673000> }
cylinder{<0,0,0><0,0.036000,0>0.050800 translate<4.960600,0.000000,23.673000>}
cylinder{<0,0,0><0,0.036000,0>0.050800 translate<4.960600,0.000000,17.463000>}
box{<0,0,-0.050800><6.210000,0.036000,0.050800> rotate<0,-90.000000,0> translate<4.960600,0.000000,17.463000> }
cylinder{<0,0,0><0,0.036000,0>0.050800 translate<-2.160600,0.000000,23.673000>}
cylinder{<0,0,0><0,0.036000,0>0.050800 translate<-2.160600,0.000000,17.463000>}
box{<0,0,-0.050800><6.210000,0.036000,0.050800> rotate<0,-90.000000,0> translate<-2.160600,0.000000,17.463000> }
cylinder{<0,0,0><0,0.036000,0>0.101600 translate<0.857000,0.000000,19.568000>}
cylinder{<0,0,0><0,0.036000,0>0.101600 translate<2.230000,0.000000,20.568000>}
box{<0,0,-0.101600><1.698567,0.036000,0.101600> rotate<0,-36.064673,0> translate<0.857000,0.000000,19.568000> }
cylinder{<0,0,0><0,0.036000,0>0.101600 translate<2.230000,0.000000,20.568000>}
cylinder{<0,0,0><0,0.036000,0>0.101600 translate<0.857000,0.000000,21.568000>}
box{<0,0,-0.101600><1.698567,0.036000,0.101600> rotate<0,36.064673,0> translate<0.857000,0.000000,21.568000> }
cylinder{<0,0,0><0,0.036000,0>0.101600 translate<0.857000,0.000000,21.568000>}
cylinder{<0,0,0><0,0.036000,0>0.101600 translate<0.857000,0.000000,19.568000>}
box{<0,0,-0.101600><2.000000,0.036000,0.101600> rotate<0,-90.000000,0> translate<0.857000,0.000000,19.568000> }
box{<-0.266700,0,-1.092200><0.266700,0.036000,1.092200> rotate<0,-180.000000,0> translate<5.227300,0.000000,20.568000>}
box{<-0.266700,0,-1.092200><0.266700,0.036000,1.092200> rotate<0,-180.000000,0> translate<-2.427300,0.000000,20.568000>}
box{<-0.625000,0,-3.100000><0.625000,0.036000,3.100000> rotate<0,-180.000000,0> translate<2.875000,0.000000,20.568000>}
//D2 silk screen
cylinder{<0,0,0><0,0.036000,0>0.050800 translate<0.060600,-1.536000,24.708200>}
cylinder{<0,0,0><0,0.036000,0>0.050800 translate<-4.460600,-1.536000,24.708200>}
box{<0,0,-0.050800><4.521200,0.036000,0.050800> rotate<0,0.000000,0> translate<-4.460600,-1.536000,24.708200> }
cylinder{<0,0,0><0,0.036000,0>0.050800 translate<0.060600,-1.536000,20.898200>}
cylinder{<0,0,0><0,0.036000,0>0.050800 translate<-4.460600,-1.536000,20.898200>}
box{<0,0,-0.050800><4.521200,0.036000,0.050800> rotate<0,0.000000,0> translate<-4.460600,-1.536000,20.898200> }
cylinder{<0,0,0><0,0.036000,0>0.050800 translate<0.060600,-1.536000,20.898200>}
cylinder{<0,0,0><0,0.036000,0>0.050800 translate<0.060600,-1.536000,24.708200>}
box{<0,0,-0.050800><3.810000,0.036000,0.050800> rotate<0,90.000000,0> translate<0.060600,-1.536000,24.708200> }
cylinder{<0,0,0><0,0.036000,0>0.050800 translate<-4.460600,-1.536000,20.898200>}
cylinder{<0,0,0><0,0.036000,0>0.050800 translate<-4.460600,-1.536000,24.708200>}
box{<0,0,-0.050800><3.810000,0.036000,0.050800> rotate<0,90.000000,0> translate<-4.460600,-1.536000,24.708200> }
cylinder{<0,0,0><0,0.036000,0>0.101600 translate<-2.393000,-1.536000,23.803200>}
cylinder{<0,0,0><0,0.036000,0>0.101600 translate<-1.370000,-1.536000,22.803200>}
box{<0,0,-0.101600><1.430569,0.036000,0.101600> rotate<0,44.345691,0> translate<-2.393000,-1.536000,23.803200> }
cylinder{<0,0,0><0,0.036000,0>0.101600 translate<-1.370000,-1.536000,22.803200>}
cylinder{<0,0,0><0,0.036000,0>0.101600 translate<-2.393000,-1.536000,21.803200>}
box{<0,0,-0.101600><1.430569,0.036000,0.101600> rotate<0,-44.345691,0> translate<-2.393000,-1.536000,21.803200> }
cylinder{<0,0,0><0,0.036000,0>0.101600 translate<-2.393000,-1.536000,21.803200>}
cylinder{<0,0,0><0,0.036000,0>0.101600 translate<-2.393000,-1.536000,23.803200>}
box{<0,0,-0.101600><2.000000,0.036000,0.101600> rotate<0,90.000000,0> translate<-2.393000,-1.536000,23.803200> }
box{<-0.266700,0,-1.092200><0.266700,0.036000,1.092200> rotate<0,-180.000000,0> translate<0.327300,-1.536000,22.803200>}
box{<-0.266700,0,-1.092200><0.266700,0.036000,1.092200> rotate<0,-180.000000,0> translate<-4.727300,-1.536000,22.803200>}
box{<-0.275000,0,-1.900000><0.275000,0.036000,1.900000> rotate<0,-180.000000,0> translate<-1.125000,-1.536000,22.803200>}
//D3 silk screen
cylinder{<0,0,0><0,0.036000,0>0.050800 translate<36.095000,-1.536000,16.090600>}
cylinder{<0,0,0><0,0.036000,0>0.050800 translate<36.095000,-1.536000,8.969400>}
box{<0,0,-0.050800><7.121200,0.036000,0.050800> rotate<0,-90.000000,0> translate<36.095000,-1.536000,8.969400> }
cylinder{<0,0,0><0,0.036000,0>0.050800 translate<42.305000,-1.536000,16.090600>}
cylinder{<0,0,0><0,0.036000,0>0.050800 translate<42.305000,-1.536000,8.969400>}
box{<0,0,-0.050800><7.121200,0.036000,0.050800> rotate<0,-90.000000,0> translate<42.305000,-1.536000,8.969400> }
cylinder{<0,0,0><0,0.036000,0>0.050800 translate<42.305000,-1.536000,16.090600>}
cylinder{<0,0,0><0,0.036000,0>0.050800 translate<36.095000,-1.536000,16.090600>}
box{<0,0,-0.050800><6.210000,0.036000,0.050800> rotate<0,0.000000,0> translate<36.095000,-1.536000,16.090600> }
cylinder{<0,0,0><0,0.036000,0>0.050800 translate<42.305000,-1.536000,8.969400>}
cylinder{<0,0,0><0,0.036000,0>0.050800 translate<36.095000,-1.536000,8.969400>}
box{<0,0,-0.050800><6.210000,0.036000,0.050800> rotate<0,0.000000,0> translate<36.095000,-1.536000,8.969400> }
cylinder{<0,0,0><0,0.036000,0>0.101600 translate<38.200000,-1.536000,11.987000>}
cylinder{<0,0,0><0,0.036000,0>0.101600 translate<39.200000,-1.536000,13.360000>}
box{<0,0,-0.101600><1.698567,0.036000,0.101600> rotate<0,-53.929387,0> translate<38.200000,-1.536000,11.987000> }
cylinder{<0,0,0><0,0.036000,0>0.101600 translate<39.200000,-1.536000,13.360000>}
cylinder{<0,0,0><0,0.036000,0>0.101600 translate<40.200000,-1.536000,11.987000>}
box{<0,0,-0.101600><1.698567,0.036000,0.101600> rotate<0,53.929387,0> translate<39.200000,-1.536000,13.360000> }
cylinder{<0,0,0><0,0.036000,0>0.101600 translate<40.200000,-1.536000,11.987000>}
cylinder{<0,0,0><0,0.036000,0>0.101600 translate<38.200000,-1.536000,11.987000>}
box{<0,0,-0.101600><2.000000,0.036000,0.101600> rotate<0,0.000000,0> translate<38.200000,-1.536000,11.987000> }
box{<-0.266700,0,-1.092200><0.266700,0.036000,1.092200> rotate<0,-270.000000,0> translate<39.200000,-1.536000,16.357300>}
box{<-0.266700,0,-1.092200><0.266700,0.036000,1.092200> rotate<0,-270.000000,0> translate<39.200000,-1.536000,8.702700>}
box{<-0.625000,0,-3.100000><0.625000,0.036000,3.100000> rotate<0,-270.000000,0> translate<39.200000,-1.536000,14.005000>}
//D4 silk screen
cylinder{<0,0,0><0,0.036000,0>0.050800 translate<49.305000,-1.536000,14.039400>}
cylinder{<0,0,0><0,0.036000,0>0.050800 translate<49.305000,-1.536000,21.160600>}
box{<0,0,-0.050800><7.121200,0.036000,0.050800> rotate<0,90.000000,0> translate<49.305000,-1.536000,21.160600> }
cylinder{<0,0,0><0,0.036000,0>0.050800 translate<43.095000,-1.536000,14.039400>}
cylinder{<0,0,0><0,0.036000,0>0.050800 translate<43.095000,-1.536000,21.160600>}
box{<0,0,-0.050800><7.121200,0.036000,0.050800> rotate<0,90.000000,0> translate<43.095000,-1.536000,21.160600> }
cylinder{<0,0,0><0,0.036000,0>0.050800 translate<43.095000,-1.536000,14.039400>}
cylinder{<0,0,0><0,0.036000,0>0.050800 translate<49.305000,-1.536000,14.039400>}
box{<0,0,-0.050800><6.210000,0.036000,0.050800> rotate<0,0.000000,0> translate<43.095000,-1.536000,14.039400> }
cylinder{<0,0,0><0,0.036000,0>0.050800 translate<43.095000,-1.536000,21.160600>}
cylinder{<0,0,0><0,0.036000,0>0.050800 translate<49.305000,-1.536000,21.160600>}
box{<0,0,-0.050800><6.210000,0.036000,0.050800> rotate<0,0.000000,0> translate<43.095000,-1.536000,21.160600> }
cylinder{<0,0,0><0,0.036000,0>0.101600 translate<47.200000,-1.536000,18.143000>}
cylinder{<0,0,0><0,0.036000,0>0.101600 translate<46.200000,-1.536000,16.770000>}
box{<0,0,-0.101600><1.698567,0.036000,0.101600> rotate<0,-53.929387,0> translate<46.200000,-1.536000,16.770000> }
cylinder{<0,0,0><0,0.036000,0>0.101600 translate<46.200000,-1.536000,16.770000>}
cylinder{<0,0,0><0,0.036000,0>0.101600 translate<45.200000,-1.536000,18.143000>}
box{<0,0,-0.101600><1.698567,0.036000,0.101600> rotate<0,53.929387,0> translate<45.200000,-1.536000,18.143000> }
cylinder{<0,0,0><0,0.036000,0>0.101600 translate<45.200000,-1.536000,18.143000>}
cylinder{<0,0,0><0,0.036000,0>0.101600 translate<47.200000,-1.536000,18.143000>}
box{<0,0,-0.101600><2.000000,0.036000,0.101600> rotate<0,0.000000,0> translate<45.200000,-1.536000,18.143000> }
box{<-0.266700,0,-1.092200><0.266700,0.036000,1.092200> rotate<0,-90.000000,0> translate<46.200000,-1.536000,13.772700>}
box{<-0.266700,0,-1.092200><0.266700,0.036000,1.092200> rotate<0,-90.000000,0> translate<46.200000,-1.536000,21.427300>}
box{<-0.625000,0,-3.100000><0.625000,0.036000,3.100000> rotate<0,-90.000000,0> translate<46.200000,-1.536000,16.125000>}
//D5 silk screen
cylinder{<0,0,0><0,0.036000,0>0.050800 translate<-14.685800,-1.536000,0.755800>}
cylinder{<0,0,0><0,0.036000,0>0.050800 translate<-7.564600,-1.536000,0.755800>}
box{<0,0,-0.050800><7.121200,0.036000,0.050800> rotate<0,0.000000,0> translate<-14.685800,-1.536000,0.755800> }
cylinder{<0,0,0><0,0.036000,0>0.050800 translate<-14.685800,-1.536000,6.965800>}
cylinder{<0,0,0><0,0.036000,0>0.050800 translate<-7.564600,-1.536000,6.965800>}
box{<0,0,-0.050800><7.121200,0.036000,0.050800> rotate<0,0.000000,0> translate<-14.685800,-1.536000,6.965800> }
cylinder{<0,0,0><0,0.036000,0>0.050800 translate<-14.685800,-1.536000,6.965800>}
cylinder{<0,0,0><0,0.036000,0>0.050800 translate<-14.685800,-1.536000,0.755800>}
box{<0,0,-0.050800><6.210000,0.036000,0.050800> rotate<0,-90.000000,0> translate<-14.685800,-1.536000,0.755800> }
cylinder{<0,0,0><0,0.036000,0>0.050800 translate<-7.564600,-1.536000,6.965800>}
cylinder{<0,0,0><0,0.036000,0>0.050800 translate<-7.564600,-1.536000,0.755800>}
box{<0,0,-0.050800><6.210000,0.036000,0.050800> rotate<0,-90.000000,0> translate<-7.564600,-1.536000,0.755800> }
cylinder{<0,0,0><0,0.036000,0>0.101600 translate<-10.582200,-1.536000,2.860800>}
cylinder{<0,0,0><0,0.036000,0>0.101600 translate<-11.955200,-1.536000,3.860800>}
box{<0,0,-0.101600><1.698567,0.036000,0.101600> rotate<0,36.064673,0> translate<-11.955200,-1.536000,3.860800> }
cylinder{<0,0,0><0,0.036000,0>0.101600 translate<-11.955200,-1.536000,3.860800>}
cylinder{<0,0,0><0,0.036000,0>0.101600 translate<-10.582200,-1.536000,4.860800>}
box{<0,0,-0.101600><1.698567,0.036000,0.101600> rotate<0,-36.064673,0> translate<-11.955200,-1.536000,3.860800> }
cylinder{<0,0,0><0,0.036000,0>0.101600 translate<-10.582200,-1.536000,4.860800>}
cylinder{<0,0,0><0,0.036000,0>0.101600 translate<-10.582200,-1.536000,2.860800>}
box{<0,0,-0.101600><2.000000,0.036000,0.101600> rotate<0,-90.000000,0> translate<-10.582200,-1.536000,2.860800> }
box{<-0.266700,0,-1.092200><0.266700,0.036000,1.092200> rotate<0,-0.000000,0> translate<-14.952500,-1.536000,3.860800>}
box{<-0.266700,0,-1.092200><0.266700,0.036000,1.092200> rotate<0,-0.000000,0> translate<-7.297900,-1.536000,3.860800>}
box{<-0.625000,0,-3.100000><0.625000,0.036000,3.100000> rotate<0,-0.000000,0> translate<-12.600200,-1.536000,3.860800>}
//F1 silk screen
cylinder{<0,0,0><0,0.036000,0>0.076200 translate<41.030000,0.000000,2.495000>}
cylinder{<0,0,0><0,0.036000,0>0.076200 translate<42.300000,0.000000,2.495000>}
box{<0,0,-0.076200><1.270000,0.036000,0.076200> rotate<0,0.000000,0> translate<41.030000,0.000000,2.495000> }
cylinder{<0,0,0><0,0.036000,0>0.076200 translate<42.300000,0.000000,6.305000>}
cylinder{<0,0,0><0,0.036000,0>0.076200 translate<42.300000,0.000000,2.495000>}
box{<0,0,-0.076200><3.810000,0.036000,0.076200> rotate<0,-90.000000,0> translate<42.300000,0.000000,2.495000> }
cylinder{<0,0,0><0,0.036000,0>0.076200 translate<42.300000,0.000000,6.305000>}
cylinder{<0,0,0><0,0.036000,0>0.076200 translate<41.030000,0.000000,6.305000>}
box{<0,0,-0.076200><1.270000,0.036000,0.076200> rotate<0,0.000000,0> translate<41.030000,0.000000,6.305000> }
cylinder{<0,0,0><0,0.036000,0>0.076200 translate<40.395000,0.000000,1.860000>}
cylinder{<0,0,0><0,0.036000,0>0.076200 translate<40.395000,0.000000,0.590000>}
box{<0,0,-0.076200><1.270000,0.036000,0.076200> rotate<0,-90.000000,0> translate<40.395000,0.000000,0.590000> }
cylinder{<0,0,0><0,0.036000,0>0.076200 translate<40.395000,0.000000,0.590000>}
cylinder{<0,0,0><0,0.036000,0>0.076200 translate<36.585000,0.000000,0.590000>}
box{<0,0,-0.076200><3.810000,0.036000,0.076200> rotate<0,0.000000,0> translate<36.585000,0.000000,0.590000> }
cylinder{<0,0,0><0,0.036000,0>0.076200 translate<36.585000,0.000000,1.860000>}
cylinder{<0,0,0><0,0.036000,0>0.076200 translate<36.585000,0.000000,0.590000>}
box{<0,0,-0.076200><1.270000,0.036000,0.076200> rotate<0,-90.000000,0> translate<36.585000,0.000000,0.590000> }
cylinder{<0,0,0><0,0.036000,0>0.076200 translate<40.395000,0.000000,6.940000>}
cylinder{<0,0,0><0,0.036000,0>0.076200 translate<40.395000,0.000000,8.210000>}
box{<0,0,-0.076200><1.270000,0.036000,0.076200> rotate<0,90.000000,0> translate<40.395000,0.000000,8.210000> }
cylinder{<0,0,0><0,0.036000,0>0.076200 translate<36.585000,0.000000,8.210000>}
cylinder{<0,0,0><0,0.036000,0>0.076200 translate<40.395000,0.000000,8.210000>}
box{<0,0,-0.076200><3.810000,0.036000,0.076200> rotate<0,0.000000,0> translate<36.585000,0.000000,8.210000> }
cylinder{<0,0,0><0,0.036000,0>0.076200 translate<36.585000,0.000000,8.210000>}
cylinder{<0,0,0><0,0.036000,0>0.076200 translate<36.585000,0.000000,6.940000>}
box{<0,0,-0.076200><1.270000,0.036000,0.076200> rotate<0,-90.000000,0> translate<36.585000,0.000000,6.940000> }
cylinder{<0,0,0><0,0.036000,0>0.076200 translate<35.950000,0.000000,2.495000>}
cylinder{<0,0,0><0,0.036000,0>0.076200 translate<34.680000,0.000000,2.495000>}
box{<0,0,-0.076200><1.270000,0.036000,0.076200> rotate<0,0.000000,0> translate<34.680000,0.000000,2.495000> }
cylinder{<0,0,0><0,0.036000,0>0.076200 translate<34.680000,0.000000,2.495000>}
cylinder{<0,0,0><0,0.036000,0>0.076200 translate<34.680000,0.000000,2.876000>}
box{<0,0,-0.076200><0.381000,0.036000,0.076200> rotate<0,90.000000,0> translate<34.680000,0.000000,2.876000> }
cylinder{<0,0,0><0,0.036000,0>0.076200 translate<34.680000,0.000000,6.305000>}
cylinder{<0,0,0><0,0.036000,0>0.076200 translate<35.950000,0.000000,6.305000>}
box{<0,0,-0.076200><1.270000,0.036000,0.076200> rotate<0,0.000000,0> translate<34.680000,0.000000,6.305000> }
cylinder{<0,0,0><0,0.036000,0>0.076200 translate<23.250000,0.000000,2.495000>}
cylinder{<0,0,0><0,0.036000,0>0.076200 translate<24.520000,0.000000,2.495000>}
box{<0,0,-0.076200><1.270000,0.036000,0.076200> rotate<0,0.000000,0> translate<23.250000,0.000000,2.495000> }
cylinder{<0,0,0><0,0.036000,0>0.076200 translate<24.520000,0.000000,6.305000>}
cylinder{<0,0,0><0,0.036000,0>0.076200 translate<24.520000,0.000000,5.924000>}
box{<0,0,-0.076200><0.381000,0.036000,0.076200> rotate<0,-90.000000,0> translate<24.520000,0.000000,5.924000> }
cylinder{<0,0,0><0,0.036000,0>0.076200 translate<24.520000,0.000000,6.305000>}
cylinder{<0,0,0><0,0.036000,0>0.076200 translate<23.250000,0.000000,6.305000>}
box{<0,0,-0.076200><1.270000,0.036000,0.076200> rotate<0,0.000000,0> translate<23.250000,0.000000,6.305000> }
cylinder{<0,0,0><0,0.036000,0>0.076200 translate<22.615000,0.000000,1.860000>}
cylinder{<0,0,0><0,0.036000,0>0.076200 translate<22.615000,0.000000,0.590000>}
box{<0,0,-0.076200><1.270000,0.036000,0.076200> rotate<0,-90.000000,0> translate<22.615000,0.000000,0.590000> }
cylinder{<0,0,0><0,0.036000,0>0.076200 translate<22.615000,0.000000,0.590000>}
cylinder{<0,0,0><0,0.036000,0>0.076200 translate<18.805000,0.000000,0.590000>}
box{<0,0,-0.076200><3.810000,0.036000,0.076200> rotate<0,0.000000,0> translate<18.805000,0.000000,0.590000> }
cylinder{<0,0,0><0,0.036000,0>0.076200 translate<18.805000,0.000000,1.860000>}
cylinder{<0,0,0><0,0.036000,0>0.076200 translate<18.805000,0.000000,0.590000>}
box{<0,0,-0.076200><1.270000,0.036000,0.076200> rotate<0,-90.000000,0> translate<18.805000,0.000000,0.590000> }
cylinder{<0,0,0><0,0.036000,0>0.076200 translate<22.615000,0.000000,6.940000>}
cylinder{<0,0,0><0,0.036000,0>0.076200 translate<22.615000,0.000000,8.210000>}
box{<0,0,-0.076200><1.270000,0.036000,0.076200> rotate<0,90.000000,0> translate<22.615000,0.000000,8.210000> }
cylinder{<0,0,0><0,0.036000,0>0.076200 translate<18.805000,0.000000,8.210000>}
cylinder{<0,0,0><0,0.036000,0>0.076200 translate<22.615000,0.000000,8.210000>}
box{<0,0,-0.076200><3.810000,0.036000,0.076200> rotate<0,0.000000,0> translate<18.805000,0.000000,8.210000> }
cylinder{<0,0,0><0,0.036000,0>0.076200 translate<18.805000,0.000000,8.210000>}
cylinder{<0,0,0><0,0.036000,0>0.076200 translate<18.805000,0.000000,6.940000>}
box{<0,0,-0.076200><1.270000,0.036000,0.076200> rotate<0,-90.000000,0> translate<18.805000,0.000000,6.940000> }
cylinder{<0,0,0><0,0.036000,0>0.076200 translate<18.170000,0.000000,2.495000>}
cylinder{<0,0,0><0,0.036000,0>0.076200 translate<16.900000,0.000000,2.495000>}
box{<0,0,-0.076200><1.270000,0.036000,0.076200> rotate<0,0.000000,0> translate<16.900000,0.000000,2.495000> }
cylinder{<0,0,0><0,0.036000,0>0.076200 translate<16.900000,0.000000,2.495000>}
cylinder{<0,0,0><0,0.036000,0>0.076200 translate<16.900000,0.000000,6.305000>}
box{<0,0,-0.076200><3.810000,0.036000,0.076200> rotate<0,90.000000,0> translate<16.900000,0.000000,6.305000> }
cylinder{<0,0,0><0,0.036000,0>0.076200 translate<16.900000,0.000000,6.305000>}
cylinder{<0,0,0><0,0.036000,0>0.076200 translate<18.170000,0.000000,6.305000>}
box{<0,0,-0.076200><1.270000,0.036000,0.076200> rotate<0,0.000000,0> translate<16.900000,0.000000,6.305000> }
cylinder{<0,0,0><0,0.036000,0>0.076200 translate<35.315000,0.000000,1.606000>}
cylinder{<0,0,0><0,0.036000,0>0.076200 translate<23.885000,0.000000,1.606000>}
box{<0,0,-0.076200><11.430000,0.036000,0.076200> rotate<0,0.000000,0> translate<23.885000,0.000000,1.606000> }
cylinder{<0,0,0><0,0.036000,0>0.076200 translate<35.315000,0.000000,7.194000>}
cylinder{<0,0,0><0,0.036000,0>0.076200 translate<23.885000,0.000000,7.194000>}
box{<0,0,-0.076200><11.430000,0.036000,0.076200> rotate<0,0.000000,0> translate<23.885000,0.000000,7.194000> }
cylinder{<0,0,0><0,0.036000,0>0.076200 translate<34.680000,0.000000,2.876000>}
cylinder{<0,0,0><0,0.036000,0>0.076200 translate<24.520000,0.000000,5.924000>}
box{<0,0,-0.076200><10.607351,0.036000,0.076200> rotate<0,16.698142,0> translate<24.520000,0.000000,5.924000> }
cylinder{<0,0,0><0,0.036000,0>0.076200 translate<34.680000,0.000000,2.876000>}
cylinder{<0,0,0><0,0.036000,0>0.076200 translate<34.680000,0.000000,6.305000>}
box{<0,0,-0.076200><3.429000,0.036000,0.076200> rotate<0,90.000000,0> translate<34.680000,0.000000,6.305000> }
cylinder{<0,0,0><0,0.036000,0>0.076200 translate<24.520000,0.000000,5.924000>}
cylinder{<0,0,0><0,0.036000,0>0.076200 translate<24.520000,0.000000,2.495000>}
box{<0,0,-0.076200><3.429000,0.036000,0.076200> rotate<0,-90.000000,0> translate<24.520000,0.000000,2.495000> }
difference{
cylinder{<29.600000,0,4.400000><29.600000,0.036000,4.400000>0.203200 translate<0,0.000000,0>}
cylinder{<29.600000,-0.1,4.400000><29.600000,0.135000,4.400000>0.050800 translate<0,0.000000,0>}}
box{<-0.635000,0,-0.635000><0.635000,0.036000,0.635000> rotate<0,-180.000000,0> translate<41.030000,0.000000,1.860000>}
box{<-0.635000,0,-0.635000><0.635000,0.036000,0.635000> rotate<0,-180.000000,0> translate<41.030000,0.000000,6.940000>}
box{<-0.317500,0,-2.540000><0.317500,0.036000,2.540000> rotate<0,-180.000000,0> translate<40.712500,0.000000,4.400000>}
box{<-0.635000,0,-0.635000><0.635000,0.036000,0.635000> rotate<0,-180.000000,0> translate<35.950000,0.000000,1.860000>}
box{<-0.635000,0,-0.635000><0.635000,0.036000,0.635000> rotate<0,-180.000000,0> translate<35.950000,0.000000,6.940000>}
box{<-0.317500,0,-2.540000><0.317500,0.036000,2.540000> rotate<0,-180.000000,0> translate<36.267500,0.000000,4.400000>}
box{<-1.905000,0,-0.889000><1.905000,0.036000,0.889000> rotate<0,-180.000000,0> translate<38.490000,0.000000,2.749000>}
box{<-1.905000,0,-0.889000><1.905000,0.036000,0.889000> rotate<0,-180.000000,0> translate<38.490000,0.000000,6.051000>}
box{<-0.635000,0,-0.635000><0.635000,0.036000,0.635000> rotate<0,-180.000000,0> translate<23.250000,0.000000,1.860000>}
box{<-0.635000,0,-0.635000><0.635000,0.036000,0.635000> rotate<0,-180.000000,0> translate<23.250000,0.000000,6.940000>}
box{<-0.317500,0,-2.540000><0.317500,0.036000,2.540000> rotate<0,-180.000000,0> translate<22.932500,0.000000,4.400000>}
box{<-0.635000,0,-0.635000><0.635000,0.036000,0.635000> rotate<0,-180.000000,0> translate<18.170000,0.000000,1.860000>}
box{<-0.635000,0,-0.635000><0.635000,0.036000,0.635000> rotate<0,-180.000000,0> translate<18.170000,0.000000,6.940000>}
box{<-0.317500,0,-2.540000><0.317500,0.036000,2.540000> rotate<0,-180.000000,0> translate<18.487500,0.000000,4.400000>}
box{<-1.905000,0,-0.889000><1.905000,0.036000,0.889000> rotate<0,-180.000000,0> translate<20.710000,0.000000,2.749000>}
box{<-1.905000,0,-0.889000><1.905000,0.036000,0.889000> rotate<0,-180.000000,0> translate<20.710000,0.000000,6.051000>}
//IC1 silk screen
cylinder{<0,0,0><0,0.036000,0>0.063500 translate<23.600000,-1.536000,4.000000>}
cylinder{<0,0,0><0,0.036000,0>0.063500 translate<23.600000,-1.536000,8.800000>}
box{<0,0,-0.063500><4.800000,0.036000,0.063500> rotate<0,90.000000,0> translate<23.600000,-1.536000,8.800000> }
cylinder{<0,0,0><0,0.036000,0>0.063500 translate<23.600000,-1.536000,8.800000>}
cylinder{<0,0,0><0,0.036000,0>0.063500 translate<30.400000,-1.536000,8.800000>}
box{<0,0,-0.063500><6.800000,0.036000,0.063500> rotate<0,0.000000,0> translate<23.600000,-1.536000,8.800000> }
cylinder{<0,0,0><0,0.036000,0>0.063500 translate<30.400000,-1.536000,8.800000>}
cylinder{<0,0,0><0,0.036000,0>0.063500 translate<30.400000,-1.536000,4.000000>}
box{<0,0,-0.063500><4.800000,0.036000,0.063500> rotate<0,-90.000000,0> translate<30.400000,-1.536000,4.000000> }
cylinder{<0,0,0><0,0.036000,0>0.063500 translate<30.400000,-1.536000,4.000000>}
cylinder{<0,0,0><0,0.036000,0>0.063500 translate<23.600000,-1.536000,4.000000>}
box{<0,0,-0.063500><6.800000,0.036000,0.063500> rotate<0,0.000000,0> translate<23.600000,-1.536000,4.000000> }
object{ARC(0.635000,0.127000,180.000000,360.000000,0.036000) translate<28.905000,-1.536000,8.305000>}
cylinder{<0,0,0><0,0.036000,0>0.063500 translate<29.540000,-1.536000,8.305000>}
cylinder{<0,0,0><0,0.036000,0>0.063500 translate<29.540000,-1.536000,8.740000>}
box{<0,0,-0.063500><0.435000,0.036000,0.063500> rotate<0,90.000000,0> translate<29.540000,-1.536000,8.740000> }
cylinder{<0,0,0><0,0.036000,0>0.063500 translate<28.270000,-1.536000,8.305000>}
cylinder{<0,0,0><0,0.036000,0>0.063500 translate<28.270000,-1.536000,8.740000>}
box{<0,0,-0.063500><0.435000,0.036000,0.063500> rotate<0,90.000000,0> translate<28.270000,-1.536000,8.740000> }
box{<-0.437500,0,-1.075000><0.437500,0.036000,1.075000> rotate<0,-90.000000,0> translate<31.550000,-1.536000,5.112500>}
box{<-0.437500,0,-1.075000><0.437500,0.036000,1.075000> rotate<0,-90.000000,0> translate<31.550000,-1.536000,7.652500>}
box{<-0.437500,0,-1.075000><0.437500,0.036000,1.075000> rotate<0,-270.000000,0> translate<22.450000,-1.536000,5.147500>}
box{<-0.437500,0,-1.075000><0.437500,0.036000,1.075000> rotate<0,-270.000000,0> translate<22.450000,-1.536000,7.687500>}
//IC2 silk screen
cylinder{<0,0,0><0,0.036000,0>0.101600 translate<4.900000,-1.536000,20.600000>}
cylinder{<0,0,0><0,0.036000,0>0.101600 translate<8.200000,-1.536000,20.600000>}
box{<0,0,-0.101600><3.300000,0.036000,0.101600> rotate<0,0.000000,0> translate<4.900000,-1.536000,20.600000> }
cylinder{<0,0,0><0,0.036000,0>0.101600 translate<8.200000,-1.536000,20.600000>}
cylinder{<0,0,0><0,0.036000,0>0.101600 translate<8.700000,-1.536000,20.600000>}
box{<0,0,-0.101600><0.500000,0.036000,0.101600> rotate<0,0.000000,0> translate<8.200000,-1.536000,20.600000> }
cylinder{<0,0,0><0,0.036000,0>0.101600 translate<8.700000,-1.536000,20.600000>}
cylinder{<0,0,0><0,0.036000,0>0.101600 translate<8.700000,-1.536000,25.400000>}
box{<0,0,-0.101600><4.800000,0.036000,0.101600> rotate<0,90.000000,0> translate<8.700000,-1.536000,25.400000> }
cylinder{<0,0,0><0,0.036000,0>0.101600 translate<8.700000,-1.536000,25.400000>}
cylinder{<0,0,0><0,0.036000,0>0.101600 translate<8.200000,-1.536000,25.400000>}
box{<0,0,-0.101600><0.500000,0.036000,0.101600> rotate<0,0.000000,0> translate<8.200000,-1.536000,25.400000> }
cylinder{<0,0,0><0,0.036000,0>0.101600 translate<8.200000,-1.536000,25.400000>}
cylinder{<0,0,0><0,0.036000,0>0.101600 translate<4.900000,-1.536000,25.400000>}
box{<0,0,-0.101600><3.300000,0.036000,0.101600> rotate<0,0.000000,0> translate<4.900000,-1.536000,25.400000> }
cylinder{<0,0,0><0,0.036000,0>0.101600 translate<4.900000,-1.536000,25.400000>}
cylinder{<0,0,0><0,0.036000,0>0.101600 translate<4.900000,-1.536000,20.600000>}
box{<0,0,-0.101600><4.800000,0.036000,0.101600> rotate<0,-90.000000,0> translate<4.900000,-1.536000,20.600000> }
cylinder{<0,0,0><0,0.036000,0>0.101600 translate<8.200000,-1.536000,20.600000>}
cylinder{<0,0,0><0,0.036000,0>0.101600 translate<8.200000,-1.536000,25.400000>}
box{<0,0,-0.101600><4.800000,0.036000,0.101600> rotate<0,90.000000,0> translate<8.200000,-1.536000,25.400000> }
box{<-0.245000,0,-0.550000><0.245000,0.036000,0.550000> rotate<0,-270.000000,0> translate<9.350000,-1.536000,24.905000>}
box{<-0.245000,0,-0.550000><0.245000,0.036000,0.550000> rotate<0,-270.000000,0> translate<9.350000,-1.536000,23.635000>}
box{<-0.245000,0,-0.550000><0.245000,0.036000,0.550000> rotate<0,-270.000000,0> translate<9.350000,-1.536000,22.365000>}
box{<-0.245000,0,-0.550000><0.245000,0.036000,0.550000> rotate<0,-270.000000,0> translate<9.350000,-1.536000,21.095000>}
box{<-0.245000,0,-0.550000><0.245000,0.036000,0.550000> rotate<0,-270.000000,0> translate<4.250000,-1.536000,21.095000>}
box{<-0.245000,0,-0.550000><0.245000,0.036000,0.550000> rotate<0,-270.000000,0> translate<4.250000,-1.536000,22.365000>}
box{<-0.245000,0,-0.550000><0.245000,0.036000,0.550000> rotate<0,-270.000000,0> translate<4.250000,-1.536000,23.635000>}
box{<-0.245000,0,-0.550000><0.245000,0.036000,0.550000> rotate<0,-270.000000,0> translate<4.250000,-1.536000,24.905000>}
//IC3 silk screen
cylinder{<0,0,0><0,0.036000,0>0.063500 translate<-7.337000,-1.536000,7.785400>}
cylinder{<0,0,0><0,0.036000,0>0.063500 translate<-7.337000,-1.536000,12.585400>}
box{<0,0,-0.063500><4.800000,0.036000,0.063500> rotate<0,90.000000,0> translate<-7.337000,-1.536000,12.585400> }
cylinder{<0,0,0><0,0.036000,0>0.063500 translate<-7.337000,-1.536000,12.585400>}
cylinder{<0,0,0><0,0.036000,0>0.063500 translate<-0.537000,-1.536000,12.585400>}
box{<0,0,-0.063500><6.800000,0.036000,0.063500> rotate<0,0.000000,0> translate<-7.337000,-1.536000,12.585400> }
cylinder{<0,0,0><0,0.036000,0>0.063500 translate<-0.537000,-1.536000,12.585400>}
cylinder{<0,0,0><0,0.036000,0>0.063500 translate<-0.537000,-1.536000,7.785400>}
box{<0,0,-0.063500><4.800000,0.036000,0.063500> rotate<0,-90.000000,0> translate<-0.537000,-1.536000,7.785400> }
cylinder{<0,0,0><0,0.036000,0>0.063500 translate<-0.537000,-1.536000,7.785400>}
cylinder{<0,0,0><0,0.036000,0>0.063500 translate<-7.337000,-1.536000,7.785400>}
box{<0,0,-0.063500><6.800000,0.036000,0.063500> rotate<0,0.000000,0> translate<-7.337000,-1.536000,7.785400> }
object{ARC(0.635000,0.127000,180.000000,360.000000,0.036000) translate<-2.032000,-1.536000,12.090400>}
cylinder{<0,0,0><0,0.036000,0>0.063500 translate<-1.397000,-1.536000,12.090400>}
cylinder{<0,0,0><0,0.036000,0>0.063500 translate<-1.397000,-1.536000,12.525400>}
box{<0,0,-0.063500><0.435000,0.036000,0.063500> rotate<0,90.000000,0> translate<-1.397000,-1.536000,12.525400> }
cylinder{<0,0,0><0,0.036000,0>0.063500 translate<-2.667000,-1.536000,12.090400>}
cylinder{<0,0,0><0,0.036000,0>0.063500 translate<-2.667000,-1.536000,12.525400>}
box{<0,0,-0.063500><0.435000,0.036000,0.063500> rotate<0,90.000000,0> translate<-2.667000,-1.536000,12.525400> }
box{<-0.437500,0,-1.075000><0.437500,0.036000,1.075000> rotate<0,-90.000000,0> translate<0.613000,-1.536000,8.897900>}
box{<-0.437500,0,-1.075000><0.437500,0.036000,1.075000> rotate<0,-90.000000,0> translate<0.613000,-1.536000,11.437900>}
box{<-0.437500,0,-1.075000><0.437500,0.036000,1.075000> rotate<0,-270.000000,0> translate<-8.487000,-1.536000,8.932900>}
box{<-0.437500,0,-1.075000><0.437500,0.036000,1.075000> rotate<0,-270.000000,0> translate<-8.487000,-1.536000,11.472900>}
//IC4 silk screen
cylinder{<0,0,0><0,0.036000,0>0.063500 translate<23.700000,-1.536000,9.700000>}
cylinder{<0,0,0><0,0.036000,0>0.063500 translate<23.700000,-1.536000,14.500000>}
box{<0,0,-0.063500><4.800000,0.036000,0.063500> rotate<0,90.000000,0> translate<23.700000,-1.536000,14.500000> }
cylinder{<0,0,0><0,0.036000,0>0.063500 translate<23.700000,-1.536000,14.500000>}
cylinder{<0,0,0><0,0.036000,0>0.063500 translate<30.500000,-1.536000,14.500000>}
box{<0,0,-0.063500><6.800000,0.036000,0.063500> rotate<0,0.000000,0> translate<23.700000,-1.536000,14.500000> }
cylinder{<0,0,0><0,0.036000,0>0.063500 translate<30.500000,-1.536000,14.500000>}
cylinder{<0,0,0><0,0.036000,0>0.063500 translate<30.500000,-1.536000,9.700000>}
box{<0,0,-0.063500><4.800000,0.036000,0.063500> rotate<0,-90.000000,0> translate<30.500000,-1.536000,9.700000> }
cylinder{<0,0,0><0,0.036000,0>0.063500 translate<30.500000,-1.536000,9.700000>}
cylinder{<0,0,0><0,0.036000,0>0.063500 translate<23.700000,-1.536000,9.700000>}
box{<0,0,-0.063500><6.800000,0.036000,0.063500> rotate<0,0.000000,0> translate<23.700000,-1.536000,9.700000> }
object{ARC(0.635000,0.127000,180.000000,360.000000,0.036000) translate<29.005000,-1.536000,14.005000>}
cylinder{<0,0,0><0,0.036000,0>0.063500 translate<29.640000,-1.536000,14.005000>}
cylinder{<0,0,0><0,0.036000,0>0.063500 translate<29.640000,-1.536000,14.440000>}
box{<0,0,-0.063500><0.435000,0.036000,0.063500> rotate<0,90.000000,0> translate<29.640000,-1.536000,14.440000> }
cylinder{<0,0,0><0,0.036000,0>0.063500 translate<28.370000,-1.536000,14.005000>}
cylinder{<0,0,0><0,0.036000,0>0.063500 translate<28.370000,-1.536000,14.440000>}
box{<0,0,-0.063500><0.435000,0.036000,0.063500> rotate<0,90.000000,0> translate<28.370000,-1.536000,14.440000> }
box{<-0.437500,0,-1.075000><0.437500,0.036000,1.075000> rotate<0,-90.000000,0> translate<31.650000,-1.536000,10.812500>}
box{<-0.437500,0,-1.075000><0.437500,0.036000,1.075000> rotate<0,-90.000000,0> translate<31.650000,-1.536000,13.352500>}
box{<-0.437500,0,-1.075000><0.437500,0.036000,1.075000> rotate<0,-270.000000,0> translate<22.550000,-1.536000,10.847500>}
box{<-0.437500,0,-1.075000><0.437500,0.036000,1.075000> rotate<0,-270.000000,0> translate<22.550000,-1.536000,13.387500>}
//J1 silk screen
cylinder{<0,0,0><0,0.036000,0>0.127000 translate<53.480000,0.000000,8.540000>}
cylinder{<0,0,0><0,0.036000,0>0.127000 translate<50.305000,0.000000,8.540000>}
box{<0,0,-0.127000><3.175000,0.036000,0.127000> rotate<0,0.000000,0> translate<50.305000,0.000000,8.540000> }
cylinder{<0,0,0><0,0.036000,0>0.127000 translate<50.305000,0.000000,8.540000>}
cylinder{<0,0,0><0,0.036000,0>0.127000 translate<50.305000,0.000000,7.905000>}
box{<0,0,-0.127000><0.635000,0.036000,0.127000> rotate<0,-90.000000,0> translate<50.305000,0.000000,7.905000> }
cylinder{<0,0,0><0,0.036000,0>0.127000 translate<50.305000,0.000000,7.905000>}
cylinder{<0,0,0><0,0.036000,0>0.127000 translate<50.940000,0.000000,7.905000>}
box{<0,0,-0.127000><0.635000,0.036000,0.127000> rotate<0,0.000000,0> translate<50.305000,0.000000,7.905000> }
cylinder{<0,0,0><0,0.036000,0>0.127000 translate<50.940000,0.000000,7.905000>}
cylinder{<0,0,0><0,0.036000,0>0.127000 translate<50.940000,0.000000,6.635000>}
box{<0,0,-0.127000><1.270000,0.036000,0.127000> rotate<0,-90.000000,0> translate<50.940000,0.000000,6.635000> }
cylinder{<0,0,0><0,0.036000,0>0.127000 translate<50.940000,0.000000,6.635000>}
cylinder{<0,0,0><0,0.036000,0>0.127000 translate<50.305000,0.000000,6.635000>}
box{<0,0,-0.127000><0.635000,0.036000,0.127000> rotate<0,0.000000,0> translate<50.305000,0.000000,6.635000> }
cylinder{<0,0,0><0,0.036000,0>0.127000 translate<50.305000,0.000000,6.635000>}
cylinder{<0,0,0><0,0.036000,0>0.127000 translate<50.305000,0.000000,5.365000>}
box{<0,0,-0.127000><1.270000,0.036000,0.127000> rotate<0,-90.000000,0> translate<50.305000,0.000000,5.365000> }
cylinder{<0,0,0><0,0.036000,0>0.127000 translate<50.305000,0.000000,5.365000>}
cylinder{<0,0,0><0,0.036000,0>0.127000 translate<50.940000,0.000000,5.365000>}
box{<0,0,-0.127000><0.635000,0.036000,0.127000> rotate<0,0.000000,0> translate<50.305000,0.000000,5.365000> }
cylinder{<0,0,0><0,0.036000,0>0.127000 translate<50.940000,0.000000,5.365000>}
cylinder{<0,0,0><0,0.036000,0>0.127000 translate<50.940000,0.000000,4.095000>}
box{<0,0,-0.127000><1.270000,0.036000,0.127000> rotate<0,-90.000000,0> translate<50.940000,0.000000,4.095000> }
cylinder{<0,0,0><0,0.036000,0>0.127000 translate<50.940000,0.000000,4.095000>}
cylinder{<0,0,0><0,0.036000,0>0.127000 translate<50.305000,0.000000,4.095000>}
box{<0,0,-0.127000><0.635000,0.036000,0.127000> rotate<0,0.000000,0> translate<50.305000,0.000000,4.095000> }
cylinder{<0,0,0><0,0.036000,0>0.127000 translate<50.305000,0.000000,4.095000>}
cylinder{<0,0,0><0,0.036000,0>0.127000 translate<50.305000,0.000000,3.460000>}
box{<0,0,-0.127000><0.635000,0.036000,0.127000> rotate<0,-90.000000,0> translate<50.305000,0.000000,3.460000> }
cylinder{<0,0,0><0,0.036000,0>0.127000 translate<50.305000,0.000000,3.460000>}
cylinder{<0,0,0><0,0.036000,0>0.127000 translate<53.480000,0.000000,3.460000>}
box{<0,0,-0.127000><3.175000,0.036000,0.127000> rotate<0,0.000000,0> translate<50.305000,0.000000,3.460000> }
cylinder{<0,0,0><0,0.036000,0>0.127000 translate<53.480000,0.000000,3.460000>}
cylinder{<0,0,0><0,0.036000,0>0.127000 translate<53.480000,0.000000,4.349000>}
box{<0,0,-0.127000><0.889000,0.036000,0.127000> rotate<0,90.000000,0> translate<53.480000,0.000000,4.349000> }
cylinder{<0,0,0><0,0.036000,0>0.127000 translate<53.480000,0.000000,4.349000>}
cylinder{<0,0,0><0,0.036000,0>0.127000 translate<53.480000,0.000000,7.270000>}
box{<0,0,-0.127000><2.921000,0.036000,0.127000> rotate<0,90.000000,0> translate<53.480000,0.000000,7.270000> }
cylinder{<0,0,0><0,0.036000,0>0.127000 translate<53.480000,0.000000,7.270000>}
cylinder{<0,0,0><0,0.036000,0>0.127000 translate<53.480000,0.000000,8.540000>}
box{<0,0,-0.127000><1.270000,0.036000,0.127000> rotate<0,90.000000,0> translate<53.480000,0.000000,8.540000> }
cylinder{<0,0,0><0,0.036000,0>0.127000 translate<53.480000,0.000000,7.270000>}
cylinder{<0,0,0><0,0.036000,0>0.127000 translate<61.481000,0.000000,7.270000>}
box{<0,0,-0.127000><8.001000,0.036000,0.127000> rotate<0,0.000000,0> translate<53.480000,0.000000,7.270000> }
cylinder{<0,0,0><0,0.036000,0>0.127000 translate<61.481000,0.000000,7.270000>}
cylinder{<0,0,0><0,0.036000,0>0.127000 translate<61.989000,0.000000,7.016000>}
box{<0,0,-0.127000><0.567961,0.036000,0.127000> rotate<0,26.563298,0> translate<61.481000,0.000000,7.270000> }
cylinder{<0,0,0><0,0.036000,0>0.127000 translate<61.989000,0.000000,7.016000>}
cylinder{<0,0,0><0,0.036000,0>0.127000 translate<61.989000,0.000000,5.949200>}
box{<0,0,-0.127000><1.066800,0.036000,0.127000> rotate<0,-90.000000,0> translate<61.989000,0.000000,5.949200> }
cylinder{<0,0,0><0,0.036000,0>0.127000 translate<53.480000,0.000000,4.730000>}
cylinder{<0,0,0><0,0.036000,0>0.127000 translate<61.481000,0.000000,4.730000>}
box{<0,0,-0.127000><8.001000,0.036000,0.127000> rotate<0,0.000000,0> translate<53.480000,0.000000,4.730000> }
cylinder{<0,0,0><0,0.036000,0>0.127000 translate<61.481000,0.000000,4.730000>}
cylinder{<0,0,0><0,0.036000,0>0.127000 translate<61.989000,0.000000,4.984000>}
box{<0,0,-0.127000><0.567961,0.036000,0.127000> rotate<0,-26.563298,0> translate<61.481000,0.000000,4.730000> }
cylinder{<0,0,0><0,0.036000,0>0.127000 translate<61.989000,0.000000,4.984000>}
cylinder{<0,0,0><0,0.036000,0>0.127000 translate<61.989000,0.000000,5.949200>}
box{<0,0,-0.127000><0.965200,0.036000,0.127000> rotate<0,90.000000,0> translate<61.989000,0.000000,5.949200> }
cylinder{<0,0,0><0,0.036000,0>0.127000 translate<61.989000,0.000000,5.949200>}
cylinder{<0,0,0><0,0.036000,0>0.127000 translate<61.989000,0.000000,6.050800>}
box{<0,0,-0.127000><0.101600,0.036000,0.127000> rotate<0,90.000000,0> translate<61.989000,0.000000,6.050800> }
cylinder{<0,0,0><0,0.036000,0>0.127000 translate<53.480000,0.000000,7.270000>}
cylinder{<0,0,0><0,0.036000,0>0.127000 translate<53.480000,0.000000,6.889000>}
box{<0,0,-0.127000><0.381000,0.036000,0.127000> rotate<0,-90.000000,0> translate<53.480000,0.000000,6.889000> }
cylinder{<0,0,0><0,0.036000,0>0.127000 translate<53.480000,0.000000,4.349000>}
cylinder{<0,0,0><0,0.036000,0>0.127000 translate<59.703000,0.000000,4.349000>}
box{<0,0,-0.127000><6.223000,0.036000,0.127000> rotate<0,0.000000,0> translate<53.480000,0.000000,4.349000> }
cylinder{<0,0,0><0,0.036000,0>0.127000 translate<59.703000,0.000000,4.349000>}
cylinder{<0,0,0><0,0.036000,0>0.127000 translate<60.058600,0.000000,4.679200>}
box{<0,0,-0.127000><0.485266,0.036000,0.127000> rotate<0,-42.876074,0> translate<59.703000,0.000000,4.349000> }
cylinder{<0,0,0><0,0.036000,0>0.127000 translate<53.480000,0.000000,7.651000>}
cylinder{<0,0,0><0,0.036000,0>0.127000 translate<59.703000,0.000000,7.651000>}
box{<0,0,-0.127000><6.223000,0.036000,0.127000> rotate<0,0.000000,0> translate<53.480000,0.000000,7.651000> }
cylinder{<0,0,0><0,0.036000,0>0.127000 translate<59.703000,0.000000,7.651000>}
cylinder{<0,0,0><0,0.036000,0>0.127000 translate<60.058600,0.000000,7.320800>}
box{<0,0,-0.127000><0.485266,0.036000,0.127000> rotate<0,42.876074,0> translate<59.703000,0.000000,7.651000> }
box{<-0.254000,0,-0.254000><0.254000,0.036000,0.254000> rotate<0,-90.000000,0> translate<48.400000,0.000000,7.270000>}
box{<-0.254000,0,-0.254000><0.254000,0.036000,0.254000> rotate<0,-90.000000,0> translate<48.400000,0.000000,4.730000>}
box{<-0.254000,0,-1.117600><0.254000,0.036000,1.117600> rotate<0,-90.000000,0> translate<49.797000,0.000000,7.270000>}
box{<-0.254000,0,-1.117600><0.254000,0.036000,1.117600> rotate<0,-90.000000,0> translate<49.797000,0.000000,4.730000>}
//L1 silk screen
cylinder{<0,0,0><0,0.036000,0>0.101600 translate<-15.500000,0.000000,13.500000>}
cylinder{<0,0,0><0,0.036000,0>0.101600 translate<-2.475000,0.000000,13.500000>}
box{<0,0,-0.101600><13.025000,0.036000,0.101600> rotate<0,0.000000,0> translate<-15.500000,0.000000,13.500000> }
cylinder{<0,0,0><0,0.036000,0>0.101600 translate<-2.475000,0.000000,13.500000>}
cylinder{<0,0,0><0,0.036000,0>0.101600 translate<-2.475000,0.000000,0.500000>}
box{<0,0,-0.101600><13.000000,0.036000,0.101600> rotate<0,-90.000000,0> translate<-2.475000,0.000000,0.500000> }
cylinder{<0,0,0><0,0.036000,0>0.101600 translate<-2.475000,0.000000,0.500000>}
cylinder{<0,0,0><0,0.036000,0>0.101600 translate<-15.500000,0.000000,0.500000>}
box{<0,0,-0.101600><13.025000,0.036000,0.101600> rotate<0,0.000000,0> translate<-15.500000,0.000000,0.500000> }
cylinder{<0,0,0><0,0.036000,0>0.101600 translate<-15.500000,0.000000,0.500000>}
cylinder{<0,0,0><0,0.036000,0>0.101600 translate<-15.500000,0.000000,13.500000>}
box{<0,0,-0.101600><13.000000,0.036000,0.101600> rotate<0,90.000000,0> translate<-15.500000,0.000000,13.500000> }
object{ARC(0.535028,1.016000,35.329023,237.394609,0.036000) translate<-13.237100,0.000000,11.233709>}
object{ARC(0.535028,1.016000,125.329023,327.394609,0.036000) translate<-13.233709,0.000000,2.762900>}
object{ARC(0.535028,1.016000,215.329023,417.394609,0.036000) translate<-4.762900,0.000000,2.766291>}
object{ARC(0.535028,1.016000,305.329023,507.394609,0.036000) translate<-4.766291,0.000000,11.237100>}
object{ARC(5.900006,0.203200,33.716520,89.926299,0.036000) translate<-9.007591,0.000000,7.000000>}
object{ARC(5.900006,0.203200,89.908652,146.865282,0.036000) translate<-9.009406,0.000000,7.000000>}
object{ARC(5.900006,0.203200,213.716520,269.926299,0.036000) translate<-8.992409,0.000000,7.000000>}
object{ARC(5.900006,0.203200,269.908652,326.865282,0.036000) translate<-8.990594,0.000000,7.000000>}
difference{
cylinder{<-9.000000,0,7.000000><-9.000000,0.036000,7.000000>6.001600 translate<0,0.000000,0>}
cylinder{<-9.000000,-0.1,7.000000><-9.000000,0.135000,7.000000>5.798400 translate<0,0.000000,0>}}
//Q1 silk screen
cylinder{<0,0,0><0,0.036000,0>0.076200 translate<-4.901000,0.000000,25.282000>}
cylinder{<0,0,0><0,0.036000,0>0.076200 translate<-4.647000,0.000000,25.536000>}
box{<0,0,-0.076200><0.359210,0.036000,0.076200> rotate<0,-44.997030,0> translate<-4.901000,0.000000,25.282000> }
cylinder{<0,0,0><0,0.036000,0>0.076200 translate<-4.901000,0.000000,25.282000>}
cylinder{<0,0,0><0,0.036000,0>0.076200 translate<-14.299000,0.000000,25.282000>}
box{<0,0,-0.076200><9.398000,0.036000,0.076200> rotate<0,0.000000,0> translate<-14.299000,0.000000,25.282000> }
cylinder{<0,0,0><0,0.036000,0>0.076200 translate<-14.553000,0.000000,25.536000>}
cylinder{<0,0,0><0,0.036000,0>0.076200 translate<-14.299000,0.000000,25.282000>}
box{<0,0,-0.076200><0.359210,0.036000,0.076200> rotate<0,44.997030,0> translate<-14.553000,0.000000,25.536000> }
cylinder{<0,0,0><0,0.036000,0>0.076200 translate<-4.520000,0.000000,28.457000>}
cylinder{<0,0,0><0,0.036000,0>0.076200 translate<-4.647000,0.000000,25.536000>}
box{<0,0,-0.076200><2.923760,0.036000,0.076200> rotate<0,-87.504672,0> translate<-4.647000,0.000000,25.536000> }
cylinder{<0,0,0><0,0.036000,0>0.076200 translate<-14.553000,0.000000,25.536000>}
cylinder{<0,0,0><0,0.036000,0>0.076200 translate<-14.680000,0.000000,28.457000>}
box{<0,0,-0.076200><2.923760,0.036000,0.076200> rotate<0,87.504672,0> translate<-14.680000,0.000000,28.457000> }
difference{
cylinder{<-14.095800,0,25.891600><-14.095800,0.036000,25.891600>0.254000 translate<0,0.000000,0>}
cylinder{<-14.095800,-0.1,25.891600><-14.095800,0.135000,25.891600>0.254000 translate<0,0.000000,0>}}
box{<-5.334000,0,-0.381000><5.334000,0.036000,0.381000> rotate<0,-0.000000,0> translate<-9.600000,0.000000,29.219000>}
box{<-0.952500,0,-0.254000><0.952500,0.036000,0.254000> rotate<0,-0.000000,0> translate<-13.981500,0.000000,28.584000>}
box{<-0.381000,0,-0.254000><0.381000,0.036000,0.254000> rotate<0,-0.000000,0> translate<-10.870000,0.000000,28.584000>}
box{<-0.889000,0,-0.254000><0.889000,0.036000,0.254000> rotate<0,-0.000000,0> translate<-12.140000,0.000000,28.584000>}
box{<-0.381000,0,-0.254000><0.381000,0.036000,0.254000> rotate<0,-0.000000,0> translate<-8.330000,0.000000,28.584000>}
box{<-0.952500,0,-0.254000><0.952500,0.036000,0.254000> rotate<0,-0.000000,0> translate<-5.218500,0.000000,28.584000>}
box{<-0.889000,0,-0.254000><0.889000,0.036000,0.254000> rotate<0,-0.000000,0> translate<-9.600000,0.000000,28.584000>}
box{<-0.889000,0,-0.254000><0.889000,0.036000,0.254000> rotate<0,-0.000000,0> translate<-7.060000,0.000000,28.584000>}
//Q2 silk screen
cylinder{<0,0,0><0,0.036000,0>0.127000 translate<21.777200,0.000000,25.028400>}
cylinder{<0,0,0><0,0.036000,0>0.127000 translate<31.022800,0.000000,25.028400>}
box{<0,0,-0.127000><9.245600,0.036000,0.127000> rotate<0,0.000000,0> translate<21.777200,0.000000,25.028400> }
cylinder{<0,0,0><0,0.036000,0>0.127000 translate<31.022800,0.000000,25.028400>}
cylinder{<0,0,0><0,0.036000,0>0.127000 translate<31.530800,0.000000,26.552400>}
box{<0,0,-0.127000><1.606437,0.036000,0.127000> rotate<0,-71.560328,0> translate<31.022800,0.000000,25.028400> }
cylinder{<0,0,0><0,0.036000,0>0.127000 translate<31.530800,0.000000,26.552400>}
cylinder{<0,0,0><0,0.036000,0>0.127000 translate<31.530800,0.000000,28.152600>}
box{<0,0,-0.127000><1.600200,0.036000,0.127000> rotate<0,90.000000,0> translate<31.530800,0.000000,28.152600> }
cylinder{<0,0,0><0,0.036000,0>0.127000 translate<21.269200,0.000000,28.152600>}
cylinder{<0,0,0><0,0.036000,0>0.127000 translate<21.269200,0.000000,26.552400>}
box{<0,0,-0.127000><1.600200,0.036000,0.127000> rotate<0,-90.000000,0> translate<21.269200,0.000000,26.552400> }
cylinder{<0,0,0><0,0.036000,0>0.127000 translate<21.269200,0.000000,26.552400>}
cylinder{<0,0,0><0,0.036000,0>0.127000 translate<21.777200,0.000000,25.028400>}
box{<0,0,-0.127000><1.606437,0.036000,0.127000> rotate<0,71.560328,0> translate<21.269200,0.000000,26.552400> }
cylinder{<0,0,0><0,0.036000,0>0.127000 translate<21.269200,0.000000,28.152600>}
cylinder{<0,0,0><0,0.036000,0>0.127000 translate<31.530800,0.000000,28.152600>}
box{<0,0,-0.127000><10.261600,0.036000,0.127000> rotate<0,0.000000,0> translate<21.269200,0.000000,28.152600> }
box{<-5.283200,0,-0.711200><5.283200,0.036000,0.711200> rotate<0,-0.000000,0> translate<26.400000,0.000000,28.889200>}
//Q3 silk screen
cylinder{<0,0,0><0,0.036000,0>0.127000 translate<33.377200,0.000000,25.028400>}
cylinder{<0,0,0><0,0.036000,0>0.127000 translate<42.622800,0.000000,25.028400>}
box{<0,0,-0.127000><9.245600,0.036000,0.127000> rotate<0,0.000000,0> translate<33.377200,0.000000,25.028400> }
cylinder{<0,0,0><0,0.036000,0>0.127000 translate<42.622800,0.000000,25.028400>}
cylinder{<0,0,0><0,0.036000,0>0.127000 translate<43.130800,0.000000,26.552400>}
box{<0,0,-0.127000><1.606437,0.036000,0.127000> rotate<0,-71.560328,0> translate<42.622800,0.000000,25.028400> }
cylinder{<0,0,0><0,0.036000,0>0.127000 translate<43.130800,0.000000,26.552400>}
cylinder{<0,0,0><0,0.036000,0>0.127000 translate<43.130800,0.000000,28.152600>}
box{<0,0,-0.127000><1.600200,0.036000,0.127000> rotate<0,90.000000,0> translate<43.130800,0.000000,28.152600> }
cylinder{<0,0,0><0,0.036000,0>0.127000 translate<32.869200,0.000000,28.152600>}
cylinder{<0,0,0><0,0.036000,0>0.127000 translate<32.869200,0.000000,26.552400>}
box{<0,0,-0.127000><1.600200,0.036000,0.127000> rotate<0,-90.000000,0> translate<32.869200,0.000000,26.552400> }
cylinder{<0,0,0><0,0.036000,0>0.127000 translate<32.869200,0.000000,26.552400>}
cylinder{<0,0,0><0,0.036000,0>0.127000 translate<33.377200,0.000000,25.028400>}
box{<0,0,-0.127000><1.606437,0.036000,0.127000> rotate<0,71.560328,0> translate<32.869200,0.000000,26.552400> }
cylinder{<0,0,0><0,0.036000,0>0.127000 translate<32.869200,0.000000,28.152600>}
cylinder{<0,0,0><0,0.036000,0>0.127000 translate<43.130800,0.000000,28.152600>}
box{<0,0,-0.127000><10.261600,0.036000,0.127000> rotate<0,0.000000,0> translate<32.869200,0.000000,28.152600> }
box{<-5.283200,0,-0.711200><5.283200,0.036000,0.711200> rotate<0,-0.000000,0> translate<38.000000,0.000000,28.889200>}
//R1 silk screen
cylinder{<0,0,0><0,0.036000,0>0.076200 translate<-15.320400,-1.536000,14.064000>}
cylinder{<0,0,0><0,0.036000,0>0.076200 translate<-15.320400,-1.536000,9.315000>}
box{<0,0,-0.076200><4.749000,0.036000,0.076200> rotate<0,-90.000000,0> translate<-15.320400,-1.536000,9.315000> }
cylinder{<0,0,0><0,0.036000,0>0.076200 translate<-12.374400,-1.536000,14.064000>}
cylinder{<0,0,0><0,0.036000,0>0.076200 translate<-12.374400,-1.536000,9.315000>}
box{<0,0,-0.076200><4.749000,0.036000,0.076200> rotate<0,-90.000000,0> translate<-12.374400,-1.536000,9.315000> }
box{<-0.424950,0,-1.550050><0.424950,0.036000,1.550050> rotate<0,-270.000000,0> translate<-13.848050,-1.536000,14.477450>}
box{<-0.424950,0,-1.550050><0.424950,0.036000,1.550050> rotate<0,-270.000000,0> translate<-13.848050,-1.536000,8.914850>}
//R2 silk screen
cylinder{<0,0,0><0,0.036000,0>0.076200 translate<-13.688800,0.000000,22.735000>}
cylinder{<0,0,0><0,0.036000,0>0.076200 translate<-15.111200,0.000000,22.735000>}
box{<0,0,-0.076200><1.422400,0.036000,0.076200> rotate<0,0.000000,0> translate<-15.111200,0.000000,22.735000> }
cylinder{<0,0,0><0,0.036000,0>0.076200 translate<-13.688800,0.000000,21.465000>}
cylinder{<0,0,0><0,0.036000,0>0.076200 translate<-15.111200,0.000000,21.465000>}
box{<0,0,-0.076200><1.422400,0.036000,0.076200> rotate<0,0.000000,0> translate<-15.111200,0.000000,21.465000> }
box{<-0.177800,0,-0.711200><0.177800,0.036000,0.711200> rotate<0,-0.000000,0> translate<-15.263600,0.000000,22.100000>}
box{<-0.177800,0,-0.711200><0.177800,0.036000,0.711200> rotate<0,-0.000000,0> translate<-13.536400,0.000000,22.100000>}
//R3 silk screen
cylinder{<0,0,0><0,0.036000,0>0.076200 translate<3.025000,0.000000,6.911800>}
cylinder{<0,0,0><0,0.036000,0>0.076200 translate<3.025000,0.000000,8.334200>}
box{<0,0,-0.076200><1.422400,0.036000,0.076200> rotate<0,90.000000,0> translate<3.025000,0.000000,8.334200> }
cylinder{<0,0,0><0,0.036000,0>0.076200 translate<1.755000,0.000000,6.911800>}
cylinder{<0,0,0><0,0.036000,0>0.076200 translate<1.755000,0.000000,8.334200>}
box{<0,0,-0.076200><1.422400,0.036000,0.076200> rotate<0,90.000000,0> translate<1.755000,0.000000,8.334200> }
box{<-0.177800,0,-0.711200><0.177800,0.036000,0.711200> rotate<0,-270.000000,0> translate<2.390000,0.000000,8.486600>}
box{<-0.177800,0,-0.711200><0.177800,0.036000,0.711200> rotate<0,-270.000000,0> translate<2.390000,0.000000,6.759400>}
//R4 silk screen
cylinder{<0,0,0><0,0.036000,0>0.076200 translate<49.362000,0.000000,17.527000>}
cylinder{<0,0,0><0,0.036000,0>0.076200 translate<44.613000,0.000000,17.527000>}
box{<0,0,-0.076200><4.749000,0.036000,0.076200> rotate<0,0.000000,0> translate<44.613000,0.000000,17.527000> }
cylinder{<0,0,0><0,0.036000,0>0.076200 translate<49.362000,0.000000,20.473000>}
cylinder{<0,0,0><0,0.036000,0>0.076200 translate<44.613000,0.000000,20.473000>}
box{<0,0,-0.076200><4.749000,0.036000,0.076200> rotate<0,0.000000,0> translate<44.613000,0.000000,20.473000> }
box{<-0.424950,0,-1.550050><0.424950,0.036000,1.550050> rotate<0,-180.000000,0> translate<49.775450,0.000000,18.999350>}
box{<-0.424950,0,-1.550050><0.424950,0.036000,1.550050> rotate<0,-180.000000,0> translate<44.212850,0.000000,18.999350>}
//R5 silk screen
cylinder{<0,0,0><0,0.036000,0>0.076200 translate<-1.663800,-1.536000,26.570000>}
cylinder{<0,0,0><0,0.036000,0>0.076200 translate<-3.086200,-1.536000,26.570000>}
box{<0,0,-0.076200><1.422400,0.036000,0.076200> rotate<0,0.000000,0> translate<-3.086200,-1.536000,26.570000> }
cylinder{<0,0,0><0,0.036000,0>0.076200 translate<-1.663800,-1.536000,27.840000>}
cylinder{<0,0,0><0,0.036000,0>0.076200 translate<-3.086200,-1.536000,27.840000>}
box{<0,0,-0.076200><1.422400,0.036000,0.076200> rotate<0,0.000000,0> translate<-3.086200,-1.536000,27.840000> }
box{<-0.177800,0,-0.711200><0.177800,0.036000,0.711200> rotate<0,-0.000000,0> translate<-3.238600,-1.536000,27.205000>}
box{<-0.177800,0,-0.711200><0.177800,0.036000,0.711200> rotate<0,-0.000000,0> translate<-1.511400,-1.536000,27.205000>}
//R6 silk screen
cylinder{<0,0,0><0,0.036000,0>0.076200 translate<-8.788800,0.000000,22.835000>}
cylinder{<0,0,0><0,0.036000,0>0.076200 translate<-10.211200,0.000000,22.835000>}
box{<0,0,-0.076200><1.422400,0.036000,0.076200> rotate<0,0.000000,0> translate<-10.211200,0.000000,22.835000> }
cylinder{<0,0,0><0,0.036000,0>0.076200 translate<-8.788800,0.000000,21.565000>}
cylinder{<0,0,0><0,0.036000,0>0.076200 translate<-10.211200,0.000000,21.565000>}
box{<0,0,-0.076200><1.422400,0.036000,0.076200> rotate<0,0.000000,0> translate<-10.211200,0.000000,21.565000> }
box{<-0.177800,0,-0.711200><0.177800,0.036000,0.711200> rotate<0,-0.000000,0> translate<-10.363600,0.000000,22.200000>}
box{<-0.177800,0,-0.711200><0.177800,0.036000,0.711200> rotate<0,-0.000000,0> translate<-8.636400,0.000000,22.200000>}
//R7 silk screen
cylinder{<0,0,0><0,0.036000,0>0.076200 translate<12.947200,0.000000,3.741000>}
cylinder{<0,0,0><0,0.036000,0>0.076200 translate<11.524800,0.000000,3.741000>}
box{<0,0,-0.076200><1.422400,0.036000,0.076200> rotate<0,0.000000,0> translate<11.524800,0.000000,3.741000> }
cylinder{<0,0,0><0,0.036000,0>0.076200 translate<12.947200,0.000000,2.471000>}
cylinder{<0,0,0><0,0.036000,0>0.076200 translate<11.524800,0.000000,2.471000>}
box{<0,0,-0.076200><1.422400,0.036000,0.076200> rotate<0,0.000000,0> translate<11.524800,0.000000,2.471000> }
box{<-0.177800,0,-0.711200><0.177800,0.036000,0.711200> rotate<0,-0.000000,0> translate<11.372400,0.000000,3.106000>}
box{<-0.177800,0,-0.711200><0.177800,0.036000,0.711200> rotate<0,-0.000000,0> translate<13.099600,0.000000,3.106000>}
//R8 silk screen
cylinder{<0,0,0><0,0.036000,0>0.076200 translate<6.998800,0.000000,3.425000>}
cylinder{<0,0,0><0,0.036000,0>0.076200 translate<8.421200,0.000000,3.425000>}
box{<0,0,-0.076200><1.422400,0.036000,0.076200> rotate<0,0.000000,0> translate<6.998800,0.000000,3.425000> }
cylinder{<0,0,0><0,0.036000,0>0.076200 translate<6.998800,0.000000,4.695000>}
cylinder{<0,0,0><0,0.036000,0>0.076200 translate<8.421200,0.000000,4.695000>}
box{<0,0,-0.076200><1.422400,0.036000,0.076200> rotate<0,0.000000,0> translate<6.998800,0.000000,4.695000> }
box{<-0.177800,0,-0.711200><0.177800,0.036000,0.711200> rotate<0,-180.000000,0> translate<8.573600,0.000000,4.060000>}
box{<-0.177800,0,-0.711200><0.177800,0.036000,0.711200> rotate<0,-180.000000,0> translate<6.846400,0.000000,4.060000>}
//R9 silk screen
cylinder{<0,0,0><0,0.036000,0>0.076200 translate<15.788800,-1.536000,11.163600>}
cylinder{<0,0,0><0,0.036000,0>0.076200 translate<17.211200,-1.536000,11.163600>}
box{<0,0,-0.076200><1.422400,0.036000,0.076200> rotate<0,0.000000,0> translate<15.788800,-1.536000,11.163600> }
cylinder{<0,0,0><0,0.036000,0>0.076200 translate<15.788800,-1.536000,9.893600>}
cylinder{<0,0,0><0,0.036000,0>0.076200 translate<17.211200,-1.536000,9.893600>}
box{<0,0,-0.076200><1.422400,0.036000,0.076200> rotate<0,0.000000,0> translate<15.788800,-1.536000,9.893600> }
box{<-0.177800,0,-0.711200><0.177800,0.036000,0.711200> rotate<0,-180.000000,0> translate<17.363600,-1.536000,10.528600>}
box{<-0.177800,0,-0.711200><0.177800,0.036000,0.711200> rotate<0,-180.000000,0> translate<15.636400,-1.536000,10.528600>}
//R10 silk screen
cylinder{<0,0,0><0,0.036000,0>0.076200 translate<6.299200,-1.536000,11.430000>}
cylinder{<0,0,0><0,0.036000,0>0.076200 translate<4.876800,-1.536000,11.430000>}
box{<0,0,-0.076200><1.422400,0.036000,0.076200> rotate<0,0.000000,0> translate<4.876800,-1.536000,11.430000> }
cylinder{<0,0,0><0,0.036000,0>0.076200 translate<6.299200,-1.536000,12.700000>}
cylinder{<0,0,0><0,0.036000,0>0.076200 translate<4.876800,-1.536000,12.700000>}
box{<0,0,-0.076200><1.422400,0.036000,0.076200> rotate<0,0.000000,0> translate<4.876800,-1.536000,12.700000> }
box{<-0.177800,0,-0.711200><0.177800,0.036000,0.711200> rotate<0,-0.000000,0> translate<4.724400,-1.536000,12.065000>}
box{<-0.177800,0,-0.711200><0.177800,0.036000,0.711200> rotate<0,-0.000000,0> translate<6.451600,-1.536000,12.065000>}
//R11 silk screen
cylinder{<0,0,0><0,0.036000,0>0.076200 translate<5.130800,-1.536000,15.240000>}
cylinder{<0,0,0><0,0.036000,0>0.076200 translate<6.553200,-1.536000,15.240000>}
box{<0,0,-0.076200><1.422400,0.036000,0.076200> rotate<0,0.000000,0> translate<5.130800,-1.536000,15.240000> }
cylinder{<0,0,0><0,0.036000,0>0.076200 translate<5.130800,-1.536000,13.970000>}
cylinder{<0,0,0><0,0.036000,0>0.076200 translate<6.553200,-1.536000,13.970000>}
box{<0,0,-0.076200><1.422400,0.036000,0.076200> rotate<0,0.000000,0> translate<5.130800,-1.536000,13.970000> }
box{<-0.177800,0,-0.711200><0.177800,0.036000,0.711200> rotate<0,-180.000000,0> translate<6.705600,-1.536000,14.605000>}
box{<-0.177800,0,-0.711200><0.177800,0.036000,0.711200> rotate<0,-180.000000,0> translate<4.978400,-1.536000,14.605000>}
//R12 silk screen
cylinder{<0,0,0><0,0.036000,0>0.076200 translate<13.867000,-1.536000,7.905200>}
cylinder{<0,0,0><0,0.036000,0>0.076200 translate<13.867000,-1.536000,6.482800>}
box{<0,0,-0.076200><1.422400,0.036000,0.076200> rotate<0,-90.000000,0> translate<13.867000,-1.536000,6.482800> }
cylinder{<0,0,0><0,0.036000,0>0.076200 translate<12.597000,-1.536000,7.905200>}
cylinder{<0,0,0><0,0.036000,0>0.076200 translate<12.597000,-1.536000,6.482800>}
box{<0,0,-0.076200><1.422400,0.036000,0.076200> rotate<0,-90.000000,0> translate<12.597000,-1.536000,6.482800> }
box{<-0.177800,0,-0.711200><0.177800,0.036000,0.711200> rotate<0,-90.000000,0> translate<13.232000,-1.536000,6.330400>}
box{<-0.177800,0,-0.711200><0.177800,0.036000,0.711200> rotate<0,-90.000000,0> translate<13.232000,-1.536000,8.057600>}
//R13 silk screen
cylinder{<0,0,0><0,0.036000,0>0.076200 translate<20.435000,-1.536000,27.511200>}
cylinder{<0,0,0><0,0.036000,0>0.076200 translate<20.435000,-1.536000,26.088800>}
box{<0,0,-0.076200><1.422400,0.036000,0.076200> rotate<0,-90.000000,0> translate<20.435000,-1.536000,26.088800> }
cylinder{<0,0,0><0,0.036000,0>0.076200 translate<19.165000,-1.536000,27.511200>}
cylinder{<0,0,0><0,0.036000,0>0.076200 translate<19.165000,-1.536000,26.088800>}
box{<0,0,-0.076200><1.422400,0.036000,0.076200> rotate<0,-90.000000,0> translate<19.165000,-1.536000,26.088800> }
box{<-0.177800,0,-0.711200><0.177800,0.036000,0.711200> rotate<0,-90.000000,0> translate<19.800000,-1.536000,25.936400>}
box{<-0.177800,0,-0.711200><0.177800,0.036000,0.711200> rotate<0,-90.000000,0> translate<19.800000,-1.536000,27.663600>}
//R14 silk screen
cylinder{<0,0,0><0,0.036000,0>0.076200 translate<35.143200,-1.536000,22.185000>}
cylinder{<0,0,0><0,0.036000,0>0.076200 translate<33.720800,-1.536000,22.185000>}
box{<0,0,-0.076200><1.422400,0.036000,0.076200> rotate<0,0.000000,0> translate<33.720800,-1.536000,22.185000> }
cylinder{<0,0,0><0,0.036000,0>0.076200 translate<35.143200,-1.536000,23.455000>}
cylinder{<0,0,0><0,0.036000,0>0.076200 translate<33.720800,-1.536000,23.455000>}
box{<0,0,-0.076200><1.422400,0.036000,0.076200> rotate<0,0.000000,0> translate<33.720800,-1.536000,23.455000> }
box{<-0.177800,0,-0.711200><0.177800,0.036000,0.711200> rotate<0,-0.000000,0> translate<33.568400,-1.536000,22.820000>}
box{<-0.177800,0,-0.711200><0.177800,0.036000,0.711200> rotate<0,-0.000000,0> translate<35.295600,-1.536000,22.820000>}
//R15 silk screen
cylinder{<0,0,0><0,0.036000,0>0.076200 translate<17.511200,-1.536000,12.965000>}
cylinder{<0,0,0><0,0.036000,0>0.076200 translate<16.088800,-1.536000,12.965000>}
box{<0,0,-0.076200><1.422400,0.036000,0.076200> rotate<0,0.000000,0> translate<16.088800,-1.536000,12.965000> }
cylinder{<0,0,0><0,0.036000,0>0.076200 translate<17.511200,-1.536000,14.235000>}
cylinder{<0,0,0><0,0.036000,0>0.076200 translate<16.088800,-1.536000,14.235000>}
box{<0,0,-0.076200><1.422400,0.036000,0.076200> rotate<0,0.000000,0> translate<16.088800,-1.536000,14.235000> }
box{<-0.177800,0,-0.711200><0.177800,0.036000,0.711200> rotate<0,-0.000000,0> translate<15.936400,-1.536000,13.600000>}
box{<-0.177800,0,-0.711200><0.177800,0.036000,0.711200> rotate<0,-0.000000,0> translate<17.663600,-1.536000,13.600000>}
//R16 silk screen
cylinder{<0,0,0><0,0.036000,0>0.076200 translate<5.180000,0.000000,14.996200>}
cylinder{<0,0,0><0,0.036000,0>0.076200 translate<5.180000,0.000000,13.573800>}
box{<0,0,-0.076200><1.422400,0.036000,0.076200> rotate<0,-90.000000,0> translate<5.180000,0.000000,13.573800> }
cylinder{<0,0,0><0,0.036000,0>0.076200 translate<6.450000,0.000000,14.996200>}
cylinder{<0,0,0><0,0.036000,0>0.076200 translate<6.450000,0.000000,13.573800>}
box{<0,0,-0.076200><1.422400,0.036000,0.076200> rotate<0,-90.000000,0> translate<6.450000,0.000000,13.573800> }
box{<-0.177800,0,-0.711200><0.177800,0.036000,0.711200> rotate<0,-90.000000,0> translate<5.815000,0.000000,13.421400>}
box{<-0.177800,0,-0.711200><0.177800,0.036000,0.711200> rotate<0,-90.000000,0> translate<5.815000,0.000000,15.148600>}
//R17 silk screen
cylinder{<0,0,0><0,0.036000,0>0.076200 translate<-1.346200,-1.536000,17.780000>}
cylinder{<0,0,0><0,0.036000,0>0.076200 translate<0.076200,-1.536000,17.780000>}
box{<0,0,-0.076200><1.422400,0.036000,0.076200> rotate<0,0.000000,0> translate<-1.346200,-1.536000,17.780000> }
cylinder{<0,0,0><0,0.036000,0>0.076200 translate<-1.346200,-1.536000,16.510000>}
cylinder{<0,0,0><0,0.036000,0>0.076200 translate<0.076200,-1.536000,16.510000>}
box{<0,0,-0.076200><1.422400,0.036000,0.076200> rotate<0,0.000000,0> translate<-1.346200,-1.536000,16.510000> }
box{<-0.177800,0,-0.711200><0.177800,0.036000,0.711200> rotate<0,-180.000000,0> translate<0.228600,-1.536000,17.145000>}
box{<-0.177800,0,-0.711200><0.177800,0.036000,0.711200> rotate<0,-180.000000,0> translate<-1.498600,-1.536000,17.145000>}
//R18 silk screen
cylinder{<0,0,0><0,0.036000,0>0.076200 translate<-1.346200,-1.536000,14.605000>}
cylinder{<0,0,0><0,0.036000,0>0.076200 translate<0.076200,-1.536000,14.605000>}
box{<0,0,-0.076200><1.422400,0.036000,0.076200> rotate<0,0.000000,0> translate<-1.346200,-1.536000,14.605000> }
cylinder{<0,0,0><0,0.036000,0>0.076200 translate<-1.346200,-1.536000,13.335000>}
cylinder{<0,0,0><0,0.036000,0>0.076200 translate<0.076200,-1.536000,13.335000>}
box{<0,0,-0.076200><1.422400,0.036000,0.076200> rotate<0,0.000000,0> translate<-1.346200,-1.536000,13.335000> }
box{<-0.177800,0,-0.711200><0.177800,0.036000,0.711200> rotate<0,-180.000000,0> translate<0.228600,-1.536000,13.970000>}
box{<-0.177800,0,-0.711200><0.177800,0.036000,0.711200> rotate<0,-180.000000,0> translate<-1.498600,-1.536000,13.970000>}
//S1 silk screen
cylinder{<0,0,0><0,0.036000,0>0.076200 translate<51.502000,0.000000,25.638000>}
cylinder{<0,0,0><0,0.036000,0>0.076200 translate<51.248000,0.000000,25.638000>}
box{<0,0,-0.076200><0.254000,0.036000,0.076200> rotate<0,0.000000,0> translate<51.248000,0.000000,25.638000> }
cylinder{<0,0,0><0,0.036000,0>0.076200 translate<51.502000,0.000000,25.638000>}
cylinder{<0,0,0><0,0.036000,0>0.076200 translate<51.502000,0.000000,27.162000>}
box{<0,0,-0.076200><1.524000,0.036000,0.076200> rotate<0,90.000000,0> translate<51.502000,0.000000,27.162000> }
cylinder{<0,0,0><0,0.036000,0>0.076200 translate<51.248000,0.000000,27.162000>}
cylinder{<0,0,0><0,0.036000,0>0.076200 translate<51.502000,0.000000,27.162000>}
box{<0,0,-0.076200><0.254000,0.036000,0.076200> rotate<0,0.000000,0> translate<51.248000,0.000000,27.162000> }
cylinder{<0,0,0><0,0.036000,0>0.076200 translate<51.248000,0.000000,27.416000>}
cylinder{<0,0,0><0,0.036000,0>0.076200 translate<51.248000,0.000000,28.940000>}
box{<0,0,-0.076200><1.524000,0.036000,0.076200> rotate<0,90.000000,0> translate<51.248000,0.000000,28.940000> }
cylinder{<0,0,0><0,0.036000,0>0.076200 translate<44.898000,0.000000,27.162000>}
cylinder{<0,0,0><0,0.036000,0>0.076200 translate<45.152000,0.000000,27.162000>}
box{<0,0,-0.076200><0.254000,0.036000,0.076200> rotate<0,0.000000,0> translate<44.898000,0.000000,27.162000> }
cylinder{<0,0,0><0,0.036000,0>0.076200 translate<44.898000,0.000000,27.162000>}
cylinder{<0,0,0><0,0.036000,0>0.076200 translate<44.898000,0.000000,25.638000>}
box{<0,0,-0.076200><1.524000,0.036000,0.076200> rotate<0,-90.000000,0> translate<44.898000,0.000000,25.638000> }
cylinder{<0,0,0><0,0.036000,0>0.076200 translate<45.152000,0.000000,25.638000>}
cylinder{<0,0,0><0,0.036000,0>0.076200 translate<44.898000,0.000000,25.638000>}
box{<0,0,-0.076200><0.254000,0.036000,0.076200> rotate<0,0.000000,0> translate<44.898000,0.000000,25.638000> }
cylinder{<0,0,0><0,0.036000,0>0.076200 translate<51.248000,0.000000,28.940000>}
cylinder{<0,0,0><0,0.036000,0>0.076200 translate<50.740000,0.000000,29.448000>}
box{<0,0,-0.076200><0.718420,0.036000,0.076200> rotate<0,44.997030,0> translate<50.740000,0.000000,29.448000> }
cylinder{<0,0,0><0,0.036000,0>0.076200 translate<50.740000,0.000000,23.352000>}
cylinder{<0,0,0><0,0.036000,0>0.076200 translate<51.248000,0.000000,23.860000>}
box{<0,0,-0.076200><0.718420,0.036000,0.076200> rotate<0,-44.997030,0> translate<50.740000,0.000000,23.352000> }
cylinder{<0,0,0><0,0.036000,0>0.076200 translate<51.248000,0.000000,23.860000>}
cylinder{<0,0,0><0,0.036000,0>0.076200 translate<51.248000,0.000000,25.384000>}
box{<0,0,-0.076200><1.524000,0.036000,0.076200> rotate<0,90.000000,0> translate<51.248000,0.000000,25.384000> }
cylinder{<0,0,0><0,0.036000,0>0.076200 translate<45.660000,0.000000,29.448000>}
cylinder{<0,0,0><0,0.036000,0>0.076200 translate<45.152000,0.000000,28.940000>}
box{<0,0,-0.076200><0.718420,0.036000,0.076200> rotate<0,-44.997030,0> translate<45.152000,0.000000,28.940000> }
cylinder{<0,0,0><0,0.036000,0>0.076200 translate<45.152000,0.000000,28.940000>}
cylinder{<0,0,0><0,0.036000,0>0.076200 translate<45.152000,0.000000,27.416000>}
box{<0,0,-0.076200><1.524000,0.036000,0.076200> rotate<0,-90.000000,0> translate<45.152000,0.000000,27.416000> }
cylinder{<0,0,0><0,0.036000,0>0.076200 translate<45.660000,0.000000,23.352000>}
cylinder{<0,0,0><0,0.036000,0>0.076200 translate<45.152000,0.000000,23.860000>}
box{<0,0,-0.076200><0.718420,0.036000,0.076200> rotate<0,44.997030,0> translate<45.152000,0.000000,23.860000> }
cylinder{<0,0,0><0,0.036000,0>0.076200 translate<45.152000,0.000000,23.860000>}
cylinder{<0,0,0><0,0.036000,0>0.076200 translate<45.152000,0.000000,25.384000>}
box{<0,0,-0.076200><1.524000,0.036000,0.076200> rotate<0,90.000000,0> translate<45.152000,0.000000,25.384000> }
cylinder{<0,0,0><0,0.036000,0>0.025400 translate<46.930000,0.000000,27.670000>}
cylinder{<0,0,0><0,0.036000,0>0.025400 translate<46.930000,0.000000,25.130000>}
box{<0,0,-0.025400><2.540000,0.036000,0.025400> rotate<0,-90.000000,0> translate<46.930000,0.000000,25.130000> }
cylinder{<0,0,0><0,0.036000,0>0.025400 translate<49.470000,0.000000,25.130000>}
cylinder{<0,0,0><0,0.036000,0>0.025400 translate<46.930000,0.000000,25.130000>}
box{<0,0,-0.025400><2.540000,0.036000,0.025400> rotate<0,0.000000,0> translate<46.930000,0.000000,25.130000> }
cylinder{<0,0,0><0,0.036000,0>0.025400 translate<49.470000,0.000000,25.130000>}
cylinder{<0,0,0><0,0.036000,0>0.025400 translate<49.470000,0.000000,27.670000>}
box{<0,0,-0.025400><2.540000,0.036000,0.025400> rotate<0,90.000000,0> translate<49.470000,0.000000,27.670000> }
cylinder{<0,0,0><0,0.036000,0>0.025400 translate<46.930000,0.000000,27.670000>}
cylinder{<0,0,0><0,0.036000,0>0.025400 translate<49.470000,0.000000,27.670000>}
box{<0,0,-0.025400><2.540000,0.036000,0.025400> rotate<0,0.000000,0> translate<46.930000,0.000000,27.670000> }
cylinder{<0,0,0><0,0.036000,0>0.025400 translate<46.930000,0.000000,29.448000>}
cylinder{<0,0,0><0,0.036000,0>0.025400 translate<46.930000,0.000000,29.194000>}
box{<0,0,-0.025400><0.254000,0.036000,0.025400> rotate<0,-90.000000,0> translate<46.930000,0.000000,29.194000> }
cylinder{<0,0,0><0,0.036000,0>0.025400 translate<49.470000,0.000000,29.194000>}
cylinder{<0,0,0><0,0.036000,0>0.025400 translate<46.930000,0.000000,29.194000>}
box{<0,0,-0.025400><2.540000,0.036000,0.025400> rotate<0,0.000000,0> translate<46.930000,0.000000,29.194000> }
cylinder{<0,0,0><0,0.036000,0>0.025400 translate<49.470000,0.000000,29.194000>}
cylinder{<0,0,0><0,0.036000,0>0.025400 translate<49.470000,0.000000,29.448000>}
box{<0,0,-0.025400><0.254000,0.036000,0.025400> rotate<0,90.000000,0> translate<49.470000,0.000000,29.448000> }
cylinder{<0,0,0><0,0.036000,0>0.025400 translate<49.343000,0.000000,23.606000>}
cylinder{<0,0,0><0,0.036000,0>0.025400 translate<46.930000,0.000000,23.606000>}
box{<0,0,-0.025400><2.413000,0.036000,0.025400> rotate<0,0.000000,0> translate<46.930000,0.000000,23.606000> }
cylinder{<0,0,0><0,0.036000,0>0.025400 translate<49.343000,0.000000,23.606000>}
cylinder{<0,0,0><0,0.036000,0>0.025400 translate<49.343000,0.000000,23.352000>}
box{<0,0,-0.025400><0.254000,0.036000,0.025400> rotate<0,-90.000000,0> translate<49.343000,0.000000,23.352000> }
cylinder{<0,0,0><0,0.036000,0>0.025400 translate<46.930000,0.000000,23.606000>}
cylinder{<0,0,0><0,0.036000,0>0.025400 translate<46.930000,0.000000,23.352000>}
box{<0,0,-0.025400><0.254000,0.036000,0.025400> rotate<0,-90.000000,0> translate<46.930000,0.000000,23.352000> }
cylinder{<0,0,0><0,0.036000,0>0.076200 translate<50.740000,0.000000,23.352000>}
cylinder{<0,0,0><0,0.036000,0>0.076200 translate<50.359000,0.000000,23.352000>}
box{<0,0,-0.076200><0.381000,0.036000,0.076200> rotate<0,0.000000,0> translate<50.359000,0.000000,23.352000> }
cylinder{<0,0,0><0,0.036000,0>0.076200 translate<45.660000,0.000000,23.352000>}
cylinder{<0,0,0><0,0.036000,0>0.076200 translate<46.041000,0.000000,23.352000>}
box{<0,0,-0.076200><0.381000,0.036000,0.076200> rotate<0,0.000000,0> translate<45.660000,0.000000,23.352000> }
cylinder{<0,0,0><0,0.036000,0>0.076200 translate<46.041000,0.000000,23.352000>}
cylinder{<0,0,0><0,0.036000,0>0.076200 translate<46.930000,0.000000,23.352000>}
box{<0,0,-0.076200><0.889000,0.036000,0.076200> rotate<0,0.000000,0> translate<46.041000,0.000000,23.352000> }
cylinder{<0,0,0><0,0.036000,0>0.076200 translate<45.660000,0.000000,29.448000>}
cylinder{<0,0,0><0,0.036000,0>0.076200 translate<46.041000,0.000000,29.448000>}
box{<0,0,-0.076200><0.381000,0.036000,0.076200> rotate<0,0.000000,0> translate<45.660000,0.000000,29.448000> }
cylinder{<0,0,0><0,0.036000,0>0.076200 translate<50.740000,0.000000,29.448000>}
cylinder{<0,0,0><0,0.036000,0>0.076200 translate<50.359000,0.000000,29.448000>}
box{<0,0,-0.076200><0.381000,0.036000,0.076200> rotate<0,0.000000,0> translate<50.359000,0.000000,29.448000> }
cylinder{<0,0,0><0,0.036000,0>0.076200 translate<50.359000,0.000000,29.448000>}
cylinder{<0,0,0><0,0.036000,0>0.076200 translate<49.470000,0.000000,29.448000>}
box{<0,0,-0.076200><0.889000,0.036000,0.076200> rotate<0,0.000000,0> translate<49.470000,0.000000,29.448000> }
cylinder{<0,0,0><0,0.036000,0>0.076200 translate<49.470000,0.000000,29.448000>}
cylinder{<0,0,0><0,0.036000,0>0.076200 translate<46.930000,0.000000,29.448000>}
box{<0,0,-0.076200><2.540000,0.036000,0.076200> rotate<0,0.000000,0> translate<46.930000,0.000000,29.448000> }
cylinder{<0,0,0><0,0.036000,0>0.076200 translate<46.930000,0.000000,29.448000>}
cylinder{<0,0,0><0,0.036000,0>0.076200 translate<46.041000,0.000000,29.448000>}
box{<0,0,-0.076200><0.889000,0.036000,0.076200> rotate<0,0.000000,0> translate<46.041000,0.000000,29.448000> }
cylinder{<0,0,0><0,0.036000,0>0.076200 translate<46.930000,0.000000,23.352000>}
cylinder{<0,0,0><0,0.036000,0>0.076200 translate<49.343000,0.000000,23.352000>}
box{<0,0,-0.076200><2.413000,0.036000,0.076200> rotate<0,0.000000,0> translate<46.930000,0.000000,23.352000> }
cylinder{<0,0,0><0,0.036000,0>0.076200 translate<49.343000,0.000000,23.352000>}
cylinder{<0,0,0><0,0.036000,0>0.076200 translate<50.359000,0.000000,23.352000>}
box{<0,0,-0.076200><1.016000,0.036000,0.076200> rotate<0,0.000000,0> translate<49.343000,0.000000,23.352000> }
cylinder{<0,0,0><0,0.036000,0>0.076200 translate<51.248000,0.000000,25.638000>}
cylinder{<0,0,0><0,0.036000,0>0.076200 translate<51.248000,0.000000,25.384000>}
box{<0,0,-0.076200><0.254000,0.036000,0.076200> rotate<0,-90.000000,0> translate<51.248000,0.000000,25.384000> }
cylinder{<0,0,0><0,0.036000,0>0.076200 translate<51.248000,0.000000,27.162000>}
cylinder{<0,0,0><0,0.036000,0>0.076200 translate<51.248000,0.000000,27.416000>}
box{<0,0,-0.076200><0.254000,0.036000,0.076200> rotate<0,90.000000,0> translate<51.248000,0.000000,27.416000> }
cylinder{<0,0,0><0,0.036000,0>0.076200 translate<45.152000,0.000000,25.638000>}
cylinder{<0,0,0><0,0.036000,0>0.076200 translate<45.152000,0.000000,25.384000>}
box{<0,0,-0.076200><0.254000,0.036000,0.076200> rotate<0,-90.000000,0> translate<45.152000,0.000000,25.384000> }
cylinder{<0,0,0><0,0.036000,0>0.076200 translate<45.152000,0.000000,27.162000>}
cylinder{<0,0,0><0,0.036000,0>0.076200 translate<45.152000,0.000000,27.416000>}
box{<0,0,-0.076200><0.254000,0.036000,0.076200> rotate<0,90.000000,0> translate<45.152000,0.000000,27.416000> }
cylinder{<0,0,0><0,0.036000,0>0.076200 translate<46.930000,0.000000,24.241000>}
cylinder{<0,0,0><0,0.036000,0>0.076200 translate<49.470000,0.000000,24.241000>}
box{<0,0,-0.076200><2.540000,0.036000,0.076200> rotate<0,0.000000,0> translate<46.930000,0.000000,24.241000> }
cylinder{<0,0,0><0,0.036000,0>0.076200 translate<49.470000,0.000000,28.686000>}
cylinder{<0,0,0><0,0.036000,0>0.076200 translate<46.930000,0.000000,28.686000>}
box{<0,0,-0.076200><2.540000,0.036000,0.076200> rotate<0,0.000000,0> translate<46.930000,0.000000,28.686000> }
cylinder{<0,0,0><0,0.036000,0>0.076200 translate<45.787000,0.000000,27.670000>}
cylinder{<0,0,0><0,0.036000,0>0.076200 translate<45.787000,0.000000,26.908000>}
box{<0,0,-0.076200><0.762000,0.036000,0.076200> rotate<0,-90.000000,0> translate<45.787000,0.000000,26.908000> }
cylinder{<0,0,0><0,0.036000,0>0.076200 translate<45.787000,0.000000,25.892000>}
cylinder{<0,0,0><0,0.036000,0>0.076200 translate<45.787000,0.000000,25.130000>}
box{<0,0,-0.076200><0.762000,0.036000,0.076200> rotate<0,-90.000000,0> translate<45.787000,0.000000,25.130000> }
cylinder{<0,0,0><0,0.036000,0>0.076200 translate<45.787000,0.000000,26.908000>}
cylinder{<0,0,0><0,0.036000,0>0.076200 translate<46.041000,0.000000,26.019000>}
box{<0,0,-0.076200><0.924574,0.036000,0.076200> rotate<0,74.049717,0> translate<45.787000,0.000000,26.908000> }
difference{
cylinder{<48.200000,0,26.400000><48.200000,0.036000,26.400000>1.854200 translate<0,0.000000,0>}
cylinder{<48.200000,-0.1,26.400000><48.200000,0.135000,26.400000>1.701800 translate<0,0.000000,0>}}
difference{
cylinder{<46.041000,0,24.241000><46.041000,0.036000,24.241000>0.584200 translate<0,0.000000,0>}
cylinder{<46.041000,-0.1,24.241000><46.041000,0.135000,24.241000>0.431800 translate<0,0.000000,0>}}
difference{
cylinder{<50.359000,0,24.368000><50.359000,0.036000,24.368000>0.584200 translate<0,0.000000,0>}
cylinder{<50.359000,-0.1,24.368000><50.359000,0.135000,24.368000>0.431800 translate<0,0.000000,0>}}
difference{
cylinder{<50.359000,0,28.559000><50.359000,0.036000,28.559000>0.584200 translate<0,0.000000,0>}
cylinder{<50.359000,-0.1,28.559000><50.359000,0.135000,28.559000>0.431800 translate<0,0.000000,0>}}
difference{
cylinder{<46.041000,0,28.559000><46.041000,0.036000,28.559000>0.584200 translate<0,0.000000,0>}
cylinder{<46.041000,-0.1,28.559000><46.041000,0.135000,28.559000>0.431800 translate<0,0.000000,0>}}
difference{
cylinder{<48.200000,0,26.400000><48.200000,0.036000,26.400000>0.660400 translate<0,0.000000,0>}
cylinder{<48.200000,-0.1,26.400000><48.200000,0.135000,26.400000>0.609600 translate<0,0.000000,0>}}
difference{
cylinder{<48.200000,0,26.400000><48.200000,0.036000,26.400000>0.330200 translate<0,0.000000,0>}
cylinder{<48.200000,-0.1,26.400000><48.200000,0.135000,26.400000>0.177800 translate<0,0.000000,0>}}
//SV1 silk screen
cylinder{<0,0,0><0,0.036000,0>0.076200 translate<13.540000,-1.536000,10.555000>}
cylinder{<0,0,0><0,0.036000,0>0.076200 translate<13.540000,-1.536000,11.825000>}
box{<0,0,-0.076200><1.270000,0.036000,0.076200> rotate<0,90.000000,0> translate<13.540000,-1.536000,11.825000> }
cylinder{<0,0,0><0,0.036000,0>0.076200 translate<13.540000,-1.536000,11.825000>}
cylinder{<0,0,0><0,0.036000,0>0.076200 translate<12.905000,-1.536000,12.460000>}
box{<0,0,-0.076200><0.898026,0.036000,0.076200> rotate<0,44.997030,0> translate<12.905000,-1.536000,12.460000> }
cylinder{<0,0,0><0,0.036000,0>0.076200 translate<12.905000,-1.536000,12.460000>}
cylinder{<0,0,0><0,0.036000,0>0.076200 translate<13.540000,-1.536000,13.095000>}
box{<0,0,-0.076200><0.898026,0.036000,0.076200> rotate<0,-44.997030,0> translate<12.905000,-1.536000,12.460000> }
cylinder{<0,0,0><0,0.036000,0>0.076200 translate<13.540000,-1.536000,13.095000>}
cylinder{<0,0,0><0,0.036000,0>0.076200 translate<13.540000,-1.536000,14.365000>}
box{<0,0,-0.076200><1.270000,0.036000,0.076200> rotate<0,90.000000,0> translate<13.540000,-1.536000,14.365000> }
cylinder{<0,0,0><0,0.036000,0>0.076200 translate<13.540000,-1.536000,14.365000>}
cylinder{<0,0,0><0,0.036000,0>0.076200 translate<12.905000,-1.536000,15.000000>}
box{<0,0,-0.076200><0.898026,0.036000,0.076200> rotate<0,44.997030,0> translate<12.905000,-1.536000,15.000000> }
cylinder{<0,0,0><0,0.036000,0>0.076200 translate<13.540000,-1.536000,10.555000>}
cylinder{<0,0,0><0,0.036000,0>0.076200 translate<12.905000,-1.536000,9.920000>}
box{<0,0,-0.076200><0.898026,0.036000,0.076200> rotate<0,-44.997030,0> translate<12.905000,-1.536000,9.920000> }
cylinder{<0,0,0><0,0.036000,0>0.076200 translate<12.905000,-1.536000,15.000000>}
cylinder{<0,0,0><0,0.036000,0>0.076200 translate<13.540000,-1.536000,15.635000>}
box{<0,0,-0.076200><0.898026,0.036000,0.076200> rotate<0,-44.997030,0> translate<12.905000,-1.536000,15.000000> }
cylinder{<0,0,0><0,0.036000,0>0.076200 translate<13.540000,-1.536000,15.635000>}
cylinder{<0,0,0><0,0.036000,0>0.076200 translate<13.540000,-1.536000,16.905000>}
box{<0,0,-0.076200><1.270000,0.036000,0.076200> rotate<0,90.000000,0> translate<13.540000,-1.536000,16.905000> }
cylinder{<0,0,0><0,0.036000,0>0.076200 translate<13.540000,-1.536000,16.905000>}
cylinder{<0,0,0><0,0.036000,0>0.076200 translate<12.905000,-1.536000,17.540000>}
box{<0,0,-0.076200><0.898026,0.036000,0.076200> rotate<0,44.997030,0> translate<12.905000,-1.536000,17.540000> }
cylinder{<0,0,0><0,0.036000,0>0.076200 translate<13.540000,-1.536000,18.175000>}
cylinder{<0,0,0><0,0.036000,0>0.076200 translate<13.540000,-1.536000,19.445000>}
box{<0,0,-0.076200><1.270000,0.036000,0.076200> rotate<0,90.000000,0> translate<13.540000,-1.536000,19.445000> }
cylinder{<0,0,0><0,0.036000,0>0.076200 translate<13.540000,-1.536000,18.175000>}
cylinder{<0,0,0><0,0.036000,0>0.076200 translate<12.905000,-1.536000,17.540000>}
box{<0,0,-0.076200><0.898026,0.036000,0.076200> rotate<0,-44.997030,0> translate<12.905000,-1.536000,17.540000> }
cylinder{<0,0,0><0,0.036000,0>0.076200 translate<13.540000,-1.536000,19.445000>}
cylinder{<0,0,0><0,0.036000,0>0.076200 translate<12.905000,-1.536000,20.080000>}
box{<0,0,-0.076200><0.898026,0.036000,0.076200> rotate<0,44.997030,0> translate<12.905000,-1.536000,20.080000> }
cylinder{<0,0,0><0,0.036000,0>0.076200 translate<12.905000,-1.536000,20.080000>}
cylinder{<0,0,0><0,0.036000,0>0.076200 translate<9.095000,-1.536000,20.080000>}
box{<0,0,-0.076200><3.810000,0.036000,0.076200> rotate<0,0.000000,0> translate<9.095000,-1.536000,20.080000> }
cylinder{<0,0,0><0,0.036000,0>0.076200 translate<9.095000,-1.536000,12.460000>}
cylinder{<0,0,0><0,0.036000,0>0.076200 translate<8.460000,-1.536000,11.825000>}
box{<0,0,-0.076200><0.898026,0.036000,0.076200> rotate<0,-44.997030,0> translate<8.460000,-1.536000,11.825000> }
cylinder{<0,0,0><0,0.036000,0>0.076200 translate<9.095000,-1.536000,15.000000>}
cylinder{<0,0,0><0,0.036000,0>0.076200 translate<8.460000,-1.536000,14.365000>}
box{<0,0,-0.076200><0.898026,0.036000,0.076200> rotate<0,-44.997030,0> translate<8.460000,-1.536000,14.365000> }
cylinder{<0,0,0><0,0.036000,0>0.076200 translate<8.460000,-1.536000,14.365000>}
cylinder{<0,0,0><0,0.036000,0>0.076200 translate<8.460000,-1.536000,13.095000>}
box{<0,0,-0.076200><1.270000,0.036000,0.076200> rotate<0,-90.000000,0> translate<8.460000,-1.536000,13.095000> }
cylinder{<0,0,0><0,0.036000,0>0.076200 translate<8.460000,-1.536000,13.095000>}
cylinder{<0,0,0><0,0.036000,0>0.076200 translate<9.095000,-1.536000,12.460000>}
box{<0,0,-0.076200><0.898026,0.036000,0.076200> rotate<0,44.997030,0> translate<8.460000,-1.536000,13.095000> }
cylinder{<0,0,0><0,0.036000,0>0.076200 translate<12.905000,-1.536000,9.920000>}
cylinder{<0,0,0><0,0.036000,0>0.076200 translate<9.095000,-1.536000,9.920000>}
box{<0,0,-0.076200><3.810000,0.036000,0.076200> rotate<0,0.000000,0> translate<9.095000,-1.536000,9.920000> }
cylinder{<0,0,0><0,0.036000,0>0.076200 translate<9.095000,-1.536000,9.920000>}
cylinder{<0,0,0><0,0.036000,0>0.076200 translate<8.460000,-1.536000,10.555000>}
box{<0,0,-0.076200><0.898026,0.036000,0.076200> rotate<0,44.997030,0> translate<8.460000,-1.536000,10.555000> }
cylinder{<0,0,0><0,0.036000,0>0.076200 translate<8.460000,-1.536000,11.825000>}
cylinder{<0,0,0><0,0.036000,0>0.076200 translate<8.460000,-1.536000,10.555000>}
box{<0,0,-0.076200><1.270000,0.036000,0.076200> rotate<0,-90.000000,0> translate<8.460000,-1.536000,10.555000> }
cylinder{<0,0,0><0,0.036000,0>0.076200 translate<9.095000,-1.536000,17.540000>}
cylinder{<0,0,0><0,0.036000,0>0.076200 translate<8.460000,-1.536000,16.905000>}
box{<0,0,-0.076200><0.898026,0.036000,0.076200> rotate<0,-44.997030,0> translate<8.460000,-1.536000,16.905000> }
cylinder{<0,0,0><0,0.036000,0>0.076200 translate<8.460000,-1.536000,16.905000>}
cylinder{<0,0,0><0,0.036000,0>0.076200 translate<8.460000,-1.536000,15.635000>}
box{<0,0,-0.076200><1.270000,0.036000,0.076200> rotate<0,-90.000000,0> translate<8.460000,-1.536000,15.635000> }
cylinder{<0,0,0><0,0.036000,0>0.076200 translate<8.460000,-1.536000,15.635000>}
cylinder{<0,0,0><0,0.036000,0>0.076200 translate<9.095000,-1.536000,15.000000>}
box{<0,0,-0.076200><0.898026,0.036000,0.076200> rotate<0,44.997030,0> translate<8.460000,-1.536000,15.635000> }
cylinder{<0,0,0><0,0.036000,0>0.076200 translate<9.095000,-1.536000,17.540000>}
cylinder{<0,0,0><0,0.036000,0>0.076200 translate<8.460000,-1.536000,18.175000>}
box{<0,0,-0.076200><0.898026,0.036000,0.076200> rotate<0,44.997030,0> translate<8.460000,-1.536000,18.175000> }
cylinder{<0,0,0><0,0.036000,0>0.076200 translate<8.460000,-1.536000,19.445000>}
cylinder{<0,0,0><0,0.036000,0>0.076200 translate<8.460000,-1.536000,18.175000>}
box{<0,0,-0.076200><1.270000,0.036000,0.076200> rotate<0,-90.000000,0> translate<8.460000,-1.536000,18.175000> }
cylinder{<0,0,0><0,0.036000,0>0.076200 translate<9.095000,-1.536000,20.080000>}
cylinder{<0,0,0><0,0.036000,0>0.076200 translate<8.460000,-1.536000,19.445000>}
box{<0,0,-0.076200><0.898026,0.036000,0.076200> rotate<0,-44.997030,0> translate<8.460000,-1.536000,19.445000> }
box{<-0.254000,0,-0.254000><0.254000,0.036000,0.254000> rotate<0,-90.000000,0> translate<9.730000,-1.536000,13.730000>}
box{<-0.254000,0,-0.254000><0.254000,0.036000,0.254000> rotate<0,-90.000000,0> translate<9.730000,-1.536000,11.190000>}
box{<-0.254000,0,-0.254000><0.254000,0.036000,0.254000> rotate<0,-90.000000,0> translate<9.730000,-1.536000,16.270000>}
box{<-0.254000,0,-0.254000><0.254000,0.036000,0.254000> rotate<0,-90.000000,0> translate<9.730000,-1.536000,18.810000>}
box{<-0.254000,0,-0.254000><0.254000,0.036000,0.254000> rotate<0,-90.000000,0> translate<12.270000,-1.536000,11.190000>}
box{<-0.254000,0,-0.254000><0.254000,0.036000,0.254000> rotate<0,-90.000000,0> translate<12.270000,-1.536000,13.730000>}
box{<-0.254000,0,-0.254000><0.254000,0.036000,0.254000> rotate<0,-90.000000,0> translate<12.270000,-1.536000,16.270000>}
box{<-0.254000,0,-0.254000><0.254000,0.036000,0.254000> rotate<0,-90.000000,0> translate<12.270000,-1.536000,18.810000>}
//T1 silk screen
cylinder{<0,0,0><0,0.036000,0>0.063500 translate<-9.377600,-1.536000,20.239600>}
cylinder{<0,0,0><0,0.036000,0>0.063500 translate<-9.377600,-1.536000,21.560400>}
box{<0,0,-0.063500><1.320800,0.036000,0.063500> rotate<0,90.000000,0> translate<-9.377600,-1.536000,21.560400> }
cylinder{<0,0,0><0,0.036000,0>0.063500 translate<-9.377600,-1.536000,21.560400>}
cylinder{<0,0,0><0,0.036000,0>0.063500 translate<-12.222400,-1.536000,21.560400>}
box{<0,0,-0.063500><2.844800,0.036000,0.063500> rotate<0,0.000000,0> translate<-12.222400,-1.536000,21.560400> }
cylinder{<0,0,0><0,0.036000,0>0.063500 translate<-12.222400,-1.536000,21.560400>}
cylinder{<0,0,0><0,0.036000,0>0.063500 translate<-12.222400,-1.536000,20.239600>}
box{<0,0,-0.063500><1.320800,0.036000,0.063500> rotate<0,-90.000000,0> translate<-12.222400,-1.536000,20.239600> }
cylinder{<0,0,0><0,0.036000,0>0.063500 translate<-12.222400,-1.536000,20.239600>}
cylinder{<0,0,0><0,0.036000,0>0.063500 translate<-9.377600,-1.536000,20.239600>}
box{<0,0,-0.063500><2.844800,0.036000,0.063500> rotate<0,0.000000,0> translate<-12.222400,-1.536000,20.239600> }
box{<-0.228600,0,-0.292100><0.228600,0.036000,0.292100> rotate<0,-0.000000,0> translate<-10.800000,-1.536000,19.896700>}
box{<-0.228600,0,-0.292100><0.228600,0.036000,0.292100> rotate<0,-0.000000,0> translate<-9.860200,-1.536000,21.903300>}
box{<-0.228600,0,-0.292100><0.228600,0.036000,0.292100> rotate<0,-0.000000,0> translate<-11.739800,-1.536000,21.903300>}
//T2 silk screen
cylinder{<0,0,0><0,0.036000,0>0.063500 translate<-6.527800,-1.536000,16.078200>}
cylinder{<0,0,0><0,0.036000,0>0.063500 translate<-6.527800,-1.536000,14.757400>}
box{<0,0,-0.063500><1.320800,0.036000,0.063500> rotate<0,-90.000000,0> translate<-6.527800,-1.536000,14.757400> }
cylinder{<0,0,0><0,0.036000,0>0.063500 translate<-6.527800,-1.536000,14.757400>}
cylinder{<0,0,0><0,0.036000,0>0.063500 translate<-3.683000,-1.536000,14.757400>}
box{<0,0,-0.063500><2.844800,0.036000,0.063500> rotate<0,0.000000,0> translate<-6.527800,-1.536000,14.757400> }
cylinder{<0,0,0><0,0.036000,0>0.063500 translate<-3.683000,-1.536000,14.757400>}
cylinder{<0,0,0><0,0.036000,0>0.063500 translate<-3.683000,-1.536000,16.078200>}
box{<0,0,-0.063500><1.320800,0.036000,0.063500> rotate<0,90.000000,0> translate<-3.683000,-1.536000,16.078200> }
cylinder{<0,0,0><0,0.036000,0>0.063500 translate<-3.683000,-1.536000,16.078200>}
cylinder{<0,0,0><0,0.036000,0>0.063500 translate<-6.527800,-1.536000,16.078200>}
box{<0,0,-0.063500><2.844800,0.036000,0.063500> rotate<0,0.000000,0> translate<-6.527800,-1.536000,16.078200> }
box{<-0.228600,0,-0.292100><0.228600,0.036000,0.292100> rotate<0,-180.000000,0> translate<-5.105400,-1.536000,16.421100>}
box{<-0.228600,0,-0.292100><0.228600,0.036000,0.292100> rotate<0,-180.000000,0> translate<-6.045200,-1.536000,14.414500>}
box{<-0.228600,0,-0.292100><0.228600,0.036000,0.292100> rotate<0,-180.000000,0> translate<-4.165600,-1.536000,14.414500>}
//U1 silk screen
cylinder{<0,0,0><0,0.036000,0>0.101600 translate<-7.700000,0.000000,15.000000>}
cylinder{<0,0,0><0,0.036000,0>0.101600 translate<-11.000000,0.000000,15.000000>}
box{<0,0,-0.101600><3.300000,0.036000,0.101600> rotate<0,0.000000,0> translate<-11.000000,0.000000,15.000000> }
cylinder{<0,0,0><0,0.036000,0>0.101600 translate<-11.000000,0.000000,15.000000>}
cylinder{<0,0,0><0,0.036000,0>0.101600 translate<-11.500000,0.000000,15.000000>}
box{<0,0,-0.101600><0.500000,0.036000,0.101600> rotate<0,0.000000,0> translate<-11.500000,0.000000,15.000000> }
cylinder{<0,0,0><0,0.036000,0>0.101600 translate<-11.500000,0.000000,15.000000>}
cylinder{<0,0,0><0,0.036000,0>0.101600 translate<-11.500000,0.000000,19.800000>}
box{<0,0,-0.101600><4.800000,0.036000,0.101600> rotate<0,90.000000,0> translate<-11.500000,0.000000,19.800000> }
cylinder{<0,0,0><0,0.036000,0>0.101600 translate<-11.500000,0.000000,19.800000>}
cylinder{<0,0,0><0,0.036000,0>0.101600 translate<-11.000000,0.000000,19.800000>}
box{<0,0,-0.101600><0.500000,0.036000,0.101600> rotate<0,0.000000,0> translate<-11.500000,0.000000,19.800000> }
cylinder{<0,0,0><0,0.036000,0>0.101600 translate<-11.000000,0.000000,19.800000>}
cylinder{<0,0,0><0,0.036000,0>0.101600 translate<-7.700000,0.000000,19.800000>}
box{<0,0,-0.101600><3.300000,0.036000,0.101600> rotate<0,0.000000,0> translate<-11.000000,0.000000,19.800000> }
cylinder{<0,0,0><0,0.036000,0>0.101600 translate<-7.700000,0.000000,19.800000>}
cylinder{<0,0,0><0,0.036000,0>0.101600 translate<-7.700000,0.000000,15.000000>}
box{<0,0,-0.101600><4.800000,0.036000,0.101600> rotate<0,-90.000000,0> translate<-7.700000,0.000000,15.000000> }
cylinder{<0,0,0><0,0.036000,0>0.101600 translate<-11.000000,0.000000,15.000000>}
cylinder{<0,0,0><0,0.036000,0>0.101600 translate<-11.000000,0.000000,19.800000>}
box{<0,0,-0.101600><4.800000,0.036000,0.101600> rotate<0,90.000000,0> translate<-11.000000,0.000000,19.800000> }
box{<-0.245000,0,-0.550000><0.245000,0.036000,0.550000> rotate<0,-270.000000,0> translate<-12.150000,0.000000,19.305000>}
box{<-0.245000,0,-0.550000><0.245000,0.036000,0.550000> rotate<0,-270.000000,0> translate<-12.150000,0.000000,18.035000>}
box{<-0.245000,0,-0.550000><0.245000,0.036000,0.550000> rotate<0,-270.000000,0> translate<-12.150000,0.000000,16.765000>}
box{<-0.245000,0,-0.550000><0.245000,0.036000,0.550000> rotate<0,-270.000000,0> translate<-12.150000,0.000000,15.495000>}
box{<-0.245000,0,-0.550000><0.245000,0.036000,0.550000> rotate<0,-270.000000,0> translate<-7.050000,0.000000,15.495000>}
box{<-0.245000,0,-0.550000><0.245000,0.036000,0.550000> rotate<0,-270.000000,0> translate<-7.050000,0.000000,16.765000>}
box{<-0.245000,0,-0.550000><0.245000,0.036000,0.550000> rotate<0,-270.000000,0> translate<-7.050000,0.000000,18.035000>}
box{<-0.245000,0,-0.550000><0.245000,0.036000,0.550000> rotate<0,-270.000000,0> translate<-7.050000,0.000000,19.305000>}
//X1 silk screen
cylinder{<0,0,0><0,0.036000,0>0.101600 translate<19.550000,0.000000,21.990000>}
cylinder{<0,0,0><0,0.036000,0>0.101600 translate<19.550000,0.000000,23.790000>}
box{<0,0,-0.101600><1.800000,0.036000,0.101600> rotate<0,90.000000,0> translate<19.550000,0.000000,23.790000> }
cylinder{<0,0,0><0,0.036000,0>0.101600 translate<19.550000,0.000000,23.790000>}
cylinder{<0,0,0><0,0.036000,0>0.101600 translate<19.550000,0.000000,27.785000>}
box{<0,0,-0.101600><3.995000,0.036000,0.101600> rotate<0,90.000000,0> translate<19.550000,0.000000,27.785000> }
cylinder{<0,0,0><0,0.036000,0>0.101600 translate<19.550000,0.000000,27.785000>}
cylinder{<0,0,0><0,0.036000,0>0.101600 translate<19.550000,0.000000,29.585000>}
box{<0,0,-0.101600><1.800000,0.036000,0.101600> rotate<0,90.000000,0> translate<19.550000,0.000000,29.585000> }
cylinder{<0,0,0><0,0.036000,0>0.101600 translate<19.550000,0.000000,29.585000>}
cylinder{<0,0,0><0,0.036000,0>0.101600 translate<9.775000,0.000000,29.585000>}
box{<0,0,-0.101600><9.775000,0.036000,0.101600> rotate<0,0.000000,0> translate<9.775000,0.000000,29.585000> }
cylinder{<0,0,0><0,0.036000,0>0.101600 translate<9.775000,0.000000,29.585000>}
cylinder{<0,0,0><0,0.036000,0>0.101600 translate<9.775000,0.000000,21.990000>}
box{<0,0,-0.101600><7.595000,0.036000,0.101600> rotate<0,-90.000000,0> translate<9.775000,0.000000,21.990000> }
cylinder{<0,0,0><0,0.036000,0>0.101600 translate<9.775000,0.000000,21.990000>}
cylinder{<0,0,0><0,0.036000,0>0.101600 translate<19.550000,0.000000,21.990000>}
box{<0,0,-0.101600><9.775000,0.036000,0.101600> rotate<0,0.000000,0> translate<9.775000,0.000000,21.990000> }
cylinder{<0,0,0><0,0.036000,0>0.101600 translate<17.125000,0.000000,27.785000>}
cylinder{<0,0,0><0,0.036000,0>0.101600 translate<17.125000,0.000000,23.790000>}
box{<0,0,-0.101600><3.995000,0.036000,0.101600> rotate<0,-90.000000,0> translate<17.125000,0.000000,23.790000> }
cylinder{<0,0,0><0,0.036000,0>0.101600 translate<17.125000,0.000000,23.790000>}
cylinder{<0,0,0><0,0.036000,0>0.101600 translate<19.550000,0.000000,23.790000>}
box{<0,0,-0.101600><2.425000,0.036000,0.101600> rotate<0,0.000000,0> translate<17.125000,0.000000,23.790000> }
cylinder{<0,0,0><0,0.036000,0>0.101600 translate<17.125000,0.000000,27.785000>}
cylinder{<0,0,0><0,0.036000,0>0.101600 translate<19.550000,0.000000,27.785000>}
box{<0,0,-0.101600><2.425000,0.036000,0.101600> rotate<0,0.000000,0> translate<17.125000,0.000000,27.785000> }
//X2 silk screen
cylinder{<0,0,0><0,0.036000,0>0.101600 translate<52.050000,0.000000,24.770000>}
cylinder{<0,0,0><0,0.036000,0>0.101600 translate<52.050000,0.000000,22.970000>}
box{<0,0,-0.101600><1.800000,0.036000,0.101600> rotate<0,-90.000000,0> translate<52.050000,0.000000,22.970000> }
cylinder{<0,0,0><0,0.036000,0>0.101600 translate<52.050000,0.000000,22.970000>}
cylinder{<0,0,0><0,0.036000,0>0.101600 translate<52.050000,0.000000,11.055000>}
box{<0,0,-0.101600><11.915000,0.036000,0.101600> rotate<0,-90.000000,0> translate<52.050000,0.000000,11.055000> }
cylinder{<0,0,0><0,0.036000,0>0.101600 translate<52.050000,0.000000,11.055000>}
cylinder{<0,0,0><0,0.036000,0>0.101600 translate<52.050000,0.000000,9.255000>}
box{<0,0,-0.101600><1.800000,0.036000,0.101600> rotate<0,-90.000000,0> translate<52.050000,0.000000,9.255000> }
cylinder{<0,0,0><0,0.036000,0>0.101600 translate<52.050000,0.000000,9.255000>}
cylinder{<0,0,0><0,0.036000,0>0.101600 translate<61.825000,0.000000,9.255000>}
box{<0,0,-0.101600><9.775000,0.036000,0.101600> rotate<0,0.000000,0> translate<52.050000,0.000000,9.255000> }
cylinder{<0,0,0><0,0.036000,0>0.101600 translate<61.825000,0.000000,9.255000>}
cylinder{<0,0,0><0,0.036000,0>0.101600 translate<61.825000,0.000000,24.770000>}
box{<0,0,-0.101600><15.515000,0.036000,0.101600> rotate<0,90.000000,0> translate<61.825000,0.000000,24.770000> }
cylinder{<0,0,0><0,0.036000,0>0.101600 translate<61.825000,0.000000,24.770000>}
cylinder{<0,0,0><0,0.036000,0>0.101600 translate<52.050000,0.000000,24.770000>}
box{<0,0,-0.101600><9.775000,0.036000,0.101600> rotate<0,0.000000,0> translate<52.050000,0.000000,24.770000> }
cylinder{<0,0,0><0,0.036000,0>0.101600 translate<54.475000,0.000000,11.055000>}
cylinder{<0,0,0><0,0.036000,0>0.101600 translate<54.475000,0.000000,22.970000>}
box{<0,0,-0.101600><11.915000,0.036000,0.101600> rotate<0,90.000000,0> translate<54.475000,0.000000,22.970000> }
cylinder{<0,0,0><0,0.036000,0>0.101600 translate<54.475000,0.000000,22.970000>}
cylinder{<0,0,0><0,0.036000,0>0.101600 translate<52.050000,0.000000,22.970000>}
box{<0,0,-0.101600><2.425000,0.036000,0.101600> rotate<0,0.000000,0> translate<52.050000,0.000000,22.970000> }
cylinder{<0,0,0><0,0.036000,0>0.101600 translate<54.475000,0.000000,11.055000>}
cylinder{<0,0,0><0,0.036000,0>0.101600 translate<52.050000,0.000000,11.055000>}
box{<0,0,-0.101600><2.425000,0.036000,0.101600> rotate<0,0.000000,0> translate<52.050000,0.000000,11.055000> }
texture{col_slk}
}
#end
translate<mac_x_ver,mac_y_ver,mac_z_ver>
rotate<mac_x_rot,mac_y_rot,mac_z_rot>
}//End union
#end

#if(use_file_as_inc=off)
object{  KICK_2010_REVB2(-22.000000,0,-15.000000,pcb_rotate_x,pcb_rotate_y,pcb_rotate_z)
#if(pcb_upsidedown=on)
rotate pcb_rotdir*180
#end
}
#end


//Parts not found in 3dpack.dat or 3dusrpac.dat are:
//IC1	PC817	SFH
//IC3	PC817	SFH
//IC4	PC817	SFH
